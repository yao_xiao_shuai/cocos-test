/**@description 大厅类公共cmd定义 */
export const SUB_CMD_LOBBY = {
    /**@description json消息测试 */
    TEST_JSON_MSG: 2,
    /**@description proto消息测试 */
    TEST_PROTO_MSG: 3,
    /**@description 二进制流消息测试 */
    TEST_BINARY_MSG: 4,
}