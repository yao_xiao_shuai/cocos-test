 
 export namespace NetTest{
     export enum NetType{
         JSON,
         PROTO,
         BINARY,
     }
     export enum ServiceType{
         Lobby,
         Game,
         Chat,
     }
 }