// Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.  

CCEffect %{
  techniques:
  - passes:
    - vert: vs
      frag: fs
      blendState:
        targets:
        - blend: true
      rasterizerState:
        cullMode: none
      properties:
        texture: { value: white }
        alphaThreshold: { value: 0.5 }
}%


CCProgram vs %{
  precision highp float;

  #include <cc-global>
  #include <cc-local>

  in vec3 a_position;
  in vec4 a_color;
  out vec4 v_color;

  #if USE_TEXTURE
  in vec2 a_uv0;
  out vec2 v_uv0;
  #endif

  void main () {
    vec4 pos = vec4(a_position, 1);

    #if CC_USE_MODEL
    pos = cc_matViewProj * cc_matWorld * pos;
    #else
    pos = cc_matViewProj * pos;
    #endif

    #if USE_TEXTURE
    v_uv0 = a_uv0;
    #endif

    v_color = a_color;

    gl_Position = pos;
  }
}%


CCProgram fs %{
  precision highp float;
  
  #include <alpha-test>
  #include <texture>

  in vec4 v_color;

  #if USE_TEXTURE
  in vec2 v_uv0;
  uniform sampler2D texture;
  #endif

  void main () {
    vec4 color = v_color;
    //texture GLSL内建的texture函数来采样纹理的颜色，
    //它第一个参数是纹理采样器，第二个参数是对应的纹理坐标。
    //texture函数会使用之前设置的纹理参数对相应的颜色值进行采样。
    //这个片段着色器的输出就是纹理的（插值）纹理坐标上的(过滤后的)颜色。
    CCTexture(texture,v_uv0,color);
    float grayValue = dot(color.rgb,vec3(0.3,0.59,0.11));
    gl_FragColor = vec4(grayValue,grayValue,grayValue,color.a);
  }
}%
