var gp = {
    ServerActive: 0,
    Serial: 1
};
gp.MessageCodes = {};
gp.chk = function (loc, validLoc) {
    var value = 1;
    if (loc >= 31) {
        value = dcodeIO.Long.fromNumber(value);
        value = value.shiftLeft(loc);
        validLoc = dcodeIO.Long.fromNumber(validLoc);
        return validLoc.and(value).compare(value) == 0;
    }
    else {
        value = value << loc;
        return (validLoc & value) == value;
    }
};
gp.add = function (loc, validLoc) {
    if (gp.chk(loc, validLoc)) {
        return validLoc;
    }
    else {
        validLoc = (validLoc | (1 << loc));
        return validLoc;
    }
};
var get = function (options) {
    var result = options || {};
    result.mainType = this.MAIN_TYPE;
    result.subType = this.SUB_TYPE;
    result.destinationId = GameProtocol.destinationId;
    return result;
};
var readFromBuff = function (fieldType, b) {
    var value = undefined;
    if (fieldType == "byte" || fieldType == "char")
        value = b.rB();
    else if (fieldType == "ubyte")
        value = b.rUi8();
    else if (fieldType == "short")
        value = b.rI16();
    else if (fieldType == "int")
        value = b.rI();
    else if (fieldType == "long")
        value = b.rL();
    else if (fieldType == "float")
        value = b.rF();
    else if (fieldType == "double")
        value = b.rD();
    else if (fieldType == "bool")
        value = b.rBo();
    else if (fieldType == "string")
        value = b.rVS();
    else if (fieldType == "istring")
        value = b.rIS();
    else {
        fieldType = fieldType + "Unit";
        if (window.GameProtocol[fieldType])
            value = window.GameProtocol[fieldType].decode({}, b);
    }
    return value;
};
var baseTypeMap = {
    byte: "wB",
    char: "wB",
    ubyte: "wUi8",
    short: "wI16",
    int: "wI",
    long: "wL",
    float: "wF",
    double: "wD",
    bool: "wBo",
    string: "wVS",
    istring: "wIS"
};
function baseVueNotTemp(value) {
    return value !== null && value !== NaN && value !== undefined && value !== "";
}
var writeToBuff = function (fieldType, b, value) {
    if (baseTypeMap[fieldType]) {
        if (baseVueNotTemp(value)) {
            b[baseTypeMap[fieldType]](value);
        }
    }
    else {
        fieldType = fieldType + "Unit";
        if (window.GameProtocol[fieldType]) {
            if (value) {
                window.GameProtocol[fieldType].encode(value, b);
            }
        }
    }
};
var messageEncode = function (o, b) {
    // validate field location info
    var loc = 0;
    for (var i = 0; i < this.fields.length; i++) {
        var fieldInfo = this.fields[i];
        if (baseTypeMap[fieldInfo.type]) {
            if (baseVueNotTemp(o[fieldInfo.name])) {
                loc = window.GameProtocol.add(fieldInfo.index, loc);
            }
        }
        else if (fieldInfo.type == "List") {
            if (o[fieldInfo.name] != undefined && o[fieldInfo.name][0] != undefined) {
                loc = window.GameProtocol.add(fieldInfo.index, loc);
            }
        }
        else {
            if (o[fieldInfo.name]) {
                loc = window.GameProtocol.add(fieldInfo.index, loc);
            }
        }
    }
    // write field size to byte buff
    var sizeField = this.fields.length;
    if (sizeField >= 31) {
        b.wL(loc);
    }
    else if (sizeField >= 15) {
        b.wI(loc);
    }
    else if (sizeField >= 7) {
        b.wI16(loc);
    }
    else {
        b.wB(loc);
    }
    // encode value to byte buff
    for (var i = 0; i < this.fields.length; i++) {
        var fieldInfo = this.fields[i];
        if (fieldInfo.type == "List") {
            if (o[fieldInfo.name] && o[fieldInfo.name][0] != undefined) {
                b.wVI(o[fieldInfo.name].length);
                for (var j = 0; j < o[fieldInfo.name].length; j++) {
                    var e = o[fieldInfo.name][j];
                    writeToBuff(fieldInfo.valueType, b, e);
                }
            }
        }
        else {
            writeToBuff(fieldInfo.type, b, o[fieldInfo.name]);
        }
    }
    // only flip on class message object
    if (this.MAIN_TYPE && this.SUB_TYPE) {
        b.flip();
    }
    return b;
};
var messageDecode = function (o, b) {
    // default value
    for (var i = 0; i < this.fields.length; i++) {
        var fieldInfo = this.fields[i];
        if (fieldInfo.default != undefined) {
            o[fieldInfo.name] = fieldInfo.default;
        }
        else if (fieldInfo.type == "List") {
            o[fieldInfo.name] = [];
        }
    }
    // effective field with values
    var loc = 0;
    var sizeField = this.fields.length;
    if (sizeField >= 31) {
        loc = b.rL();
    }
    else if (sizeField >= 15) {
        loc = b.rI();
    }
    else if (sizeField >= 7) {
        loc = b.rI16();
    }
    else {
        loc = b.rB();
    }
    // decode value from byte buff
    for (var i = 0; i < this.fields.length; i++) {
        var fieldInfo = this.fields[i];
        if (window.GameProtocol.chk(fieldInfo.index, loc)) {
            if (fieldInfo.type == "List") {
                var len = b.rVI();
                for (var j = 0; j < len; j++) {
                    o[fieldInfo.name].push(readFromBuff(fieldInfo.valueType, b));
                }
            }
            else {
                o[fieldInfo.name] = readFromBuff(fieldInfo.type, b);
            }
        }
    }
    return o;
};
gp.initAllMessagesFromJson = function (messageDefines) {
    for (var index = 0; index < messageDefines.length; index++) {
        var info = messageDefines[index];
        var mObj = {
            MAIN_TYPE: info.MAIN_TYPE,
            SUB_TYPE: info.SUB_TYPE,
            fields: info.fields,
            get: get,
            encode: messageEncode,
            decode: messageDecode,
        };
        this[info.name] = mObj;
        this.MessageCodes[info.MAIN_TYPE << 8 | info.SUB_TYPE] = mObj;
    }
};
window.GameProtocol = gp;
