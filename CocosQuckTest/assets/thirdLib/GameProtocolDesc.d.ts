declare module GameProtocol { 
    var destinationId;
    var TYPE_STATE_SERVER;
    var TYPE_ACCOUNT_SERVER;
    var MessageCodes:any 
    function initAllMessagesFromJson(messageDefines):any; 
// 活动详情
    module ActivityDetailInfoUnit  {
        function decode(b):any;
    }
// 挑战神宠关卡信息
    module TiaoZhanShenShouGuanKaInfoUnit  {
        function decode(b):any;
    }
// 客户端奖励信息
    module AwardInfoUnit  {
        function decode(b):any;
    }
// 地图等级列表信息
    module BossLevelListMapUnit  {
        function decode(b):any;
    }
// 地图列表信息
    module BossListMapUnit  {
        function decode(b):any;
    }
// 地图怪物信息
    module BossMapMonsterUnit  {
        function decode(b):any;
    }
// boss玩家基本信息
    module BossPlayerInfoUnit  {
        function decode(b):any;
    }
// 地图内的攻击受击信息
    module AttackInMapUnit  {
        function decode(b):any;
    }
// 归属权
    module CanGetRewardUnit  {
        function decode(b):any;
    }
// 地图等级额外推送信息
    module BossSceneOtherUnit  {
        function decode(b):any;
    }
// 援助boss请求信息
    module BossYuanZhuInfoUnit  {
        function decode(b):any;
    }
// 玩家加血
    module BossPlayerAddHpUnit  {
        function decode(b):any;
    }
// 地图怪物信息For UI
    module BossMapMonsterForUIUnit  {
        function decode(b):any;
    }
// 跨服boss地图对应的区服编号
    module BossMapServerInfoUnit  {
        function decode(b):any;
    }
// VIP才能有机会获得的奖励
    module VipJiangliInfoUnit  {
        function decode(b):any;
    }
// 世界战场剩余次数信息
    module BossLeftCountInfoUnit  {
        function decode(b):any;
    }
// 世界战场剩余次数信息
    module BossHpInfoUnit  {
        function decode(b):any;
    }
// 比武记录数据
    module BiwuRecordInfoUnit  {
        function decode(b):any;
    }
// 比武对手武将数据
    module BiwuHeroInfoUnit  {
        function decode(b):any;
    }
// 比武对手数据
    module BiwuInfoUnit  {
        function decode(b):any;
    }
// 属性数据
    module PropertyInfoUnit  {
        function decode(b):any;
    }
// 排行信息
    module BiWuPaiHangUnit  {
        function decode(b):any;
    }
// 忍村战斗boss数据校验
    module BossCheckUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// boss房间玩家简易信息
    module BossPlayerSimpleInfoUnit  {
        function decode(b):any;
    }
// 不思议礼包
    module BuSiYiLiBaoUnit  {
        function decode(b):any;
    }
// 财神殿排行数据
    module CaishendianPaihangInfoUnit  {
        function decode(b):any;
    }
// 登录的时候推送20条信息  他的里面套一个ChatMessageInfo
    module ChatMessageLoginInfoUnit  {
        function decode(b):any;
    }
// 私聊玩家信息
    module PersonalChatInfoUnit  {
        function decode(b):any;
    }
// 聊天内容中的物品信息（1'道具', 2'装备', 3'武将', 4'阵法', 5'耳朵'）
    module ChatMessageInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 待删除的聊天信息
    module DeleteChatInfoUnit  {
        function decode(b):any;
    }
// 奖励数据
    module ChongjiJiangliInfoUnit  {
        function decode(b):any;
    }
// 充值金额数据
    module ChongzhiJineInfoUnit  {
        function decode(b):any;
    }
// 优惠券
    module YouHuiQuanInfoUnit  {
        function decode(b):any;
    }
// 首冲信息
    module ChongZhiShouCiNewUnit  {
        function decode(b):any;
    }
// 首冲的一次性奖励信息
    module ChongZhiShouCiNewOnceRewardUnit  {
        function decode(b):any;
    }
// 订单的发货额外附加信息
    module OrderDeliveryAttachInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 次日登录奖励数据
    module CiriJiangliInfoUnit  {
        function decode(b):any;
    }
// 比率数据
    module RatioDataListUnit  {
        function decode(b):any;
    }
//  数据库id和数量数据
    module DBIdAndIntListUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 客户端奖励信息
    module CompleteSuppressionInfoUnit  {
        function decode(b):any;
    }
// 奖励数据
    module CostItemUnit  {
        function decode(b):any;
    }
// 跨服锦标赛公会更新积分信息
    module KuaFuJBSGongHuiScoreInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 跨服锦标赛个人更新积分信息
    module KuaFuJBSGeRenScoreInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 财神殿奖励说明
    module CsdJiangliExplainInfoUnit  {
        function decode(b):any;
    }
// 财神殿奖励数据
    module CsdJiangliInfoUnit  {
        function decode(b):any;
    }
// 特殊累计充值展示信息
    module CumulativeRechargeDisplayInfoUnit  {
        function decode(b):any;
    }
// 道具互换
    module DaoJuHuHuanInfoUnit  {
        function decode(b):any;
    }
// 装备数据
    module DaojuInfoUnit  {
        function decode(b):any;
    }
// 道具消耗数据
    module DaoJuXiaoHaoInfoUnit  {
        function decode(b):any;
    }
// 打折商品
    module DaZheShangPinInfoUnit  {
        function decode(b):any;
    }
// 装备数据
    module DuihuanInfoUnit  {
        function decode(b):any;
    }
// 独角兽接口中的排行数据
    module DuJiaoShouRankInfoUnit  {
        function decode(b):any;
    }
// Email数据
    module EmailInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// Email小镇战报数据
    module TownReportInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 坐骑翅膀数据
    module ZuoqiUnit  {
        function decode(b):any;
    }
// 跨服坐骑翅膀数据
    module PlayerCardZuoqiUnit  {
        function decode(b):any;
    }
// 命星信息
    module FateStarInfoUnit  {
        function decode(b):any;
    }
// 命星点信息
    module FateStarPointInfoUnit  {
        function decode(b):any;
    }
// 命星技能面板信息
    module FateStarSkillPanelInfoUnit  {
        function decode(b):any;
    }
// 命星技能
    module FateStarSkillUnit  {
        function decode(b):any;
    }
// 外显信息
    module FeatureInfoUnit  {
        function decode(b):any;
    }
// 封测活动－限时冲关数据
    module XianshiChongguanInfoUnit  {
        function decode(b):any;
    }
// 封测活动－天降元宝数据
    module TianjiangYuanbaoInfoUnit  {
        function decode(b):any;
    }
// 封测活动－天天砸金蛋数据
    module TiantianZaJindanInfoUnit  {
        function decode(b):any;
    }
// 活动－连续充值数据
    module LianxuChongzhiInfoUnit  {
        function decode(b):any;
    }
// 活动－每日签到数据
    module MeiriQiandaoInfoUnit  {
        function decode(b):any;
    }
// 数字积分类数据
    module NumberValueUnit  {
        function decode(b):any;
    }
// Entity类数据
    module EntityValueUnit  {
        function decode(b):any;
    }
// Entity类数据不在奖励类型中的
    module EntityValueNoRewardTypeUnit  {
        function decode(b):any;
    }
// 文本内容积分类数据
    module StringWordsUnit  {
        function decode(b):any;
    }
// 小关数据(普通副本)
    module XiaoguanDefUnit  {
        function decode(b):any;
    }
// 大关数据(道馆挑战)
    module DaguanDefUnit  {
        function decode(b):any;
    }
// 怪物数据
    module GuaiwuDefUnit  {
        function decode(b):any;
    }
// 磨练副本数据
    module MoLianFuBenDefUnit  {
        function decode(b):any;
    }
// 磨练副本数据
    module YinCangRenWuDefUnit  {
        function decode(b):any;
    }
// 副本翻倍
    module FuBenFanBeiDefUnit  {
        function decode(b):any;
    }
// 突发事件数据
    module TuFaShiJianDefUnit  {
        function decode(b):any;
    }
// 皮肤系统数据
    module PiFuXiTongDefUnit  {
        function decode(b):any;
    }
// 称号数据
    module ChengHaoDefUnit  {
        function decode(b):any;
    }
// 公会副本数据
    module GongHuiFuBenDefUnit  {
        function decode(b):any;
    }
// 战斗地铁
    module ZhanDouDiTieDefUnit  {
        function decode(b):any;
    }
// 小镇入侵者怪物数据
    module XiaozhenRuqinGuaiwuUnit  {
        function decode(b):any;
    }
// 小镇入侵者数据
    module XiaozhenRuqinDefUnit  {
        function decode(b):any;
    }
// 达到xx后全服可领取奖励情况
    module FirstOneAllServerRewardUnit  {
        function decode(b):any;
    }
// 跟随宠信息
    module FollowPetUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 装饰
    module PetRoomDecorateUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 房间寄养的宠物
    module PetRoomParkPetUnit  {
        function decode(b):any;
    }
// 随机的房间
    module FollowPetRdmRoomUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 使用的装饰
    module PetRoomDecorateUseUnit  {
        function decode(b):any;
    }
// 宠物子代蛋
    module FollowPetEggUnit  {
        function decode(b):any;
    }
// 宠物屋饰品
    module FollowPetRoomShiPinUnit  {
        function decode(b):any;
    }
// 宠物屋饰品
    module ShiPinPositionUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 跟随宠皮肤
    module FollowPetPiFuUnit  {
        function decode(b):any;
    }
// 新的跟随宠进阶显示
    module NewFollowPetShowUnit  {
        function decode(b):any;
    }
// 被禁止登录账户信息
    module ForbiddenAccountInfoUnit  {
        function decode(b):any;
    }
// 免费时装数据
    module FreeFashionUnit  {
        function decode(b):any;
    }
// 免费时装条件数据
    module FreeFashionConditionUnit  {
        function decode(b):any;
    }
// 好友信息
    module FriendsInfoUnit  {
        function decode(b):any;
    }
// 好友聊天内容信息
    module FriendsMsgContentInfoUnit  {
        function decode(b):any;
    }
// 好友聊天信息
    module FriendsMsgInfoUnit  {
        function decode(b):any;
    }
// 副本中心数据
    module FuBenLieBiaoUnit  {
        function decode(b):any;
    }
// 副本中心数据
    module FuBenInfoUnit  {
        function decode(b):any;
    }
// 礼包信息
    module GiftPackageInfoUnit  {
        function decode(b):any;
    }
// 公告信息
    module GongGaoInfoUnit  {
        function decode(b):any;
    }
// 公会活跃度奖励信息
    module GongHuiActivityPointRewardUnit  {
        function decode(b):any;
    }
// 用在公会战显示对象上
    module GongHuiBattleMemberInfoUnit  {
        function decode(b):any;
    }
// 用在公会战传递数据
    module GongHuiZhanPiPeiInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 用在公会战显示对象上 用在回放上
    module GongHuiZhanMemberHuiFangInfoUnit  {
        function decode(b):any;
    }
// 用在公会战荣耀榜传递等
    module GongHuiZhanRankInfoUnit  {
        function decode(b):any;
    }
// 客户端传输界面信息用
    module GongHuiZhanGHInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 客户端传输界面信息用
    module GongHuiZhanFightUnit  {
        function decode(b):any;
    }
// 公会战个人战斗回放 中 敌人显示信息
    module GongHuiZhanPVPHuiFangEnemyInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 个人回放传信息用
    module GongHuiZhanMemberFightUnit  {
        function decode(b):any;
    }
// 公会战刷新group用信息
    module GongHuiZhanGroupFreshInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 公会boss怪物info
    module GongHuiBossGuaiWuInfoUnit  {
        function decode(b):any;
    }
// 用在公会Boss日志中伤害排行
    module GongHuiBossLogUnit  {
        function decode(b):any;
    }
// 公会副本
    module GongHuiFuBenInfoUnit  {
        function decode(b):any;
    }
// 用在公会副本列表的数据
    module GongHuiFuBenGuaiWuInfoUnit  {
        function decode(b):any;
    }
// 用在公会仓库面板的数据
    module GongHuiCangKuInfoUnit  {
        function decode(b):any;
    }
// 用在公会分配日志  35条
    module GongHuiFenZangLogUnit  {
        function decode(b):any;
    }
// 用在公会副本日志 中  捐献排行和伤害排行
    module GongHuiFuBenLogUnit  {
        function decode(b):any;
    }
// 公会副本奖励
    module GongHuiFuBenRewardUnit  {
        function decode(b):any;
    }
// 公会副本小关信息
    module GongHuiFuBenXiaoGuanInfoUnit  {
        function decode(b):any;
    }
// 公会副本房间信息
    module GongHuiFuBenRoomInfoUnit  {
        function decode(b):any;
    }
// 公会副本房间信息
    module GongHuiFuBenRoomPersonUnit  {
        function decode(b):any;
    }
// 公会副本日志
    module GongHuiFuBenDamageUnit  {
        function decode(b):any;
    }
// 公会副本备战英雄
    module GongHuiFuBenRoomHeroUnit  {
        function decode(b):any;
    }
// 用在公会副本日志 中  掉落信息
    module GongHuiFuBenDiaoluoLogUnit  {
        function decode(b):any;
    }
// 公会福利
    module GongHuiFuLiInfoUnit  {
        function decode(b):any;
    }
// 公会福利Log
    module GongHuiFuLiLogInfoUnit  {
        function decode(b):any;
    }
// 公会每日捐赠的日志
    module GongHuiHuoYueLogInfoUnit  {
        function decode(b):any;
    }
// 用在公会排行的数据
    module GongHuiListInfoUnit  {
        function decode(b):any;
    }
// 公会成员信息
    module GongHuiMemberInfoUnit  {
        function decode(b):any;
    }
// 公会拍卖物品信息
    module GongHuiPaiMaiInfoUnit  {
        function decode(b):any;
    }
// 公会商店信息
    module GongHuiShangDianInfoUnit  {
        function decode(b):any;
    }
// 用在跨服积分赛公会统计的数据
    module GongHuiTongJiInfoUnit  {
        function decode(b):any;
    }
// 公会战排行数据
    module GongHuiZhanPlayerInfoUnit  {
        function decode(b):any;
    }
// 公会数据
    module GongHuiDataUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 公会战公会数据
    module GongHuiZhanGongHuiInfoUnit  {
        function decode(b):any;
    }
// 公会战鼓舞信息
    module GongHuiZhanGuWuInfoUnit  {
        function decode(b):any;
    }
// 公会战排行奖励信息
    module GongHuiZhanRankRewardInfoUnit  {
        function decode(b):any;
    }
// 攻坚战道馆
    module GongJianZhanDaoGuanInfoUnit  {
        function decode(b):any;
    }
// 攻坚战道馆人物信息
    module GongJianZhanRenWuInfoUnit  {
        function decode(b):any;
    }
// 首届馆主信息
    module GuanZhuInfoUnit  {
        function decode(b):any;
    }
// 指派列表信息
    module ZhiPaiInfoUnit  {
        function decode(b):any;
    }
// 挂机敌人信息   战斗记录也用这个
    module GuaJiEnergyInfoUnit  {
        function decode(b):any;
    }
// 挂机敌人信息  记录信息 用在 战斗记录列表和 仇人列表
    module GuaJiEnergyRecordAndChouRenInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 复仇挂机敌人搜索单项信息
    module RevengeEnemySearchProjectInfoUnit  {
        function decode(b):any;
    }
// 复仇挂机敌人搜索单项的Project状态
    module RevengeEnemySearchProjectStatusUnit  {
        function decode(b):any;
    }
// 复仇挂机敌人搜索单项的操作内容
    module StateRevengeEnemyProjectSearchOperationUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 挂机每小时奖励信息
    module GuaJiHourRewardUnit  {
        function decode(b):any;
    }
// 挂机展示宝箱信息
    module GuaJiShowChestElementUnit  {
        function decode(b):any;
    }
// 骨骼显示信息
    module GugeUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 剧情对话
    module JuqingDuihuaUnit  {
        function decode(b):any;
    }
// 剧情信息
    module StoryLineUnit  {
        function decode(b):any;
    }
// 老乞丐对话
    module LaoqigaiDuihuaUnit  {
        function decode(b):any;
    }
// 关卡数据
    module GuanqiaInfoUnit  {
        function decode(b):any;
    }
// 引导礼包显示数据
    module GuildGiftPackInfoUnit  {
        function decode(b):any;
    }
// 英雄阵容信息
    module HeroFormInfoUnit  {
        function decode(b):any;
    }
// 英雄Log数据
    module HeroLogInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 英雄数据
    module HeroInfoUnit  {
        function decode(b):any;
    }
// 英雄属性数据
    module HeroPropertyInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 发给gm系统的英雄数据
    module HeroGMInfoUnit  {
        function decode(b):any;
    }
// 精灵洗练属性
    module HeroXiLianPropInfoUnit  {
        function decode(b):any;
    }
// 扭蛋图鉴宠物信息
    module ShilianXianshiPetInfoUnit  {
        function decode(b):any;
    }
// 扭蛋图鉴世代信息
    module ShilianXianshiShidaiInfoUnit  {
        function decode(b):any;
    }
// DbId List
    module DbIdListInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 疯狂十连抽位置信息
    module CrazyGachaTenPullsPositionInfoUnit  {
        function decode(b):any;
    }
// 疯狂十连抽面板信息
    module CrazyGachaTenPullsPanelInfoUnit  {
        function decode(b):any;
    }
// 心愿神魔获得状态
    module WishHeroUnit  {
        function decode(b):any;
    }
// 可选心愿神魔信息
    module OptionalWishHeroInfoUnit  {
        function decode(b):any;
    }
// 抢红包记录
    module HongBaoRecordInfoUnit  {
        function decode(b):any;
    }
// 红包数据
    module HongBaoInfoUnit  {
        function decode(b):any;
    }
// 回收商店商品数据
    module HuiShouShopGoodsInfoUnit  {
        function decode(b):any;
    }
// 通关大奖
    module InstanceCompleteRewardUnit  {
        function decode(b):any;
    }
// 奖励数据
    module JiangliInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 嘉年华奖励信息
    module JiaNianHuaRewardInfoUnit  {
        function decode(b):any;
    }
// 嘉年华商店元素
    module JiaNianHuaShopInfoUnit  {
        function decode(b):any;
    }
// 节日基金奖励列表信息
    module JieRiJiJinRewardInfoUnit  {
        function decode(b):any;
    }
// 基金info
    module JiJinInfoUnit  {
        function decode(b):any;
    }
// 精灵球数据
    module JingLingQiuInfoUnit  {
        function decode(b):any;
    }
// 跟随宠寄养日志
    module JiYangRecordUnit  {
        function decode(b):any;
    }
// 凯旋丰碑第一人信息
    module KaixuanFengbeiFirstPlayerUnit  {
        function decode(b):any;
    }
// 凯旋丰碑榜单汇总信息
    module KaixuanFengbeiRankInfoUnit  {
        function decode(b):any;
    }
// 凯旋丰碑达到条件的人信息
    module KaixuanFengbeiPlayerUnit  {
        function decode(b):any;
    }
// 凯旋丰碑进度奖励信息
    module KaixuanFengbeiRankRewardUnit  {
        function decode(b):any;
    }
// 道馆位置信息
    module PositionInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 道馆历史记录信息
    module DaoGuanHistoryInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 单个道馆信息
    module SingleDaoGuanInfoUnit  {
        function decode(b):any;
    }
// 积分信息
    module DaoGuanScoreInfoUnit  {
        function decode(b):any;
    }
// 道馆详细信息
    module DaoGuanDetailInfoUnit  {
        function decode(b):any;
    }
// 跨服锦标赛积分箱子信息
    module KuaFuJBSScoreBoxInfoUnit  {
        function decode(b):any;
    }
// 跨服锦标赛积分箱子信息
    module KuaFuJBSFightInfoUnit  {
        function decode(b):any;
    }
// 跨服砸金蛋排行数据
    module KuaFuZaJinDanRankingInfoUnit  {
        function decode(b):any;
    }
// 战队简单信息(用于显示晋级赛 标签信息)
    module ZhanDuiSimpleInfoUnit  {
        function decode(b):any;
    }
// 战队PK组
    module ZhanDuiPKGroupUnit  {
        function decode(b):any;
    }
// 战队战绩信息
    module ZhanDuiPlayerFightLogInfoUnit  {
        function decode(b):any;
    }
// 历史战绩日志
    module HistroyLogUnit  {
        function decode(b):any;
    }
// 
    module ZhanDuiPKGroupZhiChiUnit  {
        function decode(b):any;
    }
// 蓝钻礼包数据
    module LanZuanLiBaoInfoUnit  {
        function decode(b):any;
    }
// 黄金投资数据
    module HuangJinTouZiInfoUnit  {
        function decode(b):any;
    }
// 老玩家回归礼包奖励信息
    module LaoWanJiaHuiGuiInfoUnit  {
        function decode(b):any;
    }
// 已经解锁的小镇信息
    module YiJieSuoTownInfoUnit  {
        function decode(b):any;
    }
// 恋爱系统送礼物信息
    module LianAiLiWuInfoUnit  {
        function decode(b):any;
    }
// 恋爱系统面板中  技能的描述
    module LianAiJiNengDescInfoUnit  {
        function decode(b):any;
    }
// 连续充值活动信息
    module LianXuChongZhiHuoDongInfoUnit  {
        function decode(b):any;
    }
// 奖励数据
    module LianxuJiangliInfoUnit  {
        function decode(b):any;
    }
// 零元礼包
    module LingYuanLiBaoInfoUnit  {
        function decode(b):any;
    }
// 统计服务器log收集
    module LogInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 洛托姆进阶资源
    module LuoTuoMuDisplayInfoUnit  {
        function decode(b):any;
    }
// 洛托姆
    module LuoTuoMuInfoUnit  {
        function decode(b):any;
    }
// 新神技背包物品信息
    module MagicalSkillItemUnit  {
        function decode(b):any;
    }
// 新神技卡信息
    module MagicalSkillCardUnit  {
        function decode(b):any;
    }
// 新神技卡协作数据
    module MagicalSkillCooperateUnit  {
        function decode(b):any;
    }
// 新神技卡策划数据
    module MagicalSkillDefUnit  {
        function decode(b):any;
    }
// 玩家支付信息
    module PlayerPayInfoUnit  {
        function decode(b):any;
    }
// 礼品码奖励信息
    module GiftCodeRewardInfoUnit  {
        function decode(b):any;
    }
// 礼品码奖励信息
    module GiftCodeUsedInfoUnit  {
        function decode(b):any;
    }
// 消息线程池信息
    module MessagePoolInfoUnit  {
        function decode(b):any;
    }
// 消息处理效率信息
    module MessageProcessInfoUnit  {
        function decode(b):any;
    }
// 排位赛级位信息
    module GMRankingLevelGroupUnit  {
        function decode(b):any;
    }
// 段位信息
    module GMRankingGroupUnit  {
        function decode(b):any;
    }
// 排位赛玩家信息
    module GMRankingPlayerUnit  {
        function decode(b):any;
    }
// 排位赛玩家精灵信息
    module GMRankingHeroUnit  {
        function decode(b):any;
    }
// 跨服活动分组信息
    module CrossBigRuleGroupUnit  {
        function decode(b):any;
    }
// 每日团购
    module MeiRiTuanGouInfoUnit  {
        function decode(b):any;
    }
// 迷宫扫荡结果数据
    module SweepMiGongOutcomeUnit  {
        function decode(b):any;
    }
// 名片信息
    module MingPianInfoUnit  {
        function decode(b):any;
    }
// 玩家任务数据
    module MissionInfoUnit  {
        function decode(b):any;
    }
// 邀请任务
    module YaoQingMissionInfoUnit  {
        function decode(b):any;
    }
// 极限挑战任务
    module JiXianTiaoZhanRenWuUnit  {
        function decode(b):any;
    }
// 日常任务数据
    module DailyMissionInfoUnit  {
        function decode(b):any;
    }
// 
    module MoFaShaiZiInfoUnit  {
        function decode(b):any;
    }
// 磨炼排行数据
    module MoLianPaihangInfoUnit  {
        function decode(b):any;
    }
// 宝箱的领取条件
    module BaoXiangTiaoJianInfoUnit  {
        function decode(b):any;
    }
// 玩家新活动数据
    module ActivityProgressUnit  {
        function decode(b):any;
    }
// 跨服锦标赛公会排行的数据
    module JinBiaoSaiGongHuiInfoUnit  {
        function decode(b):any;
    }
// 跨服锦标赛个人排行的数据
    module JinBiaoSaiGeRenInfoUnit  {
        function decode(b):any;
    }
// 玩家新活动数据
    module NewActivityInfoUnit  {
        function decode(b):any;
    }
// 活动入口扩展信息-金猪存钱罐
    module ActivityExtendInfoPiggyBankUnit  {
        function decode(b):any;
    }
// 活动入口扩展信息-基金
    module ActivityExtendInfoFundUnit  {
        function decode(b):any;
    }
// 玩家新充值排行数据
    module NewChongZhiPaiHangInfoUnit  {
        function decode(b):any;
    }
// 玩家新限时排行数据
    module NewValueListInfoUnit  {
        function decode(b):any;
    }
// 单笔充值数据
    module DanBiChongZhiDetailInfoUnit  {
        function decode(b):any;
    }
// 五元充值数据
    module WuYuanChongZhiDetailInfoUnit  {
        function decode(b):any;
    }
// 活动任务数据
    module MissionActivityInfoUnit  {
        function decode(b):any;
    }
// 活动任务数据
    module ItemCollectActivityInfoUnit  {
        function decode(b):any;
    }
// 一冲选领
    module YiChongXuanLingDetailInfoUnit  {
        function decode(b):any;
    }
// 活动副本怪物
    module HuoDongFuBenGuaiWuInfoUnit  {
        function decode(b):any;
    }
// 活动副本界面奖励展示
    module HuoDongFuBenJiangLiPanelInfoUnit  {
        function decode(b):any;
    }
// 七日竞赛活动信息
    module SevenMatchInfoUnit  {
        function decode(b):any;
    }
// 七日竞赛活动奖励内容
    module ServerMatchRewardInfoUnit  {
        function decode(b):any;
    }
// 七日竞赛活动奖励内容
    module ServerMatchPlayerInfoUnit  {
        function decode(b):any;
    }
// 七日竞赛活动冲榜礼包
    module SevenMatchGiftInfoUnit  {
        function decode(b):any;
    }
// 开服活动
    module KaiFuHuoDongInfoUnit  {
        function decode(b):any;
    }
// 每日一购
    module MeiRiYiGouInfoUnit  {
        function decode(b):any;
    }
// 单笔购买数据
    module DanBiGouMaiDetailInfoUnit  {
        function decode(b):any;
    }
// 显示升阶数据
    module XianShiShengJieDetailInfoUnit  {
        function decode(b):any;
    }
// 合服首充数据
    module HeFuShouChongInfoUnit  {
        function decode(b):any;
    }
// 个人飙战信息
    module GeRenBiaoZhanInfoUnit  {
        function decode(b):any;
    }
// 单笔购买数据
    module HeFuYuShouGoodInfoUnit  {
        function decode(b):any;
    }
// 等级vip礼包
    module DengJiVipLiBaoInfoUnit  {
        function decode(b):any;
    }
// 怪物来袭排名信息
    module GuaiWuLaiXiRankInfoUnit  {
        function decode(b):any;
    }
// 活动－七日任务－今日好礼
    module SDTLoginInfoUnit  {
        function decode(b):any;
    }
// 活动－七日任务－任务数据
    module SDTQuestInfoUnit  {
        function decode(b):any;
    }
// 活动－七日任务－今日特惠
    module SDTBuyInfoUnit  {
        function decode(b):any;
    }
// 活动－七日任务－每日奖励
    module SDTDailyInfoUnit  {
        function decode(b):any;
    }
// 超值返利数据
    module ChaoZhiFanLiInfoUnit  {
        function decode(b):any;
    }
// 神宠基金
    module ShenChongJiJinInfoUnit  {
        function decode(b):any;
    }
// 日充累充
    module RiChongLeiChongInfoUnit  {
        function decode(b):any;
    }
// 首冲团购
    module ShouChongTuanGouUnit  {
        function decode(b):any;
    }
// 天天返利数据
    module TianTianFanLiInfoUnit  {
        function decode(b):any;
    }
// 跨服秒杀
    module KuaFuMiaoShaInfoUnit  {
        function decode(b):any;
    }
// 开服限购大R礼包
    module KaifuXiangouInfoUnit  {
        function decode(b):any;
    }
// 扭蛋商品信息
    module NiuDanShopInfoUnit  {
        function decode(b):any;
    }
// 限时召唤选择
    module XianShiZhaoHuanChooseUnit  {
        function decode(b):any;
    }
// 皇权诏令奖励内容
    module HuangquanZhaolingInfoUnit  {
        function decode(b):any;
    }
// 登录现金红包内容
    module DengluHongbaoInfoUnit  {
        function decode(b):any;
    }
// 任务类现金红包内容
    module RenwuHongbaoInfoUnit  {
        function decode(b):any;
    }
// 限时等级礼包
    module XianShiDengJiLiBaoUnit  {
        function decode(b):any;
    }
// 玩家新活动Tips数据
    module NewActivityTipsInfoUnit  {
        function decode(b):any;
    }
// 自定义礼包内容
    module DiyGiftInfoUnit  {
        function decode(b):any;
    }
// 自定义礼包商品选项信息
    module DiyGiftRewardItemUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 自定义礼包位置信息
    module DiyGiftPositionInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 折扣商店活动商品数据
    module ZhekouShangdianGoodsUnit  {
        function decode(b):any;
    }
// 七日冲榜竞赛活动战斗力差值提示内容
    module SevenMatchDiffFightTipsUnit  {
        function decode(b):any;
    }
// 各类基金活动行内容
    module FundActivityDetailInfoUnit  {
        function decode(b):any;
    }
// 活动－七日任务－七日商店数据
    module SevenDayShopGoodsInfoUnit  {
        function decode(b):any;
    }
// 金券消排行奖励内容
    module XiaohaoPaihangRewardInfoUnit  {
        function decode(b):any;
    }
// 金券消排行玩家排行信息
    module XiaohaoPaihangPlayerInfoUnit  {
        function decode(b):any;
    }
// 累计登陆活动23补签
    module LeiJiDengLuAddLoginDayUnit  {
        function decode(b):any;
    }
// 活跃任务活动的任务详细数据
    module HuoyueRenwuMissionInfoUnit  {
        function decode(b):any;
    }
// 活动详情激活开启条件
    module ActivityDetailActiveInfoUnit  {
        function decode(b):any;
    }
// 幸运转盘活动格子信息
    module LuckyWheelGridUnit  {
        function decode(b):any;
    }
// 幸运转盘活动摇奖条件信息
    module LuckyWheelRewardConditionUnit  {
        function decode(b):any;
    }
// 装备 宝石等等几个副本的奖励
    module NewFuBenJiangLiInfoUnit  {
        function decode(b):any;
    }
// 玩家排行数据
    module PaihangInfoUnit  {
        function decode(b):any;
    }
// 比武场排行分享相关数据
    module PaihangShareInfoUnit  {
        function decode(b):any;
    }
// 拍卖物品详情
    module PaiMaiWuPinInfoUnit  {
        function decode(b):any;
    }
// 拍卖物品详情
    module PaiMaiLabelInfoUnit  {
        function decode(b):any;
    }
// 拍卖记录
    module PaiMaiLogInfoUnit  {
        function decode(b):any;
    }
// 拍卖背包详情
    module PaiMaiBeiBaoInfoUnit  {
        function decode(b):any;
    }
// 拍卖物品详情
    module NotifyItemInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 累计充值送特权卡的信息
    module CumulativeRechargeCardInfoUnit  {
        function decode(b):any;
    }
// 宠物信息
    module PetInfoUnit  {
        function decode(b):any;
    }
// 宠物手册信息
    module PetHandbookInfoUnit  {
        function decode(b):any;
    }
// 宠物羁绊信息
    module PetCombinationUnit  {
        function decode(b):any;
    }
// 面板上新守护兽信息
    module NewGuardPetInfoUnit  {
        function decode(b):any;
    }
// 新守护兽上阵信息
    module NewGuardPetPositionUnit  {
        function decode(b):any;
    }
// 新守护兽进阶属性和消耗信息
    module NewGuardPetJinjieInfoUnit  {
        function decode(b):any;
    }
// 新守护兽丹药信息
    module NewGuardPetDanyaoInfoUnit  {
        function decode(b):any;
    }
// 新守护兽红点信息
    module NewGuardPetRedPointInfoUnit  {
        function decode(b):any;
    }
// 新守护兽丹药简略信息
    module NewGuardPetDanyaoMiniInfoUnit  {
        function decode(b):any;
    }
// 新守护兽升星额外属性描述
    module NewGuardPetExtraDescInfoUnit  {
        function decode(b):any;
    }
// 新守护兽升星消耗信息
    module NewGuardPetLevelUpCostInfoUnit  {
        function decode(b):any;
    }
// 战力提升
    module StrategyFightUnit  {
        function decode(b):any;
    }
// 推荐阵容
    module StrategyRecommendUnit  {
        function decode(b):any;
    }
// 本服第一阵容
    module StrategyFirstUnit  {
        function decode(b):any;
    }
// 守护兽付费技能数据
    module NewGuardPetPaySkillUnit  {
        function decode(b):any;
    }
// 皮肤图鉴系统unit
    module PifuTujianInfoUnit  {
        function decode(b):any;
    }
// 皮肤图鉴单皮肤详细信息unit
    module PifuTujianOnePifuInfoUnit  {
        function decode(b):any;
    }
// 骨骼部件信息
    module GugePartUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 抽奖结果信息
    module PifuTujianLuckyDrawInfoUnit  {
        function decode(b):any;
    }
// 皮肤unit
    module PiFuInfoUnit  {
        function decode(b):any;
    }
// 皮肤套装属性unit
    module PiFuTaozhuangInfoUnit  {
        function decode(b):any;
    }
// 皮卡丘状态信息
    module PikachuStateUnit  {
        function decode(b):any;
    }
// 玩家排行数据
    module PlayerBasicInfoUnit  {
        function decode(b):any;
    }
// 子系统属性数据
    module PlayerCardSubSystemPropertyUnit  {
        function decode(b):any;
    }
// 装备数据
    module PlayerCardZhuangbeiInfoUnit  {
        function decode(b):any;
    }
// 精灵宠物数据
    module PlayerCardHeroInfoUnit  {
        function decode(b):any;
    }
// 玩家名片上阵宠物详情数据
    module PlayerCardAllHeroesUnit  {
        function decode(b):any;
    }
// 玩家名片上阵宠物专属神器信息
    module PlayerCardZhuanShuShenQiUnit  {
        function decode(b):any;
    }
// 玩家名片上阵宠物种族值
    module PlayerCardZhongZuZhiUnit  {
        function decode(b):any;
    }
// 玩家名片基础信息
    module PlayerCardCommonInfoUnit  {
        function decode(b):any;
    }
// 玩家名片幻化
    module PlayerCardHuanHuaUnit  {
        function decode(b):any;
    }
// 玩家名片幻化皮肤
    module PlayerCardHuanHuaPiFuUnit  {
        function decode(b):any;
    }
// 玩家名片幻化皮肤
    module PlayerCardHuanHuaPiFuDetailUnit  {
        function decode(b):any;
    }
// 玩家名片皮神
    module PlayerCardLuoTuoMuUnit  {
        function decode(b):any;
    }
// 玩家名片训练师
    module PlayerCardZhuJuePeiYangUnit  {
        function decode(b):any;
    }
// 玩家名片转生
    module PlayerCardZhuanShengUnit  {
        function decode(b):any;
    }
// 玩家名片跟随宠
    module PlayerCardFollowPetUnit  {
        function decode(b):any;
    }
// 玩家名片守护兽
    module PlayerCardGuardPetUnit  {
        function decode(b):any;
    }
// 玩家名片守护兽
    module PlayerCardNewGuardPetPositionUnit  {
        function decode(b):any;
    }
// 玩家名片技能书
    module PlayerCardSkillBookUnit  {
        function decode(b):any;
    }
// 玩家创角待选职业信息
    module PlayerProfessionUnit  {
        function decode(b):any;
    }
// 玩家主角形象部分
    module ActorPhotoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 玩家个人形象信息
    module PlayerPhotoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 渠道通用领奖信息
    module ChannelJiangLiStatusUnit  {
        function decode(b):any;
    }
// Project状态
    module ProjectStatusUnit  {
        function decode(b):any;
    }
// 铁拳玩家基本信息
    module GamePvpPlayerInfoUnit  {
        function decode(b):any;
    }
// int的是阶段  字符串表示这一阶段的特殊属性
    module QiJiTeShuShuXingInfoUnit  {
        function decode(b):any;
    }
// 奇遇礼包unit
    module QiYuLiBaoInfoUnit  {
        function decode(b):any;
    }
// 奇遇面板信息
    module QiYuPanelInfoUnit  {
        function decode(b):any;
    }
// 精灵战斗信息
    module HeroFightInfoUnit  {
        function decode(b):any;
    }
// 玩家红点信息Byte型
    module ByteRedPointUnit  {
        function decode(b):any;
    }
// 玩家红点信息Bool型
    module BoolRedPointUnit  {
        function decode(b):any;
    }
// 忍村宝箱
    module RenCunBaoXiangUnit  {
        function decode(b):any;
    }
// 保卫忍村怪物
    module RenCunBossUnit  {
        function decode(b):any;
    }
// 忍村建筑技能
    module RenCunJianZhuSkillUnit  {
        function decode(b):any;
    }
// 忍村建筑
    module RenCunJianZhuUnit  {
        function decode(b):any;
    }
// 忍蛙数据
    module RenWaDataUnit  {
        function decode(b):any;
    }
// 忍蛙type数据
    module RenWaTypeDataUnit  {
        function decode(b):any;
    }
// Rogoulike地下城数据
    module RogoulikeInfoUnit  {
        function decode(b):any;
    }
// Rogoulike关卡中骨骼信息数据
    module CommonDefGugeInfoUnit  {
        function decode(b):any;
    }
// Rogoulike关卡中的怪物数据
    module CommonGuaiwuInfoUnit  {
        function decode(b):any;
    }
// Rogoulike中的diy字符串数据
    module DiyStringUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// Rogoulike中的额外点击元素信息
    module RogoulikeExtraElementInfoUnit  {
        function decode(b):any;
    }
// Rogoulike中的小怪或宝箱奖励信息
    module RogoulikeXiaoguaiBaoxiangInfoUnit  {
        function decode(b):any;
    }
// Rogoulike章节信息
    module RogoulikeChapterInfoUnit  {
        function decode(b):any;
    }
// Rogoulike每层的秘境
    module RogoulikeCengMysteryUnit  {
        function decode(b):any;
    }
// Rogoulike秘境信息
    module RogoulikeMysteryUnit  {
        function decode(b):any;
    }
// Rogoulike神魔神技扩展数据
    module RogoulikeHeroMagicalSkillExtendUnit  {
        function decode(b):any;
    }
// Rogoulike神技结算信息
    module RogoulikeMagicalSkillSettlementUnit  {
        function decode(b):any;
    }
// Rogoulike该层的地牢
    module RogoulikeCengDungeonUnit  {
        function decode(b):any;
    }
// Rogoulike地牢层级数据
    module DungeonLayerUnit  {
        function decode(b):any;
    }
// Rogoulike地牢怪物信息
    module DungeonLayerMonsterUnit  {
        function decode(b):any;
    }
// 主角某子系统皮肤数据
    module RoleSubSystemPifuUnit  {
        function decode(b):any;
    }
// 主角某子系统皮肤数据
    module RoleSubSystemPifuTypeGroupUnit  {
        function decode(b):any;
    }
// 跨服某子系统皮肤数据
    module PlayerCardSubSystemPifuUnit  {
        function decode(b):any;
    }
// 坐标信息
    module PointUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 角色形象及坐标路线
    module PlayerLocationInfoUnit  {
        function decode(b):any;
    }
// 怪物形象及坐标路线
    module MonsterLocationInfoUnit  {
        function decode(b):any;
    }
// 地图层级信息
    module SceneLevelInfoUnit  {
        function decode(b):any;
    }
// 个人PVP排行数据
    module SoloPVPPlayerInfoUnit  {
        function decode(b):any;
    }
// 个人PVP战斗记录
    module SoloPVPRecordInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 战场信息
    module GongHuiZhanSceneInfoUnit  {
        function decode(b):any;
    }
// 箱子信息
    module BoxInfoUnit  {
        function decode(b):any;
    }
// 陷阱信息
    module TrapInfoUnit  {
        function decode(b):any;
    }
// 快速反应事件选项信息
    module QuickReactionOptionUnit  {
        function decode(b):any;
    }
// 积分商店商品数据
    module ScoreShopGoodsInfoUnit  {
        function decode(b):any;
    }
// 神宠数据
    module ShenChongInfoUnit  {
        function decode(b):any;
    }
// 神器属性
    module ShenQiShuXingInfoUnit  {
        function decode(b):any;
    }
// 神器数据
    module ShenQiInfoUnit  {
        function decode(b):any;
    }
// 神器升级所需材料
    module ShenQiXiaoHaoInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 出海地区数据
    module ChuhaiAreaInfoUnit  {
        function decode(b):any;
    }
// 船只数据
    module ShipInfoUnit  {
        function decode(b):any;
    }
// 掉落道具信息
    module LootInfoUnit  {
        function decode(b):any;
    }
// 船只属性
    module ShipPropUnit  {
        function decode(b):any;
    }
// 航海日志
    module ShipLogInfoUnit  {
        function decode(b):any;
    }
// 商品数据
    module ShopInfoUnit  {
        function decode(b):any;
    }
// 黑市商品数据
    module HSGoodsInfoUnit  {
        function decode(b):any;
    }
// 商城商品数据
    module ShangChengGoodsUnit  {
        function decode(b):any;
    }
// 商城商品数据
    module NewShopGoodsUnit  {
        function decode(b):any;
    }
// 商城状态
    module ShangChengStatusUnit  {
        function decode(b):any;
    }
// 商城页签
    module ShangChengTabUnit  {
        function decode(b):any;
    }
// 商城策划数据
    module ShopDefUnit  {
        function decode(b):any;
    }
// 商品数据
    module DungeonShopGoodsUnit  {
        function decode(b):any;
    }
// 奖励数据
    module ShouchongJiangliInfoUnit  {
        function decode(b):any;
    }
// 双彩球奖励
    module ShuangCaiQiuRewardInfoUnit  {
        function decode(b):any;
    }
// type  value 这样的格式
    module ShuXingInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 属性
    module NewShuXingInfoUnit  {
        function decode(b):any;
    }
// 属性详情
    module NewShuXingInfoDetailUnit  {
        function decode(b):any;
    }
// 技能书数据
    module SkillBookUnit  {
        function decode(b):any;
    }
// 技能书协作数据
    module SkillBookXiezuoUnit  {
        function decode(b):any;
    }
// 技能书协作文本数据
    module SkillBookXiezuoBeizhuUnit  {
        function decode(b):any;
    }
// 服务器状态数据
    module ServerInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 精灵属性数据
    module HeroPropInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 遭遇战玩家技能书数据
    module PlayerSkillBookInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 遭遇战玩家数据
    module PlayerInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 排位赛战斗记录数据
    module RankingRecordInfoUnit  {
        function decode(b):any;
    }
// 战区玩家数据
    module WarZonePlayerUnit  {
        function decode(b):any;
    }
// 战区玩家精灵预览数据
    module WarZonePlayerHeroViewUnit  {
        function decode(b):any;
    }
// 排位赛玩家结算数据，结算用
    module RankingPlayerInfoUnit  {
        function decode(b):any;
    }
// 机器人宠物数据
    module RobotHeroLevelInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 机器人宠物数据
    module RobotHeroPropInfoUnit  {
        function decode(b):any;
    }
// 玩家id和排位赛名词
    module PlayerIdAndPaiWeiSaiMingCiInfoUnit  {
        function decode(b):any;
    }
// 排位赛排行数据
    module RankingPaiHangPlayerUnit  {
        function decode(b):any;
    }
// 排位赛排行数据
    module UnitedServerInfoUnit  {
        function decode(b):any;
    }
// 单场比赛结果
    module ZhanDuiPlayerFightResultUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 每一轮战队的支持总金额
    module ZhanDuiZhiChiLogUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 玩家支持
    module PlayerZhiChiLogUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 简化精灵
    module SimHeroInfoUnit  {
        function decode(b):any;
    }
// 战队赛阵型
    module ZDSZhenXingInfoUnit  {
        function decode(b):any;
    }
// 跨服砸金蛋排行数据
    module StateZaJinDanRankingInfoUnit  {
        function decode(b):any;
    }
// 神技个数
    module MagicalSkillHeroTypeInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 探索副本数据
    module TanSuoFuBenUnit  {
        function decode(b):any;
    }
// 探索副本奖励数据
    module TanSuoFuBenRewardUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 探索副本房间数据
    module TanSuoFuBenRoomInfoUnit  {
        function decode(b):any;
    }
// 探险精灵信息
    module TanXianHeroInfoUnit  {
        function decode(b):any;
    }
// 奖励数据
    module TanXianLiangLiShowInfoUnit  {
        function decode(b):any;
    }
// 探险中那些奖励属性
    module TanXianShuXingInfoUnit  {
        function decode(b):any;
    }
// 编队信息
    module TeamInfoUnit  {
        function decode(b):any;
    }
// 上阵精灵宠物
    module TeamHeroInfoUnit  {
        function decode(b):any;
    }
// 上阵技能书信息
    module TeamSkillBookInfoUnit  {
        function decode(b):any;
    }
// 装备数据
    module TempBeibaoInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 天空塔
    module TianKongTaUnit  {
        function decode(b):any;
    }
// 天空塔星星奖励
    module TianKongTaStarRewardUnit  {
        function decode(b):any;
    }
// 天空塔星星排行
    module TianKongTaPaiHangUnit  {
        function decode(b):any;
    }
// 跳动提示武将
    module TiaoDongHeroInfoUnit  {
        function decode(b):any;
    }
// 跨服头像框数据
    module PlayerCardTouxiangkuangUnit  {
        function decode(b):any;
    }
// 当前头衔信息
    module TouxianInfoUnit  {
        function decode(b):any;
    }
// 头衔任务信息
    module TouxianMissionInfoUnit  {
        function decode(b):any;
    }
// 跨服头衔数据
    module PlayerCardTouxianUnit  {
        function decode(b):any;
    }
// 头衔光环数据
    module TouxianGuanghuanInfoUnit  {
        function decode(b):any;
    }
// 跨服头衔光环数据
    module PlayerCardTouxianGuanghuanUnit  {
        function decode(b):any;
    }
// 小镇已解锁房屋信息
    module TownRoomInfoOpenUnit  {
        function decode(b):any;
    }
// 小镇未解锁房屋信息
    module TownRoomInfoUnOpenUnit  {
        function decode(b):any;
    }
// 抢来的人物信息
    module TownQiangDuoPersonUnit  {
        function decode(b):any;
    }
// 被抢信息
    module TownBeiQiangInfoUnit  {
        function decode(b):any;
    }
// 小镇产出信息
    module TownProductUnit  {
        function decode(b):any;
    }
// 小镇产出信息
    module TownProductShowUnit  {
        function decode(b):any;
    }
// 小镇兵种信息
    module TownSoldierShowUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 小镇科技
    module TownKeJiUnit  {
        function decode(b):any;
    }
// 小镇士兵
    module TownSoldierUnit  {
        function decode(b):any;
    }
// 小镇搜到的玩家部分信息
    module TownSearchPartInfoUnit  {
        function decode(b):any;
    }
// 小镇搜到的上阵宠物信息
    module TownSearchHeroInfoUnit  {
        function decode(b):any;
    }
// 小镇人物提供属性
    module TownPersonShuXingUnit  {
        function decode(b):any;
    }
// 小镇推送
    module TownPushUnit  {
        function decode(b):any;
    }
// 小镇护盾
    module TownHuDunUnit  {
        function decode(b):any;
    }
// 攻击者信息
    module TownAttackerInfoUnit  {
        function decode(b):any;
    }
// 小镇道馆信息
    module TownDaoGuanUnit  {
        function decode(b):any;
    }
// 小镇道馆驻守人物信息
    module TownDaoGuanPlayerUnit  {
        function decode(b):any;
    }
// 小镇攻击在途
    module TownDaoGuanAttackingUnit  {
        function decode(b):any;
    }
// 小镇攻击记录
    module TownDaoGuanAttackLogUnit  {
        function decode(b):any;
    }
// 小镇道馆详情
    module TownDaoGuanDetailUnit  {
        function decode(b):any;
    }
// 公会排名
    module TownDaoGuanGongHuiForPaiMingUnit  {
        function decode(b):any;
    }
// 小镇道馆赛季排名奖励
    module TownDaoGuanSeasonJiangLiUnit  {
        function decode(b):any;
    }
// 小镇道馆首次占领奖励
    module TownDaoGuanFirstJiangLiUnit  {
        function decode(b):any;
    }
// 小镇道馆贡献
    module TownDaoGuanGongXianUnit  {
        function decode(b):any;
    }
// 小镇道馆人物行军
    module TownDaoGuanPlayerXingJunUnit  {
        function decode(b):any;
    }
// 小镇医院伤员信息
    module TownWoundedUnit  {
        function decode(b):any;
    }
// 小镇集市兑换信息
    module TownShopConvertUnit  {
        function decode(b):any;
    }
// 小镇道馆玩家数据
    module TownDaoGuanPlayerInfoUnit  {
        function decode(b):any;
    }
// 小镇道馆战斗历史记录信息
    module TownDaoGuanFightRecordUnit  {
        function decode(b):any;
    }
// 小镇道馆战斗记录明细内容战报信息
    module TownDaoGuanFightRecordDetailUnit  {
        function decode(b):any;
    }
// 小镇兵种骨骼信息
    module TownSoldierGugeShowUnit  {
        function decode(b):any;
    }
// 小镇未解锁房屋简短的信息
    module TownUnOpenRoomShortInfoUnit  {
        function decode(b):any;
    }
// 小镇人物的付费技能
    module TownPersonPaySkillUnit  {
        function decode(b):any;
    }
// 金猪关卡阵容精灵宠物信息
    module TuiTuFightHeroUnit  {
        function decode(b):any;
    }
// 比武记录数据
    module VipInfoUnit  {
        function decode(b):any;
    }
// vip等级数量统计数据
    module VipLvCountInfoUnit  {
        function decode(b):any;
    }
// 新的vip数据
    module NewVipInfoUnit  {
        function decode(b):any;
    }
// 新的vip礼包数据
    module VipGiftInfoUnit  {
        function decode(b):any;
    }
// 直升VIP带来的VIP任务奖励数据
    module VipMissionRewardInfoUnit  {
        function decode(b):any;
    }
// VIP1免费领取钻石数据
    module VipFreeDiamondInfoUnit  {
        function decode(b):any;
    }
// 玩吧vip礼包
    module WanBaVipRewardInfoUnit  {
        function decode(b):any;
    }
// 周末登录奖励数据
    module WeekendLoginJiangliInfoUnit  {
        function decode(b):any;
    }
// 周末在线奖励数据
    module WeekendZaixianJiangliInfoUnit  {
        function decode(b):any;
    }
// 威名等级属性品质unit
    module WeimingPropAdnRankInfoUnit  {
        function decode(b):any;
    }
// 威名等级宝石unit
    module WeimingLevelGemstoneInfoUnit  {
        function decode(b):any;
    }
// 威名技能宝石信息unit
    module WeimingSkillGemstoneInfoUnit  {
        function decode(b):any;
    }
// 威名当前技能unit
    module WeimingSkillInfoUnit  {
        function decode(b):any;
    }
// 威名显示unit
    module WeimingShowInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 威名充值unit
    module WeimingRechargeInfoUnit  {
        function decode(b):any;
    }
// 威名名片unit
    module PlayerCardWeimingUnit  {
        function decode(b):any;
    }
// BOSS伤害排名信息
    module DamageRankInfoUnit  {
        function decode(b):any;
    }
// 我要变强数据
    module WoYaoBianQiangUnit  {
        function decode(b):any;
    }
// 我要变强单宠数据
    module WoYaoBianQiangDanChongUnit  {
        function decode(b):any;
    }
// 限时冲级info
    module XianShiChongJiInfoUnit  {
        function decode(b):any;
    }
// 洗练属性数据
    module XiLianPropUnit  {
        function decode(b):any;
    }
// 
    module XunBaoLeYuanInfoUnit  {
        function decode(b):any;
    }
// 遗迹探险任务信息
    module YiJiTanXianTaskInfoUnit  {
        function decode(b):any;
    }
// 印记之源数据数据
    module YinJiInfoUnit  {
        function decode(b):any;
    }
// 某个模板下的一元特价状态数据
    module YiyuantejiaMobanStatusInfoUnit  {
        function decode(b):any;
    }
// 月卡奖励
    module YueKaJiangliInfoUnit  {
        function decode(b):any;
    }
// 奖励数据
    module ZaixianJiangliInfoUnit  {
        function decode(b):any;
    }
// 宅急送玩家info
    module ZJSPlayerInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 宅急送属性info
    module ZJSDataInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 宅急送历史info
    module ZJSHistroyInfoUnit  {
        function decode(b):any;
    }
// 战败分析数据
    module ZhanBaiFenXiInfoUnit  {
        function decode(b):any;
    }
// 战队信息
    module ZhanDuiInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 战队里的玩家信息
    module ZhanDuiMemberInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 争霸赛商城商品信息
    module ZhanDuiShopInfoUnit  {
        function decode(b):any;
    }
// 战棋棋子
    module ZhanQiQiZiInfoUnit  {
        function decode(b):any;
    }
// 战棋棋盘
    module ZhanQiPanelInfoUnit  {
        function decode(b):any;
    }
// 战棋玩家
    module ZhanQiPlayerInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 战棋排行榜玩家
    module ZhanQiPaiHangBangPlayerInfoUnit  {
        function decode(b):any;
    }
// 战阵碎片信息
    module ZhanzhenSuipianInfoUnit  {
        function decode(b):any;
    }
// 战阵信息
    module ZhanzhenInfoUnit  {
        function decode(b):any;
    }
// 所有的战阵信息
    module ZhanzhenAllInfoUnit  {
        function decode(b):any;
    }
// 指令分析数据
    module ZhilingFenxiInfoUnit  {
        function decode(b):any;
    }
// 职业神器
    module ZhiYeShenQiUnit  {
        function decode(b):any;
    }
// 一条信息
    module ZhongZuZhiProInfoUnit  {
        function decode(b):any;
    }
// 种族值突破信息
    module ZhongZuZhiTuPoInfoUnit  {
        function decode(b):any;
    }
// 种族值突破详情
    module ZhongZuZhiTuPoDetailsUnit  {
        function decode(b):any;
    }
// 野外抓宠中和天气有关的神兽 用在地图列表和生成怪物接口里
    module TianQiGuaiWuUnit  {
        function decode(b):any;
    }
// 抓宠中心房间
    module ZhuaChongRoomUnit  {
        function decode(b):any;
    }
// 抓宠信息
    module ZhuaChongPriceInfoUnit  {
        function decode(b):any;
    }
// 抓宠助手精灵
    module ZhuaChongZhuShouHeroUnit  {
        function decode(b):any;
    }
// 装备数据
    module ZhuangbeiInfoUnit  {
        function decode(b):any;
    }
// 外显装备数据
    module ExpandZhuangbeiInfoUnit  {
        function decode(b):any;
    }
// 星级加成显示
    module ZhuangbeiStarShowUnit  {
        function decode(b):any;
    }
// 装备套装激活个人和对应的属性
    module ZhuangbeiSuitActiveCountAndPropInfoUnit  {
        function decode(b):any;
    }
// 装备套装属性
    module ZhuangbeiSuitInfoUnit  {
        function decode(b):any;
    }
// 装备图鉴属性
    module ZhuangbeiTujianInfoUnit  {
        function decode(b):any;
    }
// 转生技能
    module ZhuanShengSkillUnit  {
        function decode(b):any;
    }
// 转生天赋
    module ZhuanShengTalentUnit  {
        function decode(b):any;
    }
// 专属神器的属性  用在详情面板中
    module ZhuanShuShenQiShuXingInfoUnit  {
        function decode(b):any;
    }
// 专属神器的属性  用在背包中
    module ZhuanShuShenQiBagInfoUnit  {
        function decode(b):any;
    }
// 专属神器的描述
    module ZhuanShuShenQiDescInfoUnit  {
        function decode(b):any;
    }
// 专属神器图鉴的图标
    module ZhuanShuShenQiTuJianIconInfoUnit  {
        function decode(b):any;
    }
// 专属神器推送信息
    module ZhuanShuShenQiPushfoUnit  {
        function decode(b):any;
    }
// 专属神器的技能升级属性
    module ZhuanShuShenQiShuSkillInfoUnit  {
        function decode(b):any;
    }
// 主角装备位置
    module ZhuJueEquipPositionUnit  {
        function decode(b):any;
    }
// 主角装备
    module ZhuJueEquipUnit  {
        function decode(b):any;
    }
// 主角装备详细
    module ZhuJueEquipDetailUnit  {
        function decode(b):any;
    }
// 主角装备强化
    module ZhuJueEquipQiangHuaUnit  {
        function decode(b):any;
    }
// 主角装备属性大单元
    module ZhuJueEquipPropUnit  {
        function decode(b):any;
    }
// 主角装备属性小单元
    module EquipPropUnit  {
        function decode(b):any;
    }
// 符文信息
    module FuWenInfoUnit  {
        function decode(b):any;
    }
// 符文信息
    module FuWenShopInfoUnit  {
        function decode(b):any;
    }
// 资源月卡
    module ZiYuanYueKaUnit  {
        function decode(b):any;
    }
// 跨服Z力量数据
    module PlayerCardZliliangUnit  {
        function decode(b):any;
    }
// 组队副本面板信息
    module ZuDuiFuBenPanelInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 组队副本成员信息
    module ZuDuiFuBenMemberInfoUnit  {
        function decode(b):any;
    }
// 组队副本顺序信息   用在改变顺序的消息里
    module ZuDuiFuBenIndexInfoUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }
// 组队副本队伍信息 用在打的动画里
    module TeamDataUnit  {
        function decode(b):any;
    }
// 关卡数据
    module GuanqiaDataUnit  {
        function decode(b):any;
    }
// 组队副本战斗记录
    module ZuDuiFuBenFightLogUnit  {
        function encode(o, b):any;

        function decode(b):any;
    }

// [请求]小镇面板
    module TownPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownPanelReq;
        function encode(o, b):any;
    }

// [返回]小镇面板
    module TownPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇房屋信息
    module TownRoomPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownRoomPanelReq;
        function encode(o, b):any;
    }

// [返回]小镇房屋信息
    module TownRoomPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇管家信息
    module TownPersonPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownPersonPanelReq;
        function encode(o, b):any;
    }

// [返回]小镇管家信息
    module TownPersonPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇研究或招募信息
    module TownYanJiuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownYanJiuPanelReq;
        function encode(o, b):any;
    }

// [返回]小镇研究或招募或伤员信息
    module TownYanJiuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇升级
    module GetTownLvUPReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownLvUPReq;
        function encode(o, b):any;
    }

// [返回]小镇升级
    module GetTownLvUPRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇招募
    module GetTownZhaoMuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownZhaoMuReq;
        function encode(o, b):any;
    }

// [返回]小镇招募
    module GetTownZhaoMuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]释放临时工
    module ReleaseTownPersonReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReleaseTownPersonReq;
        function encode(o, b):any;
    }

// [返回]释放临时工
    module ReleaseTownPersonRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取士兵和临时工信息
    module GetSoldierAndPersonShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetSoldierAndPersonShowReq;
        function encode(o, b):any;
    }

// [返回]获取士兵和临时工信息
    module GetSoldierAndPersonShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开启小镇
    module OpenTownRoomReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenTownRoomReq;
        function encode(o, b):any;
    }

// [返回]开启小镇
    module OpenTownRoomRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]搜索小镇
    module SearchTownReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SearchTownReq;
        function encode(o, b):any;
    }

// [返回]搜索小镇
    module SearchTownRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取该玩家的小镇完整信息
    module GetFullTownInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFullTownInfoReq;
        function encode(o, b):any;
    }

// [返回]获取该玩家的小镇完整信息
    module GetFullTownInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]小镇产出推送
    module TownPushRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买护盾
    module BuyTownHuDunReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyTownHuDunReq;
        function encode(o, b):any;
    }

// [返回]购买护盾
    module BuyTownHuDunRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]护盾面板
    module TownHuDunPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownHuDunPanelReq;
        function encode(o, b):any;
    }

// [返回]护盾面板
    module TownHuDunPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]出征信息
    module ChuZhengInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChuZhengInfoReq;
        function encode(o, b):any;
    }

// [返回]出征信息
    module ChuZhengInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开始战斗
    module StartTownFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.StartTownFightReq;
        function encode(o, b):any;
    }

// [返回]开始战斗
    module StartTownFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]观看战斗结果
    module WatchTownFightLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WatchTownFightLogReq;
        function encode(o, b):any;
    }

// [返回]观看战斗结果
    module WatchTownFightLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]观看宠物战斗结果
    module WatchPetFightLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WatchPetFightLogReq;
        function encode(o, b):any;
    }

// [返回]观看宠物战斗结果
    module WatchPetFightLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]小镇被攻击
    module TownBeAttackedRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]查看袭击者列表
    module ShowAttackerListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowAttackerListReq;
        function encode(o, b):any;
    }

// [返回]查看袭击者列表
    module ShowAttackerListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]出征喊人
    module TownHanhuaOnChuzhengReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownHanhuaOnChuzhengReq;
        function encode(o, b):any;
    }

// [请求]出征喊人
    module TownHanhuaOnChuzhengRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]答应援助后的条件检查
    module TownCheckOnHanhuaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownCheckOnHanhuaReq;
        function encode(o, b):any;
    }

// [返回]答应援助后的条件检查
    module TownCheckOnHanhuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]小镇管家被抢走或夺回时，推送新的数据给前端
    module TownRoomUpdateRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆主面板
    module TownDaoGuanMainPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownDaoGuanMainPanelReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆主面板
    module TownDaoGuanMainPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆面板详情攻击方
    module TownDaoGuanDetailPanelForAttackReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownDaoGuanDetailPanelForAttackReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆面板详情攻击方
    module TownDaoGuanDetailPanelForAttackRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取攻防信息
    module GetTownAttackInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownAttackInfoReq;
        function encode(o, b):any;
    }

// [返回]获取攻防信息
    module GetTownAttackInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 小镇道馆出征
    module GetTownDaoGuanAttackReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanAttackReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆出征
    module GetTownDaoGuanAttackRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 小镇道馆驻守
    module GetTownDaoGuanDefenseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanDefenseReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆驻守
    module GetTownDaoGuanDefenseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 请求出征或驻守
    module GetTownDaoGuanAttackOrDefenseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanAttackOrDefenseReq;
        function encode(o, b):any;
    }

// [返回]请求出征或驻守
    module GetTownDaoGuanAttackOrDefenseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆面板详情防守方
    module TownDaoGuanDetailPanelForDefenseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownDaoGuanDetailPanelForDefenseReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆面板详情防守方
    module TownDaoGuanDetailPanelForDefenseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]撤回驻守方
    module CallBackTownGuanDefenserReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CallBackTownGuanDefenserReq;
        function encode(o, b):any;
    }

// [返回]撤回驻守方
    module CallBackTownGuanDefenserRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]放弃道馆
    module GiveUpTownDaoGuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GiveUpTownDaoGuanReq;
        function encode(o, b):any;
    }

// [返回]放弃道馆
    module GiveUpTownDaoGuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆赛季排名
    module TownDaoGuanSeasonPaiMingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownDaoGuanSeasonPaiMingReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆赛季排名
    module TownDaoGuanSeasonPaiMingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆赛季排名奖励
    module GetTownDaoGuanSeasonPaiMingRewardListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanSeasonPaiMingRewardListReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆赛季排名奖励
    module GetTownDaoGuanSeasonPaiMingRewardListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆首次占领奖励
    module GetTownDaoGuanFirstRewardListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanFirstRewardListReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆首次占领奖励
    module GetTownDaoGuanFirstRewardListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]小镇道馆信息发生变化，攻击、护盾、占领状态等
    module TownDaoGuanDataChangeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]成员贡献
    module GetTownDaoGuanGongXianPaiHangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanGongXianPaiHangReq;
        function encode(o, b):any;
    }

// [返回]成员贡献
    module GetTownDaoGuanGongXianPaiHangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]小镇道馆攻击喊人
    module TownDaoGuanAttackHanRenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownDaoGuanAttackHanRenReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆攻击喊人
    module TownDaoGuanAttackHanRenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]小镇道馆驻守喊人
    module TownDaoGuanDefenseHanRenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownDaoGuanDefenseHanRenReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆驻守喊人
    module TownDaoGuanDefenseHanRenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]小镇道馆响应聊天验证
    module TownDaoGuanClickCheckReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TownDaoGuanClickCheckReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆响应聊天验证
    module TownDaoGuanClickCheckRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇集市兑换
    module GetTownShopConvertReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownShopConvertReq;
        function encode(o, b):any;
    }

// [返回]小镇集市兑换
    module GetTownShopConvertRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆战斗历史记录信息
    module GetTownDaoGuanFightRecordsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanFightRecordsReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆战斗历史记录信息
    module GetTownDaoGuanFightRecordsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆战斗历史记录明细
    module GetTownDaoGuanFightRecordDetailReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanFightRecordDetailReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆战斗历史记录明细
    module GetTownDaoGuanFightRecordDetailRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]小镇道馆入侵出征界面
    module GetTownDaoGuanRuqinChuzhengReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTownDaoGuanRuqinChuzhengReq;
        function encode(o, b):any;
    }

// [返回]小镇道馆入侵出征界面
    module GetTownDaoGuanRuqinChuzhengRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]收集小镇房屋奖励到背包
    module CollectTownRoomRewardsToItemPackageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CollectTownRoomRewardsToItemPackageReq;
        function encode(o, b):any;
    }

// [返回]收集小镇房屋奖励到背包
    module CollectTownRoomRewardsToItemPackageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]小镇房屋付费技能更新
    module PushTownRoomPaySkillRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】面板
    module GetPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPanelReq;
        function encode(o, b):any;
    }

// 【反馈】面板
    module GetPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】兑奖
    module DuiJiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DuiJiangReq;
        function encode(o, b):any;
    }

// 【反馈】兑奖
    module DuiJiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】职业神器面板
    module ZhiYeShenQiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhiYeShenQiPanelReq;
        function encode(o, b):any;
    }

// 【反馈】职业神器面板
    module ZhiYeShenQiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】操作神器
    module ChangeZhiYeShenQiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeZhiYeShenQiReq;
        function encode(o, b):any;
    }

// 【反馈】操作神器
    module ChangeZhiYeShenQiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】神器升级面板
    module ZhiYeShenQiShengJiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhiYeShenQiShengJiPanelReq;
        function encode(o, b):any;
    }

// 【反馈】神器升级面板
    module ZhiYeShenQiShengJiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】职业神器升级
    module GetZhiYeShenQiLvUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhiYeShenQiLvUpReq;
        function encode(o, b):any;
    }

// 【反馈】职业神器升级
    module GetZhiYeShenQiLvUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】神器回收预览
    module ShenQiHuiShouViewReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenQiHuiShouViewReq;
        function encode(o, b):any;
    }

// 【反馈】神器回收预览
    module ShenQiHuiShouViewRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】神器回收
    module GetShenQiHuiShouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShenQiHuiShouReq;
        function encode(o, b):any;
    }

// 【反馈】神器回收
    module GetShenQiHuiShouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新神技背包列表
    module GetAllMagicalSkillItemListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAllMagicalSkillItemListReq;
        function encode(o, b):any;
    }

// 【反馈】新神技背包列表
    module GetAllMagicalSkillItemListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】神技卡穿上卸下操作
    module MagicalSkillPutOnAndUnloadReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MagicalSkillPutOnAndUnloadReq;
        function encode(o, b):any;
    }

// 【反馈】神技卡穿上卸下操作
    module MagicalSkillPutOnAndUnloadRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】神技属性查看
    module MagicalSkillCardViewReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MagicalSkillCardViewReq;
        function encode(o, b):any;
    }

// 【反馈】神技属性查看
    module MagicalSkillCardViewRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】已穿戴神技卡升阶升级操作
    module MagicalSkillLevelUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MagicalSkillLevelUpReq;
        function encode(o, b):any;
    }

// 【反馈】已穿戴神技卡升阶升级操作
    module MagicalSkillLevelUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】神技卡一键穿上
    module MagicalSkillOneKeyPutOnReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MagicalSkillOneKeyPutOnReq;
        function encode(o, b):any;
    }

// 【反馈】神技卡一键穿上
    module MagicalSkillOneKeyPutOnRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]转生面板
    module ZhuanShengPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShengPanelReq;
        function encode(o, b):any;
    }

// [返回]转生面板
    module ZhuanShengPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]转生
    module GetZhuanShengReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuanShengReq;
        function encode(o, b):any;
    }

// [返回]转生
    module GetZhuanShengRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]转生技能面板
    module ZhuanShengSkillPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShengSkillPanelReq;
        function encode(o, b):any;
    }

// [返回]转生技能面板
    module ZhuanShengSkillPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]转生技能升级
    module GetZhuanShengSkillReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuanShengSkillReq;
        function encode(o, b):any;
    }

// [返回]转生技能升级
    module GetZhuanShengSkillRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]转生天赋面板
    module ZhuanShengTalentPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShengTalentPanelReq;
        function encode(o, b):any;
    }

// [返回]转生天赋面板
    module ZhuanShengTalentPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]激活转生天赋
    module GetZhuanShengTalentReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuanShengTalentReq;
        function encode(o, b):any;
    }

// [返回]激活转生天赋
    module GetZhuanShengTalentRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]GetZhuanShengRedPoint
    module GetZhuanShengRedPointReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuanShengRedPointReq;
        function encode(o, b):any;
    }

// [返回]GetZhuanShengRedPoint
    module GetZhuanShengRedPointRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]不思议礼包面板
    module BuSiYiLiBaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuSiYiLiBaoPanelReq;
        function encode(o, b):any;
    }

// [返回]不思议礼包面板
    module BuSiYiLiBaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]不思议礼包出现在挂机页面
    module BuSiYiLiBaoShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 2.进入传送门界面挂机状态
    module EnterChuanSongMenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EnterChuanSongMenReq;
        function encode(o, b):any;
    }

// [反馈]   2.进入传送门界面挂机状态
    module EnterChuanSongMenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]   3.传送门界面 点击切换空间 开始搜索敌人
    module SearchEnemyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SearchEnemyReq;
        function encode(o, b):any;
    }

// [反馈]   3.传送门界面 点击切换空间 开始搜索敌人
    module SearchEnemyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]   4.传送门界面 观看战斗
    module GuaJiFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaJiFightReq;
        function encode(o, b):any;
    }

// [反馈]   4.传送门界面 观看战斗
    module GuaJiFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]   5.不看战斗受益请求
    module StartGuaJiWithoutFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.StartGuaJiWithoutFightReq;
        function encode(o, b):any;
    }

// [反馈]   5.不看战斗受益请求
    module StartGuaJiWithoutFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]展示宝箱  6.展示宝箱 
    module ShowChestReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowChestReq;
        function encode(o, b):any;
    }

// [反馈]展示宝箱  6.展示宝箱 
    module ShowChestRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取宝箱 7.领取宝箱
    module GetChestReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChestReq;
        function encode(o, b):any;
    }

// [反馈]领取宝箱 7.领取宝箱
    module GetChestRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]快速战斗信息
    module KuaiSuInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KuaiSuInfoReq;
        function encode(o, b):any;
    }

// [反馈]快速战斗信息
    module KuaiSuInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买快速战斗
    module BuyKuaiSuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyKuaiSuReq;
        function encode(o, b):any;
    }

// [反馈]购买快速战斗
    module BuyKuaiSuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 被抢以后
    module DoBeiQiangActionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoBeiQiangActionReq;
        function encode(o, b):any;
    }

// 被抢以后
    module DoBeiQiangActionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 被援助成功
    module PushBeiYuanZhuSuccessRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 援助抢夺
    module YuanZhuQiangDuoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YuanZhuQiangDuoReq;
        function encode(o, b):any;
    }

// 援助抢夺
    module YuanZhuQiangDuoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 请求新的剧情线（进入挂机地图1分钟后请求）
    module MakeNewStoryLineReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MakeNewStoryLineReq;
        function encode(o, b):any;
    }

// 推送新的剧情线
    module MakeNewStoryLineRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 清理所有剧情的推送
    module ClearAllStoryLineRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 更新剧情线状态
    module UpdateStoryLineStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateStoryLineStatusReq;
        function encode(o, b):any;
    }

// 更新剧情线状态
    module UpdateStoryLineStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 推送新的老乞丐对话
    module MakeNewLaoqigaiDuihuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 老乞丐消耗提交
    module LaoqigaiCostReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LaoqigaiCostReq;
        function encode(o, b):any;
    }

// 老乞丐消耗提交
    module LaoqigaiCostRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取老乞丐的翻倍奖励
    module GetLaoqigaiRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLaoqigaiRewardReq;
        function encode(o, b):any;
    }

// 领取老乞丐的翻倍奖励
    module GetLaoqigaiRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 挂机页面的功能开启和结束状态列表
    module GetProjectStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetProjectStatusReq;
        function encode(o, b):any;
    }

// 挂机页面的功能开启和结束状态列表
    module GetProjectStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 查看对方的荣誉值
    module GetPlayerHonourScoreReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerHonourScoreReq;
        function encode(o, b):any;
    }

// 查看对方的荣誉值
    module GetPlayerHonourScoreRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 确认订阅消息
    module ConfirmToSubscribeMessageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ConfirmToSubscribeMessageReq;
        function encode(o, b):any;
    }

// [请求]挂机抓宠的list
    module GuaJiZhuaChongListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaJiZhuaChongListReq;
        function encode(o, b):any;
    }

// [反馈]挂机抓宠的list
    module GuaJiZhuaChongListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抓
    module GuaJiZhuaChongZhuaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaJiZhuaChongZhuaReq;
        function encode(o, b):any;
    }

// [返回]抓
    module GuaJiZhuaChongZhuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]提示
    module GuaJiZhuaChongZhuaTipsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaJiZhuaChongZhuaTipsReq;
        function encode(o, b):any;
    }

// [返回]提示
    module GuaJiZhuaChongZhuaTipsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]战败分析点哪个充值项目导致的弹框
    module ZhanBaiFenXiChongZhiKuangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanBaiFenXiChongZhiKuangReq;
        function encode(o, b):any;
    }

// [请求]上传任务打点数据
    module UpdataTaskDotReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdataTaskDotReq;
        function encode(o, b):any;
    }

// [返回]上传任务打点数据
    module UpdataTaskDotRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】凯旋丰碑
    module KaixuanFengbeiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KaixuanFengbeiPanelReq;
        function encode(o, b):any;
    }

// 【反馈】凯旋丰碑
    module KaixuanFengbeiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】丰碑榜单列表
    module GetFengbeiRankReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFengbeiRankReq;
        function encode(o, b):any;
    }

// 【反馈】丰碑榜单列表
    module GetFengbeiRankRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】丰碑榜单列表
    module GetFengbeiRewardListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFengbeiRewardListReq;
        function encode(o, b):any;
    }

// 【反馈】丰碑榜单列表
    module GetFengbeiRewardListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取丰碑榜单奖励
    module ClaimFengbeiRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ClaimFengbeiRewardReq;
        function encode(o, b):any;
    }

// 【反馈】领取丰碑榜单奖励
    module ClaimFengbeiRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】丰碑奖励前5名玩家
    module GetFengbeiFirstFiveReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFengbeiFirstFiveReq;
        function encode(o, b):any;
    }

// 【反馈】丰碑奖励前5名玩家
    module GetFengbeiFirstFiveRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】进入场景
    module PlayerEnterSceneReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerEnterSceneReq;
        function encode(o, b):any;
    }

// 【反馈】进入场景
    module PlayerEnterSceneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】离开场景
    module PlayerLeaveSceneReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerLeaveSceneReq;
        function encode(o, b):any;
    }

// 【反馈】离开场景
    module PlayerLeaveSceneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家移动
    module PlayerMoveReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerMoveReq;
        function encode(o, b):any;
    }

// 【反馈】玩家移动广播
    module PlayerMoveRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播新玩家进入场景了
    module BroadcastPlayerEnterSceneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播玩家离开场景
    module BroadcastPlayerLeaveSceneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播玩家移动路径
    module BroadcastPlayerMoveRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播玩家当前坐标纠正
    module BroadcastPlayerLocationFixRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家点击其他玩家指令
    module PlayerClickReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerClickReq;
        function encode(o, b):any;
    }

// 【反馈】点击其他玩家
    module PlayerClickRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家场景中聊天
    module PlayerChatInSceneReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerChatInSceneReq;
        function encode(o, b):any;
    }

// 【反馈】玩家场景中聊天
    module BroadcastPlayerChatInSceneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩家场景中战斗
    module BroadcastPlayerFightInSceneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩家复活
    module PlayerRecallHPReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerRecallHPReq;
        function encode(o, b):any;
    }

// 【反馈】玩家复活
    module PlayerRecallHPRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩家场景中战斗内容
    module PushPlayerFightContentInSceneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】ping值
    module CheckClientPingTimeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CheckClientPingTimeReq;
        function encode(o, b):any;
    }

// 【反馈】ping值
    module CheckClientPingTimeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】结束战斗状态
    module FinishFightStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FinishFightStatusReq;
        function encode(o, b):any;
    }

// 【反馈】结束战斗状态
    module FinishFightStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】结束战斗状态
    module BroadcastPlayerFightFinishRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】改变移动速度
    module ChangeMoveSpeedReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeMoveSpeedReq;
        function encode(o, b):any;
    }

// 【反馈】改变移动速度
    module ChangeMoveSpeedRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【广播】玩家新的移动速度
    module BroadcastPlayerNewSpeedRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【推送】玩家追踪目标丢失通报
    module PushPlayerClickTargetLostRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载个人PVP活动数据
    module LoadSoloPVPDataReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadSoloPVPDataReq;
        function encode(o, b):any;
    }

// 【反馈】加载或推送个人PVP活动数据
    module LoadSoloPVPDataRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载个人PVP地图相关信息
    module LoadSoloPVPSceneDataReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadSoloPVPSceneDataReq;
        function encode(o, b):any;
    }

// 【反馈】加载个人PVP地图相关信息
    module LoadSoloPVPSceneDataRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载个人PVP战斗记录
    module LoadSoloPVPRecordListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadSoloPVPRecordListReq;
        function encode(o, b):any;
    }

// 【反馈】加载个人PVP战斗记录
    module LoadSoloPVPRecordListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载个人PVP排行数据
    module LoadSoloPVPListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadSoloPVPListReq;
        function encode(o, b):any;
    }

// 【反馈】加载个人PVP排行数据
    module LoadSoloPVPListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载个人PVP奖池信息
    module LoadSoloPVPRewardInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadSoloPVPRewardInfoReq;
        function encode(o, b):any;
    }

// 【反馈】加载个人PVP奖池信息
    module LoadSoloPVPRewardInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】铁拳追踪玩家
    module SearchPvpPlayerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SearchPvpPlayerReq;
        function encode(o, b):any;
    }

// 【反馈】铁拳追踪玩家
    module SearchPvpPlayerRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【推送】进入战场开始每35秒显示1次，快速反应事件
    module PushQuickReactionOptionListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】选择某一项目快速反应事件
    module SelectQuickReactionOptionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SelectQuickReactionOptionReq;
        function encode(o, b):any;
    }

// 【反馈】选择某一项目快速反应事件
    module SelectQuickReactionOptionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]零元礼包面板
    module LingYuanLiBaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LingYuanLiBaoPanelReq;
        function encode(o, b):any;
    }

// [返回]零元礼包面板
    module LingYuanLiBaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]零元礼包买
    module LingYuanLiBaoBuyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LingYuanLiBaoBuyReq;
        function encode(o, b):any;
    }

// [返回]零元礼包面板
    module LingYuanLiBaoBuyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]零元礼包返还
    module LingYuanLiBaoFanHuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LingYuanLiBaoFanHuanReq;
        function encode(o, b):any;
    }

// [返回]零元礼包返还
    module LingYuanLiBaoFanHuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]好友列表
    module FriendsListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FriendsListReq;
        function encode(o, b):any;
    }

// [返回]好友列表
    module FriendsListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 切磋
    module QieCuoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QieCuoReq;
        function encode(o, b):any;
    }

// [返回]切磋
    module QieCuoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 赠送
    module ZengSongReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZengSongReq;
        function encode(o, b):any;
    }

// 赠送
    module ZengSongRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 删除好友
    module DeleteFriendsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DeleteFriendsReq;
        function encode(o, b):any;
    }

// 删除好友
    module DeleteFriendsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 观看战斗记录接口 
    module WatchFightLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WatchFightLogReq;
        function encode(o, b):any;
    }

// [返回]观看战斗记录接口
    module WatchFightLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家要申请好友 
    module ApplyToFriendsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ApplyToFriendsReq;
        function encode(o, b):any;
    }

// [返回]玩家要申请好友 
    module ApplyToFriendsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 查找好友 然后返回可以申请他的那个列表 
    module FindFriendsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FindFriendsReq;
        function encode(o, b):any;
    }

// [返回]查找好友 然后返回可以申请他的那个列表
    module FindFriendsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取被申请列表
    module BeAppliedListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BeAppliedListReq;
        function encode(o, b):any;
    }

// [返回]申请好友
    module BeAppliedListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 同意对方申请
    module ApplyFriendsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ApplyFriendsReq;
        function encode(o, b):any;
    }

// [返回]申请好友
    module ApplyFriendsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 对话的玩家列表
    module TalkingFriendsListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TalkingFriendsListReq;
        function encode(o, b):any;
    }

// 对话的玩家列表
    module TalkingFriendsListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 单个玩家对话请求
    module TalkToFriendReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TalkToFriendReq;
        function encode(o, b):any;
    }

// 单个玩家对话请求
    module TalkToFriendRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 单个玩家对话请求
    module DeleteFriendMessageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DeleteFriendMessageReq;
        function encode(o, b):any;
    }

// 单个玩家对话请求
    module DeleteFriendMessageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 单个玩家对话请求
    module SendFriendMessageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SendFriendMessageReq;
        function encode(o, b):any;
    }

// 单个玩家对话请求
    module SendFriendMessageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 清理正在对话的玩家id
    module ClearIsTalkingFriendReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ClearIsTalkingFriendReq;
        function encode(o, b):any;
    }

// 清理正在对话的玩家id
    module ClearIsTalkingFriendRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]好友日志列表
    module FriendsLogListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FriendsLogListReq;
        function encode(o, b):any;
    }

// [请求]好友日志列表
    module FriendsLogListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]索要金券
    module AskJinQuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AskJinQuanReq;
        function encode(o, b):any;
    }

// 索要金券
    module AskJinQuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]赠送金券
    module GiveJinQuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GiveJinQuanReq;
        function encode(o, b):any;
    }

// 赠送金券
    module GiveJinQuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】指令分析
    module ZhilingFenxiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhilingFenxiReq;
        function encode(o, b):any;
    }

// 【反馈】指令分析
    module ZhilingFenxiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取游戏次数
    module GetZhanQiCountReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhanQiCountReq;
        function encode(o, b):any;
    }

// 获取游戏次数
    module GetZhanQiCountRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 匹配玩家
    module ZhanQiPiPeiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanQiPiPeiReq;
        function encode(o, b):any;
    }

// 匹配玩家
    module ZhanQiPiPeiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 移动棋子
    module MoveQiZiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoveQiZiReq;
        function encode(o, b):any;
    }

// 匹配玩家
    module MoveQiZiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 结算胜负
    module JieSuanShengFuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JieSuanShengFuReq;
        function encode(o, b):any;
    }

// 结算胜负
    module JieSuanShengFuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 断线重连
    module ZhanQiChongLianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanQiChongLianReq;
        function encode(o, b):any;
    }

// 断线重连
    module ZhanQiChongLianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 奖励分配
    module JiangLiFenPeiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JiangLiFenPeiReq;
        function encode(o, b):any;
    }

// 奖励分配
    module JiangLiFenPeiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本排行榜
    module GetZhanQiPaiHangBangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhanQiPaiHangBangReq;
        function encode(o, b):any;
    }

// 组队副本排行榜
    module GetZhanQiPaiHangBangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 结算胜负
    module DanXianChengJieSuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DanXianChengJieSuanReq;
        function encode(o, b):any;
    }

// 结算胜负
    module DanXianChengJieSuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】比武对手列表
    module GetBiwuListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBiwuListReq;
        function encode(o, b):any;
    }

// 【反馈】比武对手列表
    module GetBiwuListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】比武战斗
    module BiwuFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BiwuFightReq;
        function encode(o, b):any;
    }

// 【反馈】比武战斗
    module BiwuFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】比武记录列表
    module GetBiwuRecordListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBiwuRecordListReq;
        function encode(o, b):any;
    }

// 【反馈】比武对记录表
    module GetBiwuRecordListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买比武次数
    module BuyBiwuCountReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyBiwuCountReq;
        function encode(o, b):any;
    }

// 【反馈】购买比武次数
    module BuyBiwuCountRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】荣誉兑换
    module GetMaxPaihangRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMaxPaihangRewardReq;
        function encode(o, b):any;
    }

// 【反馈】荣誉兑换
    module GetMaxPaihangRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】刷新比武CD信息
    module RefreshBiwuCDReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RefreshBiwuCDReq;
        function encode(o, b):any;
    }

// 【反馈】刷新比武CD信息
    module RefreshBiwuCDRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】查看指定玩家阵容
    module HeroInfoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroInfoListReq;
        function encode(o, b):any;
    }

// 【反馈】查看指定玩家阵容
    module HeroInfoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】前二十名的名称和排名
    module GetBiWuPaiHangInfoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBiWuPaiHangInfoListReq;
        function encode(o, b):any;
    }

// 【反馈】前二十名的名称和排名
    module GetBiWuPaiHangInfoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】登录时弹出的竞技场前3名列表
    module GetPopUpBiwuListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPopUpBiwuListReq;
        function encode(o, b):any;
    }

// 【反馈】登录时弹出的竞技场前3名列表
    module GetPopUpBiwuListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家支付请求
    module PlayerPayReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerPayReq;
        function encode(o, b):any;
    }

// 玩家支付请求
    module PlayerPayRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家渠道和UId
    module GetPlayerInfoByPlayerIDReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerInfoByPlayerIDReq;
        function encode(o, b):any;
    }

// 【反馈】玩家渠道和UId
    module GetPlayerInfoByPlayerIDRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家详细信息
    module GetPlayerDetailInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerDetailInfoReq;
        function encode(o, b):any;
    }

// 【反馈】玩家详细信息
    module GetPlayerDetailInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】申请手机号验证码
    module SendPhoneVerificationCodeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SendPhoneVerificationCodeReq;
        function encode(o, b):any;
    }

// 【返回】申请手机号验证码
    module SendPhoneVerificationCodeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】绑定手机号
    module LinkMobilePhoneReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LinkMobilePhoneReq;
        function encode(o, b):any;
    }

// 【返回】绑定手机号
    module LinkMobilePhoneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本列表
    module GongHuiFuBenListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenListReq;
        function encode(o, b):any;
    }

// [返回]公会副本列表
    module GongHuiFuBenListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开启公会副本
    module KaiQiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KaiQiReq;
        function encode(o, b):any;
    }

// [返回]开启公会副本
    module KaiQiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]捐献钻石
    module JuanXianXuanShiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JuanXianXuanShiReq;
        function encode(o, b):any;
    }

// [返回]捐献钻石
    module JuanXianXuanShiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本章节怪物
    module ZhangJieGuaiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhangJieGuaiWuReq;
        function encode(o, b):any;
    }

// [返回]公会副本章节怪物
    module ZhangJieGuaiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本章节怪物详情
    module GuaiWudetailsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaiWudetailsReq;
        function encode(o, b):any;
    }

// [返回]公会副本章节怪物详情
    module GuaiWudetailsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本  打
    module GongHuiFuBenFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenFightReq;
        function encode(o, b):any;
    }

// [返回]公会副本  打
    module GongHuiFuBenFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会仓库
    module GongHuiCangKuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiCangKuReq;
        function encode(o, b):any;
    }

// [返回]公会仓库
    module GongHuiCangKuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会仓库分赃
    module GongHuiFenZangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFenZangReq;
        function encode(o, b):any;
    }

// [返回]公会仓库分赃
    module GongHuiFenZangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本日志
    module GongHuiFuBenLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenLogReq;
        function encode(o, b):any;
    }

// [返回]公会副本日志
    module GongHuiFuBenLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本击杀排行
    module GongHuiFuBenJiShaPaiHangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenJiShaPaiHangReq;
        function encode(o, b):any;
    }

// [返回]公会副本击杀排行  展示和公会列表相同 吧宣言变成击杀数量
    module GongHuiFuBenJiShaPaiHangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本伤害排行列表
    module GongHuiFuBenShangHaiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenShangHaiListReq;
        function encode(o, b):any;
    }

// [返回]公会副本伤害排行列表
    module GongHuiFuBenShangHaiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本面板
    module GongHuiFuBenPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenPanelReq;
        function encode(o, b):any;
    }

// [返回]公会副本面板
    module GongHuiFuBenPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本战斗面板
    module GongHuiFuBenFightPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenFightPanelReq;
        function encode(o, b):any;
    }

// [返回]公会副本战斗面板
    module GongHuiFuBenFightPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本日志
    module GetGongHuiFuBenLogListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiFuBenLogListReq;
        function encode(o, b):any;
    }

// [返回]公会副本日志
    module GetGongHuiFuBenLogListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]阵型界面
    module RoomFightPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RoomFightPanelReq;
        function encode(o, b):any;
    }

// [返回]阵型界面
    module RoomFightPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]更换战斗精灵
    module ChangeRoomHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeRoomHeroReq;
        function encode(o, b):any;
    }

// [返回]更换战斗精灵
    module ChangeRoomHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]更改自动战斗设置
    module ChangeRoomFightStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeRoomFightStatusReq;
        function encode(o, b):any;
    }

// [返回]更改自动战斗设置
    module ChangeRoomFightStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]更改房间人员
    module ChangeRoomFightPersonReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeRoomFightPersonReq;
        function encode(o, b):any;
    }

// [返回]更改房间人员
    module ChangeRoomFightPersonRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会副本挑战
    module GongHuiFuBenFightStartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenFightStartReq;
        function encode(o, b):any;
    }

// [返回]公会副本挑战
    module GongHuiFuBenFightStartRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]创建房间
    module ChangeRoomReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeRoomReq;
        function encode(o, b):any;
    }

// [返回]创建房间
    module ChangeRoomRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]重置公会副本章节
    module ResetGongHuiFuBenDaGuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ResetGongHuiFuBenDaGuanReq;
        function encode(o, b):any;
    }

// [返回]重置公会副本章节
    module ResetGongHuiFuBenDaGuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]重置公会副本章节
    module PushGongHuiFuBenStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]公会副本喊话
    module GongHuiFuBenHanHuaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiFuBenHanHuaReq;
        function encode(o, b):any;
    }

// [返回]公会副本喊话
    module GongHuiFuBenHanHuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】保存eduIdx
    module SaveEduIdxReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SaveEduIdxReq;
        function encode(o, b):any;
    }

// 【反馈】保存eduIdx
    module SaveEduIdxRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新手引导招募特定武将
    module NewbieGetHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewbieGetHeroReq;
        function encode(o, b):any;
    }

// 【反馈】新手引导招募特定武将
    module NewbieGetHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新手引导残魂招募特定武将
    module NewbieGetHeroBySoulReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewbieGetHeroBySoulReq;
        function encode(o, b):any;
    }

// 【反馈】新手引导残魂招募特定武将
    module NewbieGetHeroBySoulRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】触发引导字段保存
    module SaveEduCodeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SaveEduCodeReq;
        function encode(o, b):any;
    }

// 【反馈】触发引导字段保存
    module SaveEduCodeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取在线奖励列表
    module GetZaixianJiangliListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZaixianJiangliListReq;
        function encode(o, b):any;
    }

// 【反馈】获取在线奖励列表
    module GetZaixianJiangliListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取在线奖励
    module DoGetZaixianJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetZaixianJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取在线奖励
    module DoGetZaixianJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取连续奖励
    module DoGetLianxuJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetLianxuJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取连续奖励
    module DoGetLianxuJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取玩家连续登录奖励的列表
    module GetLianxuJiangliListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLianxuJiangliListReq;
        function encode(o, b):any;
    }

// 【反馈】获取玩家连续登录奖励的列表
    module GetLianxuJiangliListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取冲级奖励列表
    module GetChongjiJiangliListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChongjiJiangliListReq;
        function encode(o, b):any;
    }

// 【反馈】获取冲级奖励列表
    module GetChongjiJiangliListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取冲级奖励
    module DoGetChongjiJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetChongjiJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取冲级奖励
    module DoGetChongjiJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取领取体力的信息
    module GetTiliInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTiliInfoReq;
        function encode(o, b):any;
    }

// 【反馈】获取领取体力的信息
    module GetTiliInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取体力
    module DoGetTiliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetTiliReq;
        function encode(o, b):any;
    }

// 【反馈】领取体力
    module DoGetTiliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取问答题目
    module GetWendaItemReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWendaItemReq;
        function encode(o, b):any;
    }

// 【反馈】获取问答题目
    module GetWendaItemRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】回答问题
    module AnswerQuestionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AnswerQuestionReq;
        function encode(o, b):any;
    }

// 【反馈】回答问题
    module AnswerQuestionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取玩家星数奖励下一个奖励的ID
    module GetNextXingshuJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNextXingshuJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】获取玩家星数奖励下一个奖励的ID
    module GetNextXingshuJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取星数奖励
    module DoGetXingshuJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetXingshuJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取星数奖励
    module DoGetXingshuJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取首充奖励列表
    module GetShouchongJiangliListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShouchongJiangliListReq;
        function encode(o, b):any;
    }

// 【反馈】获取首充奖励列表
    module GetShouchongJiangliListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取首充奖励
    module DoGetShouchongJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetShouchongJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取首充奖励
    module DoGetShouchongJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取月卡奖励列表
    module LoadYueKaJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadYueKaJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】获取月卡奖励列表
    module LoadYueKaJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取月卡奖励
    module DoGetYueKaJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetYueKaJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取月卡奖励
    module DoGetYueKaJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取限时冲关列表
    module GetXianshiChongguanListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetXianshiChongguanListReq;
        function encode(o, b):any;
    }

// 【反馈】获取限时冲关列表
    module GetXianshiChongguanListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取限时冲关奖励
    module DoGetXianshiChongguanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetXianshiChongguanReq;
        function encode(o, b):any;
    }

// 【反馈】领取限时冲关奖励
    module DoGetXianshiChongguanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取天降元宝列表
    module GetTianjiangYuanbaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTianjiangYuanbaoListReq;
        function encode(o, b):any;
    }

// 【反馈】获取天降元宝列表
    module GetTianjiangYuanbaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取天降元宝奖励
    module DoGetTianjiangYuanbaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetTianjiangYuanbaoReq;
        function encode(o, b):any;
    }

// 【反馈】领取天降元宝奖励
    module DoGetTianjiangYuanbaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取天天砸金蛋列表
    module GetTiantianZaJindanListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTiantianZaJindanListReq;
        function encode(o, b):any;
    }

// 【反馈】获取天天砸金蛋列表
    module GetTiantianZaJindanListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取天天砸金蛋奖励
    module DoGetTiantianZaJindanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetTiantianZaJindanReq;
        function encode(o, b):any;
    }

// 【反馈】领取天天砸金蛋奖励
    module DoGetTiantianZaJindanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取天天砸金蛋的信息
    module GetTiantianZaJindanInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTiantianZaJindanInfoReq;
        function encode(o, b):any;
    }

// 【反馈】获取天天砸金蛋的信息
    module GetTiantianZaJindanInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】砸金蛋
    module DoZaJindanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoZaJindanReq;
        function encode(o, b):any;
    }

// 【反馈】领取体力
    module DoZaJindanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取次日登录奖励
    module DoGetCiriJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetCiriJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取次日登录奖励
    module DoGetCiriJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取玩家次日登录奖励的列表
    module GetCiriJiangliListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetCiriJiangliListReq;
        function encode(o, b):any;
    }

// 【反馈】获取玩家次日登录奖励的列表
    module GetCiriJiangliListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取连续充值奖励
    module DoGetLianxuChongzhiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetLianxuChongzhiReq;
        function encode(o, b):any;
    }

// 【反馈】领取连续充值奖励
    module DoGetLianxuChongzhiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取玩家连续充值奖励的列表
    module GetLianxuChongzhiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLianxuChongzhiListReq;
        function encode(o, b):any;
    }

// 【反馈】获取玩家连续充值奖励的列表
    module GetLianxuChongzhiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取每日签到奖励
    module DoGetMeiriQiandaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetMeiriQiandaoReq;
        function encode(o, b):any;
    }

// 【反馈】领取每日签到奖励
    module DoGetMeiriQiandaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取玩家每日签到奖励的列表
    module GetMeiriQiandaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMeiriQiandaoListReq;
        function encode(o, b):any;
    }

// 【反馈】获取玩家每日签到奖励的列表
    module GetMeiriQiandaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】每日签到补签操作
    module BuQianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuQianReq;
        function encode(o, b):any;
    }

// 【反馈】每日签到补签操作
    module BuQianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】天降元宝补签操作
    module BuQianTianjiangYuanbaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuQianTianjiangYuanbaoReq;
        function encode(o, b):any;
    }

// 【反馈】天降元宝补签操作
    module BuQianTianjiangYuanbaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】免费抽取和充值额外奖励信息
    module GetFreeRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFreeRewardReq;
        function encode(o, b):any;
    }

// 【反馈】免费抽取和充值额外奖励信息
    module GetFreeRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取小额充值奖励
    module GetXiaoyeChongzhiJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetXiaoyeChongzhiJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】获取小额充值奖励
    module GetXiaoyeChongzhiJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取周卡奖励列表
    module LoadZhouKaJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadZhouKaJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】获取周卡奖励列表
    module LoadZhouKaJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取周卡奖励
    module DoGetZhouKaJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetZhouKaJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取周卡奖励
    module DoGetZhouKaJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取终身卡奖励列表
    module LoadZhongShenKaJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadZhongShenKaJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】获取终身卡奖励列表
    module LoadZhongShenKaJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取终身卡奖励
    module DoGetZhongShenKaJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetZhongShenKaJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取终身卡奖励
    module DoGetZhongShenKaJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 一元特价类奖励请求
    module GetYiyuantejiaRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetYiyuantejiaRewardReq;
        function encode(o, b):any;
    }

// 一元特价类奖励反馈 神秘来电活动奖励推送(神秘来电是直购活动 充完了直接发 所以需要提示给玩家)
    module GetYiyuantejiaRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】财神殿闯关战斗
    module CsdFightPVEReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CsdFightPVEReq;
        function encode(o, b):any;
    }

// 【反馈】财神殿闯关战斗
    module CsdFightPVERsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家已通过(财神殿)活动关列表
    module GetCsdHuodongguanListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetCsdHuodongguanListReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家已通过(财神殿)活动关列表
    module GetCsdHuodongguanListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】（财神殿）挂机
    module CsdGuajiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CsdGuajiReq;
        function encode(o, b):any;
    }

// 【反馈】（财神殿）挂机
    module CsdGuajiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取财神殿排名列表（前50名）
    module GetCsdPaihangListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetCsdPaihangListReq;
        function encode(o, b):any;
    }

// 【反馈】获取财神殿排名列表（前50名）
    module GetCsdPaihangListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】财神殿排名观看他人战斗
    module WatchingOtherFightOfPaimingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WatchingOtherFightOfPaimingReq;
        function encode(o, b):any;
    }

// 【反馈】财神殿排名观看他人战斗
    module WatchingOtherFightOfPaimingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】财神殿奖励观看他人战斗
    module WatchingOtherFightOfJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WatchingOtherFightOfJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】财神殿奖励观看他人战斗
    module WatchingOtherFightOfJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取财神殿奖励说明列表
    module GetJiangliExplainListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetJiangliExplainListReq;
        function encode(o, b):any;
    }

// 【反馈】获取财神殿奖励说明列表
    module GetJiangliExplainListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤图鉴系统面板
    module PifuTujianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PifuTujianPanelReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤图鉴系统面板
    module PifuTujianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤图鉴操作面板
    module PifuTujianOperationPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PifuTujianOperationPanelReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤图鉴操作面板
    module PifuTujianOperationPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤图鉴单皮肤操作
    module PifuTujianOperateReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PifuTujianOperateReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤图鉴单皮肤操作
    module PifuTujianOperateRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤图鉴使用皮肤
    module PifuTujianUseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PifuTujianUseReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤图鉴使用皮肤
    module PifuTujianUseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤图鉴自定义
    module PifuTujianPersonalDefineReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PifuTujianPersonalDefineReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤图鉴自定义
    module PifuTujianPersonalDefineRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]获得新的玩家皮肤图鉴
    module PushNewPifuTujianInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤图鉴夺宝抽奖
    module PifuTujianLuckyDrawReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PifuTujianLuckyDrawReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤图鉴夺宝抽奖
    module PifuTujianLuckyDrawRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤图鉴替换单个皮肤部件
    module PifuTujianSwitchOneGugePartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PifuTujianSwitchOneGugePartReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤图鉴替换单个皮肤部件
    module PifuTujianSwitchOneGugePartRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]名片面板
    module MingPianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MingPianPanelReq;
        function encode(o, b):any;
    }

// [返回]名片面板
    module MingPianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获得名片  跨服
    module MingPianPanelCrossServerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MingPianPanelCrossServerReq;
        function encode(o, b):any;
    }

// [反馈]获得名片 跨服
    module MingPianPanelCrossServerRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]改变自己名片的状态
    module ChangeMingPianStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeMingPianStatusReq;
        function encode(o, b):any;
    }

// [反馈]改变自己名片的状态
    module ChangeMingPianStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]改变自己名片的签名
    module ChangeMingPianQianMingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeMingPianQianMingReq;
        function encode(o, b):any;
    }

// [反馈]改变自己名片的签名
    module ChangeMingPianQianMingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跨服玩家名片面板
    module CrossPlayerCardPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CrossPlayerCardPanelReq;
        function encode(o, b):any;
    }

// [返回]名片面板
    module CrossPlayerCardPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跨服玩家名片精灵详情
    module CrossPlayerCardHeroPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CrossPlayerCardHeroPanelReq;
        function encode(o, b):any;
    }

// [返回]跨服玩家名片精灵详情
    module CrossPlayerCardHeroPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跨服名片玩家详情信息
    module CrossPlayerDetailInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CrossPlayerDetailInfoReq;
        function encode(o, b):any;
    }

// [返回]跨服名片玩家详情信息
    module CrossPlayerDetailInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】排行榜
    module GetPaihangBangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPaihangBangReq;
        function encode(o, b):any;
    }

// 【反馈】排行榜
    module GetPaihangBangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求排行榜分享奖励
    module GetPaihangBangShareRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPaihangBangShareRewardReq;
        function encode(o, b):any;
    }

// 【反馈】请求排行榜分享奖励
    module GetPaihangBangShareRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求所有排行榜类型
    module GetAllPaihangBangTypeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAllPaihangBangTypeReq;
        function encode(o, b):any;
    }

// 【反馈】请求所有排行榜类型
    module GetAllPaihangBangTypeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]商品列表
    module ScoreShopGoodsListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ScoreShopGoodsListReq;
        function encode(o, b):any;
    }

// [反馈]商品列表
    module ScoreShopGoodsListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买商品
    module BuyScoreShopGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyScoreShopGoodsReq;
        function encode(o, b):any;
    }

// [返回]购买商品
    module BuyScoreShopGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]刷新商品
    module RefreshScoreShopReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RefreshScoreShopReq;
        function encode(o, b):any;
    }

// [返回]刷新商品
    module RefreshScoreShopRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]恋爱互动面板
    module LianAiHuDongPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LianAiHuDongPanelReq;
        function encode(o, b):any;
    }

// [返回]恋爱互动面板
    module LianAiHuDongPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]恋爱送礼物
    module SongLiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SongLiWuReq;
        function encode(o, b):any;
    }

// [返回]恋爱送礼物
    module SongLiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]恋爱切磋
    module LianAiQieCuoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LianAiQieCuoReq;
        function encode(o, b):any;
    }

// [返回]恋爱切磋
    module LianAiQieCuoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]恋爱任务最后一步
    module FinishLianAiRenWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FinishLianAiRenWuReq;
        function encode(o, b):any;
    }

// [返回]恋爱任务最后一步
    module FinishLianAiRenWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]恋爱任务中的打斗
    module LianAiTaskFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LianAiTaskFightReq;
        function encode(o, b):any;
    }

// [返回]恋爱任务中的打斗
    module LianAiTaskFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]她的故事
    module HerStoryReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HerStoryReq;
        function encode(o, b):any;
    }

// [返回]她的故事
    module HerStoryRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]恋爱技能升级面板
    module LianAiJiNengShengJiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LianAiJiNengShengJiPanelReq;
        function encode(o, b):any;
    }

// [返回]恋爱技能升级面板
    module LianAiJiNengShengJiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]恋爱技能升级
    module LianAiJiNengShengJiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LianAiJiNengShengJiReq;
        function encode(o, b):any;
    }

// [返回]恋爱技能升级
    module LianAiJiNengShengJiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]恋爱技能开关
    module LianAiJiNengSwitchReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LianAiJiNengSwitchReq;
        function encode(o, b):any;
    }

// [返回]恋爱技能开关
    module LianAiJiNengSwitchRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]周期性特权卡面板信息
    module PeriodCardPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PeriodCardPanelReq;
        function encode(o, b):any;
    }

// [返回]购买周期性特权卡
    module PeriodCardPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]周期性特权卡领取特权表奖励
    module PeriodCardGetTequanRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PeriodCardGetTequanRewardReq;
        function encode(o, b):any;
    }

// [返回]周期性特权卡领取特权表奖励
    module PeriodCardGetTequanRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】位置上的技能书升级属性预览
    module SkillBookUpgradePropShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SkillBookUpgradePropShowReq;
        function encode(o, b):any;
    }

// 【反馈】位置上的技能书升级属性预览
    module SkillBookUpgradePropShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】位置上的技能书升级
    module SkillBookUpgradeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SkillBookUpgradeReq;
        function encode(o, b):any;
    }

// 【反馈】位置上的技能书升级
    module SkillBookUpgradeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】位置上的技能书升星属性预览
    module SkillBookStarUpPropShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SkillBookStarUpPropShowReq;
        function encode(o, b):any;
    }

// 【反馈】位置上的技能书升星属性预览
    module SkillBookStarUpPropShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】技能书升星
    module SkillBookStarUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SkillBookStarUpReq;
        function encode(o, b):any;
    }

// 【反馈】技能书升星
    module SkillBookStarUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】技能书分解
    module SkillBookSendBackReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SkillBookSendBackReq;
        function encode(o, b):any;
    }

// 【请求】技能书分解
    module SkillBookSendBackRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】某玩家某位置的技能书协作列表
    module GetSkillBookXiezuoInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetSkillBookXiezuoInfoReq;
        function encode(o, b):any;
    }

// 【反馈】某玩家某位置的技能书协作列表
    module GetSkillBookXiezuoInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】技能书上阵
    module SkillBookTakePositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SkillBookTakePositionReq;
        function encode(o, b):any;
    }

// 【反馈】技能书上阵
    module SkillBookTakePositionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】技能书相应的宠物上阵
    module SkillBookHeroTakePositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SkillBookHeroTakePositionReq;
        function encode(o, b):any;
    }

// 【反馈】技能书相应的宠物上阵
    module SkillBookHeroTakePositionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】技能书合成
    module SkillBookMergeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SkillBookMergeReq;
        function encode(o, b):any;
    }

// 【反馈】技能书合成
    module SkillBookMergeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买一元特价商品
    module BuyYiyuanGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyYiyuanGoodsReq;
        function encode(o, b):any;
    }

// 【反馈】购买一元特价商品
    module BuyYiyuanGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取（首次或日常）一元商品列表
    module GetYiyuanGoodsListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetYiyuanGoodsListReq;
        function encode(o, b):any;
    }

// 【反馈】获取（首次或日常）一元商品列表
    module GetYiyuanGoodsListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 加载新活动列表
    module GetNewActivityListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewActivityListReq;
        function encode(o, b):any;
    }

// 加载新活动列表
    module GetNewActivityListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家获取迎财神活动信息请求
    module GetYingCaiShenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetYingCaiShenReq;
        function encode(o, b):any;
    }

// 玩家获取迎财神活动信息反馈
    module GetYingCaiShenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家获取迎财神活动奖励请求
    module GetAwardFromYingCaiShenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAwardFromYingCaiShenReq;
        function encode(o, b):any;
    }

// 玩家获取迎财神活动奖励反馈
    module GetAwardFromYingCaiShenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家获取充值排行活动信息请求
    module GetChongZhiPaiHangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChongZhiPaiHangReq;
        function encode(o, b):any;
    }

// 玩家获取充值排行活动信息请求
    module GetChongZhiPaiHangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家获取新限时排行数据信息请求
    module GetNewValueListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewValueListReq;
        function encode(o, b):any;
    }

// 玩家获取新限时排行数据信息请求
    module GetNewValueListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开累计充值面板
    module GetLeijiChongzhiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLeijiChongzhiListReq;
        function encode(o, b):any;
    }

// 打开累计充值面板
    module GetLeijiChongzhiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取累计充值奖励
    module DoGetLeijiChongzhiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetLeijiChongzhiReq;
        function encode(o, b):any;
    }

// 领取累计充值奖励
    module DoGetLeijiChongzhiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开累计消费面板
    module GetXiaofeiJiangliListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetXiaofeiJiangliListReq;
        function encode(o, b):any;
    }

// 打开累计消费面板
    module GetXiaofeiJiangliListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取消费奖励
    module DoGetXiaofeiJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetXiaofeiJiangliReq;
        function encode(o, b):any;
    }

// 领取消费奖励
    module DoGetXiaofeiJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取每日充值列表
    module GetMeiriChongzhiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMeiriChongzhiListReq;
        function encode(o, b):any;
    }

// [反馈]获取每日充值列表
    module GetMeiriChongzhiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取每日充值奖励
    module DoGetMeiriChongzhiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetMeiriChongzhiReq;
        function encode(o, b):any;
    }

// [反馈]领取每日充值奖励
    module DoGetMeiriChongzhiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取每日消费列表
    module GetMeiriXiaofeiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMeiriXiaofeiListReq;
        function encode(o, b):any;
    }

// [反馈]获取每日消费列表
    module GetMeiriXiaofeiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取消费奖励
    module DoGetMeiriXiaofeiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetMeiriXiaofeiReq;
        function encode(o, b):any;
    }

// [反馈]领取每日消费奖励
    module DoGetMeiriXiaofeiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]限时冲级列表
    module XianShiChongJiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.XianShiChongJiListReq;
        function encode(o, b):any;
    }

// [反馈]限时冲级列表
    module XianShiChongJiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取冲级奖励
    module DoGetXianShiChongJiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetXianShiChongJiReq;
        function encode(o, b):any;
    }

// [反馈]领取每日冲级奖励
    module DoGetXianShiChongJiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]打折商品list
    module DaZheShangPinListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DaZheShangPinListReq;
        function encode(o, b):any;
    }

// [反馈]打折商品list
    module DaZheShangPinListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买打折商品
    module GouMaiDaZheShangPinReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiDaZheShangPinReq;
        function encode(o, b):any;
    }

// [反馈]购买打折商品
    module GouMaiDaZheShangPinRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]成长基金list
    module ChengZhangJiJinListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChengZhangJiJinListReq;
        function encode(o, b):any;
    }

// [请求]成长基金list
    module ChengZhangJiJinListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取成长基金
    module GetChengZhangJiJinReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChengZhangJiJinReq;
        function encode(o, b):any;
    }

// [请求]领取成长基金
    module GetChengZhangJiJinRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载单笔充值数据
    module LoadDanBiChongZhiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadDanBiChongZhiListReq;
        function encode(o, b):any;
    }

// [反馈]加载单笔充值数据
    module LoadDanBiChongZhiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取单笔充值
    module GetDanBiChongZhiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDanBiChongZhiReq;
        function encode(o, b):any;
    }

// [反馈]领取单笔充值
    module GetDanBiChongZhiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载五元充值数据
    module LoadWuYuanChongZhiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadWuYuanChongZhiListReq;
        function encode(o, b):any;
    }

// [反馈]加载五元充值数据
    module LoadWuYuanChongZhiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取五元充值
    module GetWuYuanChongZhiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWuYuanChongZhiReq;
        function encode(o, b):any;
    }

// [反馈]领取五元充值
    module GetWuYuanChongZhiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载活动任务数据
    module LoadMissionActivityListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadMissionActivityListReq;
        function encode(o, b):any;
    }

// [反馈]加载活动任务数据
    module LoadMissionActivityListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取活动任务奖励
    module GetMissionActivityRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMissionActivityRewardReq;
        function encode(o, b):any;
    }

// [反馈]领取活动任务奖励
    module GetMissionActivityRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]登录送礼活动
    module GetLeiJiDengLuRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLeiJiDengLuRewardReq;
        function encode(o, b):any;
    }

// [反馈]登录送礼活动
    module GetLeiJiDengLuRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]登录送礼数据
    module LoadLeiJiDengLuListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadLeiJiDengLuListReq;
        function encode(o, b):any;
    }

// [反馈]登录送礼数据
    module LoadLeiJiDengLuListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载道具收集活动数据
    module LoadItemCollectActivityListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadItemCollectActivityListReq;
        function encode(o, b):any;
    }

// [反馈]加载道具收集活动数据
    module LoadItemCollectActivityListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取道具收集活动数据
    module GetItemCollectActivityRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetItemCollectActivityRewardReq;
        function encode(o, b):any;
    }

// [反馈]领取道具收集活动数据
    module GetItemCollectActivityRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载一冲选领数据
    module LoadYiChongXuanLingListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadYiChongXuanLingListReq;
        function encode(o, b):any;
    }

// [反馈]加载一冲选领数据
    module LoadYiChongXuanLingListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取一冲选领
    module GetYiChongXuanLingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetYiChongXuanLingReq;
        function encode(o, b):any;
    }

// [反馈]领取一冲选领
    module GetYiChongXuanLingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]摇奖面板
    module YaoJiangPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YaoJiangPanelReq;
        function encode(o, b):any;
    }

// [返回]摇奖面板
    module YaoJiangPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]摇奖
    module YaoJiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YaoJiangReq;
        function encode(o, b):any;
    }

// [返回]摇奖
    module YaoJiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 活动副本面板
    module HuoDongFuBenPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HuoDongFuBenPanelReq;
        function encode(o, b):any;
    }

// [返回] 活动副本面板
    module HuoDongFuBenPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 进入副本
    module EnterFuBenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EnterFuBenReq;
        function encode(o, b):any;
    }

// [返回] 进入副本
    module EnterFuBenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 打活动副本
    module FightHuoDongFuBenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightHuoDongFuBenReq;
        function encode(o, b):any;
    }

// [返回] 打活动副本
    module FightHuoDongFuBenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 扫荡副本
    module HuoDongFuBenSaoDangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HuoDongFuBenSaoDangReq;
        function encode(o, b):any;
    }

// [返回] 扫荡副本
    module HuoDongFuBenSaoDangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 购买门票 买完了就直接就进去了
    module GouMaiMenPiaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiMenPiaoReq;
        function encode(o, b):any;
    }

// [返回] 购买门票 买完了就直接就进去了
    module GouMaiMenPiaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开服每日一冲列表
    module GetKaiFuMeiRiYiChongListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetKaiFuMeiRiYiChongListReq;
        function encode(o, b):any;
    }

// [反馈]开服每日一冲列表
    module GetKaiFuMeiRiYiChongListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取每日一冲奖励
    module DoGetKaiFuMeiRiYiChongReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetKaiFuMeiRiYiChongReq;
        function encode(o, b):any;
    }

// [反馈]领取每日一冲奖励
    module DoGetKaiFuMeiRiYiChongRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开服登录列表
    module GetKaiFuDengLuListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetKaiFuDengLuListReq;
        function encode(o, b):any;
    }

// [反馈]开服登录列表
    module GetKaiFuDengLuListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取登录奖励
    module DoGetKaiFuDengLuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetKaiFuDengLuReq;
        function encode(o, b):any;
    }

// [反馈]领取登录奖励
    module DoGetKaiFuDengLuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开服半价购买列表
    module GetKaiFuBanJiaGouMaiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetKaiFuBanJiaGouMaiListReq;
        function encode(o, b):any;
    }

// [请求]开服半价购买列表
    module GetKaiFuBanJiaGouMaiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买
    module DoGetKaiFuBanJiaGouMaiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetKaiFuBanJiaGouMaiReq;
        function encode(o, b):any;
    }

// [反馈]购买
    module DoGetKaiFuBanJiaGouMaiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 七日竞赛活动列表数据
    module SevenMatchPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SevenMatchPanelReq;
        function encode(o, b):any;
    }

// [返回] 七日竞赛活动列表数据
    module SevenMatchPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 七日竞赛玩家排行数据
    module SevenMatchPlayerListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SevenMatchPlayerListReq;
        function encode(o, b):any;
    }

// [返回] 七日竞赛玩家排行数据
    module SevenMatchPlayerListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 购买奖励请求预览
    module GetSevenMatchGiftInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetSevenMatchGiftInfoReq;
        function encode(o, b):any;
    }

// [返回] 购买奖励请求预览
    module GetSevenMatchGiftInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 购买七日竞赛礼包
    module BuySevenMatchGiftReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuySevenMatchGiftReq;
        function encode(o, b):any;
    }

// [返回] 购买七日竞赛礼包
    module BuySevenMatchGiftRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]每日一够列表
    module GetMeiRiYiGouListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMeiRiYiGouListReq;
        function encode(o, b):any;
    }

// [反馈]每日一够列表
    module GetMeiRiYiGouListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取每日一购奖励
    module DoGetMeiRiYiGouJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetMeiRiYiGouJiangLiReq;
        function encode(o, b):any;
    }

// [反馈]领取领取每日一购奖励
    module DoGetMeiRiYiGouJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载单笔充值数据
    module LoadDanBiGouMaiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadDanBiGouMaiListReq;
        function encode(o, b):any;
    }

// [反馈]加载单笔充值数据
    module LoadDanBiGouMaiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获得单笔购买奖励
    module GetDanBiGouMaiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDanBiGouMaiReq;
        function encode(o, b):any;
    }

// [反馈]获得单笔购买奖励
    module GetDanBiGouMaiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载显示升阶数据
    module LoadXianShiShengJieListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadXianShiShengJieListReq;
        function encode(o, b):any;
    }

// [反馈]加载限时升阶数据
    module LoadXianShiShengJieListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获得显示升阶奖励
    module GetXianShiShengJieReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetXianShiShengJieReq;
        function encode(o, b):any;
    }

// [反馈]获得显示升阶奖励
    module GetXianShiShengJieRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载大转盘信息(新)
    module LoadDaZhuanPanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadDaZhuanPanPanelReq;
        function encode(o, b):any;
    }

// [反馈]加载大转盘信息(新)
    module LoadDaZhuanPanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]大转盘摇奖(新)
    module GetDaZhuanPanRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDaZhuanPanRewardReq;
        function encode(o, b):any;
    }

// [反馈]大转盘摇奖(新)
    module GetDaZhuanPanRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 请求锦标赛排行信息
    module GetKuaFuJinBiaoSaiRankInfosReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetKuaFuJinBiaoSaiRankInfosReq;
        function encode(o, b):any;
    }

// 请求锦标赛排行信息
    module GetKuaFuJinBiaoSaiRankInfosRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 跨服锦标赛副本信息
    module GetKuaFuJinBiaoSaiInfosReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetKuaFuJinBiaoSaiInfosReq;
        function encode(o, b):any;
    }

// 跨服锦标赛副本信息
    module GetKuaFuJinBiaoSaiInfosRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 挑战boss
    module KuaFuJinBiaoSaiFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KuaFuJinBiaoSaiFightReq;
        function encode(o, b):any;
    }

// 挑战boss
    module KuaFuJinBiaoSaiFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 购买挑战次数
    module KuaFuJinBiaoSaiBuyTimesReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KuaFuJinBiaoSaiBuyTimesReq;
        function encode(o, b):any;
    }

// 购买挑战次数
    module KuaFuJinBiaoSaiBuyTimesRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 购买箱子
    module KuaFuJinBiaoSaiBuyBoxReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KuaFuJinBiaoSaiBuyBoxReq;
        function encode(o, b):any;
    }

// 购买箱子
    module KuaFuJinBiaoSaiBuyBoxRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开箱子(上限为1000个,代码限制就行)
    module KuaFuJinBiaoSaiOpenBoxReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KuaFuJinBiaoSaiOpenBoxReq;
        function encode(o, b):any;
    }

// 打开箱子
    module KuaFuJinBiaoSaiOpenBoxRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]切换地图
    module SwitchMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SwitchMapReq;
        function encode(o, b):any;
    }

// [返回]切换地图
    module SwitchMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取怪物
    module GetGuaiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGuaiWuReq;
        function encode(o, b):any;
    }

// [返回]获取怪物
    module GetGuaiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]打怪物
    module FightGuaiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightGuaiWuReq;
        function encode(o, b):any;
    }

// [返回]打怪物
    module FightGuaiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取跟天气有关的怪物
    module GetTianQiGuaiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTianQiGuaiWuReq;
        function encode(o, b):any;
    }

// [返回]获取跟天气有关的怪物
    module GetTianQiGuaiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获得天气地图list
    module GetTianQiMapListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTianQiMapListReq;
        function encode(o, b):any;
    }

// [返回]获得天气地图list
    module GetTianQiMapListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]打天气怪物
    module FightTianQiGuaiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightTianQiGuaiWuReq;
        function encode(o, b):any;
    }

// [返回]打天气怪物
    module FightTianQiGuaiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】设定当前使用战阵
    module ZhanZhenNowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanZhenNowReq;
        function encode(o, b):any;
    }

// 【反馈】设定当前使用战阵，返回当前使用战阵dbID
    module ZhanZhenNowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】合成战阵
    module MakeZhanZhenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MakeZhanZhenReq;
        function encode(o, b):any;
    }

// 【反馈】返回新合成战阵的dbID
    module MakeZhanZhenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】战阵强化
    module ZhanZhenQianghuaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanZhenQianghuaReq;
        function encode(o, b):any;
    }

// 【反馈】战阵强化
    module ZhanZhenQianghuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得战阵列表
    module GetZhanZhenListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhanZhenListReq;
        function encode(o, b):any;
    }

// 【反馈】获得战阵列表
    module GetZhanZhenListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得战阵碎片列表
    module GetZhanZhenSuiPianListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhanZhenSuiPianListReq;
        function encode(o, b):any;
    }

// 【反馈】获得战阵碎片列表
    module GetZhanZhenSuiPianListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会boss面板
    module GongHuiBossPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiBossPanelReq;
        function encode(o, b):any;
    }

// [返回]公会boss面板   打死boss的推送/召唤 全公会在线玩家推送也会推送
    module GongHuiBossPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]打公会boss
    module FightGongHuiBossReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightGongHuiBossReq;
        function encode(o, b):any;
    }

// [反馈]打公会boss
    module FightGongHuiBossRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]召唤  想本公会所有在下人员推送面板消息
    module ZhaoHuanGongHuiBossReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhaoHuanGongHuiBossReq;
        function encode(o, b):any;
    }

// [请求]公会boss伤害排行列表
    module GongHuiBossShangHaiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiBossShangHaiListReq;
        function encode(o, b):any;
    }

// [返回]公会boss伤害排行列表
    module GongHuiBossShangHaiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会boss是否自动召唤
    module GongHuiBossAutoZhaoHuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiBossAutoZhaoHuanReq;
        function encode(o, b):any;
    }

// [返回]公会boss是否自动召唤
    module GongHuiBossAutoZhaoHuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开日充累充面板
    module GetRiChongLeiChongListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRiChongLeiChongListReq;
        function encode(o, b):any;
    }

// 打开日充累充面板
    module GetRiChongLeiChongListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取累计充值奖励
    module DoGetRiChongLeiChongReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetRiChongLeiChongReq;
        function encode(o, b):any;
    }

// 领取累计充值奖励
    module DoGetRiChongLeiChongRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 首冲团购面板
    module ShouChongTuanGouPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShouChongTuanGouPanelReq;
        function encode(o, b):any;
    }

// 首冲团购面板
    module ShouChongTuanGouPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取首充团购奖励
    module GetShouChongTuanGouRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShouChongTuanGouRewardReq;
        function encode(o, b):any;
    }

// 领取首充团购奖励
    module GetShouChongTuanGouRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开签到奖励
    module GetQianDaoJiangLiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetQianDaoJiangLiListReq;
        function encode(o, b):any;
    }

// 打开签到奖励
    module GetQianDaoJiangLiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取签到奖励
    module DoGetQianDaoJiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetQianDaoJiangReq;
        function encode(o, b):any;
    }

// 领取签到奖励
    module DoGetQianDaoJiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]天天返利面板
    module TianTianFanLiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TianTianFanLiPanelReq;
        function encode(o, b):any;
    }

// [反馈]加载天天返利面板
    module TianTianFanLiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取天天返利奖励
    module ReceiveTianTianFanLiRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReceiveTianTianFanLiRewardReq;
        function encode(o, b):any;
    }

// [反馈]领取天天返利奖励
    module ReceiveTianTianFanLiRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]神秘来电面板
    module ShenMiLaiDianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenMiLaiDianPanelReq;
        function encode(o, b):any;
    }

// [反馈]神秘来电面板
    module ShenMiLaiDianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开登录好礼
    module GetDengLuHaoLiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDengLuHaoLiListReq;
        function encode(o, b):any;
    }

// 打开登录好礼
    module GetDengLuHaoLiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取登录好礼
    module DoDengLuHaoLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoDengLuHaoLiReq;
        function encode(o, b):any;
    }

// 领取登录好礼
    module DoDengLuHaoLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]多倍奖励面板
    module DuoBeiJiangLiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DuoBeiJiangLiPanelReq;
        function encode(o, b):any;
    }

// [返回]多倍奖励面板
    module DuoBeiJiangLiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取多倍奖励
    module GetDuoBeiJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDuoBeiJiangLiReq;
        function encode(o, b):any;
    }

// [返回]领取多倍奖励
    module GetDuoBeiJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跨服秒杀面板
    module KuaFuMiaoShaPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KuaFuMiaoShaPanelReq;
        function encode(o, b):any;
    }

// [返回]跨服秒杀面板
    module KuaFuMiaoShaPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]买跨服秒杀
    module BuyKuaFuMiaoShaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyKuaFuMiaoShaReq;
        function encode(o, b):any;
    }

// [返回]买跨服秒杀
    module BuyKuaFuMiaoShaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开服限购大R礼包活动
    module KaifuXiangouInfoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KaifuXiangouInfoPanelReq;
        function encode(o, b):any;
    }

// [返回]开服限购大R礼包活动
    module KaifuXiangouInfoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]买开服限购大R礼包
    module BuyKaifuXiangouInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyKaifuXiangouInfoReq;
        function encode(o, b):any;
    }

// [返回]买开服限购大R礼包
    module BuyKaifuXiangouInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 领取达到xx后的全服奖励
    module TakeFirstOneAllServerRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TakeFirstOneAllServerRewardReq;
        function encode(o, b):any;
    }

// [返回] 领取达到xx后的全服奖励
    module TakeFirstOneAllServerRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 限时召唤面板
    module XianShiZhaoHuanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.XianShiZhaoHuanPanelReq;
        function encode(o, b):any;
    }

// [返回] 限时召唤面板
    module XianShiZhaoHuanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 限时召唤
    module GetXianShiZhaoHuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetXianShiZhaoHuanReq;
        function encode(o, b):any;
    }

// [返回] 限时召唤
    module GetXianShiZhaoHuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 限时召唤选择心愿
    module GetXianShiZhaoHuanChooseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetXianShiZhaoHuanChooseReq;
        function encode(o, b):any;
    }

// [返回] 限时召唤选择心愿
    module GetXianShiZhaoHuanChooseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 皇权诏令面板
    module HuangquanZhaolingPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HuangquanZhaolingPanelReq;
        function encode(o, b):any;
    }

// [返回] 皇权诏令面板
    module HuangquanZhaolingPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 皇权诏令领取奖励
    module GetHuangquanZhaolingRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHuangquanZhaolingRewardReq;
        function encode(o, b):any;
    }

// [返回] 皇权诏令领取奖励
    module GetHuangquanZhaolingRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 皇权诏令购买活跃
    module BuyHuangquanZhaolingHuoyueReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyHuangquanZhaolingHuoyueReq;
        function encode(o, b):any;
    }

// [返回] 皇权诏令购买活跃
    module BuyHuangquanZhaolingHuoyueRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 登录现金红包面板
    module DengluHongbaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DengluHongbaoPanelReq;
        function encode(o, b):any;
    }

// [返回] 登录现金红包面板
    module DengluHongbaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 登录现金红包开红包
    module OpenDengluHongbaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenDengluHongbaoReq;
        function encode(o, b):any;
    }

// [返回] 登录现金红包开红包
    module OpenDengluHongbaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 红包提现
    module GetCashFromHongbaoRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetCashFromHongbaoRewardReq;
        function encode(o, b):any;
    }

// [返回] 红包提现
    module GetCashFromHongbaoRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 任务现金红包面板
    module RenwuHongbaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenwuHongbaoPanelReq;
        function encode(o, b):any;
    }

// [返回] 任务现金红包面板
    module RenwuHongbaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 任务现金红包开红包
    module OpenRenwuHongbaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenRenwuHongbaoReq;
        function encode(o, b):any;
    }

// [返回] 任务现金红包开红包
    module OpenRenwuHongbaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]申请开服限购活动的折扣
    module ApplyDiscountOfKaifuXiangouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ApplyDiscountOfKaifuXiangouReq;
        function encode(o, b):any;
    }

// [返回]申请开服限购活动的折扣
    module ApplyDiscountOfKaifuXiangouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 限时等级礼包面板
    module XianShiDengJiLiBaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.XianShiDengJiLiBaoPanelReq;
        function encode(o, b):any;
    }

// [返回] 限时等级礼包面板
    module XianShiDengJiLiBaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 领取限时等级礼包奖励
    module GetXianShiDengJiLiBaoRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetXianShiDengJiLiBaoRewardReq;
        function encode(o, b):any;
    }

// [返回] 领取限时等级礼包奖励
    module GetXianShiDengJiLiBaoRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 自定义礼包充值消费礼包活动面板
    module DiyGiftPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DiyGiftPanelReq;
        function encode(o, b):any;
    }

// [返回] 自定义礼包充值消费礼包活动面板
    module DiyGiftPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 自定义礼包充值消费礼包奖励配置面板
    module DiySelectRewardPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DiySelectRewardPanelReq;
        function encode(o, b):any;
    }

// [返回] 自定义礼包充值消费礼包奖励配置面板
    module DiySelectRewardPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 自定义礼包充值消费礼包确认选择的奖励
    module DiySelectRewardConfirmReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DiySelectRewardConfirmReq;
        function encode(o, b):any;
    }

// [返回] 自定义礼包充值消费礼包确认选择的奖励
    module DiySelectRewardConfirmRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 购买自定义礼包充值消费礼包
    module DiyGiftBuyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DiyGiftBuyReq;
        function encode(o, b):any;
    }

// [返回] 购买自定义礼包充值消费礼包
    module DiyGiftBuyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 折扣商店活动面板
    module ZhekouShangdianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhekouShangdianPanelReq;
        function encode(o, b):any;
    }

// [返回] 折扣商店活动面板
    module ZhekouShangdianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 折扣商店活动刷新商品
    module ZhekouShangdianRefreshGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhekouShangdianRefreshGoodsReq;
        function encode(o, b):any;
    }

// [返回] 折扣商店活动刷新商品
    module ZhekouShangdianRefreshGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 折扣商店活动购买商品
    module ZhekouShangdianBuyGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhekouShangdianBuyGoodsReq;
        function encode(o, b):any;
    }

// [返回] 折扣商店活动购买商品
    module ZhekouShangdianBuyGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 各类基金活动面板
    module FundActivityPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FundActivityPanelReq;
        function encode(o, b):any;
    }

// [返回] 各类基金活动面板
    module FundActivityPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 各类基金活动领取奖励
    module GetFundActivityRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFundActivityRewardReq;
        function encode(o, b):any;
    }

// [返回] 各类基金活动领取奖励
    module GetFundActivityRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 各类基金活动解锁高级奖励
    module UnlockFundActivityHighRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UnlockFundActivityHighRewardReq;
        function encode(o, b):any;
    }

// [返回] 各类基金活动解锁高级奖励
    module UnlockFundActivityHighRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 七日商店购买商品
    module BuySevenDayShopGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuySevenDayShopGoodsReq;
        function encode(o, b):any;
    }

// [返回] 七日商店购买商品
    module BuySevenDayShopGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]金券消耗排行列表
    module XiaohaoPaihangListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.XiaohaoPaihangListReq;
        function encode(o, b):any;
    }

// [反馈]金券消耗排行列表
    module XiaohaoPaihangListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]活跃任务数据列表
    module HuoyueRenwuMissionListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HuoyueRenwuMissionListReq;
        function encode(o, b):any;
    }

// [反馈]活跃任务数据列表
    module HuoyueRenwuMissionListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]回馈礼包活动充值登录礼包活动
    module GetRechargeLoginGiftRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRechargeLoginGiftRewardReq;
        function encode(o, b):any;
    }

// [反馈]回馈礼包活动充值登录礼包活动
    module GetRechargeLoginGiftRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]回馈礼包活动充值登录礼包活动面板消息
    module RechargeLoginGiftPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RechargeLoginGiftPanelReq;
        function encode(o, b):any;
    }

// [反馈]回馈礼包活动充值登录礼包活动面板消息
    module RechargeLoginGiftPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]幸运转盘活动面板消息
    module LuckyWheelPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LuckyWheelPanelReq;
        function encode(o, b):any;
    }

// [反馈]幸运转盘活动面板消息
    module LuckyWheelPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]幸运转盘活动摇奖
    module GetLuckyWheelRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLuckyWheelRewardReq;
        function encode(o, b):any;
    }

// [反馈]幸运转盘活动摇奖
    module GetLuckyWheelRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]金猪存钱罐活动面板消息
    module PiggyBankPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PiggyBankPanelReq;
        function encode(o, b):any;
    }

// [反馈]金猪存钱罐活动面板消息
    module PiggyBankPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]金猪存钱罐活动领奖
    module GetPiggyBankRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPiggyBankRewardReq;
        function encode(o, b):any;
    }

// [反馈]金猪存钱罐活动领奖
    module GetPiggyBankRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]金猪存钱罐充值奖励查看通知
    module ReadPiggyBankRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReadPiggyBankRewardReq;
        function encode(o, b):any;
    }

// [反馈]金猪存钱罐充值奖励查看通知
    module ReadPiggyBankRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]七日竞赛活动领取在线时长礼包
    module SevenMatchGetOnlineRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SevenMatchGetOnlineRewardReq;
        function encode(o, b):any;
    }

// [反馈]七日竞赛活动领取在线时长礼包
    module SevenMatchGetOnlineRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 客户端心跳请求
    module UpdateClientStateReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateClientStateReq;
        function encode(o, b):any;
    }

// 客户端心跳请求
    module UpdateClientStateRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 加载服务器列表请求
    module LoadServerListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        var SERVER_TYPE
        function get(options ?:any):msgObj.LoadServerListReq;
        function encode(o, b):any;
    }

// 加载服务器列表反馈
    module LoadServerListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        var SERVER_TYPE
        function decode(o, b):any;

    }

// 加载服务器列表请求(gm专用)
    module LoadServerList4GMReq  {
        var MAIN_TYPE
        var SUB_TYPE
        var SERVER_TYPE
        function get(options ?:any):msgObj.LoadServerList4GMReq;
        function encode(o, b):any;
    }

// 加载服务器列表反馈(gm专用)
    module LoadServerList4GMRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 检测并使用礼品码
    module CheckGiftCodeAndUseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        var SERVER_TYPE
        function get(options ?:any):msgObj.CheckGiftCodeAndUseReq;
        function encode(o, b):any;
    }

// 检测并使用礼品码
    module CheckGiftCodeAndUseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        var SERVER_TYPE
        function decode(o, b):any;

    }

// 中心服ping值检测
    module CheckPingTimeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        var SERVER_TYPE
        function get(options ?:any):msgObj.CheckPingTimeReq;
        function encode(o, b):any;
    }

// 中心服ping值检测
    module CheckPingTimeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】查看排位赛王者大师的排行
    module GetRankingPaiHangInfoCrossServerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        var SERVER_TYPE
        function get(options ?:any):msgObj.GetRankingPaiHangInfoCrossServerReq;
        function encode(o, b):any;
    }

// 【反馈】查看排位赛王者大师的排行
    module GetRankingPaiHangInfoCrossServerRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        var SERVER_TYPE
        function decode(o, b):any;

    }

// [请求]忍蛙面板
    module RenWaPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenWaPanelReq;
        function encode(o, b):any;
    }

// [返回]忍蛙面板
    module RenWaPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍蛙升级面板
    module RenWaUpPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenWaUpPanelReq;
        function encode(o, b):any;
    }

// [返回]忍蛙升级面板
    module RenWaUpPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍蛙升级
    module GetRenWaUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRenWaUpReq;
        function encode(o, b):any;
    }

// [返回]忍蛙升级
    module GetRenWaUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍村建筑升级面板
    module RenCunJianZhuUpPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenCunJianZhuUpPanelReq;
        function encode(o, b):any;
    }

// [返回]忍村建筑升级面板
    module RenCunJianZhuUpPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍村建筑升级
    module GetRenCunJianZhuUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRenCunJianZhuUpReq;
        function encode(o, b):any;
    }

// [返回]忍村建筑升级
    module GetRenCunJianZhuUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍村建筑技能面板
    module RenCunJianZhuSkillPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenCunJianZhuSkillPanelReq;
        function encode(o, b):any;
    }

// [返回]忍村建筑技能面板
    module RenCunJianZhuSkillPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]建筑技能升级
    module GetJianZhuSkillUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetJianZhuSkillUpReq;
        function encode(o, b):any;
    }

// [返回]建筑技能升级
    module GetJianZhuSkillUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]战斗准备界面
    module FightReadyPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightReadyPanelReq;
        function encode(o, b):any;
    }

// [返回]战斗准备界面
    module FightReadyPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]保卫战斗开始
    module GetBaoWeiFightStartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBaoWeiFightStartReq;
        function encode(o, b):any;
    }

// [返回]保卫战斗开始
    module GetBaoWeiFightStartRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]保卫忍村每日奖励面板
    module MeiRiJiangLiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MeiRiJiangLiPanelReq;
        function encode(o, b):any;
    }

// [返回]保卫忍村每日奖励面板
    module MeiRiJiangLiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取每日奖励
    module GetMeiRiJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMeiRiJiangLiReq;
        function encode(o, b):any;
    }

// [返回]领取每日奖励
    module GetMeiRiJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开宝箱
    module GetBaoXiangRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBaoXiangRewardReq;
        function encode(o, b):any;
    }

// [返回]开宝箱
    module GetBaoXiangRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]扫荡上一层
    module RenCunSaoDangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenCunSaoDangReq;
        function encode(o, b):any;
    }

// [返回]扫荡上一层
    module RenCunSaoDangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍村战斗校验
    module RenCunFightCheckReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenCunFightCheckReq;
        function encode(o, b):any;
    }

// [返回]忍村战斗校验
    module RenCunFightCheckRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍村红点
    module RenCunRedPointReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenCunRedPointReq;
        function encode(o, b):any;
    }

// [返回]忍村红点
    module RenCunRedPointRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍蛙的属性详情
    module RenWaAllShuXingListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenWaAllShuXingListReq;
        function encode(o, b):any;
    }

// [返回]忍蛙的属性详情
    module RenWaAllShuXingListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍蛙新面板
    module RenWaPanelNewReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RenWaPanelNewReq;
        function encode(o, b):any;
    }

// [返回]忍蛙的属性详情
    module RenWaPanelNewRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]忍蛙升级
    module GetRenWaLevelUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRenWaLevelUpReq;
        function encode(o, b):any;
    }

// [返回]忍蛙升级
    module GetRenWaLevelUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]所有忍蛙type
    module GetAllRenWaTypeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAllRenWaTypeReq;
        function encode(o, b):any;
    }

// [返回]所有忍蛙type
    module GetAllRenWaTypeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】英雄状态改变的英雄列表
    module ChangedHeroListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】临时背包状态改变的列表
    module ChangedTempBeibaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】道具状态改变的列表
    module ChangedDaojuListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家用户信息变更推送
    module ChangedUserRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】战阵碎片状态改变的列表
    module ChangedZhanzhenSuipianListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】战阵状态改变的列表
    module ChangedZhanzhenListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】装备状态改变的列表
    module ChangedZhuangbeiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】武将转化为残魂
    module ChangedHeroToSoulRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】各频道公告推送
    module PushBroadcastRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】比武场比武战斗CD相关信息
    module PushBiwuFightCDInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】聊天信息
    module PushChatMessageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】财神殿挂机，推送已打过的关卡
    module PushCsdGuajiMessageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】有任务完成可以领取奖励了
    module PushMissionDoGetRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】IP地址
    module PushIPRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】匹配战获取奖励或开始结束信息的推送
    module PushPipeiZhanInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩家开启的功能列表
    module PushPlayerFunctionsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩家订单支付成功告知
    module PushOrderPayOKRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】通知玩家已在其它地方登陆
    module PushPlayerMultiLoginRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】9899特价类充值活动
    module PushYiyuantejiaInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]在主城循环发的红点
    module PushLoopRedPointReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PushLoopRedPointReq;
        function encode(o, b):any;
    }

// [反馈]在主城循环发的红点    false 不显示红点    true  显示红点
    module PushLoopRedPointRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]推送神器改变
    module ChangedShenqiListBackRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】推送恋爱类型的任务  登录的时候 推送  完成恋爱类型的任务 推送
    module PushLianAiMissionListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】跳动提示
    module PushTiaoDongTiShiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩吧交互
    module PushWanBaJaoHuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 种族值改变突破信息
    module ChangedZhongZuZhiTuPoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】宠物改变的英雄列表
    module ChangedFollowPetListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 推送今天0点和明天0点的时间点
    module PushTimeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 推送发生变化的数据
    module PushChangedCurrencyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 登录时推送玩家基本信息
    module PushPlayerBasicInfoAtLoginReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PushPlayerBasicInfoAtLoginReq;
        function encode(o, b):any;
    }

// 登录时推送玩家基本信息
    module PushPlayerBasicInfoAtLoginRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩家优惠券订单支付成功告知
    module PushYouHuiQuanPayOKRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取假公告数据
    module GetFakeGongGaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFakeGongGaoListReq;
        function encode(o, b):any;
    }

// [反馈]获取假公告数据
    module GetFakeGongGaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [反馈]实时广播房间玩家信息
    module BroadBossRoomPlayerInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]目标玩家是否跟我同服
    module GetPlayerIsTheSameServerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerIsTheSameServerReq;
        function encode(o, b):any;
    }

// [反馈]目标玩家是否跟我同服
    module GetPlayerIsTheSameServerRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [反馈]第一次获得精灵
    module PushFirstGetHeroShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]复仇页面的对手上线了
    module PushYourEnemyLoginRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]自动激活的Project通知
    module PushAutoActiveProjectInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]资源礼包的通知
    module PushResourceGiftActivityRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]特殊累计充值展示
    module PushCumulativeRechargeDisplayRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备面板
    module ZhuJueZhuangBeiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuJueZhuangBeiPanelReq;
        function encode(o, b):any;
    }

// [返回]主角装备面板
    module ZhuJueZhuangBeiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备强化
    module GetZhuangBeiQiangHuaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuangBeiQiangHuaReq;
        function encode(o, b):any;
    }

// [返回]主角装备强化
    module GetZhuangBeiQiangHuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]灵魂石升级
    module GetLingHunShiLvUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLingHunShiLvUpReq;
        function encode(o, b):any;
    }

// [返回]灵魂石升级
    module GetLingHunShiLvUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]操作装备或者灵魂石
    module ChangeEquipOrItemReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeEquipOrItemReq;
        function encode(o, b):any;
    }

// [返回]操作装备或者灵魂石
    module ChangeEquipOrItemRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]链接精灵
    module GetLinkHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLinkHeroReq;
        function encode(o, b):any;
    }

// [返回]链接精灵
    module GetLinkHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备背包
    module ZhuJueEquipBagReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuJueEquipBagReq;
        function encode(o, b):any;
    }

// [返回]主角装备背包
    module ZhuJueEquipBagRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备详情面板
    module ZhuJueEquipDetailPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuJueEquipDetailPanelReq;
        function encode(o, b):any;
    }

// [返回]装备详情面板
    module ZhuJueEquipDetailPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备强化面板
    module ZhuJueEquipQiangHuaPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuJueEquipQiangHuaPanelReq;
        function encode(o, b):any;
    }

// [返回]主角装备强化面板
    module ZhuJueEquipQiangHuaPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]链接宠的信息发生改变
    module ChangeLinkHeroInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeLinkHeroInfoReq;
        function encode(o, b):any;
    }

// [返回]链接宠的信息发生改变
    module ChangeLinkHeroInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备红点信息
    module GetZhuJueEquipRedPointReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuJueEquipRedPointReq;
        function encode(o, b):any;
    }

// [返回]主角装备红点信息
    module GetZhuJueEquipRedPointRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备重铸面板
    module ZhuJueEquipChongZhuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuJueEquipChongZhuPanelReq;
        function encode(o, b):any;
    }

// [返回]主角装备重铸面板
    module ZhuJueEquipChongZhuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备重铸
    module GetZhuJueEquipChongZhuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuJueEquipChongZhuReq;
        function encode(o, b):any;
    }

// [返回]主角装备重铸
    module GetZhuJueEquipChongZhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备重铸结果操作
    module OperateChongZhuResultReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OperateChongZhuResultReq;
        function encode(o, b):any;
    }

// [返回]主角装备重铸
    module OperateChongZhuResultRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角装备一键重铸
    module ZhuJueEquipOneKeyChongZhuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuJueEquipOneKeyChongZhuReq;
        function encode(o, b):any;
    }

// [返回]主角装备一键重铸
    module ZhuJueEquipOneKeyChongZhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]操作一键重铸结果
    module OperateEquipOneKeyResultReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OperateEquipOneKeyResultReq;
        function encode(o, b):any;
    }

// [返回]操作一键重铸结果
    module OperateEquipOneKeyResultRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]停止一键重铸
    module StopOneKeyChongZhuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.StopOneKeyChongZhuReq;
        function encode(o, b):any;
    }

// [返回]停止一键重铸
    module StopOneKeyChongZhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]符文面板
    module GetFuWenPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFuWenPanelReq;
        function encode(o, b):any;
    }

// [返回]符文面板
    module GetFuWenPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]符文背包
    module GetFuWenPacketReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFuWenPacketReq;
        function encode(o, b):any;
    }

// [返回]符文背包
    module GetFuWenPacketRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]分解符文预览
    module BreakFuWenYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BreakFuWenYuLanReq;
        function encode(o, b):any;
    }

// [返回]分解符文预览
    module BreakFuWenYuLanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]符文商店
    module FuWenShopReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FuWenShopReq;
        function encode(o, b):any;
    }

// [返回]符文商店
    module FuWenShopRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]符文购买
    module FuWenBuyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FuWenBuyReq;
        function encode(o, b):any;
    }

// [返回]符文购买
    module FuWenBuyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]符文镶嵌
    module FuWenEquipReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FuWenEquipReq;
        function encode(o, b):any;
    }

// [返回]符文镶嵌
    module FuWenEquipRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]分解符文
    module BreakFuWenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BreakFuWenReq;
        function encode(o, b):any;
    }

// [返回]分解符文
    module BreakFuWenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]宅急送面板
    module ZJSPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSPanelReq;
        function encode(o, b):any;
    }

// [返还]宅急送面板
    module ZJSPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]刷新星级
    module ZJSFreshReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSFreshReq;
        function encode(o, b):any;
    }

// [返还]刷新星级
    module ZJSFreshRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]查看当前状态
    module ZJSStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSStatusReq;
        function encode(o, b):any;
    }

// [返还]查看当前状态
    module ZJSStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队界面
    module ZJSZuDuiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSZuDuiPanelReq;
        function encode(o, b):any;
    }

// [返还]组队界面
    module ZJSZuDuiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]护送者进入退出队伍
    module ZJSpersonActReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSpersonActReq;
        function encode(o, b):any;
    }

// [返还]护送者进入退出队伍
    module ZJSpersonActRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]踢人
    module ZJSKickReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSKickReq;
        function encode(o, b):any;
    }

// [返还]踢人
    module ZJSKickRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开始配送
    module ZJSPeiSongStartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSPeiSongStartReq;
        function encode(o, b):any;
    }

// [返还]开始配送
    module ZJSPeiSongStartRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]坐骑界面
    module ZJSZuoQiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSZuoQiPanelReq;
        function encode(o, b):any;
    }

// [返还]坐骑界面
    module ZJSZuoQiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]坐骑切换
    module ZJSZuoQiChangeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSZuoQiChangeReq;
        function encode(o, b):any;
    }

// [返还]坐骑切换
    module ZJSZuoQiChangeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备界面
    module ZJSEquipPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSEquipPanelReq;
        function encode(o, b):any;
    }

// [返还]装备界面
    module ZJSEquipPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备强化
    module ZJSEquipUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSEquipUpReq;
        function encode(o, b):any;
    }

// [返还]装备强化
    module ZJSEquipUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]配送界面
    module ZJSPeiSongPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSPeiSongPanelReq;
        function encode(o, b):any;
    }

// [返还]开始配送
    module ZJSPeiSongPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取新的玩家组
    module ZJSOthersReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSOthersReq;
        function encode(o, b):any;
    }

// [返还]获取新的玩家组
    module ZJSOthersRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取抢夺对手
    module ZJSgetTargetsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSgetTargetsReq;
        function encode(o, b):any;
    }

// [返还]获取抢夺对手
    module ZJSgetTargetsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买抢夺次数
    module ZJSBuyQiangDuoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSBuyQiangDuoReq;
        function encode(o, b):any;
    }

// [返还]购买抢夺次数
    module ZJSBuyQiangDuoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抢夺可得奖励
    module ZJSQiangDuoJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSQiangDuoJiangLiReq;
        function encode(o, b):any;
    }

// [返回]抢夺可得奖励
    module ZJSQiangDuoJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抢夺某人
    module ZJSQiangDuoFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSQiangDuoFightReq;
        function encode(o, b):any;
    }

// [返回]抢夺结果
    module ZJSQiangDuoFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]查看历史记录
    module ZJSShowHistroyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSShowHistroyReq;
        function encode(o, b):any;
    }

// [返回]查看历史记录
    module ZJSShowHistroyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]查看历史记录
    module ZJSFightLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSFightLogReq;
        function encode(o, b):any;
    }

// [返回]查看历史记录
    module ZJSFightLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]双倍时间红点提示
    module ZJSTipInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZJSTipInfoReq;
        function encode(o, b):any;
    }

// [返回]双倍时间红点提示
    module ZJSTipInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 宅急送喊话邀请
    module ZhaiJiSongHanHuaYaoQingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhaiJiSongHanHuaYaoQingReq;
        function encode(o, b):any;
    }

// 宅急送喊话邀请
    module ZhaiJiSongHanHuaYaoQingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 宅急送扫荡
    module SweepZhaijisongReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepZhaijisongReq;
        function encode(o, b):any;
    }

// 宅急送扫荡
    module SweepZhaijisongRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]寻宝乐园面板
    module XunBaoLeYuanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.XunBaoLeYuanPanelReq;
        function encode(o, b):any;
    }

// [返回]寻宝乐园面板
    module XunBaoLeYuanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]摇奖
    module XunBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.XunBaoReq;
        function encode(o, b):any;
    }

// [返回]寻宝乐园面板
    module XunBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取神宠信息
    module GetShenChongInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShenChongInfoReq;
        function encode(o, b):any;
    }

// 获取神宠信息
    module GetShenChongInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开神宠升级界面
    module OpenShenChongLvUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenShenChongLvUpReq;
        function encode(o, b):any;
    }

// 打开神宠升级界面
    module OpenShenChongLvUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 神宠升级
    module ShenChongLvUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenChongLvUpReq;
        function encode(o, b):any;
    }

// 神宠升级
    module ShenChongLvUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 神宠激活
    module ShenChongActivateReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenChongActivateReq;
        function encode(o, b):any;
    }

// 神宠激活
    module ShenChongActivateRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开神宠进阶
    module OpenShenChongUpgradeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenShenChongUpgradeReq;
        function encode(o, b):any;
    }

// 打开神宠进阶
    module OpenShenChongUpgradeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 神宠进阶
    module ShenChongUpgradeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenChongUpgradeReq;
        function encode(o, b):any;
    }

// 神宠提升(进阶或激活)
    module ShenChongUpgradeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开神宠选择或切换界面
    module OpenShenChongSelectReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenShenChongSelectReq;
        function encode(o, b):any;
    }

// 打开神宠选择或切换界面
    module OpenShenChongSelectRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 神宠系统别选择或切换
    module ShenChongSelectReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenChongSelectReq;
        function encode(o, b):any;
    }

// 神宠系统别选择或切换
    module ShenChongSelectRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 出战哪只宠物
    module EquipShenChongReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EquipShenChongReq;
        function encode(o, b):any;
    }

// 出战哪只宠物
    module EquipShenChongRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]骑技面板
    module QiJiTuPoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QiJiTuPoPanelReq;
        function encode(o, b):any;
    }

// [返回]骑技面板
    module QiJiTuPoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]骑技突破
    module QiJiTuPoTuPoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QiJiTuPoTuPoReq;
        function encode(o, b):any;
    }

// [返回]骑技面板
    module QiJiTuPoTuPoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]骑技突破  满级预览
    module QiJiTuPoManJieYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QiJiTuPoManJieYuLanReq;
        function encode(o, b):any;
    }

// [请求]骑技突破  满级预览
    module QiJiTuPoManJieYuLanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取充值金额列表
    module GetChongzhiJineListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChongzhiJineListReq;
        function encode(o, b):any;
    }

// 【反馈】获取充值金额列
    module GetChongzhiJineListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】点击充值
    module DoGetChongzhiJineReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetChongzhiJineReq;
        function encode(o, b):any;
    }

// 【反馈】点击充值
    module DoGetChongzhiJineRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】点击购买月卡
    module BuyYueKaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyYueKaReq;
        function encode(o, b):any;
    }

// 【反馈】点击购买月卡
    module BuyYueKaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】首冲
    module GetChongzhiShouciNewReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChongzhiShouciNewReq;
        function encode(o, b):any;
    }

// 【反馈】首冲
    module GetChongzhiShouciNewRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取首冲四倍奖励
    module GetChongzhiShouciNewRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChongzhiShouciNewRewardReq;
        function encode(o, b):any;
    }

// 【反馈】领取首冲四倍奖励
    module GetChongzhiShouciNewRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】发起充值,通用接口
    module ChargeStartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChargeStartReq;
        function encode(o, b):any;
    }

// 【反馈】发起充值,通用接口
    module ChargeStartRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】腾讯开放平台发起充值
    module ChargeStartQQReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChargeStartQQReq;
        function encode(o, b):any;
    }

// 【反馈】腾讯开放平台发起充值
    module ChargeStartQQRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩吧充值
    module ChargeStartQZoneReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChargeStartQZoneReq;
        function encode(o, b):any;
    }

// 【反馈】玩吧充值
    module ChargeStartQZoneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】QQ游戏大厅充值
    module ChargeStartQQGameReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChargeStartQQGameReq;
        function encode(o, b):any;
    }

// 【反馈】QQ游戏大厅充值
    module ChargeStartQQGameRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取充值代币列表
    module ChongZhiDaiBiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChongZhiDaiBiListReq;
        function encode(o, b):any;
    }

// 【反馈】获取充值代币列表
    module ChongZhiDaiBiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】金券购买
    module JinQuanGouMaiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JinQuanGouMaiReq;
        function encode(o, b):any;
    }

// 【反馈】金券购买
    module JinQuanGouMaiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】改变充值发货状态
    module UpdateChongZhiFaHuoStyleReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateChongZhiFaHuoStyleReq;
        function encode(o, b):any;
    }

// 【请求】收到Sdk方的支付成功消息
    module PayOrderSuccessReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PayOrderSuccessReq;
        function encode(o, b):any;
    }

// 【推送】推送引导礼包
    module PushGuildGiftPackInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取引导礼包奖励
    module GetGuildGiftPackRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGuildGiftPackRewardReq;
        function encode(o, b):any;
    }

// [返回]领取引导礼包奖励
    module GetGuildGiftPackRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】发起支付时追加额外信息例如支付方式等
    module AddExtendInfoForChargeStartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AddExtendInfoForChargeStartReq;
        function encode(o, b):any;
    }

// [请求]加载等级-vip礼包数据
    module LoadDengJiVipLiBaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadDengJiVipLiBaoListReq;
        function encode(o, b):any;
    }

// [反馈]加载等级-vip礼包数据
    module LoadDengJiVipLiBaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取等级-vip礼包
    module GetDengJiVipLiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDengJiVipLiBaoReq;
        function encode(o, b):any;
    }

// [反馈]领取等级-vip礼包
    module GetDengJiVipLiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]砸金蛋面板
    module LoadZaJinDanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadZaJinDanPanelReq;
        function encode(o, b):any;
    }

// [返回]砸金蛋面板
    module LoadZaJinDanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]砸金蛋
    module GetZaJinDanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZaJinDanReq;
        function encode(o, b):any;
    }

// [返回]砸金蛋
    module GetZaJinDanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买锤子
    module GouMaiChuiZiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiChuiZiReq;
        function encode(o, b):any;
    }

// [返回]购买锤子
    module GouMaiChuiZiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 加载周末在线礼包
    module LoadWeekendZaiXianJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadWeekendZaiXianJiangLiReq;
        function encode(o, b):any;
    }

// [返回]加载周末在线礼包
    module LoadWeekendZaiXianJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取周末在线礼包
    module GetWeekendZaiXianJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWeekendZaiXianJiangLiReq;
        function encode(o, b):any;
    }

// [返回]领取周末在线礼包
    module GetWeekendZaiXianJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 记载周末登录礼包
    module LoadWeekendLoginJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadWeekendLoginJiangLiReq;
        function encode(o, b):any;
    }

// [返回]加载周末登录礼包
    module LoadWeekendLoginJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 预览周末登陆礼包
    module ShowWeekendLoginJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowWeekendLoginJiangLiReq;
        function encode(o, b):any;
    }

// [返回]预览周末登陆礼包
    module ShowWeekendLoginJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取周末登录礼包
    module GetWeekendLoginJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWeekendLoginJiangLiReq;
        function encode(o, b):any;
    }

// [返回]领取周末登录礼包
    module GetWeekendLoginJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]刮刮乐面板
    module LoadGuaGuaLePanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadGuaGuaLePanelReq;
        function encode(o, b):any;
    }

// [返回]刮刮乐面板
    module LoadGuaGuaLePanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]刮刮乐
    module GetGuaGuaLeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGuaGuaLeReq;
        function encode(o, b):any;
    }

// [返回]刮刮乐
    module GetGuaGuaLeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买刮刮刀
    module GouMaiGuaGuaDaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiGuaGuaDaoReq;
        function encode(o, b):any;
    }

// [返回]购买刮刮刀
    module GouMaiGuaGuaDaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]老玩家回归面板
    module LaoWanJiaHuiGuiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LaoWanJiaHuiGuiPanelReq;
        function encode(o, b):any;
    }

// [返回]老玩家回归面板
    module LaoWanJiaHuiGuiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取老玩家回归奖励
    module GetLaoWanJiaHuiGuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLaoWanJiaHuiGuiReq;
        function encode(o, b):any;
    }

// [返回]领取老玩家回归奖励
    module GetLaoWanJiaHuiGuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跨服积分赛公会统计
    module GetGongHuiTongJiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiTongJiReq;
        function encode(o, b):any;
    }

// [返回]跨服积分赛公会统计
    module GetGongHuiTongJiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]道具消耗返利面板
    module DaoJuXiaoHaoFanLiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DaoJuXiaoHaoFanLiPanelReq;
        function encode(o, b):any;
    }

// [返回]道具消耗返利面板
    module DaoJuXiaoHaoFanLiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取道具消耗奖励
    module GetDaoJuXiaoHaoRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDaoJuXiaoHaoRewardReq;
        function encode(o, b):any;
    }

// [返回]领取道具消耗奖励
    module GetDaoJuXiaoHaoRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]召唤皮神面板
    module LoadZhaoHuanPiShenPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadZhaoHuanPiShenPanelReq;
        function encode(o, b):any;
    }

// [返回]召唤皮神面板
    module LoadZhaoHuanPiShenPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]召唤皮神
    module GetZhaoHuanPiShenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhaoHuanPiShenReq;
        function encode(o, b):any;
    }

// [返回]召唤皮神
    module GetZhaoHuanPiShenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买召唤卡
    module GouMaiZhaoHuanKaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiZhaoHuanKaReq;
        function encode(o, b):any;
    }

// [返回]购买召唤卡
    module GouMaiZhaoHuanKaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]道具互换界面
    module DaoJuHuHuanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DaoJuHuHuanPanelReq;
        function encode(o, b):any;
    }

// [返回]道具互换界面
    module DaoJuHuHuanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]进行道具互换
    module GetDaoJuHuHuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDaoJuHuHuanReq;
        function encode(o, b):any;
    }

// [返回]进行道具互换
    module GetDaoJuHuHuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]每日团购界面
    module MeiRituanGouPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MeiRituanGouPanelReq;
        function encode(o, b):any;
    }

// [返回]每日团购界面
    module MeiRituanGouPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买团购礼包
    module GetMeiRituanGouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMeiRituanGouReq;
        function encode(o, b):any;
    }

// [返回]购买团购礼包
    module GetMeiRituanGouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取礼包额外奖励
    module GetOtherRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetOtherRewardReq;
        function encode(o, b):any;
    }

// [返回]获取礼包额外奖励
    module GetOtherRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]怪物来袭面板
    module LoadGuaiWuLaiXiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadGuaiWuLaiXiPanelReq;
        function encode(o, b):any;
    }

// [返回]怪物来袭面板
    module LoadGuaiWuLaiXiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]杀怪
    module KillGuaiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KillGuaiWuReq;
        function encode(o, b):any;
    }

// [返回]杀怪
    module KillGuaiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买道具
    module GouMaiKillToolReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiKillToolReq;
        function encode(o, b):any;
    }

// [返回]购买道具
    module GouMaiKillToolRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取排行榜
    module GuaiWuLaiXiRankReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaiWuLaiXiRankReq;
        function encode(o, b):any;
    }

// [返回]购买道具
    module GuaiWuLaiXiRankRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]变化的活动数据
    module PushChangeNewActivityRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]节日基金面板
    module LoadJieRiJiJinPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadJieRiJiJinPanelReq;
        function encode(o, b):any;
    }

// [返回]]节日基金面板
    module LoadJieRiJiJinPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]节日基金套现
    module GetJieRiJiJinRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetJieRiJiJinRewardReq;
        function encode(o, b):any;
    }

// [返回]节日基金套现
    module GetJieRiJiJinRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]节日基金投资
    module BuyJieRiJiJinReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyJieRiJiJinReq;
        function encode(o, b):any;
    }

// [返回]节日基金投资
    module BuyJieRiJiJinRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]招财猫面板
    module GetZhaoCaiMaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhaoCaiMaoPanelReq;
        function encode(o, b):any;
    }

// [返回]招财猫面板
    module GetZhaoCaiMaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]招财
    module GetZhaoCaiMaoRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhaoCaiMaoRewardReq;
        function encode(o, b):any;
    }

// [返回]招财
    module GetZhaoCaiMaoRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】七日任务
    module OpenSevenDayTaskReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenSevenDayTaskReq;
        function encode(o, b):any;
    }

// 【反馈】七日任务
    module OpenSevenDayTaskRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取七日任务奖励
    module SevenDayTaskReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SevenDayTaskReq;
        function encode(o, b):any;
    }

// 【反馈】领取七日任务奖励
    module SevenDayTaskRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]充值一冲选领
    module ChongZhiYiChongXuanLingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChongZhiYiChongXuanLingReq;
        function encode(o, b):any;
    }

// [反馈]领取一冲选领
    module ChongZhiYiChongXuanLingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]挑战神宠界面
    module TiaoZhanShenShouPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TiaoZhanShenShouPanelReq;
        function encode(o, b):any;
    }

// [反馈]挑战神宠界面
    module TiaoZhanShenShouPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]挑战神宠
    module TiaoZhanShenShouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TiaoZhanShenShouReq;
        function encode(o, b):any;
    }

// [反馈]挑战神宠
    module TiaoZhanShenShouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]嘉年华面板
    module LoadJiaNianHuaPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadJiaNianHuaPanelReq;
        function encode(o, b):any;
    }

// [返回]嘉年华面板
    module LoadJiaNianHuaPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取嘉年华奖励
    module GetJiaNiaNianHuaRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetJiaNiaNianHuaRewardReq;
        function encode(o, b):any;
    }

// [请求]获取嘉年华奖励
    module GetJiaNiaNianHuaRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]嘉年华商店面板
    module LoadJiaNianHuaShopPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadJiaNianHuaShopPanelReq;
        function encode(o, b):any;
    }

// [返回]嘉年华商店面板
    module LoadJiaNianHuaShopPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取嘉年华买道具
    module BuyJiaNiaNianHuaItemReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyJiaNiaNianHuaItemReq;
        function encode(o, b):any;
    }

// [请求]获取嘉年华买道具
    module BuyJiaNiaNianHuaItemRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]连续充值活动面板
    module LianXuChongZhiHuoDongPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LianXuChongZhiHuoDongPanelReq;
        function encode(o, b):any;
    }

// [请求]连续充值活动面板
    module LianXuChongZhiHuoDongPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]连续充值活动面板
    module GetLianXuChongZhiHuoDongRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLianXuChongZhiHuoDongRewardReq;
        function encode(o, b):any;
    }

// [请求]领取连续充值活动奖励
    module GetLianXuChongZhiHuoDongRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]神宠基金列表
    module ShenChongJiJinListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenChongJiJinListReq;
        function encode(o, b):any;
    }

// [反馈]神宠基金列表
    module ShenChongJiJinListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取神宠基金
    module GetShenChongJiJinReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShenChongJiJinReq;
        function encode(o, b):any;
    }

// [反馈]领取神宠基金
    module GetShenChongJiJinRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买神宠基金
    module BuyShenChongJiJinReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyShenChongJiJinReq;
        function encode(o, b):any;
    }

// [反馈]购买神宠基金
    module BuyShenChongJiJinRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载超值返利数据
    module LoadChaoZhiFanLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadChaoZhiFanLiReq;
        function encode(o, b):any;
    }

// [反馈]加载超值返利数据
    module LoadChaoZhiFanLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取超值返利奖励
    module ReceiveChaoZhiFanLiRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReceiveChaoZhiFanLiRewardReq;
        function encode(o, b):any;
    }

// [反馈]领取超值返利奖励
    module ReceiveChaoZhiFanLiRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跨服砸金蛋面板
    module LoadKuaFuZaJinDanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadKuaFuZaJinDanPanelReq;
        function encode(o, b):any;
    }

// [返回]跨服砸金蛋面板
    module LoadKuaFuZaJinDanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跨服砸金蛋
    module GetKuaFuZaJinDanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetKuaFuZaJinDanReq;
        function encode(o, b):any;
    }

// [返回]跨服砸金蛋
    module GetKuaFuZaJinDanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跨服砸金蛋排行榜
    module GetKuaFuZaJinDanRankingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetKuaFuZaJinDanRankingReq;
        function encode(o, b):any;
    }

// [返回]跨服砸金蛋排行榜
    module GetKuaFuZaJinDanRankingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】挖宝石
    module DigBaoshiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DigBaoshiReq;
        function encode(o, b):any;
    }

// 【反馈】挖宝石
    module DigBaoshiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】挖高级宝石
    module DigHighBaoshiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DigHighBaoshiReq;
        function encode(o, b):any;
    }

// 【反馈】挖高级宝石
    module DigHighBaoshiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】挖顶级宝石
    module DigTopLevelBaoshiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DigTopLevelBaoshiReq;
        function encode(o, b):any;
    }

// 【反馈】挖顶级宝石
    module DigTopLevelBaoshiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】宝石转换
    module TransformBaoShiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TransformBaoShiReq;
        function encode(o, b):any;
    }

// 【反馈】宝石转换
    module TransformBaoShiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载公会战地图相关信息
    module LoadGongHuiZhanSceneDataReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadGongHuiZhanSceneDataReq;
        function encode(o, b):any;
    }

// 【反馈】加载公会战地图相关信息
    module LoadGongHuiZhanSceneDataRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载加载公会战活动数据
    module LoadGongHuiZhanDataReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadGongHuiZhanDataReq;
        function encode(o, b):any;
    }

// 【反馈】加载或推送加载公会战活动数据
    module LoadGongHuiZhanDataRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】公会战鼓舞
    module GongHuiZhanGuWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiZhanGuWuReq;
        function encode(o, b):any;
    }

// 【反馈】公会战鼓舞
    module GongHuiZhanGuWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取公会战鼓舞信息
    module GetGongHuiZhanGuWuInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiZhanGuWuInfoReq;
        function encode(o, b):any;
    }

// 【反馈】获取公会战鼓舞信息
    module GetGongHuiZhanGuWuInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载战场排行数据
    module LoadGongHuiZhanRankReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadGongHuiZhanRankReq;
        function encode(o, b):any;
    }

// 【反馈】加载战场排行数据
    module LoadGongHuiZhanRankRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 请求公会战奖励
    module GetGongHuiZhanRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiZhanRewardReq;
        function encode(o, b):any;
    }

// 请求公会战奖励
    module GetGongHuiZhanRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】公会战排行奖励
    module GetGongHuiZhanRankRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiZhanRankRewardReq;
        function encode(o, b):any;
    }

// 【反馈】公会战排行奖励
    module GetGongHuiZhanRankRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】扩展玩家物件空间
    module BuySpaceReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuySpaceReq;
        function encode(o, b):any;
    }

// 【反馈】返回扩展玩家物件空间
    module BuySpaceRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家道具列表
    module GetPlayerDaojuListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerDaojuListReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家道具列表
    module GetPlayerDaojuListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】卖出道具
    module SellDaojuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SellDaojuReq;
        function encode(o, b):any;
    }

// 【反馈】卖出道具
    module SellDaojuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】使用道具（根据F_SCRIPT_ID做相应操作）
    module OpenDaojuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenDaojuReq;
        function encode(o, b):any;
    }

// 【反馈】使用道具（根据F_SCRIPT_ID做相应操作）
    module OpenDaojuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】一键碎片合成
    module HeChengJingLingOneKeyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeChengJingLingOneKeyReq;
        function encode(o, b):any;
    }

// 【反馈】一键碎片合成
    module HeChengJingLingOneKeyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 拆分宝石
    module ChaiFenBaoShiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChaiFenBaoShiReq;
        function encode(o, b):any;
    }

// 【反馈】拆分宝石
    module ChaiFenBaoShiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 快速购买道具
    module BuyItemQuickReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyItemQuickReq;
        function encode(o, b):any;
    }

// 快速购买道具
    module BuyItemQuickRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取临时背包物品
    module GetTempBeibaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTempBeibaoReq;
        function encode(o, b):any;
    }

// 【反馈】领取临时背包物品
    module GetTempBeibaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取任务类道具的任务条件进度值
    module GetMissionCountForItemReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMissionCountForItemReq;
        function encode(o, b):any;
    }

// 【反馈】获取任务类道具的任务条件进度值
    module GetMissionCountForItemRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取优惠券列表
    module GetYouHuiQuanListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetYouHuiQuanListReq;
        function encode(o, b):any;
    }

// 【反馈】获取优惠券列表
    module GetYouHuiQuanListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】使用优惠券
    module UseYouHuiQuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UseYouHuiQuanReq;
        function encode(o, b):any;
    }

// 【反馈】使用优惠券
    module UseYouHuiQuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】战斗pve普通、pve精英
    module FightPVEReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightPVEReq;
        function encode(o, b):any;
    }

// 【反馈】战斗pve普通、pve精英
    module FightPVERsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家已通过小关列表
    module GetPlayerXiaoguanListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerXiaoguanListReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家已通过小关列表
    module GetPlayerXiaoguanListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家已通过大关列表
    module GetPlayerDaguanListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerDaguanListReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家已通过大关列表
    module GetPlayerDaguanListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】关卡--扫荡：小关、大关 
    module SweepFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepFightReq;
        function encode(o, b):any;
    }

// 【反馈】关卡--扫荡：小关、大关、装备副本
    module SweepFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】关卡--重置某一章节的所有精英关卡
    module ResetDaguanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ResetDaguanReq;
        function encode(o, b):any;
    }

// 【反馈】关卡--重置精英关卡
    module ResetDaguanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买挑战次数
    module BuyChallengeTimesReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyChallengeTimesReq;
        function encode(o, b):any;
    }

// 【反馈】购买挑战次数
    module BuyChallengeTimesRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买挑战次数
    module TongYiFightCountReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TongYiFightCountReq;
        function encode(o, b):any;
    }

// 【反馈】购买挑战次数
    module TongYiFightCountRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 装备副本战斗
    module ZhuangBeiFuBenFightPVEReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuangBeiFuBenFightPVEReq;
        function encode(o, b):any;
    }

// [请求] 装备副本战斗
    module ZhuangBeiFuBenFightPVERsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 获得装备副本次数
    module GetZhuangBeiFuBenCishuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuangBeiFuBenCishuReq;
        function encode(o, b):any;
    }

// [请求] 获得装备副本次数
    module GetZhuangBeiFuBenCishuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家已通过副本中心列表
    module GetFubenZhongXinListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFubenZhongXinListReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家已通过副本中心列表
    module GetFubenZhongXinListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得游戏配置数据
    module GetGameDefListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGameDefListReq;
        function encode(o, b):any;
    }

// 【反馈】获得游戏配置数据
    module GetGameDefListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 装备副本战斗
    module FuBenFightPVEReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FuBenFightPVEReq;
        function encode(o, b):any;
    }

// [请求] 装备副本战斗
    module FuBenFightPVERsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 副本中心列表
    module FuBenLieBiaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FuBenLieBiaoReq;
        function encode(o, b):any;
    }

// [请求] 副本中心列表
    module FuBenLieBiaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】关卡--扫荡：小关、大关、装备   宝石  附魔石   骑术  副本  超进化
    module SweepFightNewFuBenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepFightNewFuBenReq;
        function encode(o, b):any;
    }

// 【反馈】装备副本 宝石 等等几个副本
    module SweepFightNewFuBenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 购买副本挑战次数
    module GouMaiFuBenCiShuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiFuBenCiShuReq;
        function encode(o, b):any;
    }

// 【反馈】购买副本挑战次数
    module GouMaiFuBenCiShuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 副本扫荡消耗金券
    module FuBenSaoDangByJinQuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FuBenSaoDangByJinQuanReq;
        function encode(o, b):any;
    }

// 【反馈】副本扫荡消耗金券
    module FuBenSaoDangByJinQuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】道馆关卡Boss战斗结束后的通知
    module NoticeDaguanBossFightIsEndReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NoticeDaguanBossFightIsEndReq;
        function encode(o, b):any;
    }

// 【请求】副本通关大奖
    module LoadInstanceCompleteRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadInstanceCompleteRewardReq;
        function encode(o, b):any;
    }

// 【反馈】副本通关大奖
    module LoadInstanceCompleteRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】副本通关大奖
    module ReceiveInstanceCompleteRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReceiveInstanceCompleteRewardReq;
        function encode(o, b):any;
    }

// 【反馈】副本通关大奖
    module ReceiveInstanceCompleteRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]红包列表
    module GetHongBaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHongBaoListReq;
        function encode(o, b):any;
    }

// [反馈]红包列表
    module GetHongBaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抢红包
    module GetHongBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHongBaoReq;
        function encode(o, b):any;
    }

// [返回]抢红包
    module GetHongBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取邮件奖励
    module GetEmailRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetEmailRewardReq;
        function encode(o, b):any;
    }

// 【反馈】领取邮件奖励
    module GetEmailRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取玩家Email的列表
    module GetEmailListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetEmailListReq;
        function encode(o, b):any;
    }

// 【反馈】获取玩家Emial的列表
    module GetEmailListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]删除邮件
    module DeleteEmailReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DeleteEmailReq;
        function encode(o, b):any;
    }

// [返回]删除邮件
    module DeleteEmailRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]删除所有邮件 删除所有已经查看且没有附件的邮件
    module DeleteAllEmailReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DeleteAllEmailReq;
        function encode(o, b):any;
    }

// [返回]删除所有邮件 删除所有已经查看且没有附件的邮件
    module DeleteAllEmailRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]查看邮件
    module ViewEmailReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ViewEmailReq;
        function encode(o, b):any;
    }

// [返回]查看邮件
    module ViewEmailRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取所有邮件
    module GetAllEmailReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAllEmailReq;
        function encode(o, b):any;
    }

// [返回]领取所有邮件
    module GetAllEmailRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】向服务器发送聊天信息
    module SendMessageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SendMessageReq;
        function encode(o, b):any;
    }

// 【反馈】向服务器发送聊天信息
    module SendMessageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求聊天中英雄的信息
    module GetChatHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChatHeroReq;
        function encode(o, b):any;
    }

// 【反馈】请求聊天中英雄的信息
    module GetChatHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求聊天中装备的信息
    module GetChatZhuangbeiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChatZhuangbeiReq;
        function encode(o, b):any;
    }

// 【反馈】请求聊天中英雄的信息
    module GetChatZhuangbeiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】向所有服务器发送聊天信息
    module SendAllServerMessageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SendAllServerMessageReq;
        function encode(o, b):any;
    }

// 【反馈】向服务器发送聊天信息
    module SendAllServerMessageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】登录的时候 向服务器请求20条信息 防止玩家进来的时候 聊天窗口是空的
    module GetMessageAtLoginReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMessageAtLoginReq;
        function encode(o, b):any;
    }

// 【反馈】登录的时候 向服务器请求20条信息 防止玩家进来的时候 聊天窗口是空的
    module GetMessageAtLoginRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求聊天中英雄的信息
    module GetChatHeroCrossServerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChatHeroCrossServerReq;
        function encode(o, b):any;
    }

// 【反馈】请求聊天中英雄的信息
    module GetChatHeroCrossServerRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求聊天中装备的信息
    module GetChatZhuangbeiCrossReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChatZhuangbeiCrossReq;
        function encode(o, b):any;
    }

// 【反馈】请求聊天中英雄的信息
    module GetChatZhuangbeiCrossRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】举报广告刷屏玩家
    module ReportAdvertPlayerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReportAdvertPlayerReq;
        function encode(o, b):any;
    }

// 【反馈】举报广告刷屏玩家
    module ReportAdvertPlayerRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 用户离开聊天界面
    module LeaveChatReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LeaveChatReq;
        function encode(o, b):any;
    }

// [请求]聊天界面战斗
    module ChatFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChatFightReq;
        function encode(o, b):any;
    }

// [返回]聊天界面战斗
    module ChatFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]膜拜玩家
    module MoBaiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoBaiReq;
        function encode(o, b):any;
    }

// [返回]膜拜玩家
    module MoBaiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】私聊
    module EnterPersonalChatReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EnterPersonalChatReq;
        function encode(o, b):any;
    }

// 【反馈】私聊
    module EnterPersonalChatRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】离开私聊
    module LeavePersonalChatReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LeavePersonalChatReq;
        function encode(o, b):any;
    }

// 【反馈】有新的私聊消息到达
    module PushPersonalChatIsRedRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】删除与该玩家的私聊记录
    module ClearPersonalChatRecordsByPlayerIdReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ClearPersonalChatRecordsByPlayerIdReq;
        function encode(o, b):any;
    }

// 【请求】通知前端：删除与该玩家的私聊记录
    module ClearPersonalChatRecordsByPlayerIdRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】更新已读的ChatDBId
    module UpdatePersonalChatDBIdReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdatePersonalChatDBIdReq;
        function encode(o, b):any;
    }

// 【请求】通知前端：删除相应聊天记录
    module PushDeleteChatDbIdInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本列表
    module TanSuoFuBenListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanSuoFuBenListReq;
        function encode(o, b):any;
    }

// [返回]探索副本列表
    module TanSuoFuBenListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本采集奖励信息 
    module GetCaiJiInfoByVipReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetCaiJiInfoByVipReq;
        function encode(o, b):any;
    }

// [返回]探索副本采集奖励信息
    module GetCaiJiInfoByVipRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开启探索副本
    module OpenTanSuoFuBenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenTanSuoFuBenReq;
        function encode(o, b):any;
    }

// [返回]开启探索副本
    module OpenTanSuoFuBenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]更新人物坐标
    module UpdatePersonPositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdatePersonPositionReq;
        function encode(o, b):any;
    }

// [返回]更新人物坐标
    module UpdatePersonPositionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本阵型界面
    module TanSuoFuBenRoomPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanSuoFuBenRoomPanelReq;
        function encode(o, b):any;
    }

// [返回]阵型界面
    module TanSuoFuBenRoomPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本更换战斗精灵
    module TanSuoFuBenChangeRoomHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanSuoFuBenChangeRoomHeroReq;
        function encode(o, b):any;
    }

// [返回]探索副本更换战斗精灵
    module TanSuoFuBenChangeRoomHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本更改房间人员
    module TanSuoFuBenChangeRoomPersonReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanSuoFuBenChangeRoomPersonReq;
        function encode(o, b):any;
    }

// [返回]探索副本更改房间人员
    module TanSuoFuBenChangeRoomPersonRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本挑战
    module TanSuoFuBenFightStartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanSuoFuBenFightStartReq;
        function encode(o, b):any;
    }

// [返回]探索副本挑战
    module TanSuoFuBenFightStartRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本创建或加入房间
    module TanSuoFuBenCreateOrJoinRoomReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanSuoFuBenCreateOrJoinRoomReq;
        function encode(o, b):any;
    }

// [返回]探索副本创建或加入房间
    module TanSuoFuBenCreateOrJoinRoomRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本采集奖励
    module TanSuoFuBenCaiJiJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanSuoFuBenCaiJiJiangLiReq;
        function encode(o, b):any;
    }

// [返回]探索副本采集奖励
    module TanSuoFuBenCaiJiJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探索副本一键邀请
    module TanSuoFuBenInviteOthersReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanSuoFuBenInviteOthersReq;
        function encode(o, b):any;
    }

// [返回]探索副本一键邀请
    module TanSuoFuBenInviteOthersRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]修改探索副本战力要求
    module UpdateTanSuoZhanLiLimitReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateTanSuoZhanLiLimitReq;
        function encode(o, b):any;
    }

// [返回]修改探索副本战力要求
    module UpdateTanSuoZhanLiLimitRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]探索副本重置推送
    module PushTanSuoFuBenResetRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]一键开启探索副本
    module OpenTanSuoFuBenOneKeyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenTanSuoFuBenOneKeyReq;
        function encode(o, b):any;
    }

// [返回]一键开启探索副本
    module OpenTanSuoFuBenOneKeyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]重置探索副本
    module ResetTanSuoFuBenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ResetTanSuoFuBenReq;
        function encode(o, b):any;
    }

// [返回]重置探索副本
    module ResetTanSuoFuBenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]奇遇面板
    module QuYuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QuYuPanelReq;
        function encode(o, b):any;
    }

// [返回]奇遇面板
    module QuYuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取次数
    module GetHintCountInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHintCountInfoReq;
        function encode(o, b):any;
    }

// [返回]获取次数
    module GetHintCountInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】天空塔面板
    module TianKongTaPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TianKongTaPanelReq;
        function encode(o, b):any;
    }

// 【反馈】天空塔面板
    module TianKongTaPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取天空塔奖励
    module GetTianKongTaRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTianKongTaRewardReq;
        function encode(o, b):any;
    }

// 【反馈】领取天空塔奖励
    module GetTianKongTaRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】一键扫荡
    module SweepOneKeyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepOneKeyReq;
        function encode(o, b):any;
    }

// 【反馈】一键扫荡
    module SweepOneKeyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】天空塔战斗
    module TianKongTaFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TianKongTaFightReq;
        function encode(o, b):any;
    }

// 【反馈】天空塔战斗
    module TianKongTaFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】天空塔排行
    module GetTianKongTaPaiHangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTianKongTaPaiHangReq;
        function encode(o, b):any;
    }

// 【反馈】天空塔排行
    module GetTianKongTaPaiHangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】宠物手册信息
    module GetPetHandbookPannelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPetHandbookPannelReq;
        function encode(o, b):any;
    }

// 【反馈】宠物手册信息
    module GetPetHandbookPannelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活宠物手册
    module ActivePetHandbookReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActivePetHandbookReq;
        function encode(o, b):any;
    }

// 【反馈】激活宠物手册
    module ActivePetHandbookRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】宠物羁绊信息
    module GetPetCombinationPannelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPetCombinationPannelReq;
        function encode(o, b):any;
    }

// 【反馈】宠物羁绊信息
    module GetPetCombinationPannelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活宠物羁绊
    module ActivePetCombinationReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActivePetCombinationReq;
        function encode(o, b):any;
    }

// 【反馈】激活宠物羁绊
    module ActivePetCombinationRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽面板
    module NewGuardPetPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewGuardPetPanelReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽面板
    module NewGuardPetPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽技能信息
    module NewGuardPetSkillInfoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewGuardPetSkillInfoPanelReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽技能信息
    module NewGuardPetSkillInfoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽升级
    module GetNewGuardPetLvUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetLvUpReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽升级
    module GetNewGuardPetLvUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽技能升级
    module GetNewGuardPetSkillLvUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetSkillLvUpReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽技能升级
    module GetNewGuardPetSkillLvUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽上阵交换
    module NewGuardPetPositionChangeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewGuardPetPositionChangeReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽上阵交换
    module NewGuardPetPositionChangeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】唤醒守护兽技能
    module ActiveGuardPetSkillReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActiveGuardPetSkillReq;
        function encode(o, b):any;
    }

// 【反馈】唤醒守护兽技能
    module ActiveGuardPetSkillRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽进阶面板
    module GetNewGuardPetJinjiePanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetJinjiePanelReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽进阶面板
    module GetNewGuardPetJinjiePanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽进阶升级
    module GetNewGuardPetJinjieUpgradeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetJinjieUpgradeReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽进阶升级
    module GetNewGuardPetJinjieUpgradeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽洗练面板
    module GetNewGuardPetXilianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetXilianPanelReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽洗练面板
    module GetNewGuardPetXilianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽进阶洗练
    module GetNewGuardPetXilianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetXilianReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽进阶洗练
    module GetNewGuardPetXilianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽遣散
    module NewGuardPetSendBackReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewGuardPetSendBackReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽遣散
    module NewGuardPetSendBackRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽吃丹面板
    module GetNewGuardPetDanyaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetDanyaoPanelReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽吃丹面板
    module GetNewGuardPetDanyaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版守护兽吃丹药
    module GetNewGuardPetEatDanyaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetEatDanyaoReq;
        function encode(o, b):any;
    }

// 【反馈】新版守护兽吃丹药
    module GetNewGuardPetEatDanyaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】新版守护兽的红点推送
    module PushNewGuardPetRedPointRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新守护兽升星额外属性描述
    module GetNewGuardPetExtraDescInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewGuardPetExtraDescInfoReq;
        function encode(o, b):any;
    }

// 【反馈】新守护兽升星额外属性描述
    module GetNewGuardPetExtraDescInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新手攻略战力提升
    module GetStrategyFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetStrategyFightReq;
        function encode(o, b):any;
    }

// 【反馈】新手攻略战力提升
    module GetStrategyFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新手攻略热度阵容进阶阵容
    module GetStrategyRecommendReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetStrategyRecommendReq;
        function encode(o, b):any;
    }

// 【反馈】新手攻略热度阵容进阶阵容
    module GetStrategyRecommendRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新手攻略大神风采
    module GetStrategyFirstReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetStrategyFirstReq;
        function encode(o, b):any;
    }

// 【反馈】新手攻略大神风采
    module GetStrategyFirstRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】通过道具激活守护兽付费技能
    module ActiveNewGuardPetPaySkillReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActiveNewGuardPetPaySkillReq;
        function encode(o, b):any;
    }

// 【反馈】通过道具激活守护兽付费技能
    module ActiveNewGuardPetPaySkillRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 跨服的战队PK信息
    module ZhanDuiRemotePKInfosReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanDuiRemotePKInfosReq;
        function encode(o, b):any;
    }

// 跨服的战队PK信息
    module ZhanDuiRemotePKInfosRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开支持战队界面
    module OpenZhiChiZhanDuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenZhiChiZhanDuiReq;
        function encode(o, b):any;
    }

// 打开支持战队界面
    module OpenZhiChiZhanDuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开支持日志界面
    module OpenZhiChiLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenZhiChiLogReq;
        function encode(o, b):any;
    }

// 打开支持日志界面
    module OpenZhiChiLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 历史战绩日志
    module HistroyFightLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HistroyFightLogReq;
        function encode(o, b):any;
    }

// 历史战绩日志
    module HistroyFightLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 战队支持
    module ZhanDuiZhiChiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanDuiZhiChiReq;
        function encode(o, b):any;
    }

// 战队支持
    module ZhanDuiZhiChiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 战斗回放
    module FightReplayReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightReplayReq;
        function encode(o, b):any;
    }

// 战队支持
    module FightReplayRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 本地的战队PK信息
    module ZhanDuiLocalPKInfosReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanDuiLocalPKInfosReq;
        function encode(o, b):any;
    }

// 本地的战队PK信息
    module ZhanDuiLocalPKInfosRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 修改战队成员出战位置
    module UpdateRemoteZhanDuiPositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateRemoteZhanDuiPositionReq;
        function encode(o, b):any;
    }

// 修改战队成员出战位置
    module UpdateRemoteZhanDuiPositionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 检查跨服战队赛状态
    module CheckZhanDuiSaiStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CheckZhanDuiSaiStatusReq;
        function encode(o, b):any;
    }

// 检查跨服战队赛状态
    module CheckZhanDuiSaiStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取前三名的信息
    module GetTopThreeZhanDuiInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTopThreeZhanDuiInfoReq;
        function encode(o, b):any;
    }

// 获取前三名的信息
    module GetTopThreeZhanDuiInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 查看玩家阵型
    module ShowPlayerZhenXingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowPlayerZhenXingReq;
        function encode(o, b):any;
    }

// 查看玩家阵型
    module ShowPlayerZhenXingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]z力量面板
    module ZLiLiangPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZLiLiangPanelReq;
        function encode(o, b):any;
    }

// [返回]z力量面板
    module ZLiLiangPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]使用道具激活魔之力Z力量
    module ActiveZliliangWithItemReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActiveZliliangWithItemReq;
        function encode(o, b):any;
    }

// [返回]使用道具激活魔之力Z力量
    module ActiveZliliangWithItemRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取十连抽1列表
    module GetShilianChouListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShilianChouListReq;
        function encode(o, b):any;
    }

// 【反馈】获取十连抽1列表
    module GetShilianChouListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取获取十连抽奖励
    module DoGetShilianChouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoGetShilianChouReq;
        function encode(o, b):any;
    }

// 【反馈】领取获取十连抽奖励
    module DoGetShilianChouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】兑换元宵红包
    module DuihuanHongbaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DuihuanHongbaoReq;
        function encode(o, b):any;
    }

// 【反馈】兑换元宵红包
    module DuihuanHongbaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取大转盘信息
    module GetDzpInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDzpInfoReq;
        function encode(o, b):any;
    }

// 【反馈】获取大转盘信息
    module GetDzpInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】抽取大转盘奖励
    module GetDzpRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDzpRewardReq;
        function encode(o, b):any;
    }

// 【反馈】抽取大转盘奖励
    module GetDzpRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】重置大转盘
    module ResetDzpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ResetDzpReq;
        function encode(o, b):any;
    }

// 【反馈】重置大转盘
    module ResetDzpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]魔法骰子面板
    module MoFaShaiZiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoFaShaiZiPanelReq;
        function encode(o, b):any;
    }

// [返回]魔法骰子面板
    module MoFaShaiZiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]掷骰子
    module GetShaiZiJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShaiZiJiangLiReq;
        function encode(o, b):any;
    }

// [返回]掷骰子
    module GetShaiZiJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会排行
    module GongHuiPaiHangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiPaiHangReq;
        function encode(o, b):any;
    }

// [返回]公会排行
    module GongHuiPaiHangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]创建公会
    module CreateGongHuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CreateGongHuiReq;
        function encode(o, b):any;
    }

// [返回]创建公会
    module CreateGongHuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]申请公会
    module ApplyForGongHuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ApplyForGongHuiReq;
        function encode(o, b):any;
    }

// [返回]创建公会
    module ApplyForGongHuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]查找公会
    module FindGongHuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FindGongHuiReq;
        function encode(o, b):any;
    }

// [返回]查找公会
    module FindGongHuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [返回]玩家的公会id 如果玩家在线 给他推送
    module WanJiaGongHuiIdRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会主面板
    module GongHuiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiPanelReq;
        function encode(o, b):any;
    }

// [返回]查公会主面板
    module GongHuiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]编辑宣言
    module EditXuanYanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EditXuanYanReq;
        function encode(o, b):any;
    }

// [返回]编辑宣言
    module EditXuanYanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取成员列表
    module GetMembersListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMembersListReq;
        function encode(o, b):any;
    }

// [返回]获取成员列表
    module GetMembersListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]修改职位
    module ChangeZhiWeiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeZhiWeiReq;
        function encode(o, b):any;
    }

// [返回]获取成员列表
    module ChangeZhiWeiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会的申请列表
    module RequestListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RequestListReq;
        function encode(o, b):any;
    }

// [返回]公会的申请列表
    module RequestListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]同意申请
    module ApplyRequestReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ApplyRequestReq;
        function encode(o, b):any;
    }

// [返回]同意申请
    module ApplyRequestRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]大厅信息
    module DaTingInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DaTingInfoReq;
        function encode(o, b):any;
    }

// [返回]大厅信息
    module DaTingInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]建设工会
    module JianSheReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JianSheReq;
        function encode(o, b):any;
    }

// [返回]建设工会
    module JianSheRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取公会活跃奖励
    module GetGongHuiActivityPointRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiActivityPointRewardReq;
        function encode(o, b):any;
    }

// [返回]领取公会活跃奖励
    module GetGongHuiActivityPointRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]改变加入
    module ChangeJoinTypeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeJoinTypeReq;
        function encode(o, b):any;
    }

// [返回]改变加入
    module ChangeJoinTypeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会商店列表 主要返回那个商品能不能买 商店商品有每日限购
    module GongHuiShangDianListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiShangDianListReq;
        function encode(o, b):any;
    }

// [返回]公会商店列表 主要返回那个商品能不能买 商店商品有每日限购
    module GongHuiShangDianListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买
    module BuyGongHuiShangDianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyGongHuiShangDianReq;
        function encode(o, b):any;
    }

// [返回]购买
    module BuyGongHuiShangDianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]弹劾会长
    module TanHeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanHeReq;
        function encode(o, b):any;
    }

// [返回]弹劾会长
    module TanHeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会福利列表
    module FuLiBaoXiangListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FuLiBaoXiangListReq;
        function encode(o, b):any;
    }

// [返回]公会福利列表
    module FuLiBaoXiangListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获取福利并加载领取历史记录
    module GetFuLiAndLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFuLiAndLogReq;
        function encode(o, b):any;
    }

// [返回]获取福利并加载领取历史记录
    module GetFuLiAndLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]群发邮件
    module QunFaEmailReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QunFaEmailReq;
        function encode(o, b):any;
    }

// [返回]群发邮件
    module QunFaEmailRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会改名
    module ChangeGongHuiNameReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeGongHuiNameReq;
        function encode(o, b):any;
    }

// [返回]公会改名
    module ChangeGongHuiNameRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]发送公会福利
    module SendGongHuiFuLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SendGongHuiFuLiReq;
        function encode(o, b):any;
    }

// [返回]发送公会福利
    module SendGongHuiFuLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]福利奖励预览
    module ShowFuLiJiangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowFuLiJiangLiReq;
        function encode(o, b):any;
    }

// [返回]福利奖励预览
    module ShowFuLiJiangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]签到
    module GongHuiSginReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiSginReq;
        function encode(o, b):any;
    }

// [反馈]签到
    module GongHuiSginRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]签到信息
    module GongHuiSginInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiSginInfoReq;
        function encode(o, b):any;
    }

// [反馈]签到信息
    module GongHuiSginInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】选择公会徽章
    module ChooseGongHuiHuiZhangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChooseGongHuiHuiZhangReq;
        function encode(o, b):any;
    }

// 【反馈】选择公会徽章
    module ChooseGongHuiHuiZhangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】一键申请公会
    module ApplyGongHuiOneKeyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ApplyGongHuiOneKeyReq;
        function encode(o, b):any;
    }

// 【反馈】一键申请公会
    module ApplyGongHuiOneKeyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】公会拍卖商品信息
    module GongHuiPaiMaiInfoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiPaiMaiInfoListReq;
        function encode(o, b):any;
    }

// 【反馈】公会拍卖商品信息
    module GongHuiPaiMaiInfoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买公会拍卖商品
    module GouMaiPaiMaiInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiPaiMaiInfoReq;
        function encode(o, b):any;
    }

// 【反馈】购买公会拍卖商品
    module GouMaiPaiMaiInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买公会拍卖商品日志
    module GouMaiRiZhiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiRiZhiListReq;
        function encode(o, b):any;
    }

// 【反馈】购买公会拍卖商品日志
    module GouMaiRiZhiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】公会拍卖品推送
    module PushGongHuiPaiMaiInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】公会守护面板
    module GetGongHuiShouHuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiShouHuPanelReq;
        function encode(o, b):any;
    }

// 【反馈】公会守护面板
    module GetGongHuiShouHuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】公会守护报名
    module GetGongHuiShouHuBaoMingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiShouHuBaoMingReq;
        function encode(o, b):any;
    }

// 【反馈】公会守护报名
    module GetGongHuiShouHuBaoMingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】公会守护掷骰子
    module GetGongHuiShouHuTouZiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGongHuiShouHuTouZiReq;
        function encode(o, b):any;
    }

// 【反馈】公会守护掷骰子
    module GetGongHuiShouHuTouZiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】公会守护状态
    module PushShouHuGongHuiStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】秘法证书面板信息
    module MifaZhengshuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MifaZhengshuPanelReq;
        function encode(o, b):any;
    }

// 【反馈】秘法证书面板信息
    module MifaZhengshuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家装备列表
    module GetPlayerZhuangbeiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerZhuangbeiListReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家装备列表
    module GetPlayerZhuangbeiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家装备变化
    module ChangeZhuangbeiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeZhuangbeiReq;
        function encode(o, b):any;
    }

// 【反馈】玩家装备变化
    module ChangeZhuangbeiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】英雄装备穿上、卸下
    module PutZhuangbeiOnOffReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PutZhuangbeiOnOffReq;
        function encode(o, b):any;
    }

// 【反馈】英雄装备穿上、卸下
    module PutZhuangbeiOnOffRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】打开装备礼包道具，随机获取一件道具
    module OpenZhuangbeiGiftReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenZhuangbeiGiftReq;
        function encode(o, b):any;
    }

// 【反馈】打开装备礼包道具，随机获取一件道具
    module OpenZhuangbeiGiftRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】装备强化
    module ZhuangbeiQianghuaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuangbeiQianghuaReq;
        function encode(o, b):any;
    }

// 【反馈】装备强化
    module ZhuangbeiQianghuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]英雄一键穿装备
    module WearEquipmentReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WearEquipmentReq;
        function encode(o, b):any;
    }

// [返回]英雄一键穿装备
    module WearEquipmentRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备一键强化
    module EquipmentChangesReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EquipmentChangesReq;
        function encode(o, b):any;
    }

// 【反馈】玩家装备变化
    module EquipmentChangesRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]合成宝石
    module HeChengBaoShiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeChengBaoShiReq;
        function encode(o, b):any;
    }

// [反馈]合成宝石
    module HeChengBaoShiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备出售预览
    module ZBChuShouYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZBChuShouYuLanReq;
        function encode(o, b):any;
    }

// [反馈]装备出售预览
    module ZBChuShouYuLanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备出售
    module ZBChuShouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZBChuShouReq;
        function encode(o, b):any;
    }

// [反馈]装备出售
    module ZBChuShouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备合成
    module ZBHeChengReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZBHeChengReq;
        function encode(o, b):any;
    }

// [反馈]装备合成
    module ZBHeChengRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备合成预览
    module ZBHeChengYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZBHeChengYuLanReq;
        function encode(o, b):any;
    }

// [反馈]装备合成预览
    module ZBHeChengYuLanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]卸下装备
    module ReplaceZhuangBeiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReplaceZhuangBeiReq;
        function encode(o, b):any;
    }

// [反馈]卸下装备
    module ReplaceZhuangBeiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]操作外显装备
    module ChangeExpandZhuangBeiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeExpandZhuangBeiReq;
        function encode(o, b):any;
    }

// [反馈]操作外显装备
    module ChangeExpandZhuangBeiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家外显装备列表
    module GetExpandZhuangbeiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetExpandZhuangbeiListReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家外显装备列表
    module GetExpandZhuangbeiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家骑技飞技信息
    module GetQiJiAndFEiJiInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetQiJiAndFEiJiInfoReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家骑技飞技信息
    module GetQiJiAndFEiJiInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】升星预览
    module YuLanZhuangbeiShengxingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YuLanZhuangbeiShengxingReq;
        function encode(o, b):any;
    }

// 【反馈】升星预览
    module YuLanZhuangbeiShengxingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】装备升星加成属性查看
    module ShowZhuangbeiJiaChengPropReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowZhuangbeiJiaChengPropReq;
        function encode(o, b):any;
    }

// 【反馈】装备升星加成属性查看
    module ShowZhuangbeiJiaChengPropRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】装备洗练
    module ZhuangbeiXiLianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuangbeiXiLianReq;
        function encode(o, b):any;
    }

// 【反馈】装备洗练
    module ZhuangbeiXiLianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】装备精练
    module ZhuangbeiJingLianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuangbeiJingLianReq;
        function encode(o, b):any;
    }

// 【反馈】装备精练
    module ZhuangbeiJingLianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】精炼预览
    module YuLanZhuangbeiJinglianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YuLanZhuangbeiJinglianReq;
        function encode(o, b):any;
    }

// 【反馈】精炼预览
    module YuLanZhuangbeiJinglianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】装备熔炼
    module ZhuangbeiRongLianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuangbeiRongLianPanelReq;
        function encode(o, b):any;
    }

// 【反馈】装备熔炼
    module ZhuangbeiRongLianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】装备天赋（进阶）属性列表
    module GetZhuangbeiTianfuPropsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuangbeiTianfuPropsReq;
        function encode(o, b):any;
    }

// 【反馈】装备天赋（进阶）属性列表
    module GetZhuangbeiTianfuPropsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】装备突破属性列表
    module GetZhuangbeiTUPoPropsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuangbeiTUPoPropsReq;
        function encode(o, b):any;
    }

// 【反馈】装备突破属性列表
    module GetZhuangbeiTUPoPropsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家装备变化
    module ZhuangbeiHechengReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuangbeiHechengReq;
        function encode(o, b):any;
    }

// 【反馈】装备合成
    module ZhuangbeiHechengRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】装备图鉴面板
    module ZhuangbeiTujianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuangbeiTujianPanelReq;
        function encode(o, b):any;
    }

// 【反馈】装备图鉴面板
    module ZhuangbeiTujianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取已有战队信息
    module GetZhanDuiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhanDuiListReq;
        function encode(o, b):any;
    }

// 获取已有战队信息
    module GetZhanDuiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开战队界面
    module OpenZhanDuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenZhanDuiReq;
        function encode(o, b):any;
    }

// 打开战队界面
    module OpenZhanDuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 创建战队
    module CreateZhanDuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CreateZhanDuiReq;
        function encode(o, b):any;
    }

// 创建战队(创建成功后，直接返回打开战队界面)
    module CreateZhanDuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 申请加入战队
    module ApplyZhanDuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ApplyZhanDuiReq;
        function encode(o, b):any;
    }

// 申请加入战队
    module ApplyZhanDuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开战队申请列表
    module OpenZhanDuiApplyListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenZhanDuiApplyListReq;
        function encode(o, b):any;
    }

// 打开战队申请列表
    module OpenZhanDuiApplyListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 处理申请人员
    module DealZhanDuiApplyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DealZhanDuiApplyReq;
        function encode(o, b):any;
    }

// 处理申请人员
    module DealZhanDuiApplyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 踢掉战队人员
    module KickZhanDuiMemberReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KickZhanDuiMemberReq;
        function encode(o, b):any;
    }

// 踢掉战队人员
    module KickZhanDuiMemberRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 解散战队或脱离战队
    module DelZhanDuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DelZhanDuiReq;
        function encode(o, b):any;
    }

// 解散战队
    module DelZhanDuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 报名参赛
    module ApplyMatchReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ApplyMatchReq;
        function encode(o, b):any;
    }

// 报名参赛
    module ApplyMatchRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 邀请加入战队
    module InviteJoinZhanDuiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.InviteJoinZhanDuiReq;
        function encode(o, b):any;
    }

// 邀请加入战队
    module InviteJoinZhanDuiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 处理邀请信息
    module DealZhanDuiInviteReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DealZhanDuiInviteReq;
        function encode(o, b):any;
    }

// 处理邀请信息
    module DealZhanDuiInviteRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开搜索队员界面
    module OpenMemberSearchReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenMemberSearchReq;
        function encode(o, b):any;
    }

// 打开搜索队员界面
    module OpenMemberSearchRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 搜索队员
    module ZhanDuiMemberSearchReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanDuiMemberSearchReq;
        function encode(o, b):any;
    }

// 搜索队员
    module ZhanDuiMemberSearchRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 修改战队成员出战位置
    module UpdateZhanDuiPositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateZhanDuiPositionReq;
        function encode(o, b):any;
    }

// 修改战队成员出战位置
    module UpdateZhanDuiPositionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 登陆时检查玩家是否已经在战队中
    module CheckPlayerStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CheckPlayerStatusReq;
        function encode(o, b):any;
    }

// 登陆时检查玩家是否已经在战队中
    module CheckPlayerStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 打开战队邀请列表
    module OpenZhanDuiInviteListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OpenZhanDuiInviteListReq;
        function encode(o, b):any;
    }

// 打开战队邀请列表
    module OpenZhanDuiInviteListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取战队比赛奖励
    module GetZhanduiMatchRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhanduiMatchRewardReq;
        function encode(o, b):any;
    }

// 获取战队比赛奖励
    module GetZhanduiMatchRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 收回战队比赛奖励
    module RemoveZhanduiRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RemoveZhanduiRewardReq;
        function encode(o, b):any;
    }

// 收回战队比赛奖励
    module RemoveZhanduiRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 争霸赛商城列表
    module ZhanduiShopListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanduiShopListReq;
        function encode(o, b):any;
    }

// 争霸赛商城列表
    module ZhanduiShopListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 购买争霸赛商城商品
    module GetZhanduiShopGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhanduiShopGoodsReq;
        function encode(o, b):any;
    }

// 争霸赛商城列表
    module GetZhanduiShopGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取联盟币数量
    module GetLianMengScoreCountReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLianMengScoreCountReq;
        function encode(o, b):any;
    }

// 获取联盟币数量
    module GetLianMengScoreCountRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】商店购买商品
    module ShopGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShopGoodsReq;
        function encode(o, b):any;
    }

// 【反馈】商店购买商品
    module ShopGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取玩家购买商品的列表
    module GetShopGoodsListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShopGoodsListReq;
        function encode(o, b):any;
    }

// 【反馈】获取玩家购买商品的列表
    module GetShopGoodsListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载黑市商品列表
    module LoadHSGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadHSGoodsReq;
        function encode(o, b):any;
    }

// 【反馈】加载黑市商品列表
    module LoadHSGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买黑市商品
    module ShopHSGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShopHSGoodsReq;
        function encode(o, b):any;
    }

// 【反馈】购买黑市商品
    module ShopHSGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】黑市商品刷新
    module RefreshHSGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RefreshHSGoodsReq;
        function encode(o, b):any;
    }

// 【反馈】黑市商品刷新
    module RefreshHSGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】商店中 这个这个商品 购买了多少个
    module GetHasBuyCountShopReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHasBuyCountShopReq;
        function encode(o, b):any;
    }

// 【反馈】黑市商品刷新
    module GetHasBuyCountShopRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】商城面板
    module ShangChengPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShangChengPanelReq;
        function encode(o, b):any;
    }

// 【反馈】商城面板
    module ShangChengPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买商城商品
    module BuyShangChengGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyShangChengGoodsReq;
        function encode(o, b):any;
    }

// 【反馈】购买商城商品
    module BuyShangChengGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】推送商城页签状态
    module PushShangChengStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PushShangChengStatusReq;
        function encode(o, b):any;
    }

// 【反馈】推送商城页签状态
    module PushShangChengStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】商品预览
    module ShangChengGoodsShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShangChengGoodsShowReq;
        function encode(o, b):any;
    }

// 【反馈】商品预览
    module ShangChengGoodsShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】商城面板
    module NewShopPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewShopPanelReq;
        function encode(o, b):any;
    }

// 【反馈】商城面板
    module NewShopPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买商品
    module BuyNewShopGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyNewShopGoodsReq;
        function encode(o, b):any;
    }

// 【反馈】购买商品
    module BuyNewShopGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】刷新商品
    module RefreshNewShopReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RefreshNewShopReq;
        function encode(o, b):any;
    }

// 【反馈】刷新商品
    module RefreshNewShopRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】老商城商品信息
    module OldShopShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OldShopShowReq;
        function encode(o, b):any;
    }

// 【反馈】老商城商品信息
    module OldShopShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】地牢商店面板
    module DungeonShopPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DungeonShopPanelReq;
        function encode(o, b):any;
    }

// 【反馈】地牢商店面板
    module DungeonShopPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买地牢商品
    module BuyDungeonShopGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyDungeonShopGoodsReq;
        function encode(o, b):any;
    }

// 【反馈】购买地牢商品
    module BuyDungeonShopGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家任务列表
    module GetMissionListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMissionListReq;
        function encode(o, b):any;
    }

// 玩家任务列表
    module GetMissionListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家获取任务奖励请求
    module GetMissionJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMissionJiangliReq;
        function encode(o, b):any;
    }

// 玩家获取任务奖励请求
    module GetMissionJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家引导任务
    module GetGuideMissionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGuideMissionReq;
        function encode(o, b):any;
    }

// 玩家引导任务
    module GetGuideMissionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 展示邀请面板
    module ShowYaoQingPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowYaoQingPanelReq;
        function encode(o, b):any;
    }

// 展示邀请面板
    module ShowYaoQingPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 极限挑战面板
    module LoadJiXianTiaoZhanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadJiXianTiaoZhanPanelReq;
        function encode(o, b):any;
    }

// 极限挑战面板
    module LoadJiXianTiaoZhanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取极限挑战奖励
    module GetJiXianTiaoZhanRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetJiXianTiaoZhanRewardReq;
        function encode(o, b):any;
    }

// 领取极限挑战奖励
    module GetJiXianTiaoZhanRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取七日大奖任务奖励
    module GetSevenMatchRewardMissionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetSevenMatchRewardMissionReq;
        function encode(o, b):any;
    }

// 领取七日大奖任务奖励
    module GetSevenMatchRewardMissionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 参与日常任务引导
    module JoinDaliyMissionYinDaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JoinDaliyMissionYinDaoReq;
        function encode(o, b):any;
    }

// 参与日常任务引导
    module JoinDaliyMissionYinDaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取日常任务列表
    module GetDailyMissionListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDailyMissionListReq;
        function encode(o, b):any;
    }

// 获取日常任务列表
    module GetDailyMissionListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 接收到被踢的消息
    module GetFollowPetBeiTiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 接收到搜索玩家房间信息的请求
    module GetSearchPetRoomRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]蓝钻信息
    module LanZuanInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LanZuanInfoReq;
        function encode(o, b):any;
    }

// [返回]蓝钻信息
    module LanZuanInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]蓝钻每日礼包
    module LanZuanMeiRiLiBaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LanZuanMeiRiLiBaoListReq;
        function encode(o, b):any;
    }

// [返回]蓝钻每日礼包
    module LanZuanMeiRiLiBaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]蓝钻成长礼包
    module LanZuanChengZhangLiBaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LanZuanChengZhangLiBaoListReq;
        function encode(o, b):any;
    }

// [返回]蓝钻成长礼包
    module LanZuanChengZhangLiBaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]蓝钻新手礼包
    module LanZuanXinShouLiBaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LanZuanXinShouLiBaoListReq;
        function encode(o, b):any;
    }

// [返回]蓝钻新手礼包
    module LanZuanXinShouLiBaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取蓝钻每日礼包
    module GetLanZuanMeiRiLiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLanZuanMeiRiLiBaoReq;
        function encode(o, b):any;
    }

// [返回]领取蓝钻每日礼包
    module GetLanZuanMeiRiLiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取蓝钻成长礼包
    module GetLanZuanChengZhangLiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLanZuanChengZhangLiBaoReq;
        function encode(o, b):any;
    }

// [返回]领取蓝钻成长礼包
    module GetLanZuanChengZhangLiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取蓝钻新手礼包
    module GetLanZuanXinShouLiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLanZuanXinShouLiBaoReq;
        function encode(o, b):any;
    }

// [返回]领取蓝钻新手礼包
    module GetLanZuanXinShouLiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取特权奖励 首充
    module GetTeQuanRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTeQuanRewardReq;
        function encode(o, b):any;
    }

// [返回]领取特权奖励
    module GetTeQuanRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]最强礼包预览
    module LoadZuiQiangLiBaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadZuiQiangLiBaoPanelReq;
        function encode(o, b):any;
    }

// [返回]最强礼包预览
    module LoadZuiQiangLiBaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取最强礼包
    module GetZuiQiangLiBaoRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZuiQiangLiBaoRewardReq;
        function encode(o, b):any;
    }

// [返回]领取最强礼包
    module GetZuiQiangLiBaoRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]资源月卡界面
    module ZiYuanYueKaPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZiYuanYueKaPanelReq;
        function encode(o, b):any;
    }

// [返回]资源月卡界面
    module ZiYuanYueKaPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取资源月卡奖励
    module GetZiYuanYueKaRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZiYuanYueKaRewardReq;
        function encode(o, b):any;
    }

// [返回]领取资源月卡奖励
    module GetZiYuanYueKaRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]蓝钻礼包列表
    module LanZuanLiBaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LanZuanLiBaoListReq;
        function encode(o, b):any;
    }

// [返回]蓝钻礼包列表
    module LanZuanLiBaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]一键领取蓝钻礼包
    module GetLanZuanLiBaoOneKeyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLanZuanLiBaoOneKeyReq;
        function encode(o, b):any;
    }

// [返回]一键领取蓝钻礼包
    module GetLanZuanLiBaoOneKeyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]qq大厅礼包列表
    module QQGameLiBaoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QQGameLiBaoListReq;
        function encode(o, b):any;
    }

// [返回]qq大厅礼包列表
    module QQGameLiBaoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取qq大厅礼包
    module GetQQGameLiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetQQGameLiBaoReq;
        function encode(o, b):any;
    }

// [返回]领取qq大厅礼包
    module GetQQGameLiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]黄金投资list
    module HuangJinTouZiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HuangJinTouZiListReq;
        function encode(o, b):any;
    }

// [请求]黄金投资list
    module HuangJinTouZiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取黄金投资奖励
    module GetHuangJinTouZiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHuangJinTouZiReq;
        function encode(o, b):any;
    }

// [请求]领取黄金投资
    module GetHuangJinTouZiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]激活黄金投资
    module ActiveHuangJinTouZiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActiveHuangJinTouZiReq;
        function encode(o, b):any;
    }

// [请求]激活黄金投资
    module ActiveHuangJinTouZiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]黄金贵族面板
    module HuangJinGuiZuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HuangJinGuiZuPanelReq;
        function encode(o, b):any;
    }

// [请求]黄金贵族面板
    module HuangJinGuiZuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]修改贵族特权开关
    module UpdateGuiZuAutoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateGuiZuAutoReq;
        function encode(o, b):any;
    }

// [请求]修改贵族特权开关
    module UpdateGuiZuAutoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 贵族特权开关信息
    module PushGuiZuAutoChangeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]创角返利面板
    module ChuangJueFanLiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChuangJueFanLiPanelReq;
        function encode(o, b):any;
    }

// 创角返利面板
    module ChuangJueFanLiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取创角返利奖励
    module GetChuangJueFanLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChuangJueFanLiReq;
        function encode(o, b):any;
    }

// 领取创角返利奖励
    module GetChuangJueFanLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]专属神器面板  详情面板  所有属性都能看到的面板
    module ZhuanShuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuPanelReq;
        function encode(o, b):any;
    }

// [返回]专属神器面板  详情面板  所有属性都能看到的面板
    module ZhuanShuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】精灵 专属 神器穿上、卸下
    module PutZhuanShuShenQiOnOffReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PutZhuanShuShenQiOnOffReq;
        function encode(o, b):any;
    }

// 【反馈】请求】精灵 专属 神器穿上、卸下
    module PutZhuanShuShenQiOnOffRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]专属神器背包
    module ZhuanShuShenQiBagListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuShenQiBagListReq;
        function encode(o, b):any;
    }

// [返回]专属神器背包
    module ZhuanShuShenQiBagListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】专属神器强化
    module ZhuanShuQiangHuaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuQiangHuaReq;
        function encode(o, b):any;
    }

// 【反馈】专属神器强化
    module ZhuanShuQiangHuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】专属神器回收预览
    module ZhuanShuHuiShouYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuHuiShouYuLanReq;
        function encode(o, b):any;
    }

// 【反馈】专属神器回收预览
    module ZhuanShuHuiShouYuLanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】专属神器回收批量预览   返回 ZhuanShuHuiShouYuLan
    module ZhuanShuHuiShouPiLiangYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuHuiShouPiLiangYuLanReq;
        function encode(o, b):any;
    }

// 【请求】专属神器回收  接受dbId
    module ZhuanShuHuiShouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuHuiShouReq;
        function encode(o, b):any;
    }

// 【反馈】专属神器回收结果
    module ZhuanShuHuiShouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】专属神器回收批量   返回 ZhuanShuHuiShou
    module ZhuanShuHuiShouPiLiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuHuiShouPiLiangReq;
        function encode(o, b):any;
    }

// 【请求】专属神器强化面板
    module ZhuanShuQiangHuaPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuQiangHuaPanelReq;
        function encode(o, b):any;
    }

// 【反馈】专属神器强化面板
    module ZhuanShuQiangHuaPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】专属神器图鉴ICON
    module ZhuanShuTuJianIconReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuTuJianIconReq;
        function encode(o, b):any;
    }

// 【反馈】专属神器图鉴ICON
    module ZhuanShuTuJianIconRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】专属神器图鉴itemInfo
    module ZhuanShuTuJianItemInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuTuJianItemInfoReq;
        function encode(o, b):any;
    }

// 【反馈】专属神器图鉴itemInfo
    module ZhuanShuTuJianItemInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活专属神器图鉴返回材料
    module JiHuoTuJianFanHuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JiHuoTuJianFanHuanReq;
        function encode(o, b):any;
    }

// 【返回】激活专属神器图鉴返回材料
    module JiHuoTuJianFanHuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活专属神器图鉴
    module JiHuoTuJianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JiHuoTuJianReq;
        function encode(o, b):any;
    }

// 【返回】激活专属神器图鉴
    module JiHuoTuJianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】专属神器红点
    module tuJianPointReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.tuJianPointReq;
        function encode(o, b):any;
    }

// 【返回】专属神器红点
    module tuJianPointRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]洛托姆之力主界面
    module LuoTuoMuMainPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LuoTuoMuMainPanelReq;
        function encode(o, b):any;
    }

// [返回]洛托姆之力主界面
    module LuoTuoMuMainPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]洛托姆之力界面
    module LuoTuoMuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LuoTuoMuPanelReq;
        function encode(o, b):any;
    }

// [返回]洛托姆之力界面
    module LuoTuoMuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]洛托姆之力升级
    module LuoTuoMuUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LuoTuoMuUpReq;
        function encode(o, b):any;
    }

// [返回]洛托姆之力升级
    module LuoTuoMuUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]进阶界面
    module LuoTuoMuJinJiePanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LuoTuoMuJinJiePanelReq;
        function encode(o, b):any;
    }

// [返回]进阶界面
    module LuoTuoMuJinJiePanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]洛托姆之力进阶
    module LuoTuoMuUpGradeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LuoTuoMuUpGradeReq;
        function encode(o, b):any;
    }

// [返回]洛托姆之力进阶
    module LuoTuoMuUpGradeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]洛托姆之力信息
    module PushLuoTuoMuJinJieInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PushLuoTuoMuJinJieInfoReq;
        function encode(o, b):any;
    }

// [推送]洛托姆之力信息
    module PushLuoTuoMuJinJieInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]切换皮肤
    module ChangeDisplayReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeDisplayReq;
        function encode(o, b):any;
    }

// [回复]切换皮肤
    module ChangeDisplayRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】快捷购买体力
    module QuickbuyTiliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QuickbuyTiliReq;
        function encode(o, b):any;
    }

// 【反馈】快捷购买体力
    module QuickbuyTiliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】快捷购买银两
    module QuickbuyYinliangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QuickbuyYinliangReq;
        function encode(o, b):any;
    }

// 【反馈】快捷购买银两
    module QuickbuyYinliangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取出海地区信息
    module GetAreaListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAreaListReq;
        function encode(o, b):any;
    }

// 获取出海地区信息
    module GetAreaListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获得当前船只信息
    module GetShipInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShipInfoReq;
        function encode(o, b):any;
    }

// 获得当前船只信息
    module GetShipInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 进行出海
    module ChuhaiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChuhaiReq;
        function encode(o, b):any;
    }

// 进行出海
    module ChuhaiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 立即完成出海
    module FinishShipReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FinishShipReq;
        function encode(o, b):any;
    }

// 立即完成出海
    module FinishShipRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取升级属性列表
    module LevelUpListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LevelUpListReq;
        function encode(o, b):any;
    }

// 获取升级属性列表
    module LevelUpListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 升级
    module LevelUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LevelUpReq;
        function encode(o, b):any;
    }

// 升级
    module LevelUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取激活属性列表
    module JihuoListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JihuoListReq;
        function encode(o, b):any;
    }

// 获取激活属性列表
    module JihuoListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 激活
    module JihuoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JihuoReq;
        function encode(o, b):any;
    }

// 激活
    module JihuoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 航海日志
    module ShipLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShipLogReq;
        function encode(o, b):any;
    }

// 航海日志
    module ShipLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取奖励
    module GetShipRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShipRewardReq;
        function encode(o, b):any;
    }

// 领取奖励
    module GetShipRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]专属神器显示属性
    module ZhuanShuShenQiItemInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanShuShenQiItemInfoReq;
        function encode(o, b):any;
    }

// [返回]专属神器显示属性
    module ZhuanShuShenQiItemInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取专属神器
    module GetZhuanShuShenQiItemReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuanShuShenQiItemReq;
        function encode(o, b):any;
    }

// [返回]领取专属神器
    module GetZhuanShuShenQiItemRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取拍卖信息
    module GetPaiMaiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPaiMaiListReq;
        function encode(o, b):any;
    }

// 获取拍卖信息
    module GetPaiMaiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 竞拍物品
    module JingPaiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JingPaiReq;
        function encode(o, b):any;
    }

// 竞拍物品
    module JingPaiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 竞拍物品分类列表
    module GetPaiMaiLabelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPaiMaiLabelReq;
        function encode(o, b):any;
    }

// 竞拍物品分类列表
    module GetPaiMaiLabelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 交易记录
    module GetPaiMaiLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPaiMaiLogReq;
        function encode(o, b):any;
    }

// 交易记录
    module GetPaiMaiLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 上架物品
    module ShangJiaPaiMaiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShangJiaPaiMaiReq;
        function encode(o, b):any;
    }

// 上架物品
    module ShangJiaPaiMaiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 拍卖物品背包
    module PaiMaiBeiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PaiMaiBeiBaoReq;
        function encode(o, b):any;
    }

// 拍卖物品背包
    module PaiMaiBeiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取我的拍卖信息
    module GetMyPaiMaiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMyPaiMaiListReq;
        function encode(o, b):any;
    }

// 获取我的拍卖信息
    module GetMyPaiMaiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取我的拍卖信息
    module GetMyPaiMaiItemReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMyPaiMaiItemReq;
        function encode(o, b):any;
    }

// 获取我的拍卖信息
    module GetMyPaiMaiItemRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】遗迹探险面板
    module YiJiTanXianPannelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YiJiTanXianPannelReq;
        function encode(o, b):any;
    }

// 【反馈】遗迹探险面板
    module YiJiTanXianPannelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】遗迹探险刷新
    module YiJiTanXianRefreshReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YiJiTanXianRefreshReq;
        function encode(o, b):any;
    }

// 【反馈】遗迹探险刷新
    module YiJiTanXianRefreshRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】单条任务详细信息
    module YiJiTanXianTaskDetailPannelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YiJiTanXianTaskDetailPannelReq;
        function encode(o, b):any;
    }

// 【反馈】单条任务详细信息
    module YiJiTanXianTaskDetailPannelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】遗迹探险派出
    module YiJiTanXianStartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YiJiTanXianStartReq;
        function encode(o, b):any;
    }

// 【反馈】遗迹探险派出
    module YiJiTanXianStartRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】遗迹探险提前完成任务
    module YiJiTanXianFinishTaskReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YiJiTanXianFinishTaskReq;
        function encode(o, b):any;
    }

// 【反馈】遗迹探险提前完成任务
    module YiJiTanXianFinishTaskRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】遗迹探险领取奖励
    module YiJiTanXianReceiveBonusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YiJiTanXianReceiveBonusReq;
        function encode(o, b):any;
    }

// 【反馈】遗迹探险领取奖励
    module YiJiTanXianReceiveBonusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得兑换列表
    module GetDuihuanListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDuihuanListReq;
        function encode(o, b):any;
    }

// 【反馈】获得兑换列表
    module GetDuihuanListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】兑换请求
    module DoExchangeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DoExchangeReq;
        function encode(o, b):any;
    }

// 【反馈】兑换请求
    module DoExchangeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获得当前小关
    module CurrXiaoGuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CurrXiaoGuanReq;
        function encode(o, b):any;
    }

// [反馈]获得当前小关
    module CurrXiaoGuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]推图战斗
    module TuiTuFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TuiTuFightReq;
        function encode(o, b):any;
    }

// [返回]推图战斗
    module TuiTuFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]推图战斗的Boss和奖励
    module GetTuiTuFightBossAndRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTuiTuFightBossAndRewardReq;
        function encode(o, b):any;
    }

// [返回]推图战斗
    module GetTuiTuFightBossAndRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]碾压
    module CompleteSuppressionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CompleteSuppressionReq;
        function encode(o, b):any;
    }

// [返回]碾压
    module CompleteSuppressionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】更新Rogoulike地下城的Diy json数据
    module UpdateRogoulikeDiyStringReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateRogoulikeDiyStringReq;
        function encode(o, b):any;
    }

// 【反馈】更新Rogoulike地下城的Diy json数据
    module UpdateRogoulikeDiyStringRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike隐藏精英怪战斗
    module RogoulikeHideGuaiwuFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RogoulikeHideGuaiwuFightReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike隐藏精英怪战斗
    module RogoulikeHideGuaiwuFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike初始化新的一层数据
    module InitRogoulikeCengDataReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.InitRogoulikeCengDataReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike初始化新的一层数据
    module InitRogoulikeCengDataRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike地下城装备刚刚获得的神技卡
    module PutOnMagicalSkillAtRogoulikeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PutOnMagicalSkillAtRogoulikeReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike地下城装备刚刚获得的神技卡
    module PutOnMagicalSkillAtRogoulikeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加载所有神技卡数据
    module GetAllRogoulikeMagicalSkillsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAllRogoulikeMagicalSkillsReq;
        function encode(o, b):any;
    }

// [返回]加载所有神技卡数据
    module GetAllRogoulikeMagicalSkillsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]进入Rogoulike地图
    module EnterRogoulikeMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EnterRogoulikeMapReq;
        function encode(o, b):any;
    }

// [返回]进入Rogoulike地图
    module EnterRogoulikeMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领奖来自Rogoulike小怪和宝箱
    module GetRewardFromRogoulikeXiaoguaiAndBaoxiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRewardFromRogoulikeXiaoguaiAndBaoxiangReq;
        function encode(o, b):any;
    }

// [返回]领奖来自Rogoulike小怪和宝箱
    module GetRewardFromRogoulikeXiaoguaiAndBaoxiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike已开启的章节列表
    module GetRogoulikeChapterListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRogoulikeChapterListReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike已开启的章节列表
    module GetRogoulikeChapterListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike当前章节获得的所有奖励
    module GetRogoulikeAllRewardsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRogoulikeAllRewardsReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike当前章节获得的所有奖励
    module GetRogoulikeAllRewardsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike开启新的章节
    module NewRogoulikeChapterReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewRogoulikeChapterReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike开启新的章节
    module NewRogoulikeChapterRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]确认离开Rogoulike迷宫
    module ConfirmToLeaveRogoulikeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ConfirmToLeaveRogoulikeReq;
        function encode(o, b):any;
    }

// [请求]Rogoulike秘境发奖
    module GetRogoulikeMysteryRewardsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRogoulikeMysteryRewardsReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike秘境发奖
    module GetRogoulikeMysteryRewardsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike秘境战斗
    module RogoulikeMysteryFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RogoulikeMysteryFightReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike秘境战斗
    module RogoulikeMysteryFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike秘境金猪奖励上限
    module GetRogoulikeMysteryLimitRewardsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRogoulikeMysteryLimitRewardsReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike秘境金猪奖励上限
    module GetRogoulikeMysteryLimitRewardsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送]Rogoulike地下城信息推送
    module PushRogoulikeChangeDataRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】Rogoulike神技属性查看
    module RogoulikeMagicalSkillCardViewReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RogoulikeMagicalSkillCardViewReq;
        function encode(o, b):any;
    }

// 【反馈】Rogoulike神技属性查看
    module RogoulikeMagicalSkillCardViewRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]地牢怪战斗
    module RogoulikeDungeonMonsterFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RogoulikeDungeonMonsterFightReq;
        function encode(o, b):any;
    }

// [返回]地牢怪战斗
    module RogoulikeDungeonMonsterFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]地牢重置
    module RogoulikeDungeonResetReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RogoulikeDungeonResetReq;
        function encode(o, b):any;
    }

// [返回]地牢重置
    module RogoulikeDungeonResetRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]迷宫内传送功能面板
    module ChuansongGuanghuanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChuansongGuanghuanPanelReq;
        function encode(o, b):any;
    }

// [返回]迷宫内传送功能面板
    module ChuansongGuanghuanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]Rogoulike中玩家被打死了
    module RogoulikePlayerIsDeadReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RogoulikePlayerIsDeadReq;
        function encode(o, b):any;
    }

// [返回]Rogoulike中玩家被打死了
    module RogoulikePlayerIsDeadRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]奇遇礼包面板
    module QiYuLiBaoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QiYuLiBaoPanelReq;
        function encode(o, b):any;
    }

// [返回]奇遇礼包面板
    module QiYuLiBaoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买奇遇礼包
    module BuyQiYuLiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyQiYuLiBaoReq;
        function encode(o, b):any;
    }

// [返回]购买奇遇礼包
    module BuyQiYuLiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取奇遇礼包
    module GetQiYuLiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetQiYuLiBaoReq;
        function encode(o, b):any;
    }

// [返回]领取奇遇礼包
    module GetQiYuLiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求获取的称号ID list
    module LoadPlayerTitleIdListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadPlayerTitleIdListReq;
        function encode(o, b):any;
    }

// 【反馈】请求获取的称号IDlist
    module LoadPlayerTitleIdListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取称号接口 （消耗代金券）
    module BuyPlayerTitleReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyPlayerTitleReq;
        function encode(o, b):any;
    }

// 【反馈】获取称号接口 （消耗代金券）
    module BuyPlayerTitleRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】使用称号接口
    module ChangePlayerTitleReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangePlayerTitleReq;
        function encode(o, b):any;
    }

// 【反馈】使用称号接口
    module ChangePlayerTitleRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】查看新获取的称号ID
    module ReadPlayerTitleReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReadPlayerTitleReq;
        function encode(o, b):any;
    }

// 【请求】请求获取的头像框ID list
    module LoadTouxiangkuangIdListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadTouxiangkuangIdListReq;
        function encode(o, b):any;
    }

// 【反馈】请求获取的头像框IDlist
    module LoadTouxiangkuangIdListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活头像框接口
    module ActiveTouxiangkuangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActiveTouxiangkuangReq;
        function encode(o, b):any;
    }

// 【反馈】激活头像框接口
    module ActiveTouxiangkuangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】使用头像框接口
    module ChangeTouxiangkuangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeTouxiangkuangReq;
        function encode(o, b):any;
    }

// 【反馈】使用头像框接口
    module ChangeTouxiangkuangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会战报名
    module GongHuiZhanBaoMingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiZhanBaoMingReq;
        function encode(o, b):any;
    }

// [返回]公会战报名状态
    module GongHuiZhanBaoMingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]阵型调整
    module GongHuiGroupTiaoZhengReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiGroupTiaoZhengReq;
        function encode(o, b):any;
    }

// [返回]公会战调整是否成功
    module GongHuiGroupTiaoZhengRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会战阵型
    module GongHuiZhanZhenXingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiZhanZhenXingReq;
        function encode(o, b):any;
    }

// [返回]公会战阵型
    module GongHuiZhanZhenXingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 历史回放界面
    module HisToryPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HisToryPanelReq;
        function encode(o, b):any;
    }

// 历史回放界面
    module HisToryPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 返回排行榜全部
    module ShowGHZRankAllReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowGHZRankAllReq;
        function encode(o, b):any;
    }

// 返回排行榜全部
    module ShowGHZRankAllRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 公会战展示主界面
    module ShowGHZPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowGHZPanelReq;
        function encode(o, b):any;
    }

// 公会战展示主界面
    module ShowGHZPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 公会战战斗主界面
    module ShowGHZFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowGHZFightReq;
        function encode(o, b):any;
    }

// 公会战战斗主界面
    module ShowGHZFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会战集体回放
    module GongHuiZhanJiTiHuiFangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiZhanJiTiHuiFangReq;
        function encode(o, b):any;
    }

// [返回]公会战集体回放
    module GongHuiZhanJiTiHuiFangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]收到其他服务器发过来的 公会战pvp战斗记录id
    module WatchGongHuiZhanPVPReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WatchGongHuiZhanPVPReq;
        function encode(o, b):any;
    }

// [返回]收到其他服务器发过来的 公会战pvp战斗记录id
    module WatchGongHuiZhanPVPRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取跟随宠信息
    module GetFollowPetListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFollowPetListReq;
        function encode(o, b):any;
    }

// 获取跟随宠信息
    module GetFollowPetListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 进入跟随宠房间
    module EnterFollowPetRoomReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EnterFollowPetRoomReq;
        function encode(o, b):any;
    }

// 进入跟随宠房间
    module EnterFollowPetRoomRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 升级装饰面板
    module PetRoomDecoratePanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PetRoomDecoratePanelReq;
        function encode(o, b):any;
    }

// 升级装饰面板
    module PetRoomDecoratePanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 升级装饰
    module PetRoomDecorateLvUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PetRoomDecorateLvUpReq;
        function encode(o, b):any;
    }

// 升级装饰
    module PetRoomDecorateLvUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 返回自己房间
    module BackFollowPetRoomReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BackFollowPetRoomReq;
        function encode(o, b):any;
    }

// 返回自己房间
    module BackFollowPetRoomRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 选择装饰
    module GetPetRoomDecorateReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPetRoomDecorateReq;
        function encode(o, b):any;
    }

// 选择装饰
    module GetPetRoomDecorateRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 宠物传承
    module GetPetChuanChengReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPetChuanChengReq;
        function encode(o, b):any;
    }

// 宠物传承
    module GetPetChuanChengRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 宠物成长
    module GetPetChengZhangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPetChengZhangReq;
        function encode(o, b):any;
    }

// 宠物成长
    module GetPetChengZhangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 跟随宠物孵化
    module GetPetFuHuaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPetFuHuaReq;
        function encode(o, b):any;
    }

// 跟随宠物孵化
    module GetPetFuHuaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 随机房间
    module FollowPetRdmRoomReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FollowPetRdmRoomReq;
        function encode(o, b):any;
    }

// 随机房间
    module FollowPetRdmRoomRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 寄养
    module FollowPetParkPetReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FollowPetParkPetReq;
        function encode(o, b):any;
    }

// 寄养
    module FollowPetParkPetRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 改变跟随状态
    module ChangeFollowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeFollowReq;
        function encode(o, b):any;
    }

// 改变跟随状态
    module ChangeFollowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 检测是否有可孵化的蛋
    module FollowPetCheckChildReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FollowPetCheckChildReq;
        function encode(o, b):any;
    }

// 检测是否有可孵化的蛋
    module FollowPetCheckChildRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 孵化子代
    module FollowPetHatchChildReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FollowPetHatchChildReq;
        function encode(o, b):any;
    }

// 孵化子代
    module FollowPetHatchChildRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 提高生育率
    module FollowPetUpRearRateReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FollowPetUpRearRateReq;
        function encode(o, b):any;
    }

// 提高生育率
    module FollowPetUpRearRateRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 点赞
    module FollowPetDianZanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FollowPetDianZanReq;
        function encode(o, b):any;
    }

// 点赞
    module FollowPetDianZanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获得点赞
    module GetFollowPetDianZanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFollowPetDianZanReq;
        function encode(o, b):any;
    }

// 获得点赞
    module GetFollowPetDianZanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 踢人
    module FollowPetTiRenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FollowPetTiRenReq;
        function encode(o, b):any;
    }

// 踢人
    module FollowPetTiRenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取寄养日志
    module GetFollowPetJiYangRecordReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFollowPetJiYangRecordReq;
        function encode(o, b):any;
    }

// 获取寄养日志
    module GetFollowPetJiYangRecordRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 本服寄养预览 公会或者排行榜
    module BenFuJiYangYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BenFuJiYangYuLanReq;
        function encode(o, b):any;
    }

// 本服寄养预览 公会或者排行榜
    module BenFuJiYangYuLanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 提升代数
    module IncreaseMyGenerationReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.IncreaseMyGenerationReq;
        function encode(o, b):any;
    }

// 提升代数
    module IncreaseMyGenerationRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 修改房间公告
    module UpdatePetRoomGongGaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdatePetRoomGongGaoReq;
        function encode(o, b):any;
    }

// 修改房间公告
    module UpdatePetRoomGongGaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 修改房间密码面板
    module UpdatePasswordPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdatePasswordPanelReq;
        function encode(o, b):any;
    }

// 修改房间密码面板
    module UpdatePasswordPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 修改房间密码或道具状态
    module UpdatePasswordOrItemReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdatePasswordOrItemReq;
        function encode(o, b):any;
    }

// 修改房间密码或道具状态
    module UpdatePasswordOrItemRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 按房间号搜索房间
    module SearchFollowPetRoomReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SearchFollowPetRoomReq;
        function encode(o, b):any;
    }

// 按房间号搜索房间
    module SearchFollowPetRoomRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 修改自动孵化上阵状态
    module UpdateAutoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateAutoReq;
        function encode(o, b):any;
    }

// 修改自动孵化上阵状态
    module UpdateAutoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 解锁
    module UnLockReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UnLockReq;
        function encode(o, b):any;
    }

// 解锁
    module UnLockRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 房间饰品面板
    module PetRoomShiPinPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PetRoomShiPinPanelReq;
        function encode(o, b):any;
    }

// 房间饰品面板
    module PetRoomShiPinPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 激活房间饰品
    module AddPetRoomShiPinReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AddPetRoomShiPinReq;
        function encode(o, b):any;
    }

// 添加房间饰品
    module AddPetRoomShiPinRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 放入房间饰品
    module PutPetRoomShiPinReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PutPetRoomShiPinReq;
        function encode(o, b):any;
    }

// 放入房间饰品
    module PutPetRoomShiPinRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 调整饰品位置
    module ChangeShiPinPositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeShiPinPositionReq;
        function encode(o, b):any;
    }

// 调整饰品位置
    module ChangeShiPinPositionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 跟随宠皮肤面板
    module FollowPetPiFuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FollowPetPiFuPanelReq;
        function encode(o, b):any;
    }

// 跟随宠皮肤面板
    module FollowPetPiFuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 激活跟随宠皮肤
    module GetFollowPetPiFuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFollowPetPiFuReq;
        function encode(o, b):any;
    }

// 激活跟随宠皮肤
    module GetFollowPetPiFuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 穿戴跟随宠皮肤
    module PutOnFollowPetPiFuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PutOnFollowPetPiFuReq;
        function encode(o, b):any;
    }

// 穿戴跟随宠皮肤
    module PutOnFollowPetPiFuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取跟随宠皮肤信息
    module GetFollowPetPiFuInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFollowPetPiFuInfoReq;
        function encode(o, b):any;
    }

// 获取跟随宠皮肤信息
    module GetFollowPetPiFuInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取跟随宠装饰红点
    module GetPetDecorateRedPointReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPetDecorateRedPointReq;
        function encode(o, b):any;
    }

// 获取跟随宠装饰红点
    module GetPetDecorateRedPointRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]新跟随宠界面
    module NewFollowPetPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewFollowPetPanelReq;
        function encode(o, b):any;
    }

// [返回]新跟随宠界面
    module NewFollowPetPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]新跟随宠强化界面
    module NewFollowPetQiangHuaPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewFollowPetQiangHuaPanelReq;
        function encode(o, b):any;
    }

// [返回]新跟随宠强化界面
    module NewFollowPetQiangHuaPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]新跟随宠进阶界面
    module NewFollowPetJinJiePanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewFollowPetJinJiePanelReq;
        function encode(o, b):any;
    }

// [返回]新跟随宠强化界面
    module NewFollowPetJinJiePanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]跟随宠升级或进阶
    module GetNewFollowPetLvUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewFollowPetLvUpReq;
        function encode(o, b):any;
    }

// [返回]跟随宠升级或进阶
    module GetNewFollowPetLvUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]修改显示类型
    module ChangeShowTypeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeShowTypeReq;
        function encode(o, b):any;
    }

// [返回]修改显示类型
    module ChangeShowTypeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]新跟随宠信息
    module PushNewFollowPetInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PushNewFollowPetInfoReq;
        function encode(o, b):any;
    }

// [返回]新跟随宠信息
    module PushNewFollowPetInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤面板
    module PiFuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PiFuPanelReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤面板
    module PiFuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家皮肤类型的集合
    module PiFuTypeListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PiFuTypeListReq;
        function encode(o, b):any;
    }

// [返回]玩家皮肤类型的集合
    module PiFuTypeListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]穿戴皮肤
    module ChuanDaiPiFuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChuanDaiPiFuReq;
        function encode(o, b):any;
    }

// [返回]穿戴皮肤
    module ChuanDaiPiFuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买皮肤
    module GouMaiPiFuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiPiFuReq;
        function encode(o, b):any;
    }

// [返回]购买皮肤
    module GouMaiPiFuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]进阶皮肤面板
    module JinJiePiFuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JinJiePiFuPanelReq;
        function encode(o, b):any;
    }

// [返回]进阶皮肤面板
    module JinJiePiFuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]进阶皮肤
    module JinJiePiFuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JinJiePiFuReq;
        function encode(o, b):any;
    }

// [返回]进阶皮肤
    module JinJiePiFuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩吧空间背景
    module CoverRewardPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CoverRewardPanelReq;
        function encode(o, b):any;
    }

// 玩吧空间背景
    module CoverRewardPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取玩吧空间背景奖励
    module GetCoverRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetCoverRewardReq;
        function encode(o, b):any;
    }

// 领取玩吧空间背景奖励
    module GetCoverRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩吧vip礼包面板
    module WanBaVipRewardPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WanBaVipRewardPanelReq;
        function encode(o, b):any;
    }

// 玩吧vip礼包面板
    module WanBaVipRewardPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取玩吧vip礼包奖励
    module GetWanBaVipRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWanBaVipRewardReq;
        function encode(o, b):any;
    }

// 领取玩吧vip礼包奖励
    module GetWanBaVipRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家养成线升级时，道具材料不足
    module CultivateCardActionBeLackOfItemsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CultivateCardActionBeLackOfItemsReq;
        function encode(o, b):any;
    }

// [请求]玩家养成线弹窗礼包推送
    module PushCultivateCardActionGiftPackRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买养成线弹窗礼包
    module BuyCultivateCardActionGiftPackReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyCultivateCardActionGiftPackReq;
        function encode(o, b):any;
    }

// [请求]购买养成线弹窗礼包
    module BuyCultivateCardActionGiftPackRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]面板
    module PanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PanelReq;
        function encode(o, b):any;
    }

// [返回]面板
    module PanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]战斗
    module DiTieFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DiTieFightReq;
        function encode(o, b):any;
    }

// [返回]战斗
    module DiTieFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]扫荡
    module SaoDangFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SaoDangFightReq;
        function encode(o, b):any;
    }

// [返回]扫荡
    module SaoDangFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买上限
    module GouMaiShangXianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GouMaiShangXianReq;
        function encode(o, b):any;
    }

// [返回]购买上限
    module GouMaiShangXianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 公共异常反馈
    module PublicErrorRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 游戏管理接口请求
    module GameManageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GameManageReq;
        function encode(o, b):any;
    }

// 游戏管理接口反馈
    module GameManageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家更换头像
    module ChangePhotoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangePhotoReq;
        function encode(o, b):any;
    }

// 玩家更换头像
    module ChangePhotoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家更换名称
    module ChangeNameReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeNameReq;
        function encode(o, b):any;
    }

// 玩家更换名称
    module ChangeNameRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 礼品码兑换
    module UseGiftCodeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UseGiftCodeReq;
        function encode(o, b):any;
    }

// 礼品码兑换
    module UseGiftCodeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩家已经拥有过的武将列表
    module GetHeroLogListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHeroLogListReq;
        function encode(o, b):any;
    }

// 【反馈】玩家已经拥有过的武将列表
    module GetHeroLogListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】获取精灵图鉴面板
    module GetHeroTuJianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHeroTuJianPanelReq;
        function encode(o, b):any;
    }

// 【反馈】获取精灵图鉴面板
    module GetHeroTuJianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】领取精灵图鉴奖励
    module GetHeroTuJianRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHeroTuJianRewardReq;
        function encode(o, b):any;
    }

// 【反馈】领取精灵图鉴奖励
    module GetHeroTuJianRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取VIP奖励
    module GetVipJiangliReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetVipJiangliReq;
        function encode(o, b):any;
    }

// 【反馈】领取VIP奖励
    module GetVipJiangliRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取以成为的VIP列表
    module GetVipListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetVipListReq;
        function encode(o, b):any;
    }

// 【反馈】获取以成为的VIP列表
    module GetVipListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】VIP免费激活
    module VipFreeUpgradeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.VipFreeUpgradeReq;
        function encode(o, b):any;
    }

// 【反馈】VIP免费激活
    module VipFreeUpgradeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新的vip列表
    module GetNewVipListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewVipListReq;
        function encode(o, b):any;
    }

// 【反馈】新的vip列表
    module GetNewVipListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】VIP激活
    module NewVipUpgradeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewVipUpgradeReq;
        function encode(o, b):any;
    }

// 【反馈】VIP免费激活
    module NewVipUpgradeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活并领取当前累计钻石
    module JiHuoAndGetLeiJiZuanShiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JiHuoAndGetLeiJiZuanShiReq;
        function encode(o, b):any;
    }

// 【反馈】激活并领取当前累计钻石
    module JiHuoAndGetLeiJiZuanShiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】推送VIP1免费领取钻石状态
    module PushVipFreeDiamondInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】VIP1免费领取钻石
    module GetVipFreeDiamondReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetVipFreeDiamondReq;
        function encode(o, b):any;
    }

// 【反馈】VIP1免费领取钻石
    module GetVipFreeDiamondRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取VIP直升时间状态信息
    module GetVipLevelUpDirectlyInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetVipLevelUpDirectlyInfoReq;
        function encode(o, b):any;
    }

// 【请求】获取VIP直升时间状态信息
    module GetVipLevelUpDirectlyInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】VIP直升
    module ClickVipLevelUpDirectlyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ClickVipLevelUpDirectlyReq;
        function encode(o, b):any;
    }

// 【反馈】VIP直升
    module ClickVipLevelUpDirectlyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]迷宫面板
    module MiGongPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MiGongPanelReq;
        function encode(o, b):any;
    }

// [反馈]迷宫面板
    module MiGongPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]迷宫主怪战斗
    module MiGongZhuGuaiFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MiGongZhuGuaiFightReq;
        function encode(o, b):any;
    }

// [返回]迷宫主怪战斗
    module MiGongZhuGuaiFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]迷宫主怪战斗
    module MiGongBossFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MiGongBossFightReq;
        function encode(o, b):any;
    }

// [返回]迷宫主怪战斗
    module MiGongBossFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]获得迷宫宝箱
    module GetBaoXiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBaoXiangReq;
        function encode(o, b):any;
    }

// [返回]获得迷宫宝箱
    module GetBaoXiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]保存迷宫信息
    module SaveInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SaveInfoReq;
        function encode(o, b):any;
    }

// [返回]保存迷宫信息
    module SaveInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求] 重置迷宫
    module ResetMiGongReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ResetMiGongReq;
        function encode(o, b):any;
    }

// [请求] 重置迷宫  返回迷宫面板信息
    module ResetMiGongRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]根据迷宫def和层数 和获得迷宫信息
    module GetMiGongInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMiGongInfoReq;
        function encode(o, b):any;
    }

// [返回]根据迷宫def和层数 和获得迷宫信息
    module GetMiGongInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]迷宫扫荡
    module SweepMiGongReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepMiGongReq;
        function encode(o, b):any;
    }

// [返回]迷宫扫荡
    module SweepMiGongRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抓宠面板
    module ZhuaChongPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuaChongPanelReq;
        function encode(o, b):any;
    }

// [反馈]抓宠面板
    module ZhuaChongPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]扔石头
    module RengShiTouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RengShiTouReq;
        function encode(o, b):any;
    }

// [返回]扔石头
    module RengShiTouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抓宠物
    module ZhuaChongWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuaChongWuReq;
        function encode(o, b):any;
    }

// [返回]抓宠物
    module ZhuaChongWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]遇到敌人
    module YuDiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YuDiReq;
        function encode(o, b):any;
    }

// [返回]遇到敌人
    module YuDiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领宠物
    module LingChongWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LingChongWuReq;
        function encode(o, b):any;
    }

// [请求]领宠物
    module LingChongWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]扔石头
    module YeWaiRengShiTouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YeWaiRengShiTouReq;
        function encode(o, b):any;
    }

// [返回]扔石头
    module YeWaiRengShiTouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抓宠物
    module YeWaiZhuaChongWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YeWaiZhuaChongWuReq;
        function encode(o, b):any;
    }

// [返回]抓宠物
    module YeWaiZhuaChongWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抓宠面板
    module YeWaiZhuaChongPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YeWaiZhuaChongPanelReq;
        function encode(o, b):any;
    }

// [反馈]抓宠面板
    module YeWaiZhuaChongPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抓宠房间面板
    module ZhuaChongRoomPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuaChongRoomPanelReq;
        function encode(o, b):any;
    }

// [反馈]抓宠房间面板
    module ZhuaChongRoomPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买抓宠中心探索次数
    module BuySearchTimesReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuySearchTimesReq;
        function encode(o, b):any;
    }

// [反馈]购买抓宠中心探索次数
    module BuySearchTimesRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [反馈]获取抓宠助手精灵列表
    module GetZhuaChongZhuShouHeroListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuaChongZhuShouHeroListReq;
        function encode(o, b):any;
    }

// [反馈]获取抓宠助手精灵列表
    module GetZhuaChongZhuShouHeroListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]修改抓宠助手状态
    module UpdateZhuaChongZhuShouAutoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateZhuaChongZhuShouAutoReq;
        function encode(o, b):any;
    }

// [反馈]购买抓宠中心探索次数
    module UpdateZhuaChongZhuShouAutoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]打开流量转盘面板
    module GetLiuLiangPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLiuLiangPanelReq;
        function encode(o, b):any;
    }

// [反馈]打开流量转盘面板
    module GetLiuLiangPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩转盘 如果玩家摇到流量并且已经保存了手机号,直接发奖 如果没保存手机号  打开手机号面板
    module PlayTurntableReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayTurntableReq;
        function encode(o, b):any;
    }

// [反馈]玩转盘
    module PlayTurntableRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]保存手机号 这个消息的出发情况:玩家摇到流量 但是没有保存手机号 ,保存手机号后发奖
    module SavePhoneNOReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SavePhoneNOReq;
        function encode(o, b):any;
    }

// [反馈]保存手机号
    module SavePhoneNORsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 皮卡丘run面板信息
    module PikaChuRunPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PikaChuRunPanelReq;
        function encode(o, b):any;
    }

// 皮卡丘run面板信息
    module PikaChuRunPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 皮卡丘run押注
    module PikaChuRunBetReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PikaChuRunBetReq;
        function encode(o, b):any;
    }

// 皮卡丘run押注
    module PikaChuRunBetRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 皮卡丘run加buff次数
    module PikaChuRunBuffReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PikaChuRunBuffReq;
        function encode(o, b):any;
    }

// 皮卡丘run加buff次数
    module PikaChuRunBuffRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 开始比赛后刷新下状态
    module PikaChuRunRefreshReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PikaChuRunRefreshReq;
        function encode(o, b):any;
    }

// 开始比赛后刷新下状态
    module PikaChuRunRefreshRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角培养面板
    module ZhuJuePeiYangPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuJuePeiYangPanelReq;
        function encode(o, b):any;
    }

// [返回]主角培养面板
    module ZhuJuePeiYangPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主角培养
    module GetZhuJuePeiYangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZhuJuePeiYangReq;
        function encode(o, b):any;
    }

// [返回]主角培养
    module GetZhuJuePeiYangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家战阵面板
    module ZhanZhenPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhanZhenPanelReq;
        function encode(o, b):any;
    }

// [反馈]玩家战阵面板 返回玩家当前徽章id
    module ZhanZhenPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]用道具升级战阵
    module UpGradeZhanZhenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpGradeZhanZhenReq;
        function encode(o, b):any;
    }

// [反馈]用道具升级战阵
    module UpGradeZhanZhenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]重置道馆
    module ResetDaoGuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ResetDaoGuanReq;
        function encode(o, b):any;
    }

// [反馈]重置道馆
    module ResetDaoGuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]道馆扫荡预览
    module SweepDaGuanShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepDaGuanShowReq;
        function encode(o, b):any;
    }

// [返回]道馆扫荡预览
    module SweepDaGuanShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]道馆扫荡确认
    module SweepDaGuanEnsureReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepDaGuanEnsureReq;
        function encode(o, b):any;
    }

// [返回]道馆扫荡确认
    module SweepDaGuanEnsureRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本面板
    module ZuDuiFuBenPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuDuiFuBenPanelReq;
        function encode(o, b):any;
    }

// [返回]组队副本面板
    module ZuDuiFuBenPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]创建副本队伍
    module CreateFuBenDuiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CreateFuBenDuiWuReq;
        function encode(o, b):any;
    }

// [返回]组队面板
    module CreateFuBenDuiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]加入队伍
    module JoinDuiWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JoinDuiWuReq;
        function encode(o, b):any;
    }

// [返回]加入队伍
    module JoinDuiWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本踢人
    module ZuDuiFuBenTiRenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuDuiFuBenTiRenReq;
        function encode(o, b):any;
    }

// [返回]组队副本踢人
    module ZuDuiFuBenTiRenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]修改限制战力
    module ModifyLimitFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ModifyLimitFightReq;
        function encode(o, b):any;
    }

// [返回]修改限制战力
    module ModifyLimitFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本 准备
    module ZuDuiFuBenReadyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuDuiFuBenReadyReq;
        function encode(o, b):any;
    }

// [返回]组队副本 准备
    module ZuDuiFuBenReadyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]退出队伍
    module LeaveTeamReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LeaveTeamReq;
        function encode(o, b):any;
    }

// [返回]退出队伍
    module LeaveTeamRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本 打
    module ZuDuiFuBenFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuDuiFuBenFightReq;
        function encode(o, b):any;
    }

// [返回]组队副本 打
    module ZuDuiFuBenFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本成员界面  去队长服务器找队伍 找不到 有可能是你停服的时候 队长把你踢了 如果找不到 要做相应处理
    module ZuDuiFuBenMemberListPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuDuiFuBenMemberListPanelReq;
        function encode(o, b):any;
    }

// [返回]组队面板
    module ZuDuiFuBenMemberListPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]查看组队副本成员精灵信息
    module LoadZuDuiFuBenMemberHeroInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadZuDuiFuBenMemberHeroInfoReq;
        function encode(o, b):any;
    }

// [请求]查看组队副本成员精灵信息
    module LoadZuDuiFuBenMemberHeroInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]根据房间(队伍号)号搜索队伍
    module SearchTeamByRoomIdReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SearchTeamByRoomIdReq;
        function encode(o, b):any;
    }

// [请求]根据房间(队伍号)号搜索队伍
    module SearchTeamByRoomIdRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [推送] 玩家在线被踢 推送这个状态
    module ZuDuiFuBenStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]变换出场顺序
    module ChangeIndexReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChangeIndexReq;
        function encode(o, b):any;
    }

// [返回] 变换出场顺序
    module ChangeIndexRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]进入战斗界面 组队副本 打的动画信息   打的时候前端除了发打 还要发这个 返回当前播放到哪个关卡
    module TeamFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TeamFightReq;
        function encode(o, b):any;
    }

// [请求]进入战斗界面 组队副本 打的动画信息   打的时候前端除了发打 还要发这个 返回当前播放到哪个关卡
    module TeamFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]异步请求关卡信息
    module PushTeamGuanqiaFightInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PushTeamGuanqiaFightInfoReq;
        function encode(o, b):any;
    }

// [请求]异步请求关卡信息
    module PushTeamGuanqiaFightInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本排行榜
    module GetZuDuiFuBenPaiHangBangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZuDuiFuBenPaiHangBangReq;
        function encode(o, b):any;
    }

// 组队副本排行榜
    module GetZuDuiFuBenPaiHangBangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本聊天
    module SendZuDuiFuBenChatMessageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SendZuDuiFuBenChatMessageReq;
        function encode(o, b):any;
    }

// 组队副本聊天
    module SendZuDuiFuBenChatMessageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]向其他服务器请求 组队副本奖励的的信息
    module GetTeamGuanqiaFightInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTeamGuanqiaFightInfoReq;
        function encode(o, b):any;
    }

// [请求]向其他服务器请求 组队副本奖励的的信息
    module GetTeamGuanqiaFightInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]向队长服发送组队副本聊天
    module SendZuDuiFuBenChatMessageToMemberReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SendZuDuiFuBenChatMessageToMemberReq;
        function encode(o, b):any;
    }

// 向队长服发送组队副本聊天
    module SendZuDuiFuBenChatMessageToMemberRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]组队副本匹配 开始
    module ZuDuiFuBenPiPeiKaiShiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuDuiFuBenPiPeiKaiShiReq;
        function encode(o, b):any;
    }

// [请求]组队副本匹配 取消
    module ZuDuiFuBenPiPeiQuXiaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuDuiFuBenPiPeiQuXiaoReq;
        function encode(o, b):any;
    }

// 组队副本聊天红点
    module PushZuDuiFuBenChatRedReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PushZuDuiFuBenChatRedReq;
        function encode(o, b):any;
    }

// 组队副本聊天红点
    module PushZuDuiFuBenChatRedRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获得组队副本
    module GetZuDuiFuBenStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetZuDuiFuBenStatusReq;
        function encode(o, b):any;
    }

// 组队副本聊天红点
    module GetZuDuiFuBenStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 组队副本战斗记录
    module ZuDuFuBenFightLogReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuDuFuBenFightLogReq;
        function encode(o, b):any;
    }

// 组队副本战斗记录
    module ZuDuFuBenFightLogRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 组队喊话邀请
    module HanHuaYaoQingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HanHuaYaoQingReq;
        function encode(o, b):any;
    }

// 组队喊话邀请
    module HanHuaYaoQingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]骑技面板
    module ChiBangTuPoPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChiBangTuPoPanelReq;
        function encode(o, b):any;
    }

// [返回]骑技面板
    module ChiBangTuPoPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]翅膀突破
    module ChiBangTuPoTuPoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChiBangTuPoTuPoReq;
        function encode(o, b):any;
    }

// [返回]骑技面板
    module ChiBangTuPoTuPoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]翅膀突破  满级预览
    module ChiBangTuPoManJieYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChiBangTuPoManJieYuLanReq;
        function encode(o, b):any;
    }

// [请求]翅膀突破  满级预览
    module ChiBangTuPoManJieYuLanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探险面板
    module TanXianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TanXianPanelReq;
        function encode(o, b):any;
    }

// [返回]探险面板
    module TanXianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]升级探险
    module ShengJiTanXianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShengJiTanXianReq;
        function encode(o, b):any;
    }

// [返回]升级探险
    module ShengJiTanXianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]升级探险属性
    module ShengJiShuXingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShengJiShuXingReq;
        function encode(o, b):any;
    }

// [返回]升级探险属性
    module ShengJiShuXingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取探险奖励
    module GetTanXianRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTanXianRewardReq;
        function encode(o, b):any;
    }

// [返回]领取探险奖励
    module GetTanXianRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]展示探险奖励
    module ShowTanXianRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowTanXianRewardReq;
        function encode(o, b):any;
    }

// [返回]展示探险属性
    module ShowTanXianRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]展示探险精灵列表
    module GetTanXianHeroListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTanXianHeroListReq;
        function encode(o, b):any;
    }

// [返回]展示探险精灵列表
    module GetTanXianHeroListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探险精灵上阵
    module MakeTanXianHeroInOutReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MakeTanXianHeroInOutReq;
        function encode(o, b):any;
    }

// [返回]探险精灵上阵
    module MakeTanXianHeroInOutRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探险精灵召唤
    module MakeNewTanXianHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MakeNewTanXianHeroReq;
        function encode(o, b):any;
    }

// [返回]探险精灵召唤
    module MakeNewTanXianHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]探险精灵召升级
    module ShengJiTanXianHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShengJiTanXianHeroReq;
        function encode(o, b):any;
    }

// [返回]探险精灵升级
    module ShengJiTanXianHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】礼包信息
    module GetGiftPackageListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGiftPackageListReq;
        function encode(o, b):any;
    }

// 【反馈】礼包信息
    module GetGiftPackageListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买礼包
    module BuyGiftPackageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyGiftPackageReq;
        function encode(o, b):any;
    }

// 【反馈】购买礼包
    module BuyGiftPackageRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]突发事件面板
    module TuFaShiJianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TuFaShiJianPanelReq;
        function encode(o, b):any;
    }

// [返回]突发事件面板
    module TuFaShiJianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领取突发事件的奖励
    module TuFaShiJianLingJiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TuFaShiJianLingJiangReq;
        function encode(o, b):any;
    }

// [返回]领取突发事件的奖励
    module TuFaShiJianLingJiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】查看自身排位赛数据
    module LoadMyRankingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadMyRankingReq;
        function encode(o, b):any;
    }

// 【反馈】查看自身排位赛数据
    module LoadMyRankingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】排位赛战斗
    module RankingFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RankingFightReq;
        function encode(o, b):any;
    }

// 【反馈】排位赛战斗
    module RankingFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】排位赛战斗奖励结算
    module MakeRewardOnRankingFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MakeRewardOnRankingFightReq;
        function encode(o, b):any;
    }

// 【反馈】排位赛战斗奖励结算
    module MakeRewardOnRankingFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】查看战区数据
    module GetRankingWarZoneInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRankingWarZoneInfoReq;
        function encode(o, b):any;
    }

// 【反馈】查看战区数据
    module GetRankingWarZoneInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】查看排位赛他人数据
    module LoadRankingOtherPlayerInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadRankingOtherPlayerInfoReq;
        function encode(o, b):any;
    }

// 【反馈】查看排位赛他人数据
    module LoadRankingOtherPlayerInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】排位赛记录列表
    module GetMatchRecordListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetMatchRecordListReq;
        function encode(o, b):any;
    }

// 【反馈】排位赛记录列表
    module GetMatchRecordListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】排位赛战斗观看
    module GetRankingRecordFightJsonReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRankingRecordFightJsonReq;
        function encode(o, b):any;
    }

// 【反馈】排位赛战斗观看
    module GetRankingRecordFightJsonRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】选择等级段(等级段最小等级)
    module ChooseLevelPartReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChooseLevelPartReq;
        function encode(o, b):any;
    }

// 【请求】选择等级段(等级段最小等级)
    module ChooseLevelPartRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】查看排位赛王者大师的排行
    module GetRankingPaiHangInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRankingPaiHangInfoReq;
        function encode(o, b):any;
    }

// 【反馈】查看排位赛王者大师的排行
    module GetRankingPaiHangInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】排位赛战斗
    module RankingFightSomeoneReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RankingFightSomeoneReq;
        function encode(o, b):any;
    }

// 【反馈】排位赛战斗
    module RankingFightSomeoneRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】转生排位赛战斗
    module RankingFightSomeoneZSReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RankingFightSomeoneZSReq;
        function encode(o, b):any;
    }

// 【反馈】转生排位赛战斗
    module RankingFightSomeoneZSRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】购买排位赛挑战次数
    module BuyPaiWeiFightCountReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyPaiWeiFightCountReq;
        function encode(o, b):any;
    }

// 【反馈】购买排位赛挑战次数
    module BuyPaiWeiFightCountRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】排位赛扫荡
    module SweepMatchRankingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepMatchRankingReq;
        function encode(o, b):any;
    }

// 【反馈】排位赛扫荡
    module SweepMatchRankingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获得玩家英雄列表
    module GetPlayerHeroListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPlayerHeroListReq;
        function encode(o, b):any;
    }

// 【反馈】获得玩家英雄列表
    module GetPlayerHeroListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】招募英雄
    module GetHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHeroReq;
        function encode(o, b):any;
    }

// 【反馈】招募英雄
    module GetHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】残魂招募英雄
    module GetHeroBySoulReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHeroBySoulReq;
        function encode(o, b):any;
    }

// 【反馈】残魂招募英雄
    module GetHeroBySoulRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】英雄变化
    module HeroChangeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroChangeReq;
        function encode(o, b):any;
    }

// 【反馈】英雄变化
    module HeroChangeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】英雄阵容调整
    module HeroTakePositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroTakePositionReq;
        function encode(o, b):any;
    }

// 【反馈】英雄阵容调整
    module HeroTakePositionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】英雄上阵位置交换
    module HeroChangePositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroChangePositionReq;
        function encode(o, b):any;
    }

// 【反馈】英雄上阵位置交换，返回新的战阵位置武将信息
    module HeroChangePositionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】协作英雄上阵
    module HeroAssistTakePositionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroAssistTakePositionReq;
        function encode(o, b):any;
    }

// 【更新】返回新的协作武将位置信息
    module HeroAssistListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】自动补充协作英雄
    module HeroAssistAddHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroAssistAddHeroReq;
        function encode(o, b):any;
    }

// 【请求】一键协作
    module HeroAssistAutoAddReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroAssistAutoAddReq;
        function encode(o, b):any;
    }

// 【请求】英雄回收预览
    module HeroSendBackRewardViewReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroSendBackRewardViewReq;
        function encode(o, b):any;
    }

// 【请求】英雄回收预览
    module HeroSendBackRewardViewRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]点亮命星
    module DianLiangMingXingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.DianLiangMingXingReq;
        function encode(o, b):any;
    }

// [返回]点亮命星
    module DianLiangMingXingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】回收武将
    module HeroSendBackReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroSendBackReq;
        function encode(o, b):any;
    }

// 【请求】回收武将
    module HeroSendBackRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】武将重生
    module HeroChongShengReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroChongShengReq;
        function encode(o, b):any;
    }

// 【请求】武将重生
    module HeroChongShengRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】武将属性计算显示值
    module HeroPropertyShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroPropertyShowReq;
        function encode(o, b):any;
    }

// 【反馈】武将属性计算显示值
    module HeroPropertyShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活协作
    module JiHuoXieZuoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JiHuoXieZuoReq;
        function encode(o, b):any;
    }

// 【请求】武将重生预览
    module HeroChongShengYuLanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroChongShengYuLanReq;
        function encode(o, b):any;
    }

// 【请求】武将重生预览
    module HeroChongShengYuLanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】超进化升级
    module ChaoJinHuaShengJiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChaoJinHuaShengJiReq;
        function encode(o, b):any;
    }

// 【请求】超进化升级
    module ChaoJinHuaShengJiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】超进化选择和切换
    module ChaoJinHuaXuanZeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChaoJinHuaXuanZeReq;
        function encode(o, b):any;
    }

// 【请求】超进化选择和切换
    module ChaoJinHuaXuanZeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】展示超进化属性
    module ChaoJinHuaShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChaoJinHuaShowReq;
        function encode(o, b):any;
    }

// 【反馈】展示超进化属性
    module ChaoJinHuaShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】展示超进化属性
    module ChaoJinHuaZhuanHuanShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChaoJinHuaZhuanHuanShowReq;
        function encode(o, b):any;
    }

// 【反馈】展示超进化属性
    module ChaoJinHuaZhuanHuanShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】闪光喷漆展示属性
    module ShanGuangPenQiShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShanGuangPenQiShowReq;
        function encode(o, b):any;
    }

// 【反馈】闪光喷漆展示属性
    module ShanGuangPenQiShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】闪光喷漆
    module ShanGuangPenQiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShanGuangPenQiReq;
        function encode(o, b):any;
    }

// 【请求】闪光喷漆
    module ShanGuangPenQiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】精灵洗练
    module HeroXiLianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroXiLianReq;
        function encode(o, b):any;
    }

// 【请求】精灵洗练
    module HeroXiLianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版种族值升级
    module NewZhongZuZhiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewZhongZuZhiReq;
        function encode(o, b):any;
    }

// 【反馈】新版种族值升级
    module NewZhongZuZhiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】新版种族值突破详情面板
    module NewZhongZuZhiTuPoInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewZhongZuZhiTuPoInfoReq;
        function encode(o, b):any;
    }

// 【反馈】新版种族值突破详情面板
    module NewZhongZuZhiTuPoInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】预览新版种族值
    module YuLanNewZhongZuZhiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YuLanNewZhongZuZhiReq;
        function encode(o, b):any;
    }

// 【反馈】预览新版种族值
    module YuLanNewZhongZuZhiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】新版种族值新增武将更新前端
    module NewWuJiangZhongZuZhiTuPoInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】加载十连抽扭蛋图鉴列表
    module GetShilianXianshiShidaiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShilianXianshiShidaiListReq;
        function encode(o, b):any;
    }

// 【反馈】加载十连抽扭蛋图鉴列表
    module GetShilianXianshiShidaiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取属性信息列表
    module GetShuXingListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShuXingListReq;
        function encode(o, b):any;
    }

// 【反馈】获取属性信息列表
    module GetShuXingListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】英雄显示装备样式设置
    module HeroSetShowEquipFlagReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeroSetShowEquipFlagReq;
        function encode(o, b):any;
    }

// 【反馈】英雄显示装备样式设置
    module HeroSetShowEquipFlagRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】记录图鉴已上阵Id
    module AddActiveAniSignIDReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AddActiveAniSignIDReq;
        function encode(o, b):any;
    }

// 【请求】疯狂十连抽面板信息
    module GetCrazyGachaTenPullsPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetCrazyGachaTenPullsPanelReq;
        function encode(o, b):any;
    }

// 【反馈】疯狂十连抽面板信息
    module GetCrazyGachaTenPullsPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】疯狂十连抽
    module CrazyGachaTenPullsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CrazyGachaTenPullsReq;
        function encode(o, b):any;
    }

// 【反馈】疯狂十连抽
    module CrazyGachaTenPullsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】疯狂十连抽领取奖励
    module GetRewardFromCrazyGachaTenPullsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRewardFromCrazyGachaTenPullsReq;
        function encode(o, b):any;
    }

// 【反馈】疯狂十连抽领取奖励
    module GetRewardFromCrazyGachaTenPullsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】种族抽卡
    module ZhongzuQiyueGachaReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhongzuQiyueGachaReq;
        function encode(o, b):any;
    }

// 【反馈】种族抽卡
    module ZhongzuQiyueGachaRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】心愿神魔面板
    module WishHeroPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WishHeroPanelReq;
        function encode(o, b):any;
    }

// 【反馈】心愿神魔面板
    module WishHeroPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】指定心愿神魔
    module UpdateWishHeroReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UpdateWishHeroReq;
        function encode(o, b):any;
    }

// 【反馈】指定心愿神魔
    module UpdateWishHeroRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】积分契约
    module GachaCostScoreReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GachaCostScoreReq;
        function encode(o, b):any;
    }

// 【反馈】积分契约
    module GachaCostScoreRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】积分兑换奖池
    module ShowWishJifenShenmoAllReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShowWishJifenShenmoAllReq;
        function encode(o, b):any;
    }

// 【反馈】积分兑换奖池
    module ShowWishJifenShenmoAllRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载世界BOSS信息
    module GetBossInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBossInfoReq;
        function encode(o, b):any;
    }

// 【反馈】加载世界BOSS信息
    module GetBossInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】攻击世界BOSS
    module BossFightReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BossFightReq;
        function encode(o, b):any;
    }

// 【反馈】攻击世界BOSS
    module BossFightRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】征讨令购买
    module TokenBuyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TokenBuyReq;
        function encode(o, b):any;
    }

// 【反馈】征讨令购买
    module TokenBuyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】世界BOSS红点信息
    module BossTipsInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BossTipsInfoReq;
        function encode(o, b):any;
    }

// 【反馈】世界BOSS红点信息
    module BossTipsInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】立即消除世界BOSS普通战斗冷却时间
    module RemoveBossFightCDReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RemoveBossFightCDReq;
        function encode(o, b):any;
    }

// 【反馈】立即消除世界BOSS普通战斗冷却时间
    module RemoveBossFightCDRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】公会战鼓舞
    module SGongHuiZhanGuWuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SGongHuiZhanGuWuReq;
        function encode(o, b):any;
    }

// 【反馈】公会战鼓舞
    module SGongHuiZhanGuWuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】公会战邮件奖励
    module SGongHuiZhanEmailRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SGongHuiZhanEmailRewardReq;
        function encode(o, b):any;
    }

// [请求]获得神器列表
    module GetShenQiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShenQiListReq;
        function encode(o, b):any;
    }

// [返回]获得神器列表
    module GetShenQiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]神器升级
    module ShenQiLevelUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenQiLevelUpReq;
        function encode(o, b):any;
    }

// [返回]神器升级
    module ShenQiLevelUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]神器洗练
    module ShenQiXiLianReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShenQiXiLianReq;
        function encode(o, b):any;
    }

// [返回]神器洗练
    module ShenQiXiLianRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】英雄神器穿上、卸下
    module PutShenQiOnOffReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PutShenQiOnOffReq;
        function encode(o, b):any;
    }

// 【反馈】英雄神器穿上、卸下
    module PutShenQiOnOffRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】锁定属性
    module ShuXingSuoDingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShuXingSuoDingReq;
        function encode(o, b):any;
    }

// 【反馈】锁定属性
    module ShuXingSuoDingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】属性替换
    module ShuXingTiHuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShuXingTiHuanReq;
        function encode(o, b):any;
    }

// 【反馈】属性替换
    module ShuXingTiHuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]装备商店列表
    module ZhuanBeiShopGoodsListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuanBeiShopGoodsListReq;
        function encode(o, b):any;
    }

// [反馈]装备商店列表
    module ZhuanBeiShopGoodsListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买装备商店商品
    module BuyZhuanBeiShouShopGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyZhuanBeiShouShopGoodsReq;
        function encode(o, b):any;
    }

// [返回]购买商品
    module BuyZhuanBeiShouShopGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]精灵商店列表
    module JingLingShopGoodsListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.JingLingShopGoodsListReq;
        function encode(o, b):any;
    }

// [反馈]精灵商店列表
    module JingLingShopGoodsListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买精灵商店商品
    module BuyJingLingShouShopGoodsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyJingLingShouShopGoodsReq;
        function encode(o, b):any;
    }

// [返回]购买精灵商店商品
    module BuyJingLingShouShopGoodsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]刷新精灵商品
    module RefreshJingLingShopReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RefreshJingLingShopReq;
        function encode(o, b):any;
    }

// [返回]刷新商品
    module RefreshJingLingShopRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家威名面板信息
    module GetWeimingLevelInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWeimingLevelInfoReq;
        function encode(o, b):any;
    }

// [返回]玩家威名面板信息
    module GetWeimingLevelInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家威名升级进阶
    module WeimingLevelUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WeimingLevelUpReq;
        function encode(o, b):any;
    }

// [返回]玩家威名升级进阶
    module WeimingLevelUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家威名技能信息
    module GetWeimingSkillInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWeimingSkillInfoReq;
        function encode(o, b):any;
    }

// [返回]玩家威名技能信息
    module GetWeimingSkillInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家威名升级技能
    module WeimingSkillLevelUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WeimingSkillLevelUpReq;
        function encode(o, b):any;
    }

// [返回]玩家威名升级技能
    module WeimingSkillLevelUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]玩家威名充值面板信息
    module GetWeimingRechargeInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWeimingRechargeInfoReq;
        function encode(o, b):any;
    }

// [返回]玩家威名充值面板信息
    module GetWeimingRechargeInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求boss等级列表
    module GetBossLevelListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBossLevelListReq;
        function encode(o, b):any;
    }

// 【反馈】请求boss列表
    module GetBossLevelListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求boss列表
    module GetBossListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBossListReq;
        function encode(o, b):any;
    }

// 【反馈】请求boss列表
    module GetBossListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】进入地图
    module EnterBossMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EnterBossMapReq;
        function encode(o, b):any;
    }

// 【反馈】进入地图
    module EnterBossMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】离开地图
    module PlayerLeaveMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerLeaveMapReq;
        function encode(o, b):any;
    }

// 【反馈】离开地图
    module PlayerLeaveMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家移动
    module PlayerMoveInMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerMoveInMapReq;
        function encode(o, b):any;
    }

// 【反馈】玩家移动广播
    module PlayerMoveInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家发起攻击
    module PlayerAttackInMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerAttackInMapReq;
        function encode(o, b):any;
    }

// 【反馈】玩家发起攻击
    module PlayerAttackInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】玩家复活
    module PlayerRecallHPInMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerRecallHPInMapReq;
        function encode(o, b):any;
    }

// 【反馈】玩家复活
    module PlayerRecallHPInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】ping值
    module CheckClientPingTimeInMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CheckClientPingTimeInMapReq;
        function encode(o, b):any;
    }

// 【反馈】ping值
    module CheckClientPingTimeInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】推送攻击信息
    module PushAttackInfoInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播玩家移动路径
    module BroadcastPlayerMoveInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播玩家当前坐标纠正
    module BroadcastPlayerLocationFixInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播新玩家进入场景了
    module BroadcastPlayerEnterSceneInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播怪物信息
    module BroadcastMonsterInfoInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播玩家离开场景
    module BroadcastPlayerLeaveSceneInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播归属权变化
    module BroadcastRewardPersonChangeInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】广播变化
    module BroadcastPersonChangeInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】搜人
    module SearchPlayerInMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SearchPlayerInMapReq;
        function encode(o, b):any;
    }

// 【反馈】搜人
    module SearchPlayerInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 添加仇人
    module AddChouRenInMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AddChouRenInMapReq;
        function encode(o, b):any;
    }

// 【反馈】添加仇人
    module AddChouRenInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 购买个人boss挑战次数
    module BuyGeRenBossJoinCountReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BuyGeRenBossJoinCountReq;
        function encode(o, b):any;
    }

// 【反馈】购买个人boss挑战次数
    module BuyGeRenBossJoinCountRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】怪物列表额外推送
    module BossLevelListPushRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】当前可打的最大boss
    module MaxBossCanFightPushRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】boss奖励双倍
    module BossRewardDoubleReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BossRewardDoubleReq;
        function encode(o, b):any;
    }

// 【反馈】boss奖励双倍
    module BossRewardDoubleRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】请求boss援助
    module GetBossYuanZhuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBossYuanZhuReq;
        function encode(o, b):any;
    }

// 【反馈】请求boss援助
    module GetBossYuanZhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】推送boss援助列表
    module PushBossYuanZhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】推送加血列表
    module PushPlayerAddHpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】推送加血列表
    module PushYuanZhuRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 修改boss状态
    module BossStatusChangeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BossStatusChangeReq;
        function encode(o, b):any;
    }

// 【反馈】推送boss时间变化
    module PushBossTimeChangeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【跨服反馈】世界战场，剩余次数变化的推送
    module BossLeftCountInfoPushRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【跨服推送】世界战场，Boss剩余HP变化的推送
    module BossHpInfoPushRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 施放秘法证书
    module UseMifaZhengshuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UseMifaZhengshuReq;
        function encode(o, b):any;
    }

// 【反馈】施放秘法证书
    module UseMifaZhengshuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】本服Boss世界战场扫荡
    module SweepBossInMapReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepBossInMapReq;
        function encode(o, b):any;
    }

// 【反馈】本服Boss世界战场扫荡
    module SweepBossInMapRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]攻坚战道馆列表
    module GongJianZhanDaoGuanListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongJianZhanDaoGuanListReq;
        function encode(o, b):any;
    }

// [请求]攻坚战道馆列表
    module GongJianZhanDaoGuanListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]进入攻坚战
    module EnterGongJianZhanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.EnterGongJianZhanReq;
        function encode(o, b):any;
    }

// [请求]进入攻坚战
    module EnterGongJianZhanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]打攻坚战
    module FightGongJianZhanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightGongJianZhanReq;
        function encode(o, b):any;
    }

// [请求]打攻坚战
    module FightGongJianZhanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]首届馆主
    module GongJianZhanShouJieGuanZhuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongJianZhanShouJieGuanZhuReq;
        function encode(o, b):any;
    }

// [请求]首届馆主
    module GongJianZhanShouJieGuanZhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会攻坚战排行
    module GongHuiGongJianZhanPaiHangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongHuiGongJianZhanPaiHangReq;
        function encode(o, b):any;
    }

// [返回]公会攻坚战排行
    module GongHuiGongJianZhanPaiHangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]攻坚战记录
    module GongJianZhanRecordReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongJianZhanRecordReq;
        function encode(o, b):any;
    }

// [请求]攻坚战记录
    module GongJianZhanRecordRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]攻坚战指派列表
    module GongJianZhanZhiPaiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongJianZhanZhiPaiListReq;
        function encode(o, b):any;
    }

// [请求]攻坚战指派列表
    module GongJianZhanZhiPaiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]攻坚战成员贡献列表
    module ChengYuanGongXianListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChengYuanGongXianListReq;
        function encode(o, b):any;
    }

// [请求]攻坚战成员贡献列表
    module ChengYuanGongXianListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]邮件
    module GongJianZhanEmailReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GongJianZhanEmailReq;
        function encode(o, b):any;
    }

// [返回]邮件
    module GongJianZhanEmailRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会道馆援助
    module GonghuiDaoguanYuanzhuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GonghuiDaoguanYuanzhuReq;
        function encode(o, b):any;
    }

// [返回]公会道馆援助
    module GonghuiDaoguanYuanzhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]公会道馆发起援助
    module GonghuiDaoguanFaQiYuanzhuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GonghuiDaoguanFaQiYuanzhuReq;
        function encode(o, b):any;
    }

// [返回]公会道馆发起援助
    module GonghuiDaoguanFaQiYuanzhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]磨炼面板
    module MoLianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoLianPanelReq;
        function encode(o, b):any;
    }

// [返回]磨炼面板
    module MoLianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]磨炼排行
    module MoLianPaiHangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoLianPaiHangReq;
        function encode(o, b):any;
    }

// [返回]磨炼排行
    module MoLianPaiHangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】磨炼战斗
    module MoLianFightPVEReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoLianFightPVEReq;
        function encode(o, b):any;
    }

// 【反馈】磨炼战斗
    module MoLianFightPVERsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】磨炼隐藏任务战斗
    module MoLianRenWuFightPVEReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoLianRenWuFightPVEReq;
        function encode(o, b):any;
    }

// 【反馈】磨炼隐藏任务战斗
    module MoLianRenWuFightPVERsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]领这界面上 第三关的奖励
    module LingJiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LingJiangReq;
        function encode(o, b):any;
    }

// [返回]领这界面上 第三关的奖励
    module LingJiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]随机buff界面
    module RandomMoLianBuffPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RandomMoLianBuffPanelReq;
        function encode(o, b):any;
    }

// [返回]随机buff界面
    module RandomMoLianBuffPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]选择buff
    module ChooseMoLianBuffReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChooseMoLianBuffReq;
        function encode(o, b):any;
    }

// [返回]选择buff
    module ChooseMoLianBuffRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]刷新buff
    module RefreshMoLianBuffReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RefreshMoLianBuffReq;
        function encode(o, b):any;
    }

// [返回]刷新buff
    module RefreshMoLianBuffRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】磨炼战斗简单
    module MoLianFightNormalReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoLianFightNormalReq;
        function encode(o, b):any;
    }

// 【反馈】磨炼战斗简单
    module MoLianFightNormalRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】磨炼战斗回放
    module MoLianFightRecordReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoLianFightRecordReq;
        function encode(o, b):any;
    }

// 【反馈】磨炼战斗回放
    module MoLianFightRecordRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】一键战斗
    module MoLianFightOneKeyReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoLianFightOneKeyReq;
        function encode(o, b):any;
    }

// 【反馈】一键战斗
    module MoLianFightOneKeyRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】坐骑翅膀面板
    module ZuoqiPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuoqiPanelReq;
        function encode(o, b):any;
    }

// 【反馈】坐骑翅膀面板
    module ZuoqiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】坐骑翅膀激活
    module ZuoqiJihuoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuoqiJihuoReq;
        function encode(o, b):any;
    }

// 【反馈】坐骑翅膀激活
    module ZuoqiJihuoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】坐骑翅膀升星面板
    module ZuoqiShengxingPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuoqiShengxingPanelReq;
        function encode(o, b):any;
    }

// 【反馈】坐骑翅膀升星面板
    module ZuoqiShengxingPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】坐骑翅膀升星
    module ZuoqiShengxingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuoqiShengxingReq;
        function encode(o, b):any;
    }

// 【反馈】坐骑翅膀升星
    module ZuoqiShengxingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】坐骑翅膀切换使用
    module ZuoqiSwtichReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZuoqiSwtichReq;
        function encode(o, b):any;
    }

// 【反馈】坐骑翅膀切换使用
    module ZuoqiSwtichRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取主角页面信息
    module GetActorInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetActorInfoReq;
        function encode(o, b):any;
    }

// 【反馈】获取主角页面信息
    module GetActorInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】获取主角免费时装
    module GetAllFreeFashionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAllFreeFashionReq;
        function encode(o, b):any;
    }

// 【反馈】获取主角免费时装
    module GetAllFreeFashionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取主角免费时装
    module GetFreeFashionRewardsReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFreeFashionRewardsReq;
        function encode(o, b):any;
    }

// 【反馈】领取主角免费时装
    module GetFreeFashionRewardsRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】头衔面板消息
    module TouxianPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TouxianPanelReq;
        function encode(o, b):any;
    }

// 【反馈】头衔面板消息
    module TouxianPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】头衔晋升
    module TouxianLevelUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TouxianLevelUpReq;
        function encode(o, b):any;
    }

// 【反馈】头衔晋升
    module TouxianLevelUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取头衔奖励
    module ReceiveTouxianRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReceiveTouxianRewardReq;
        function encode(o, b):any;
    }

// 【反馈】领取头衔奖励
    module ReceiveTouxianRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】主角某子系统皮肤面板
    module RoleSubSystemPifuPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RoleSubSystemPifuPanelReq;
        function encode(o, b):any;
    }

// 【反馈】主角某子系统皮肤面板
    module RoleSubSystemPifuPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】主角某子系统皮肤激活
    module RoleSubSystemPifuActiveReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RoleSubSystemPifuActiveReq;
        function encode(o, b):any;
    }

// 【反馈】主角某子系统皮肤激活
    module RoleSubSystemPifuActiveRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】主角某子系统皮肤升星面板
    module RoleSubSystemPifuStarLevelUpPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RoleSubSystemPifuStarLevelUpPanelReq;
        function encode(o, b):any;
    }

// 【反馈】主角某子系统皮肤升星面板
    module RoleSubSystemPifuStarLevelUpPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】主角某子系统皮肤升星操作
    module RoleSubSystemPifuStarLevelUpReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RoleSubSystemPifuStarLevelUpReq;
        function encode(o, b):any;
    }

// 【反馈】主角某子系统皮肤升星操作
    module RoleSubSystemPifuStarLevelUpRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】主角某子系统皮肤切换使用
    module RoleSubSystemPifuSwitchReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RoleSubSystemPifuSwitchReq;
        function encode(o, b):any;
    }

// 【反馈】主角某子系统皮肤切换使用
    module RoleSubSystemPifuSwitchRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】命星系统面板
    module FateStarPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FateStarPanelReq;
        function encode(o, b):any;
    }

// 【反馈】命星系统面板
    module FateStarPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活命星点
    module ActiveFateStarPointReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActiveFateStarPointReq;
        function encode(o, b):any;
    }

// 【反馈】激活命星点
    module ActiveFateStarPointRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】头衔光环面板
    module TouxianGuanghuanPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TouxianGuanghuanPanelReq;
        function encode(o, b):any;
    }

// 【反馈】头衔光环面板
    module TouxianGuanghuanPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活下一个头衔光环
    module ActiveNextTouxianGuanghuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ActiveNextTouxianGuanghuanReq;
        function encode(o, b):any;
    }

// 【反馈】激活下一个头衔光环
    module ActiveNextTouxianGuanghuanRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取合服首充活动列表信息
    module GetHeFuShouChongInfosReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHeFuShouChongInfosReq;
        function encode(o, b):any;
    }

// 获取合服首充活动列表信息
    module GetHeFuShouChongInfosRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取合服首充奖励
    module HeFuShouChongRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeFuShouChongRewardReq;
        function encode(o, b):any;
    }

// 领取合服首充奖励
    module HeFuShouChongRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取个人飙战列表信息
    module GetGeRenBiaoZhanInfosReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGeRenBiaoZhanInfosReq;
        function encode(o, b):any;
    }

// 获取个人飙战列表信息
    module GetGeRenBiaoZhanInfosRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取合服首充奖励
    module GeRenBiaoZhanRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GeRenBiaoZhanRewardReq;
        function encode(o, b):any;
    }

// 领取合服首充奖励
    module GeRenBiaoZhanRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取合服飙战列表信息
    module GetHeFuBiaoZhanInfosReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHeFuBiaoZhanInfosReq;
        function encode(o, b):any;
    }

// 获取合服飙战列表信息
    module GetHeFuBiaoZhanInfosRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 合服前预售活动
    module GetHeFuYuShouInfosReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetHeFuYuShouInfosReq;
        function encode(o, b):any;
    }

// 合服前预售活动
    module GetHeFuYuShouInfosRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 单笔购买奖励
    module HeFuYuShouBuyGoodReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.HeFuYuShouBuyGoodReq;
        function encode(o, b):any;
    }

// 单笔购买奖励
    module HeFuYuShouBuyGoodRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]印记列表
    module YinJiLieBiaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YinJiLieBiaoReq;
        function encode(o, b):any;
    }

// [返回]印记列表
    module YinJiLieBiaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]升级印记
    module YinJiShengJiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YinJiShengJiReq;
        function encode(o, b):any;
    }

// [返回]升级印记
    module YinJiShengJiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]我要变强面板
    module WoYaoBianQiangPanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.WoYaoBianQiangPanelReq;
        function encode(o, b):any;
    }

// [返回]我要变强面板
    module WoYaoBianQiangPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买变强礼包
    module GetBianQiangLiBaoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBianQiangLiBaoReq;
        function encode(o, b):any;
    }

// [返回]购买变强礼包
    module GetBianQiangLiBaoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】奖励列表
    module GetAwardsListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetAwardsListReq;
        function encode(o, b):any;
    }

// 【反馈】奖励列表
    module GetAwardsListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取奖励
    module ReceiveAwardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ReceiveAwardReq;
        function encode(o, b):any;
    }

// 【反馈】领取奖励
    module ReceiveAwardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家登陆游戏请求
    module LoginGameReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoginGameReq;
        function encode(o, b):any;
    }

// 玩家登陆游戏反馈
    module LoginGameRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家用户登陆请求
    module UserLoginReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UserLoginReq;
        function encode(o, b):any;
    }

// 玩家用户登陆反馈
    module UserLoginRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家创建角色请求
    module CreateActorReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CreateActorReq;
        function encode(o, b):any;
    }

// 玩家用户登陆反馈
    module CreateActorRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家重新登录在重连后
    module LoginOnReconnectingReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoginOnReconnectingReq;
        function encode(o, b):any;
    }

// 玩家重新登录在重连后
    module LoginOnReconnectingRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 标记玩家已经接收到PushPlayerBasicInfoAtLogin请求了
    module MarkPlayerReceivePushPlayerBasicInfoAtLoginMessageReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MarkPlayerReceivePushPlayerBasicInfoAtLoginMessageReq;
        function encode(o, b):any;
    }

// 玩家用户属性更新
    module PlayerPropUpdateRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家用户登出
    module UserLogoutReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.UserLogoutReq;
        function encode(o, b):any;
    }

// 反馈玩家用户登出
    module UserLogoutRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 玩家登录成功进入家园主画面后的请求，用于一些挂机结算处理
    module LoginIsOKReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoginIsOKReq;
        function encode(o, b):any;
    }

// 【请求】玩家功能开启
    module PlayerFunctionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerFunctionReq;
        function encode(o, b):any;
    }

// 保存玩家等级提示引导
    module SaveLevelTipReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SaveLevelTipReq;
        function encode(o, b):any;
    }

// 保存玩家等级提示引导
    module SaveLevelTipRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取等级提示引导
    module GetLevelTipReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLevelTipReq;
        function encode(o, b):any;
    }

// 获取等级提示引导
    module GetLevelTipRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取分享奖励
    module GetFenxiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetFenxiangReq;
        function encode(o, b):any;
    }

// 获取分享奖励
    module GetFenxiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取关注奖励
    module GetGuanZhuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGuanZhuReq;
        function encode(o, b):any;
    }

// 获取关注奖励
    module GetGuanZhuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取关注奖励
    module GetGuanZhu175Req  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetGuanZhu175Req;
        function encode(o, b):any;
    }

// 获取关注奖励
    module GetGuanZhu175Rsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 是否可以分享
    module CanFenxiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CanFenxiangReq;
        function encode(o, b):any;
    }

// 是否可以分享
    module CanFenxiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 邀请奖励发放
    module GetInviteRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetInviteRewardReq;
        function encode(o, b):any;
    }

// 邀请奖励发放
    module GetInviteRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 某页面进入通知指令
    module OnPageEnterReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OnPageEnterReq;
        function encode(o, b):any;
    }

// 某页面退出通知指令
    module OnPageExitReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.OnPageExitReq;
        function encode(o, b):any;
    }

// 【请求】更多随机昵称
    module MoreRandomNamesReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MoreRandomNamesReq;
        function encode(o, b):any;
    }

// 【反馈】更多随机昵称
    module MoreRandomNamesRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】检查玩家昵称是否可使用
    module MarkNickNameUseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MarkNickNameUseReq;
        function encode(o, b):any;
    }

// 【反馈】检查玩家昵称是否可使用
    module MarkNickNameUseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】激活年卡
    module GetYearCardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetYearCardReq;
        function encode(o, b):any;
    }

// 【反馈】激活年卡
    module GetYearCardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】请求随机名字
    module GetRandomNamesReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetRandomNamesReq;
        function encode(o, b):any;
    }

// 【反馈】请求随机名字
    module GetRandomNamesRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【推送】推送主角武将等批量变化属性
    module PushChangedEntitiesBatchReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PushChangedEntitiesBatchReq;
        function encode(o, b):any;
    }

// 保存音乐开关等状态信息
    module StatusJsonReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.StatusJsonReq;
        function encode(o, b):any;
    }

// 保存玩家创建角色步骤
    module CreatePlayerStepRecordReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.CreatePlayerStepRecordReq;
        function encode(o, b):any;
    }

// 保存关注状态
    module SaveGuanZhuStatusReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SaveGuanZhuStatusReq;
        function encode(o, b):any;
    }

// 保存关注状态
    module SaveGuanZhuStatusRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取前端特定符号状态
    module GetClientMarkValueReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetClientMarkValueReq;
        function encode(o, b):any;
    }

// 获取前端特定符号状态
    module GetClientMarkValueRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 标识前端特定符号状态值
    module MarkClientMarkValueReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.MarkClientMarkValueReq;
        function encode(o, b):any;
    }

// 【请求】通过渠道请求奖励
    module GetJiangLiListByChannelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetJiangLiListByChannelReq;
        function encode(o, b):any;
    }

// 【反馈】通过渠道请求奖励
    module GetJiangLiListByChannelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩吧奖励
    module GetWanBaJiangLiListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetWanBaJiangLiListReq;
        function encode(o, b):any;
    }

// 【反馈】玩吧奖励
    module GetWanBaJiangLiListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】玩家登陆流程处理
    module PlayerLoginProcessReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.PlayerLoginProcessReq;
        function encode(o, b):any;
    }

// 【请求】赠送玩家金券
    module AddJinQuanReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AddJinQuanReq;
        function encode(o, b):any;
    }

// 新手引导面板
    module NewPlayerGuidePanelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NewPlayerGuidePanelReq;
        function encode(o, b):any;
    }

// 【反馈】新手引导面板
    module NewPlayerGuidePanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 新手引导摇奖
    module GetNewPlayerYaoJiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetNewPlayerYaoJiangReq;
        function encode(o, b):any;
    }

// 【反馈】新手引导摇奖
    module GetNewPlayerYaoJiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 领取累计钻石
    module GetLeiJiZuanShiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetLeiJiZuanShiReq;
        function encode(o, b):any;
    }

// 【反馈】领取累计钻石
    module GetLeiJiZuanShiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 通知活动结束
    module NotifyActivityCloseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.NotifyActivityCloseReq;
        function encode(o, b):any;
    }

// 【反馈】通知活动结束
    module NotifyActivityCloseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 获取活动是否结束
    module GetActivityCloseReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetActivityCloseReq;
        function encode(o, b):any;
    }

// 【反馈】获取活动是否结束
    module GetActivityCloseRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】前端接到这个消息 弹出防沉迷面板 不输入关面板或者输入成年身份证  前端自己做刷新
    module FangChenMiPanelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】 输入防沉迷信息  前端做验证 是不是成年 如果是 给后端发这个  不关游戏
    module InputFangChenMiInfoReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.InputFangChenMiInfoReq;
        function encode(o, b):any;
    }

// 【反馈】 输入防沉迷信息  前端做验证 是不是成年 如果是 给后端发这个  不关游戏
    module InputFangChenMiInfoRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】 取得对应id的服务器内信息
    module GetBeiZhuLiteReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetBeiZhuLiteReq;
        function encode(o, b):any;
    }

// 【回馈】  取得对应id的服务器内信息
    module GetBeiZhuLiteRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】 输入手机号领取奖励
    module GetPhoneRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetPhoneRewardReq;
        function encode(o, b):any;
    }

// 【回馈】输入手机号领取奖励
    module GetPhoneRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】增加分享次数
    module AddShareTimesReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AddShareTimesReq;
        function encode(o, b):any;
    }

// 【反馈】增加分享次数
    module AddShareTimesRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】收藏小游戏奖励
    module GetShouCangLiReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetShouCangLiReq;
        function encode(o, b):any;
    }

// 【反馈】收藏小游戏奖励
    module GetShouCangLiRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】跳过战斗
    module TiaoGuoZhanDouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.TiaoGuoZhanDouReq;
        function encode(o, b):any;
    }

// 【反馈】跳过战斗
    module TiaoGuoZhanDouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】修正地址指令
    module FixRemoteResourceUrlRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 回到挂机页面，第一次登陆时不要调用，只能是从其他页面返回时调用
    module GoBackToGuajiLayerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GoBackToGuajiLayerReq;
        function encode(o, b):any;
    }

// 【请求】收藏小游戏奖励展示
    module ShouCangLiShowReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShouCangLiShowReq;
        function encode(o, b):any;
    }

// 【反馈】收藏小游戏奖励展示
    module ShouCangLiShowRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【反馈】创角弹出特殊奖励
    module PushSpecialCreateRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取创角特殊奖励
    module GetSpecialCreateRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetSpecialCreateRewardReq;
        function encode(o, b):any;
    }

// 【反馈】领取创角特殊奖励
    module GetSpecialCreateRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】加载渠道通用分类奖励请求
    module LoadChannelCommonRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.LoadChannelCommonRewardReq;
        function encode(o, b):any;
    }

// 【反馈】加载渠道通用分类奖励请求
    module LoadChannelCommonRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】领取渠道通用分类奖励请求
    module GetChannelCommonRewardReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetChannelCommonRewardReq;
        function encode(o, b):any;
    }

// 【反馈】领取渠道通用分类奖励请求
    module GetChannelCommonRewardRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】刷新职业
    module RefreshPlayerProfessionReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RefreshPlayerProfessionReq;
        function encode(o, b):any;
    }

// 【反馈】刷新职业
    module RefreshPlayerProfessionRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】通过渠道请求奖励状态
    module GetJiangLiStatusByChannelReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetJiangLiStatusByChannelReq;
        function encode(o, b):any;
    }

// 【反馈】通过渠道请求奖励
    module GetJiangLiStatusByChannelRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// 【请求】消耗道具获取体力
    module GetTiliByDaojuReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetTiliByDaojuReq;
        function encode(o, b):any;
    }

// 【反馈】消耗道具获取体力
    module GetTiliByDaojuRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]敌人列表
    module GuaJiEnergyListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaJiEnergyListReq;
        function encode(o, b):any;
    }

// [反馈]敌人列表
    module GuaJiEnergyListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]被动战斗
    module BeiDongZhanDouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.BeiDongZhanDouReq;
        function encode(o, b):any;
    }

// [返回]被动战斗
    module BeiDongZhanDouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主动战斗
    module ZhuDongZhanDouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ZhuDongZhanDouReq;
        function encode(o, b):any;
    }

// [返回]主动战斗
    module ZhuDongZhanDouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]战斗记录
    module FightRecordReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FightRecordReq;
        function encode(o, b):any;
    }

// [返回]战斗记录
    module FightRecordRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]复仇
    module FuChouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.FuChouReq;
        function encode(o, b):any;
    }

// [返回]复仇
    module FuChouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]敌人消息
    module GuaJiEnergyDisappearReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaJiEnergyDisappearReq;
        function encode(o, b):any;
    }

// [请求]敌人列表
    module YinDaoGuaJiEnergyListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YinDaoGuaJiEnergyListReq;
        function encode(o, b):any;
    }

// [反馈]敌人列表
    module YinDaoGuaJiEnergyListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]主动战斗
    module YinDaoZhuDongZhanDouReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.YinDaoZhuDongZhanDouReq;
        function encode(o, b):any;
    }

// [返回]主动战斗
    module YinDaoZhuDongZhanDouRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]仇人列表
    module ChouRenListReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ChouRenListReq;
        function encode(o, b):any;
    }

// [返回]仇人列表
    module ChouRenListRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]抢夺仇人
    module QiangDuoChouRenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.QiangDuoChouRenReq;
        function encode(o, b):any;
    }

// [返回]抢夺仇人
    module QiangDuoChouRenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]添加仇人
    module AddChouRenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AddChouRenReq;
        function encode(o, b):any;
    }

// [反馈]添加仇人
    module AddChouRenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]删除仇人
    module ShanChuChouRenReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.ShanChuChouRenReq;
        function encode(o, b):any;
    }

// [反馈]删除仇人
    module ShanChuChouRenRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]开宝箱
    module KeiBaoXiangReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.KeiBaoXiangReq;
        function encode(o, b):any;
    }

// [反馈]开宝箱
    module KeiBaoXiangRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]挂机pvp宝箱面板
    module GuaJiPVPBaoXiangPenalReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GuaJiPVPBaoXiangPenalReq;
        function encode(o, b):any;
    }

// [反馈]挂机pvp宝箱面板
    module GuaJiPVPBaoXiangPenalRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]根据玩家名字 获得地图跑动敌人 跨服
    module GetDiTuDiRenByPlayerNameCrossServerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.GetDiTuDiRenByPlayerNameCrossServerReq;
        function encode(o, b):any;
    }

// [反馈]根据玩家名字 获得地图跑动敌人 跨服
    module GetDiTuDiRenByPlayerNameCrossServerRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]购买复仇宿敌奖励次数
    module AddFuChouSuDiRewardCountReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.AddFuChouSuDiRewardCountReq;
        function encode(o, b):any;
    }

// [反馈]购买复仇宿敌奖励次数
    module AddFuChouSuDiRewardCountRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]挂机随机事件的抢劫玩家
    module RobPlayerOnStoryLineReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RobPlayerOnStoryLineReq;
        function encode(o, b):any;
    }

// [反馈]挂机随机事件的抢劫玩家
    module RobPlayerOnStoryLineRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]复仇敌人当前Project状态搜索
    module RevengeEnemyProjectStatusSearchReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RevengeEnemyProjectStatusSearchReq;
        function encode(o, b):any;
    }

// [反馈]复仇敌人当前Project状态搜索
    module RevengeEnemyProjectStatusSearchRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]复仇敌人当前Project内容信息搜索
    module RevengeEnemyProjectInfoSearchReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RevengeEnemyProjectInfoSearchReq;
        function encode(o, b):any;
    }

// [反馈]复仇敌人当前Project内容信息搜索
    module RevengeEnemyProjectInfoSearchRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]前往复仇对手所在页面
    module RevengeEnemyLeaveForNewLayerReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.RevengeEnemyLeaveForNewLayerReq;
        function encode(o, b):any;
    }

// [反馈]前往复仇对手所在页面
    module RevengeEnemyLeaveForNewLayerRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

// [请求]遭遇战和复仇抢夺的扫荡
    module SweepSuddenFightAndRevengeReq  {
        var MAIN_TYPE
        var SUB_TYPE
        function get(options ?:any):msgObj.SweepSuddenFightAndRevengeReq;
        function encode(o, b):any;
    }

// [返回]遭遇战和复仇抢夺的扫荡
    module SweepSuddenFightAndRevengeRsp  {
        var MAIN_TYPE
        var SUB_TYPE
        function decode(o, b):any;

    }

}