//本声明文件下的接口仅仅为调用数据方便不可被实例化
declare module msgObj { 
    interface ReqObj{
        mainType ?:number
        subType ?:number
        destinationId ?:number
        serial ?:number
    }
    interface RspObj{
        mainType ?:number
        subType ?:number
        sourceId ?:number
        isErrorMessage ?:boolean
        serial ?:number
        easyGameId ?:number
        errorCode ?:number
        errorMsg ?:number
        errorContents ?:Array<any>
        isHandled ?:boolean
    }
// 活动详情
    interface ActivityDetailInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 达到什么条件显示可领取 一般是金额
                tiaojian ?:number
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
        // 数量
                shuoming ?:string
        // 限制类型 1:天 2:大周期（整个活动）
                limitType ?:number
        // 已领取次数
                hasReceivedTimes ?:number
        // 最大领取次数
                maxReceiveTimes ?:number
        // 剩余可领取次数
                leftReceiveTimes ?:number
        // 已经充的钱数（前端计算还差多少可领时使用）
                hasRechargeCount ?:number
    }
// 挑战神宠关卡信息
    interface TiaoZhanShenShouGuanKaInfoUnit  {
        // 关卡ID
                defId ?:number
        // 第几关卡
                num ?:number
        // 怪物骨骼信息
                guaiwuGuge ?:msgObj.GugeUnit
        // 说明
                explanation ?:string
        // 奖励内容，默认第一个是大奖，最上面列表中需要显示
                gains ?:Array<JiangliInfoUnit>
        // 推荐挑战战力
                challenge ?:number
        // 是否可以挑战
                canFight ?:boolean
    }
// 客户端奖励信息
    interface AwardInfoUnit  {
        // 奖励数据
                jiangliList ?:Array<JiangliInfoUnit>
        // 领取状态：0、不可领；1、可以领，但尚未领取 2、已领取；
                doGet ?:number
        // 奖励id，对应kehuduangjiangli表中defId
                awardDefId ?:number
    }
// 地图等级列表信息
    interface BossLevelListMapUnit  {
        // 世代
                shiDai ?:number
        // 最小等级
                levelFrom ?:number
        // 最大等级
                levelTo ?:number
        // 转生等级
                zhuanSheng ?:number
    }
// 地图列表信息
    interface BossListMapUnit  {
        // mapId
                mapId ?:number
        // 地图名字
                mapName ?:string
        // 1 本服 2 个人
                sType ?:number
        // 1 和平场 2 战争场
                fType ?:number
        // 1最后一击 2首摸 3最高伤害
                rewardRule ?:number
        // 多少级之内的玩家才能获得奖励
                rewardLevel ?:number
        // 世代数
                generation ?:number
        // 本服(玩家等级限制) 个人(vip等级限制)
                limitLevel ?:number
        // 开启条件
                cost ?:msgObj.JiangliInfoUnit
        // 可能掉落的奖励列表
                jiangLiList ?:Array<JiangliInfoUnit>
        // boss列表
                bossList ?:Array<BossMapMonsterForUIUnit>
        // 是否开启
                isOpen ?:boolean
        // 是否被禁
                isBan ?:boolean
        // 被禁结束时间
                banedOverTime ?:number
        // 是否是首杀
                isFirstKill ?:boolean
        // 需要转生等级
                zhuanShengLevel ?:number
        // 需要上阵宠物达到等级
                needHeroLevel ?:number
        // 需要穿戴装备数量
                needZhuangBeiCount ?:number
        // 需要装备强化等级
                needZhuangBeiQiangHua ?:number
        // 需要上阵宠物达到等级的数量
                needHeroCount ?:number
        // 地图寻路属性
                findType ?:string
        // 惩罚结束时间，下次可进入时间
                nextEnterTime ?:number
        // 需要的VIP等级
                vipLevel ?:number
        // 领主条件 通关No.XX
                lingzhu ?:number
    }
// 地图怪物信息
    interface BossMapMonsterUnit  {
        // bossId
                bossId ?:number
        // boss对应的武将ID
                bossWuJiangId ?:number
        // boss名称
                bossName ?:string
        // boss形象
                bossImage ?:string
        // boss头像
                bossTouXiang ?:string
        // 大招技能Id
                daZhaoSkill ?:number
        // 小招技能Id
                xiaoZhaoSkill ?:number
        // 推荐战力
                tuiJianZhanLi ?:number
        // 下一次刷新时间
                refreshTime ?:number
        // 当前血量
                hp ?:number
        // 血量上限
                hpMax ?:number
        // 大招范围1
                daZhao1 ?:number
        // 大招范围2
                daZhao2 ?:number
        // 小招范围1
                xiaoZhao1 ?:number
        // 小招范围2
                xiaoZhao2 ?:number
        // 怪的位置
                location ?:msgObj.PointUnit
        // 归属权
                rewardPlayerId ?:number
        // 是否无敌
                isNoEnemy ?:boolean
        // 无敌结束时间
                noEnemyTime ?:number
        // 大招攻击时间(时间戳)
                daZhaoAttackTime ?:number
        // 大招攻速(毫秒)
                daZhaoGongSu ?:number
        // 小招攻击时间(时间戳)
                xiaoZhaoAttackTime ?:number
        // 小招攻速(毫秒)
                xiaoZhaoGongSu ?:number
        // 稀有度
                sssr ?:number
        // boss ICON
                icon ?:string
        // 没有攻击，自动回复满血时间戳，单位：毫秒。-1表示被攻击了，重新计时
                fullHpTime ?:number
        // 死亡次数
                damageCount ?:number
        // 狂暴死亡次数
                kuangBaoCount ?:number
        // 整宠判上限 0 不判上限 1 判断上限
                isMax ?:number
        // 出现时间
                appearTime ?:number
        // 结束时间
                fightEndTime ?:number
    }
// boss玩家基本信息
    interface BossPlayerInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 头像Id
                touXiangId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家等级
                playerLevel ?:number
        // 玩家战力
                playerFight ?:number
        // 公会Id
                gongHuiId ?:number
        // 公会名
                gongHuiName ?:string
        // 公会图标
                gongHuiImage ?:string
        // 玩家坐骑ID
                playerZuoqi ?:number
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 坐标
                location ?:msgObj.PointUnit
        // 移动路径
                pathList ?:Array<PointUnit>
        // 是否有归属权
                canHaveReward ?:Array<CanGetRewardUnit>
        // 玩家血量
                hp ?:number
        // 最大血量
                hpMax ?:number
        // 回血CD
                reClHpCd ?:number
        // 回血量
                reClHpValue ?:number
        // 毫秒
                attackCd ?:number
        // 占有时间 单位/秒
                occupancyTime ?:number
        // 称号
                chengHaoId ?:number
        // 玩家所属区服编号
                serverNum ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 队伍首宠的系别
                typeOfFirstHero ?:string
        // Unit类型：5、正常玩家；7、机器人
                unitType ?:number
    }
// 地图内的攻击受击信息
    interface AttackInMapUnit  {
        // 玩家Id
                playerId ?:number
        // 玩家Id
                bossId ?:number
        // 1 小招 2 大招
                bossAttackType ?:number
        // -1不预警 1 小招 2 大招
                nextAttackType ?:number
        // 1人 2 怪
                sType ?:number
        // 位置
                location ?:msgObj.PointUnit
        // 攻击值
                attack ?:number
        // 血量
                hp ?:number
        // 归属权列表
                canGetRewardList ?:Array<CanGetRewardUnit>
        // 是否是无敌状态
                isNoEnemy ?:boolean
        // 无敌状态结束时间戳
                noEnemyEndTime ?:number
        // 掉落奖励
                jiangLiList ?:Array<JiangliInfoUnit>
    }
// 归属权
    interface CanGetRewardUnit  {
        // bossId
                bossId ?:number
        // 伤害值
                damage ?:number
        // 是否具有归属权
                isReward ?:boolean
    }
// 地图等级额外推送信息
    interface BossSceneOtherUnit  {
        // 地图Id
                mapId ?:number
        // 怪物Id
                bossId ?:number
        // 刷新
                refreshTime ?:number
        // 是否已经在地图内了，如果在，则不需要重复扣次数
                isPlayerInTheMap ?:boolean
    }
// 援助boss请求信息
    interface BossYuanZhuInfoUnit  {
        // 地图信息
                mapInfo ?:msgObj.BossListMapUnit
        // 开始时间
                startTime ?:number
    }
// 玩家加血
    interface BossPlayerAddHpUnit  {
        // playerId
                playerId ?:number
        // 加血百分比
                addHpPercent ?:number
        // 当前血量
                curHp ?:number
        // 当前血量上限
                curHpMax ?:number
    }
// 地图怪物信息For UI
    interface BossMapMonsterForUIUnit  {
        // bossId
                bossId ?:number
        // boss对应的武将ID
                bossWuJiangId ?:number
        // boss名称
                bossName ?:string
        // boss形象
                bossImage ?:string
        // boss头像
                bossTouXiang ?:string
        // 推荐战力
                tuiJianZhanLi ?:number
        // 下一次刷新时间
                refreshTime ?:number
        // 稀有度
                sssr ?:number
        // boss ICON
                icon ?:string
        // 是否已经在地图内了，如果在，则不需要重复扣次数
                isPlayerInTheMap ?:boolean
        // 血量剩余比例，万分比
                hpPercentValue ?:number
        // boss骨骼配置
                bossGugeConfig ?:string
        // boss骨骼纹理
                bossGugeTexture ?:string
    }
// 跨服boss地图对应的区服编号
    interface BossMapServerInfoUnit  {
        // 区服编号
                serverNum ?:number
        // 区服名称
                serverName ?:string
    }
// VIP才能有机会获得的奖励
    interface VipJiangliInfoUnit  {
        // VIP等级
                vip ?:number
        // 奖励内容
                jiangliInfo ?:msgObj.JiangliInfoUnit
    }
// 世界战场剩余次数信息
    interface BossLeftCountInfoUnit  {
        // boss对应的project
                projectDefId ?:number
        // 剩余次数
                count ?:number
    }
// 世界战场剩余次数信息
    interface BossHpInfoUnit  {
        // 当前服务器时间，单位毫秒
                serverTime ?:number
        // 当前血量
                hp ?:number
        // 下次扣血时间，单位毫秒
                nextSubHpTime ?:number
        // 下次扣血时间时间到达后的剩余血量
                hpNext ?:number
        // 扣血方式：1正常掉血；2秘法扣血
                hpType ?:number
    }
// 比武记录数据
    interface BiwuRecordInfoUnit  {
        // DBID
                biwuRecordDBId ?:number
        // 玩家Name
                ownNane ?:string
        // 玩家DBId
                playerId ?:number
        // 比武发起人DBId
                tiaozhanZheId ?:number
        // 对手的Name
                enemyName ?:string
        // 返回码：1我方获胜，2我方失败
                win ?:number
        // 排名变更情况：正数表示名次提高，负数表示名次下降
                rankingChange ?:number
        // 当时排名
                curPaihang ?:number
        // 添加时间
                addTime ?:number
    }
// 比武对手武将数据
    interface BiwuHeroInfoUnit  {
        // 武将ID, 静态ID
                heroID ?:number
        // 等级
                level ?:number
        // 战斗力【计算属性】
                fight ?:number
        // 培养等级/宠物升星等级
                levelGuanzhi ?:number
    }
// 比武对手数据
    interface BiwuInfoUnit  {
        // 玩家Name
                playerName ?:string
        // 玩家Id
                playerId ?:number
        // 战斗力
                zhandouli ?:number
        // 玩家头像ID
                touxiangId ?:number
        // 当前排行
                curPaihang ?:number
        // 坐骑
                playerZuoqi ?:number
        // 翅膀
                playerChibang ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否是豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 称号
                chenghaoId ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 属性数据
    interface PropertyInfoUnit  {
        // 属性类型
                type ?:number
        // 属性数值
                value ?:number
    }
// 排行信息
    interface BiWuPaiHangUnit  {
        // 昵称
                name ?:string
        // 排名
                paiMing ?:number
    }
// 忍村战斗boss数据校验
    interface BossCheckUnit  {
        // bossid
                id ?:number
        // 生命
                hp ?:number
        // 攻击
                atk ?:number
        // 防御
                def ?:number
        // 受到技能伤害次数
                skillNum ?:number
        // 忍蛙攻击次数
                renwaatkNum ?:number
    }
// boss房间玩家简易信息
    interface BossPlayerSimpleInfoUnit  {
        // 玩家id
                playerId ?:number
        // boss持有时间
                occupancTime ?:number
    }
// 不思议礼包
    interface BuSiYiLiBaoUnit  {
        // 0 可以买  1 不能买
                status ?:number
        // 人民币 元
                price ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 充值id
                chongZhiId ?:number
        // 礼包说明
                liBaoShuoMing ?:string
        // false 单买 true 全买
                isAllBuy ?:boolean
    }
// 财神殿排行数据
    interface CaishendianPaihangInfoUnit  {
        // 玩家Name
                playerName ?:string
        // 玩家头像
                touxiang ?:number
        // VIP等级
                vipLevel ?:number
        // 玩家等级
                level ?:number
        // 最大的关卡ID
                guanQiaId ?:number
        // 财神殿关DBId
                csdDBId ?:number
    }
// 登录的时候推送20条信息  他的里面套一个ChatMessageInfo
    interface ChatMessageLoginInfoUnit  {
        // 讲话人ID
                playerId ?:number
        // 聊天信息的DBId
                chatDBId ?:number
        // 聊天内容
                content ?:string
        // 聊天内容中的物品信息（1'道具', 2'装备', 3'武将', 4'阵法', 5'耳朵'）
                messageInfoList ?:Array<ChatMessageInfoUnit>
        // 
                vipLevel ?:number
        // 
                playerName ?:string
        // 排位赛段位的defId
                paihangPPDefId ?:number
        // 等级段
                levelPart ?:number
        // 服务器id
                serverId ?:number
        // 
                level ?:number
        // 头像
                face ?:number
        // 徽章图
                huiZhangTu ?:string
        // gonghuiid
                gonghuiId ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 先使用服务器编号,来自玩家原始创角时所属服务器编号
                showServerNum ?:number
        // 角色信息
                playerInfo ?:msgObj.PaihangInfoUnit
        // 抢夺记录Id
                qiangDuoRecordId ?:number
        // 失效时间戳
                shiXiaoTime ?:number
        // 0 援助失败 1记录过期 2援助成功 
                status ?:number
        // 是否可以援助
                canYuanZhu ?:boolean
        // 敌人Id
                enemyPlayerId ?:number
        // 限制等级
                limitLevel ?:number
        // 聊天时间
                chatTime ?:number
        // 公会道馆id
                daoguanId ?:number
        // 公会道馆位置
                daoguanWeizhi ?:number
        // 关联类型 1 抢夺 2小镇
                joinType ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 聊天展示特权表Id列表
                liaotianZhanshiDefIdList ?:Array<number>
        // 聊天框Id
                liaotiankuangDefId ?:number
    }
// 私聊玩家信息
    interface PersonalChatInfoUnit  {
        // 讲话人ID
                playerId ?:number
        // 讲话人昵称
                playerName ?:string
        // 最后一次聊天时间
                chatTime ?:number
        // 是否有未读消息
                isRed ?:boolean
        // 服务器编号,来自玩家原始创角时所属服务器编号
                showServerNum ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 聊天展示特权表Id列表
                liaotianZhanshiDefIdList ?:Array<number>
        // 聊天框Id
                liaotiankuangDefId ?:number
    }
// 聊天内容中的物品信息（1'道具', 2'装备', 3'武将', 4'阵法', 5'耳朵'）
    interface ChatMessageInfoUnit  {
        // 1'道具', 2'装备', 3'武将', 4'阵法', 5'耳朵'6 公会副本 7 探索 8抢夺请求援助 9抢夺援助结果 10 boss发起援助 11 白银山喊话
                type ?:number
        // 物品的DBID
                dbId ?:number
        // 物品的静态ID
                id ?:number
        // 英雄的协作，该字段仅供前端使用，原封转发给前端即可
                xiezuo ?:string
        // 聊天内容
                strContent ?:string
        // 
                level ?:number
    }
// 待删除的聊天信息
    interface DeleteChatInfoUnit  {
        // 聊天频道 0世界  1本服  2公会 3公告 100 私聊
                channel ?:number
        // 聊天信息的DBId
                chatDBId ?:Array<number>
    }
// 奖励数据
    interface ChongjiJiangliInfoUnit  {
        // ID，静态表主Key
                chongjijiangliId ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
    }
// 充值金额数据
    interface ChongzhiJineInfoUnit  {
        // ID，充值金额静态表Id
                chongzhijineId ?:number
        // 是否已经充值：0、未领取；1、已领取；
                doGet ?:number
        // 是否是首次充值
                isFirst ?:number
        // 是否是首次充值奖励金额双倍
                isDoubleReward ?:boolean
    }
// 优惠券
    interface YouHuiQuanInfoUnit  {
        // 优惠券Id
                liquanId ?:number
        // 优惠券数量
                count ?:number
    }
// 首冲信息
    interface ChongZhiShouCiNewUnit  {
        // 第几天
                day ?:number
        // 奖励List
                jiangLiList ?:Array<JiangliInfoUnit>
        // 是否领取过了
                isGet ?:boolean
    }
// 首冲的一次性奖励信息
    interface ChongZhiShouCiNewOnceRewardUnit  {
        // 所需VIP条件值
                vip ?:number
        // 一次性奖励列表
                onceRewardList ?:Array<JiangliInfoUnit>
        // 是否领取了一次性奖励了
                isGetOnceReward ?:boolean
    }
// 订单的发货额外附加信息
    interface OrderDeliveryAttachInfoUnit  {
        // 发起充值来自哪里的分类：1、来自活动；2、来自守护兽付费技能Id；3、七日竞赛活动购买一元特价礼包
                type ?:number
        // 跟前面的分类对应，1、活动DbId；2、守护兽DbId；3、七日竞赛活动DbId
                dbId ?:number
        // 哪一项。1、对应改行的defId或者detailId；2、守护兽付费技能表Id；3、无需传值
                defId ?:number
    }
// 次日登录奖励数据
    interface CiriJiangliInfoUnit  {
        // ID，静态表主Key
                ciriJiangliId ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
    }
// 比率数据
    interface RatioDataListUnit  {
        // 比率1
                ratio1 ?:number
        // 比率2
                ratio2 ?:number
        // 比率3
                ratio3 ?:number
        // 比率4
                ratio4 ?:number
    }
//  数据库id和数量数据
    interface DBIdAndIntListUnit  {
        // 数据库id
                dbId ?:number
        // 数量
                count ?:number
    }
// 客户端奖励信息
    interface CompleteSuppressionInfoUnit  {
        // 奖励数据
                jiangliList ?:Array<JiangliInfoUnit>
        // 小关表Id
                defId ?:number
        // 隐藏精英怪的奖励数据
                hideMonsterRewardList ?:Array<JiangliInfoUnit>
    }
// 奖励数据
    interface CostItemUnit  {
        // 类型
                type ?:number
        // 消耗
                cost ?:msgObj.JiangliInfoUnit
    }
// 跨服锦标赛公会更新积分信息
    interface KuaFuJBSGongHuiScoreInfoUnit  {
        // 公会ID
                gongHuiId ?:number
        // 公会名称
                name ?:string
        // 所在服务器
                serverId ?:number
        // 服务器编号
                serverNum ?:number
        // 增量积分
                score ?:number
        // 要清空的公会ID
                clearGongHuiId ?:number
        // 公会徽章
                gongHuiHuiZhang ?:string
    }
// 跨服锦标赛个人更新积分信息
    interface KuaFuJBSGeRenScoreInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家Name
                playerName ?:string
        // 玩家头像
                touxiang ?:number
        // VIP等级
                vipLevel ?:number
        // 所在服务器
                serverId ?:number
        // 服务器编号
                serverNum ?:number
        // 覆盖积分
                score ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 财神殿奖励说明
    interface CsdJiangliExplainInfoUnit  {
        // 财神殿大关ID（静态表）
                id ?:number
        // 是否有人通关：1、未通关；2、通关
                isTongguan ?:number
    }
// 财神殿奖励数据
    interface CsdJiangliInfoUnit  {
        // ID，静态表主Key
                id ?:number
        // 财神殿关卡DefId
                csdId ?:number
        // 1: 经验，2：武将经验 3：银两 4：装备 5 武将 6 道具 7 阵法 8 宝石 9 战阵碎片 10 世界BOSS积分   11 匹配战积分
                type ?:number
        // 数量
                count ?:number
    }
// 特殊累计充值展示信息
    interface CumulativeRechargeDisplayInfoUnit  {
        // 活动类型
                activityType ?:number
        // 累计充值表Id
                defId ?:number
        // 1为悟空神装 2为开启阵位 3为开启协作位 4为头衔证书
                danduxianshi ?:number
        // 展示结束时间，单位：毫秒
                endTime ?:number
        // 奖励内容
                rewardList ?:Array<JiangliInfoUnit>
        // 还差多少钱就可以完成，单位：元
                diffYuan ?:number
        // 是否达成条件，可以领取奖励了
                isCanTakeReward ?:boolean
    }
// 道具互换
    interface DaoJuHuHuanInfoUnit  {
        // ID，静态表主Key
                id ?:number
        // 模板Type
                mobanType ?:number
        // 所需金券数
                jinQuan ?:number
        // 类型
                typeAwards ?:string
        // id
                items ?:string
        // 数量
                counts ?:string
    }
// 装备数据
    interface DaojuInfoUnit  {
        // 道具数据表dbID，数据表主Key
                daojuDbID ?:number
        // 道具ID，静态表主Key
                daojuID ?:number
        // 玩家拥有此类道具的数量
                daojuCount ?:number
        // 此字段仅在该道具为宝石时，有效。0 - 表示没有镶嵌， 非0-表示镶嵌所在装备的dbID上。
                zhuangBeiDbID ?:number
    }
// 道具消耗数据
    interface DaoJuXiaoHaoInfoUnit  {
        // 策划表id
                defId ?:number
        // 所需消耗道具信息
                costInfo ?:msgObj.JiangliInfoUnit
        // 描述
                miaoShu ?:string
        // 已经消耗的道具数量
                costNum ?:number
        // 已经领取过该奖励多少次
                leftCount ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 打折商品
    interface DaZheShangPinInfoUnit  {
        // 策划表defId
                id ?:number
        // 礼包的标题你懂得
                biaoti ?:string
        // 购买次数
                goumaoCishu ?:number
        // 价格
                jiaGe ?:number
        // 0 代表全部获得1 代表多选一
                xuan ?:number
        // 类型
                typeAward1 ?:number
        // 奖励ID
                item1 ?:number
        // 数量
                count1 ?:number
        // 类型
                typeAward2 ?:number
        // 奖励ID
                item2 ?:number
        // 数量
                count2 ?:number
        // 类型
                typeAward3 ?:number
        // 奖励ID
                item3 ?:number
        // 数量
                count3 ?:number
        // 类型
                typeAward4 ?:number
        // 奖励ID
                item4 ?:number
        // 数量
                count4 ?:number
        // 买了多少次
                hasBuy ?:number
    }
// 装备数据
    interface DuihuanInfoUnit  {
        // 数据表主Key
                duihuanDbId ?:number
        // 兑换索引ID，静态表主Key
                duihuansuoyinId ?:number
        // 兑换代价表静态ID
                daijiaId ?:number
        // 兑换奖励表静态ID
                jiangliId ?:number
        // 是否能兑换：1、可兑换；2、不可兑换；
                canExchange ?:number
    }
// 独角兽接口中的排行数据
    interface DuJiaoShouRankInfoUnit  {
        // 服务器ID，按照登录时间倒序排列
                serverId ?:number
        // 游戏的playerid
                userId ?:number
        // 平台id
                uid ?:string
        // 
                userName ?:string
        // 
                power ?:number
    }
// Email数据
    interface EmailInfoUnit  {
        // 1 系统；2 战报（不包含小镇）；3小镇战报
                type ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 商品Email数据表dbID，数据表主Key
                emailDBID ?:number
        // 玩家id
                playerId ?:number
        // 邮件标题
                title ?:string
        // 邮件状态的标志：0:新邮件 1:已查看 2:已领取
                status ?:number
        // 邮件说明
                shuoming ?:string
        // 邮件接收日期 格式：2016-11-11
                mailDate ?:string
        // 发件人 后端不传 前端显示默认的
                sender ?:string
        // 抢夺记录Id
                qiangDuoRecord ?:number
        // 1 抢夺成功 2 邮件失效
                qiangDuoStatus ?:number
        // 失效时间
                shiXiaoTime ?:number
        // 1 遭遇战 2 宅急送 3 假邮件(无回放) 4 公会道馆 5 公会道馆成功 6 公会道馆指派 7:小镇战报 8:小镇道馆攻击NPC 9:小镇道馆反击入侵者
                sType ?:number
        // 公会道馆ID
                gongHuiDaoGuanId ?:number
        // 在道馆中的位置
                weizhi ?:number
        // 道馆名字
                daoguanName ?:string
        // 小镇战报附加内容
                townReportInfo ?:msgObj.TownReportInfoUnit
        // 邮件战报的敌人Id
                enemyPlayerId ?:number
    }
// Email小镇战报数据
    interface TownReportInfoUnit  {
        // 是否我方胜利
                isMeWin ?:boolean
        // 攻击方还是被攻击方，1表示攻击方，2表示被攻击方
                attackDefend ?:number
        // 战斗胜利失败描述文字
                fightDesc ?:string
        // 我方损失兵种列表
                myLostSoliderList ?:Array<TownSoldierShowUnit>
        // 对方损失兵种列表
                enemyLostSoliderList ?:Array<TownSoldierShowUnit>
        // 损失资源列表
                resourceList ?:Array<JiangliInfoUnit>
        // 被攻击方被抢走的人物名称，含产量描述文字，可能为undefined
                personName ?:string
        // 稀有掉落道具描述，可能为undefined
                unusualReward ?:msgObj.JiangliInfoUnit
        // 攻击方角色ID
                attackPlayerId ?:number
        // 被攻击方角色ID
                targetPlayerId ?:number
        // 攻击方名字，包含小镇坐标
                attackPlayerName ?:string
        // 战报副标题
                subtitle ?:string
        // 普通掉落道具描述
                commonRewardList ?:Array<JiangliInfoUnit>
        // 战斗回放脚本LogId
                logId ?:number
    }
// 坐骑翅膀数据
    interface ZuoqiUnit  {
        // defId,升星，激活，使用应该需要用到 ，也可以用别的，主要是区分不同坐骑翅膀的，保证唯一性就行
                defId ?:number
        // 名称
                name ?:string
        // 坐骑翅膀形象Banner
                banner ?:string
        // 三种状态，0未点亮，1可点亮; 2点亮。
                status ?:number
        // 激活消耗
                jihuoCostList ?:Array<JiangliInfoUnit>
        // 升星消耗
                shengxingCostList ?:Array<JiangliInfoUnit>
        // 是否使用中
                isInUse ?:boolean
        // 是否满星级了
                isUpStarMax ?:boolean
        // 当前星级
                xingLevel ?:number
    }
// 跨服坐骑翅膀数据
    interface PlayerCardZuoqiUnit  {
        // 1是坐骑；2是翅膀。
                type ?:number
        // 全队属性加成
                shuxingList ?:Array<PropertyInfoUnit>
        // 全部坐骑翅膀
                zuoqiList ?:Array<ZuoqiUnit>
        // 是否隐藏坐骑翅膀
                isHideZuoqi ?:boolean
    }
// 命星信息
    interface FateStarInfoUnit  {
        // 命星DefId
                id ?:number
        // 命星图标
                icon ?:string
        // 大命星图标
                icon2 ?:string
        // 命星名字
                name ?:string
        // 是否未解锁
                isLock ?:boolean
        // 是否已经全部激活
                isFull ?:boolean
        // 当前界面9颗星
                pointList ?:Array<FateStarPointInfoUnit>
        // 命星当前点激活需要的消耗
                costList ?:Array<JiangliInfoUnit>
    }
// 命星点信息
    interface FateStarPointInfoUnit  {
        // 顺序1-9
                id ?:number
        // 是否已经激活
                isActived ?:boolean
        // 属性列表
                propertyList ?:Array<PropertyInfoUnit>
    }
// 命星技能面板信息
    interface FateStarSkillPanelInfoUnit  {
        // 当前技能
                currSkill ?:msgObj.FateStarSkillUnit
        // Next技能，可能为空
                nextSkill ?:msgObj.FateStarSkillUnit
    }
// 命星技能
    interface FateStarSkillUnit  {
        // mingxingjineng表Id
                id ?:number
        // 是否已经激活
                isActived ?:boolean
        // 技能等级, mingxingjineng表Level
                level ?:number
        // 当前点亮个数
                currCount ?:number
        // 所需条件个数
                condCount ?:number
        // 使用技能后可增加的HP绝对值
                hp ?:number
        // 技能冷却时间
                cd ?:number
    }
// 外显信息
    interface FeatureInfoUnit  {
        // 1 坐骑 2 翅膀
                type ?:number
        // 策划表id
                defId ?:number
        // 星级
                level ?:number
    }
// 封测活动－限时冲关数据
    interface XianshiChongguanInfoUnit  {
        // ID，静态表主Key
                defId ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
    }
// 封测活动－天降元宝数据
    interface TianjiangYuanbaoInfoUnit  {
        // ID，静态表主Key
                defId ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
        // 是否已经补签：0、未补签；1、已补签；
                isBuqian ?:number
    }
// 封测活动－天天砸金蛋数据
    interface TiantianZaJindanInfoUnit  {
        // ID，静态表主Key
                defId ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
    }
// 活动－连续充值数据
    interface LianxuChongzhiInfoUnit  {
        // ID，静态表主Key
                defId ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
    }
// 活动－每日签到数据
    interface MeiriQiandaoInfoUnit  {
        // ID，静态表主Key
                defId ?:number
        // 名称
                shuoming ?:string
        // 对应签到天数 单位：天
                tiaojian ?:number
        // 对应VIP等级和以上双倍 对应VIP等级0以上双倍
                vip ?:number
        // 到达贵族等级 双倍奖励
                doubleNoble ?:number
        // 跳转页面
                jump ?:string
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
    }
// 数字积分类数据
    interface NumberValueUnit  {
        // 类型
                type ?:number
        // 货币数量
                count ?:number
    }
// Entity类数据
    interface EntityValueUnit  {
        // 类型
                type ?:number
        // 是否飘字
                isPiaoZi ?:boolean
        // 1新增 2更新 3删除
                changeType ?:number
        // 变化的道具信息
                itemInfoList ?:Array<DaojuInfoUnit>
        // 变化的精灵信息
                changeHeroInfoList ?:Array<HeroInfoUnit>
        // 变化的装备信息
                zhuangBeiInfoList ?:Array<ZhuangbeiInfoUnit>
        // 变化的神器信息
                shenQiInfoList ?:Array<ShenQiInfoUnit>
        // 变化的职业神器信息
                zhiYeShenQiList ?:Array<ZhiYeShenQiUnit>
        // 变化的专属神器信息
                zhuanShuShenQiList ?:Array<ZhuanShuShenQiPushfoUnit>
        // 变化的技能书信息
                skillBookInfoList ?:Array<SkillBookUnit>
        // 变化的新神技信息（含道具和已穿戴的技能卡）
                magicalSkillItemList ?:Array<MagicalSkillItemUnit>
    }
// Entity类数据不在奖励类型中的
    interface EntityValueNoRewardTypeUnit  {
        // 类型 等于后面的字段名，例如guardPetLevelUpCostInfoList、sevenMatchDiffFightTips
                typeStr ?:string
        // 1新增 2更新 3删除
                changeType ?:number
        // 变化的守护兽升星消耗信息
                guardPetLevelUpCostInfoList ?:Array<NewGuardPetLevelUpCostInfoUnit>
        // 七日冲榜竞赛活动战斗力差值提示内容
                sevenMatchDiffFightTips ?:msgObj.SevenMatchDiffFightTipsUnit
        // 消耗排行活动（活动类型24）消耗差值提示内容
                xiaohaoPaihangActivityTips ?:msgObj.SevenMatchDiffFightTipsUnit
    }
// 文本内容积分类数据
    interface StringWordsUnit  {
        // 类型
                type ?:number
        // 文本内容
                words ?:string
    }
// 小关数据(普通副本)
    interface XiaoguanDefUnit  {
        // Def ID
                id ?:number
        // 关卡名称
                name ?:string
        // 是否boss战
                boss ?:number
        // 战斗力
                para2 ?:number
        // 关卡图标
                para3 ?:string
        // 关卡音乐
                para4 ?:string
        // 挂机经验 每次结算，挂机60秒结算一次
                guajiExp ?:number
        // 挂机金币 每次结算，挂机60秒结算一次
                guajiJinbi ?:number
        // 等待次数 挑战N次后挑战BOSS
                guajicishu ?:number
        // 挂机背景地图 1 草原 2 大海 3 火山 4 沙漠 5 山洞
                guajiMap ?:number
        // 地图信息
                map ?:string
        // 开服天数
                kaiFuTianShu ?:number
        // 地形类型
                dixingType ?:string
        // 怪物骨骼信息
                guaiwuGuge ?:msgObj.GugeUnit
        // 去重后的怪物骨骼Id列表
                guaiwuGugeDefIdList ?:Array<number>
        // 碾压所需的战斗力
                fightOfCompleteSuppression ?:number
    }
// 大关数据(道馆挑战)
    interface DaguanDefUnit  {
        // Def ID
                id ?:number
        // 关卡名称
                name ?:string
        // 是否boss战
                boss ?:number
        // 挑战次数
                count ?:number
        // 战斗场景
                map ?:string
        // 奖励类型1
                typeAward1 ?:number
        // 奖励ID1
                item1 ?:number
        // 奖励数量1
                count1 ?:number
        // 奖励类型2
                typeAward2 ?:number
        // 奖励ID2
                item2 ?:number
        // 奖励数量2
                count2 ?:number
        // 奖励类型3
                typeAward3 ?:number
        // 奖励ID3
                item3 ?:number
        // 奖励数量3
                count3 ?:number
        // 奖励类型4
                typeAward4 ?:number
        // 奖励ID4
                item4 ?:number
        // 奖励数量4
                count4 ?:number
        // 队长头像
                para1 ?:number
        // 战斗力
                para2 ?:number
        // 关卡图标
                para3 ?:string
        // 关卡音乐
                para4 ?:string
    }
// 怪物数据
    interface GuaiwuDefUnit  {
        // Def ID
                id ?:number
        // 怪物名称
                name ?:string
        // 武将id
                hero ?:number
        // 攻击
                attack ?:number
        // 怪物缩放
                suofang ?:number
        // 战前说话
                xuanyan ?:string
        // 遗言
                yiyan ?:string
        // 怪物骨骼数据，可能为undefined
                guaiwuguge ?:msgObj.GugeUnit
        // 精英或BOSS光效
                lightingEffect ?:string
        // 坐骑
                zuoqi ?:number
        // 翅膀
                chibang ?:number
        // 怪物使用哪个 0 执行之前逻辑 1 zuoqi作为怪物
                type ?:number
    }
// 磨练副本数据
    interface MoLianFuBenDefUnit  {
        // Def ID
                id ?:number
        // 显示等级
                level ?:number
        // 推荐战斗力
                tuiJian ?:number
        // 头像和名称
                para1 ?:number
        // 战斗场景
                map ?:string
        // 1位敌人
                enemy1 ?:number
        // 2位敌人
                enemy2 ?:number
        // 3位敌人
                enemy3 ?:number
        // 4位敌人
                enemy4 ?:number
        // 5位敌人
                enemy5 ?:number
        // 6位敌人
                enemy6 ?:number
        // 奖励1
                typeAward1 ?:number
        // 对应ID1
                item1 ?:number
        // 奖励数量1
                count1 ?:number
        // 奖励2
                typeAward2 ?:number
        // 对应ID2
                item2 ?:number
        // 奖励数量2
                count2 ?:number
        // 奖励2
                typeAward3 ?:number
        // 对应ID3
                item3 ?:number
        // 奖励数量3
                count3 ?:number
        // 首通奖励1
                firstTypeAward1 ?:number
        // 对应ID1
                firstItem1 ?:number
        // 奖励数量1
                firstCount1 ?:number
        // 首通奖励2
                firstTypeAward2 ?:number
        // 对应ID2
                firstItem2 ?:number
        // 奖励数量2
                firstCount2 ?:number
        // 属性类型
                type1 ?:number
        // 数值
                value1 ?:number
        // 属性类型
                type2 ?:number
        // 数值
                value2 ?:number
        // 属性类型
                type3 ?:number
        // 数值
                value3 ?:number
        // 
                desc1 ?:string
        // 
                juqing ?:string
        // 
                biaoti ?:string
        // 
                mapFile ?:string
        // 
                diren ?:string
        // 
                charName ?:string
        // 
                direction ?:number
        // 
                dialogue ?:string
        // 
                victroyDialog ?:string
        // 
                direnType ?:number
        // 
                dituming ?:string
        // 队长的怪物信息，含骨骼信息
                leaderDef ?:msgObj.GuaiwuDefUnit
        // 调整等级
                fightLevel ?:number
        // 挑战条件领主关卡
                lingzhu ?:number
    }
// 磨练副本数据
    interface YinCangRenWuDefUnit  {
        // 
                id ?:number
        // 显示等级
                diren ?:string
        // 
                direnType ?:number
        // 
                direction ?:number
        // 
                desc1 ?:string
        // 战斗场景
                map ?:string
        // 奖励1
                typeAward1 ?:number
        // 对应ID1
                item1 ?:number
        // 奖励数量1
                count1 ?:number
        // 奖励2
                typeAward2 ?:number
        // 对应ID2
                item2 ?:number
        // 奖励数量2
                count2 ?:number
        // 奖励3
                typeAward3 ?:number
        // 对应ID3
                item3 ?:number
        // 奖励数量3
                count3 ?:number
        // 奖励4
                typeAward4 ?:number
        // 对应ID4
                item4 ?:number
        // 奖励数量4
                count4 ?:number
    }
// 副本翻倍
    interface FuBenFanBeiDefUnit  {
        // 
                id ?:number
        // 
                week ?:string
        // 
                multiple ?:number
    }
// 突发事件数据
    interface TuFaShiJianDefUnit  {
        // 显示等级
                diren ?:string
        // 
                desc1 ?:string
        // 奖励1
                typeAward1 ?:number
        // 对应ID1
                item1 ?:number
        // 奖励数量1
                count1 ?:number
        // 奖励2
                typeAward2 ?:number
        // 对应ID2
                item2 ?:number
        // 奖励数量2
                count2 ?:number
        // 奖励3
                typeAward3 ?:number
        // 对应ID3
                item3 ?:number
        // 奖励数量3
                count3 ?:number
    }
// 皮肤系统数据
    interface PiFuXiTongDefUnit  {
        // 
                id ?:number
        // 
                name ?:string
        // 
                type ?:number
        // 
                typeAward ?:number
        // 
                item ?:number
        // 
                count ?:number
        // 
                icon ?:string
        // 
                onsale ?:number
        // 单位毫秒
                kaiShiShiJian ?:number
        // 单位毫秒
                jieShuShiJian ?:number
        // 单位毫秒
                saleStart ?:number
        // 单位毫秒
                saleEnd ?:number
        // 
                daZhe ?:string
        // 
                xianJia ?:number
        // 
                type1 ?:number
        // 
                value1 ?:number
        // 
                type2 ?:number
        // 
                value2 ?:number
        // 
                type3 ?:number
        // 
                value3 ?:number
        // 
                type4 ?:number
        // 
                value4 ?:number
        // 
                type5 ?:number
        // 
                value5 ?:number
        // 
                type6 ?:number
        // 
                value6 ?:number
        // 
                chongZhiID ?:number
        // true    一直有      false
                hongdian ?:boolean
        // 套装组
                taozhuangGroup ?:number
    }
// 称号数据
    interface ChengHaoDefUnit  {
        // Def ID
                id ?:number
        // 分类
                type ?:number
        // 名称
                typename ?:string
        // 分类排序
                typeSequence ?:number
        // 名称
                name ?:string
        // 资源类型 1，图片 2，帧动画
                imageType ?:number
        // 称号资源名 称号美术资源名
                image ?:string
        // 起始帧
                startFrame ?:number
        // 结束帧
                endFrame ?:number
        // 获取途径
                getApproach ?:string
        // 获取类型 1，竞技场 使用字段（paihangLevelFrom，paihangLevelTo） 2，个人pvp 使用字段（pvpDanChangKill） 3，金券激活 使用字段（costType，count）
                getApproachType ?:number
        // 消耗类型 1,道具; 4,钻石 21,金券
                costType ?:number
        // 消耗道具ID
                itemId ?:number
        // 数量
                count ?:number
        // 上线跑马灯
                paomadeng ?:number
        // 经验加成,百分比
                expMuitiple ?:number
        // 属性
                propList ?:Array<PropertyInfoUnit>
        // pvp单场击杀 杀人数大于等于时
                pvpDanChangKill ?:number
        // 经验称号类型
                dianShuType ?:number
        // 称号所需互助点数
                dianShu ?:number
        // 特效名
                effectName ?:string
        // 缩放百分比
                scaling ?:number
    }
// 公会副本数据
    interface GongHuiFuBenDefUnit  {
        // 
                id ?:number
        // 
                Rank ?:number
        // 
                YincangBOSS ?:number
        // 
                Gailv ?:number
        // 
                para1 ?:number
        // 
                map ?:string
        // 
                dituming ?:string
        // 
                diren ?:string
        // 
                enemy1 ?:number
        // 
                enemy2 ?:number
        // 
                enemy3 ?:number
        // 
                enemy4 ?:number
        // 
                enemy5 ?:number
        // 
                enemy6 ?:number
        // 
                item1 ?:string
        // 
                type1 ?:string
        // 
                count1 ?:string
        // 
                item2 ?:string
        // 
                type2 ?:string
        // 
                count2 ?:string
        // 
                item3 ?:string
        // 
                type3 ?:string
        // 
                count3 ?:string
    }
// 战斗地铁
    interface ZhanDouDiTieDefUnit  {
        // 
                id ?:number
        // 
                goumaiTiliMAX ?:number
        // 
                goumaiTiliCost ?:number
        // 
                vip ?:number
    }
// 小镇入侵者怪物数据
    interface XiaozhenRuqinGuaiwuUnit  {
        // 怪物骨骼id
                guaiwu ?:number
        // 怪物位置集合【x,y】
                pos ?:string
    }
// 小镇入侵者数据
    interface XiaozhenRuqinDefUnit  {
        // id
                id ?:number
        // 机器人Id
                jiqirenDefId ?:number
        // 机器人名称
                jiqirenName ?:string
        // 平台
                pingtai ?:string
        // 背景图
                beijing ?:string
        // 怪物数据集合
                guaiwuList ?:Array<XiaozhenRuqinGuaiwuUnit>
    }
// 达到xx后全服可领取奖励情况
    interface FirstOneAllServerRewardUnit  {
        // FirstOneDefId
                defId ?:number
        // 达成条件的玩家显示内容
                playerStr ?:string
        // 全服玩家可领取的奖励内容
                rewardList ?:Array<JiangliInfoUnit>
        // 0 未达成；1已达成未领取；2已领取。已领取后通常不发的
                status ?:number
    }
// 跟随宠信息
    interface FollowPetUnit  {
        // 跟随宠id
                followPetDbId ?:number
        // 跟随宠数据表id
                followPetDefId ?:number
        // 跟随宠大小
                size ?:number
        // 生育率
                rearRate ?:number
        // 跟随宠第几代
                generation ?:number
        // 是否跟随中
                isFollow ?:boolean
        // 属性信息
                propertyInfos ?:Array<HeroPropertyInfoUnit>
        // 皮肤id
                piFuId ?:number
    }
// 装饰
    interface PetRoomDecorateUnit  {
        // 外显类型 1,背景；2,装饰 3,食盆 4,垫子
                type ?:number
        // 当前等级
                level ?:number
        // 装扮的装饰数据id
                defId ?:number
        // 经验值
                exp ?:number
    }
// 房间寄养的宠物
    interface PetRoomParkPetUnit  {
        // 寄养者或被寄养的玩家id
                playerId ?:number
        // 还剩多久回家
                leftTime ?:number
        // 寄养者或被寄养的的服务器id
                serverId ?:number
        // 寄养者或被寄养的服务器编号
                serverNum ?:number
        // 寄养者或被寄养的玩家名字
                playerName ?:string
        // 寄养者或被寄养的玩家等级
                playerLv ?:number
        // 使用的装饰
                roomDecorate ?:Array<PetRoomDecorateUseUnit>
        // 跟随宠物
                followPet ?:msgObj.FollowPetUnit
        // 皮肤id
                piFuId ?:number
    }
// 随机的房间
    interface FollowPetRdmRoomUnit  {
        // 房间主人id
                playerId ?:number
        // 寄养者或被寄养的的服务器id
                serverId ?:number
        // 服务器编号
                serverNum ?:number
        // 玩家名字
                playerName ?:string
        // 头像id
                headId ?:number
        // 玩家等级
                playerLv ?:number
        // 房间装饰
                roomDecorate ?:Array<PetRoomDecorateUnit>
        // 房间装饰属性影响值
                heroPropertyInfos ?:Array<HeroPropertyInfoUnit>
        // 房间等级
                roomLv ?:number
        // 宠物
                followPet ?:msgObj.FollowPetUnit
        // 点赞数
                dianZanCount ?:number
        // 房间公告
                gongGao ?:string
        // 房间是否设置了密码
                isLock ?:boolean
        // 房间号
                roomId ?:number
        // 是否可以通过消耗道具进入房间
                isDaoJu ?:boolean
        // 房间饰品位置信息
                shiPinList ?:Array<ShiPinPositionUnit>
        // 皮肤id
                petPiFuId ?:number
    }
// 使用的装饰
    interface PetRoomDecorateUseUnit  {
        // 外显类型 1,背景；2,装饰 3,食盆 4,垫子
                type ?:number
        // 装扮的装饰数据id
                defId ?:number
    }
// 宠物子代蛋
    interface FollowPetEggUnit  {
        // 子代跟随宠
                petDefId ?:number
        // 亲家名字
                friendName ?:string
        // 亲家跟随宠defId
                friendPetDefId ?:number
        // 亲家跟随宠defId
                heroProp ?:Array<HeroPropertyInfoUnit>
    }
// 宠物屋饰品
    interface FollowPetRoomShiPinUnit  {
        // 饰品类型
                defType ?:number
        // 激活条件
                costInfo ?:msgObj.JiangliInfoUnit
        // 拥有的上限
                ownLimit ?:number
        // 资源
                icon ?:string
        // 我现在拥有的数量
                myCount ?:number
        // 放置在房间内的数量
                countInRoom ?:number
        // 属性列表
                shuXingList ?:Array<ShuXingInfoUnit>
    }
// 宠物屋饰品
    interface ShiPinPositionUnit  {
        // 饰品类型
                defType ?:number
        // 位置类型
                locType ?:number
        // 横坐标
                px ?:number
        // 纵坐标
                py ?:number
        // z轴
                pz ?:number
    }
// 跟随宠皮肤
    interface FollowPetPiFuUnit  {
        // defId
                defId ?:number
        // 名称
                name ?:string
        // 技能描述
                skilldesc ?:string
        // 激活类型
                typeAward ?:number
        // 激活道具
                item ?:number
        // 激活数量
                count ?:number
        // 是否已经穿戴
                isPutOn ?:boolean
        // 是否已经激活
                isJiHuo ?:boolean
        // 属性列表
                shuXingInfoList ?:Array<ShuXingInfoUnit>
    }
// 新的跟随宠进阶显示
    interface NewFollowPetShowUnit  {
        // 显示
                showType ?:number
        // 开启阶数
                openJieDuan ?:number
        // 名称
                name ?:string
        // 品质
                pinzhi ?:number
        // 头像
                icon ?:string
        // 骨骼动画
                armature ?:string
        // 形象
                image ?:string
        // 插槽形象
                slotsName ?:string
    }
// 被禁止登录账户信息
    interface ForbiddenAccountInfoUnit  {
        // Id
                Id ?:number
        // 账户id
                accountId ?:number
        // 渠道
                channel ?:string
    }
// 免费时装数据
    interface FreeFashionUnit  {
        // 免费时装表Def Id
                defId ?:number
        // 名称
                name ?:string
        // 条件列表
                conditionList ?:Array<FreeFashionConditionUnit>
        // 图标3
                icon3 ?:string
        // 点击图片弹出描述3
                tanchu3 ?:string
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
        // 0，不可领取；1，可领取奖励；2，已领取奖励
                status ?:number
        // 剩余件数
                leftRewardCount ?:number
    }
// 免费时装条件数据
    interface FreeFashionConditionUnit  {
        // 条件描述
                desc ?:string
        // 图标
                icon ?:string
        // 是否已经完成该条件
                isComplete ?:boolean
        // 跳转ID
                jump ?:number
        // 点击图片弹出描述
                tanchu ?:string
    }
// 好友信息
    interface FriendsInfoUnit  {
        // 玩家id
                friendId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家头像
                playerTouXiangId ?:number
        // 等级段
                levelPart ?:number
        // 排位赛defId
                paihangPPDefId ?:number
        // 
                level ?:number
        // 
                vipLevel ?:number
        // 
                nobleLevel ?:number
        // true在线 false 不在
                isOnline ?:boolean
        // 魅力值
                meiLi ?:number
        // 好友数量
                friendsCount ?:number
        // 0送精灵球   1送玫瑰  2挑战成功   3挑战失败
                logType ?:number
        // 战斗力
                zhandouli ?:number
        // 日志的时间戳
                logTime ?:number
        // 申请记录的id 需要根据这个删除申请记录
                requestRecordDbId ?:number
        // 0发出者 1接受者
                type ?:number
        // 如果赠送类型是玫瑰 这字段代表数量
                zengSongCount ?:number
        // 送过的精灵球数量
                zengSongJLQ ?:number
        // 离线时长 单位秒
                liXianShiChang ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 充值金额defId
                chongzhijinedefId ?:number
        // 好友赠送索要金券留言
                message ?:string
        // 是否已经赠送
                isFinish ?:boolean
        // 是否已经索要
                isAsked ?:boolean
        // 公会名称
                gongHuiName ?:string
        // 公会徽章 边框,底图,标志
                gongHuiHuiZhang ?:string
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 好友聊天内容信息
    interface FriendsMsgContentInfoUnit  {
        // 玩家id
                playerId ?:number
        // 好友id
                friendId ?:number
        // 聊天内容
                content ?:string
        // 最后一条信息的时间（用于排序）
                time ?:number
        // 
                level ?:number
        // 好友Friend的个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 好友聊天信息
    interface FriendsMsgInfoUnit  {
        // 玩家id
                friendId ?:number
        // 私信红点 有true  没有 false
                flg ?:boolean
        // 最后一条信息的时间（用于排序）
                time ?:number
        // 
                level ?:number
        // 好友Friend的个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 副本中心数据
    interface FuBenLieBiaoUnit  {
        // 次数
                ciShu ?:number
        // 倒计时
                deadTime ?:number
        // 已经打了多少次
                hasFight ?:number
        // 恢复到多少停止
                cishuMax ?:number
        // 已经购买了多少次
                hasBuy ?:number
    }
// 副本中心数据
    interface FuBenInfoUnit  {
        // ID，静态表主Key
                defIdList ?:Array<number>
    }
// 礼包信息
    interface GiftPackageInfoUnit  {
        // defId，对应shenchonglibao表中defId
                id ?:number
        // 模板类型
                mobanType ?:number
        // 排序
                index ?:number
        // 商品名称
                name ?:string
        // 消耗类型
                costType ?:number
        // 消耗数量
                costValue ?:number
        // 图标
                icon ?:string
        // 
                banner ?:string
        // 奖励列表
                AwardList ?:Array<JiangliInfoUnit>
        // 领取状态：0、不可领；1、可以领，但尚未领取 2、已领取
                doGet ?:number
        // 按钮位置
                btnPostion ?:number
        // 按钮图标
                btnIcon ?:string
        // 结束时间（比服务器时间晚1秒）
                finishTime ?:number
    }
// 公告信息
    interface GongGaoInfoUnit  {
        // VIP等级
                vip ?:number
        // 跑马灯内容
                paoMaDengWords ?:string
    }
// 公会活跃度奖励信息
    interface GongHuiActivityPointRewardUnit  {
        // 活跃度奖励表Id：lianmenghuoyue表Id
                defId ?:number
        // 活跃度条件值
                needPoint ?:number
        // 所需职位，0表示没有职位限制
                needZhiwei ?:number
        // 活跃度奖励内容
                rewardList ?:Array<JiangliInfoUnit>
        // 最大可领取份数，0表示无限制
                maxSize ?:number
        // 当前已经领取份数
                currSize ?:number
        // 是否可以领取该份奖励
                isCan ?:boolean
        // 我是不是已经领取过该份奖励了
                isGotReward ?:boolean
    }
// 用在公会战显示对象上
    interface GongHuiBattleMemberInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家创建角色时,所属服务器编号
                sourceServerNum ?:number
        // 合服后的实际服务器ID,合服前,跟创角服务器相同
                serverId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家头像ID
                playerTouxiangId ?:number
        // 玩家坐骑ID
                playerZuoqi ?:number
        // 玩家翅膀ID
                playerChibang ?:number
        // 玩家战斗力
                playerFight ?:number
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 玩家形象 地图跑动小人的男女
                xingXiang ?:number
        // 第几回合失败
                loseRound ?:number
        // 第几回合退场 一个人最多战斗10场就下去了
                retreatRound ?:number
        // 每一回合以后 剩余血量的百分比
                remainHPList ?:Array<number>
        // 是否离线3天 1是 0否
                afk3 ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 用在公会战传递数据
    interface GongHuiZhanPiPeiInfoUnit  {
        // 本服公会id
                localGHid ?:number
        // 公会id
                gonghuiId ?:number
        // 公会名称
                gonghuiName ?:string
        // 会长角色Id
                huizhangPlayerId ?:number
        // 会长头像
                huizhangTouxiang ?:number
        // 会长名称
                huizhangName ?:string
        // 公会战力
                zhanLi ?:string
        // 回合数
                round ?:number
        // 得分，32强后的排序规则
                score ?:number
        // 服务器短id
                serverNo ?:number
        // 服务器长id
                serverId ?:number
        // 对应的DBid
                dbId ?:number
        // 公会对战记录id
                fightlogId ?:number
        // 决赛分配 默认0 1是12名  2是34名
                finGroup ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 用在公会战显示对象上 用在回放上
    interface GongHuiZhanMemberHuiFangInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家形象 地图跑动小人的男女
                xingXiang ?:number
        // 第几回合失败
                loseRound ?:number
        // 第几回合退场 一个人最多战斗10场就下去了
                retreatRound ?:number
        // 每一回合以后 剩余血量的百分比
                remainHPList ?:Array<number>
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 用在公会战荣耀榜传递等
    interface GongHuiZhanRankInfoUnit  {
        // 排名
                rank ?:number
        // 公会名称
                gonghuiName ?:string
        // 公会id
                gonghuiId ?:number
        // 公会等级
                gonghuiLevel ?:number
        // 公会人数
                gonghuiNum ?:number
        // 公会战力
                gonghuiZhanli ?:string
        // 会长名称
                huizhangName ?:string
        // 会长数据
                huizhangInfo ?:msgObj.GongHuiBattleMemberInfoUnit
        // 服务器no
                serverNo ?:number
    }
// 客户端传输界面信息用
    interface GongHuiZhanGHInfoUnit  {
        // 公会id
                gonghuiId ?:number
        // 公会名称
                gonghuiName ?:string
        // 公会战力
                gonghuiZhanli ?:string
        // 会长名称
                huizhangName ?:string
        // 会长头像
                huizhangTouxiang ?:number
        // 服务器no
                serverNo ?:number
        // 是否是A组
                isFightA ?:number
        // 新版会长个人形象数据
                huizhangPhoto ?:msgObj.PlayerPhotoUnit
    }
// 客户端传输界面信息用
    interface GongHuiZhanFightUnit  {
        // 定位标识
                pipeiDbId ?:number
        // 分组数据标识，服务器用
                crossid ?:number
        // 决赛排序用，服务器用 2：季军 1：冠亚
                finGroup ?:number
        // A公会
                gonghuiA ?:msgObj.GongHuiZhanGHInfoUnit
        // 战斗时间
                fightTime ?:number
        // B公会
                gonghuiB ?:msgObj.GongHuiZhanGHInfoUnit
        // 胜利公会Id
                winId ?:number
        // 战斗log  -1 无对手 -2 对手放弃
                fightlog ?:number
        // 战斗log所在服务器id
                logServerId ?:number
    }
// 公会战个人战斗回放 中 敌人显示信息
    interface GongHuiZhanPVPHuiFangEnemyInfoUnit  {
        // 敌人名字
                enemyName ?:string
        // 敌人战力
                enemyZhanLi ?:number
        // 敌人头像
                enemyTouXiang ?:number
        // 新版敌人个人形象数据
                enemyPhoto ?:msgObj.PlayerPhotoUnit
    }
// 个人回放传信息用
    interface GongHuiZhanMemberFightUnit  {
        // 请求方信息
                playerInfo ?:msgObj.GongHuiZhanPVPHuiFangEnemyInfoUnit
        // 对手信息
                enemyInfo ?:msgObj.GongHuiZhanPVPHuiFangEnemyInfoUnit
        // 1 你赢了  2 你输了  3平局
                fightResultType ?:number
        // 战斗log  -1 无对手 -2 对手放弃
                fightlog ?:number
        // 最后的刷新时间
                fightTime ?:number
        // 战斗log所在服务器id
                logServerId ?:number
    }
// 公会战刷新group用信息
    interface GongHuiZhanGroupFreshInfoUnit  {
        // 公会id
                gonghuiId ?:number
        // 回合
                round ?:number
        // 积分
                score ?:number
        // 战斗id
                fightDbId ?:number
        // 对手公会
                enemyInfo ?:msgObj.GongHuiZhanGHInfoUnit
        // 战斗log所在服务器id
                logServerId ?:number
        // 胜方ID
                winId ?:number
    }
// 公会boss怪物info
    interface GongHuiBossGuaiWuInfoUnit  {
        // 公会bossdefId
                id ?:number
        // 这个boss需要的公会等级
                level ?:number
        // boss形象
                bossImage ?:string
        // 战斗场景
                map ?:string
        // boss名字
                bossName ?:string
        // boss描述
                bossDes ?:string
        // boss必掉奖励List
                biDiaoJiangliList ?:Array<JiangliInfoUnit>
        // boss骨骼信息
                bossGuge ?:msgObj.GugeUnit
    }
// 用在公会Boss日志中伤害排行
    interface GongHuiBossLogUnit  {
        // 排行 从1开始
                paiHang ?:number
        // 玩家角色Id
                playerId ?:number
        // 名字
                name ?:string
        // 次数
                value1 ?:number
        // 伤害
                value2 ?:number
    }
// 公会副本
    interface GongHuiFuBenInfoUnit  {
        // 大关Id
                daGuanId ?:number
        // 章节名字
                daGuanName ?:string
        // 打到第几关
                jinDu ?:number
        // 怪物形象
                bossWuJiang ?:number
        // 掉落奖励
                diaoLuojiangLiList ?:Array<JiangliInfoUnit>
        // 开启等级
                openLevel ?:number
        // 开启状态
                openStatus ?:number
    }
// 用在公会副本列表的数据
    interface GongHuiFuBenGuaiWuInfoUnit  {
        // 
                dbId ?:number
        // 
                defId ?:number
        // 剩余的血量百分比  剩余百分之10  这个数就是10  如果这个是值不是100 要显示血条
                leftHp ?:number
        // true 死了   flase  还在
                isDead ?:boolean
    }
// 用在公会仓库面板的数据
    interface GongHuiCangKuInfoUnit  {
        // 
                dbId ?:number
        // ID，静态表主Key
                itemId ?:number
        // 
                type ?:number
        // 数量
                count ?:number
    }
// 用在公会分配日志  35条
    interface GongHuiFenZangLogUnit  {
        // 字符串
                str ?:string
        //  单位  秒   分发时间与当前时间的差值   格式 一天内的 满了一小时 显示 xx小时以前  不足 显示xxx分钟以前  以前以上 显示xxx天以前
                time ?:number
    }
// 用在公会副本日志 中  捐献排行和伤害排行
    interface GongHuiFuBenLogUnit  {
        // 排行 从1开始
                paiHang ?:number
        // 名字
                name ?:string
        // 数字
                value ?:number
    }
// 公会副本奖励
    interface GongHuiFuBenRewardUnit  {
        // 小关Id
                xiaoGuanId ?:number
        // 首通奖励
                shouTongJiangLiList ?:Array<JiangliInfoUnit>
        // 参与奖励
                canYuJiangLiList ?:Array<JiangliInfoUnit>
    }
// 公会副本小关信息
    interface GongHuiFuBenXiaoGuanInfoUnit  {
        // 小关Id
                xiaoGuanId ?:number
        // boss形象
                bossWuJiang ?:number
        // 是否首通
                isShouTong ?:boolean
        // boss当前血量
                curBossHp ?:number
        // boss最大血量
                maxBossHp ?:number
        // 小关奖励
                jiangLi ?:msgObj.GongHuiFuBenRewardUnit
    }
// 公会副本房间信息
    interface GongHuiFuBenRoomInfoUnit  {
        // 房间号
                roomId ?:number
        // 大关Id
                daGuanId ?:number
        // 小关Id
                xiaoGuanId ?:number
        // 剩余可操作次数
                leftShouYiCount ?:number
        // 剩余可参与次数
                leftJoinCount ?:number
        // 是否满员战斗
                isFullFight ?:boolean
        // 强制cd战斗
                isTimeFight ?:boolean
        // 房间内的玩家信息
                personList ?:Array<GongHuiFuBenRoomPersonUnit>
        // 概率加成，千分比的数字，百分之20这里的赋值是200.
                probabilityBonus ?:number
    }
// 公会副本房间信息
    interface GongHuiFuBenRoomPersonUnit  {
        // 玩家ID
                playerId ?:number
        // 1 队长 2 中间 3 右边
                position ?:number
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 玩家昵称
                playerName ?:string
        // 玩家头像ID
                playerTouxiangId ?:number
        // 玩家战斗力
                playerFight ?:number
        // 玩家等级
                playerLevel ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 公会副本日志
    interface GongHuiFuBenDamageUnit  {
        // 排名
                position ?:number
        // 玩家角色Id
                playerId ?:number
        // 名字
                name ?:string
        // 总伤害
                totalDamage ?:number
    }
// 公会副本备战英雄
    interface GongHuiFuBenRoomHeroUnit  {
        // 玩家Id
                playerId ?:number
        // 玩家名字
                playerName ?:string
        // 1 队长 2 中间 3 右边
                position ?:number
        // 第一个精灵信息
                heroInfoUnit1 ?:msgObj.HeroInfoUnit
        // 第二个精灵信息
                heroInfoUnit2 ?:msgObj.HeroInfoUnit
        // 第一个精灵的装备ID列表
                hero1ZhuangbeiIdList ?:Array<number>
        // 第二个精灵的装备ID列表
                hero2ZhuangbeiIdList ?:Array<number>
    }
// 用在公会副本日志 中  掉落信息
    interface GongHuiFuBenDiaoluoLogUnit  {
        // 掉落时间，单位：毫秒
                time ?:number
        // 掉落内容详细描述
                desc ?:string
    }
// 公会福利
    interface GongHuiFuLiInfoUnit  {
        // 福利宝箱的dbId 领取的时候把这个传给后端
                id ?:number
        // 发送宝箱的玩家名称
                playerName ?:string
        // 
                leixng ?:number
        // 
                defId ?:number
        // 宝箱的名字
                baoXiangName ?:string
        // 职位
                zhiWei ?:number
        // 总共可领取的数量
                totalCount ?:number
        // 总共可领取的数量
                leftCount ?:number
        // 生成时间单位 分钟
                createTime ?:number
        // true  领过了  false 没有
                isGet ?:boolean
        // 
                playerTouXiang ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 公会福利Log
    interface GongHuiFuLiLogInfoUnit  {
        // 
                dbId ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 领取者的名字
                playerName ?:string
        // 生成时间单位 分钟
                getTime ?:number
        // 领取时候的等级
                level ?:number
        // 领取者Id
                playerId ?:number
        // true是自己   false不是
                isOwn ?:boolean
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 公会每日捐赠的日志
    interface GongHuiHuoYueLogInfoUnit  {
        // 捐赠时间
                time ?:number
        // 玩家名字
                playerName ?:string
        // 1 2 3 代表钱 4代表精灵
                type ?:number
        // 精灵的defId 是精灵捐赠才用
                defId ?:number
    }
// 用在公会排行的数据
    interface GongHuiListInfoUnit  {
        // 显示服务器序号（短）
                serverNumShow ?:number
        // 公会dbId
                id ?:number
        // 公会名称
                name ?:string
        // 公会等级
                level ?:number
        // 宣言
                xuanYan ?:string
        // 公会已经有的人数
                renshu ?:number
        // 公会排行
                paiHang ?:number
        // 会长名字
                huiZhangName ?:string
        // 0自动加入  1批准加入 权限 会长 副会长
                joinType ?:number
        // 有没有申请过,true有   false 没有
                hasRequest ?:boolean
        // 徽章图名字
                huiZhangTu ?:string
        // 积分
                totalJiFen ?:number
        // 成员们的总产出
                totalChuanChu ?:number
        // 公会徽章 边框,底图,标志
                gongHuiHuiZhang ?:string
        // 会长Player.id
                huiZhangPlayerId ?:number
    }
// 公会成员信息
    interface GongHuiMemberInfoUnit  {
        // 
                id ?:number
        // 玩家昵称
                name ?:string
        // 职位 1会长2副会长3长老4精英5普通会员
                zhiWei ?:number
        // 玩家头像
                playerTouXiangId ?:number
        // 
                level ?:number
        // 
                vipLevel ?:number
        // 等级段
                levelPart ?:number
        // 排位赛defId
                paihangPPDefId ?:number
        // 贡献
                gongXian ?:number
        // 今日贡献
                gongXianToday ?:number
        // 战斗力
                zhandouli ?:number
        // true在线 false 不在
                isOnline ?:boolean
        // 玩家离线时长  当前时间 - 上次登出时间
                liXianShiChang ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 公会活跃度
                activityPoint ?:number
    }
// 公会拍卖物品信息
    interface GongHuiPaiMaiInfoUnit  {
        // 数据库id
                dbId ?:number
        // 开始购买时间戳
                startBuyTime ?:number
        // 结束购买时间戳
                endBuyTime ?:number
        // 奖励信息
                jiangliInfo ?:msgObj.JiangliInfoUnit
        // 购买奖励消耗信息
                costBuyInfo ?:msgObj.JiangliInfoUnit
    }
// 公会商店信息
    interface GongHuiShangDianInfoUnit  {
        // 商店里的商品id
                defId ?:number
        // 已经买的数量
                hasBuyCount ?:number
    }
// 用在跨服积分赛公会统计的数据
    interface GongHuiTongJiInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 职位
                zhiwei ?:number
        // 昵称
                name ?:string
        // 个人积分
                jifen ?:number
    }
// 公会战排行数据
    interface GongHuiZhanPlayerInfoUnit  {
        // 排名
                index ?:number
        // 玩家ID
                playerId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家头像ID
                headId ?:number
        // 公会名字
                gongHuiName ?:string
        // 对战数量
                fightSize ?:number
        // 胜场
                winSize ?:number
        // 积分
                score ?:number
        // 玩家奖励比例,万分比
                playerValue ?:number
        // 活动期间获得的累积经验值,只发自己的,不发排行列表玩家的
                exp ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 公会数据
    interface GongHuiDataUnit  {
        // 公会id
                gongHuiId ?:number
        // 公会积分
                gongHuiScore ?:number
        // 公会人数
                gongHuiMember ?:number
        // 公会等级
                gongHuiLv ?:number
        // 公会战力
                gongHuiFight ?:number
        // 活跃人数
                gongHuiLiveCount ?:number
        // 公会所在服务器id
                serverId ?:number
        // 公会所在服务器num
                serverNum ?:number
    }
// 公会战公会数据
    interface GongHuiZhanGongHuiInfoUnit  {
        // 排名
                rank ?:number
        // 公会名字
                gongHuiName ?:string
        // 公会会长头像ID
                headId ?:number
        // 公会积分
                score ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 公会战鼓舞信息
    interface GongHuiZhanGuWuInfoUnit  {
        // 还有多久可以鼓舞
                leftCdTime ?:number
        // 当前次数
                curTimes ?:number
        // 最大次数
                maxTimes ?:number
    }
// 公会战排行奖励信息
    interface GongHuiZhanRankRewardInfoUnit  {
        // 类型
                type ?:number
        // 排名开始
                rankStart ?:number
        // 排名结束
                rankEnd ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
    }
// 攻坚战道馆
    interface GongJianZhanDaoGuanInfoUnit  {
        // 显示服务器序号
                serverNumShow ?:number
        // 
                defId ?:number
        // 道馆名字
                daoGuanName ?:string
        // 占领率 百分比的数 前端显示就行
                percent ?:number
        // 馆主的名字
                guanZhuName ?:string
        // 开放时间点  秒
                openTime ?:number
        // 0 npc  1 默认主角  2幻化皮肤
                piFuImgType ?:number
        // 如果piFuImgType是2 是幻化皮肤类型 这里放皮肤的defid
                piFuDefId ?:number
        // 这个玩家是不是再这个道馆
                inThis ?:boolean
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 如果该道馆归NPC，则显示NPC的骨骼动画
                npcGuge ?:msgObj.GugeUnit
    }
// 攻坚战道馆人物信息
    interface GongJianZhanRenWuInfoUnit  {
        // 显示服务器编号
                serverNumShow ?:number
        // 道馆图
                daoGuanMap ?:string
        // true 是npc false 不是   如果是npc用piFuTu代表小人形象  false代表真人 用 piFuDefIdList当做形象 参考以前的代码
                isNpc ?:boolean
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 玩家头像ID
                playerTouxiangId ?:number
        // 玩家坐骑ID
                playerZuoqi ?:number
        // 玩家翅膀ID
                playerChibang ?:number
        // 战力
                zhanLi ?:number
        // 玩家名
                name ?:string
        // 是不是同工会 true是  false不是
                isSameGongHui ?:boolean
        // 第几个位置
                weiZhi ?:number
        // 
                defId ?:number
        // 生产积分
                score ?:number
        // 占领时间点 秒
                holdTime ?:number
        // 保护时间截止点 秒
                protectEndTime ?:number
        // 公会名字
                gongHuiName ?:string
        // 职位
                zhiWei ?:number
        // 玩家id
                playerId ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 如果该道馆归NPC，则显示NPC的骨骼动画
                npcGuge ?:msgObj.GugeUnit
    }
// 首届馆主信息
    interface GuanZhuInfoUnit  {
        // 
                playerTouXiang ?:number
        // 玩家名
                playerName ?:string
        // 道馆名字
                daoGuanName ?:string
        // 战力
                zhanLi ?:number
        // 徽章图片名
                huiZhangTu ?:string
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 指派列表信息
    interface ZhiPaiInfoUnit  {
        // 
                playerTouXiang ?:number
        // 玩家名
                playerName ?:string
        // 战力
                zhanLi ?:number
        // 玩家战力
                playerId ?:number
    }
// 挂机敌人信息   战斗记录也用这个
    interface GuaJiEnergyInfoUnit  {
        // 敌人的dbid
                energyPlayerDBId ?:number
        // 战斗力
                playerFightPower ?:number
        // 竞技场头衔
                paihangbw ?:number
        // 等级
                energyLevel ?:number
        // 名字
                energyName ?:string
        // 服务器号 短id 用于显示
                serverInfo ?:number
        // 服务器ID 长id 用于跨服名片消息
                serverChangId ?:number
        // 这个记录的状态  这个记录的状态  0失败   1复仇   2复仇失败  3复仇成功  4战斗胜利 5战斗失败
                status ?:number
        // 记录的DbId
                recordDbId ?:number
        // 头像
                playerTouxiangId ?:number
        // 坐骑id
                playerZuoqi ?:number
        // 翅膀id
                playerChibang ?:number
        // 建号选择宠物的defId
                xuanZeChongWuDefId ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 称号
                chenghaoId ?:number
        // 神宠ID
                shenchongDefId ?:number
        // 神宠使用的技能ID
                shenchongSkillId ?:number
        // 神宠属性
                shenchongPropValues ?:string
        // 跟随宠显示
                followPetShow ?:msgObj.NewFollowPetShowUnit
        // 跟随宠大小
                followPetSize ?:number
        // 跟随宠皮肤
                followPetPiFuId ?:number
        // 皮神进阶外显皮肤
                displayLevel ?:number
        // 光环
                guangHuan ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 挂机敌人信息  记录信息 用在 战斗记录列表和 仇人列表
    interface GuaJiEnergyRecordAndChouRenInfoUnit  {
        // 敌人的dbid
                energyPlayerDBId ?:number
        // 战斗力
                playerFightPower ?:number
        // 等级
                energyLevel ?:number
        // 名字
                energyName ?:string
        // 所在服务器 用于展示
                serverInfo ?:number
        // 这个记录的状态  这个记录的状态  0失败   1复仇   2复仇失败  3复仇成功  4战斗胜利 5战斗失败 6邮件夺回成功
                status ?:number
        // 记录的DbId
                recordDbId ?:number
        // 是不是宿敌 用在战斗记录列表里
                isSuDi ?:boolean
        // 抢夺列表
                qiangDuoList ?:Array<JiangliInfoUnit>
        // 仇人打我多少次
                fightTimes ?:number
        // 我打仇人多少次
                fightCount ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 是否在线
                isOnline ?:boolean
        // 最后搜索我的时间，单位：毫秒
                theLastSearchTime ?:number
        // 剩余可被抢次数
                leftNumberOfRobbers ?:number
        // 在【神魔战场】中击杀我N次
                serverBossKillMeTimes ?:number
        // 【神魔战场】Project.Id
                serverBossProjectId ?:number
    }
// 复仇挂机敌人搜索单项信息
    interface RevengeEnemySearchProjectInfoUnit  {
        // 1、神兽战场；2、小镇坐标
                type ?:number
        // Project表Id
                projectDefId ?:number
        // 对应的表Id，如果是神兽战场，则表示BossZhiJiaDef.id
                defId ?:number
        // 对应的表.name，如果是神兽战场，则表示地图名
                defName ?:string
        // 小镇x坐标
                x ?:number
        // 小镇y坐标
                y ?:number
        // 小镇剩余可攻击次数，该次数为0，表示被击飞了，不可以再被攻击
                canAttackCount ?:number
        // 小镇无敌盾结束时间，单位：毫秒
                notAttackTime ?:number
    }
// 复仇挂机敌人搜索单项的Project状态
    interface RevengeEnemySearchProjectStatusUnit  {
        // 1、神兽战场；2、小镇坐标
                type ?:number
        // Project表Id
                projectDefId ?:number
        // 对手是否开启当前Project
                isEnemyOpen ?:boolean
        // 我自己是否开启当前Project
                isSelfOpen ?:boolean
    }
// 复仇挂机敌人搜索单项的操作内容
    interface StateRevengeEnemyProjectSearchOperationUnit  {
        // 1、本服所有内容；2、跨服搜索位面战争
                searchType ?:number
        // 目标服务器Id
                targetServerId ?:number
    }
// 挂机每小时奖励信息
    interface GuaJiHourRewardUnit  {
        // 每小时 金币 经验果实 精灵币 奖励
                hourRewardList ?:Array<JiangliInfoUnit>
        // 提升 每小时 金币 经验果实 精灵币 奖励
                hourUpRewardList ?:Array<JiangliInfoUnit>
    }
// 挂机展示宝箱信息
    interface GuaJiShowChestElementUnit  {
        // 奖励
                reward ?:msgObj.JiangliInfoUnit
        // 单次奖励
                singleReward ?:msgObj.JiangliInfoUnit
        // 每小时 金币 经验果实 精灵币 奖励
                hourReward ?:msgObj.JiangliInfoUnit
    }
// 骨骼显示信息
    interface GugeUnit  {
        // 剧情表Id，对应策划表：guajijuqing
                id ?:number
        // 眼
                yan ?:string
        // 眉毛
                meim ?:string
        // 嘴
                rzui ?:string
        // 鼻子
                bizi ?:string
        // 脸部装饰
                liashi ?:string
        // 头
                lian ?:string
        // 头发
                toufa ?:string
        // 身体
                shenzi ?:string
        // 左臂
                bizuo ?:string
        // 右臂
                biyou ?:string
        // 腿
                tui ?:string
        // 肩
                jianba ?:string
        // 后层
                houce ?:string
        // 武器
                wuqi ?:string
        // 盾牌
                dunpai ?:string
        // 翅膀
                chibang ?:string
        // 缩放比例
                suofang ?:number
        // 名字1
                name1 ?:string
        // 名字2
                name2 ?:string
        // 职业 1 群攻 2 刺客 3 防御 4 治疗 5 控制
                photo ?:string
        // 动作和表情
                biaoqing ?:number
        // 挂机出门的怪说话
                juqing ?:string
        // 数据是否来自怪物骨骼
                isFromGuaiwuGuge ?:boolean
        // guaiwuguge表hetu
                hetu ?:number
        // 肩2最前层
                jianbb ?:string
        // 头像
                face ?:string
    }
// 剧情对话
    interface JuqingDuihuaUnit  {
        // 形象，guge表的Id
                xingxiang ?:number
        // 动作和表情
                biaoqing ?:number
        // 剧情
                juqing ?:string
        // NPC名称
                npcName ?:string
    }
// 剧情信息
    interface StoryLineUnit  {
        // Entity Db Id
                dbId ?:number
        // 剧情表Id，对应策划表：guajijuqing
                defId ?:number
        // 类型
                type ?:number
        // 子类，对应策划表字段：subType
                subClass ?:number
        // 剧情按钮列表
                juqingAnniuList ?:Array<string>
        // 剧情按钮操作列表
                juqingAnniuCaozuoList ?:Array<number>
        // 消耗，无消耗，该值为undefined
                cost ?:msgObj.JiangliInfoUnit
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 出现时间，单位：毫秒
                showTime ?:number
        // 剧情出选择按钮
                juqingDuihuaUnit ?:msgObj.JuqingDuihuaUnit
        // 剧情对话前置
                prevJuqingDuihuaUnit ?:Array<JuqingDuihuaUnit>
        // 剧情对话后置
                nextJuqingDuihuaUnit ?:Array<JuqingDuihuaUnit>
        // 地图中的形象，数据例如1;100
                npcList ?:Array<GugeUnit>
    }
// 老乞丐对话
    interface LaoqigaiDuihuaUnit  {
        // 老乞丐表Id，对应策划表：laoqigai
                defId ?:number
        // 剧情出选择按钮
                juqingDuihuaUnit ?:msgObj.JuqingDuihuaUnit
        // 剧情对话前置
                prevJuqingDuihuaUnit ?:Array<JuqingDuihuaUnit>
        // 剧情对话后置
                nextJuqingDuihuaUnit ?:Array<JuqingDuihuaUnit>
        // 地图中的形象，数据例如1;100
                npcList ?:Array<GugeUnit>
        // 老乞丐消耗列表
                costList ?:Array<JiangliInfoUnit>
        // 上次的奖励列表，如果是最后一个了，则是当前的奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 是否领取过奖励了
                isHadReward ?:boolean
    }
// 关卡数据
    interface GuanqiaInfoUnit  {
        // 关卡数据表dbID，数据表主Key
                guanqiaDbID ?:number
        // 关卡ID，静态表主Key
                guanqiaID ?:number
        // 今天挑战次数
                count ?:number
        // 1没通关  2通关
                isTongGuan ?:number
        // 战斗获取星星数量
                starcount ?:number
        // 今天购买了多少次
                buyTimes ?:number
    }
// 引导礼包显示数据
    interface GuildGiftPackInfoUnit  {
        // 礼包Id，领奖用
                giftPackId ?:number
        // Jump Id
                jumpId ?:number
    }
// 英雄阵容信息
    interface HeroFormInfoUnit  {
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_11 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_12 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_13 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_14 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_15 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_16 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_17 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_18 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_19 ?:number
    }
// 英雄Log数据
    interface HeroLogInfoUnit  {
        // 武将ID, 静态ID
                heroID ?:number
        // 武将数量即图鉴等级
                heroCount ?:number
    }
// 英雄数据
    interface HeroInfoUnit  {
        // 武将数据表Key
                heroDbID ?:number
        // 1级数据，heroID相同的所有同类武将db id集合；该参数目前仅支持指令GetPlayerHeroListRsp，其他指令不能使用
                dbIdListOnSameHero ?:Array<number>
        // 武将ID, 静态ID
                heroID ?:number
        // 等级
                level ?:number
        // 当前经验值
                exp ?:number
        // 官职等级
                levelGuanZhi ?:number
        // 当前官职等级经验值
                expGuanZhi ?:number
        // 单宠战斗力
                fight ?:number
        // 宠物属性列表
                propList ?:Array<HeroPropertyInfoUnit>
        // 是否可以被吃掉或兑换
                isCanDelete ?:boolean
        // 当前武将的品阶
                pinJie ?:number
        // 命星阶段
                mxjieduan ?:number
        // 命星等级
                mxlevel ?:number
        // 超进化阶段
                chaoJinHuaJie ?:number
        // 超进化星级
                chaoJinHuaXing ?:number
        // 小培养阶段
                xiaoPeiYangJieDuan ?:number
        // false 不会超进化       true会超进化
                shiFouChaojinhua ?:boolean
        // false 不会闪光       true会闪光
                shiFouShanguang ?:boolean
        // 超进化消耗列表
                chaoJinHuaCostList ?:Array<JiangliInfoUnit>
        // 洗练属性
                xiLianPropList ?:Array<HeroXiLianPropInfoUnit>
        // 种族值突破等级
                zhongZuZhiTupoLv ?:number
        // 种族值消耗列表
                zhongZuZhiCostList ?:Array<JiangliInfoUnit>
        // 精灵宠物等级进阶表ID
                wujiangleveljinjieDefId ?:number
        // 是否处于遗迹探险派出状态
                isYiji ?:boolean
    }
// 英雄属性数据
    interface HeroPropertyInfoUnit  {
        // 属性枚举项
                type ?:number
        // 属性值
                value ?:number
    }
// 发给gm系统的英雄数据
    interface HeroGMInfoUnit  {
        // 
                defId ?:number
        // 培养数据 大培养+,+小培养
                guanZhi ?:string
        // 超进化数据 阶段+,+星级
                chaoJInHuaLevel ?:string
        // 
                gongHuiGeRenKeJiDefId ?:number
        // 玩家获得装备列表
                zhuangbeiList ?:Array<ZhuangbeiInfoUnit>
        // 神器
                shenQiInfo ?:msgObj.ShenQiInfoUnit
        // 属性条值
                proInfos ?:Array<ZhongZuZhiProInfoUnit>
    }
// 精灵洗练属性
    interface HeroXiLianPropInfoUnit  {
        // 属性编号
                no ?:number
        // 品质
                pinZhi ?:number
        // 属性
                shuXingInfo ?:msgObj.ShuXingInfoUnit
    }
// 扭蛋图鉴宠物信息
    interface ShilianXianshiPetInfoUnit  {
        // 宠物Id
                defId ?:number
        // 是否已经点亮
                isActive ?:boolean
    }
// 扭蛋图鉴世代信息
    interface ShilianXianshiShidaiInfoUnit  {
        // 宠物世代
                shidai ?:string
        // 宠物列表
                petInfoList ?:Array<ShilianXianshiPetInfoUnit>
    }
// DbId List
    interface DbIdListInfoUnit  {
        // DBId列表
                dbIdList ?:Array<number>
    }
// 疯狂十连抽位置信息
    interface CrazyGachaTenPullsPositionInfoUnit  {
        // 位置1-3
                pos ?:number
        // 是否已经抽卡
                isGacha ?:boolean
        // 最好的那张五星卡
                bestReward ?:msgObj.JiangliInfoUnit
        // 十连抽奖励结果
                rewardList ?:Array<JiangliInfoUnit>
    }
// 疯狂十连抽面板信息
    interface CrazyGachaTenPullsPanelInfoUnit  {
        // 疯狂十连抽表Id
                defId ?:number
        // 新的疯狂十连抽，领取奖励的关卡领主No
                lingzhu ?:number
        // 疯狂十连抽面板状态：0.未开启；1.开启进行中可抽卡，须全部位置抽卡完成才能进入状态2，否则一直是状态1；2.抽卡完成未满足领取奖励条件；3.满足领取条件可以领走任意一个奖励
                panelStatus ?:number
        // 描述信息
                desc ?:string
        // 描述信息1
                desc1 ?:string
        // 新的疯狂十连抽，位置信息
                posList ?:Array<CrazyGachaTenPullsPositionInfoUnit>
    }
// 心愿神魔获得状态
    interface WishHeroUnit  {
        // 神魔Id，Wujiang表Id
                heroDefId ?:number
        // 是否已经获得
                isGot ?:boolean
    }
// 可选心愿神魔信息
    interface OptionalWishHeroInfoUnit  {
        // 神魔Id，Wujiang表Id
                heroDefId ?:number
        // 神魔Id，Wujiang表Id
                id ?:number
    }
// 抢红包记录
    interface HongBaoRecordInfoUnit  {
        // 玩家Id
                playerId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家等级
                playerLevel ?:number
        // 抢了多少钱
                getMoney ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 红包数据
    interface HongBaoInfoUnit  {
        // 红包的dbId
                hongbaoDbId ?:number
        // 玩家发的红包的道具defId
                itemId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家头像
                playerTouxiang ?:number
        // 剩下多少钱
                leftMoney ?:number
        // 红包发出的时间--从发出到现在的秒值
                hongBaoCreateTime ?:number
        // 0 未抢（未抢完）1.抢过（未抢完） 2.已经被抢空 
                status ?:number
        // 还剩多少个没抢
                leafCount ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 回收商店商品数据
    interface HuiShouShopGoodsInfoUnit  {
        // 商品Email数据表dbID，数据表主Key
                goodsDBID ?:number
        // 策划表的defId
                defId ?:number
        // 还能买多少次 0   表示无限制 想买就买    >0 还能卖多少次
                leftBuyCount ?:number
    }
// 通关大奖
    interface InstanceCompleteRewardUnit  {
        // 领奖ID
                id ?:number
        // 对应副本表的Define ID
                defId ?:number
        // 星数
                starNum ?:number
        // 状态 0:不可领取 1:完成未领取 2:完成已领取
                status ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
    }
// 奖励数据
    interface JiangliInfoUnit  {
        // ID，静态表主Key
                id ?:number
        // 星级
                star ?:number
        // 1: 经验，2：武将经验 3：银两 4：装备 5 武将 6 道具 7 阵法 8 宝石 9 战阵碎片 10 世界BOSS积分   11 匹配战积分
                type ?:number
        // 数量
                count ?:number
    }
// 嘉年华奖励信息
    interface JiaNianHuaRewardInfoUnit  {
        // 图标
                ico ?:number
        // 兑换比例
                rate ?:number
        // 积分数量
                count ?:number
        // 0,登陆,1充值,2消耗
                getType ?:number
        // 是否可领
                canGet ?:boolean
    }
// 嘉年华商店元素
    interface JiaNianHuaShopInfoUnit  {
        // 定义id，购买的时候用这个
                defId ?:number
        // 道具id
                itemId ?:number
        // 道具类型
                itemType ?:number
        // 描述
                desc ?:string
        // 数量
                count ?:number
        // 消耗积分
                price ?:number
    }
// 节日基金奖励列表信息
    interface JieRiJiJinRewardInfoUnit  {
        // 第几天登陆
                day ?:number
        // 显示内容
                desc ?:string
        // 是否达成
                canGet ?:boolean
    }
// 基金info
    interface JiJinInfoUnit  {
        // 策划表defId
                id ?:number
        // 充值额度
                chongZhi ?:number
        // 购买后达到多少等级可领取
                level ?:number
        // 类型
                typeAward1 ?:number
        // 奖励ID
                item1 ?:number
        // 数量
                count1 ?:number
        // 类型
                typeAward2 ?:number
        // 奖励ID
                item2 ?:number
        // 数量
                count2 ?:number
        // 类型
                typeAward3 ?:number
        // 奖励ID
                item3 ?:number
        // 数量
                count3 ?:number
        // 类型
                typeAward4 ?:number
        // 奖励ID
                item4 ?:number
        // 数量
                count4 ?:number
        // 
                status ?:number
    }
// 精灵球数据
    interface JingLingQiuInfoUnit  {
        // 精灵球的id,具有唯一性
                id ?:number
        // 精灵球里那个精灵的defId
                jingLingDefId ?:number
        // 精灵球的type
                jingLingQiuType ?:number
        // 到了这个时间才能领,秒
                time ?:number
        // 领取的等级限制
                levelLimit ?:number
    }
// 跟随宠寄养日志
    interface JiYangRecordUnit  {
        // 1别人来 2 我离开 3 我被踢 4 生蛋 5 服务器维护造成的寄养中断
                type ?:number
        // 对方玩家名字
                otherPlayerName ?:string
        // 对方服务器号
                serverNumber ?:number
        // 事件发生时间
                time ?:number
    }
// 凯旋丰碑第一人信息
    interface KaixuanFengbeiFirstPlayerUnit  {
        // 玩家Id
                id ?:number
        // 玩家名字
                name ?:string
        // 玩家头像ID
                face ?:number
        // 玩家等级
                level ?:number
        // 参数1，没有不要传
                process1 ?:number
        // 参数2，没有不要传
                process2 ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 凯旋丰碑榜单汇总信息
    interface KaixuanFengbeiRankInfoUnit  {
        // 榜单类型
                type ?:number
        // 玩家信息，如果有的话，没有则无需赋值
                firstPlayer ?:msgObj.KaixuanFengbeiFirstPlayerUnit
        // 榜单名字
                name ?:string
    }
// 凯旋丰碑达到条件的人信息
    interface KaixuanFengbeiPlayerUnit  {
        // 玩家Id
                id ?:number
        // 玩家名字
                name ?:string
        // 玩家头像ID
                face ?:number
        // 玩家等级
                level ?:number
        // 到达时间，单位毫秒
                achieveTime ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 凯旋丰碑进度奖励信息
    interface KaixuanFengbeiRankRewardUnit  {
        // FirstOneDef.id
                defId ?:number
        // 描述对应备注表id
                beizhuId ?:number
        // 玩家信息
                player ?:msgObj.KaixuanFengbeiPlayerUnit
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 领取状态  0:未完成 1:完成未领取 2:完成已领取
                status ?:number
        // 完成任务所需值，对应FirstOne.tiaojian1
                needValue ?:number
    }
// 道馆位置信息
    interface PositionInfoUnit  {
        // 位置Id
                positionId ?:number
        // 玩家id
                positionPlayerId ?:number
        // 显示服务器序号（短）
                serverNumShow ?:number
        // 公会名称
                guildName ?:string
        // 公会id
                positionGuildId ?:number
        // 名称
                playerName ?:string
        // 产出速度（积分/分钟）
                output ?:number
        // 占领时间点 秒
                holdTime ?:number
        // 保护时间截止点 秒
                protectEndTime ?:number
        // 0：不显示队伍标签（自己）1：显示己方成员 2：显示敌方成员
                team ?:number
        // 皮肤图的名字 sadf.png
                piFuTu ?:string
        // true 是npc false 不是   如果是npc用piFuTu代表小人形象  false代表真人 用 piFuDefIdList当做形象 参考以前的代码
                isNpc ?:boolean
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 玩家头像ID
                playerTouxiangId ?:number
        // 玩家坐骑ID
                playerZuoqi ?:number
        // 玩家翅膀ID
                playerChibang ?:number
        // 公会职位
                gonghuiZhiwei ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 道馆历史记录信息
    interface DaoGuanHistoryInfoUnit  {
        // 记录的DbId
                recordDbId ?:number
        // 场景Id
                sceneId ?:number
        // 敌人的dbid
                energyPlayerDBId ?:number
        // 战斗力
                playerFightPower ?:number
        // 等级
                energyLevel ?:number
        // 名字
                energyName ?:string
        // 所在服务器 用于展示
                serverInfo ?:number
        // 服务器ID 长id 用于跨服名片消息
                serverChangId ?:number
        // 防守结果  0防守失败   1防守成功 
                fightResult ?:number
        // 头像
                playerTouxiangId ?:number
        // 是不是挑战者
                isTiaozhan ?:boolean
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 单个道馆信息
    interface SingleDaoGuanInfoUnit  {
        // 道馆Id
                sceneId ?:number
        // 道馆馆主
                guanzhuName ?:string
        // 道馆位置信息
                daoGuanPositionList ?:Array<PositionInfoUnit>
    }
// 积分信息
    interface DaoGuanScoreInfoUnit  {
        // dbId
                dbId ?:number
        // score
                score ?:number
    }
// 道馆详细信息
    interface DaoGuanDetailInfoUnit  {
        // 游戏状态
                activityState ?:number
        // 开始时间
                startTime ?:number
        // 结束时间
                endTime ?:number
        // 道馆列表
                daoGuanList ?:Array<SingleDaoGuanInfoUnit>
        // 公会排名列表
                gongHuiInfoList ?:Array<GongHuiListInfoUnit>
        // 个人积分信息列表
                playerScoreList ?:Array<DaoGuanScoreInfoUnit>
        // 公会积分信息列表
                guildScoreList ?:Array<DaoGuanScoreInfoUnit>
    }
// 跨服锦标赛积分箱子信息
    interface KuaFuJBSScoreBoxInfoUnit  {
        // 箱子id
                defId ?:number
        // 拥有的数量
                haveCount ?:number
        // 是否可以购买
                canBuy ?:boolean
        // 可以单次购买的最大个数
                buyMaxCount ?:number
        // 物品价格信息
                priceInfo ?:msgObj.JiangliInfoUnit
    }
// 跨服锦标赛积分箱子信息
    interface KuaFuJBSFightInfoUnit  {
        // 拥有的挑战次数
                haveFightTimes ?:number
        // 最大的挑战次数
                maxFightTimes ?:number
        // 单次可购买的最大挑战次数
                maxBuyFightTimes ?:number
        // 物品价格信息
                priceInfo ?:msgObj.JiangliInfoUnit
        // 剩余多久(s)回复一次挑战次数
                leftTime ?:number
        // 多久(s)回复一次挑战次数
                maxTime ?:number
    }
// 跨服砸金蛋排行数据
    interface KuaFuZaJinDanRankingInfoUnit  {
        // 排名
                rank ?:number
        // playerId
                playerId ?:number
        // 名字
                name ?:string
        // 服务器名字
                serverName ?:string
        // 头像
                ico ?:number
        // 积分
                score ?:number
        // 奖励列表
                rewards ?:Array<JiangliInfoUnit>
    }
// 战队简单信息(用于显示晋级赛 标签信息)
    interface ZhanDuiSimpleInfoUnit  {
        // 战队ID
                zhanDuiId ?:number
        // 头像id
                zhanDuiHeadId ?:number
        // 队长id
                leaderId ?:number
        // 战队名字
                zhanDuiName ?:string
        // 服务器编号
                serverNum ?:number
        // 战队战力
                zhanDuiFight ?:number
        // 战队成员
                zhanDuiMembers ?:Array<ZhanDuiMemberInfoUnit>
        // 玩家战斗胜负
                playerFightLogInfos ?:Array<ZhanDuiPlayerFightLogInfoUnit>
        // 声望
                shengwang ?:number
        // 支持人数
                zhichiCount ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 战队PK组
    interface ZhanDuiPKGroupUnit  {
        // 对战组id
                pkGroupId ?:number
        // 左边战队
                leftZhanDuiInfo ?:msgObj.ZhanDuiSimpleInfoUnit
        // 右边战队
                rightZhanDuiInfo ?:msgObj.ZhanDuiSimpleInfoUnit
        // 1:左右胜 2:右边胜
                win ?:number
    }
// 战队战绩信息
    interface ZhanDuiPlayerFightLogInfoUnit  {
        // 玩家战斗日志
                fightLogId ?:number
        // 是否胜利
                isWin ?:boolean
        // 左方角色Id
                playerId ?:number
        // 右方角色Id
                enemyId ?:number
        // 左方战队位置（队伍1,2,3)
                position ?:number
        // 右方战队位置（队伍1,2,3)
                enemyPosition ?:number
    }
// 历史战绩日志
    interface HistroyLogUnit  {
        // 左边战队
                leftZhanDuiInfo ?:msgObj.ZhanDuiSimpleInfoUnit
        // 右边战队
                rightZhanDuiInfo ?:msgObj.ZhanDuiSimpleInfoUnit
        // 玩家战斗胜负
                playerFightLogInfos ?:Array<ZhanDuiPlayerFightLogInfoUnit>
        // 胜利与否
                result ?:boolean
        // 日志时间
                logTime ?:number
    }
// 
    interface ZhanDuiPKGroupZhiChiUnit  {
        // 对战组id
                pkGroupId ?:number
        // 左边支持的声望数量
                leftZhiChiShengWang ?:number
        // 右边支持的声望数量
                rightZhiChiShengWang ?:number
    }
// 蓝钻礼包数据
    interface LanZuanLiBaoInfoUnit  {
        // 等级
                lanZuanLevel ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 0不能领   1能领  2领过了
                liBaoStatus ?:number
    }
// 黄金投资数据
    interface HuangJinTouZiInfoUnit  {
        // 策划表Id
                defId ?:number
        // 消耗类型
                xiaoHaoType ?:number
        // 消耗数量
                xiaoHaoCount ?:number
        // 需要连续登陆天数
                loginDay ?:number
        // 是否领取过了
                isHave ?:boolean
        // 奖励列表
                JiangLiList ?:Array<JiangliInfoUnit>
    }
// 老玩家回归礼包奖励信息
    interface LaoWanJiaHuiGuiInfoUnit  {
        // ID，静态表主Key
                defId ?:number
        // 1 可领取 2不可领取
                status ?:number
        // ID，静态表主Key
                jiangliList ?:Array<JiangliInfoUnit>
        // 礼包说明
                shuoming ?:string
    }
// 已经解锁的小镇信息
    interface YiJieSuoTownInfoUnit  {
        // 人物id
                renWuId ?:number
        // 好感度等级
                haoGanDuLevel ?:number
        // true满了     false没有
                isTop ?:boolean
        // 人物名称
                name ?:string
        // 这个好感度下的对话列表 前端从这里面随机一个
                riChengDuiHuaList ?:Array<string>
        // 人物的头像
                renWuTouXiang ?:string
        // 房屋的图
                fangWuTu ?:string
        // 技能的等级
                jiNengLevel ?:number
        // 红点 true有   false没有
                isTips ?:boolean
    }
// 恋爱系统送礼物信息
    interface LianAiLiWuInfoUnit  {
        // 道具id
                itemId ?:number
        // 是否有限制  true 限制了   false 没有
                limited ?:boolean
        // 还剩多少次
                leftCount ?:number
        // 一个道具加多少经验
                addExpPerItem ?:number
    }
// 恋爱系统面板中  技能的描述
    interface LianAiJiNengDescInfoUnit  {
        // 技能名字
                jiNengName ?:string
        // 技能描述
                jiNengDesc ?:string
        // 技能表的defid
                jiNengDefId ?:number
        // 职业 1 群攻 2 刺客 3 防御 4 治疗 5 控制
                jiNengPhoto ?:number
        // 是不是满了
                isTop ?:boolean
        // 是不是已经激活    true已经激活  false 没有
                isJiHuo ?:boolean
        // 有没有红点    true有  false 没有
                isTips ?:boolean
        // 1.功能开启 2.功能关闭 0.无对应的功能
                isFunctionOpen ?:number
        // 类型 1，排位赛自动参加  参数  每日次数 2，排位赛可选择对手  参数 每日次数 3，公会boss自动参加   参数 无 4，公会boss参与人数增加  参数  参与人数
                skillType ?:number
    }
// 连续充值活动信息
    interface LianXuChongZhiHuoDongInfoUnit  {
        // defID
                defId ?:number
        // 模版类型
                mobanType ?:number
        // 名称
                shuoMing ?:string
        // 需持续天数 单位：天
                tiaoJian ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 是否领取过
                isHave ?:boolean
        // 充值金额限制
                chongZhiTiaoJian ?:number
        // 今天充值数
                totayChongZhi ?:number
    }
// 奖励数据
    interface LianxuJiangliInfoUnit  {
        // ID，静态表主Key
                lianxujiangliId ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
    }
// 零元礼包
    interface LingYuanLiBaoInfoUnit  {
        // 
                defId ?:number
        // 到了什么时间点可以领取 (秒)
                sendBackTime ?:number
        // 价格类型 钻石 还是金券还是其他 参考奖励类型
                moneyType ?:number
        // 售价
                moneyCount ?:number
        // 返还类型 钻石 还是金券还是其他 参考奖励类型
                fanHuanMoneyType ?:number
        // 返还的数量
                fanHuanMoneyCount ?:number
        // 当前状态 1：未购买；2：买了没到返还时间；3：到返还时间能领取返还；4：已返还
                status ?:number
        // 描述
                desc ?:string
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 买以后多少时间发还 展示用 秒
                fanHuanTime ?:number
        // 充值金额id（对应chongzhijine表中的id）
                rechargeId ?:number
        // 战力
                zhanli ?:number
        // 显示名称
                showName ?:string
        // 角标
                jiaobiao ?:string
        // 已激活的皮肤id
                pifuId ?:number
    }
// 统计服务器log收集
    interface LogInfoUnit  {
        // 数据类名称
                className ?:string
        // 数据内容
                jsonContent ?:string
    }
// 洛托姆进阶资源
    interface LuoTuoMuDisplayInfoUnit  {
        // 开始等级
                beginLevel ?:number
        // 显示类型
                showType ?:number
        // 名称
                dispalyName ?:string
        // 
                pinzhi ?:number
        // 头像
                ico ?:string
        // 形象
                xingxiang ?:string
    }
// 洛托姆
    interface LuoTuoMuInfoUnit  {
        // 类型
                pointType ?:number
        // 等级
                pointLevel ?:number
    }
// 新神技背包物品信息
    interface MagicalSkillItemUnit  {
        // 分类：19、来自背包，未穿戴状态，可叠加；1005、来自神技卡，已穿戴
                rewardType ?:number
        // 对应的DbId。注意：背包dbId可能会跟神技卡dbId重复
                dbId ?:number
        // SkillStatic表Id
                defId ?:number
        // 神技等级
                level ?:number
        // 神魔职业 0通用 1群攻 2刺客 3肉盾 4奶妈 5控制
                petClassify ?:number
        // 神技名字
                skillName ?:string
        // 神技最终描述。对应：SkillStatic表的SkillTotalDesc字段
                skillTotalDesc ?:string
        // 已经装备到那个英雄身上，仅限1005神技卡有效
                heroId ?:number
        // 神技装备位置1-N，从1开始。仅限1005神技卡有效
                wearIndex ?:number
        // 升级消耗
                xiaohaoItemList ?:Array<JiangliInfoUnit>
        // 神技的边框以及名称颜色2 绿色；3蓝色；4紫色；5金色；6橙色；7红色
                rank ?:number
        // 技能icon
                skillIcon ?:string
        // 拥有数量
                count ?:number
        // 是否已经满级了
                isFullLevel ?:boolean
        // 当前升级进度，小于当前升级消耗.count
                exp ?:number
        // 描述，对应：SkillChange表Jianjie字段的描述
                jianjie ?:string
        // 描述，对应：SkillChange表Desc字段
                desc ?:string
        // 元素属性 0 无属性 1 火 Fire 2 水 Water 3 草 Grass 4 光 Light 5 暗 Dark
                shuxingType ?:number
    }
// 新神技卡信息
    interface MagicalSkillCardUnit  {
        // 对应的DbId
                dbId ?:number
        // 神技等级
                level ?:number
        // 类型 1 升级类型 2 消耗类型。对应策划表字段名：type
                type ?:number
        // 神魔职业 0通用 1群攻 2刺客 3肉盾 4奶妈 5控制
                petClassify ?:number
        // 神技名字
                skillName ?:string
        // 本等级描述
                desc ?:string
        // 升级消耗
                xiaohaoItemList ?:Array<JiangliInfoUnit>
        // 神技的边框以及名称颜色2 绿色；3蓝色；4紫色；5金色；6橙色；7红色
                rank ?:number
        // 技能icon
                icon ?:string
        // 是否已经满级了
                isFullLevel ?:boolean
        // 当前等级神技属性
                currPropertyList ?:Array<PropertyInfoUnit>
        // 下一等级神技属性
                nextPropertyList ?:Array<PropertyInfoUnit>
        // 元素属性 0 无属性 1 火 Fire 2 水 Water 3 草 Grass 4 光 Light 5 暗 Dark
                shuxingType ?:number
    }
// 新神技卡协作数据
    interface MagicalSkillCooperateUnit  {
        // 协作分类：1神技；2神魔
                type ?:number
        // 元素属性 0 无属性 1 火 Fire 2 水 Water 3 草 Grass 4 光 Light 5 暗 Dark
                shuxingType ?:number
        // 重复的个数
                repeatCount ?:number
        // 提审效果千分比。显示效果为(value/1000)然后转百分比显示
                value ?:number
    }
// 新神技卡策划数据
    interface MagicalSkillDefUnit  {
        // 神技Id
                defId ?:number
        // 神技名字
                skillName ?:string
        // 神技等级
                level ?:number
        // 技能icon
                icon ?:string
        // 神技的边框以及名称颜色2 绿色；3蓝色；4紫色；5金色；6橙色；7红色
                rank ?:number
        // 神魔职业 0通用 1群攻 2刺客 3肉盾 4奶妈 5控制
                petClassify ?:number
        // 元素属性 0 无属性 1 火 Fire 2 水 Water 3 草 Grass 4 光 Light 5 暗 Dark
                shuxingType ?:number
        // 神技属性
                propertyList ?:Array<PropertyInfoUnit>
        // 神技效果描述
                desc ?:string
    }
// 玩家支付信息
    interface PlayerPayInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 角色名称
                name ?:string
        // vip等级
                vipLevel ?:number
        // 总的人民币（单位：元）
                totalRmb ?:number
    }
// 礼品码奖励信息
    interface GiftCodeRewardInfoUnit  {
        // 奖励类型
                type ?:number
        // 奖励ID
                item ?:number
        // 奖励数量
                count ?:number
    }
// 礼品码奖励信息
    interface GiftCodeUsedInfoUnit  {
        // 礼品码序号，前缀，关键字
                id ?:string
        // 礼品码
                code ?:string
        // 使用时间
                time ?:number
        // 使用者ID
                playerId ?:number
    }
// 消息线程池信息
    interface MessagePoolInfoUnit  {
        // Index
                index ?:number
        // 任务总数
                taskCount ?:number
        // 已完成的任务数量
                completedTaskCount ?:number
    }
// 消息处理效率信息
    interface MessageProcessInfoUnit  {
        // Message Service
                serviceName ?:string
        // Method Name
                methodName ?:string
        // 最小值
                min ?:number
        // 最大值
                max ?:number
        // 平均处理时长
                time ?:number
        // 处理次数
                count ?:number
    }
// 排位赛级位信息
    interface GMRankingLevelGroupUnit  {
        // 级位ID
                id ?:number
        // 战区数量
                warZoneCount ?:number
    }
// 段位信息
    interface GMRankingGroupUnit  {
        // 段位ID
                id ?:number
        // 段位昵称
                name ?:string
        // 级位数据
                levelGroupList ?:Array<GMRankingLevelGroupUnit>
    }
// 排位赛玩家信息
    interface GMRankingPlayerUnit  {
        // 排位赛ID
                id ?:number
        // 玩家ID
                playerId ?:number
        // 注册服务器编号
                sourceServerNo ?:number
        // 实际服务器ID
                serverId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家头像ID
                playerTouxiangId ?:number
        // 玩家坐骑ID
                playerZuoqi ?:number
        // 玩家翅膀ID
                playerChibang ?:number
        // 玩家等级
                playerLevel ?:number
        // 玩家VIP等级
                playerVipLevel ?:number
        // 玩家战斗力
                playerFight ?:number
        // 竞技场排名
                paihangBW ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position11 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position12 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position13 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position14 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position15 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position16 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position17 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position18 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position19 ?:number
        // 积分
                score ?:number
        // 隐藏积分
                scoreHide ?:number
        // 战区点数
                point ?:number
        // 段位
                type ?:number
        // 级位
                level ?:number
        // 战区
                warZoneId ?:number
        // 连胜或者连败次数,正数表示连胜,负数表示连败
                successiveFight ?:number
        // 战斗局数
                sizeFight ?:number
        // 胜利战斗局数
                sizeWin ?:number
        // 晋升状态: 0未晋升; 1晋升中; 2晋升成功 3晋升失败
                promoteState ?:number
        // 晋升战斗局数
                sizeFightPromote ?:number
        // 晋升战斗结果
                promoteFightResult ?:number
        // 上次匹配目标ID
                lastTargetId ?:number
        // 等级段
                levelPart ?:number
    }
// 排位赛玩家精灵信息
    interface GMRankingHeroUnit  {
        // 精灵ID
                heroID ?:number
        // 武将ID
                heroDefID ?:number
        // 等级
                level ?:number
        // 官职等级
                levelGuanZhi ?:number
        // 生命值
                hp ?:number
        // 攻击值
                attack ?:number
        // 防御值
                defense ?:number
        // 速度
                speed ?:number
        // 命中
                aim ?:number
        // 闪避
                dodge ?:number
        // 暴击
                critical ?:number
        // 免爆
                unCritical ?:number
        // 伤害加深
                amplifyDamage ?:number
        // 伤害减免
                weaken ?:number
        // 吸血
                stealHP ?:number
        // 战斗力
                fight ?:number
        // 火克制
                fireRestraint ?:number
        // 水克制 
                waterRestraint ?:number
        // 草克制 
                grassRestraint ?:number
        // 光克制
                lightRestraint ?:number
        // 暗克制
                darkRestraint ?:number
        // 火抗性
                fireUnRestraint ?:number
        // 水抗性
                waterUnRestraint ?:number
        // 草抗性
                grassUnRestraint ?:number
        // 光抗性
                lightUnRestraint ?:number
        // 暗抗性
                darkUnRestraint ?:number
        // NewBuff附加额外Buff
                newBuffIdList ?:Array<number>
        // 技能伤害
                skillDamagel ?:number
        // 概率增加弹射次数
                attackLaunchProbability ?:number
        // 暴击伤害
                criticalDamagel ?:number
        // 眩晕几率
                vertigoProbability ?:number
        // 反伤
                antiInjury ?:number
        // 混乱技能
                confusionValue ?:number
        // 破攻
                cutAttack ?:number
        // 破防
                cutDefense ?:number
        // 破血
                cutHp ?:number
        // 附伤
                extraDamage ?:number
        // 可免伤次数,挨打一次未死亡的才触发
                noDamageUnderAttack ?:number
        // 抗眩晕
                noVertigo ?:number
        // 抗混乱
                noConfusion ?:number
        // 抗混乱
                fightBack ?:number
        // 穿刺时眩晕几率
                vertigoPolearm ?:number
        // 净化
                cleansing ?:number
        // 诅咒
                curse ?:number
        // 先手
                initiation ?:number
        // 除上面属性外的所有属性
                otherHeroProp ?:Array<HeroPropertyInfoUnit>
    }
// 跨服活动分组信息
    interface CrossBigRuleGroupUnit  {
        // 分组ID
                groupId ?:number
        // 本组所有区服编号列表
                serverNumList ?:Array<number>
    }
// 每日团购
    interface MeiRiTuanGouInfoUnit  {
        // 策划表id
                defId ?:number
        // 礼包类型
                libaotype ?:number
        // 礼包名字
                libaoName ?:string
        // 礼包价格
                price ?:number
        // 所需人数
                needRenShu ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 是否已经购买
                isGot ?:boolean
        // 当前已经购买的人数
                gotRenShu ?:number
    }
// 迷宫扫荡结果数据
    interface SweepMiGongOutcomeUnit  {
        // 
                cengShu ?:number
        // 1,2,3,4,6,7
                defIds ?:string
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 名片信息
    interface MingPianInfoUnit  {
        // 
                str ?:string
        // 页签
                label ?:number
        // 排序
                queue ?:number
        // 描述
                miaoShu ?:number
        // 
                value1 ?:number
        // 
                value2 ?:number
        // 状态 1完美   以后待定
                status ?:number
        // 
                defId ?:number
    }
// 玩家任务数据
    interface MissionInfoUnit  {
        // 任务ID
                id ?:number
        // 任务定义ID
                renwuId ?:number
        // 任务状态:-1格子未开启，0未开始，1进行中，2已完成
                status ?:number
        // 任务刷新时间
                refreshTime ?:number
        // 任务完成度 改成long针对战力
                count ?:number
        // 领取状态:1未领取；2已领取
                isGet ?:number
    }
// 邀请任务
    interface YaoQingMissionInfoUnit  {
        // 玩家名
                playreName ?:string
        // 服务器号      给什么显示什么
                serverId ?:number
        // 头像
                touXiang ?:number
        // 任务ID
                id ?:number
        // 任务定义ID
                renwuId ?:number
        // 任务状态:-1格子未开启，0未开始，1进行中，2已完成   3 已经领取了
                status ?:number
    }
// 极限挑战任务
    interface JiXianTiaoZhanRenWuUnit  {
        // 任务ID
                id ?:number
        // 图标
                tuBiao ?:string
        // 是否完成
                isFinish ?:boolean
        // 是否已经领奖
                isGetReward ?:boolean
        // 任务名称
                name ?:string
        // 任务说明
                shuoMing ?:string
        // 任务说明
                shuYuType ?:number
        // 刷新类型
                timeType ?:number
        // 显示优先级
                youXian ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 玩家等级
                level ?:number
        // 前置任务Id
                qianZhi ?:number
        // 公告 0.不公告  1.公告
                gongGao ?:number
        // 跳转页面
                jump ?:string
        // 引导索引
                zhiXiang ?:number
        // 是否强制引导0不是  1是
                isQiangZhi ?:number
        // 是否小手提示
                isTiShi ?:number
        // 是否提示返回
                fanHui ?:number
        // 前置弹窗
                qianZhiTan ?:number
        // 后置弹窗
                houZhiTan ?:number
        // 章节表Id
                chapterDefId ?:number
        // 章节名字
                chapterName ?:string
        // 章节层数
                ceng ?:number
        // 章节描述
                desc ?:string
        // 小关表Id
                xiaoguanDefId ?:number
        // 小关表boss名
                guaiwuName ?:string
        // 小关表boss怪物骨骼数据
                guaiwuGuge ?:msgObj.GugeUnit
    }
// 日常任务数据
    interface DailyMissionInfoUnit  {
        // 任务ID
                id ?:number
        // 任务定义ID
                renwuId ?:number
        // 任务状态:1 待完成状态 2 可领取状态 3 已完成状态 4 待购买状态 
                status ?:number
        // 任务刷新时间
                refreshTime ?:number
        // 进度
                count ?:number
        // 活跃值
                huoYueCount ?:number
        // 是否有扫荡的权限
                isCanSweep ?:boolean
    }
// 
    interface MoFaShaiZiInfoUnit  {
        // 记位置
                defId ?:number
        // 该位置的奖励
                jiangliInfo ?:msgObj.JiangliInfoUnit
    }
// 磨炼排行数据
    interface MoLianPaihangInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 服务器编号
                serverNum ?:number
        // 玩家昵称
                playerName ?:string
        // 标题
                biaoti ?:string
        // 玩家头像ID
                playerTouxiangId ?:number
        // 玩家等级
                playerLevel ?:number
        // 玩家VIP等级
                playerVipLevel ?:number
        // 玩家战斗力
                playerFight ?:number
        // 排名
                position ?:number
        // 磨炼关卡id
                defId ?:number
        // 段位，匹配战用
                part ?:number
        // 
                paihangPPDefId ?:number
        // 玩家坐骑ID
                playerZuoqi ?:number
        // 玩家翅膀ID
                playerChibang ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 玩家自己的称号
                chenghaoId ?:number
        // 皮神进阶外显皮肤
                displayLevel ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 宝箱的领取条件
    interface BaoXiangTiaoJianInfoUnit  {
        // 任务描述
                renWuMiaoShu ?:string
        // 是否已经完成  true 是    false 不是
                isFinish ?:boolean
    }
// 玩家新活动数据
    interface ActivityProgressUnit  {
        // 活动列表索引
                id ?:number
        // 活动进度
                progress ?:number
        // 活动状态 0:未完成 1:完成未领取 2:完成已领取 3:活动过期
                state ?:number
    }
// 跨服锦标赛公会排行的数据
    interface JinBiaoSaiGongHuiInfoUnit  {
        // 数据索引
                rank ?:number
        // 公会名称
                name ?:string
        // 所在服务器
                serverId ?:number
        // 积分
                score ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 公会徽章
                gongHuiHuiZhang ?:string
    }
// 跨服锦标赛个人排行的数据
    interface JinBiaoSaiGeRenInfoUnit  {
        // 数据索引
                rank ?:number
        // 玩家ID
                playerId ?:number
        // 玩家Name
                playerName ?:string
        // 玩家头像
                touxiang ?:number
        // VIP等级
                vipLevel ?:number
        // 所在服务器
                serverId ?:number
        // 积分
                score ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 玩家新活动数据
    interface NewActivityInfoUnit  {
        // 活动ID
                id ?:number
        // 活动名称
                defId ?:number
        // 活动名称
                name ?:string
        // 活动类型
                type ?:number
        // 活动状态:0活动未开启；1活动已开启；2 活动待结束；3活动已结束；4活动完全结束
                status ?:number
        // 是否出现前端提示
                isTips ?:boolean
        // 活动开始时间，单位：毫秒
                startTime ?:number
        // 活动结束时间，单位：毫秒
                endTime ?:number
        // 活动入口 0 无 1 活动 2 节日活动
                activityEnter ?:number
        // 图标
                icon ?:string
        // 活动说明
                shuoming ?:string
        // 活动排序
                paixu ?:number
        // Banner大图
                banner ?:string
        // 模板
                moban ?:string
        // 是否显示到期时间 0 显示时间 1 不显示时间 2 显示永久  3每日重置
                shijianxianshi ?:number
        // 跳转类型  参见策划表 NewActivity
                jumpType ?:number
        // 模板类型
                mobanType ?:number
        // Banner小图
                bannerSmall ?:string
        // 展示形式 同一种活动类型(type) 前端需要不同的展现形式
                displayType ?:number
        // 是否已买 现在只开服限购赋值
                hasBuy ?:boolean
        // 测试服专用标识。非测试服该值为undefined
                testServerMark ?:string
        // Project表Id
                projectId ?:number
        // 金猪存钱罐扩展信息
                piggyBank ?:msgObj.ActivityExtendInfoPiggyBankUnit
        // 基金扩展信息
                fund ?:msgObj.ActivityExtendInfoFundUnit
    }
// 活动入口扩展信息-金猪存钱罐
    interface ActivityExtendInfoPiggyBankUnit  {
        // 是否奖励已满
                isFullReward ?:boolean
    }
// 活动入口扩展信息-基金
    interface ActivityExtendInfoFundUnit  {
        // 是否领取完全部免费奖励了
                isGetAllNormalRewards ?:boolean
        // 是否奖励已满
                rewardNormalList ?:Array<JiangliInfoUnit>
        // 所需累计值对应的描述文本
                valueDesc ?:string
        // 是否有红点提示
                isTips ?:boolean
    }
// 玩家新充值排行数据
    interface NewChongZhiPaiHangInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家Name
                playerName ?:string
        // 玩家头像
                touxiang ?:number
        // VIP等级
                vipLevel ?:number
        // 当前排行
                curPaihang ?:number
        // 最低充值 充值金额 (单位RMB)
                chongzhiMin ?:number
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 玩家新限时排行数据
    interface NewValueListInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家Name
                playerName ?:string
        // 玩家头像
                touxiang ?:number
        // VIP等级
                vipLevel ?:number
        // 当前排行
                curPaihang ?:number
        // 分数
                value ?:number
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 单笔充值数据
    interface DanBiChongZhiDetailInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 单笔累计充值次数
                payCount ?:number
        // 已经领取次数
                count ?:number
        // 最大可领取次数
                maxCount ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 暂无
                subType1 ?:number
        // 0: 不显示兑换次数上限2: 显示兑换次数上限
                tableCellType ?:number
        // 说明内容
                shuoming ?:string
        // 充值金额表Id
                chongzhiJineDefId ?:number
        // 标题，内容来自策划表biaoti字段
                title ?:string
        // 礼包获得条件类型1、充值获得；2、消费金券获得
                tiaojianType ?:number
        // 金券价格
                jinquanPrice ?:number
        // 表示对应礼包需要显示的图标
                zhekoubiaoshi ?:string
    }
// 五元充值数据
    interface WuYuanChongZhiDetailInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 要求达到的天数
                dayCount ?:number
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
    }
// 活动任务数据
    interface MissionActivityInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 条件1,对应renwuhuodongDef的tiaojian1
                tiaojian1 ?:number
        // 策划要求任务数值
                tiaojian ?:number
        // 当前玩家进度数值
                value ?:number
        // 数值比较方式：1等于，2大于等于
                checkType ?:number
        // 是否已经完成
                isComplete ?:boolean
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
        // 描述
                desc ?:string
        // 前往
                jump ?:string
    }
// 活动任务数据
    interface ItemCollectActivityInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 条件列表
                itemList ?:Array<JiangliInfoUnit>
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
        // 描述
                desc ?:string
        // 0 无 1 今日限购 2 活动期间内限购
                limitType ?:number
        // 已经购买次数
                buyCount ?:number
        // 最大可购买次数
                maxCount ?:number
    }
// 一冲选领
    interface YiChongXuanLingDetailInfoUnit  {
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 描述
                miaoShu ?:string
    }
// 活动副本怪物
    interface HuoDongFuBenGuaiWuInfoUnit  {
        // 怪物顺序 从0开始
                index ?:number
        // 武将id
                heroId ?:number
        // 死亡状态 true死了  false没有
                isDead ?:boolean
        // 可能掉落奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 活动副本界面奖励展示
    interface HuoDongFuBenJiangLiPanelInfoUnit  {
        // 可能掉落奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 策划表id
                defId ?:number
        // 副本类型
                fuBenType ?:number
        // 消耗类型
                xiaoHaoType ?:number
        // 消耗id
                xiaoHaoId ?:number
        // 消耗数量
                xiaoHaoCount ?:number
        // 是否正在挑战 true 是   false  不是
                isFight ?:boolean
        // 购买进入门票类型
                jiaGeType ?:number
        // 购买进入门票对应ID
                jiaGeId ?:number
        // 购买进入门票单价
                jiaGeCount ?:number
        // 
                subType1 ?:number
        // 
                name ?:string
        // 剩余多少次
                leftCount ?:number
        // true 显示  false不显示
                isShow ?:boolean
        // 一次最多购买多少
                onceCountMax ?:number
    }
// 七日竞赛活动信息
    interface SevenMatchInfoUnit  {
        // 活动名称
                name ?:string
        // 竞赛活动类型
                matchType ?:number
        // 活动状态:0活动未开启；1活动已开启；2 活动待结束；3活动已结束；4活动完全结束
                status ?:number
        // 是否出现前端提示
                isTips ?:boolean
        // 活动结束时间，单位：毫秒
                endTime ?:number
        // 活动说明
                shuoming ?:string
        // Banner大图
                banner ?:string
        // icon
                icon ?:string
        // 0 不可领取 1 可领取  2 已经领取
                rewardStatus ?:number
        // 是否已经购买
                isBuy ?:boolean
        // 排行描述
                paihangDesc ?:string
        // 排序字段
                paixu ?:number
        // 竞赛活动ID
                matchId ?:number
        // 天数, 文字
                tianshu ?:string
        // 没有礼包, true: 不显示, false: 显示
                isWithoutLibaoMenu ?:boolean
        // 上榜条件
                shangbangTiaojian ?:string
    }
// 七日竞赛活动奖励内容
    interface ServerMatchRewardInfoUnit  {
        // 当前排行
                curPaihang ?:number
        // 特殊奖励,如果没有,则为空
                specialJiangli ?:msgObj.JiangliInfoUnit
        // 特殊奖励文字描述
                specialDesc ?:string
        // 可获得奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 称号图标
                titleIcon ?:string
        // 单位
                danwei ?:string
        // 上榜条件类型
                limitType ?:number
        // 上榜条件值
                limitValue ?:number
        // 进入排名 人民币 单位/元
                tiaojian ?:number
    }
// 七日竞赛活动奖励内容
    interface ServerMatchPlayerInfoUnit  {
        // 玩家ID,多人奖励联合显示的时候,不返回该值
                playerId ?:number
        // 玩家Name,多人奖励联合显示的时候,不返回该值
                playerName ?:string
        // 玩家头像,多人奖励联合显示的时候,不返回该值
                touxiang ?:number
        // VIP等级,多人奖励联合显示的时候,不返回该值
                vipLevel ?:number
        // 当前排行
                curPaihang ?:number
        // 排行显示值,例如超梦X战斗力或玩家总战力等
                paihangValue1 ?:number
        // 排行显示值,翅膀有阶星
                paihangValue2 ?:number
        // 限制上榜类型，对应竞赛类型
                limitType ?:number
        // 限制上榜数值
                limit ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 七日竞赛活动冲榜礼包
    interface SevenMatchGiftInfoUnit  {
        // 礼包编号
                liBaoNum ?:number
        // 礼包名
                giftName ?:string
        // 金券价格
                jinquanCount ?:number
        // vip限制
                vipLimit ?:number
        // 剩余购买次数
                gouMaiCiShuLeft ?:number
        // 
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 开服活动
    interface KaiFuHuoDongInfoUnit  {
        // 
                defId ?:number
        // 
                jiangliList ?:Array<JiangliInfoUnit>
        // 原价格
                jiaGeOld ?:number
        // 价格
                jiaGe ?:number
        // 0 时间过期  1 能领(买) 2 领(买)过了  3时间未到  4可充值  
                status ?:number
        // 第几天的奖励
                diJiTian ?:number
        // 说明
                shuoMing ?:string
    }
// 每日一购
    interface MeiRiYiGouInfoUnit  {
        // 
                defId ?:number
        // 
                jiangliList ?:Array<JiangliInfoUnit>
        // 消耗的类型  这里是奖励类型
                costType ?:number
        // 货币数量
                tiaojian ?:number
        // 天数
                tianshu ?:number
        // true 领过了  false 没有
                isGet ?:boolean
        // 描述
                miaoShu ?:string
    }
// 单笔购买数据
    interface DanBiGouMaiDetailInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 货币类型 4钻石; 21金券
                moneyType ?:number
        // 打折后购买价格
                moneyCount ?:number
        // 打折前购买价格
                moneyCountOld ?:number
        // 达到了这个vip等级才能买
                limitVip ?:number
        // 累计购买次数
                buyCount ?:number
        // 最大可购买次数
                maxCount ?:number
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
        // 暂无
                subType1 ?:number
        // 0: 不显示兑换次数上限2: 显示兑换次数上限
                shuoming ?:string
        // 0: 不显示兑换次数上限2: 显示兑换次数上限
                tableCellType ?:number
    }
// 显示升阶数据
    interface XianShiShengJieDetailInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 要求等级
                tiaojian ?:number
        // 备注表id
                miaoshu1 ?:number
        // 备注表id
                miaoshu2 ?:number
        // 
                jiangliList ?:Array<JiangliInfoUnit>
        // 结束时间
                jieshushijian ?:number
        // 当前阶段
                currJieDuan ?:number
        // 0不隐藏  1隐藏
                hide ?:number
        // 0 不可领取 1 可领取  2 已经领取 4 已过期
                rewardStatus ?:number
    }
// 合服首充数据
    interface HeFuShouChongInfoUnit  {
        // 天数
                tianshu ?:number
        // 条件
                tiaojian ?:number
        // 描述
                miaoshu ?:string
        // 兑换模式
                tableCellType ?:number
        // progress: 指的是充值额度
                activityProgress ?:msgObj.ActivityProgressUnit
        // 
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 个人飙战信息
    interface GeRenBiaoZhanInfoUnit  {
        // 条件1
                tiaojian1 ?:number
        // 条件2
                tiaojian2 ?:number
        // 描述
                des ?:string
        // progress: 提升的战力值
                activityProgress ?:msgObj.ActivityProgressUnit
        // 
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 单笔购买数据
    interface HeFuYuShouGoodInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 1:平民版道具 2:土豪版道具
                type ?:number
        // 下一个可以购买的模版id
                nextDefId ?:number
        // 货币类型 4钻石; 21金券
                moneyType ?:number
        // 购买价格
                moneyCount ?:number
        // 累计购买次数
                buyCount ?:number
        // 最大可购买次数
                maxCount ?:number
        // 
                jiangliList ?:Array<JiangliInfoUnit>
        // 暂无
                subType1 ?:number
        // 0: 不显示兑换次数上限2: 显示兑换次数上限
                shuoming ?:string
        // 0: 不显示兑换次数上限2: 显示兑换次数上限
                tableCellType ?:number
        // progress: 购买次数 state: 0: 不能购买 1: 能购买未购买 2: 已经购买 
                activityProgress ?:msgObj.ActivityProgressUnit
    }
// 等级vip礼包
    interface DengJiVipLiBaoInfoUnit  {
        // 活动定义ID
                defId ?:number
        // 0 玩家等级   1vip等级
                tiaoJianType1 ?:number
        // 0 玩家等级   1vip等级
                tiaoJianType2 ?:number
        // 
                tiaoJianValue1 ?:number
        // 
                tiaoJianValue2 ?:number
        // 消耗类型  内容   同奖励类型
                costType ?:number
        // 消耗数量
                costCount ?:number
        // 描述  后端拼好  前段显示   达到xxxx
                des2 ?:string
        // 描述  后端拼好  前段显示     需要xxxxx
                des1 ?:string
        // 
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 怪物来袭排名信息
    interface GuaiWuLaiXiRankInfoUnit  {
        // 玩家id
                playerId ?:number
        // 玩家等级 
                level ?:number
        // 玩家名称
                name ?:string
        // 玩家头像
                touxiang ?:number
        // 排名
                rank ?:number
        // 积分
                score ?:number
        // 刷新时间
                updatetime ?:number
        // 
                jiangliList ?:Array<JiangliInfoUnit>
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 活动－七日任务－今日好礼
    interface SDTLoginInfoUnit  {
        // 奖励数据
                jiangliList ?:Array<JiangliInfoUnit>
        // 领取状态：0、不可领；1、可以领，但尚未领取 2、已领取；
                doGet ?:number
    }
// 活动－七日任务－任务数据
    interface SDTQuestInfoUnit  {
        // 任务模板ID
                defId ?:number
        // 任务名称
                name ?:string
        // 任务类型
                type ?:number
        // 参数1
                param1 ?:number
        // 参数2
                param2 ?:number
        // 奖励数据
                jiangliList ?:Array<JiangliInfoUnit>
        // 领取状态：0、未领取；1、可领取；2、已领取；
                doGet ?:number
    }
// 活动－七日任务－今日特惠
    interface SDTBuyInfoUnit  {
        // 奖励数据
                jiangliList ?:Array<JiangliInfoUnit>
        // 消耗数据
                consume ?:msgObj.JiangliInfoUnit
        // 领取状态：0、不可领；1、可以领，但尚未领取 2、已领取；
                doGet ?:number
    }
// 活动－七日任务－每日奖励
    interface SDTDailyInfoUnit  {
        // 任务模板ID
                defId ?:number
        // 任务名称
                name ?:string
        // 任务类型
                type ?:number
        // 奖励数据
                jiangliList ?:Array<JiangliInfoUnit>
        // 领取状态：0、不可领；1、可以领，但尚未领取 2、已领取；
                doGet ?:number
        // 当前完成任务数量
                curCount ?:number
        // 需要完成任务总数量
                needCount ?:number
    }
// 超值返利数据
    interface ChaoZhiFanLiInfoUnit  {
        // defId 领奖时发送此id
                defId ?:number
        // 任务类型：1 培养等级2 超进化星级3 徽章总等级4 坐骑等级5 翅膀等级6 守护兽等级7 战力
                questType ?:number
        // 任务说明，对应chongzhifanli.xls中shuoming字段
                shuoming ?:string
        // 任务说明2，对应chongzhifanli.xls中shuoming2字段
                shuoming2 ?:string
        // 宠物id
                petId ?:number
        // 领取状态：0、不可领；1、可以领，但尚未领取 2、已领取；
                doGet ?:number
        // 当前进度
                curCount ?:number
        // 完成任务所需数量
                needCount ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 本服剩余的限购次数 -1表示不限购
                leftlimitBuyCountLocalServer ?:number
    }
// 神宠基金
    interface ShenChongJiJinInfoUnit  {
        // 
                defId ?:number
        // 
                jiangliList ?:Array<JiangliInfoUnit>
        // 0 未达成    1 可领取    2 已领取
                status ?:number
        // 描述
                miaoShu ?:string
    }
// 日充累充
    interface RiChongLeiChongInfoUnit  {
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 描述
                miaoShu ?:string
        // 领取状态：0、不可领；1、可以领，但尚未领取 2、已领取；
                doGet ?:number
        // 充到多少能
                dailyRechargeNum ?:number
        // 需要多少个累计充值才能领
                needLeiJiCount ?:number
        // 需要的vip等级
                needVip ?:number
        // 第几天的奖励
                howManyDaysReward ?:number
        // 条件类型 周卡的 月卡的 年卡的
                conditionType ?:number
    }
// 首冲团购
    interface ShouChongTuanGouUnit  {
        // defId
                defId ?:number
        // 首充玩家数量
                tiaoJian ?:number
        // 免费奖励列表
                mianFeiJiangLiList ?:Array<JiangliInfoUnit>
        // 首充奖励列表
                ShouChongJiangLiList ?:Array<JiangliInfoUnit>
        // 高级奖励列表
                specialJiangLiList ?:Array<JiangliInfoUnit>
        // 领取高级奖励需要充值金券
                specialNeed ?:number
        // 是否领取过免费奖励
                isGetMianFei ?:boolean
        // 是否领取过首冲奖励
                isGetShouChong ?:boolean
        // 是否领取过高级奖励
                isGetSpecial ?:boolean
    }
// 天天返利数据
    interface TianTianFanLiInfoUnit  {
        // defId 领奖时发送此id
                defId ?:number
        // 累计充值天数
                totalRechargeDays ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
        // 状态 0:不可领取 1:完成未领取 2:完成已领取
                state ?:number
    }
// 跨服秒杀
    interface KuaFuMiaoShaInfoUnit  {
        // defId 领奖时发送此id
                defId ?:number
        // 商品
                reward ?:msgObj.JiangliInfoUnit
        // 
                oldPrice ?:number
        // 价格  货币类型根据什么专场来区分
                price ?:number
        // 总共的个人的限购个数  限购分数的分母
                geRenXianGouTotal ?:number
        // 个人限购已经买了多少个 限购分数的分子
                hasBuy ?:number
        // 剩余xxx件 全服限购还剩多少个
                leftCount ?:number
        // 状态 0:不可购买 1:可以购买
                state ?:number
        // 这个商品通过什么货币买 (奖励类型)
                typeSpendingMoney ?:number
    }
// 开服限购大R礼包
    interface KaifuXiangouInfoUnit  {
        // 活动id
                activityId ?:number
        // defId 领奖时发送此id
                defId ?:number
        // 购买价格信息
                priceInfo ?:msgObj.JiangliInfoUnit
        // 是否已经购买
                hasBuy ?:boolean
        // 图片1
                img1 ?:string
        // 图片2
                img2 ?:string
        // 图片3
                img3 ?:string
        // 展示图
                display ?:string
        // 展示图类型
                imgType ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
        // 折扣价格信息
                discountPrice ?:msgObj.JiangliInfoUnit
        // 当前当前礼包折扣剩余可用次数
                nowLeft ?:number
        // 再支付多少金额，可以获得奖励折扣的机会。单位：人民币元
                nowPay ?:number
        // 最低折扣价格
                minDiscountPrice ?:number
    }
// 扭蛋商品信息
    interface NiuDanShopInfoUnit  {
        // 道具Id
                itemId ?:number
        // 原始价格
                oldPrice ?:number
        // 折后价格
                nowPrice ?:number
    }
// 限时召唤选择
    interface XianShiZhaoHuanChooseUnit  {
        // 策划表Id
                defId ?:number
        // 奖励信息
                jiangliInfo ?:msgObj.JiangliInfoUnit
        // 所需次数
                needNum ?:number
    }
// 皇权诏令奖励内容
    interface HuangquanZhaolingInfoUnit  {
        // 策划表Id
                defId ?:number
        // 所需活跃度
                tiaojian ?:number
        // 活跃累计奖励
                rewardNormalList ?:Array<JiangliInfoUnit>
        // 解锁奖励
                unlockRewardList ?:Array<JiangliInfoUnit>
        // 0：不可领取；1:可领取
                status ?:number
        // 0:未领取过；1:普通奖励领取过；2:全部领取过
                rewardStatus ?:number
        // jianglizhanshi 奖励展示  0为不展示  1为展示
                jianglizhanshi ?:number
    }
// 登录现金红包内容
    interface DengluHongbaoInfoUnit  {
        // 策划表Id，也就是天数
                defId ?:number
        // 0不可领 1可开红包 2可领红包 3已经领取
                status ?:number
        // 返现金额，单位：分
                amount ?:number
        // 是不是可提现的那一天
                isCashTheDay ?:boolean
    }
// 任务类现金红包内容
    interface RenwuHongbaoInfoUnit  {
        // 策划表类型，对应leixing字段
                renwuType ?:number
        // 0不可领 1可开红包 2可领红包 3已经领取
                status ?:number
        // 返现金额，单位：分
                amount ?:number
        // 标签名字，对应shuoming字段
                renwuTypeName ?:string
    }
// 限时等级礼包
    interface XianShiDengJiLiBaoUnit  {
        // defId
                defId ?:number
        // 充值Id
                chongZhiId ?:number
        // 名称
                title ?:string
        // 说明
                desc ?:string
        // 奖励列表
                jiangLiList ?:Array<JiangliInfoUnit>
        // 结束时间
                endTime ?:number
        // 是否买过了
                isBuy ?:boolean
    }
// 玩家新活动Tips数据
    interface NewActivityTipsInfoUnit  {
        // 活动ID
                id ?:number
        // 是否出现前端提示
                isTips ?:boolean
    }
// 自定义礼包内容
    interface DiyGiftInfoUnit  {
        // 策划表Id
                defId ?:number
        // 礼包名
                title ?:string
        // 已购买次数
                buyCount ?:number
        // 全部可购买次数，0表示无限制
                allCount ?:number
        // 奖励列表，可能为空奖励即type和count都是0表示未配置。列表的长度等于该礼包的位置个数
                rewardItemList ?:Array<DiyGiftRewardItemUnit>
        // 充值ID
                rechargeId ?:number
        // 消耗金券数量
                jinquanCount ?:number
        // 当前已经消耗的金券数量
                currJinquanCount ?:number
    }
// 自定义礼包商品选项信息
    interface DiyGiftRewardItemUnit  {
        // 商品Id
                itemId ?:number
        // 奖励
                reward ?:msgObj.JiangliInfoUnit
        // 是否有多个奖励可换
                isMoreThanOne ?:boolean
    }
// 自定义礼包位置信息
    interface DiyGiftPositionInfoUnit  {
        // 位置编号
                pos ?:number
        // 已选择的商品Id
                selectItemId ?:number
        // 商品待选项列表
                rewardItemList ?:Array<DiyGiftRewardItemUnit>
    }
// 折扣商店活动商品数据
    interface ZhekouShangdianGoodsUnit  {
        // 商品ID
                defId ?:number
        // 货币信息
                moneyInfo ?:msgObj.JiangliInfoUnit
        // 原来的货币价格
                originMoneyCount ?:number
        // 商品信息
                goodsInfo ?:msgObj.JiangliInfoUnit
        // 是否能购买
                isCanBuy ?:boolean
        // 需要的vip等级
                vipNeed ?:number
        // 领取次数上限 0为无限制
                ciShu ?:number
        // 已经购买次数
                hasBuyCount ?:number
    }
// 七日冲榜竞赛活动战斗力差值提示内容
    interface SevenMatchDiffFightTipsUnit  {
        // 文字提示
                words ?:string
        // 是否立即弹窗提示
                isPopNow ?:boolean
    }
// 各类基金活动行内容
    interface FundActivityDetailInfoUnit  {
        // 策划表Id
                defId ?:number
        // 基金类型:1 战力基金；2 契约神祗；3 古神宝藏
                type ?:number
        // 所需累计值
                value ?:number
        // 免费的累计奖励
                rewardNormalList ?:Array<JiangliInfoUnit>
        // 解锁奖励
                unlockRewardList ?:Array<JiangliInfoUnit>
        // 0：不可领取；1:可领取
                status ?:number
        // 0:未领取过；1:普通奖励领取过；2:全部领取过
                rewardStatus ?:number
        // 所需累计值对应的描述文本
                valueDesc ?:string
        // (关卡基金专用)0:未领取过；2:可领取；3:普通奖励领取过；
                freeStatus ?:number
        // (关卡基金专用)0:未领取过；1:满足条件但是未付费解锁；2:可领取；3:奖励已领取过
                costStatus ?:number
    }
// 活动－七日任务－七日商店数据
    interface SevenDayShopGoodsInfoUnit  {
        // sevendayshop表ID
                defId ?:number
        // 购买条件VIP限制
                vip ?:number
        // 折扣，单位/百分比
                zhekou ?:number
        // 原来价格
                yuanjia ?:number
        // 现价
                xianjiaInfo ?:msgObj.JiangliInfoUnit
        // 奖励
                rewardInfo ?:msgObj.JiangliInfoUnit
        // 是否已经购买
                isBought ?:boolean
    }
// 金券消排行奖励内容
    interface XiaohaoPaihangRewardInfoUnit  {
        // 排行From
                paihangFrom ?:number
        // 排行To
                paihangTo ?:number
        // 可获得奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 单位
                danwei ?:string
        // 上榜条件值
                limitValue ?:number
        // 单位1
                danwei1 ?:string
        // 当前排行榜排序条件的上榜条件值，积分排行榜则为积分条件值
                limitPaihangValue ?:number
        // 单位2，跟limitPaihangValue对应
                danwei2 ?:string
    }
// 金券消排行玩家排行信息
    interface XiaohaoPaihangPlayerInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家Name
                playerName ?:string
        // VIP等级
                vipLevel ?:number
        // 当前排行
                curPaihang ?:number
        // 排行显示值,例如XX金券等
                paihangValue ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 累计登陆活动23补签
    interface LeiJiDengLuAddLoginDayUnit  {
        // 剩余可补签条数
                leftAddLoginDay ?:number
        // 当前补签价格，阶梯价格，随着补签天数而变化
                costInfo ?:msgObj.JiangliInfoUnit
        // 补签按钮是否可见
                isButtonVisible ?:boolean
    }
// 活跃任务活动的任务详细数据
    interface HuoyueRenwuMissionInfoUnit  {
        // 任务ID
                id ?:number
        // 任务定义ID
                renwuId ?:number
        // 任务完成度 改成long针对战力
                count ?:number
        // 任务状态:-1格子未开启，0未开始，1进行中，2已完成
                status ?:number
        // 领取状态:1未领取；2已领取
                isGet ?:number
        // 任务名称
                name ?:string
        // 任务类型
                type ?:number
        // 任务属于类型
                renwuType ?:number
        // 跳转页面
                jump ?:string
        // 任务总的进度，条件值 改成long针对战力
                jindu ?:number
        // 可获得奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 活动详情激活开启条件
    interface ActivityDetailActiveInfoUnit  {
        // 激活条件
                costInfo ?:msgObj.JiangliInfoUnit
        // 可获得奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 幸运转盘活动格子信息
    interface LuckyWheelGridUnit  {
        // 格子ID，对应xingyunzhuanpan表Id
                defId ?:number
        // 大奖展示 客户端展示当前未获得的道具
                dajiangzhanshi ?:number
        // 可获得奖励
                rewardInfo ?:msgObj.JiangliInfoUnit
        // 是否已经获得该奖励
                isHadGet ?:boolean
    }
// 幸运转盘活动摇奖条件信息
    interface LuckyWheelRewardConditionUnit  {
        // 1-3分别代表充值金额 消耗道具 下次需要的消耗材料
                type ?:number
        // 增加一次摇奖次数所需的消耗
                costInfo ?:msgObj.JiangliInfoUnit
        // 消耗材料不足时的文字提示
                beizhuDefId ?:number
        // 跳转
                jump ?:string
    }
// 装备 宝石等等几个副本的奖励
    interface NewFuBenJiangLiInfoUnit  {
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 玩家排行数据
    interface PaihangInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 服务器编号
                serverNum ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家头像ID
                playerTouxiangId ?:number
        // 服务器id  长id 用于跨服消息
                serverChangId ?:number
        // 玩家坐骑ID
                playerZuoqi ?:number
        // 玩家翅膀ID
                playerChibang ?:number
        // 玩家等级
                playerLevel ?:number
        // 玩家VIP等级
                playerVipLevel ?:number
        // 玩家贵族等级
                playerNobleLevel ?:number
        // 玩家战斗力
                playerFight ?:number
        // 竞技场排名
                paihangBW ?:number
        // 排名
                position ?:number
        // 积分
                score ?:number
        // 段位，匹配战用
                part ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 称号
                chenghaoId ?:number
        // 公会名称
                gonghuiName ?:string
        // 公会等级
                gonghuiLevel ?:number
        // 跟随宠显示
                followPetShow ?:msgObj.NewFollowPetShowUnit
        // 跟随宠大小
                followPetSize ?:number
        // 跟随宠皮肤Id
                followPetPiFu ?:number
        // 战棋胜场
                zhanQiShengChang ?:number
        // 战棋段位
                zhanQiDuanWei ?:number
        // 皮神进阶外显皮肤
                displayLevel ?:number
        // 公会职位
                gonghuiZhiwei ?:number
        // 公会徽章 边框,底图,标志
                gongHuiHuiZhang ?:string
        // 排位赛
                paihangPPDefId ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 威名显示unit
                weimingInfo ?:msgObj.WeimingShowInfoUnit
        // 威名阶段，所有排行榜都有
                weimingLevel ?:number
    }
// 比武场排行分享相关数据
    interface PaihangShareInfoUnit  {
        // 排行榜类型：1,等级榜；2,战力榜；3,竞技榜；4,匹配榜；5,遭遇榜  等等
                type ?:number
        // 今日还可分享多少次
                leftShareTimes ?:number
        // true  有红点   false 没有
                isTips ?:boolean
    }
// 拍卖物品详情
    interface PaiMaiWuPinInfoUnit  {
        // db ID
                id ?:number
        // 拍卖物品名称
                name ?:string
        // 物品图标
                image ?:string
        // 一口价
                price ?:number
        // 当前价格
                currentPrice ?:number
        // 出价
                offerPrice ?:number
        // 起拍时间
                startTime ?:number
        // 0.公会 1.世界 2.仅在世界拍卖
                paiMaiFangShi ?:number
        // 0.未竞价 1.出价被超过 2.出价最高
                myState ?:number
        // 标签类型
                labelType ?:number
        // 标签名称
                labelName ?:string
        // 结束时间
                endTime ?:number
        // 道具ID
                itemDefId ?:number
        // 道具ID
                itemRank ?:number
    }
// 拍卖物品详情
    interface PaiMaiLabelInfoUnit  {
        // 栏位名称
                labelName ?:string
        // 栏位
                labelType ?:number
    }
// 拍卖记录
    interface PaiMaiLogInfoUnit  {
        // 拍卖物品名称
                name ?:string
        // 物品图标
                image ?:string
        // 成交价格
                price ?:number
        // 交易方式 1.买入 2.卖出
                tradeType ?:number
        // 交易对象名字
                tagName ?:string
        // 交易对象区服
                tarServerId ?:number
        // 0.公会 1.世界 2.仅在世界拍卖
                paiMaiFangShi ?:number
        // 成交日期
                tradeTime ?:string
        // 物品defID
                itemDefId ?:number
        // 道具ID
                itemRank ?:number
    }
// 拍卖背包详情
    interface PaiMaiBeiBaoInfoUnit  {
        // 物品名称
                itemName ?:string
        // 物品图标
                itemIcon ?:string
        // 物品数量
                itemCount ?:number
        // 物品defId
                itemDefId ?:number
        // 物品类型
                itemType ?:number
        // 物品一口价
                price ?:number
        // 物品起拍价
                startPrice ?:number
        // 物品dbid
                itemDbId ?:number
        // 道具ID
                itemRank ?:number
    }
// 拍卖物品详情
    interface NotifyItemInfoUnit  {
        // 物品类型
                itemType ?:number
        // 物品数量
                itemCount ?:number
        // 物品DefId
                itemDefId ?:number
    }
// 累计充值送特权卡的信息
    interface CumulativeRechargeCardInfoUnit  {
        // Project表Id
                projectDefId ?:number
        // 当前累充
                curValue ?:number
        // 所需累充条件数值
                conditionValue ?:number
        // 是否已经领取特权表奖励
                isGetTequanReward ?:boolean
    }
// 宠物信息
    interface PetInfoUnit  {
        // defId
                defId ?:number
        // 名称
                name ?:string
        // 头像
                icon ?:string
        // 是否拥有过
                hasOwned ?:boolean
        // 是否已进化
                isJinHua ?:boolean
        // 超进化级别
                CJHLevel ?:number
        // 是否已闪光
                isShanGuang ?:boolean
        // 培养级别
                PeiYangLevel ?:number
    }
// 宠物手册信息
    interface PetHandbookInfoUnit  {
        // defId
                defId ?:number
        // 星级
                star ?:number
        // 激活状态（0不可激活，1可激活，2已激活）
                activeState ?:number
    }
// 宠物羁绊信息
    interface PetCombinationUnit  {
        // defId，对应jinglingjiban表中id
                defId ?:number
        // 阶数
                step ?:number
        // 组合id
                combinationId ?:number
        // 组合名称
                name ?:string
        // 组合开启等级
                openLevel ?:number
        // 组合稀有度：1优良2高级3精品4稀有5传说
                rarity ?:number
        // 目标（100 收集; 101 进化; 102 全部超进化+5; 103 全部闪光
                target ?:number
        // 完成目标所需数量（目前对于类型102，这是达成目标所需要的超进化级别;100,101,103不需要此参数；）
                needCount ?:number
        // 宠物信息列表
                petList ?:Array<PetInfoUnit>
        // 激活状态（0不可激活，1可激活，2已激活）
                activeState ?:number
        // 集齐增加属性列表
                addPropList ?:Array<ShuXingInfoUnit>
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 面板上新守护兽信息
    interface NewGuardPetInfoUnit  {
        // 守护兽DB Id
                dbId ?:number
        // 守护兽名称
                petName ?:string
        // 骨骼动画文件名
                petActName ?:string
        // 守护神本体形象
                petImage ?:string
        // 0未解锁 1已解锁
                status ?:number
        // 解锁等级
                unlockLevel ?:number
        // 技能等级
                skillLevel ?:number
        // 是否可升级
                isRed ?:boolean
        // 当前等级提供给全队的属性列表
                curHeroShuXingList ?:Array<ShuXingInfoUnit>
        // 下一等级提供给全队的属性列表。如果该位置的守护兽已经满级，则不提供下一等级的属性，该值为空数组
                nextHeroShuXingList ?:Array<ShuXingInfoUnit>
        // 当前等级守护兽自身属性列表
                curGuardPetShuXingList ?:Array<ShuXingInfoUnit>
        // 下一等级守护兽自身属性列表。如果该位置的守护兽已经满级，则不提供下一等级的属性，该值为空数组
                nextGuardPetShuXingList ?:Array<ShuXingInfoUnit>
        // 是否满级
                isFullLevel ?:boolean
        // 是否已经唤醒
                isOpen ?:boolean
        // 守护兽头像
                petHeadImage ?:string
        // 额外属性
                eatraShuXing ?:msgObj.ShuXingInfoUnit
        // 额外属性描述
                eatraDesc ?:string
        // 技能描述
                skillDesc ?:string
        // 攻击目标显示
                attackTarget ?:string
        // 守护兽贴图
                skeletonName ?:string
        // 守护兽头像边框，档次颜色
                grade ?:number
        // 当前等级
                curLevel ?:number
        // 消耗列表
                costList ?:Array<JiangliInfoUnit>
        // 是否技能已经满级
                isSkillMax ?:boolean
        // 技能升级消耗列表
                skillCostList ?:Array<JiangliInfoUnit>
        // 消耗相同守护神道具可立刻激活，已激活的守护兽该值为undefined
                activeCostList ?:Array<JiangliInfoUnit>
        // 进阶等级
                jinjieNum ?:number
        // 当前进阶等级提供的属性列表
                curJinjieShuXingList ?:Array<ShuXingInfoUnit>
        // 当前进阶的洗练属性列表
                xilianShuXingList ?:Array<XiLianPropUnit>
        // 未开启的洗练属性，开启进阶等级列表
                lockJinjieNumList ?:Array<number>
        // 单个守护兽的红点列表：进阶红点、洗练红点、初中高3类的丹药红点100007~100009
                redPointList ?:Array<BoolRedPointUnit>
        // 超过0次数的丹药简略信息列表
                danyaoTypeGreaterThanZeroList ?:Array<NewGuardPetDanyaoMiniInfoUnit>
        // 守护兽系统提供给全队的属性列表
                allTeamPropList ?:Array<ShuXingInfoUnit>
        // 守护兽名字图片
                petNameStr ?:string
        // VIP多少级增加属性
                vipLevel ?:number
        // 守护兽VIP属性，如果策划表填的是0，这里赋值为undefined，不传
                vipShuXing ?:msgObj.ShuXingInfoUnit
        // 守护兽策划表ID
                defId ?:number
        // 守护兽付费技能数据
                paySkillList ?:Array<NewGuardPetPaySkillUnit>
        // 当前进阶等级提供的升星等级上限
                maxLevel ?:number
        // 守护兽自身总战力
                fight ?:number
        // 守护兽推荐属性
                recommendProp ?:msgObj.ShuXingInfoUnit
    }
// 新守护兽上阵信息
    interface NewGuardPetPositionUnit  {
        // 位置1-5
                position ?:number
        // 位置描述
                positionDesc ?:string
        // 上阵的守护兽DB Id，不能为空
                dbId ?:number
        // 位置是否已经开启
                isOpen ?:boolean
    }
// 新守护兽进阶属性和消耗信息
    interface NewGuardPetJinjieInfoUnit  {
        // 守护兽DB Id
                dbId ?:number
        // 当前进阶等级
                jinjieNum ?:number
        // 是否进阶已经满级
                isFullJinjie ?:boolean
        // 消耗列表
                costList ?:Array<JiangliInfoUnit>
        // 当前进阶等级提供的属性列表
                curShuXingList ?:Array<ShuXingInfoUnit>
        // 下一阶进阶等级提供提供的属性列表。如果该位置的守护兽已经满级，则不提供下一等级的属性，该值为空数组
                nextShuXingList ?:Array<ShuXingInfoUnit>
        // 当前进阶的洗练属性列表
                xilianShuXingList ?:Array<XiLianPropUnit>
        // 未开启的洗练属性，开启进阶等级列表
                lockJinjieNumList ?:Array<number>
        // 当前进阶等级提供的升星等级上限
                curMaxLevel ?:number
        // 下一进阶等级提供的升星等级上限
                nextMaxLevel ?:number
        // 当前等级守护兽自身属性列表
                curGuardPetShuXingList ?:Array<ShuXingInfoUnit>
        // 下一等级守护兽自身属性列表。如果该位置的守护兽已经满级，则不提供下一等级的属性，该值为空数组
                nextGuardPetShuXingList ?:Array<ShuXingInfoUnit>
    }
// 新守护兽丹药信息
    interface NewGuardPetDanyaoInfoUnit  {
        // 守护兽DB Id
                dbId ?:number
        // 丹药的分类：初中高
                danyaoType ?:number
        // 当前吃丹次数
                danyaoNum ?:number
        // 吃丹次数上限
                maxDanyaoNum ?:number
        // 是否吃丹已经满级
                isFullDanyao ?:boolean
        // 消耗
                costInfo ?:msgObj.JiangliInfoUnit
        // 全队属性列表
                propList ?:Array<PropertyInfoUnit>
        // 守护兽自身属性列表
                guardPetShuXingList ?:Array<ShuXingInfoUnit>
    }
// 新守护兽红点信息
    interface NewGuardPetRedPointInfoUnit  {
        // 守护兽DB Id
                dbId ?:number
        // 单个守护兽的红点列表：进阶红点、洗练红点、初中高3类的丹药红点100007~100009
                redPointList ?:Array<BoolRedPointUnit>
    }
// 新守护兽丹药简略信息
    interface NewGuardPetDanyaoMiniInfoUnit  {
        // 丹药的分类：初中高
                danyaoType ?:number
        // 当前吃丹次数
                danyaoNum ?:number
    }
// 新守护兽升星额外属性描述
    interface NewGuardPetExtraDescInfoUnit  {
        // 解锁等级
                level ?:number
        // 额外属性描述
                extraDesc ?:string
    }
// 新守护兽升星消耗信息
    interface NewGuardPetLevelUpCostInfoUnit  {
        // 守护兽DB Id
                dbId ?:number
        // 是否满级
                isFullLevel ?:boolean
        // 是否已经唤醒
                isOpen ?:boolean
        // 当前等级
                curLevel ?:number
        // 消耗列表
                costList ?:Array<JiangliInfoUnit>
        // 消耗相同守护神道具可立刻激活，已激活的守护兽该值为undefined
                activeCostList ?:Array<JiangliInfoUnit>
        // 守护兽策划表ID
                defId ?:number
        // 守护兽名称
                petName ?:string
        // 守护兽头像
                petHeadImage ?:string
        // 守护兽头像边框，档次颜色
                grade ?:number
        // 当前进阶等级所允许的最高守护兽等级
                maxLevel ?:number
        // 守护兽自身总战力
                fight ?:number
    }
// 战力提升
    interface StrategyFightUnit  {
        // 图标
                icon ?:string
        // 关键字
                keyword ?:string
        // 描述
                name ?:string
        // 描述
                desc ?:string
        // 战力
                fight ?:number
        // 本服最强战力
                maxFight ?:number
        // 排序
                paixu ?:number
        // jump表id
                jumpId ?:number
    }
// 推荐阵容
    interface StrategyRecommendUnit  {
        // 阵容DefId
                defId ?:number
        // 万分比
                percent ?:number
        //  精灵数据
                heroList ?:Array<PetHandbookInfoUnit>
    }
// 本服第一阵容
    interface StrategyFirstUnit  {
        // 战力
                fight ?:number
        //  精灵数据
                heroList ?:Array<PlayerCardHeroInfoUnit>
    }
// 守护兽付费技能数据
    interface NewGuardPetPaySkillUnit  {
        // 付费技能表Id
                defId ?:number
        // 是否已经激活
                isActive ?:boolean
        // 付费技能名称
                name ?:string
        // 充值金额表Id
                chongzhiId ?:number
        //  付费技能激活道具
                xiaohaoList ?:Array<JiangliInfoUnit>
        // 技能图片
                icon ?:string
        // 点击图片弹出描述
                tanchu ?:string
        // 类型 0 直购类型 1 活动购买对应类型
                type ?:number
        // 免费时装，单笔充值完成moban对应 1 充值对应 2 活动对应 3 现场购买 4 特权类型
                moban ?:number
        // 跳转jump表提供 0 无跳转 配置参数跳转至配置页面
                jump ?:number
        //  属性列表
                propertyList ?:Array<PropertyInfoUnit>
    }
// 皮肤图鉴系统unit
    interface PifuTujianInfoUnit  {
        // 皮肤图鉴表Id
                defId ?:number
        // 是否激活点亮了
                isOpen ?:boolean
        // 当前星级或者等级
                level ?:number
        // 升级消耗
                costForLevelUp ?:msgObj.JiangliInfoUnit
    }
// 皮肤图鉴单皮肤详细信息unit
    interface PifuTujianOnePifuInfoUnit  {
        // 皮肤图鉴表Id
                defId ?:number
        // 是否激活点亮了
                isOpen ?:boolean
        // 当前星级或者登记
                level ?:number
        // 当前等级属性
                curPropList ?:Array<PropertyInfoUnit>
        // 下一等级属性
                nextPropList ?:Array<PropertyInfoUnit>
        // 是否满级
                isFull ?:boolean
        // 升级消耗
                costForLevelUp ?:msgObj.JiangliInfoUnit
    }
// 骨骼部件信息
    interface GugePartUnit  {
        // 部件名称，同骨骼表
                part ?:string
        // 部件内容
                value ?:string
    }
// 抽奖结果信息
    interface PifuTujianLuckyDrawInfoUnit  {
        // 皮肤图鉴表Id
                defId ?:number
        // 是否重复的皮肤
                isRepeat ?:boolean
    }
// 皮肤unit
    interface PiFuInfoUnit  {
        // 
                defId ?:number
        // true  激活了  false 没有 激活=拥有
                isJiHuo ?:boolean
        // 当前进阶的星级 0未进阶
                jinJieStar ?:number
        // 当前皮肤属性
                curShuXingList ?:Array<ShuXingInfoUnit>
        // 是否满级
                isFull ?:boolean
    }
// 皮肤套装属性unit
    interface PiFuTaozhuangInfoUnit  {
        // 套装组
                taozhuangGroup ?:number
        // 套装数量
                taozhuangNum ?:number
        // 套装名称
                name ?:string
        // 套装描述
                desc ?:string
        // 属性
                propList ?:Array<PropertyInfoUnit>
        // 已经激活多少件
                jiHuoCount ?:number
    }
// 皮卡丘状态信息
    interface PikachuStateUnit  {
        // 跑了多少米
                meter ?:number
        // 被押注多少钱
                betMoney ?:number
        // 总共押了多少钱
                totalMoney ?:number
        // 多少人点击了刷新buff
                buffTimes ?:number
        // 就否已经点击了buff
                haveBuff ?:boolean
        // buff:0:禁锢 1:慢跑 2:快跑
                buff ?:number
    }
// 玩家排行数据
    interface PlayerBasicInfoUnit  {
        // 玩家昵称
                playerName ?:string
        // 玩家性别
                gender ?:number
        // 金币
                money ?:number
        // 钻石
                currency ?:number
        // 等级
                level ?:number
        // vip等级
                viplevel ?:number
        // 贵族等级
                nobleLevel ?:number
        // 经验值
                exp ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_11 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_12 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_13 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_14 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_15 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_16 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_17 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_18 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                zf_19 ?:number
        // 是否付费用户：0、未付费；1、已付费；
                isPay ?:number
        // 头像ID
                touxiangId ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 子系统属性数据
    interface PlayerCardSubSystemPropertyUnit  {
        // Project表Id
                projectId ?:number
        // 该系统提供的属性
                propList ?:Array<HeroPropertyInfoUnit>
    }
// 装备数据
    interface PlayerCardZhuangbeiInfoUnit  {
        // 装备数据表Key
                zhuangbeiDbID ?:number
        // 原始装备ID-静态表主键
                zhuangbeiID ?:number
        // 装备类型 0：武器 1：盔甲 2：坐骑 3: 宝物
                zhuangbeiType ?:number
        // 强化等级
                qianghuaLevel ?:number
        // 附魔等级
                fumoLevel ?:number
        // 升星等级
                shengxingLevel ?:number
        // 镶嵌宝石1
                baoshiID1 ?:number
        // 镶嵌宝石2
                baoshiID2 ?:number
        // 镶嵌宝石3
                baoshiID3 ?:number
        // 升星属性列表
                shengXingPropInfoList ?:Array<PropertyInfoUnit>
        // 洗练属性列表
                xilianPropInfoList ?:Array<XiLianPropUnit>
        // 精炼属性列表
                jinglianPropInfoList ?:Array<PropertyInfoUnit>
        // 精炼等级
                jinglianLevel ?:number
        // 附魔等级上限
                maxFumoLevel ?:number
    }
// 精灵宠物数据
    interface PlayerCardHeroInfoUnit  {
        // 上阵宠物位置：11-19，协作宠物位置：111-119
                pos ?:number
        // 武将数据表Key
                heroDbID ?:number
        // 武将ID, 静态ID
                heroID ?:number
        // 等级
                level ?:number
        // 官职等级
                levelGuanZhi ?:number
        // 小培养阶段
                xiaoPeiYangJieDuan ?:number
        // 单宠战斗力
                fight ?:number
        // 宠物属性列表
                propList ?:Array<HeroPropertyInfoUnit>
        // 当前武将的品阶
                pinJie ?:number
        // 当前武将的进阶
                wujiangleveljinjieDefId ?:number
        // 超进化阶段
                chaoJinHuaJie ?:number
        // 超进化星级
                chaoJinHuaXing ?:number
        // false 不会超进化       true会超进化
                shiFouChaojinhua ?:boolean
        // false 不会闪光       true会闪光
                shiFouShanguang ?:boolean
        // 洗练属性
                xiLianPropList ?:Array<HeroXiLianPropInfoUnit>
        // 宠物子系统提供的属性
                subSystemPropList ?:Array<PlayerCardSubSystemPropertyUnit>
        // 宠物已经穿戴的装备列表
                zhuangbeiList ?:Array<PlayerCardZhuangbeiInfoUnit>
        // 职业神器
                zhiYeShenQiInfo ?:msgObj.ZhiYeShenQiUnit
        // 专属神器
                zhuanShuShenQiInfo ?:msgObj.PlayerCardZhuanShuShenQiUnit
        // 种族值信息
                zhongZhuZhiInfo ?:msgObj.PlayerCardZhongZuZhiUnit
        // 新神技卡列表
                magicalSkillList ?:Array<MagicalSkillItemUnit>
        // 单宠本服排行
                rank ?:number
        // 神技协作效果列表
                magicalSkillCooperateList ?:Array<MagicalSkillCooperateUnit>
    }
// 玩家名片上阵宠物详情数据
    interface PlayerCardAllHeroesUnit  {
        // 玩家ID
                playerId ?:number
        // 精灵数据
                heroList ?:Array<PlayerCardHeroInfoUnit>
        // 没有穿戴身上的专属神器图鉴信息
                exZhuanShuShenQiList ?:Array<PlayerCardZhuanShuShenQiUnit>
        // 技能书
                skillBookList ?:Array<PlayerCardSkillBookUnit>
    }
// 玩家名片上阵宠物专属神器信息
    interface PlayerCardZhuanShuShenQiUnit  {
        // 强化属性列表
                qiangHuaShuXing ?:Array<ShuXingInfoUnit>
        // 神器属性列表
                shenQiShuXing ?:Array<ShuXingInfoUnit>
        // dbId
                dbId ?:number
        // defId
                defId ?:number
        // 等级
                zhuanShuShenLevel ?:number
        // 图鉴星级
                starNum ?:number
        // 是否激活专属技能
                isJiHuo ?:boolean
        // 专属神器描述
                desc ?:string
        // 专属神器升星预览属性列表
                starShowPropList ?:Array<PropertyInfoUnit>
    }
// 玩家名片上阵宠物种族值
    interface PlayerCardZhongZuZhiUnit  {
        // 种族值等级
                zhongZhuZhiLevel ?:number
        // 当前属性列表
                curShuXing ?:Array<PropertyInfoUnit>
        // 突破详情
                toPoDetails ?:Array<ZhongZuZhiTuPoDetailsUnit>
    }
// 玩家名片基础信息
    interface PlayerCardCommonInfoUnit  {
        // dbid
                playerId ?:number
        // 战斗力
                playerFight ?:number
        // 等级
                playerLevel ?:number
        // vip
                vip ?:number
        // 名字
                playerName ?:string
        // 所在服务器
                serverNum ?:number
        // 头像
                playerTouxiangId ?:number
        // 坐骑id
                playerZuoqi ?:number
        // 翅膀id
                playerChibang ?:number
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 称号
                chenghaoId ?:number
        // 创建角色时间
                createTime ?:string
        // 竞技场排名
                paihangBW ?:number
        // 战斗力排名
                paihangFight ?:number
        // 排位赛段位
                paiHangPPId ?:number
        // 排位赛等级段
                levelPart ?:number
        // 精灵收集数量
                heroCollectSize ?:number
        // 玩家的公会id
                gongHuiId ?:number
        // 公会名称
                gongHuiName ?:string
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 是否是对手
                isEnemy ?:boolean
        // 是否可以成为对手 （我的对手数量有上限）
                isCanBeEnemy ?:boolean
        // 转生等级
                zhuanshengLevel ?:number
        // 头像框表Id
                touxiangkuangDefId ?:number
    }
// 玩家名片幻化
    interface PlayerCardHuanHuaUnit  {
        // 属性列表
                shuXingInfoList ?:Array<ShuXingInfoUnit>
        // 套装属性信息列表
                taozhuangInfoList ?:Array<PiFuTaozhuangInfoUnit>
        // 皮肤信息列表
                piFuInfoList ?:Array<PlayerCardHuanHuaPiFuUnit>
    }
// 玩家名片幻化皮肤
    interface PlayerCardHuanHuaPiFuUnit  {
        // 类型
                type ?:number
        // 皮肤列表
                piFuInfoList ?:Array<PlayerCardHuanHuaPiFuDetailUnit>
    }
// 玩家名片幻化皮肤
    interface PlayerCardHuanHuaPiFuDetailUnit  {
        // 皮肤基本信息
                piFuInfo ?:msgObj.PiFuInfoUnit
        // 进阶属性
                shuXingList ?:Array<ShuXingInfoUnit>
    }
// 玩家名片皮神
    interface PlayerCardLuoTuoMuUnit  {
        // 属性列表
                shuXingInfoList ?:Array<ShuXingInfoUnit>
        // 当前进阶等级
                level ?:number
        // 显示类型
                showType ?:number
        // 资源数据
                taozhuangInfoList ?:Array<LuoTuoMuDisplayInfoUnit>
    }
// 玩家名片训练师
    interface PlayerCardZhuJuePeiYangUnit  {
        // 属性列表
                shuXingInfoList ?:Array<ShuXingInfoUnit>
        // 当前进阶等级
                level ?:number
    }
// 玩家名片转生
    interface PlayerCardZhuanShengUnit  {
        // 属性列表
                shuXingInfoList ?:Array<ShuXingInfoUnit>
        // 当前等级
                curLevel ?:number
        // 当前经验值
                hasExp ?:number
        // 满级所需经验值
                needExp ?:number
        // 特效
                teXiao ?:number
    }
// 玩家名片跟随宠
    interface PlayerCardFollowPetUnit  {
        // 属性列表
                shuXingInfoList ?:Array<ShuXingInfoUnit>
        // 当前进阶等级
                level ?:number
        // 显示类型
                showType ?:number
        // 资源数据
                taozhuangInfoList ?:Array<NewFollowPetShowUnit>
    }
// 玩家名片守护兽
    interface PlayerCardGuardPetUnit  {
        // 总属性列表
                totalShuXingList ?:Array<ShuXingInfoUnit>
        // 守护兽列表
                guardPetList ?:Array<PlayerCardNewGuardPetPositionUnit>
        // 守护兽上阵信息列表
                positionList ?:Array<NewGuardPetPositionUnit>
    }
// 玩家名片守护兽
    interface PlayerCardNewGuardPetPositionUnit  {
        // 外部信息
                guardPetInfo ?:msgObj.NewGuardPetInfoUnit
        // 守护兽头像
                petAdvatar ?:string
        // 技能名称
                skillName ?:string
        // 技能等级
                skillLevel ?:number
        // 技能描述
                skillDesc ?:string
        // 下一技能描述
                skillNextDesc ?:string
    }
// 玩家名片技能书
    interface PlayerCardSkillBookUnit  {
        // 宠物位置编号,位置编号 1-3
                index ?:number
        // 等级
                level ?:number
        // 星级
                star ?:number
        // 上阵技能书的DbId
                skillBookDbId ?:number
        // 上阵的宠物Db Id List
                heroIdList ?:Array<number>
        // 技能书属性列表
                propList ?:Array<HeroPropertyInfoUnit>
        // 上阵技能书的星级描述
                starDesc ?:string
        // 上阵技能书Id
                defId ?:number
        // 技能书协作文本列表
                xiezuoList ?:Array<SkillBookXiezuoBeizhuUnit>
    }
// 玩家创角待选职业信息
    interface PlayerProfessionUnit  {
        // 职业Id
                professionDefId ?:number
        // 骨骼表Id
                gugeDefId ?:number
        // 名称
                name ?:string
        // 说明
                desc ?:string
    }
// 玩家主角形象部分
    interface ActorPhotoUnit  {
        // 称号表Id
                chenghao ?:number
        // 坐骑表Id
                zuoqi ?:number
        // 翅膀表Id
                chibang ?:number
        // 足迹表Id
                zuji ?:number
        // 头像框表Id
                touxiangkuang ?:number
        // 头衔表Id
                touxian ?:number
        // 精灵(洛托姆)皮肤Id
                luotuomuPifu ?:number
        // 跟随宠皮肤Id
                followPetPifu ?:number
        // 神格(主角培养)皮肤Id
                zhujuePeiyangPifu ?:number
        // 洛托姆(皮神之力)进阶外显，对应luotuomujinjie表id
                luotuomuDisplay ?:number
        // 跟随宠外面，对应gensuichong表Id
                followPetDefId ?:number
        // 光环
                guangHuan ?:number
    }
// 玩家个人形象信息
    interface PlayerPhotoUnit  {
        // 骨骼表Id
                gugeDefId ?:number
        // 边框Id
                borderId ?:number
        // 有改变的骨骼内容，改变个人默认形象系统未开启前，该值可为undefined
                changeGuge ?:msgObj.GugeUnit
        // 玩家主角形象部分
                actorPhoto ?:msgObj.ActorPhotoUnit
    }
// 渠道通用领奖信息
    interface ChannelJiangLiStatusUnit  {
        // 奖励分类:1关注；2实名认证；3分享；4添加桌面；
                type ?:number
        // 奖励列表
                jiangLiList ?:Array<JiangliInfoUnit>
        // 是否已经领奖完毕
                isGetReward ?:boolean
    }
// Project状态
    interface ProjectStatusUnit  {
        // Project表Id
                projectDefId ?:number
        // 是否开启，false表示功能结束了
                isOpen ?:boolean
        // 开启时间
                startTime ?:number
        // 结束时间
                endTime ?:number
    }
// 铁拳玩家基本信息
    interface GamePvpPlayerInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 铁拳结束时间
                endTime ?:number
        // 玩家昵称
                playerName ?:string
        // 服务器长ID
                serverId ?:number
        // 服务器编号
                serverNum ?:number
        // 场景等级
                sceneLevel ?:number
        // 主服ID
                masterServerId ?:number
        // 场景编号
                sceneNum ?:number
    }
// int的是阶段  字符串表示这一阶段的特殊属性
    interface QiJiTeShuShuXingInfoUnit  {
        // 
                jieDuan ?:number
        // 特殊属性的中文描述
                shuXingMiaoShu ?:string
    }
// 奇遇礼包unit
    interface QiYuLiBaoInfoUnit  {
        // 礼包dbId
                dbId ?:number
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
        // 货币类型1rmb  2钻石   3 金币
                currencyType ?:number
        // 货币数量
                currencyCount ?:number
        // 0 可以买 1可以领取 2领过
                status ?:number
    }
// 奇遇面板信息
    interface QiYuPanelInfoUnit  {
        // id
                id ?:number
        // 功能ID
                projectId ?:number
        // 功能名称
                name ?:string
        // 描述
                desc ?:string
        // 描述1
                desc1 ?:string
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 精灵战斗信息
    interface HeroFightInfoUnit  {
        // 精灵Definition ID
                heroId ?:number
        // 输出伤害
                makeDamage ?:number
        // 回复生命
                recoverHp ?:number
        // 承受伤害
                damage ?:number
    }
// 玩家红点信息Byte型
    interface ByteRedPointUnit  {
        // 功能ID
                fcId ?:number
        // 兼容int byte类型
                byteStatus ?:number
    }
// 玩家红点信息Bool型
    interface BoolRedPointUnit  {
        // 功能ID
                fcId ?:number
        // true显示false不显示
                boolStatus ?:boolean
    }
// 忍村宝箱
    interface RenCunBaoXiangUnit  {
        // 宝箱dbid
                dbid ?:number
        // 宝箱defId
                defId ?:number
        // 获取时间
                getTime ?:number
        // 倒计时长
                daoJiShi ?:number
        // 消耗金券
                costPrice ?:number
    }
// 保卫忍村怪物
    interface RenCunBossUnit  {
        // bossid
                id ?:number
        // 名称
                name ?:string
        // 0 近身 1 远程
                type ?:number
        // 0 默认距离最近 1 攻击主城
                mubiao ?:number
        // 怪物形象
                image ?:string
        // 生命
                hp ?:number
        // 攻击
                atk ?:number
        // 防御
                def ?:number
        // 移动速度 1秒移动的帧 0 不会移动
                movespeed ?:number
        // 攻击速度 单位/毫秒 攻击1次
                atkspeed ?:number
        // 攻击范围 像素
                atkscope ?:number
    }
// 忍村建筑技能
    interface RenCunJianZhuSkillUnit  {
        // 技能id
                id ?:number
        // 技能类型
                type ?:number
        // 技能等级
                level ?:number
        // 当前攻击(被动技能)
                curAtk ?:number
        // 当前伤害(主动技能)
                curDamage ?:number
        // 下一级伤害
                nextDamage ?:number
        // 下一级百分比
                nextPercent ?:number
        // 需要的道具id
                xiaohaoItem ?:number
        // 需要的道具数量
                xiaohaoCount ?:number
        // 是否已经满级
                isFull ?:boolean
        // 描述
                desc ?:string
        // 技能图标
                icon ?:string
        // 下一级忍蛙攻击
                nextAtk ?:number
        // 当前技能属性列表
                curShuXingList ?:Array<ShuXingInfoUnit>
        // 下一级技能属性列表
                nextShuXingList ?:Array<ShuXingInfoUnit>
    }
// 忍村建筑
    interface RenCunJianZhuUnit  {
        // 建筑id
                id ?:number
        // 建筑名字
                name ?:string
    }
// 忍蛙数据
    interface RenWaDataUnit  {
        // 忍蛙类型
                type ?:number
        // 名字
                name ?:string
        // 形象
                image ?:string
        // 忍蛙等级
                renWaLevel ?:number
        // 消耗列表 激活或升级 多个是突破
                costList ?:Array<JiangliInfoUnit>
        // 当前属性列表
                currShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
                yuLanShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 是不是突破
                isTuPo ?:boolean
        // 是不是升到满级了
                isFull ?:boolean
    }
// 忍蛙type数据
    interface RenWaTypeDataUnit  {
        // 忍蛙类型
                type ?:number
        // 头像
                icon ?:string
        // 品质
                pinZhi ?:number
        // 红点
                redPoint ?:boolean
    }
// Rogoulike地下城数据
    interface RogoulikeInfoUnit  {
        // 章节Id xiaoguanzhangjie表Id
                chapterDefId ?:number
        // 章节名称
                name ?:string
        // 开始关卡 xiaoguan表Id
                startDefId ?:number
        // 结束关卡 xiaoguan表Id
                endDefId ?:number
        // 当前关卡id
                currentDefId ?:number
        // 层数
                numberOfDefId ?:number
        // 地图类型
                mapType ?:number
        // 普通关卡信息列表
                commonDefGugeInfoList ?:Array<CommonDefGugeInfoUnit>
        // 小怪列表数据
                guaiwuList ?:Array<CommonGuaiwuInfoUnit>
        // diy字符串数据，前端存入的json列表。
                diyStringList ?:Array<DiyStringUnit>
        // 地图额外图块范围开始
                dituFuzaduKaishi ?:number
        // 地图额外图块范围结束
                dituFuzaduJieshu ?:number
        // 每层出下的宝箱个数
                baoxiangShu ?:number
        // 每层出下的罐子个数
                guanziShu ?:number
        // 本章所有层的秘境数据
                cengMysteryList ?:Array<RogoulikeCengMysteryUnit>
        // 本层出现的偶遇金猪个数
                goldenPigCount ?:number
        // 玩家当前的攻击和血量 hp:113, attack:110
                propertyList ?:Array<HeroPropertyInfoUnit>
        // 普通层的额外点击元素集合列表
                extraElementList ?:Array<RogoulikeExtraElementInfoUnit>
        // 最终层的额外点击元素集合列表
                lastCengExtraElementList ?:Array<RogoulikeExtraElementInfoUnit>
        // 额外点击元素的奖励类型和Id集合列表，重复的奖励类型+id将会自动merge
                allExtraElementRewardList ?:Array<JiangliInfoUnit>
        // 地牢
                dungeon ?:msgObj.RogoulikeCengDungeonUnit
        // 禁止进入时间戳，单位：毫秒
                forbiddenTime ?:number
        // xiaoguanzhangjie表新增projectId字段，填写project表的ID
                projectDefId ?:number
        // 是否能碾压
                isCanCompleteSuppression ?:boolean
    }
// Rogoulike关卡中骨骼信息数据
    interface CommonDefGugeInfoUnit  {
        // 关卡 xiaoguan表Id
                defId ?:number
        // boss形象
                bossGugeId ?:number
        // 隐藏怪物数据
                hideGuaiwu ?:msgObj.CommonGuaiwuInfoUnit
        // boss名字
                name ?:string
    }
// Rogoulike关卡中的怪物数据
    interface CommonGuaiwuInfoUnit  {
        // 1小怪；2隐藏精英怪
                type ?:number
        // 怪物唯一id
                defId ?:number
        // 怪物形象id
                guaiwuGugeId ?:number
        // 怪物表数据，目前只有小怪才赋值，其他怪没有赋值，请注意！
                guaiwuDef ?:msgObj.GuaiwuDefUnit
        // 怪物名字
                name ?:string
    }
// Rogoulike中的diy字符串数据
    interface DiyStringUnit  {
        // 字段名
                fieldName ?:string
        // 字段值
                stringValue ?:string
    }
// Rogoulike中的额外点击元素信息
    interface RogoulikeExtraElementInfoUnit  {
        // 1、小怪；2：宝箱；3、罐子; 4、偶遇金猪；5：加血buff；6、加攻buff；
                type ?:number
        // 对应层的小怪id和奖励信息列表
                elementList ?:Array<RogoulikeXiaoguaiBaoxiangInfoUnit>
    }
// Rogoulike中的小怪或宝箱奖励信息
    interface RogoulikeXiaoguaiBaoxiangInfoUnit  {
        // 本章节中id唯一
                id ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
        // 怪物当前的攻击和血量 hp:113, attack:110
                propertyList ?:Array<HeroPropertyInfoUnit>
    }
// Rogoulike章节信息
    interface RogoulikeChapterInfoUnit  {
        // 章节id
                id ?:number
        // 章节名称
                name ?:string
        // 描述
                desc ?:string
        // 可能获得的奖励列表
                rewardList ?:Array<JiangliInfoUnit>
        // 领主大Boss，怪物信息
                guaiwu ?:msgObj.GuaiwuDefUnit
        // Boss战斗力
                guaiwuFight ?:number
    }
// Rogoulike每层的秘境
    interface RogoulikeCengMysteryUnit  {
        // 第几层
                ceng ?:number
        // 秘境列表
                mysteryList ?:Array<RogoulikeMysteryUnit>
    }
// Rogoulike秘境信息
    interface RogoulikeMysteryUnit  {
        // 秘境唯一Id，前后端发奖用
                id ?:number
        // 秘境类型1 大树，打爆；2 开门出怪打；3 金猪；4 一个BOSS
                type ?:number
        // 怪物骨骼Id，只有3和4类型才赋值
                guaiwuGuge ?:number
        // 怪物坐骑Id，只有3和4类型才赋值
                zuoqi ?:number
    }
// Rogoulike神魔神技扩展数据
    interface RogoulikeHeroMagicalSkillExtendUnit  {
        // 上阵神魔DbId
                heroDbId ?:number
        // 神技强度
                heroFightIncrease ?:number
        // 神技协作效果列表
                cooperateList ?:Array<MagicalSkillCooperateUnit>
    }
// Rogoulike神技结算信息
    interface RogoulikeMagicalSkillSettlementUnit  {
        // 神技卡品质
                rank ?:number
        // 该品质的神技卡数量
                magicalSkillCount ?:number
        // 金币奖励
                rewardInfo ?:msgObj.JiangliInfoUnit
    }
// Rogoulike该层的地牢
    interface RogoulikeCengDungeonUnit  {
        // 地牢Id
                id ?:number
        // 地牢状态 1 没有结束 2 需要重置 3.重置后没有结束 4.已关闭
                mapStatus ?:number
        // 入口位置，对应xiaoguan关卡
                commonInstanceDefId ?:number
        // 小怪列表数据
                guaiwuList ?:Array<CommonGuaiwuInfoUnit>
        // 地牢层级数据列表
                layerList ?:Array<DungeonLayerUnit>
        // 重置消耗的金券
                resetCost ?:msgObj.JiangliInfoUnit
    }
// Rogoulike地牢层级数据
    interface DungeonLayerUnit  {
        // 地牢层级：1……N
                layerNum ?:number
        // 普通层的额外点击元素集合列表
                extraElementList ?:Array<RogoulikeExtraElementInfoUnit>
        // 地牢怪物
                monster ?:msgObj.DungeonLayerMonsterUnit
    }
// Rogoulike地牢怪物信息
    interface DungeonLayerMonsterUnit  {
        // Def ID
                id ?:number
        // 关卡名称
                name ?:string
        // 怪物表数据
                guaiwuDef ?:msgObj.GuaiwuDefUnit
    }
// 主角某子系统皮肤数据
    interface RoleSubSystemPifuUnit  {
        // defId,升星，激活，使用应该需要用到 ，也可以用别的，主要是区分不同皮肤的，保证唯一性就行
                defId ?:number
        // 名称
                name ?:string
        // 骨骼id
                guge ?:number
        // 皮肤背景
                banner ?:string
        // 字体颜色 4 紫色 5 金色 7 红色 10彩色
                rank ?:number
        // 三种状态，0未点亮，1可点亮; 2点亮。
                status ?:number
        // 激活消耗
                activeCostList ?:Array<JiangliInfoUnit>
        // 升星消耗
                starLevelUpCostList ?:Array<JiangliInfoUnit>
        // 是否使用中
                isInUse ?:boolean
        // 是否星级已满
                isFullStarLevel ?:boolean
        // 当前星级
                starLevel ?:number
    }
// 主角某子系统皮肤数据
    interface RoleSubSystemPifuTypeGroupUnit  {
        // 品质类型页签【1 SR；2 SSR；3 SSSR；4 UR】
                type ?:number
        // 该类型下的皮肤列表
                pifuList ?:Array<RoleSubSystemPifuUnit>
    }
// 跨服某子系统皮肤数据
    interface PlayerCardSubSystemPifuUnit  {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
                systemType ?:number
        // 全队属性加成
                propertyList ?:Array<PropertyInfoUnit>
        // 全部的分组列表
                typeGroupList ?:Array<RoleSubSystemPifuTypeGroupUnit>
    }
// 坐标信息
    interface PointUnit  {
        // X
                x ?:number
        // Y
                y ?:number
    }
// 角色形象及坐标路线
    interface PlayerLocationInfoUnit  {
        // 场景中唯一id
                intId ?:number
        // 移动玩家ID
                playerId ?:number
        // 移动玩家昵称
                playerName ?:string
        // 移动玩家头像
                playerTouxiangId ?:number
        // 移动玩家服装形象ID
                playerDressId ?:number
        // 移动玩家公会ID,如果已经入会的话
                playerGongHuiId ?:number
        // 移动玩家公会名称,如果已经入会的话
                playerGongHuiName ?:string
        // 玩家战斗力
                fight ?:number
        // 玩家当前坐标
                location ?:msgObj.PointUnit
        // 开始移动时间, 如果没有移动路径,则不传
                startTime ?:number
        // 移动路径, 如果没有移动路径,则不传
                pathList ?:Array<PointUnit>
        // 方向
                directionEnd ?:number
        // VIP等级
                vip ?:number
        // 收益,玩家可能掉落的额外货币,其他人可拾取
                lostMoney ?:number
        // 玩家状态, 6表示战斗中, 7表示已死亡
                playerStatus ?:number
        // 战斗动画结束时间,单位:秒,只有战斗中才传递此参数
                fightFinishTime ?:number
        // 战斗后剩余血量, 0~10000,只有战斗中才传递此参数
                hpValue ?:number
        // 战斗对手ID,只有战斗中才传递此参数
                fightTargetId ?:number
        // 移动速度,既每秒移动距离
                speed ?:number
        // 加速状态结束时间,可能为0,表示未在加速状态,单位:毫秒
                speedUpEndTime ?:number
        // 玩家区服
                playerServerNum ?:number
        // 击杀人数
                killNum ?:number
        // 是否是同一公会的人
                isSameGongHui ?:boolean
        // 公会徽章
                gongHuiHuiZhang ?:string
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 怪物形象及坐标路线
    interface MonsterLocationInfoUnit  {
        // 场景中唯一id
                intId ?:number
        // 怪物模型id
                monsterId ?:number
        // 怪物类型,具体数值待定
                monsterType ?:number
        // 怪物昵称
                monsterName ?:string
        // 怪物战斗力
                fight ?:number
        // 怪物当前坐标
                location ?:msgObj.PointUnit
        // 开始移动时间, 如果没有移动路径,则不传
                startTime ?:number
        // 移动路径, 如果没有移动路径,则不传
                pathList ?:Array<PointUnit>
        // 方向
                directionEnd ?:number
        // 收益,玩家可能掉落的额外货币,其他人可拾取
                lostMoney ?:number
        // 怪物状态, 6表示战斗中, 7表示已死亡
                playerStatus ?:number
        // 战斗动画结束时间,单位:秒,只有战斗中才传递此参数
                fightFinishTime ?:number
        // 战斗后剩余血量, 0~10000,只有战斗中才传递此参数
                hpValue ?:number
        // 战斗对手ID,只有战斗中才传递此参数
                fightTargetId ?:number
        // 移动速度,既每秒移动距离
                speed ?:number
        // 怪物骨骼形象数据
                guaiwuGuge ?:msgObj.GugeUnit
    }
// 地图层级信息
    interface SceneLevelInfoUnit  {
        // 起始等级
                levelStart ?:number
        // 结束等级
                levelEnd ?:number
        // 10秒获得经验值
                exp ?:number
        // 名称
                name ?:string
        // 星星数量
                star ?:number
        // 描述
                enterDesc ?:string
    }
// 个人PVP排行数据
    interface SoloPVPPlayerInfoUnit  {
        // 排名
                index ?:number
        // 玩家ID
                playerId ?:number
        // 玩家昵称
                playerName ?:string
        // 对战数量
                fightSize ?:number
        // 胜场
                winSize ?:number
        // 积分
                score ?:number
        // 玩家奖励比例,万分比
                playerValue ?:number
        // 活动期间获得的累积经验值,只发自己的,不发排行列表玩家的
                exp ?:number
        // 玩家区服
                playerServerNo ?:number
    }
// 个人PVP战斗记录
    interface SoloPVPRecordInfoUnit  {
        // 对手ID
                targetId ?:number
        // 对手昵称
                targetName ?:string
        // 是否胜利,否则战败
                isWin ?:boolean
        // 积分数量
                changeScore ?:number
    }
// 战场信息
    interface GongHuiZhanSceneInfoUnit  {
        // 战场编号
                sceneNum ?:number
        // 战场最大人数
                sceneMaxHuman ?:number
        // 战场当前人数
                sceneHuman ?:number
        // 本族人数
                gongHuiHuman ?:number
        // 最强公会名字
                gongHuiName ?:string
    }
// 箱子信息
    interface BoxInfoUnit  {
        // 场景中唯一id
                intId ?:number
        // 箱子数据id
                boxDefId ?:number
        // 玩家当前坐标
                location ?:msgObj.PointUnit
    }
// 陷阱信息
    interface TrapInfoUnit  {
        // 场景中唯一id
                intId ?:number
        // 陷阱数据id
                trapDefId ?:number
        // 玩家当前坐标
                location ?:msgObj.PointUnit
    }
// 快速反应事件选项信息
    interface QuickReactionOptionUnit  {
        // 快速反应事件表id
                defId ?:number
        // 描述
                desc ?:string
        // 边框品质
                rank ?:number
        // 图标
                icon ?:string
    }
// 积分商店商品数据
    interface ScoreShopGoodsInfoUnit  {
        // 商品Email数据表dbID，数据表主Key
                goodsDBID ?:number
        // 商品类型
                itemType ?:number
        // 商品Id
                itemId ?:number
        // 商品数量
                itemCount ?:number
        // 需要的积分
                needScore ?:number
        // 状态0可以买   1买过了
                status ?:number
        // 消耗类型
                costType ?:number
        // 打折状态（0不打折 1一折 5五折）
                rebate ?:number
        // 显示顺序
                showIndex ?:number
    }
// 神宠数据
    interface ShenChongInfoUnit  {
        // 神宠id
                shenChongId ?:number
        // 神宠静态ID
                defId ?:number
    }
// 神器属性
    interface ShenQiShuXingInfoUnit  {
        // 
                xiLianDefId ?:number
        // 
                value ?:number
        // 
                index ?:number
        // 0没锁    1 锁了
                suoDing ?:number
    }
// 神器数据
    interface ShenQiInfoUnit  {
        // 数据表dbID，数据表主Key
                id ?:number
        // 
                defId ?:number
        // 等级
                level ?:number
        // 等级
                exp ?:number
        // 穿戴的武将的dbId
                heroDbId ?:number
        // 属性
                shuXingList ?:Array<ShenQiShuXingInfoUnit>
    }
// 神器升级所需材料
    interface ShenQiXiaoHaoInfoUnit  {
        // 1装备   2精灵  3道具
                type ?:number
        // 
                dbId ?:number
        // 
                count ?:number
    }
// 出海地区数据
    interface ChuhaiAreaInfoUnit  {
        // Def ID
                id ?:number
        // 地区名称
                name ?:string
        // 地区图标
                image ?:string
        // 航海距离
                distance ?:number
        // 掉落道具ID列表
                lootList ?:Array<number>
    }
// 船只数据
    interface ShipInfoUnit  {
        // 船只编号
                shipNum ?:number
        // 出发时间
                startTime ?:number
        // 船只状态 0.就绪 1.游历中 2.冷却中
                shipState ?:number
        // 冷却结束时间
                cdTime ?:number
        // 掉落道具
                lootList ?:Array<JiangliInfoUnit>
        // 掉落神器
                shenqiList ?:Array<JiangliInfoUnit>
        // 船只属性
                shipProp ?:msgObj.ShipPropUnit
        // 起始港口名称
                portName ?:string
        // 目标港口名称
                targetPortName ?:string
        // 船只名称
                shipName ?:string
    }
// 掉落道具信息
    interface LootInfoUnit  {
        // 道具def Id
                defId ?:number
        // 道具数量
                count ?:number
    }
// 船只属性
    interface ShipPropUnit  {
        // 等级
                level ?:number
        // 航海距离
                distance ?:number
        // 船只载量
                load ?:number
        // 幸运值
                luck ?:number
        // 全队攻击
                attack ?:number
        // 全队防御
                defence ?:number
        // 全队生命
                hp ?:number
    }
// 航海日志
    interface ShipLogInfoUnit  {
        // 出海时间
                time ?:number
        // 出海次数
                num ?:number
        // 航海地区
                areaName ?:string
        // 掉落道具
                lootList ?:Array<LootInfoUnit>
        // 专属神器
                zhuanshushenqi ?:string
        // 掉落神器
                shenqiList ?:Array<LootInfoUnit>
    }
// 商品数据
    interface ShopInfoUnit  {
        // 商品ID，静态表主Key
                shopID ?:number
        // 玩家购买的数量
                count ?:number
        // 商店类型：1，普通商店；2，全服商店；3、世界BOSS商店
                type ?:number
        // 打折购买次数
                daZheCount ?:number
    }
// 黑市商品数据
    interface HSGoodsInfoUnit  {
        // 黑市商品ID
                goodsId ?:number
        // 黑市商品ID，静态表主Key
                goodsDefId ?:number
        // 玩家已购买的数量
                count ?:number
    }
// 商城商品数据
    interface ShangChengGoodsUnit  {
        // 商品ID
                defId ?:number
        // 货币信息
                moneyInfo ?:msgObj.JiangliInfoUnit
        // 商品信息
                goodsInfo ?:msgObj.JiangliInfoUnit
        // 是否能购买
                isCanBuy ?:boolean
        // 需要的vip等级
                vipNeed ?:number
        // 领取次数上限 0为无限制
                ciShu ?:number
        // 已经购买次数
                hasBuyCount ?:number
        // 排序
                paiXu ?:number
        // 限制类型
                gouMaiXianZhi ?:number
        // 限制条件
                xianZhiTiaoJian ?:number
        // 每日重置 0不重置 1重置
                meiRiChongZhi ?:number
        // 说明
                shuoMing ?:string
        // 大于0开服活动
                kaiFuHuoDong ?:number
        // 开服活动持续时间
                chiXuShiJian ?:number
        // 开始时间
                beginTime ?:number
        // 结束时间
                endTime ?:number
        // 是否显示到期时间
                shiFouShowTime ?:number
        // 打折数
                zheKouNum ?:number
    }
// 商城商品数据
    interface NewShopGoodsUnit  {
        // 商品DBID
                dbId ?:number
        // 商品DefID
                defId ?:number
        // 货币信息
                moneyInfo ?:msgObj.JiangliInfoUnit
        // 商品信息
                goodsInfo ?:msgObj.JiangliInfoUnit
        // 是否能购买
                isCanBuy ?:boolean
        // 需要的vip等级
                vipNeed ?:number
        // 领取次数上限 0为无限制
                ciShu ?:number
        // 已经购买次数
                hasBuyCount ?:number
        // 排序
                paiXu ?:number
        // 限制类型
                gouMaiXianZhi ?:number
        // 限制条件
                xianZhiTiaoJian ?:number
        // 每日重置 0不重置 1重置
                meiRiChongZhi ?:number
        // 说明
                shuoMing ?:string
        // 大于0开服活动
                kaiFuHuoDong ?:number
        // 开服活动持续时间
                chiXuShiJian ?:number
        // 开始时间
                beginTime ?:number
        // 结束时间
                endTime ?:number
        // 是否显示到期时间
                shiFouShowTime ?:number
        // 打折数
                zheKouNum ?:number
        // 对应的武将Id 0没有
                heroId ?:number
    }
// 商城状态
    interface ShangChengStatusUnit  {
        // 商城一级type
                shopType ?:number
        // 商城对应的页签
                tabList ?:Array<ShangChengTabUnit>
    }
// 商城页签
    interface ShangChengTabUnit  {
        // 商城二级type
                tabType ?:number
        // 功能ID
                projectId ?:number
        // 商城入口图标
                tabIcon ?:string
    }
// 商城策划数据
    interface ShopDefUnit  {
        // 商城二级type
                id ?:number
        // 排序
                paixu ?:number
        // 商品类型 1 正常价格 2 阶梯价格 
                type ?:number
        // 奖励类型 
                typeAward ?:number
        // 道具ID 
                daojuid ?:number
        // 道具数量 
                num ?:number
        // 商品名称 
                mingcheng ?:string
        // 活动类型 
                activityType ?:number
        // 商品简短说明 
                shuoming ?:string
        // 商店分类 1，钻石商城 2，金券商城 
                fenlei ?:number
        // 分类名称 
                leiming ?:string
        // 钻石价格 
                baoshi ?:number
        // 钻石折扣价 
                baoshizhekou ?:number
        // 金券价格 
                jinquan ?:number
        // 折扣开始时间戳 
                zhekoukaishi ?:number
        // 折扣结束时间戳 
                zhekoujieshu ?:number
        // 银两价格 
                yinliang ?:number
        // 每日限购 
                meirixiangou ?:number
        // 永久限购 
                yongjiuxiangou ?:number
        // 玩家等级 
                dengji ?:number
        // VIP等级 
                vip ?:number
        // 新道具标志 
                canshu1 ?:number
        // 上架状态 
                canshu2 ?:number
        // 保留参数3 
                canshu3 ?:number
        // 金券打折价格 
                jinquandazhejia ?:number
        // 每日折扣价购买上限 
                meiridazhegoumaishuliang ?:number
        // 第几次购买开始打折 
                zhekouqishicishu ?:number
        // shop表新增IsXianshi字段，0不显示；1显示 
                isXianshi ?:number
    }
// 商品数据
    interface DungeonShopGoodsUnit  {
        // 商品ID，dilaoshangdian表Id
                defId ?:number
        // 总的可购买次数
                totalCanBuyCount ?:number
        // 总的剩余购买次数
                totalLeftBuyCount ?:number
        // 每日的可购买次数
                dailyCanBuyCount ?:number
        // 每日的剩余购买次数
                dailyLeftBuyCount ?:number
        // 排序字段
                paixu ?:number
        // 商品
                item ?:msgObj.JiangliInfoUnit
        // 购买价格
                cost ?:msgObj.JiangliInfoUnit
    }
// 奖励数据
    interface ShouchongJiangliInfoUnit  {
        // ID，静态表主Key
                shouchongjiangliId ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
    }
// 双彩球奖励
    interface ShuangCaiQiuRewardInfoUnit  {
        // 奖励类型 47 是符文
                type ?:number
        // 符文id
                id ?:number
        // 数量
                count ?:number
    }
// type  value 这样的格式
    interface ShuXingInfoUnit  {
        // 分类 某些情况下对应策划表id
                type ?:number
        // 属性值。一元特价是为状态1未买；2已买（自动发奖）
                value ?:number
        // 扩展字段，备用。特殊用途特殊赋值
                extType ?:number
    }
// 属性
    interface NewShuXingInfoUnit  {
        // 属性分类 1 基础属性 2 高级属性 3 元素属性
                type ?:number
        // 属性名称
                detailList ?:Array<NewShuXingInfoDetailUnit>
    }
// 属性详情
    interface NewShuXingInfoDetailUnit  {
        // 属性类型
                shuXingType ?:number
        // 属性名称
                shuXingName ?:string
        // 排序
                paiXu ?:number
        // 描述
                desc ?:string
    }
// 技能书数据
    interface SkillBookUnit  {
        // 技能书Entity Db Id
                dbId ?:number
        // 技能书Def Id
                defId ?:number
        // 星级
                star ?:number
        // 对应星级增益描述字段
                desc ?:string
    }
// 技能书协作数据
    interface SkillBookXiezuoUnit  {
        // 技能书Def Id
                defId ?:number
        // 文字描述
                words ?:string
        // 是否激活
                isActive ?:boolean
    }
// 技能书协作文本数据
    interface SkillBookXiezuoBeizhuUnit  {
        // beizhu表Id
                defId ?:number
        // 是否激活
                isActive ?:boolean
    }
// 服务器状态数据
    interface ServerInfoUnit  {
        // 服务器ID
                id ?:number
        // 服务器名字
                name ?:string
        // 是否新服
                isNew ?:boolean
        // 服务器状态:-1 维护中，0 正常，1 拥挤，2 超载
                status ?:number
        // 在线总人数
                onlineUserCount ?:number
        // 区服编号
                num ?:number
        // 注册总人数
                regUserCount ?:number
        // 日活人数
                dailyLiveUserCount ?:number
        // 服务器IP
                coreHost ?:string
        // 服务器对外端口号
                corePort ?:number
        // 开服时间
                openTime ?:number
        // 是否合过服
                isMerge ?:boolean
        // 是否已经开服
                isOpen ?:boolean
    }
// 精灵属性数据
    interface HeroPropInfoUnit  {
        // 精灵ID
                heroID ?:number
        // 武将ID
                heroDefID ?:number
        // 等级
                level ?:number
        // wujiangleveljinjie表Index，从0开始，每进阶一次自动+1
                wujiangleveljinjieIndex ?:number
        // 培养等级
                levelGuanzhi ?:number
        // 生命值
                hp ?:number
        // 攻击值
                attack ?:number
        // 防御值
                defense ?:number
        // 速度
                speed ?:number
        // 命中
                aim ?:number
        // 闪避
                dodge ?:number
        // 暴击
                critical ?:number
        // 免爆
                unCritical ?:number
        // 伤害加深
                amplifyDamage ?:number
        // 伤害减免
                weaken ?:number
        // 吸血
                stealHP ?:number
        // 战斗力
                fight ?:number
        // 火克制
                fireRestraint ?:number
        // 水克制 
                waterRestraint ?:number
        // 草克制 
                grassRestraint ?:number
        // 光克制
                lightRestraint ?:number
        // 暗克制
                darkRestraint ?:number
        // 火抗性
                fireUnRestraint ?:number
        // 水抗性
                waterUnRestraint ?:number
        // 草抗性
                grassUnRestraint ?:number
        // 光抗性
                lightUnRestraint ?:number
        // 暗抗性
                darkUnRestraint ?:number
        // NewBuff附加额外Buff
                newBuffIdList ?:Array<number>
        // 技能伤害
                skillDamagel ?:number
        // 概率增加弹射次数
                attackLaunchProbability ?:number
        // 暴击伤害
                criticalDamagel ?:number
        // 眩晕几率
                vertigoProbability ?:number
        // 反伤
                antiInjury ?:number
        // 混乱技能
                confusionValue ?:number
        // 破攻
                cutAttack ?:number
        // 破防
                cutDefense ?:number
        // 破血
                cutHp ?:number
        // 附伤
                extraDamage ?:number
        // 可免伤次数,挨打一次未死亡的才触发
                noDamageUnderAttack ?:number
        // 抗眩晕
                noVertigo ?:number
        // 抗混乱
                noConfusion ?:number
        // 抗混乱
                fightBack ?:number
        // 穿刺时眩晕几率
                vertigoPolearm ?:number
        // 净化
                cleansing ?:number
        // 诅咒
                curse ?:number
        // 先手
                initiation ?:number
        // 
                otherHeroProp ?:Array<HeroPropertyInfoUnit>
        // 皮肤
                piFuId ?:number
        // 连接宠物id
                heJiHeroDefId ?:number
        // 追伤
                catchDamage ?:number
        // skillChange附加额外Buff
                skillChangeIdList ?:Array<number>
        // 装备Define Id List
                zhuangbeiDefIdList ?:Array<number>
        // 同系的神技个数
                magicalSkillHeroTypeList ?:Array<MagicalSkillHeroTypeInfoUnit>
    }
// 遭遇战玩家技能书数据
    interface PlayerSkillBookInfoUnit  {
        // 宠物位置编号,位置编号 1-3
                index ?:number
        // 等级
                level ?:number
        // 星级
                star ?:number
        // 上阵技能书Id
                hejiJinengDefId ?:number
        // 上阵的宠物Db Id List
                heroIdList ?:Array<number>
    }
// 遭遇战玩家数据
    interface PlayerInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家创建角色时,所属服务器编号
                sourceServerNum ?:number
        // 合服后的实际服务器ID,合服前,跟创角服务器相同
                serverId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家头像ID
                playerTouxiangId ?:number
        // 玩家坐骑ID
                playerZuoqi ?:number
        // 玩家翅膀ID
                playerChibang ?:number
        // 玩家等级
                playerLevel ?:number
        // 玩家VIP等级
                playerVipLevel ?:number
        // 玩家战斗力
                playerFight ?:number
        // 竞技场排名
                paihangBW ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position11 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position12 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position13 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position14 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position15 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
                position16 ?:number
        // 精灵数据
                heroList ?:Array<HeroPropInfoUnit>
        // 建号选择宠物的defId
                xuanZeChongWuDefId ?:number
        // NewBuff附加额外Buff
                newBuffIdList ?:Array<number>
        // 皮肤形象
                piFuDefIdList ?:Array<number>
        // 称号
                chenghaoId ?:number
        // 神宠ID
                shenchongDefId ?:number
        // 神宠等级
                shenchongLevel ?:number
        // 神宠使用的技能ID
                shenchongSkillId ?:number
        // 神宠属性
                shenchongPropValues ?:string
        // 玩家形象 地图跑动小人的男女
                xingXiang ?:number
        // 跟随宠defId
                followPetDefId ?:number
        // 跟随宠大小
                followPetSize ?:number
        // 是否为机器人
                isRobot ?:boolean
        // 忍蛙ID列表
                renWaList ?:Array<number>
        // 跟随宠皮肤Id
                followPetPiFuID ?:number
        // 玩家的公会id
                gongHuiId ?:number
        // 洛托姆全等级
                luotuomu ?:number
        // 洛托姆(皮神之力)进阶外显对应的等级
                luotuomuDisplay ?:number
        // 排位赛段位
                paiHangPPId ?:number
        // 排位赛等级段
                levelPart ?:number
        // 公会徽章 边框,底图,标志
                gongHuiHuiZhang ?:string
        // 玩家贵族等级
                playerNobleLevel ?:number
        // 光环
                guangHuan ?:number
        // 公会名称
                gongHuiName ?:string
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
        // 威名显示unit
                weimingInfo ?:msgObj.WeimingShowInfoUnit
        // 合击技-技能书
                skillBookList ?:Array<PlayerSkillBookInfoUnit>
        // 渠道UID
                openId ?:string
        // 渠道
                channelCode ?:string
        // 子渠道
                subChannel ?:string
        // 聊天展示特权表Id列表
                liaotianZhanshiDefIdList ?:Array<number>
        // 聊天框Id
                liaotiankuangDefId ?:number
    }
// 排位赛战斗记录数据
    interface RankingRecordInfoUnit  {
        // DBID
                recordDBId ?:number
        // 对手所在服务器ID
                enemySourceServerId ?:number
        // 对手的Name
                enemyName ?:string
        // 对手的头像ID
                enemyTouxiangId ?:number
        // 对手的战斗力
                enemyFight ?:number
        // 返回码：1我方获胜，2我方失败
                win ?:number
        // 战区点数变更情况
                pointChange ?:number
        // 对手的个人形象数据
                enemyPhoto ?:msgObj.PlayerPhotoUnit
    }
// 战区玩家数据
    interface WarZonePlayerUnit  {
        // Player ID
                playerId ?:number
        // 玩家Name
                playerName ?:string
        // 胜场
                sizeWin ?:number
        // 晋升战斗情况: 1我方获胜，2我方失败。非晋升情况不传
                fightRecords ?:Array<number>
        // 战区点数(胜点),晋升时均不传
                point ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否是豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 公会徽章 边框,底图,标志
                gongHuiHuiZhang ?:string
    }
// 战区玩家精灵预览数据
    interface WarZonePlayerHeroViewUnit  {
        // 精灵Definition ID
                heroId ?:number
        // 精灵等级
                level ?:number
        // 官职(培养)等级
                levelGuanZhi ?:number
        // 皮肤Id
                piFuId ?:number
    }
// 排位赛玩家结算数据，结算用
    interface RankingPlayerInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 排行Def Id
                positionId ?:number
        // 战区点数
                score ?:number
        // 隐藏积分
                scoreHide ?:number
        // 胜点
                point ?:number
    }
// 机器人宠物数据
    interface RobotHeroLevelInfoUnit  {
        // ID
                id ?:number
        // 宠物 Id
                heroId ?:number
        // 宠物等级
                level ?:number
        // 宠物培养等级
                levelGuanzhi ?:number
        // wujiangleveljinjie表Index，从0开始，每进阶一次自动+1
                wujiangleveljinjieIndex ?:number
    }
// 机器人宠物数据
    interface RobotHeroPropInfoUnit  {
        // ID
                id ?:number
        // 生命值
                hp ?:number
        // 攻击值
                attack ?:number
        // 防御值
                defense ?:number
        // 速度
                speed ?:number
        // 命中
                aim ?:number
        // 闪避
                dodge ?:number
        // 暴击
                critical ?:number
        // 免爆
                unCritical ?:number
        // 伤害加深
                amplifyDamage ?:number
        // 伤害减免
                weaken ?:number
        // 吸血
                stealHP ?:number
        // 战斗力
                fight ?:number
    }
// 玩家id和排位赛名词
    interface PlayerIdAndPaiWeiSaiMingCiInfoUnit  {
        // 
                playerId ?:number
        // 
                mingCi ?:number
    }
// 排位赛排行数据
    interface RankingPaiHangPlayerUnit  {
        // Player ID
                playerId ?:number
        // 玩家Name
                playerName ?:string
        // vip等级
                vipLevel ?:number
        // 战斗力
                playerFight ?:number
        // 胜场
                sizeWin ?:number
        // 服务器id
                serverNum ?:number
        // 战区点数(胜点),晋升时均不传
                point ?:number
        // 位置
                position ?:number
        // 蓝钻等级
                lanZuanLevel ?:number
        // 是否是年费蓝钻
                isLanZuanNianFei ?:boolean
        // 是否是豪华蓝钻
                isLanZuanHaoHua ?:boolean
        // 玩家头像ID
                playerTouxiangId ?:number
        // 皮神进阶外显皮肤
                displayLevel ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 排位赛排行数据
    interface UnitedServerInfoUnit  {
        // ID
                id ?:number
        // 活动类型: 1,PVP
                type ?:number
        // 联合组名称
                name ?:string
        // 描述
                desc ?:string
        // 主服务器ID
                masterServerId ?:number
        // 被合并的服务器ID
                servers ?:string
        // 显示联合区服ID
                isServerIdShow ?:boolean
        // 起始的服务器编号
                serverFrom ?:number
        // 截止的服务器编号
                serverTo ?:number
    }
// 单场比赛结果
    interface ZhanDuiPlayerFightResultUnit  {
        // 进攻方playerId
                playerId ?:number
        // 防守方playerId
                enemyPlayerId ?:number
        // 是否进攻方胜利
                isWin ?:boolean
        // 几强赛
                stage ?:number
        // 场次
                chang ?:number
        // 战斗记录
                fightJson ?:string
    }
// 每一轮战队的支持总金额
    interface ZhanDuiZhiChiLogUnit  {
        // 战队id
                zhanDuiId ?:number
        // 战队名字
                zhanDuiName ?:string
        // 战队所属服务器NO
                zhanDuiServerNo ?:number
        // 钻石
                zuanshi ?:number
        // 金卷
                jinjuan ?:number
        // 历史记录押注数量
                historyBetCount ?:number
    }
// 玩家支持
    interface PlayerZhiChiLogUnit  {
        // 战队id
                zhanDuiId ?:number
        // 战队名字
                zhanDuiName ?:string
        // 战队所属服务器NO
                zhanDuiServerNo ?:number
        // 钻石
                zuanshi ?:number
        // 金卷
                jinjuan ?:number
        // 钻石
                addzuanshi ?:number
        // 金卷
                addjinjuan ?:number
    }
// 简化精灵
    interface SimHeroInfoUnit  {
        // dbid
                heroDBId ?:number
        // 阵中位置
                position ?:number
        // wujiangId
                heroId ?:number
        // 等级
                level ?:number
        // 战力
                zhanli ?:string
        // 是否红字 1 是 0否
                isRed ?:number
        // 星级
                star ?:number
    }
// 战队赛阵型
    interface ZDSZhenXingInfoUnit  {
        // 神宠ID
                shenchongDefId ?:number
        // 神宠等级
                shenchongLevel ?:number
        // 忍蛙id列表
                renwaIdList ?:Array<number>
        // 总战力
                zhanli ?:string
        // 阵上精灵
                zhenXing ?:Array<SimHeroInfoUnit>
    }
// 跨服砸金蛋排行数据
    interface StateZaJinDanRankingInfoUnit  {
        // 服务器id
                serverId ?:number
        // 服务器name
                serverName ?:string
        // 玩家id
                playerId ?:number
        // 积分
                score ?:number
        // 玩家名字
                playerName ?:string
        // playerIco
                playerIco ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 神技个数
    interface MagicalSkillHeroTypeInfoUnit  {
        // 系别，神魔系别
                type ?:number
        // 数量
                count ?:number
    }
// 探索副本数据
    interface TanSuoFuBenUnit  {
        // 副本类型
                fuBenType ?:number
        // 副本位置
                index ?:number
        // 出生点
                bornPoint ?:msgObj.PointUnit
        // boss位置
                bossPoint ?:msgObj.PointUnit
        // 边界点位置
                bianJiePointList ?:Array<PointUnit>
        // 副本名字
                name ?:string
        // 开启等级
                openLevel ?:number
        // 开启条件
                openTiaoJian ?:msgObj.JiangliInfoUnit
        // 是否开启
                isOpen ?:boolean
        // 是否通关
                isTongGuan ?:boolean
        // 推荐战力
                tuiJianZhanLi ?:number
        // 头像和名称
                map ?:string
        // boss对应的武将Id
                bossId ?:number
        // boss对应的武将等级
                bossLevel ?:number
        // 免费奖励数据
                freeJiangLiList ?:Array<TanSuoFuBenRewardUnit>
        // boss采集奖励数据
                bossJiangLiList ?:Array<TanSuoFuBenRewardUnit>
        // 战斗掉落奖励数据
                diaoLuoJiangLiList ?:Array<JiangliInfoUnit>
        // 协作掉落奖励数据
                xieZuoJiangLiList ?:Array<JiangliInfoUnit>
        // 所需组队人数
                zuDuiRenShuLimit ?:number
    }
// 探索副本奖励数据
    interface TanSuoFuBenRewardUnit  {
        // 奖励内容
                jiangLiInfo ?:msgObj.JiangliInfoUnit
        // 奖励DbId
                jiangLiDbId ?:number
        // 坐标
                point ?:msgObj.PointUnit
        // 单价
                danJia ?:msgObj.JiangliInfoUnit
    }
// 探索副本房间数据
    interface TanSuoFuBenRoomInfoUnit  {
        // 房间号
                roomId ?:number
        // 副本类型
                fuBenType ?:number
        // boss对应的武将Id
                bossId ?:number
        // boss等级
                bossLevel ?:number
        // 推荐战力
                tuiJianZhanLi ?:number
        // 房间号
                personList ?:Array<GongHuiFuBenRoomPersonUnit>
        // 战力限制
                zhanLiLimit ?:number
        // 掉落奖励
                diaoLuoJiangLiList ?:Array<JiangliInfoUnit>
        // 协作奖励
                xieZuoJiangLiList ?:Array<JiangliInfoUnit>
        // 协作总次数
                xieZuoTotal ?:number
        // 剩余协作收益次数
                xieZuoLeft ?:number
        // 所需组队人数
                zuDuiRenShuLimit ?:number
        // 上次邀请时间戳
                lastInviteTime ?:number
    }
// 探险精灵信息
    interface TanXianHeroInfoUnit  {
        // dbId 上下阵用
                dbId ?:number
        // 探险精灵ID
                heroId ?:number
        // 是否上阵
                isInLine ?:boolean
        // 探险精灵等级
                level ?:number
        // 探险精灵表的defId
                tanxianjignlingleveldefId ?:number
    }
// 奖励数据
    interface TanXianLiangLiShowInfoUnit  {
        // ID，静态表主Key
                id ?:number
        // 1: 经验，2：武将经验 3：银两 4：装备 5 武将 6 道具 7 阵法 8 宝石 9 战阵碎片 10 世界BOSS积分   11 匹配战积分
                type ?:number
        // 
                defId ?:number
        // 数量
                count ?:number
        // 还多长时间能领  单位  分钟
                leftTime ?:number
    }
// 探险中那些奖励属性
    interface TanXianShuXingInfoUnit  {
        // dbId 升级用的
                dbId ?:number
        // 奖励定义ID
                defId ?:number
        // 该属性对应的上阵精灵ID
                heroId ?:number
        // 是否锁定中
                isLock ?:boolean
        // 探险精灵表的defId
                tanxianjignlingleveldefId ?:number
    }
// 编队信息
    interface TeamInfoUnit  {
        // 0表示主阵容，1-10表示1-10编队
                index ?:number
        // 互斥关系：0表示主阵容，1表示1-N编队
                group ?:number
        // 精灵宠物列表
                heroList ?:Array<TeamHeroInfoUnit>
        // 技能书列表
                skillBookList ?:Array<TeamSkillBookInfoUnit>
    }
// 上阵精灵宠物
    interface TeamHeroInfoUnit  {
        // 宠物位置编号,位置编号 11-19 协作111-119
                index ?:number
        // 上阵宠物的DbId
                heroDbId ?:number
        // 装备List
                equipList ?:Array<number>
        // 神戒(职业神器)的DbId
                zhiyeShenqiDbId ?:number
        // 专属神器的DbId
                zhuanshuShenqiDbId ?:number
        // 单宠战斗力
                fight ?:number
        // 宠物属性列表
                propList ?:Array<HeroPropertyInfoUnit>
        // 是否显示装备样式
                isShowEquip ?:boolean
        // 宠物复杂红点列表（后端计算，目前已支持神技）
                redPointList ?:Array<BoolRedPointUnit>
        // 单宠本服排行
                rank ?:number
        // 神技卡List
                magicalSkillList ?:Array<number>
        // 神技协作效果列表
                magicalSkillCooperateList ?:Array<MagicalSkillCooperateUnit>
    }
// 上阵技能书信息
    interface TeamSkillBookInfoUnit  {
        // 宠物位置编号,位置编号 1-3
                index ?:number
        // 等级
                level ?:number
        // 上阵技能书的DbId
                skillBookDbId ?:number
        // 上阵的宠物Db Id List
                heroIdList ?:Array<number>
        // 技能书属性列表
                propList ?:Array<HeroPropertyInfoUnit>
        // 上阵技能书的星级描述
                starDesc ?:string
        // 升级所需消耗
                upgradeXiaohaoList ?:Array<JiangliInfoUnit>
        // 升星所需消耗
                starUpXiaohaoList ?:Array<JiangliInfoUnit>
        // 技能书协作文本列表
                xiezuoList ?:Array<SkillBookXiezuoBeizhuUnit>
        // 最高等级
                maxLevel ?:number
        // 最高星级
                maxStar ?:number
    }
// 装备数据
    interface TempBeibaoInfoUnit  {
        // 临时物品静态Id
                tempId ?:number
        // 物品数量
                count ?:number
        // 物品类型：1、道具；2、装备；3、战阵碎片；4、战阵；5、武将；
                type ?:number
    }
// 天空塔
    interface TianKongTaUnit  {
        // 章节Id
                zhangJieId ?:number
        // 战斗底图
                zhanDouMap ?:string
        // 怪物编号
                bossNum ?:number
        // 怪物名字
                bossName ?:string
        // 怪物对应的骨骼ID
                guaiwuguge ?:number
        // 是否打赢过
                isWin ?:boolean
        // 星星数
                starCount ?:number
        // 首通奖励列表
                FirstRewardList ?:Array<JiangliInfoUnit>
        // 是否领取首通奖励
                isHaveFirstReward ?:boolean
        // 通关奖励列表
                TongGuanRewardList ?:Array<JiangliInfoUnit>
        // 是否领取通关奖励
                isHaveTonGuanReward ?:boolean
        // 挑战条件领主关卡
                lingzhu ?:number
    }
// 天空塔星星奖励
    interface TianKongTaStarRewardUnit  {
        // 章节Id
                zhangJieId ?:number
        // 星星数
                star ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
        // 是否领取
                isGot ?:boolean
    }
// 天空塔星星排行
    interface TianKongTaPaiHangUnit  {
        // 排名
                position ?:number
        // 名称
                playerName ?:string
        // 星星数
                star ?:number
    }
// 跳动提示武将
    interface TiaoDongHeroInfoUnit  {
        // 精灵dbid
                heroDbId ?:number
        // 可以上阵的位置
                index ?:number
    }
// 跨服头像框数据
    interface PlayerCardTouxiangkuangUnit  {
        // 已经激活的Id列表
                defIdList ?:Array<number>
        // 当前使用的DefId
                currentDefId ?:number
    }
// 当前头衔信息
    interface TouxianInfoUnit  {
        // 头衔ID, 静态ID
                defId ?:number
        // 达到当前等级的时间
                completeTime ?:number
        // 数据状态：1、已达成；2、待晋升；3、下一个预览的头衔
                status ?:number
        // 晋升所需材料，只有status=2时才有值
                costList ?:Array<JiangliInfoUnit>
        // 当前属性信息
                propList ?:Array<PropertyInfoUnit>
        // 神魔对boss伤害属性
                bossDamageProp ?:msgObj.PropertyInfoUnit
        // 任务列表信息
                missionList ?:Array<TouxianMissionInfoUnit>
        // 是否最后一个头衔了
                isMax ?:boolean
        // 超越的玩家比例，万分比
                beyondPlayerRate ?:number
    }
// 头衔任务信息
    interface TouxianMissionInfoUnit  {
        // 条件分类：1战斗力条件；2任务条件
                type ?:number
        // 所需战斗力
                needFight ?:number
        // 任务表ID, 静态ID
                renwuDefId ?:number
        // 是否完成任务了
                isFinish ?:boolean
        // 完成任务的时间
                finishTime ?:number
        // 任务名称
                name ?:string
        // 跳转页面
                jump ?:string
        // 任务总的进度，条件值 改成long针对战力
                jindu ?:number
    }
// 跨服头衔数据
    interface PlayerCardTouxianUnit  {
        // 头衔信息
                touxianList ?:Array<TouxianInfoUnit>
    }
// 头衔光环数据
    interface TouxianGuanghuanInfoUnit  {
        // 光环ID, 静态ID
                defId ?:number
        // 当前光环描述
                desc ?:string
        // 数据状态：1、未激活；2、可以激活；3、已激活
                status ?:number
    }
// 跨服头衔光环数据
    interface PlayerCardTouxianGuanghuanUnit  {
        // 光环列表
                guanghuanList ?:Array<TouxianGuanghuanInfoUnit>
        // 总战力文字描述
                totalFightDesc ?:string
    }
// 小镇已解锁房屋信息
    interface TownRoomInfoOpenUnit  {
        // 房屋类型
                roomType ?:number
        // 建筑名称
                roomName ?:string
        // 建筑等级
                roomLevel ?:number
        // 房屋的图
                roomImage ?:string
        // 建筑状态
                roomStatus ?:number
        // 人物Id
                personId ?:number
        // 人物名称
                personName ?:string
        // 人物等级
                personLevel ?:number
        // 人物的头像
                personTouXiang ?:string
        // 人物的状态 0默认 1被抢
                personStatus ?:number
        // 归来时间戳
                personBackTime ?:number
        // 1房屋 2人物 3科技/招募 4奖励到背包采集红点
                tipList ?:Array<number>
        // 是否满仓
                isFull ?:boolean
        // 可提升技能编号
                canUpgradeSkillNo ?:number
        // 可招募兵种
                canZhaoMuName ?:string
        // 产出类型
                chanChuType ?:number
        // 技能产出类型列表
                skillProductTypeList ?:Array<number>
    }
// 小镇未解锁房屋信息
    interface TownRoomInfoUnOpenUnit  {
        // 房屋类型
                roomType ?:number
        // 房屋名字
                roomName ?:string
        // 人物Id
                personId ?:number
        // 人物的头像
                personTouXiang ?:string
        // 房屋开启所需小镇等级
                needTownLevel ?:number
        // 任务描述
                missionDesc ?:string
        // 任务名称
                taskName ?:string
        // 跳转的id
                tiaoZhuanId ?:number
        // 任务表defId
                missionDefId ?:number
    }
// 抢来的人物信息
    interface TownQiangDuoPersonUnit  {
        // 人物Id
                personId ?:number
        // 抢夺人物的DBId
                personDbId ?:number
        // 离开时间戳
                personBackTime ?:number
        // 主人名字
                masterName ?:string
        // 主人服务器
                masterServerNum ?:number
        // 增加产出属性
                addJiangLi ?:Array<TownPersonShuXingUnit>
        // 增加属性列表
                addShuXingList ?:Array<ShuXingInfoUnit>
        // 付费技能列表
                paySkillList ?:Array<TownPersonPaySkillUnit>
    }
// 被抢信息
    interface TownBeiQiangInfoUnit  {
        // 归来时间戳
                personBackTime ?:number
        // 抢夺者id
                QiangDuoPlayerId ?:number
        // 抢夺者名字
                QiangDuoName ?:string
        // 抢夺者服务器
                QiangDuoServerNum ?:number
        // 减少产出属性
                produceJiangLi ?:Array<TownPersonShuXingUnit>
        // 减少属性列表
                produceShuXingList ?:Array<ShuXingInfoUnit>
        // 抢夺者X坐标
                qiangDuoPosX ?:number
        // 抢夺者Y坐标
                qiangDuoPosY ?:number
    }
// 小镇产出信息
    interface TownProductUnit  {
        // 产出类型
                type ?:number
        // 产出数量
                nowValue ?:number
        // 加成数量
                addValue ?:number
        // 解锁所需房屋等级
                openRoomLevel ?:number
        // 是否是千分比
                isPercent ?:boolean
        // 是否是上限
                isMax ?:boolean
    }
// 小镇产出信息
    interface TownProductShowUnit  {
        // 产出类型
                type ?:number
        // 产出数量
                value ?:number
        // 单位时间(秒)
                perTime ?:number
        // 单位数量
                perCount ?:number
        // 数量上限
                countMax ?:number
    }
// 小镇兵种信息
    interface TownSoldierShowUnit  {
        // 兵种
                soldierType ?:number
        // 名字
                soldierName ?:string
        // 数量
                soldierCount ?:number
        // 负重
                fuzhong ?:number
        // 属性列表
                propertyList ?:Array<ShuXingInfoUnit>
    }
// 小镇科技
    interface TownKeJiUnit  {
        // 技能编号
                sType ?:number
        // 产出
                product ?:msgObj.JiangliInfoUnit
        // 当前储量
                curCount ?:number
        // 储量上限
                maxCount ?:number
        // 单位产量
                perCount ?:number
        // 产量加值
                addCount ?:number
        // 单位时间
                perTime ?:number
        // 满仓时间
                fullTimeStamp ?:number
        // 开启需要房屋等级
                needRoomLevel ?:number
        // 当前等级
                curLevel ?:number
        // 是否满级
                isFull ?:boolean
        // 消耗列表
                costList ?:Array<JiangliInfoUnit>
    }
// 小镇士兵
    interface TownSoldierUnit  {
        // 兵种
                sType ?:number
        // 名字
                name ?:string
        // 头像
                touXiang ?:string
        // 形象
                xingXiang ?:string
        // 当前等级
                curLevel ?:number
        // 是否满级
                isFullLevel ?:boolean
        // 当前储量
                curCount ?:number
        // 储量上限
                maxCount ?:number
        // 开启需要房屋等级
                needRoomLevel ?:number
        // 消耗列表
                costList ?:Array<JiangliInfoUnit>
        // 属性列表
                shuXingList ?:Array<ShuXingInfoUnit>
        // 处于招募中的数量
                countInZhaomuQueue ?:number
        // 单个士兵招募需要时间，单位秒
                secondOfOneSoldier ?:number
        // 完成招募时间戳，单位：毫秒
                finishZhaomuTime ?:number
        // 招募立即完成所需消耗/1分钟=N金券
                costAtOnce ?:msgObj.JiangliInfoUnit
        // 当前状态（1:可招募状态，2:招募中状态）
                cureState ?:number
        // 1个队员需消耗便当比例;【时间/小时】
                foodCostOfOneSoldier ?:msgObj.JiangliInfoUnit
        // 付费技能追加的属性列表
                appendPropertyList ?:Array<ShuXingInfoUnit>
    }
// 小镇搜到的玩家部分信息
    interface TownSearchPartInfoUnit  {
        // 玩家角色ID
                playerId ?:number
        // 玩家昵称
                playerName ?:string
        // 玩家区服
                serverNum ?:number
        // 小镇等级
                townLevel ?:number
        // 可攻击次数
                canAttackCount ?:number
        // 护盾结束时间，单位：毫秒。可能为0，也可能为过去时间
                hudunEndTime ?:number
        // 小镇x坐标
                x ?:number
        // 小镇y坐标
                y ?:number
        // 当前产出列表
                curProductList ?:Array<JiangliInfoUnit>
        // 房屋图片资源列表
                roomImageList ?:Array<string>
        // 玩家类型：1真实玩家；2NPC
                playerType ?:number
    }
// 小镇搜到的上阵宠物信息
    interface TownSearchHeroInfoUnit  {
        // 武将数据表Key
                heroDbID ?:number
        // 武将ID, 静态ID
                heroID ?:number
        // 等级
                level ?:number
        // 培养等级
                levelGuanZhi ?:number
    }
// 小镇人物提供属性
    interface TownPersonShuXingUnit  {
        // 类型
                type ?:number
        // 值
                value ?:number
        // 是否是增加
                isAdd ?:boolean
        // 是否是千分比
                isPercent ?:boolean
    }
// 小镇推送
    interface TownPushUnit  {
        // 类型
                type ?:number
        // 值
                value ?:number
        // 是否飘字
                isPiaoZi ?:boolean
    }
// 小镇护盾
    interface TownHuDunUnit  {
        // 时间(小时)
                time ?:number
        // 消耗
                cost ?:msgObj.JiangliInfoUnit
        // 是否折扣
                isDiscount ?:boolean
    }
// 攻击者信息
    interface TownAttackerInfoUnit  {
        // 服务器
                serverNum ?:number
        // 玩家名称
                playerName ?:string
        // 到达时间
                arriveTime ?:number
    }
// 小镇道馆信息
    interface TownDaoGuanUnit  {
        // 策划表Id
                defId ?:number
        // 占领公会服务器
                masterGongHuiServerNum ?:number
        // 占领公会Id
                masterGongHuiId ?:number
        // 占领公会名字
                masterGongHuiName ?:string
        // 占领公会徽章
                masterGongHuiHuiZhang ?:string
        // 护盾结束时间
                huDunEndTime ?:number
        // 攻击公会列表
                attackList ?:Array<number>
        // 被攻击是否结束
                fightIsEnd ?:boolean
    }
// 小镇道馆驻守人物信息
    interface TownDaoGuanPlayerUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家名称
                playerName ?:string
        // 玩家头像
                playerTouXiangId ?:number
        // 玩家皮肤列表
                piFuDefIdList ?:Array<number>
        // 公会职位
                gongHuiZhiWei ?:number
        // 战力
                playerFight ?:number
        // 驻守兵力
                soldierNum ?:number
        // 总兵力
                soldierMaxNum ?:number
        // 行军结束时间
                xingJunEndTime ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 小镇攻击在途
    interface TownDaoGuanAttackingUnit  {
        // 服务器号
                serverNum ?:number
        // 公会名称
                gongHuiName ?:string
        // 公会徽章
                gongHuiHuiZhang ?:string
        // 玩家名称
                playerName ?:string
        // 到达时间
                arriveTime ?:number
    }
// 小镇攻击记录
    interface TownDaoGuanAttackLogUnit  {
        // 排序
                position ?:number
        // 服务器号
                serverNum ?:number
        // 公会名称
                gongHuiName ?:string
        // 公会徽章
                gongHuiHuiZhang ?:string
        // 杀敌人数
                killTotalNum ?:number
    }
// 小镇道馆详情
    interface TownDaoGuanDetailUnit  {
        // 占领公会名称
                masterGongHuiName ?:string
        // 占领公会徽章
                masterGongHuiHuiZhang ?:string
        // 提升奖励奖励列表
                jiangLiList ?:Array<JiangliInfoUnit>
        // 当前驻守人数
                curDefenseNum ?:number
        // 最大驻守人数
                maxDefenseNum ?:number
        // 驻守机器人个数
                liveRobotNum ?:number
        // 驻守士兵人数
                defenseSoldierNum ?:number
    }
// 公会排名
    interface TownDaoGuanGongHuiForPaiMingUnit  {
        // 排名
                position ?:number
        // 区服编号
                serverNum ?:number
        // 公会名称
                gongHuiName ?:string
        // 公会徽章
                gongHuiHuiZhang ?:string
        // 当前人数
                curNum ?:number
        // 最大人数
                maxNum ?:number
        // 占领道馆数
                daoGuanNum ?:number
        // 杀敌总数
                killTotal ?:number
    }
// 小镇道馆赛季排名奖励
    interface TownDaoGuanSeasonJiangLiUnit  {
        // 排名开始
                positionStart ?:number
        // 排名结束
                positionEnd ?:number
        // 奖励列表
                jiangLiList ?:Array<JiangliInfoUnit>
    }
// 小镇道馆首次占领奖励
    interface TownDaoGuanFirstJiangLiUnit  {
        // 道馆Id
                daoGuanId ?:number
        // 公会服务器
                gongHuiNum ?:number
        // 公会名称
                gongHuiName ?:string
        // 公会徽章
                gongHuiHuiZhang ?:string
        // 奖励列表
                jiangLiList ?:Array<JiangliInfoUnit>
    }
// 小镇道馆贡献
    interface TownDaoGuanGongXianUnit  {
        // 排名
                position ?:number
        // 名字
                playerName ?:string
        // 公会职位
                zhiWei ?:number
        // 杀敌数
                killNum ?:number
        // 损失数
                loseNum ?:number
    }
// 小镇道馆人物行军
    interface TownDaoGuanPlayerXingJunUnit  {
        // 1、攻击；2、驻守；3、反击入侵者
                type ?:number
        // 道馆Id
                daoGuanId ?:number
        // 结束时间
                endTime ?:number
        // 机器人名字
                jiqirenName ?:string
    }
// 小镇医院伤员信息
    interface TownWoundedUnit  {
        // 兵种
                sType ?:number
        // 名字
                name ?:string
        // 头像
                touXiang ?:string
        // 形象
                xingXiang ?:string
        // 当前等级
                curLevel ?:number
        // 当前储量
                curCount ?:number
        // 储量上限
                maxCount ?:number
        // Db Id
                dbId ?:number
        // 完成治疗时间戳，单位：毫秒
                finishCureTime ?:number
        // 1个兵的治疗所消耗的能量块
                crystalOneSoldier ?:msgObj.JiangliInfoUnit
        // 立即完成消耗
                costAtOnce ?:msgObj.JiangliInfoUnit
        // 当前状态（1:可治疗状态，2:治疗中状态）
                cureState ?:number
    }
// 小镇集市兑换信息
    interface TownShopConvertUnit  {
        // 策划表Id
                defId ?:number
        // 兑换消耗材料
                costForConvert ?:msgObj.JiangliInfoUnit
        // 可选择的兑换奖励额
                selectList ?:Array<JiangliInfoUnit>
        // 剩余可兑换次数
                number ?:number
    }
// 小镇道馆玩家数据
    interface TownDaoGuanPlayerInfoUnit  {
        // 攻击方区服编号
                serverNum ?:number
        // 攻击方角色ID
                playerId ?:number
        // 攻击方名称
                playerName ?:string
        // 攻击方公会ID
                gonghuiId ?:number
        // 攻击方公会名称
                gonghuiName ?:string
        // 攻击方公会徽章
                gonghuiHuizhang ?:string
    }
// 小镇道馆战斗历史记录信息
    interface TownDaoGuanFightRecordUnit  {
        // 战斗记录Id，查看明细须回传
                id ?:number
        // 战斗发生时间，单位：毫秒
                fightTime ?:number
        // 是否我方胜利
                isAttackWin ?:boolean
        // 攻击方玩家信息
                attackPlayerInfo ?:msgObj.TownDaoGuanPlayerInfoUnit
    }
// 小镇道馆战斗记录明细内容战报信息
    interface TownDaoGuanFightRecordDetailUnit  {
        // 战斗记录Id，查看明细须回传
                id ?:number
        // 战斗发生时间，单位：毫秒
                fightTime ?:number
        // 是否攻击方胜利
                isAttackWin ?:boolean
        // 攻击方玩家信息
                attackPlayerInfo ?:msgObj.TownDaoGuanPlayerInfoUnit
        // 被攻击方玩家信息
                targetPlayerInfo ?:msgObj.TownDaoGuanPlayerInfoUnit
        // 攻击方损失兵种列表
                attackLostSoliderList ?:Array<TownSoldierShowUnit>
        // 被攻击方损失兵种列表
                targetLostSoliderList ?:Array<TownSoldierShowUnit>
        // 战斗回放脚本LogId
                logId ?:number
    }
// 小镇兵种骨骼信息
    interface TownSoldierGugeShowUnit  {
        // 兵种
                soldierType ?:number
        // 骨骼 guge表Id
                guge ?:number
    }
// 小镇未解锁房屋简短的信息
    interface TownUnOpenRoomShortInfoUnit  {
        // 房屋类型
                roomType ?:number
        // 建筑名称
                roomName ?:string
    }
// 小镇人物的付费技能
    interface TownPersonPaySkillUnit  {
        // 技能Id
                skillId ?:number
        // 技能名称
                name ?:string
        // 技能描述
                skillDesc ?:string
        // 技能ICON
                icon ?:string
        // 是否已经激活技能
                isActive ?:boolean
        // 激活类型：1活动激活；2充值激活
                activeType ?:number
        // Jump Id
                jump ?:number
        // 充值金额表Id
                chongzhiJineDefId ?:number
    }
// 金猪关卡阵容精灵宠物信息
    interface TuiTuFightHeroUnit  {
        // 精灵宠物Db id
                dbId ?:number
        // 精灵宠物Def Id数据
                uID ?:number
        // 培养等级，宠物星级
                peiyangLevel ?:number
        // 精灵宠物等级
                heroLevel ?:number
        // 宠物本服排行，只排前30名
                rank ?:number
    }
// 比武记录数据
    interface VipInfoUnit  {
        // VIP静态 ID
                vipId ?:number
        // 返回码：1未领取，2已领取
                doGet ?:number
    }
// vip等级数量统计数据
    interface VipLvCountInfoUnit  {
        // vip级别
                vipLevel ?:number
        // 数量
                count ?:number
    }
// 新的vip数据
    interface NewVipInfoUnit  {
        // vip级别
                vipLevel ?:number
        // 说明
                shuoMing ?:string
        // 任务列表
                missionList ?:Array<MissionInfoUnit>
        // 激活主线任务Id
                jiHuoRenWu ?:number
    }
// 新的vip礼包数据
    interface VipGiftInfoUnit  {
        // vip级别
                vipLevel ?:number
        // 是否购买了VIP礼包
                isBuyVipGift ?:boolean
        // VIP礼包奖励列表
                vipGiftRewardList ?:Array<JiangliInfoUnit>
        // VIP礼包价格信息
                vipGiftCostInfo ?:msgObj.JiangliInfoUnit
    }
// 直升VIP带来的VIP任务奖励数据
    interface VipMissionRewardInfoUnit  {
        // vip级别
                vipLevel ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
    }
// VIP1免费领取钻石数据
    interface VipFreeDiamondInfoUnit  {
        // 下次可领取钻石的时间戳，超过了自动为可领取状态
                getRewardTime ?:number
        // 本次可领取的奖励信息
                rewardInfo ?:msgObj.JiangliInfoUnit
        // 累计库存钻石
                sumOfDiamond ?:msgObj.JiangliInfoUnit
        // 领取状态：1免费领取中；2待充值；3待领取库存砖石；4充值后领取中；5结束
                status ?:number
        // 钻石剩余可领取次数
                getRewardCount ?:number
        // 剩余充值金券
                jinquan ?:number
    }
// 玩吧vip礼包
    interface WanBaVipRewardInfoUnit  {
        // 策划表对应id
                defId ?:number
        // 所需金券价格
                price ?:number
        // 所需vip等级
                needLevel ?:number
        // 是否可以领取当前这个礼包
                isCan ?:boolean
        // 是否已经买过
                isGet ?:boolean
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 周末登录奖励数据
    interface WeekendLoginJiangliInfoUnit  {
        // ID，静态表主Key
                defId ?:number
        // 子类型
                subtype ?:number
        // 区分同一子类，礼包先后
                dangwei ?:number
        // 名称
                shuoming1 ?:number
        // 预约前标题
                shuoming2 ?:number
        // 分享文字
                fenxiangwenzi ?:number
        // 领取日期
                getDate ?:string
        // 档位状态 1.可领取 2.可预约 3.已预约 4.已结束
                status ?:number
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
    }
// 周末在线奖励数据
    interface WeekendZaixianJiangliInfoUnit  {
        // ID，静态表主Key
                zaixianjiangliId ?:number
        // 说明
                shuoMing ?:string
        // 需在线时间，单位:s 秒
                tiaojian ?:number
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
        // 跳转
                jump ?:string
    }
// 威名等级属性品质unit
    interface WeimingPropAdnRankInfoUnit  {
        // 属性列表
                propInfoList ?:Array<PropertyInfoUnit>
        // 威名名称
                name ?:string
    }
// 威名等级宝石unit
    interface WeimingLevelGemstoneInfoUnit  {
        // 威名阶段
                jieduan ?:number
        // 威名名称资源
                name ?:string
        // 1、max已经进阶完成；2、觉醒状态，处于当前正在升级进阶中；3、未达到，未开启，无法升级。
                status ?:number
    }
// 威名技能宝石信息unit
    interface WeimingSkillGemstoneInfoUnit  {
        // 是否点亮
                isLight ?:boolean
        // 属性信息
                propInfo ?:msgObj.PropertyInfoUnit
    }
// 威名当前技能unit
    interface WeimingSkillInfoUnit  {
        // 技能等级
                skillLevel ?:number
        // 威名技能是否已经满级
                isFullSkillLevel ?:boolean
        // 威名当前技能描述
                currDesc ?:string
        // 威名下一技能等级描述
                nextDesc ?:string
        // 奖励列表
                costList ?:Array<JiangliInfoUnit>
        // 威名技能Icon
                icon ?:string
        // 六颗宝石信息列表
                gemstoneInfoList ?:Array<WeimingSkillGemstoneInfoUnit>
        // 威名当前阶段值，技能等级不允许超过威名当前阶段值
                weimingCurrJieduan ?:number
    }
// 威名显示unit
    interface WeimingShowInfoUnit  {
        // 威名当前等级
                level ?:number
        // 技能等级
                skillLevel ?:number
        // 威望名称
                name ?:string
        // 威名技能Icon
                icon ?:string
    }
// 威名充值unit
    interface WeimingRechargeInfoUnit  {
        // 威名充值表ID
                defId ?:number
        // 充值金额表ID
                chongzhiId ?:number
        // 名称
                name ?:string
        // 每日购买上限
                max ?:number
        // 今日已购买次数
                todayNum ?:number
        // 突破
                banner ?:string
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 威名名片unit
    interface PlayerCardWeimingUnit  {
        // 威名当前阶段
                jieudan ?:number
        // 次数
                cishu ?:number
        // 满阶次数
                maxCishu ?:number
        // 威名阶段是否已经满阶
                isFullJieduan ?:boolean
        // 当前升级或者进阶信息
                currPropAndRank ?:msgObj.WeimingPropAdnRankInfoUnit
        // 宝石列表
                gemstoneInfoList ?:Array<WeimingLevelGemstoneInfoUnit>
        // 技能阶段
                skillLevel ?:number
        // 威名技能Icon
                icon ?:string
        // 技能描述
                skillDesc ?:string
        // 威名名称资源
                weimingName ?:string
    }
// BOSS伤害排名信息
    interface DamageRankInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 角色名称
                name ?:string
        // 伤害排名
                index ?:number
        // 伤害总值
                totalDamage ?:number
    }
// 我要变强数据
    interface WoYaoBianQiangUnit  {
        // 小类型
                SubType ?:number
        // 性价比
                xingJiaBi ?:number
        // 提升战力
                upFight ?:number
        // 名称
                name ?:string
    }
// 我要变强单宠数据
    interface WoYaoBianQiangDanChongUnit  {
        // 精灵id
                heroDbid ?:number
        // 精灵信息
                heroInfo ?:Array<WoYaoBianQiangUnit>
    }
// 限时冲级info
    interface XianShiChongJiInfoUnit  {
        // 详情id 把这个id发给后端 领哪个奖励
                detailId ?:number
        // 达到什么等级显示可领取 
                dabiaoLevel ?:number
        // 结束时刻的毫秒数
                endTime ?:number
        // 奖励类型
                typeAward1 ?:number
        // 物品id
                item1 ?:number
        // 数量
                count1 ?:number
        // 奖励类型
                typeAward2 ?:number
        // 物品id
                item2 ?:number
        // 数量
                count2 ?:number
        // 奖励类型
                typeAward3 ?:number
        // 物品id
                item3 ?:number
        // 数量
                count3 ?:number
        // 奖励类型
                typeAward4 ?:number
        // 物品id
                item4 ?:number
        // 数量
                count4 ?:number
        // 奖励类型
                typeAward5 ?:number
        // 物品id
                item5 ?:number
        // 数量
                count5 ?:number
        // 奖励类型
                typeAward6 ?:number
        // 物品id
                item6 ?:number
        // 数量
                count6 ?:number
        // 奖励类型
                typeAward7 ?:number
        // 物品id
                item7 ?:number
        // 数量
                count7 ?:number
        // 奖励类型
                typeAward8 ?:number
        // 物品id
                item8 ?:number
        // 数量
                count8 ?:number
        // 奖励类型
                typeAward9 ?:number
        // 物品id
                item9 ?:number
        // 数量
                count9 ?:number
        // 奖励类型
                typeAward10 ?:number
        // 物品id
                item10 ?:number
        // 数量
                count10 ?:number
    }
// 洗练属性数据
    interface XiLianPropUnit  {
        // 洗练属性品质
                grade ?:number
        // 洗练属性
                propertyInfo ?:msgObj.PropertyInfoUnit
    }
// 
    interface XunBaoLeYuanInfoUnit  {
        // 位置从0开始
                index ?:number
        // 位置从0开始
                jiangliInfo ?:msgObj.JiangliInfoUnit
        // 皮肤类型
                piFuType ?:number
        // 皮肤id
                piFuId ?:number
    }
// 遗迹探险任务信息
    interface YiJiTanXianTaskInfoUnit  {
        // 任务dbId
                taskDbId ?:number
        // 任务defId
                taskDefId ?:number
        // 任务状态（0：未接取 1：进行中 2：已完成（可以领奖） 3：已经领奖）
                taskStatus ?:number
        // 奖励数据
                bonusList ?:Array<JiangliInfoUnit>
        // 消耗
                costInfo ?:msgObj.JiangliInfoUnit
        // 任务星级
                star ?:number
        // 接取任务时间戳
                acceptTaskTime ?:number
        // 任务持续时间（毫秒）
                taskLastTime ?:number
        // 加快完成任务所需消耗
                finishTaskNeedCost ?:msgObj.JiangliInfoUnit
        // 是否显示红点
                showRedTips ?:boolean
    }
// 印记之源数据数据
    interface YinJiInfoUnit  {
        // 1火    2水    3草    4光 5暗
                type ?:number
        // 当前印记之源的等级
                yuanLevel ?:number
    }
// 某个模板下的一元特价状态数据
    interface YiyuantejiaMobanStatusInfoUnit  {
        // 入口类型
                mobanType ?:number
        // 连续礼包状态列表
                idStatusList ?:Array<ShuXingInfoUnit>
    }
// 月卡奖励
    interface YueKaJiangliInfoUnit  {
        // 月卡静态表id
                yueKaId ?:number
        // 剩余领取次数
                remain ?:number
        // 目前可领取次数
                canGet ?:number
    }
// 奖励数据
    interface ZaixianJiangliInfoUnit  {
        // ID，静态表主Key
                zaixianjiangliId ?:number
        // 说明
                shuoming ?:string
        // 需在线时间单位：秒
                tiaojian ?:number
        // 需VIP等级
                vip ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
                doGet ?:number
        // 跳转页面
                jump ?:string
        // 累计时间在线时长，单位:s 秒
                onlineTime ?:number
        // 奖励列表
                rewardList ?:Array<JiangliInfoUnit>
    }
// 宅急送玩家info
    interface ZJSPlayerInfoUnit  {
        // 玩家id
                playerId ?:number
        // 宅急送id
                zjsId ?:number
        // 公会id
                guildId ?:number
        // 速度
                moveSpeed ?:number
        // 玩家名字
                playerName ?:string
        // 公会名称
                guildName ?:string
        // 服务器号
                serverNo ?:number
        // 玩家头像
                touXiang ?:number
        // 玩家形象
                xingXiang ?:number
        // 玩家等级
                level ?:number
        // 玩家战力
                zhanli ?:string
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 宅急送属性info
    interface ZJSDataInfoUnit  {
        // 玩家id
                playerId ?:number
        // 服务器id
                severId ?:number
        // 宅急送id
                zjsId ?:number
        // 护送玩家列表
                huSongList ?:Array<ZJSPlayerInfoUnit>
        // 最大战力
                maxFight ?:string
        // 货物等级
                zjsLevel ?:number
        // 最快速度
                maxSpeed ?:number
        // 开始时间
                startTime ?:number
        // 完成时间
                endTime ?:number
    }
// 宅急送历史info
    interface ZJSHistroyInfoUnit  {
        // 时间
                createTime ?:number
        // 宅急送星级
                zjsLevel ?:number
        // 记录类型 0完成 1被攻击 2抢夺失败
                histroyType ?:number
        // 被抢次数
                count ?:number
        // 敌人名称
                enemyName ?:string
        // 敌人服务器id
                enemyServerNo ?:number
        // 敌人公会名称
                enemyGuildName ?:string
        // 战报id
                fightLogId ?:number
        // 奖励列表
                JiangLiList ?:Array<JiangliInfoUnit>
    }
// 战败分析数据
    interface ZhanBaiFenXiInfoUnit  {
        // 
                value1 ?:number
        // 
                value2 ?:number
        // 
                jump ?:number
        // 
                icon ?:string
        // 
                name ?:string
    }
// 战队信息
    interface ZhanDuiInfoUnit  {
        // 战队ID
                zhanDuiId ?:number
        // 头像id
                zhanDuiHeadId ?:number
        // 队长id
                leaderId ?:number
        // 战队名字(和serverId拼在一起)
                zhanDuiName ?:string
        // 战队战力
                zhanDuiFight ?:number
        // 队长名字
                duiZhangName ?:string
        // 战队已有人数
                zhanDuiMemberNum ?:number
        // 战队最多人数
                zhanDuiMaxMemberNum ?:number
        // 服务器id
                serverId ?:number
        // 服务器编号
                serverNumber ?:number
        // 是否是机器人
                isBot ?:boolean
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 战队里的玩家信息
    interface ZhanDuiMemberInfoUnit  {
        // 玩家ID
                playerId ?:number
        // 玩家名字
                playerName ?:string
        // 头像id
                playerHeadId ?:number
        // 玩家等级
                playerLevel ?:number
        // 是否在线
                isOnline ?:boolean
        // 玩家战力
                playerFight ?:number
        // 战队id
                zhanDuiId ?:number
        // 是否是队长
                isLeader ?:boolean
        // 服务器编号
                serverNumber ?:number
        // 服务器id
                serverId ?:number
        // 位置（队伍1,2,3）
                position ?:number
        // 是否是机器人
                isBot ?:boolean
        // 所选武将在副阵容中的位置
                teamIndex ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 争霸赛商城商品信息
    interface ZhanDuiShopInfoUnit  {
        // 策划表Id
                defId ?:number
        // 排序
                paiXu ?:number
        // 商品类型
                type ?:number
        // 商品的道具Id
                itemId ?:number
        // 商品的道具数量
                itemCount ?:number
        // 名称
                mingCheng ?:string
        // 说明
                shuoMing ?:string
        // 玩家等级
                level ?:number
        // 消耗类型
                costType ?:number
        // 消耗道具ID
                costItem ?:number
        // 消耗数量
                costCount ?:number
        // 每日限购
                meiRiXianGou ?:number
        // 我今天的购买次数
                myBuyCount ?:number
    }
// 战棋棋子
    interface ZhanQiQiZiInfoUnit  {
        // 是否有棋子
                holdState ?:number
        // 是否属于我
                isMine ?:number
        // 图标
                icon ?:string
        // 名字
                name ?:string
        // 棋子类型
                qiZiType ?:number
    }
// 战棋棋盘
    interface ZhanQiPanelInfoUnit  {
        // 30个棋子
                qiZiList ?:Array<ZhanQiQiZiInfoUnit>
        // 回合数
                huiHeShu ?:number
        // 是否是我的回合 0 对手 1 我
                myTurn ?:number
    }
// 战棋玩家
    interface ZhanQiPlayerInfoUnit  {
        // 请求者ID
                playerId ?:number
        // 姓名
                playerName ?:string
        // 段位
                duanWei ?:string
        // 皮肤图
                pifuTu ?:string
        // 皮肤ID
                pifuId ?:number
        // 皮肤类型 0.NPC 1.默认男女 2.幻化类型
                pifuType ?:number
        // 胜场数
                winCount ?:number
        // 皮肤幻化ID
                piFuDefIdList ?:Array<number>
    }
// 战棋排行榜玩家
    interface ZhanQiPaiHangBangPlayerInfoUnit  {
        // 请求者ID
                playerId ?:number
        // 姓名
                playerName ?:string
        // 段位
                duanWei ?:string
        // 胜场数
                winCount ?:number
        // 皮肤图
                pifuTu ?:string
        // 皮肤ID
                pifuId ?:number
        // 皮肤类型 0.NPC 1.默认男女 2.幻化类型
                pifuType ?:number
        // 皮肤幻化ID
                piFuDefIdList ?:Array<number>
        // 头像ID
                playerTouxiangId ?:number
        // 坐骑ID
                playerZuoqi ?:number
        // 翅膀ID
                playerChibang ?:number
        // 称号ID
                chenghaoId ?:number
        // 称号ID
                chenghaoScale ?:number
    }
// 战阵碎片信息
    interface ZhanzhenSuipianInfoUnit  {
        // 战阵碎片dbID，玩家战阵表数据库主键
                zhanzhenSuipianDbID ?:number
        // 战阵碎片静态表ID
                zhanzhenSuipianID ?:number
        // 玩家拥有此战阵碎片的数量
                zhanzhenSuipianCount ?:number
    }
// 战阵信息
    interface ZhanzhenInfoUnit  {
        // 战阵dbID，玩家战阵表数据库主键
                zhanzhenDbID ?:number
        // 战阵静态表ID
                zhanzhenID ?:number
        // 玩家拥有此战阵当前经验值
                zhanzhenExp ?:number
    }
// 所有的战阵信息
    interface ZhanzhenAllInfoUnit  {
        // 战阵dbID，玩家战阵表数据库主键
                dbId ?:number
        // 战阵静态表ID
                defId ?:number
        // 是否获取过
                isHave ?:boolean
        // 名字
                name ?:string
        // 等级
                level ?:number
        // 描述
                desc ?:string
        // 图标
                icon ?:string
        // 徽章边框
                bianKuang ?:string
        // 颜色
                color ?:number
        // 类型
                grade ?:number
        // 所需章节数
                reqNum ?:number
        // 是否突破
                isBreak ?:number
        // 当前属性列表
                curShuXingList ?:Array<ShuXingInfoUnit>
        // 下阶段属性列表
                nextShuXingList ?:Array<ShuXingInfoUnit>
        // 消耗列表
                costList ?:Array<JiangliInfoUnit>
        // 当前颜色的徽章是否已经满级了
                isMax ?:boolean
    }
// 指令分析数据
    interface ZhilingFenxiInfoUnit  {
        // 消息码
                msgCode ?:number
        // 主消息码
                mainCode ?:number
        // 次消息码
                subCode ?:number
        // 处理次数
                protimes ?:number
        // 平均处理时间
                avatime ?:number
        // 最长处理时间
                longtime ?:number
        // 最短处理时间
                shorttime ?:number
    }
// 职业神器
    interface ZhiYeShenQiUnit  {
        // DBID
                dbId ?:number
        // DefId
                defId ?:number
        // 神器等级
                level ?:number
        // 神器职业
                zhiYe ?:number
        // 基本属性
                basicShuXingList ?:Array<ShuXingInfoUnit>
        // 职业属性
                zhiYeShuXingList ?:Array<ShuXingInfoUnit>
        // 针对于4 5 职业 的职业属性得用buffId
                buffId ?:number
    }
// 一条信息
    interface ZhongZuZhiProInfoUnit  {
        // 属性类型
                type ?:number
        // 当前值
                curValue ?:number
        // 属性最大值
                maxValue ?:number
        // 最终属性值(基础值+洗出的属性值)
                proValue ?:number
    }
// 种族值突破信息
    interface ZhongZuZhiTuPoInfoUnit  {
        // 宠物基础id
                jiChuId ?:number
        // 种族值是否显示  0:不显示 1:显示
                isShow ?:number
        // 突破等级
                tuPoLv ?:number
        // 突破描述
                tuPoDes ?:Array<string>
        // 操作类型 0:洗练 1:突破 2:全满
                operType ?:number
    }
// 种族值突破详情
    interface ZhongZuZhiTuPoDetailsUnit  {
        // 种族值是否显示  0:不亮 1:亮
                isOpen ?:number
        // 突破描述
                tuPoDes ?:string
    }
// 野外抓宠中和天气有关的神兽 用在地图列表和生成怪物接口里
    interface TianQiGuaiWuUnit  {
        // 
                mapId ?:number
        // 天气Id
                tianQiId ?:number
        // 天气神兽的武将defId
                heroDefId ?:number
        // 是不是引导中的boss
                isYInDaoShenShou ?:boolean
        // 是不是死了  true是   flase  没有
                isDead ?:boolean
        // 上午开启时间 毫秒
                startTimeAm ?:number
        // 上午结束时间 毫秒
                endTimeAm ?:number
        // 下午开启时间 毫秒
                startTimePm ?:number
        // 下午结束时间 毫秒
                endTimePm ?:number
    }
// 抓宠中心房间
    interface ZhuaChongRoomUnit  {
        // 房间defId
                roomDefId ?:number
        // 房间名字
                roomName ?:string
        // 1vip   2等级
                tiaoJianType ?:number
        // 剩余次数  负数表示没有限制
                leftTimes ?:number
        // 条件的数字
                tiaoJianZhi ?:number
        // 房间上几个Icons
                jiangliList ?:Array<JiangliInfoUnit>
        // 房间可能的产出 武将id的集合
                zhuaChongRoomProduceList ?:Array<number>
        // 这个房间可以购买的最大的探索次数
                maxBuySearchTimes ?:number
        // 这个房间已经购买的探索次数
                hasBuySearchTimes ?:number
        // 购买探索次数的货币类型 同奖励类型
                buySearchCurrencyType ?:number
        // 购买探索次数的货币数量
                buySearchCurrencyCount ?:number
    }
// 抓宠信息
    interface ZhuaChongPriceInfoUnit  {
        // 怪物表Id
                chongWuId ?:number
        // 高级球价格
                gaoJiQiu ?:msgObj.JiangliInfoUnit
        // 大师球价格
                daShiQiu ?:msgObj.JiangliInfoUnit
    }
// 抓宠助手精灵
    interface ZhuaChongZhuShouHeroUnit  {
        // 武将Id
                heroId ?:number
        // 数量
                count ?:number
    }
// 装备数据
    interface ZhuangbeiInfoUnit  {
        // 装备数据表Key
                zhuangbeiDbID ?:number
        // 原始装备ID-静态表主键
                zhuangbeiID ?:number
        // 附魔经验值
                fumoExp ?:number
        // 装备类型 0：武器 1：盔甲 2：坐骑 3: 宝物
                zhuangbeiType ?:number
        // 强化等级
                qianghuaLevel ?:number
        // 附魔等级
                fumoLevel ?:number
        // 升星等级
                shengxingLevel ?:number
        // 镶嵌宝石1
                baoshiID1 ?:number
        // 镶嵌宝石2
                baoshiID2 ?:number
        // 镶嵌宝石3
                baoshiID3 ?:number
        // 升星属性列表
                shengXingPropInfoList ?:Array<PropertyInfoUnit>
        // 洗练属性列表
                xilianPropInfoList ?:Array<XiLianPropUnit>
        // 精炼属性列表
                jinglianPropInfoList ?:Array<PropertyInfoUnit>
        // 精炼等级
                jinglianLevel ?:number
        // 精炼消耗列表
                jingLianCostItemList ?:Array<JiangliInfoUnit>
        // 附魔升级消耗
                fuMoCostList ?:Array<JiangliInfoUnit>
        // 附魔等级上限
                maxFumoLevel ?:number
    }
// 外显装备数据
    interface ExpandZhuangbeiInfoUnit  {
        // 装备数据表Key
                zhuangbeiDbID ?:number
        // 原始装备ID-静态表主键
                zhuangbeiID ?:number
        // 位置
                position ?:number
        // 属性列表
                shuXingInfoList ?:Array<ShuXingInfoUnit>
    }
// 星级加成显示
    interface ZhuangbeiStarShowUnit  {
        // 星星数量，星级，或者精炼等级
                starNum ?:number
        // 属性列表
                propertyList ?:Array<PropertyInfoUnit>
    }
// 装备套装激活个人和对应的属性
    interface ZhuangbeiSuitActiveCountAndPropInfoUnit  {
        // 穿戴几件装备激活该套装属性
                activeCount ?:number
        // 属性列表
                propertyList ?:Array<PropertyInfoUnit>
    }
// 装备套装属性
    interface ZhuangbeiSuitInfoUnit  {
        // 套装Id
                suitId ?:number
        // 属性列表
                activeCountAndPropInfoList ?:Array<ZhuangbeiSuitActiveCountAndPropInfoUnit>
    }
// 装备图鉴属性
    interface ZhuangbeiTujianInfoUnit  {
        // 装备Id
                defId ?:number
        // 对应的套装Id
                suitId ?:number
        // 装备星级
                star ?:number
        // 装备基础属性列表
                basePropertyList ?:Array<PropertyInfoUnit>
    }
// 转生技能
    interface ZhuanShengSkillUnit  {
        // 技能类型
                type ?:number
        // 技能等级
                level ?:number
        // 当前属性列表
                curShuXingList ?:Array<ShuXingInfoUnit>
        // 下一级属性列表
                nextShuXingList ?:Array<ShuXingInfoUnit>
        // 需要的道具id
                xiaohaoItem ?:number
        // 需要的道具数量
                xiaohaoCount ?:number
        // 是否已经满级
                isFull ?:boolean
        // 描述
                desc ?:string
        // 当前buff类型
                logicType ?:number
        // 当前buff值
                logicCurValue ?:number
        // 下一buff类型
                logicNextType ?:number
        // 下一buff值
                logicNextValue ?:number
    }
// 转生天赋
    interface ZhuanShengTalentUnit  {
        // 天赋Id
                defId ?:number
        // 天赋名字
                name ?:string
        // 消耗类型
                costType ?:number
        // 消耗道具Id
                costItem ?:number
        // 需要的道具数量
                costCount ?:number
        // 是否已经激活
                isJiHuo ?:boolean
        // 描述
                desc ?:string
        // 属性列表
                shuXingList ?:Array<ShuXingInfoUnit>
    }
// 专属神器的属性  用在详情面板中
    interface ZhuanShuShenQiShuXingInfoUnit  {
        // true 激活了   false 没有  前端根据这个变颜色
                isJiHuo ?:boolean
        // 专属属性的描述
                desc ?:string
    }
// 专属神器的属性  用在背包中
    interface ZhuanShuShenQiBagInfoUnit  {
        // 佩戴的专属神器的dbid
                zhuanShuShenQiDbId ?:number
        // 佩戴的专属神器的等级
                zhuanShuShenLevel ?:number
        // 数量
                count ?:number
        // 图鉴星级
                starNum ?:number
        // 神器属性列表
                shenQiShuXing ?:Array<ShuXingInfoUnit>
    }
// 专属神器的描述
    interface ZhuanShuShenQiDescInfoUnit  {
        // 名称
                name ?:string
        // 描述
                desc ?:string
        // 描述
                icon ?:string
        // 需求等级
                tiaoJian ?:number
        // 品阶
                rank ?:number
    }
// 专属神器图鉴的图标
    interface ZhuanShuShenQiTuJianIconInfoUnit  {
        // 图鉴defId
                tuJianDefId ?:number
        // 图鉴图标
                tuJianIcon ?:string
        // 专属神器星级
                tuJianStar ?:number
        // 专属神器个数 包含穿戴的神器
                num ?:number
    }
// 专属神器推送信息
    interface ZhuanShuShenQiPushfoUnit  {
        // 佩戴的专属神器的dbid
                dbId ?:number
        // 策划表Id
                defId ?:number
        // 描述
                desc ?:string
        // 佩戴的专属神器的等级
                zhuanShuShenLevel ?:number
    }
// 专属神器的技能升级属性
    interface ZhuanShuShenQiShuSkillInfoUnit  {
        // 技能升级ID，对应SkillUpgrade
                skillUpgradeId ?:number
        // 技能升级信息描述，对应SkillUpgradeInfo
                desc ?:string
    }
// 主角装备位置
    interface ZhuJueEquipPositionUnit  {
        // 位置
                position ?:number
        // 装备Id
                equipId ?:number
        // 装备等级
                equipLevel ?:number
        // 装备品质
                equipPinZhi ?:number
        // 装备名称
                equipName ?:string
        // 装备图标
                equipIcon ?:string
        // 灵魂石Id
                itemId ?:number
        // 灵魂石等级
                itemLevel ?:number
        // 关联的宠物Id
                guanLianHeroId ?:number
        // 装备累计属性加成
                equipShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 灵魂石提供的属性加值
                itemShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 计算后的属性值
                resultPropValue ?:number
        // 计算后的属性值所对应的属性最大值
                resultPropMaxValue ?:number
    }
// 主角装备
    interface ZhuJueEquipUnit  {
        // 策划表Id
                defId ?:number
        // 装备类型
                type ?:number
        // 装备品质
                pinZhi ?:number
        // 名字
                name ?:string
        // 图标
                icon ?:string
        // 描述
                miaoShu ?:string
        // 装备数量
                count ?:number
        // 是否已经被装备
                isEquiped ?:boolean
    }
// 主角装备详细
    interface ZhuJueEquipDetailUnit  {
        // 位置
                position ?:number
        // 装备Id
                equipId ?:number
        // 装备等级
                equipLevel ?:number
        // 装备名称
                equipName ?:string
        // 装备品质
                equipPinZhi ?:number
        // 装备图标
                equipIcon ?:string
        // 灵魂石Id
                itemId ?:number
        // 灵魂石等级
                itemLevel ?:number
        // 灵魂石强化需要的道具id
                stoneXiaoHaoItem ?:number
        // 灵魂石强化需要的道具数量
                stoneXiaoHaoCount ?:number
        // 关联的宠物defId
                guanLianHeroId ?:number
        // 关联的宠物DbId
                guanLianHeroDbId ?:number
        // 灵魂石是否已经满级
                itemIsFull ?:boolean
        // 链接宠物提供的属性加值千分比
                LinkHeroAddPermil ?:number
        // 链接宠物本身的属性
                linkBasicShuXingList ?:Array<ShuXingInfoUnit>
        // 链接宠物为阵上宠物提供的属性加值
                linkAddShuXingList ?:Array<ShuXingInfoUnit>
    }
// 主角装备强化
    interface ZhuJueEquipQiangHuaUnit  {
        // 位置
                position ?:number
        // 装备Id
                equipId ?:number
        // 装备名称
                equipName ?:string
        // 装备品质
                equipPinZhi ?:number
        // 装备图标
                equipIcon ?:string
        // 当前等级
                curLevel ?:number
        // 消耗的道具
                xiaoHaoItem ?:number
        // 消耗的数量
                xiaoHaoCount ?:number
        // 当前属性列表
                curShuXingList ?:Array<ShuXingInfoUnit>
        // 下一级属性列表
                nextShuXingList ?:Array<ShuXingInfoUnit>
    }
// 主角装备属性大单元
    interface ZhuJueEquipPropUnit  {
        // 位置
                position ?:number
        // 装备Id
                equipId ?:number
        // 装备名称
                equipName ?:string
        // 装备品质
                equipPinZhi ?:number
        // 装备图标
                equipIcon ?:string
        // 洗练属性列表
                propList ?:Array<EquipPropUnit>
    }
// 主角装备属性小单元
    interface EquipPropUnit  {
        // 属性编号
                number ?:number
        // 属性类型
                propType ?:number
        // 属性值
                propValue ?:number
        // 祝福值
                propPercent ?:number
        // 这个属性在策划表中的最大值
                maxValue ?:number
    }
// 符文信息
    interface FuWenInfoUnit  {
        // 唯一id
                id ?:number
        // 静态表id
                defId ?:number
        // 名字
                name ?:string
        // 描述
                miaoshu ?:string
        // 颜色
                type ?:number
        // 镶嵌位置1,2,3,4,5
                pos ?:number
        // 等级
                level ?:number
        // 数量
                count ?:number
    }
// 符文信息
    interface FuWenShopInfoUnit  {
        // 商品id
                defId ?:number
        // 符文id
                fuwenId ?:number
        // 符文数量
                count ?:number
        // 分页标记
                fenye ?:number
        // 颜色
                type ?:number
        // 等级
                level ?:number
        // 名字
                name ?:string
        // 描述
                miaoshu ?:string
        // 消耗碎片数量
                xiaoHaoCount ?:number
        // 消耗的碎片id
                xiaoHaoId ?:number
    }
// 资源月卡
    interface ZiYuanYueKaUnit  {
        // 服务器ID，按照登录时间倒序排列
                id ?:number
        // 所属月卡Id
                yueKaId ?:number
        // 第几天
                day ?:number
        // 是否可领
                isCanGet ?:boolean
        // 是否已经领过了
                isHave ?:boolean
        // 奖励列表
                jiangliList ?:Array<JiangliInfoUnit>
    }
// 跨服Z力量数据
    interface PlayerCardZliliangUnit  {
        // 激活的神之力
                defIdList ?:Array<number>
        // 激活的魔之力
                defIdWithItemList ?:Array<number>
    }
// 组队副本面板信息
    interface ZuDuiFuBenPanelInfoUnit  {
        // dbid
                dbId ?:number
        // 头像id
                playerTouXiangId ?:number
        // 队长战力
                duiZhangZhanLi ?:number
        // 最高层
                maxLayer ?:number
        // 成员数量
                memberCount ?:number
        // 房间号
                roomNum ?:number
        // 战力要求
                limitFight ?:number
        // 服务器号 短id 用于显示
                serverInfo ?:number
        // 服务器ID 长id 用于跨服名片消息
                serverChangId ?:number
        // 玩家名称
                playerName ?:string
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 组队副本成员信息
    interface ZuDuiFuBenMemberInfoUnit  {
        // 头像id
                playerTouXiangId ?:number
        // 
                playerName ?:string
        // 
                memberId ?:number
        // 
                level ?:number
        // 
                vipLevel ?:number
        // 服务器号 短id 用于显示
                serverInfo ?:number
        // 服务器ID 长id 用于跨服名片消息
                serverChangId ?:number
        // 最高层
                maxLayer ?:number
        // 战力
                zhanLi ?:number
        // 1 队长      2 队员
                zhiWei ?:number
        // 是否准备  0没准备   1准备
                isReady ?:number
        // 形象 战斗中的小人是男是女 目前只有2个形象 为了方便扩展 不用性别
                xingXiang ?:number
        // 出战顺序 从1开始
                shunXu ?:number
        // 如果是true 不显示踢人按钮
                isPiPei ?:boolean
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 组队副本顺序信息   用在改变顺序的消息里
    interface ZuDuiFuBenIndexInfoUnit  {
        // 
                playerIdId ?:number
        // 位置信息 从1开始
                index ?:number
    }
// 组队副本队伍信息 用在打的动画里
    interface TeamDataUnit  {
        // 头像id
                touxiangId ?:number
        // 位置信息 从1开始
                posId ?:number
        // 1队长    2队员
                zhiWei ?:number
        // 昵称
                playerName ?:string
        // 皮肤信息
                piFuDefIdList ?:Array<number>
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }
// 关卡数据
    interface GuanqiaDataUnit  {
        // 关卡名字
                guanqiaName ?:string
        // 地图名 jpg格式
                mapName ?:string
        // 怪物名.png
                guaiwuName ?:string
    }
// 组队副本战斗记录
    interface ZuDuiFuBenFightLogUnit  {
        // 
                name ?:string
        // 
                fightLog ?:string
        // 第几场
                index ?:number
        // 玩家头像ID
                playerTouxiangId ?:number
        // 服务器号 显示用
                showServerNum ?:number
        // 是不是我
                isMe ?:boolean
        // 队伍ID
                duiWuId ?:number
        // 新版玩家个人形象数据
                playerPhoto ?:msgObj.PlayerPhotoUnit
    }

// [请求]小镇面板
    interface TownPanelReq extends ReqObj {
    }

// [返回]小镇面板
    interface TownPanelRsp extends RspObj {
        // 等级
            townLevel ?:number
        // 繁荣度
            townExp ?:number
        // 护盾结束时间
            huDunEndTime ?:number
        // 被抢次数
            beiQiangCount ?:number
        // 小镇已解锁房间信息列表
            openRoomInfoList ?:Array<TownRoomInfoOpenUnit>
        // 小镇未解锁房间信息列表
            unOpenRoomInfoList ?:Array<TownRoomInfoUnOpenUnit>
        // 产出列表
            productShowList ?:Array<TownProductShowUnit>
        // 出征结束时间
            fightEndTime ?:number
        // 被攻击结束时间列表
            beAttackedEndTimeList ?:Array<number>
        // 道馆开启时间
            daoGuanStartTime ?:number
        // 小镇士兵骨骼信息列表
            soldierGugeShowList ?:Array<TownSoldierGugeShowUnit>
        // 总的便当消耗，单位/每小时
            foodCost ?:JiangliInfoUnit
        // 道馆行军结束时间
            daoguanFightEndTime ?:number

    }

// [请求]小镇房屋信息
    interface TownRoomPanelReq extends ReqObj {
        // 房屋类型
            roomType ?:number
    }

// [返回]小镇房屋信息
    interface TownRoomPanelRsp extends RspObj {
        // 房屋名字
            roomName ?:string
        // 当前等级
            curLevel ?:number
        // 当前产出列表
            curProductList ?:Array<TownProductUnit>
        // 下一级产出列表
            nextProductList ?:Array<TownProductUnit>
        // 当前储量
            totalProduct ?:number
        // 当前满仓时间
            curFullTimeStamp ?:number
        // 是否满级
            isFull ?:boolean
        // 消耗列表
            costList ?:Array<JiangliInfoUnit>
        // 资源房屋才有产出类型
            chanChuType ?:number

    }

// [请求]小镇管家信息
    interface TownPersonPanelReq extends ReqObj {
        // 房屋类型
            roomType ?:number
    }

// [返回]小镇管家信息
    interface TownPersonPanelRsp extends RspObj {
        // 当前等级
            curLevel ?:number
        // 当前好感度
            curHaoGanDu ?:number
        // 升级总好感
            needMax ?:number
        // 当前产出列表
            curProductList ?:Array<TownProductUnit>
        // 下一级产出列表
            nextProductList ?:Array<TownProductUnit>
        // 被抢信息
            beiQiangInfo ?:TownBeiQiangInfoUnit
        // 是否满级
            isFull ?:boolean
        // 消耗列表
            costList ?:Array<JiangliInfoUnit>
        // 喊人CD结束时间戳，单位：毫秒
            hanhuaCdEndTime ?:number

    }

// [请求]小镇研究或招募信息
    interface TownYanJiuPanelReq extends ReqObj {
        // 房屋类型
            roomType ?:number
    }

// [返回]小镇研究或招募或伤员信息
    interface TownYanJiuPanelRsp extends RspObj {
        // 房间等级
            roomLevel ?:number
        // 士兵列表
            soldierList ?:Array<TownSoldierUnit>
        // 研究列表
            productList ?:Array<TownKeJiUnit>
        // 伤员列表
            woundedList ?:Array<TownWoundedUnit>
        // 集市兑换列表
            shopConvertList ?:Array<TownShopConvertUnit>

    }

// [请求]小镇升级
    interface GetTownLvUPReq extends ReqObj {
        // 房屋类型
            roomType ?:number
        // 1 房屋 2 管家 3 技能 4 士兵 5 医院伤员
            mType ?:number
        // 技能编号/兵种
            sType ?:number
        // entity db id
            dbId ?:number
    }

// [返回]小镇升级
    interface GetTownLvUPRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]小镇招募
    interface GetTownZhaoMuReq extends ReqObj {
        // 房屋类型
            roomType ?:number
        // 兵种
            soldierType ?:number
        // 招募数量
            count ?:number
    }

// [返回]小镇招募
    interface GetTownZhaoMuRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]释放临时工
    interface ReleaseTownPersonReq extends ReqObj {
        // 临时工Id
            personDbId ?:number
    }

// [返回]释放临时工
    interface ReleaseTownPersonRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]获取士兵和临时工信息
    interface GetSoldierAndPersonShowReq extends ReqObj {
        // 房子类型
            roomType ?:number
    }

// [返回]获取士兵和临时工信息
    interface GetSoldierAndPersonShowRsp extends RspObj {
        // 房子等级
            roomLevel ?:number
        // 总战力
            totalFight ?:number
        // 士兵显示列表
            soldierShowList ?:Array<TownSoldierShowUnit>
        // 抢夺列表
            qiangDuoList ?:Array<TownQiangDuoPersonUnit>
        // 出征结束时间
            fightEndTime ?:number
        // 总的便当消耗，单位/每小时
            foodCost ?:JiangliInfoUnit
        // 付费技能列表
            paySkillList ?:Array<TownPersonPaySkillUnit>

    }

// [请求]开启小镇
    interface OpenTownRoomReq extends ReqObj {
        // 房子类型
            roomType ?:number
    }

// [返回]开启小镇
    interface OpenTownRoomRsp extends RspObj {
        // 解锁后信息
            unlockUnit ?:TownRoomInfoOpenUnit
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]搜索小镇
    interface SearchTownReq extends ReqObj {
        // 搜索类型：0=随机搜索；1=坐标搜索；2邮件跳转；3聊天跳转。默认是0
            searchType ?:number
        // 小镇x坐标
            x ?:number
        // 小镇y坐标
            y ?:number
    }

// [返回]搜索小镇
    interface SearchTownRsp extends RspObj {
        // 搜索类型：0=随机搜索；1=坐标搜索；2邮件跳转；3聊天跳转；4直接夺回搜索。默认是0
            searchType ?:number
        // 我的小镇
            myTown ?:TownSearchPartInfoUnit
        // 小镇列表
            townList ?:Array<TownSearchPartInfoUnit>
        // 搜索价格
            searchPrice ?:JiangliInfoUnit
        // 下次可搜索时间戳，单位：毫秒
            nextSearchTime ?:number
        // 喊人CD结束时间戳，单位：毫秒
            hanhuaCdEndTime ?:number

    }

// [请求]获取该玩家的小镇完整信息
    interface GetFullTownInfoReq extends ReqObj {
        // 玩家角色ID
            playerId ?:number
        // 目标来源类型：0无；1来自复仇目标花钱搜索；2待定
            targetFromType ?:number
    }

// [返回]获取该玩家的小镇完整信息
    interface GetFullTownInfoRsp extends RspObj {
        // 玩家角色ID
            playerId ?:number
        // 总战力
            totalFight ?:number
        // 兵力详情列表
            soldierShowList ?:Array<TownSoldierShowUnit>
        // 上阵宠物列表
            heroList ?:Array<TownSearchHeroInfoUnit>

    }

// [返回]小镇产出推送
    interface TownPushRsp extends RspObj {
        // 推送列表
            pushList ?:Array<TownPushUnit>
        // 额外推送列表
            exrPushList ?:Array<TownProductShowUnit>
        // 数字类推送列表 1 foodCost总的便当消耗，单位/每小时；
            numberValueList ?:Array<NumberValueUnit>

    }

// [请求]购买护盾
    interface BuyTownHuDunReq extends ReqObj {
        // 是否购买折扣护盾
            isDiscount ?:boolean
    }

// [返回]购买护盾
    interface BuyTownHuDunRsp extends RspObj {

    }

// [请求]护盾面板
    interface TownHuDunPanelReq extends ReqObj {
    }

// [返回]护盾面板
    interface TownHuDunPanelRsp extends RspObj {
        // 护盾列表
            huDunList ?:Array<TownHuDunUnit>

    }

// [请求]出征信息
    interface ChuZhengInfoReq extends ReqObj {
        // 目标玩家id
            targetPlayerId ?:number
    }

// [返回]出征信息
    interface ChuZhengInfoRsp extends RspObj {
        // 出征上限
            maxCount ?:number
        // 兵种列表
            soliderList ?:Array<TownSoldierShowUnit>
        // 可掠夺资源列表
            canGetResources ?:Array<JiangliInfoUnit>
        // 行军时间
            moveTime ?:number
        // 剩余次数
            leftTimes ?:number

    }

// [请求]开始战斗
    interface StartTownFightReq extends ReqObj {
        // 目标玩家id
            targetPlayerId ?:number
        // 兵种列表
            soliderList ?:Array<TownSoldierShowUnit>
        // 喊人聊天消息中的QiangDuoRecordId，type=3时必传该值
            messageQiangDuoRecordId ?:number
        // 战斗类型：1、普通战斗；2、直接夺回管家；3、喊人帮忙；
            type ?:number
        // 被抢走的房屋类型，type=2时必传该值
            beiqiangRoomType ?:number
        // 玩家类型：1真实玩家；2NPC
            targetPlayerType ?:number
    }

// [返回]开始战斗
    interface StartTownFightRsp extends RspObj {
        // 是否成功
            result ?:boolean
        // 出征结束时间
            fightEndTime ?:number

    }

// [请求]观看战斗结果
    interface WatchTownFightLogReq extends ReqObj {
        // 日志id
            logId ?:number
    }

// [返回]观看战斗结果
    interface WatchTownFightLogRsp extends RspObj {
        // 全部战斗信息的Json串
            fightInfo ?:string

    }

// [请求]观看宠物战斗结果
    interface WatchPetFightLogReq extends ReqObj {
        // 日志id
            logId ?:number
    }

// [返回]观看宠物战斗结果
    interface WatchPetFightLogRsp extends RspObj {
        // 全部战斗信息的Json串
            fightInfo ?:string

    }

// [返回]小镇被攻击
    interface TownBeAttackedRsp extends RspObj {
        // 被攻击时间
            beAttackedTime ?:number
        // 被攻击结束时间
            beAttackedEndTime ?:number

    }

// [请求]查看袭击者列表
    interface ShowAttackerListReq extends ReqObj {
    }

// [返回]查看袭击者列表
    interface ShowAttackerListRsp extends RspObj {
        // 攻击者列表
            attackerInfoList ?:Array<TownAttackerInfoUnit>

    }

// [请求]出征喊人
    interface TownHanhuaOnChuzhengReq extends ReqObj {
        // 目标Id
            targetPlayerId ?:number
        // 喊人类型 1搜索敌情界面；2临时工管家界面
            type ?:number
        // 临时工管家，抢夺者X坐标
            qiangduoPosX ?:number
        // 临时工管家，抢夺者y坐标
            qiangduoPosY ?:number
        // 被抢走的房屋类型，type=2时必传该值
            beiqiangRoomType ?:number
    }

// [请求]出征喊人
    interface TownHanhuaOnChuzhengRsp extends RspObj {
        // 喊人是否成功
            isSuccess ?:boolean
        // 喊人CD结束时间戳，单位：毫秒
            hanhuaCdEndTime ?:number

    }

// [请求]答应援助后的条件检查
    interface TownCheckOnHanhuaReq extends ReqObj {
        // 喊人聊天消息中的QiangDuoRecordId
            qiangDuoRecordId ?:number
    }

// [返回]答应援助后的条件检查
    interface TownCheckOnHanhuaRsp extends RspObj {
        // 目标Id
            targetPlayerId ?:number
        // x坐标
            x ?:number
        // y坐标
            y ?:number
        // 喊人聊天消息中的QiangDuoRecordId
            qiangDuoRecordId ?:number

    }

// [返回]小镇管家被抢走或夺回时，推送新的数据给前端
    interface TownRoomUpdateRsp extends RspObj {
        // 小镇已解锁房间信息列表
            roomInfoUnit ?:TownRoomInfoOpenUnit

    }

// [请求]小镇道馆主面板
    interface TownDaoGuanMainPanelReq extends ReqObj {
    }

// [返回]小镇道馆主面板
    interface TownDaoGuanMainPanelRsp extends RspObj {
        // 开始时间
            startTime ?:number
        // 结束时间
            endTime ?:number
        // 道馆列表
            daoGuanList ?:Array<TownDaoGuanUnit>
        // 我驻守的道馆
            myDaoGuan ?:TownDaoGuanUnit
        // 行军信息
            xingJunInfo ?:TownDaoGuanPlayerXingJunUnit

    }

// [请求]小镇道馆面板详情攻击方
    interface TownDaoGuanDetailPanelForAttackReq extends ReqObj {
        // 道馆Id
            defId ?:number
    }

// [返回]小镇道馆面板详情攻击方
    interface TownDaoGuanDetailPanelForAttackRsp extends RspObj {
        // 道馆信息
            daoGuanInfo ?:TownDaoGuanDetailUnit
        // 喊人cd结束时间
            hanRenCdEndTime ?:number

    }

// [请求]获取攻防信息
    interface GetTownAttackInfoReq extends ReqObj {
        // 道馆Id
            defId ?:number
    }

// [返回]获取攻防信息
    interface GetTownAttackInfoRsp extends RspObj {
        // 在途列表
            attackingList ?:Array<TownDaoGuanAttackingUnit>
        // 攻击历史
            logList ?:Array<TownDaoGuanAttackLogUnit>

    }

// [请求] 小镇道馆出征
    interface GetTownDaoGuanAttackReq extends ReqObj {
        // 道馆Id
            defId ?:number
        // 兵种列表
            soliderList ?:Array<TownSoldierShowUnit>
        // 聊天点击过来的
            recordId ?:number
        // 出征分类：1、小镇道馆出征；2、攻击入侵者。默认值为1，默认值可以不传
            type ?:number
    }

// [返回]小镇道馆出征
    interface GetTownDaoGuanAttackRsp extends RspObj {
        // 返回码 0成功
            returnCode ?:number

    }

// [请求] 小镇道馆驻守
    interface GetTownDaoGuanDefenseReq extends ReqObj {
        // 道馆Id
            defId ?:number
        // 兵种列表
            soliderList ?:Array<TownSoldierShowUnit>
        // 聊天点击过来的
            recordId ?:number
    }

// [返回]小镇道馆驻守
    interface GetTownDaoGuanDefenseRsp extends RspObj {
        // 返回码 0成功
            returnCode ?:number

    }

// [请求] 请求出征或驻守
    interface GetTownDaoGuanAttackOrDefenseReq extends ReqObj {
        // 道馆Id
            defId ?:number
        // 是出征还是驻守：1出征；2驻守
            attackDefenseSide ?:number
    }

// [返回]请求出征或驻守
    interface GetTownDaoGuanAttackOrDefenseRsp extends RspObj {
        // 上限
            max ?:number
        // 兵种列表
            soliderList ?:Array<TownSoldierShowUnit>

    }

// [请求]小镇道馆面板详情防守方
    interface TownDaoGuanDetailPanelForDefenseReq extends ReqObj {
        // 道馆Id
            defId ?:number
    }

// [返回]小镇道馆面板详情防守方
    interface TownDaoGuanDetailPanelForDefenseRsp extends RspObj {
        // 道馆信息
            daoGuanInfo ?:TownDaoGuanDetailUnit
        // 驻守人员列表
            playerList ?:Array<TownDaoGuanPlayerUnit>
        // 喊人cd结束时间
            hanRenCdEndTime ?:number

    }

// [请求]撤回驻守方
    interface CallBackTownGuanDefenserReq extends ReqObj {
        // 目标玩家
            goalPlayerId ?:number
    }

// [返回]撤回驻守方
    interface CallBackTownGuanDefenserRsp extends RspObj {
        // 返回码
            returnCode ?:number

    }

// [请求]放弃道馆
    interface GiveUpTownDaoGuanReq extends ReqObj {
        // 道馆Id
            daoGuanId ?:number
    }

// [返回]放弃道馆
    interface GiveUpTownDaoGuanRsp extends RspObj {
        // 返回码
            returnCode ?:number

    }

// [请求]小镇道馆赛季排名
    interface TownDaoGuanSeasonPaiMingReq extends ReqObj {
    }

// [返回]小镇道馆赛季排名
    interface TownDaoGuanSeasonPaiMingRsp extends RspObj {
        // 参与服务器列表
            serverNumList ?:Array<number>
        // 公会列表
            gongHuiList ?:Array<TownDaoGuanGongHuiForPaiMingUnit>
        // 我的公会
            myGongHui ?:TownDaoGuanGongHuiForPaiMingUnit

    }

// [请求]小镇道馆赛季排名奖励
    interface GetTownDaoGuanSeasonPaiMingRewardListReq extends ReqObj {
    }

// [返回]小镇道馆赛季排名奖励
    interface GetTownDaoGuanSeasonPaiMingRewardListRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<TownDaoGuanSeasonJiangLiUnit>

    }

// [请求]小镇道馆首次占领奖励
    interface GetTownDaoGuanFirstRewardListReq extends ReqObj {
    }

// [返回]小镇道馆首次占领奖励
    interface GetTownDaoGuanFirstRewardListRsp extends RspObj {
        // 奖励列表
            firstJiangLiList ?:Array<TownDaoGuanFirstJiangLiUnit>

    }

// [返回]小镇道馆信息发生变化，攻击、护盾、占领状态等
    interface TownDaoGuanDataChangeRsp extends RspObj {
        // 发生变化的道馆
            daoGuan ?:TownDaoGuanUnit
        // 正数表示我新驻守的道馆，-1表示我驻守被撤回或者驻守部队被消灭
            myDaoGuanDefId ?:number
        // 行军信息
            xingJunInfo ?:TownDaoGuanPlayerXingJunUnit
        // 变化原因类型：1、小镇道馆变化；2、反击入侵者引起的行军变化。
            changeTpye ?:number

    }

// [返回]成员贡献
    interface GetTownDaoGuanGongXianPaiHangReq extends ReqObj {
        // 1 今日 2 赛季
            tag ?:number
    }

// [返回]成员贡献
    interface GetTownDaoGuanGongXianPaiHangRsp extends RspObj {
        // 贡献列表
            paiHangList ?:Array<TownDaoGuanGongXianUnit>

    }

// [返回]小镇道馆攻击喊人
    interface TownDaoGuanAttackHanRenReq extends ReqObj {
        // 道馆Id
            daoGuanId ?:number
    }

// [返回]小镇道馆攻击喊人
    interface TownDaoGuanAttackHanRenRsp extends RspObj {
        // cd结束时间
            cdEndTime ?:number

    }

// [返回]小镇道馆驻守喊人
    interface TownDaoGuanDefenseHanRenReq extends ReqObj {
        // 道馆Id
            daoGuanId ?:number
    }

// [返回]小镇道馆驻守喊人
    interface TownDaoGuanDefenseHanRenRsp extends RspObj {
        // cd结束时间
            cdEndTime ?:number

    }

// [返回]小镇道馆响应聊天验证
    interface TownDaoGuanClickCheckReq extends ReqObj {
        // recordId
            recordId ?:number
    }

// [返回]小镇道馆响应聊天验证
    interface TownDaoGuanClickCheckRsp extends RspObj {
        // 道馆Id
            daoGuanId ?:number
        // 返回值
            returnCode ?:number

    }

// [请求]小镇集市兑换
    interface GetTownShopConvertReq extends ReqObj {
        // 策划表Id
            defId ?:number
        // 兑换后的奖励内容
            reward ?:JiangliInfoUnit
    }

// [返回]小镇集市兑换
    interface GetTownShopConvertRsp extends RspObj {
        // 兑换后的奖励内容
            reward ?:JiangliInfoUnit
        // 剩余可兑换次数
            number ?:number
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]小镇道馆战斗历史记录信息
    interface GetTownDaoGuanFightRecordsReq extends ReqObj {
        // 道馆Id
            daoGuanId ?:number
    }

// [返回]小镇道馆战斗历史记录信息
    interface GetTownDaoGuanFightRecordsRsp extends RspObj {
        // 战斗历史记录集合
            fightRecordList ?:Array<TownDaoGuanFightRecordUnit>

    }

// [请求]小镇道馆战斗历史记录明细
    interface GetTownDaoGuanFightRecordDetailReq extends ReqObj {
        // 战斗记录Id
            id ?:number
    }

// [返回]小镇道馆战斗历史记录明细
    interface GetTownDaoGuanFightRecordDetailRsp extends RspObj {
        // 战斗明细数据
            detailInfo ?:TownDaoGuanFightRecordDetailUnit

    }

// [请求]小镇道馆入侵出征界面
    interface GetTownDaoGuanRuqinChuzhengReq extends ReqObj {
        // 道馆Id
            daoGuanId ?:number
    }

// [返回]小镇道馆入侵出征界面
    interface GetTownDaoGuanRuqinChuzhengRsp extends RspObj {
        // 概率掉落奖励集合
            jiangliList ?:Array<JiangliInfoUnit>
        // 入侵者士兵集合
            soldierList ?:Array<TownSoldierShowUnit>
        // 入侵者宠物精灵集合，只传值：heroId、level、star三个字段，其他字段都是undefined或者默认值！
            enemyHeroList ?:Array<SimHeroInfoUnit>
        // 入侵者宠物精灵总战力
            zhanli ?:number
        // 今日剩余可出征次数
            fightCount ?:number

    }

// [请求]收集小镇房屋奖励到背包
    interface CollectTownRoomRewardsToItemPackageReq extends ReqObj {
        // 房屋类型
            roomType ?:number
    }

// [返回]收集小镇房屋奖励到背包
    interface CollectTownRoomRewardsToItemPackageRsp extends RspObj {
        // 本次采集到的奖励类型列表
            rewardTypeList ?:Array<number>

    }

// [推送]小镇房屋付费技能更新
    interface PushTownRoomPaySkillRsp extends RspObj {
        // 房屋类型
            roomType ?:number
        // 新激活的付费技能Id
            activeSkillId ?:number

    }

// 【请求】面板
    interface GetPanelReq extends ReqObj {
    }

// 【反馈】面板
    interface GetPanelRsp extends RspObj {
        // 该次是否免费
            isFree ?:boolean

    }

// 【请求】兑奖
    interface DuiJiangReq extends ReqObj {
        // 0 机选 1 多选
            type ?:number
        // 红球[1,2,3,4,5,6,7]
            redList ?:Array<number>
        // 蓝球[8,9,10] 篮球显示的时候号码-7
            blueList ?:Array<number>
    }

// 【反馈】兑奖
    interface DuiJiangRsp extends RspObj {
        // 中奖序列
            finSeq ?:Array<number>
        // 我的序列
            mySeq ?:Array<number>
        // 1-5等奖 中出的个数
            rewardCount ?:Array<number>
        // 奖励信息
            reward ?:Array<ShuangCaiQiuRewardInfoUnit>

    }

// 【请求】职业神器面板
    interface ZhiYeShenQiPanelReq extends ReqObj {
    }

// 【反馈】职业神器面板
    interface ZhiYeShenQiPanelRsp extends RspObj {
        // 神器列表
            shenQiList ?:Array<ZhiYeShenQiUnit>

    }

// 【请求】操作神器
    interface ChangeZhiYeShenQiReq extends ReqObj {
        // 1 穿上 2更换 3 卸下 
            operateType ?:number
        // 神器DBID
            shenQiDbId ?:number
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。上下阵都要赋值
            heroIndex ?:number
    }

// 【反馈】操作神器
    interface ChangeZhiYeShenQiRsp extends RspObj {
        // 是否操作成功
            isSuccess ?:boolean

    }

// 【请求】神器升级面板
    interface ZhiYeShenQiShengJiPanelReq extends ReqObj {
        // 神器ID
            shenQiDbId ?:number
    }

// 【反馈】神器升级面板
    interface ZhiYeShenQiShengJiPanelRsp extends RspObj {
        // 当前基本属性
            curBasicShuXingList ?:Array<ShuXingInfoUnit>
        // 当前职业属性
            curZhiYeShuXingList ?:Array<ShuXingInfoUnit>
        // 当前buffId
            curBuffId ?:number
        // 下一级基本属性列表
            nextBasicShuXingList ?:Array<ShuXingInfoUnit>
        // 下一级职业属性列表
            nextZhiYeShuXingList ?:Array<ShuXingInfoUnit>
        // 下一级buffId
            nextBuffId ?:number
        // 消耗材料
            costItem ?:number
        // 消耗数量
            costCount ?:number
        // 是否满级
            isFull ?:boolean

    }

// 【请求】职业神器升级
    interface GetZhiYeShenQiLvUpReq extends ReqObj {
        // 神器Id
            shenQiDbId ?:number
    }

// 【反馈】职业神器升级
    interface GetZhiYeShenQiLvUpRsp extends RspObj {
        // 是否升级成功
            isSuccess ?:boolean

    }

// 【请求】神器回收预览
    interface ShenQiHuiShouViewReq extends ReqObj {
        // 回收的神器dbIDs
            shenQiDbIds ?:Array<number>
    }

// 【反馈】神器回收预览
    interface ShenQiHuiShouViewRsp extends RspObj {
        // 回收奖励
            jiangList ?:Array<JiangliInfoUnit>

    }

// 【请求】神器回收
    interface GetShenQiHuiShouReq extends ReqObj {
        // 回收的神器dbIDs
            shenQiDbIds ?:Array<number>
    }

// 【反馈】神器回收
    interface GetShenQiHuiShouRsp extends RspObj {
        // 回收奖励
            jiangList ?:Array<JiangliInfoUnit>

    }

// 【请求】新神技背包列表
    interface GetAllMagicalSkillItemListReq extends ReqObj {
    }

// 【反馈】新神技背包列表
    interface GetAllMagicalSkillItemListRsp extends RspObj {
        // 神技背包物品列表
            itemList ?:Array<MagicalSkillItemUnit>

    }

// 【请求】神技卡穿上卸下操作
    interface MagicalSkillPutOnAndUnloadReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。上下阵都要赋值
            heroIndex ?:number
        // 本次操作的神技位置Index
            magicalSkillIndex ?:number
        // 1、上阵或者替换卡；2、下阵
            opType ?:number
        // 分类：19、来自背包，未穿戴状态，可叠加；1005、来自神技卡，已穿戴
            rewardType ?:number
        // 操作的技能卡或者背包物品dbID
            dbId ?:number
    }

// 【反馈】神技卡穿上卸下操作
    interface MagicalSkillPutOnAndUnloadRsp extends RspObj {
        // 是否操作成功
            isSuccess ?:boolean
        // 穿上去的分类：19、来自背包，未穿戴状态，可叠加；1005、来自神技卡，已穿戴
            rewardType ?:number
        // 穿上去的技能卡或者背包物品dbID
            dbId ?:number
        // 本次操作的神技位置Index
            magicalSkillIndex ?:number

    }

// 【请求】神技属性查看
    interface MagicalSkillCardViewReq extends ReqObj {
        // 分类：19、来自背包，未穿戴状态，可叠加；1005、来自神技卡，已穿戴
            rewardType ?:number
        // 要查看的技能卡或者背包物品dbID
            dbId ?:number
    }

// 【反馈】神技属性查看
    interface MagicalSkillCardViewRsp extends RspObj {
        // 要查看的神技卡信息
            cardInfo ?:MagicalSkillCardUnit

    }

// 【请求】已穿戴神技卡升阶升级操作
    interface MagicalSkillLevelUpReq extends ReqObj {
        // 操作的技能卡dbID
            magicSkillDbId ?:number
    }

// 【反馈】已穿戴神技卡升阶升级操作
    interface MagicalSkillLevelUpRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 升阶升级后的神技卡信息
            cardInfo ?:MagicalSkillCardUnit

    }

// 【请求】神技卡一键穿上
    interface MagicalSkillOneKeyPutOnReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。上下阵都要赋值
            heroIndex ?:number
        // 对应的神技道具DbId列表。
            dbIdList ?:Array<number>
    }

// 【反馈】神技卡一键穿上
    interface MagicalSkillOneKeyPutOnRsp extends RspObj {

    }

// [请求]转生面板
    interface ZhuanShengPanelReq extends ReqObj {
    }

// [返回]转生面板
    interface ZhuanShengPanelRsp extends RspObj {
        // 本级属性信息
            curPropertyInfo ?:Array<ShuXingInfoUnit>
        // 下一级属性信息
            nextPropertyInfos ?:Array<ShuXingInfoUnit>
        // 当前几转
            curLevel ?:number
        // 预览下一级是几转
            nextLevel ?:number
        // 是否已经满级
            isFull ?:boolean
        // 已经点了几下
            hasExp ?:number
        // 需要点几下能达到满级
            needExp ?:number
        // 进阶消耗
            costList ?:Array<JiangliInfoUnit>
        // 是否突破
            isTuPo ?:boolean
        // 突破人物等级
            tuPoLimit ?:number
        // 特效
            teXiao ?:number

    }

// [请求]转生
    interface GetZhuanShengReq extends ReqObj {
        // 1 普通转生 2 一件转生
            type ?:number
    }

// [返回]转生
    interface GetZhuanShengRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]转生技能面板
    interface ZhuanShengSkillPanelReq extends ReqObj {
    }

// [返回]转生技能面板
    interface ZhuanShengSkillPanelRsp extends RspObj {
        // 技能列表
            skillList ?:Array<ZhuanShengSkillUnit>

    }

// [请求]转生技能升级
    interface GetZhuanShengSkillReq extends ReqObj {
        // 技能类型
            type ?:number
    }

// [返回]转生技能升级
    interface GetZhuanShengSkillRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]转生天赋面板
    interface ZhuanShengTalentPanelReq extends ReqObj {
    }

// [返回]转生天赋面板
    interface ZhuanShengTalentPanelRsp extends RspObj {
        // 天赋列表
            talentsList ?:Array<ZhuanShengTalentUnit>
        // 当前几转
            curLevel ?:number

    }

// [请求]激活转生天赋
    interface GetZhuanShengTalentReq extends ReqObj {
        // 天赋Id
            talentId ?:number
    }

// [返回]激活转生天赋
    interface GetZhuanShengTalentRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]GetZhuanShengRedPoint
    interface GetZhuanShengRedPointReq extends ReqObj {
    }

// [返回]GetZhuanShengRedPoint
    interface GetZhuanShengRedPointRsp extends RspObj {
        // 主角转生红点
            zhuJue ?:boolean
        // 转生技能红点
            skill ?:boolean
        // 技能转生红点
            talent ?:boolean

    }

// [请求]不思议礼包面板
    interface BuSiYiLiBaoPanelReq extends ReqObj {
    }

// [返回]不思议礼包面板
    interface BuSiYiLiBaoPanelRsp extends RspObj {
        // 
            buSiYiLiBaoList ?:Array<BuSiYiLiBaoUnit>

    }

// [请求]不思议礼包出现在挂机页面
    interface BuSiYiLiBaoShowRsp extends RspObj {

    }

// [请求] 2.进入传送门界面挂机状态
    interface EnterChuanSongMenReq extends ReqObj {
    }

// [反馈]   2.进入传送门界面挂机状态
    interface EnterChuanSongMenRsp extends RspObj {
        // 挂机类型 1 2 3 0表示没挂机
            guajiType ?:number

    }

// [请求]   3.传送门界面 点击切换空间 开始搜索敌人
    interface SearchEnemyReq extends ReqObj {
        // 挂机类型 1 2 3 0表示没挂机
            guajiType ?:number
    }

// [反馈]   3.传送门界面 点击切换空间 开始搜索敌人
    interface SearchEnemyRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// [请求]   4.传送门界面 观看战斗
    interface GuaJiFightReq extends ReqObj {
    }

// [反馈]   4.传送门界面 观看战斗
    interface GuaJiFightRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]   5.不看战斗受益请求
    interface StartGuaJiWithoutFightReq extends ReqObj {
        // 1 战斗挂机的奖励  2 不战斗挂机的奖励
            flg ?:number
    }

// [反馈]   5.不看战斗受益请求
    interface StartGuaJiWithoutFightRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 1 战斗挂机的奖励  2 不战斗挂机的奖励
            flg ?:number
        // 打了多少波小怪
            fightTimes ?:number

    }

// [请求]展示宝箱  6.展示宝箱 
    interface ShowChestReq extends ReqObj {
    }

// [反馈]展示宝箱  6.展示宝箱 
    interface ShowChestRsp extends RspObj {
        // 挂机时间
            guaJiTime ?:string
        // 宝箱内元素列表
            elementList ?:Array<GuaJiShowChestElementUnit>
        // 比率列表
            ratioList ?:Array<RatioDataListUnit>

    }

// [请求]领取宝箱 7.领取宝箱
    interface GetChestReq extends ReqObj {
    }

// [反馈]领取宝箱 7.领取宝箱
    interface GetChestRsp extends RspObj {
        // 挂机时间
            guaJiTime ?:string
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]快速战斗信息
    interface KuaiSuInfoReq extends ReqObj {
    }

// [反馈]快速战斗信息
    interface KuaiSuInfoRsp extends RspObj {
        // 已经买了多少次快速战斗
            buyTimes ?:number
        // 总次数
            maxTimes ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]购买快速战斗
    interface BuyKuaiSuReq extends ReqObj {
        // 1 钻石 2 金券
            type ?:number
    }

// [反馈]购买快速战斗
    interface BuyKuaiSuRsp extends RspObj {
        // 已经买了多少次快速战斗
            buyTimes ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 多倍奖励加成系数
            jiaChengXiShu ?:number

    }

// 被抢以后
    interface DoBeiQiangActionReq extends ReqObj {
        // 抢夺记录Id
            qiangDuoRecordId ?:number
        // 1 查看战报 2 夺回奖励 3 喊人帮忙
            sType ?:number
    }

// 被抢以后
    interface DoBeiQiangActionRsp extends RspObj {
        // 1 查看战报 2 夺回奖励 3 喊人帮忙
            sType ?:number
        // 战斗信息
            fightInfo ?:string
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 1记录过期 2已经抢夺成功 3已经被援助成功 4请求援助成功
            status ?:number

    }

// 被援助成功
    interface PushBeiYuanZhuSuccessRsp extends RspObj {
        // 援助我的玩家信息
            yuanZhuPlayerInfo ?:PaihangInfoUnit
        // 帮我抢夺回来的奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 援助的抢夺记录Id
            qiangDuoRecordId ?:number

    }

// 援助抢夺
    interface YuanZhuQiangDuoReq extends ReqObj {
        // 抢夺记录Id
            qiangDuoRecordId ?:number
    }

// 援助抢夺
    interface YuanZhuQiangDuoRsp extends RspObj {
        // 战斗信息
            fightInfo ?:string
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 1记录过期 2已经抢夺成功 3已经被援助成功 4援助成功
            status ?:number

    }

// 请求新的剧情线（进入挂机地图1分钟后请求）
    interface MakeNewStoryLineReq extends ReqObj {
    }

// 推送新的剧情线
    interface MakeNewStoryLineRsp extends RspObj {
        // 新的剧情
            newStoryLine ?:StoryLineUnit
        // 是否已经最后一个剧情线了
            isTheLastOne ?:boolean

    }

// 清理所有剧情的推送
    interface ClearAllStoryLineRsp extends RspObj {

    }

// 更新剧情线状态
    interface UpdateStoryLineStatusReq extends ReqObj {
        // 1、2、点击；13、暂时搁置；14、放弃
            type ?:number
        // 是否点击领取奖励
            isGetReward ?:boolean
        // 剧情线Entity DB Id
            dbId ?:number
    }

// 更新剧情线状态
    interface UpdateStoryLineStatusRsp extends RspObj {
        // 1、2、点击；13、暂时搁置；14、放弃
            type ?:number
        // 是否点击领取奖励
            isGetReward ?:boolean
        // 剧情线Entity DB Id
            dbId ?:number
        // 如果本次点击领奖励，则返回奖励列表，否则为空
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 推送新的老乞丐对话
    interface MakeNewLaoqigaiDuihuaRsp extends RspObj {
        // 新的老乞丐剧情
            newLaoqigaiDuihua ?:LaoqigaiDuihuaUnit

    }

// 老乞丐消耗提交
    interface LaoqigaiCostReq extends ReqObj {
        // 老乞丐表Id，对应策划表：laoqigai
            defId ?:number
        // 老乞丐消耗列表Index，从0到N-1
            costIndex ?:number
    }

// 老乞丐消耗提交
    interface LaoqigaiCostRsp extends RspObj {
        // 老乞丐表Id，对应策划表：laoqigai
            defId ?:number
        // 老乞丐消耗列表Index，从0到N-1
            costIndex ?:number
        // 本次选择的奖励
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 领取老乞丐的翻倍奖励
    interface GetLaoqigaiRewardReq extends ReqObj {
        // 老乞丐表Id，对应策划表：laoqigai
            defId ?:number
    }

// 领取老乞丐的翻倍奖励
    interface GetLaoqigaiRewardRsp extends RspObj {
        // 老乞丐表Id，对应策划表：laoqigai
            defId ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 挂机页面的功能开启和结束状态列表
    interface GetProjectStatusReq extends ReqObj {
    }

// 挂机页面的功能开启和结束状态列表
    interface GetProjectStatusRsp extends RspObj {
        // Project状态列表
            projectStatusList ?:Array<ProjectStatusUnit>

    }

// 查看对方的荣誉值
    interface GetPlayerHonourScoreReq extends ReqObj {
        // 对方Id
            targetPlayerId ?:number
    }

// 查看对方的荣誉值
    interface GetPlayerHonourScoreRsp extends RspObj {
        // 对方Id
            targetPlayerId ?:number
        // 区服编号
            targetServerNum ?:number
        // 对方昵称
            targetPlayerName ?:string
        // 对方的荣誉值信息
            honourInfo ?:JiangliInfoUnit

    }

// 确认订阅消息
    interface ConfirmToSubscribeMessageReq extends ReqObj {
        // 订阅的模板id，如果有模板id的话。目前就离线挂机收益满的模版
            templateId ?:string
        // 是否一次性模版
            isOneTime ?:boolean
    }

// [请求]挂机抓宠的list
    interface GuaJiZhuaChongListReq extends ReqObj {
    }

// [反馈]挂机抓宠的list
    interface GuaJiZhuaChongListRsp extends RspObj {
        // 到了这个时间就跑了,秒
            time ?:number
        // 宠物价格信息
            guaJiZhuaChongInfoList ?:Array<ZhuaChongPriceInfoUnit>

    }

// [请求]抓
    interface GuaJiZhuaChongZhuaReq extends ReqObj {
        // 精灵球type
            jingLingQiuType ?:number
        // 如果是高级球 这个参数是抓宠概率
            percentage ?:number
    }

// [返回]抓
    interface GuaJiZhuaChongZhuaRsp extends RspObj {
        // 是否成功的标志
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]提示
    interface GuaJiZhuaChongZhuaTipsReq extends ReqObj {
    }

// [返回]提示
    interface GuaJiZhuaChongZhuaTipsRsp extends RspObj {
        // 1,2,3,4,5
            defIds ?:string

    }

// [请求]战败分析点哪个充值项目导致的弹框
    interface ZhanBaiFenXiChongZhiKuangReq extends ReqObj {
        // 战败分析的jump
            jump ?:number
    }

// [请求]上传任务打点数据
    interface UpdataTaskDotReq extends ReqObj {
        // 1,2,3,4,5,6
            step ?:number
        // 渠道
            channel ?:string
        // 子渠道
            subchannel ?:string
        // 是否完成
            isComplete ?:boolean
    }

// [返回]上传任务打点数据
    interface UpdataTaskDotRsp extends RspObj {
        // 1成功
            ret ?:number

    }

// 【请求】凯旋丰碑
    interface KaixuanFengbeiPanelReq extends ReqObj {
    }

// 【反馈】凯旋丰碑
    interface KaixuanFengbeiPanelRsp extends RspObj {
        // 榜单信息列表
            rankList ?:Array<KaixuanFengbeiRankInfoUnit>

    }

// 【请求】丰碑榜单列表
    interface GetFengbeiRankReq extends ReqObj {
        // 榜单类型
            type ?:number
    }

// 【反馈】丰碑榜单列表
    interface GetFengbeiRankRsp extends RspObj {
        // 排行信息列表
            rankList ?:Array<PaihangInfoUnit>
        // 榜单类型
            type ?:number
        // 榜单名字或者美术图名
            title ?:string
        // 是否有可领奖励
            isRewardRed ?:boolean

    }

// 【请求】丰碑榜单列表
    interface GetFengbeiRewardListReq extends ReqObj {
        // 榜单类型
            type ?:number
    }

// 【反馈】丰碑榜单列表
    interface GetFengbeiRewardListRsp extends RspObj {
        // 领奖列表
            rewardList ?:Array<KaixuanFengbeiRankRewardUnit>
        // 榜单类型
            type ?:number

    }

// 【请求】领取丰碑榜单奖励
    interface ClaimFengbeiRewardReq extends ReqObj {
        // FirstOneDef.id
            defId ?:number
    }

// 【反馈】领取丰碑榜单奖励
    interface ClaimFengbeiRewardRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // FirstOneDef.id
            defId ?:number

    }

// 【请求】丰碑奖励前5名玩家
    interface GetFengbeiFirstFiveReq extends ReqObj {
        // FirstOneDef.id
            defId ?:number
    }

// 【反馈】丰碑奖励前5名玩家
    interface GetFengbeiFirstFiveRsp extends RspObj {
        // 玩家列表
            playerList ?:Array<KaixuanFengbeiPlayerUnit>
        // FirstOneDef.id
            defId ?:number

    }

// 【请求】进入场景
    interface PlayerEnterSceneReq extends ReqObj {
        // 场景类型: 1个人PVP
            sceneType ?:number
        // 个人PVP地图层级,55(默认值),75,95,115,135,即开放等级,依次+20
            sceneLevel ?:number
        // 追踪的玩家Id
            goalPlayerId ?:number
        // 追踪的玩家区服长ID
            goalPlayerServerId ?:number
    }

// 【反馈】进入场景
    interface PlayerEnterSceneRsp extends RspObj {
        // 返回编码, 0表示成功进入, 其他返回号待定
            returnCode ?:number
        // 场景唯一id
            intId ?:number
        // 服务器id
            serverId ?:number
        // 场景ID
            sceneId ?:number
        // 玩家初始坐标
            location ?:PointUnit
        // 玩家当前血量比例,0~10000
            hpValue ?:number
        // 玩家当前场景内的战斗力
            fight ?:number
        // 玩家状态, 6表示战斗中, 7表示已死亡
            playerStatus ?:number
        // 战斗动画结束时间,单位:秒,只有战斗中才传递此参数
            fightFinishTime ?:number
        // 战斗对手ID,只有战斗中才传递此参数
            fightTargetId ?:number
        // 开始移动时间
            startTime ?:number
        // 移动路径
            pathList ?:Array<PointUnit>
        // 方向
            directionEnd ?:number
        // 追踪的玩家ID, 0表示没有
            followTargetId ?:number
        // 追踪玩家的收益,玩家可能掉落的额外货币,其他人可拾取
            followTargetLostMoney ?:number
        // 当前场景中的玩家数据
            playerList ?:Array<PlayerLocationInfoUnit>
        // 当前场景中的怪物数据
            monsterList ?:Array<MonsterLocationInfoUnit>
        // 当前场景中的箱子数据
            boxList ?:Array<BoxInfoUnit>
        // 当前场景中的陷阱数据
            trapList ?:Array<TrapInfoUnit>
        // 移动速度,既每秒移动距离
            speed ?:number
        // 加速状态结束时间,可能为0,表示未在加速状态,单位:毫秒
            speedUpEndTime ?:number
        // 下次可加速时间戳,可能为0;单位:秒
            speedUpNextTime ?:number
        // 追击距离,格子数
            followRange ?:number
        // 原地复活钻石消耗
            reliveCost ?:number
        // 重生倒计时,倒计时结束后,随机点自动重生,单位:秒
            reliveTime ?:number
        // 加速钻石消耗
            speedUpCost ?:number
        // 加速CD持续时间,单位:秒
            duringTime ?:number
        // 钻石加速功能冷却时间,单位:秒
            speedUpCD ?:number
        // 敌人名字
            targetName ?:string
        // 连杀次数
            successiveWinSize ?:number
        // 杀人数
            killNum ?:number
        // 活动结束时间
            endTime ?:number
        // 公会徽章-只有公会铁拳场景下发
            gongHuiHuiZhang ?:string
        // 剩余获奖次数
            leftGetRewardCount ?:number
        // 击杀单份奖励列表
            killOneJiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】离开场景
    interface PlayerLeaveSceneReq extends ReqObj {
        // 是否正常离开场景
            isLeaveFromMenu ?:boolean
    }

// 【反馈】离开场景
    interface PlayerLeaveSceneRsp extends RspObj {
        // 返回编码, 0表示成功离开,34 玩家参与时间结束(主动推送), 其他返回号待定
            returnCode ?:number

    }

// 【请求】玩家移动
    interface PlayerMoveReq extends ReqObj {
        // 开始移动时间
            startTime ?:number
        // 玩家当前坐标
            location ?:PointUnit
        // 移动路径
            pathList ?:Array<PointUnit>
        // 方向
            directionEnd ?:number
    }

// 【反馈】玩家移动广播
    interface PlayerMoveRsp extends RspObj {
        // 返回编码, 0表示成功进入, 其他返回号待定
            returnCode ?:number

    }

// 【反馈】广播新玩家进入场景了
    interface BroadcastPlayerEnterSceneRsp extends RspObj {
        // 1玩家; 2怪物
            memberType ?:number
        // 新玩家ID
            playerId ?:number
        // 新玩家昵称
            playerName ?:string
        // 新玩家头像
            playerTouxiangId ?:number
        // 新玩家公会ID,如果已经入会的话
            playerGongHuiId ?:number
        // 新玩家公会名称,如果已经入会的话
            playerGongHuiName ?:string
        // 玩家战斗力
            fight ?:number
        // 玩家当前坐标
            location ?:PointUnit
        // 是否当场复活
            isRecallHP ?:boolean
        // VIP等级
            vip ?:number
        // 方向
            directionEnd ?:number
        // 怪物类型,具体数值待定
            monsterType ?:number
        // 玩家状态, 6表示战斗中, 7表示已死亡
            playerStatus ?:number
        // 战斗动画结束时间,单位:秒,只有战斗中才传递此参数
            fightFinishTime ?:number
        // 战斗后剩余血量, 0~10000,只有战斗中才传递此参数
            hpValue ?:number
        // 战斗对手ID,只有战斗中才传递此参数
            fightTargetId ?:number
        // 玩家区服
            playerServerNum ?:number
        // 击杀人数
            killNum ?:number
        // 是否跟我同工会
            isSameGongHui ?:boolean
        // 公会徽章-只有公会铁拳场景下发
            gongHuiHuiZhang ?:string
        // 新版玩家个人形象数据
            playerPhoto ?:PlayerPhotoUnit

    }

// 【反馈】广播玩家离开场景
    interface BroadcastPlayerLeaveSceneRsp extends RspObj {
        // 目标类型
            memberType ?:number
        // 新玩家ID
            playerId ?:number
        // 透传参数：类型为箱子时积分
            ext ?:number

    }

// 【反馈】广播玩家移动路径
    interface BroadcastPlayerMoveRsp extends RspObj {
        // 移动玩家ID
            playerId ?:number
        // 开始移动时间
            startTime ?:number
        // 玩家当前坐标
            location ?:PointUnit
        // 移动路径
            pathList ?:Array<PointUnit>
        // 方向
            directionEnd ?:number

    }

// 【反馈】广播玩家当前坐标纠正
    interface BroadcastPlayerLocationFixRsp extends RspObj {
        // 移动玩家ID
            playerId ?:number
        // 新的玩家坐标,前端收到后,要立即做坐标纠正
            location ?:PointUnit
        // 方向
            directionEnd ?:number

    }

// 【请求】玩家点击其他玩家指令
    interface PlayerClickReq extends ReqObj {
        // 1玩家; 2怪物
            memberType ?:number
        // 目标玩家ID,为0,表示取消跟踪
            playerId ?:number
    }

// 【反馈】点击其他玩家
    interface PlayerClickRsp extends RspObj {
        // 返回编码, 0表示玩家存在, 120表示取消跟踪,其他返回号待定
            returnCode ?:number
        // 剩余血量百分比，1~10000
            hpValue ?:number
        // 玩家状态, 6表示战斗中, 7表示已死亡
            targetStatus ?:number
        // 收益,玩家可能掉落的额外货币,其他人可拾取
            targetLostMoney ?:number
        // 目标玩家ID,为0,表示取消跟踪
            targetId ?:number
        // 目标类型
            memberType ?:number

    }

// 【请求】玩家场景中聊天
    interface PlayerChatInSceneReq extends ReqObj {
        // 目标玩家ID
            words ?:string
    }

// 【反馈】玩家场景中聊天
    interface BroadcastPlayerChatInSceneRsp extends RspObj {
        // 聊天区服
            playerServerNo ?:number
        // 聊天玩家昵称
            playerName ?:string
        // 聊天玩家ID
            playerId ?:number
        // 目标玩家ID
            words ?:string

    }

// 【反馈】玩家场景中战斗
    interface BroadcastPlayerFightInSceneRsp extends RspObj {
        // 发起战斗的玩家ID
            playerId ?:number
        // 被攻击的目标玩家ID
            targetPlayerId ?:number
        // 发起战斗的玩家剩余血量比例,1~10000
            playerHpValue ?:number
        // 被攻击的目标剩余血量比例,1~10000
            targetHpValue ?:number
        // 发起战斗的玩家坐标
            playerLocation ?:PointUnit
        // 被攻击的目标玩家坐标
            targetPlayerLocation ?:PointUnit
        // 发起战斗的玩家方向
            playerDirectionEnd ?:number
        // 被攻击的目标玩家方向
            targetDirectionEnd ?:number
        // 战斗动画结束时间,单位:秒
            fightFinishTime ?:number
        // 玩家状态:6:战斗 9:开箱子
            unitstatus ?:number

    }

// 【反馈】玩家复活
    interface PlayerRecallHPReq extends ReqObj {
        // 消耗钻石数量, 0表示随机点复活,正数表示原地复活
            cost ?:number
    }

// 【反馈】玩家复活
    interface PlayerRecallHPRsp extends RspObj {
        // 玩家当前血量比例,0~10000
            hpValue ?:number
        // 方向
            directionEnd ?:number
        // 玩家初始坐标
            location ?:PointUnit

    }

// 【反馈】玩家场景中战斗内容
    interface PushPlayerFightContentInSceneRsp extends RspObj {
        // 发起战斗的玩家ID
            playerId ?:number
        // 被攻击的目标玩家ID
            targetPlayerId ?:number
        // 发起战斗的玩家剩余血量比例,1~10000
            playerHpValue ?:number
        // 被攻击的目标剩余血量比例,1~10000
            targetHpValue ?:number
        // 发起战斗的玩家坐标
            playerLocation ?:PointUnit
        // 被攻击的目标玩家坐标
            targetPlayerLocation ?:PointUnit
        // 发起战斗的玩家方向
            playerDirectionEnd ?:number
        // 被攻击的目标玩家方向
            targetDirectionEnd ?:number
        // 战斗脚本, Json格式
            fightInfo ?:string
        // 战斗动画结束时间,单位:秒
            fightFinishTime ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】ping值
    interface CheckClientPingTimeReq extends ReqObj {
        // 时间戳
            time ?:number
    }

// 【反馈】ping值
    interface CheckClientPingTimeRsp extends RspObj {
        // 时间戳
            time ?:number
        // 上次ping的时间戳
            prePingTime ?:number

    }

// 【请求】结束战斗状态
    interface FinishFightStatusReq extends ReqObj {
    }

// 【反馈】结束战斗状态
    interface FinishFightStatusRsp extends RspObj {
        // 返回值,0表示会成功,1表示服务器端已经脱离战斗状态，客户端自行脱离战斗状态
            returnCode ?:number
        // 玩家状态, 6表示战斗中, 7表示已死亡
            playerStatus ?:number
        // 敌人名字
            targetName ?:string
        // 连杀次数
            successiveWinSize ?:number
        // 重生时间戳
            reliveTime ?:number

    }

// 【反馈】结束战斗状态
    interface BroadcastPlayerFightFinishRsp extends RspObj {
        // 发起战斗的玩家ID
            playerId ?:number
        // 击杀人数
            killNum ?:number

    }

// 【请求】改变移动速度
    interface ChangeMoveSpeedReq extends ReqObj {
        // 0免费加速, 1钻石加速
            costType ?:number
    }

// 【反馈】改变移动速度
    interface ChangeMoveSpeedRsp extends RspObj {
        // 新的速度值,既每秒移动距离
            speed ?:number
        // 加速状态结束时间,可能为0,表示未在加速状态,单位:毫秒
            speedUpEndTime ?:number
        // 下次可加速时间戳,可能为0;单位:秒
            speedUpNextTime ?:number

    }

// 【广播】玩家新的移动速度
    interface BroadcastPlayerNewSpeedRsp extends RspObj {
        // 发起战斗的玩家ID
            playerId ?:number
        // 新的速度值,既每秒移动距离
            speed ?:number
        // 加速状态结束时间,可能为0,表示未在加速状态,单位:毫秒
            speedUpEndTime ?:number

    }

// 【推送】玩家追踪目标丢失通报
    interface PushPlayerClickTargetLostRsp extends RspObj {
        // 返回编码, 6:目标处于复活保护状态，${invincibleTime}秒后解除
            returnCode ?:number
        // 复活状态,无敌保护时间
            invincibleTime ?:number
        // 目标玩家ID,为0,表示取消跟踪
            targetId ?:number

    }

// 【请求】加载个人PVP活动数据
    interface LoadSoloPVPDataReq extends ReqObj {
    }

// 【反馈】加载或推送个人PVP活动数据
    interface LoadSoloPVPDataRsp extends RspObj {
        // 活动状态, 1活动进行中, 其他数值,活动未开启
            pvpStatus ?:number
        // 活动开始时间,单位:毫秒
            beginTime ?:number
        // 活动结束时间,单位:毫秒
            endTime ?:number
        // 可重新进入时间,0,Undefined表示无限制
            reenterableTime ?:number
        // 累计胜利场数,即击杀人数
            winSize ?:number
        // 是否今天参加过活动,并且逃跑了
            hadEnteredThisDay ?:boolean

    }

// 【请求】加载个人PVP地图相关信息
    interface LoadSoloPVPSceneDataReq extends ReqObj {
    }

// 【反馈】加载个人PVP地图相关信息
    interface LoadSoloPVPSceneDataRsp extends RspObj {
        // 地图层级列表,55,75,95,115,135,即开放等级,依次+20
            sceneLevelList ?:Array<SceneLevelInfoUnit>
        // 面板显示前三名的信息
            topThreeList ?:Array<PaihangInfoUnit>
        // 上次搜索时间
            lastSearchTime ?:number

    }

// 【请求】加载个人PVP战斗记录
    interface LoadSoloPVPRecordListReq extends ReqObj {
    }

// 【反馈】加载个人PVP战斗记录
    interface LoadSoloPVPRecordListRsp extends RspObj {
        // 战斗记录
            recordList ?:Array<SoloPVPRecordInfoUnit>
        // 我的排行数据
            myData ?:SoloPVPPlayerInfoUnit

    }

// 【请求】加载个人PVP排行数据
    interface LoadSoloPVPListReq extends ReqObj {
        // 请求数量
            count ?:number
        // 是否立即结算本次活动奖励
            isClear ?:boolean
    }

// 【反馈】加载个人PVP排行数据
    interface LoadSoloPVPListRsp extends RspObj {
        // 排行数据
            playerList ?:Array<SoloPVPPlayerInfoUnit>
        // 我的数据
            myData ?:SoloPVPPlayerInfoUnit

    }

// 【请求】加载个人PVP奖池信息
    interface LoadSoloPVPRewardInfoReq extends ReqObj {
    }

// 【反馈】加载个人PVP奖池信息
    interface LoadSoloPVPRewardInfoRsp extends RspObj {
        // 总数
            total ?:number
        // 玩家自身比例,万分比
            playerValue ?:number

    }

// 【请求】铁拳追踪玩家
    interface SearchPvpPlayerReq extends ReqObj {
        // 区服
            serverNum ?:number
        // 玩家昵称
            playerName ?:string
    }

// 【反馈】铁拳追踪玩家
    interface SearchPvpPlayerRsp extends RspObj {
        // 搜索结果 0 没找到 
            status ?:number
        // 玩家Id
            playerId ?:number
        // 玩家服务器长ID
            playerServerId ?:number
        // 该玩家在铁拳内剩余时间
            endTime ?:number
        // 个人PVP地图层级
            scenceLevel ?:number
        // 上次搜索时间
            lastSearchTime ?:number

    }

// 【推送】进入战场开始每35秒显示1次，快速反应事件
    interface PushQuickReactionOptionListRsp extends RspObj {
        // 类型
            sType ?:number
        // 快速反应事件待选项列表
            optionList ?:Array<QuickReactionOptionUnit>

    }

// 【请求】选择某一项目快速反应事件
    interface SelectQuickReactionOptionReq extends ReqObj {
        // 类型
            sType ?:number
        // 快速反应事件表id
            defId ?:number
    }

// 【反馈】选择某一项目快速反应事件
    interface SelectQuickReactionOptionRsp extends RspObj {
        // 类型
            sType ?:number
        // 钻石或金币奖励
            rewardInfo ?:JiangliInfoUnit
        // 奖励描述
            rewardDesc ?:string

    }

// [请求]零元礼包面板
    interface LingYuanLiBaoPanelReq extends ReqObj {
    }

// [返回]零元礼包面板
    interface LingYuanLiBaoPanelRsp extends RspObj {
        // 
            LingYuanLiBaoInfoList ?:Array<LingYuanLiBaoInfoUnit>

    }

// [请求]零元礼包买
    interface LingYuanLiBaoBuyReq extends ReqObj {
        // 
            defId ?:number
    }

// [返回]零元礼包面板
    interface LingYuanLiBaoBuyRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 返还的时间点 (秒)
            sendBackTime ?:number
        // defId
            defId ?:number

    }

// [请求]零元礼包返还
    interface LingYuanLiBaoFanHuanReq extends ReqObj {
        // 
            defId ?:number
    }

// [返回]零元礼包返还
    interface LingYuanLiBaoFanHuanRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]好友列表
    interface FriendsListReq extends ReqObj {
    }

// [返回]好友列表
    interface FriendsListRsp extends RspObj {
        // 好友列表
            friendsInfo ?:Array<FriendsInfoUnit>
        // 玩家魅力
            playerMeiLi ?:number

    }

// 切磋
    interface QieCuoReq extends ReqObj {
        // 敌人的dbid
            friendId ?:number
    }

// [返回]切磋
    interface QieCuoRsp extends RspObj {
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 赠送
    interface ZengSongReq extends ReqObj {
        // 好友的dbid
            friendId ?:number
        // 类型 0精灵球  1玫瑰 2 一键赠送精灵球
            type ?:number
        // 数量
            count ?:number
    }

// 赠送
    interface ZengSongRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 删除好友
    interface DeleteFriendsReq extends ReqObj {
        // 要删除的好友
            friendsIds ?:number
    }

// 删除好友
    interface DeleteFriendsRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 观看战斗记录接口 
    interface WatchFightLogReq extends ReqObj {
        // 日志的dbid
            logId ?:number
    }

// [返回]观看战斗记录接口
    interface WatchFightLogRsp extends RspObj {
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string

    }

// 玩家要申请好友 
    interface ApplyToFriendsReq extends ReqObj {
        // 没有dbid
            friendId ?:number
    }

// [返回]玩家要申请好友 
    interface ApplyToFriendsRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 查找好友 然后返回可以申请他的那个列表 
    interface FindFriendsReq extends ReqObj {
        // 好友名字
            name ?:string
    }

// [返回]查找好友 然后返回可以申请他的那个列表
    interface FindFriendsRsp extends RspObj {
        // 好友列表
            friendsIds ?:Array<FriendsInfoUnit>

    }

// 获取被申请列表
    interface BeAppliedListReq extends ReqObj {
    }

// [返回]申请好友
    interface BeAppliedListRsp extends RspObj {
        // 好友列表
            friendsIds ?:Array<FriendsInfoUnit>

    }

// 同意对方申请
    interface ApplyFriendsReq extends ReqObj {
        // 0拒绝 1同意  2全部忽略 3 全部同意
            operType ?:number
        // 申请记录的id 需要根据这个删除申请记录
            requestRecordDbId ?:number
    }

// [返回]申请好友
    interface ApplyFriendsRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 好友列表
            friendsId ?:Array<number>

    }

// 对话的玩家列表
    interface TalkingFriendsListReq extends ReqObj {
    }

// 对话的玩家列表
    interface TalkingFriendsListRsp extends RspObj {
        // 好友列表
            friendsMsgInfo ?:Array<FriendsMsgInfoUnit>

    }

// 单个玩家对话请求
    interface TalkToFriendReq extends ReqObj {
        // 好友id
            friendsId ?:number
    }

// 单个玩家对话请求
    interface TalkToFriendRsp extends RspObj {
        // 好友列表
            friendsMsgContentInfo ?:Array<FriendsMsgContentInfoUnit>
        // 排位赛defId
            paihangPPDefId ?:number

    }

// 单个玩家对话请求
    interface DeleteFriendMessageReq extends ReqObj {
        // 好友列表
            friendsId ?:Array<number>
    }

// 单个玩家对话请求
    interface DeleteFriendMessageRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 单个玩家对话请求
    interface SendFriendMessageReq extends ReqObj {
        // 好友id
            friendsId ?:number
        // 聊天内容
            content ?:string
    }

// 单个玩家对话请求
    interface SendFriendMessageRsp extends RspObj {
        // 玩家id
            playerId ?:number
        // 好友id
            friendsId ?:number
        // 聊天内容
            content ?:string
        // 时间戳
            time ?:number
        // 我的个人形象数据
            playerPhoto ?:PlayerPhotoUnit

    }

// 清理正在对话的玩家id
    interface ClearIsTalkingFriendReq extends ReqObj {
    }

// 清理正在对话的玩家id
    interface ClearIsTalkingFriendRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]好友日志列表
    interface FriendsLogListReq extends ReqObj {
    }

// [请求]好友日志列表
    interface FriendsLogListRsp extends RspObj {
        // 好友列表
            friendsInfo ?:Array<FriendsInfoUnit>

    }

// [请求]索要金券
    interface AskJinQuanReq extends ReqObj {
        // 索要对象的玩家ID
            goalPlayerId ?:number
        // 充值金额defId
            chongzhijinedefId ?:number
        // 留言
            message ?:string
    }

// 索要金券
    interface AskJinQuanRsp extends RspObj {
        // 是否满足条件
            isFit ?:boolean

    }

// [请求]赠送金券
    interface GiveJinQuanReq extends ReqObj {
        // 赠送对象的玩家ID
            goalPlayerId ?:number
        // 充值金额defId
            chongzhijinedefId ?:number
        // 留言
            message ?:string
        // 日志id
            logID ?:number
    }

// 赠送金券
    interface GiveJinQuanRsp extends RspObj {
        // 是否满足条件
            isFit ?:boolean

    }

// 【请求】指令分析
    interface ZhilingFenxiReq extends ReqObj {
    }

// 【反馈】指令分析
    interface ZhilingFenxiRsp extends RspObj {
        // 指令分析列表
            ZhilingFenxiList ?:Array<ZhilingFenxiInfoUnit>

    }

// 获取游戏次数
    interface GetZhanQiCountReq extends ReqObj {
    }

// 获取游戏次数
    interface GetZhanQiCountRsp extends RspObj {
        // 免费次数
            freeCount ?:number
        // 需要消耗的金券数量
            costCount ?:number
        // 我的形象
            myImage ?:ZhanQiPlayerInfoUnit
        // 到达上限
            daoDaShangXian ?:number
        // 排行榜
            paiHangBang ?:Array<ZhanQiPaiHangBangPlayerInfoUnit>

    }

// 匹配玩家
    interface ZhanQiPiPeiReq extends ReqObj {
        // 最后一秒，发送匹配AI请求
            isAi ?:number
    }

// 匹配玩家
    interface ZhanQiPiPeiRsp extends RspObj {
        // 结果 1.成功 2.进入匹配队列 0.失败
            success ?:number
        // 棋盘
            zhanQiQiPan ?:ZhanQiPanelInfoUnit
        // 我的形象
            myImage ?:ZhanQiPlayerInfoUnit
        // 对手的形象
            duiShouImage ?:ZhanQiPlayerInfoUnit

    }

// 移动棋子
    interface MoveQiZiReq extends ReqObj {
        // 移动的棋子位置
            position ?:number
        // 上下左右的移动方式 1.上 2.下 3.左 4.右
            moveType ?:number
    }

// 匹配玩家
    interface MoveQiZiRsp extends RspObj {
        // 结果  1.成功 0.棋局不存在 -1.不是我的回合 -2.移动越界
            success ?:number
        // 棋盘 0.起始  1.终点
            zhanQiQiPan ?:ZhanQiPanelInfoUnit
        // 上下左右的移动方式 1.上 2.下 3.左 4.右
            moveType ?:number
        // 移动的棋子位置
            position ?:number

    }

// 结算胜负
    interface JieSuanShengFuReq extends ReqObj {
    }

// 结算胜负
    interface JieSuanShengFuRsp extends RspObj {
        // 棋盘
            zhanQiQiPan ?:ZhanQiPanelInfoUnit
        // 胜负 -1.负 1.胜  0.和棋
            win ?:number

    }

// 断线重连
    interface ZhanQiChongLianReq extends ReqObj {
    }

// 断线重连
    interface ZhanQiChongLianRsp extends RspObj {
        // 结果 1.成功 2.进入匹配队列 0.失败
            success ?:number
        // 棋盘
            zhanQiQiPan ?:ZhanQiPanelInfoUnit
        // 我的形象
            myImage ?:ZhanQiPlayerInfoUnit
        // 对手的形象
            duiShouImage ?:ZhanQiPlayerInfoUnit
        // 剩余时间
            shengYuShiJian ?:number

    }

// 奖励分配
    interface JiangLiFenPeiReq extends ReqObj {
    }

// 奖励分配
    interface JiangLiFenPeiRsp extends RspObj {
        // 
            shengList ?:Array<JiangliInfoUnit>
        // 
            fuList ?:Array<JiangliInfoUnit>

    }

// [请求]组队副本排行榜
    interface GetZhanQiPaiHangBangReq extends ReqObj {
        // 排行榜类型：24组队副本排行榜
            type ?:number
    }

// 组队副本排行榜
    interface GetZhanQiPaiHangBangRsp extends RspObj {
        // 排名
            position ?:number
        // 排行列表
            paihangList ?:Array<PaihangInfoUnit>

    }

// 结算胜负
    interface DanXianChengJieSuanReq extends ReqObj {
        // 胜利者ID
            shengId ?:number
        // 失败者ID
            fuId ?:number
        // 是否和棋
            heQi ?:number
        // 原消息serialId
            messageSerialId ?:number
        // AI
            isAi ?:number
    }

// 结算胜负
    interface DanXianChengJieSuanRsp extends RspObj {
        // 结果
            success ?:number

    }

// 【请求】比武对手列表
    interface GetBiwuListReq extends ReqObj {
    }

// 【反馈】比武对手列表
    interface GetBiwuListRsp extends RspObj {
        // 可挑战次数
            num ?:number
        // 总共挑战次数
            totalFreeNum ?:number
        // vip已购买挑战次数
            buyNum ?:number
        // vip购买最大值
            buyMaxNum ?:number
        // 比武对手列表
            biwuList ?:Array<BiwuInfoUnit>
        // 最高排名
            maxPaihang ?:number
        // 已经兑换的荣誉兑换Def Id 列表
            duihuanDefIdList ?:Array<number>
        // 我的排行
            myPaihang ?:number
        // 巅峰竞技场奖励列表
            jiangliinfos ?:Array<JiangliInfoUnit>
        // 购买消耗
            buyCost ?:JiangliInfoUnit
        // 金券奖励的截止时间，单位毫秒
            expireTimeOfJinquan ?:number

    }

// 【请求】比武战斗
    interface BiwuFightReq extends ReqObj {
        // 挑战对方的排行
            enemyPaihang ?:number
        // 是否扫荡
            isSweep ?:boolean
    }

// 【反馈】比武战斗
    interface BiwuFightRsp extends RspObj {
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 对手的头像(空或0表示没有头像)
            enemyTouxiangId ?:number
        // 奖励的固定荣誉
            score ?:number
        // 战斗前排名
            curPaihang0 ?:number
        // 战斗后排名
            curPaihang ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>
        // 战斗胜利奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 新版对手个人形象数据
            enemyPhoto ?:PlayerPhotoUnit
        // 是否扫荡
            isSweep ?:boolean

    }

// 【请求】比武记录列表
    interface GetBiwuRecordListReq extends ReqObj {
    }

// 【反馈】比武对记录表
    interface GetBiwuRecordListRsp extends RspObj {
        // 比武记录列表
            biwuRecordList ?:Array<BiwuRecordInfoUnit>

    }

// 【请求】购买比武次数
    interface BuyBiwuCountReq extends ReqObj {
    }

// 【反馈】购买比武次数
    interface BuyBiwuCountRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean
        // 可挑战次数
            num ?:number
        // 总共挑战次数
            totalFreeNum ?:number
        // vip已购买挑战次数
            buyNum ?:number
        // vip购买最大值
            buyMaxNum ?:number
        // 购买次数的价格
            cost ?:JiangliInfoUnit

    }

// 【请求】荣誉兑换
    interface GetMaxPaihangRewardReq extends ReqObj {
        // 荣誉兑换Def Id
            duihuanDefId ?:number
    }

// 【反馈】荣誉兑换
    interface GetMaxPaihangRewardRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 竞技场积分（竞技场币）
            score ?:number

    }

// 【请求】刷新比武CD信息
    interface RefreshBiwuCDReq extends ReqObj {
    }

// 【反馈】刷新比武CD信息
    interface RefreshBiwuCDRsp extends RspObj {
        // 可挑战次数
            num ?:number
        // 总共挑战次数
            totalNum ?:number
        // vip已购买挑战次数
            buyNum ?:number
        // 比武对手列表
            biwuList ?:Array<BiwuInfoUnit>
        // 我的排行
            myPaihang ?:number
        // 剩下的cd时间 单位:秒
            leftTime ?:number

    }

// 【请求】查看指定玩家阵容
    interface HeroInfoListReq extends ReqObj {
        // 挑战对方的排行
            enemyPaihang ?:number
        // 对方的PlayerId
            enemyPlayerId ?:number
    }

// 【反馈】查看指定玩家阵容
    interface HeroInfoListRsp extends RspObj {
        // 挑战对方的排行
            enemyPaihang ?:number
        // 宠物阵容信息
            heroInfoList ?:Array<BiwuHeroInfoUnit>

    }

// 【请求】前二十名的名称和排名
    interface GetBiWuPaiHangInfoListReq extends ReqObj {
    }

// 【反馈】前二十名的名称和排名
    interface GetBiWuPaiHangInfoListRsp extends RspObj {
        // 我的排行
            myPaiHang ?:number
        // 排行信息
            paiHangInfoList ?:Array<BiWuPaiHangUnit>

    }

// 【请求】登录时弹出的竞技场前3名列表
    interface GetPopUpBiwuListReq extends ReqObj {
    }

// 【反馈】登录时弹出的竞技场前3名列表
    interface GetPopUpBiwuListRsp extends RspObj {
        // 真人标志列表，true表示真人，false表示该位置为机器人
            realPlayerFlagList ?:Array<boolean>
        // 竞技场前3名列表
            playerList ?:Array<PlayerInfoUnit>

    }

// 玩家支付请求
    interface PlayerPayReq extends ReqObj {
        // 签名
            sign ?:string
        // CSDK 平台订单号
            orderId ?:number
        // 游戏产生的订单号
            gameOrderId ?:string
        // 价格，单位分
            price ?:number
        // CSDK 平台渠道简称
            channelAlias ?:string
        // 角色 id
            playerId ?:string
        // 服务器 id
            serverId ?:string
        //  物品 id
            goodsId ?:number
        // 1 表示成功 0 标识失败
            payResult ?:number
        // 透传参数，做扩展用
            state ?:string
        // 通知的时间戳
            payTime ?:string
        // 是否是达人：用于判断是否打八折，true－－打八折，false－－原价
            ext ?:string
        // Game Platform ID
            platformId ?:number
    }

// 玩家支付请求
    interface PlayerPayRsp extends RspObj {
        // 是否处理成功
            isSuccess ?:boolean
        // Game Platform ID
            platformId ?:number

    }

// 【请求】玩家渠道和UId
    interface GetPlayerInfoByPlayerIDReq extends ReqObj {
        // 玩家Id
            playerId ?:number
        // Game Platform ID
            platformId ?:number
    }

// 【反馈】玩家渠道和UId
    interface GetPlayerInfoByPlayerIDRsp extends RspObj {
        // 玩家渠道
            playerChannel ?:string
        // 玩家平台uid
            playerOpenId ?:string
        // Game Platform ID
            platformId ?:number

    }

// 【请求】玩家详细信息
    interface GetPlayerDetailInfoReq extends ReqObj {
        // 玩家Id
            playerId ?:number
        // Game Platform ID
            platformId ?:number
    }

// 【反馈】玩家详细信息
    interface GetPlayerDetailInfoRsp extends RspObj {
        // 
            level ?:number
        // 
            vip ?:number
        // 
            name ?:string
        // 
            serverId ?:number
        // 玩家平台uid
            playerOpenId ?:string
        // 
            heroInfos ?:Array<HeroGMInfoUnit>
        // 
            geRenKeJiDefId ?:Array<number>
        // Game Platform ID
            platformId ?:number

    }

// 【请求】申请手机号验证码
    interface SendPhoneVerificationCodeReq extends ReqObj {
        // 手机号码
            phoneNum ?:string
    }

// 【返回】申请手机号验证码
    interface SendPhoneVerificationCodeRsp extends RspObj {

    }

// 【请求】绑定手机号
    interface LinkMobilePhoneReq extends ReqObj {
        // 验证码
            verificationCode ?:string
    }

// 【返回】绑定手机号
    interface LinkMobilePhoneRsp extends RspObj {
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// [请求]公会副本列表
    interface GongHuiFuBenListReq extends ReqObj {
    }

// [返回]公会副本列表
    interface GongHuiFuBenListRsp extends RspObj {
        // 公会副本列表
            gongHuiFuBenInfoList ?:Array<GongHuiFuBenInfoUnit>
        // 剩余的挑战次数
            leftCount ?:number
        // 公会今天捐副本的总次数
            gongHuiHasJuanXianTimes ?:number

    }

// [请求]开启公会副本
    interface KaiQiReq extends ReqObj {
        // 
            zhangJieId ?:number
        // 
            gongHuiId ?:number
    }

// [返回]开启公会副本
    interface KaiQiRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]捐献钻石
    interface JuanXianXuanShiReq extends ReqObj {
        // 
            count ?:number
        // 
            zhangJieId ?:number
        // 
            gongHuiId ?:number
    }

// [返回]捐献钻石
    interface JuanXianXuanShiRsp extends RspObj {
        // 
            newCount ?:number

    }

// [请求]公会副本章节怪物
    interface ZhangJieGuaiWuReq extends ReqObj {
        // 
            zhangJieId ?:number
    }

// [返回]公会副本章节怪物
    interface ZhangJieGuaiWuRsp extends RspObj {
        // 击杀排行
            jiShaPaiHangList ?:Array<GongHuiFuBenLogUnit>
        // 公会副本列表
            gongHuiFuBenGuaiWuInfoList ?:Array<GongHuiFuBenGuaiWuInfoUnit>
        // 剩余的挑战次数
            leftCount ?:number

    }

// [请求]公会副本章节怪物详情
    interface GuaiWudetailsReq extends ReqObj {
        // 
            dbId ?:number
    }

// [返回]公会副本章节怪物详情
    interface GuaiWudetailsRsp extends RspObj {
        // 首杀公会名字
            shouShaGongHuiName ?:string
        // 首杀公会会长名字
            shouShaGongHuiHuiZhangName ?:string
        // 怪物名字
            guaiWuName ?:string

    }

// [请求]公会副本  打
    interface GongHuiFuBenFightReq extends ReqObj {
        // 
            dbId ?:number
    }

// [返回]公会副本  打
    interface GongHuiFuBenFightRsp extends RspObj {
        // 掉进仓库的奖励列表
            jinCangKuJiangLiList ?:Array<JiangliInfoUnit>
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 总伤害
            damage ?:number
        // 是否出现隐藏boss
            appearYinCangBoss ?:boolean
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]公会仓库
    interface GongHuiCangKuReq extends ReqObj {
        // 
            gongHuId ?:number
    }

// [返回]公会仓库
    interface GongHuiCangKuRsp extends RspObj {
        // 
            gongHuiCangKuInfoList ?:Array<GongHuiCangKuInfoUnit>
        // 
            gongHuiFenZangLogList ?:Array<GongHuiFenZangLogUnit>

    }

// [请求]公会仓库分赃
    interface GongHuiFenZangReq extends ReqObj {
        // 要分赃道具的dbId
            dbId ?:number
        // 数量list
            countList ?:Array<number>
        // 分给谁dbid
            memberIdList ?:Array<number>
    }

// [返回]公会仓库分赃
    interface GongHuiFenZangRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]公会副本日志
    interface GongHuiFuBenLogReq extends ReqObj {
        // 公会id
            gongHuiId ?:number
        // 章节id
            zhangJieId ?:number
    }

// [返回]公会副本日志
    interface GongHuiFuBenLogRsp extends RspObj {
        // 捐献日志排行
            juanXianLogList ?:Array<GongHuiFuBenLogUnit>
        // 掉落日志
            diaoLuoLogList ?:Array<string>
        // 伤害日志排行
            shangHaiLogList ?:Array<GongHuiFuBenLogUnit>

    }

// [请求]公会副本击杀排行
    interface GongHuiFuBenJiShaPaiHangReq extends ReqObj {
        // 第几页
            pageNumber ?:number
    }

// [返回]公会副本击杀排行  展示和公会列表相同 吧宣言变成击杀数量
    interface GongHuiFuBenJiShaPaiHangRsp extends RspObj {
        // 公会列表
            gongHuiListInfo ?:Array<GongHuiListInfoUnit>
        // 一共有多少公会
            totelCount ?:number
        // 共多少页
            pageCount ?:number
        // 我公会的排名
            ownPaiMing ?:number

    }

// [请求]公会副本伤害排行列表
    interface GongHuiFuBenShangHaiListReq extends ReqObj {
        // 章节id
            zhangJieId ?:number
    }

// [返回]公会副本伤害排行列表
    interface GongHuiFuBenShangHaiListRsp extends RspObj {
        // 伤害日志排行
            shangHaiLogList ?:Array<GongHuiFuBenLogUnit>
        // 我公会的排名
            ownPaiMing ?:number

    }

// [请求]公会副本面板
    interface GongHuiFuBenPanelReq extends ReqObj {
    }

// [返回]公会副本面板
    interface GongHuiFuBenPanelRsp extends RspObj {
        // 公会副本信息
            dataList ?:Array<GongHuiFuBenInfoUnit>
        // 之前房间所在的章节Id
            hasDaGuanId ?:number

    }

// [请求]公会副本战斗面板
    interface GongHuiFuBenFightPanelReq extends ReqObj {
        // 房间号
            roomId ?:number
        // 大关Id
            daGuanId ?:number
    }

// [返回]公会副本战斗面板
    interface GongHuiFuBenFightPanelRsp extends RspObj {
        // 请求进入的房间信息
            roomData ?:GongHuiFuBenRoomInfoUnit
        // 房间不存在进入的未完成章节
            daGuanId ?:number
        // 大关名字
            daGuanName ?:string
        // 当前可打小关Id
            curXiaoGuanId ?:number
        // 小关列表
            xiaoGuanLieBiao ?:Array<GongHuiFuBenXiaoGuanInfoUnit>
        // 掉落奖励
            diaoLuoJiangLiList ?:Array<JiangliInfoUnit>
        // 其余的房间列表
            otherRoomList ?:Array<GongHuiFuBenRoomInfoUnit>
        // 剩余收益次数
            leftShouYiCount ?:number
        // 剩余参与次数
            leftJoinCount ?:number
        // 是否可以继续打关
            isCanFight ?:boolean
        // 可以创建房间时间
            canCreateTime ?:number
        // 飘字提示Id，对应表：beizhu表
            tipsId ?:number

    }

// [请求]公会副本日志
    interface GetGongHuiFuBenLogListReq extends ReqObj {
        // 大关Id
            daGuanId ?:number
    }

// [返回]公会副本日志
    interface GetGongHuiFuBenLogListRsp extends RspObj {
        // 章节名字
            zhangJieName ?:string
        // 掉落详细描述列表
            diaoluoLogList ?:Array<GongHuiFuBenDiaoluoLogUnit>
        // 捐献列表
            juanXianNameList ?:Array<string>
        // 伤害排行
            damageList ?:Array<GongHuiFuBenDamageUnit>
        // 最大章节
            maxZhangJie ?:number

    }

// [请求]阵型界面
    interface RoomFightPanelReq extends ReqObj {
    }

// [返回]阵型界面
    interface RoomFightPanelRsp extends RspObj {
        // 精灵列表
            heroList ?:Array<GongHuiFuBenRoomHeroUnit>

    }

// [请求]更换战斗精灵
    interface ChangeRoomHeroReq extends ReqObj {
        // 新上阵的精灵ID
            newHeroId ?:number
        // 1 第一个 2 第二个
            position ?:number
    }

// [返回]更换战斗精灵
    interface ChangeRoomHeroRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]更改自动战斗设置
    interface ChangeRoomFightStatusReq extends ReqObj {
        // 1 满员 2 时间
            type ?:number
        // 新状态
            newStatus ?:boolean
    }

// [返回]更改自动战斗设置
    interface ChangeRoomFightStatusRsp extends RspObj {
        // 更改成功
            isSuccess ?:boolean

    }

// [请求]更改房间人员
    interface ChangeRoomFightPersonReq extends ReqObj {
        // 1 踢人 2 自己离开
            type ?:number
        // 踢掉的人
            playerId ?:number
    }

// [返回]更改房间人员
    interface ChangeRoomFightPersonRsp extends RspObj {
        // 房间状态 1.队长解散 2.队长踢人，3.自己离开，4,战斗正常解散 5，特殊情况解散
            roomStatus ?:number
        // 离开队伍人的Id
            leavePlayerId ?:number

    }

// [请求]公会副本挑战
    interface GongHuiFuBenFightStartReq extends ReqObj {
        // 是否扫荡
            isSweep ?:boolean
    }

// [返回]公会副本挑战
    interface GongHuiFuBenFightStartRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 是否可继续打关：（最后小关被通关时候，返回false）
            canFight ?:boolean
        // 参加人数
            joinFightNum ?:number
        // 是否扫荡
            isSweep ?:boolean

    }

// [请求]创建房间
    interface ChangeRoomReq extends ReqObj {
        // 大关Id
            daGuanId ?:number
        // 小关Id
            xiaoGuanId ?:number
        // 加入房间需传房间号
            roomId ?:number
    }

// [返回]创建房间
    interface ChangeRoomRsp extends RspObj {
        // 自己的房间信息
            roomData ?:GongHuiFuBenRoomInfoUnit
        // 用于别人修改房间时的推送
            otherRoomData ?:GongHuiFuBenRoomInfoUnit

    }

// [请求]重置公会副本章节
    interface ResetGongHuiFuBenDaGuanReq extends ReqObj {
        // 大关Id
            daGuanId ?:number
    }

// [返回]重置公会副本章节
    interface ResetGongHuiFuBenDaGuanRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [返回]重置公会副本章节
    interface PushGongHuiFuBenStatusRsp extends RspObj {
        // 章节ID
            daGuanId ?:number
        // 1通关需重置 2开启
            status ?:number

    }

// [返回]公会副本喊话
    interface GongHuiFuBenHanHuaReq extends ReqObj {
        // 章节ID
            daGuanId ?:number
    }

// [返回]公会副本喊话
    interface GongHuiFuBenHanHuaRsp extends RspObj {
        // 是否邀请成功
            isSuccess ?:boolean

    }

// 【请求】保存eduIdx
    interface SaveEduIdxReq extends ReqObj {
        // 新手引导index，取值范围1~255
            eduIdx ?:number
    }

// 【反馈】保存eduIdx
    interface SaveEduIdxRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】新手引导招募特定武将
    interface NewbieGetHeroReq extends ReqObj {
    }

// 【反馈】新手引导招募特定武将
    interface NewbieGetHeroRsp extends RspObj {
        // 英雄列表
            heroList ?:Array<HeroInfoUnit>

    }

// 【请求】新手引导残魂招募特定武将
    interface NewbieGetHeroBySoulReq extends ReqObj {
        // 残魂DBId
            daojuDBId ?:number
    }

// 【反馈】新手引导残魂招募特定武将
    interface NewbieGetHeroBySoulRsp extends RspObj {
        // 英雄列表
            heroList ?:Array<HeroInfoUnit>

    }

// 【请求】触发引导字段保存
    interface SaveEduCodeReq extends ReqObj {
        // 要保存的触发引导字段code：5~10分别代表edu5~deu10
            fieldCode ?:number
        // 要保存的触发引导字段的值
            val ?:number
    }

// 【反馈】触发引导字段保存
    interface SaveEduCodeRsp extends RspObj {
        // 是否保存成功
            isSuccess ?:boolean

    }

// 【请求】获取在线奖励列表
    interface GetZaixianJiangliListReq extends ReqObj {
        // 活动表Id
            activityId ?:number
    }

// 【反馈】获取在线奖励列表
    interface GetZaixianJiangliListRsp extends RspObj {
        // 获取在线奖励列表
            zaixianJiangliList ?:Array<ZaixianJiangliInfoUnit>
        // 针对在线活动的累计在线时长。单位：秒s;
            onlineTime ?:number

    }

// 【请求】领取在线奖励
    interface DoGetZaixianJiangliReq extends ReqObj {
        // 活动表Id
            activityId ?:number
        // 在线奖励表Id
            zaixianjiangliId ?:number
    }

// 【反馈】领取在线奖励
    interface DoGetZaixianJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// 【请求】领取连续奖励
    interface DoGetLianxuJiangliReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
        // 在线奖励表Id
            lianxujiangliId ?:number
    }

// 【反馈】领取连续奖励
    interface DoGetLianxuJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取玩家连续登录奖励的列表
    interface GetLianxuJiangliListReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
    }

// 【反馈】获取玩家连续登录奖励的列表
    interface GetLianxuJiangliListRsp extends RspObj {
        // 获取在线奖励列表
            lianxuJiangliList ?:Array<LianxuJiangliInfoUnit>
        // 连续登录的天数
            loginDays ?:number

    }

// 【请求】获取冲级奖励列表
    interface GetChongjiJiangliListReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
    }

// 【反馈】获取冲级奖励列表
    interface GetChongjiJiangliListRsp extends RspObj {
        // 获取在冲级励列表
            chongjiJiangliList ?:Array<ChongjiJiangliInfoUnit>

    }

// 【请求】领取冲级奖励
    interface DoGetChongjiJiangliReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
        // 在线奖励表Id
            chongjijiangliId ?:number
    }

// 【反馈】领取冲级奖励
    interface DoGetChongjiJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取领取体力的信息
    interface GetTiliInfoReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
    }

// 【反馈】获取领取体力的信息
    interface GetTiliInfoRsp extends RspObj {
        // 午间体力是否已领取：0、未领取；1、已领取；
            wujianReceived ?:number
        // 晚间体力是否已领取：0、未领取；1、已领取；
            wanjianReceived ?:number

    }

// 【请求】领取体力
    interface DoGetTiliReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
        // 体力类型：1、午间体力；2、晚间体力；
            type ?:number
    }

// 【反馈】领取体力
    interface DoGetTiliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取问答题目
    interface GetWendaItemReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
    }

// 【反馈】获取问答题目
    interface GetWendaItemRsp extends RspObj {
        // 获取的问答静态表ID，如果为零则表示已经答题完毕;
            wendaId ?:number
        // 已经回答问题总数
            wendaCount ?:number
        // 已经回答问题正确数量
            wendaOKCount ?:number

    }

// 【请求】回答问题
    interface AnswerQuestionReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
        // 问题回答是否正确;
            isRight ?:boolean
    }

// 【反馈】回答问题
    interface AnswerQuestionRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 获取新的问答静态表ID，如果为零则表示已经答题完毕;
            wendaId ?:number
        // 已经回答问题总数
            wendaCount ?:number
        // 已经回答问题正确数量
            wendaOKCount ?:number

    }

// 【请求】获取玩家星数奖励下一个奖励的ID
    interface GetNextXingshuJiangliReq extends ReqObj {
    }

// 【反馈】获取玩家星数奖励下一个奖励的ID
    interface GetNextXingshuJiangliRsp extends RspObj {
        // 玩家星数奖励下一个奖励的ID
            xingshuJinagliId ?:number
        // 玩家获取的总的星数
            totalStar ?:number

    }

// 【请求】领取星数奖励
    interface DoGetXingshuJiangliReq extends ReqObj {
    }

// 【反馈】领取星数奖励
    interface DoGetXingshuJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取首充奖励列表
    interface GetShouchongJiangliListReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
    }

// 【反馈】获取首充奖励列表
    interface GetShouchongJiangliListRsp extends RspObj {
        // 获取在首充奖励列表
            shouchongJiangliList ?:Array<ShouchongJiangliInfoUnit>
        // 首充金额
            shouchongjine ?:number

    }

// 【请求】领取首充奖励
    interface DoGetShouchongJiangliReq extends ReqObj {
        // 活动表Id
            huodongId ?:number
        // 首充奖励表Id
            shouchongjiangliId ?:number
    }

// 【反馈】领取首充奖励
    interface DoGetShouchongJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取月卡奖励列表
    interface LoadYueKaJiangliReq extends ReqObj {
    }

// 【反馈】获取月卡奖励列表
    interface LoadYueKaJiangliRsp extends RspObj {
        // 月卡奖励列表
            yueKaJiangliList ?:Array<YueKaJiangliInfoUnit>

    }

// 【请求】领取月卡奖励
    interface DoGetYueKaJiangliReq extends ReqObj {
        // 月卡Id
            yueKaId ?:number
    }

// 【反馈】领取月卡奖励
    interface DoGetYueKaJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 剩余次数
            remain ?:number

    }

// 【请求】获取限时冲关列表
    interface GetXianshiChongguanListReq extends ReqObj {
    }

// 【反馈】获取限时冲关列表
    interface GetXianshiChongguanListRsp extends RspObj {
        // 获取限时冲关列表
            xianshiChongguanList ?:Array<XianshiChongguanInfoUnit>

    }

// 【请求】领取限时冲关奖励
    interface DoGetXianshiChongguanReq extends ReqObj {
        // 限时冲关静态表ID
            defId ?:number
    }

// 【反馈】领取限时冲关奖励
    interface DoGetXianshiChongguanRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取天降元宝列表
    interface GetTianjiangYuanbaoListReq extends ReqObj {
    }

// 【反馈】获取天降元宝列表
    interface GetTianjiangYuanbaoListRsp extends RspObj {
        // 获取天降元宝列表
            tianjiangYuanbaoList ?:Array<TianjiangYuanbaoInfoUnit>

    }

// 【请求】领取天降元宝奖励
    interface DoGetTianjiangYuanbaoReq extends ReqObj {
        // 天降元宝静态表ID
            defId ?:number
    }

// 【反馈】领取天降元宝奖励
    interface DoGetTianjiangYuanbaoRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取天天砸金蛋列表
    interface GetTiantianZaJindanListReq extends ReqObj {
    }

// 【反馈】获取天天砸金蛋列表
    interface GetTiantianZaJindanListRsp extends RspObj {
        // 获取天天砸金蛋列表
            tiantianZaJindanList ?:Array<TiantianZaJindanInfoUnit>

    }

// 【请求】领取天天砸金蛋奖励
    interface DoGetTiantianZaJindanReq extends ReqObj {
        // 天天砸金蛋静态表ID
            defId ?:number
    }

// 【反馈】领取天天砸金蛋奖励
    interface DoGetTiantianZaJindanRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取天天砸金蛋的信息
    interface GetTiantianZaJindanInfoReq extends ReqObj {
    }

// 【反馈】获取天天砸金蛋的信息
    interface GetTiantianZaJindanInfoRsp extends RspObj {
        // 总的砸金蛋的次数
            count ?:number
        // 上午金蛋是否已已砸：0、未砸；1、已砸；
            morningOk ?:number
        // 中午金蛋是否已已砸：0、未砸；1、已砸；
            noonOk ?:number
        // 晚上金蛋是否已已砸：0、未砸；1、已砸；
            nightOk ?:number

    }

// 【请求】砸金蛋
    interface DoZaJindanReq extends ReqObj {
        // 类型：1、上午金蛋；2、下午金蛋；3.晚上金蛋
            type ?:number
    }

// 【反馈】领取体力
    interface DoZaJindanRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】领取次日登录奖励
    interface DoGetCiriJiangliReq extends ReqObj {
        // 次日登陆奖励表Id
            ciridengluId ?:number
    }

// 【反馈】领取次日登录奖励
    interface DoGetCiriJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取玩家次日登录奖励的列表
    interface GetCiriJiangliListReq extends ReqObj {
    }

// 【反馈】获取玩家次日登录奖励的列表
    interface GetCiriJiangliListRsp extends RspObj {
        // 获取在次日奖励列表
            ciriJiangliList ?:Array<CiriJiangliInfoUnit>
        // 登录天数
            loginDays ?:number

    }

// 【请求】领取连续充值奖励
    interface DoGetLianxuChongzhiReq extends ReqObj {
        // 静态表连续充值Id
            defId ?:number
    }

// 【反馈】领取连续充值奖励
    interface DoGetLianxuChongzhiRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取玩家连续充值奖励的列表
    interface GetLianxuChongzhiListReq extends ReqObj {
    }

// 【反馈】获取玩家连续充值奖励的列表
    interface GetLianxuChongzhiListRsp extends RspObj {
        // 获取在次日奖励列表
            lianxuChongzhiList ?:Array<LianxuChongzhiInfoUnit>
        // 连续充值次数
            payCount ?:number

    }

// 【请求】领取每日签到奖励
    interface DoGetMeiriQiandaoReq extends ReqObj {
        // 活动Id
            activityId ?:number
        // 静态表每日签到Id
            defId ?:number
    }

// 【反馈】领取每日签到奖励
    interface DoGetMeiriQiandaoRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// 【请求】获取玩家每日签到奖励的列表
    interface GetMeiriQiandaoListReq extends ReqObj {
        // 活动Id
            activityId ?:number
    }

// 【反馈】获取玩家每日签到奖励的列表
    interface GetMeiriQiandaoListRsp extends RspObj {
        // 获取在次日奖励列表
            meiriQiandaoList ?:Array<MeiriQiandaoInfoUnit>
        // 本月登录次数
            loginDaysOfMonth ?:number

    }

// 【请求】每日签到补签操作
    interface BuQianReq extends ReqObj {
    }

// 【反馈】每日签到补签操作
    interface BuQianRsp extends RspObj {
        // 是否补签成功
            isSuccess ?:boolean

    }

// 【请求】天降元宝补签操作
    interface BuQianTianjiangYuanbaoReq extends ReqObj {
        // 静态ID
            defId ?:number
    }

// 【反馈】天降元宝补签操作
    interface BuQianTianjiangYuanbaoRsp extends RspObj {
        // 是否补签成功
            isSuccess ?:boolean

    }

// 【请求】免费抽取和充值额外奖励信息
    interface GetFreeRewardReq extends ReqObj {
    }

// 【反馈】免费抽取和充值额外奖励信息
    interface GetFreeRewardRsp extends RspObj {
        // 主角等级上升奖励的免费20元宝抽将次数
            levelUpFreeGetHeroCount ?:number
        // 时间免费已经使用次数
            timeFreeGetHeroUseCount ?:number
        // 充值给的额外奖励次数
            chongzhiRewardCount ?:number
        // 充值给的额外奖励次数--已领取次数
            getCount ?:number
        // 期间的充值总金额：单位（元）
            totalJine ?:number
        // 扭蛋券商城打折信息
            itemShopList ?:Array<NiuDanShopInfoUnit>
        // 单词积分契约的消耗积分值
            scoreCost ?:number
        // 积分契约 所需历史累充金额，单位：元
            limitTotalRechargeYuan ?:number
        // 再超级抽多少次必得心愿神魔
            leftTimesToWish ?:number

    }

// 【请求】获取小额充值奖励
    interface GetXiaoyeChongzhiJiangliReq extends ReqObj {
    }

// 【反馈】获取小额充值奖励
    interface GetXiaoyeChongzhiJiangliRsp extends RspObj {
        // 是否获取成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】获取周卡奖励列表
    interface LoadZhouKaJiangliReq extends ReqObj {
    }

// 【反馈】获取周卡奖励列表
    interface LoadZhouKaJiangliRsp extends RspObj {
        // 周卡奖励列表
            yueKaJiangliList ?:Array<YueKaJiangliInfoUnit>

    }

// 【请求】领取周卡奖励
    interface DoGetZhouKaJiangliReq extends ReqObj {
        // 月卡Id
            yueKaId ?:number
    }

// 【反馈】领取周卡奖励
    interface DoGetZhouKaJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 剩余次数
            remain ?:number

    }

// 【请求】获取终身卡奖励列表
    interface LoadZhongShenKaJiangliReq extends ReqObj {
    }

// 【反馈】获取终身卡奖励列表
    interface LoadZhongShenKaJiangliRsp extends RspObj {
        // 终身奖励列表
            yueKaJiangliList ?:Array<YueKaJiangliInfoUnit>

    }

// 【请求】领取终身卡奖励
    interface DoGetZhongShenKaJiangliReq extends ReqObj {
        // 终身卡Id
            yueKaId ?:number
    }

// 【反馈】领取终身卡奖励
    interface DoGetZhongShenKaJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 剩余次数
            remain ?:number

    }

// 一元特价类奖励请求
    interface GetYiyuantejiaRewardReq extends ReqObj {
        // 充值金额 Def ID
            defId ?:number
    }

// 一元特价类奖励反馈 神秘来电活动奖励推送(神秘来电是直购活动 充完了直接发 所以需要提示给玩家)
    interface GetYiyuantejiaRewardRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】财神殿闯关战斗
    interface CsdFightPVEReq extends ReqObj {
        // 进行战斗的静态表中的Id
            huodongGuanId ?:number
    }

// 【反馈】财神殿闯关战斗
    interface CsdFightPVERsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】获得玩家已通过(财神殿)活动关列表
    interface GetCsdHuodongguanListReq extends ReqObj {
    }

// 【反馈】获得玩家已通过(财神殿)活动关列表
    interface GetCsdHuodongguanListRsp extends RspObj {
        // 玩家关卡列表
            guanQiaList ?:Array<GuanqiaInfoUnit>

    }

// 【请求】（财神殿）挂机
    interface CsdGuajiReq extends ReqObj {
        // 0 一关一关的请求 1 一次都给了
            guaJiType ?:number
    }

// 【反馈】（财神殿）挂机
    interface CsdGuajiRsp extends RspObj {
        // 奖励列表
            csdjiangliList ?:Array<CsdJiangliInfoUnit>

    }

// 【请求】获取财神殿排名列表（前50名）
    interface GetCsdPaihangListReq extends ReqObj {
    }

// 【反馈】获取财神殿排名列表（前50名）
    interface GetCsdPaihangListRsp extends RspObj {
        // 财神殿排行列表
            csdPaihangList ?:Array<CaishendianPaihangInfoUnit>
        // 玩家自己的排名数
            ownPaiming ?:number

    }

// 【请求】财神殿排名观看他人战斗
    interface WatchingOtherFightOfPaimingReq extends ReqObj {
        // 财神殿DBId
            csdDBId ?:number
    }

// 【反馈】财神殿排名观看他人战斗
    interface WatchingOtherFightOfPaimingRsp extends RspObj {
        // 全部战斗信息的Json串
            fightInfo ?:string

    }

// 【请求】财神殿奖励观看他人战斗
    interface WatchingOtherFightOfJiangliReq extends ReqObj {
        // 进行战斗的静态表中的Id
            huodongGuanId ?:number
    }

// 【反馈】财神殿奖励观看他人战斗
    interface WatchingOtherFightOfJiangliRsp extends RspObj {
        // 全部战斗信息的Json串
            fightInfo ?:string

    }

// 【请求】获取财神殿奖励说明列表
    interface GetJiangliExplainListReq extends ReqObj {
    }

// 【反馈】获取财神殿奖励说明列表
    interface GetJiangliExplainListRsp extends RspObj {
        // 财神殿排行列表
            csdJianlgiExplainList ?:Array<CsdJiangliExplainInfoUnit>

    }

// [请求]玩家皮肤图鉴系统面板
    interface PifuTujianPanelReq extends ReqObj {
    }

// [返回]玩家皮肤图鉴系统面板
    interface PifuTujianPanelRsp extends RspObj {
        // 皮肤图鉴信息列表
            pifuTujianInfoList ?:Array<PifuTujianInfoUnit>
        // 单抽消耗
            costForOneDraw ?:JiangliInfoUnit
        // 注册选人时配置的皮肤推荐ID
            zhuceXuanrenDefId ?:number
        // 剩余的免费自定义DIY次数
            freePersonalDefineCount ?:number
        // 自定义DIY付费消耗
            costForPersonalDefine ?:JiangliInfoUnit

    }

// [请求]玩家皮肤图鉴操作面板
    interface PifuTujianOperationPanelReq extends ReqObj {
        // 皮肤图鉴表Id
            defId ?:number
    }

// [返回]玩家皮肤图鉴操作面板
    interface PifuTujianOperationPanelRsp extends RspObj {
        // 皮肤图鉴单个皮肤的详细信息
            detailInfo ?:PifuTujianOnePifuInfoUnit

    }

// [请求]玩家皮肤图鉴单皮肤操作
    interface PifuTujianOperateReq extends ReqObj {
        // 皮肤图鉴表Id
            defId ?:number
        // 1 点亮；2 升级
            opType ?:number
        // 服务器内部请求
            isRequestFromServer ?:boolean
    }

// [返回]玩家皮肤图鉴单皮肤操作
    interface PifuTujianOperateRsp extends RspObj {
        // 皮肤图鉴单个皮肤的详细信息
            detailInfo ?:PifuTujianOnePifuInfoUnit

    }

// [请求]玩家皮肤图鉴使用皮肤
    interface PifuTujianUseReq extends ReqObj {
        // 皮肤图鉴表Id
            defId ?:number
        // 服务器内部请求
            isRequestFromServer ?:boolean
    }

// [返回]玩家皮肤图鉴使用皮肤
    interface PifuTujianUseRsp extends RspObj {
        // 玩家个人形象信息
            playerPhoto ?:PlayerPhotoUnit

    }

// [请求]玩家皮肤图鉴自定义
    interface PifuTujianPersonalDefineReq extends ReqObj {
        // 新的骨骼配置信息
            newGugePartList ?:Array<GugePartUnit>
        // 是否是注册创建角色时的个性化皮肤
            isOnCreateActor ?:boolean
    }

// [返回]玩家皮肤图鉴自定义
    interface PifuTujianPersonalDefineRsp extends RspObj {
        // 新的骨骼配置信息
            newGugePartList ?:Array<GugePartUnit>
        // 剩余的免费自定义DIY次数
            freePersonalDefineCount ?:number

    }

// [推送]获得新的玩家皮肤图鉴
    interface PushNewPifuTujianInfoRsp extends RspObj {
        // 新皮肤图鉴信息
            pifuTujianInfo ?:PifuTujianInfoUnit

    }

// [请求]玩家皮肤图鉴夺宝抽奖
    interface PifuTujianLuckyDrawReq extends ReqObj {
        // 1 单抽；2 十连抽
            opType ?:number
    }

// [返回]玩家皮肤图鉴夺宝抽奖
    interface PifuTujianLuckyDrawRsp extends RspObj {
        // 抽到的皮肤列表
            drawInfoList ?:Array<PifuTujianLuckyDrawInfoUnit>
        // 重复皮肤转换出来的道具奖励
            daojuReward ?:JiangliInfoUnit

    }

// [请求]玩家皮肤图鉴替换单个皮肤部件
    interface PifuTujianSwitchOneGugePartReq extends ReqObj {
        // 须替换的皮肤部件
            gugePart ?:GugePartUnit
    }

// [返回]玩家皮肤图鉴替换单个皮肤部件
    interface PifuTujianSwitchOneGugePartRsp extends RspObj {
        // 替换成功
            isSuccess ?:boolean

    }

// [请求]名片面板
    interface MingPianPanelReq extends ReqObj {
        // 玩家id
            playerId ?:number
        // 服务器ID 长id 用于跨服名片消息
            serverChangId ?:number
    }

// [返回]名片面板
    interface MingPianPanelRsp extends RspObj {
        // dbid
            playerDBId ?:number
        // 战斗力
            playerFightPower ?:number
        // 等级
            playerLevel ?:number
        // vip
            vip ?:number
        // 名字
            playerName ?:string
        // 所在服务器
            serverInfo ?:number
        // 头像
            playerTouxiangId ?:number
        // 坐骑id
            playerZuoqi ?:number
        // 翅膀id
            playerChibang ?:number
        // 皮肤形象
            piFuDefIdList ?:Array<number>
        // 称号
            chenghaoId ?:number
        // 名片信息列表
            mingPianList ?:Array<MingPianInfoUnit>
        // true 能查看   false 不能
            isSuccess ?:boolean
        // 
            qianMing ?:string
        // 创建角色时间
            createTime ?:string

    }

// [请求]获得名片  跨服
    interface MingPianPanelCrossServerReq extends ReqObj {
        // 玩家id
            playerId ?:number
        // 查看谁的名片
            mingPianPlayerId ?:number
        // 服务器ID 长id 用于跨服名片消息
            serverId ?:number
    }

// [反馈]获得名片 跨服
    interface MingPianPanelCrossServerRsp extends RspObj {
        // 
            playerId ?:number
        // 查看谁的名片
            mingPianPlayerId ?:number
        // 战斗力
            playerFightPower ?:number
        // 等级
            playerLevel ?:number
        // vip
            vip ?:number
        // 名字
            playerName ?:string
        // 所在服务器
            serverInfo ?:number
        // 头像
            playerTouxiangId ?:number
        // 坐骑id
            playerZuoqi ?:number
        // 翅膀id
            playerChibang ?:number
        // 皮肤形象
            piFuDefIdList ?:Array<number>
        // 称号
            chenghaoId ?:number
        // 名片信息列表
            mingPianList ?:Array<MingPianInfoUnit>
        // 
            qianMing ?:string
        // 创建角色时间
            createTime ?:string

    }

// [请求]改变自己名片的状态
    interface ChangeMingPianStatusReq extends ReqObj {
        // 0隐藏 1展示
            changeStatus ?:number
    }

// [反馈]改变自己名片的状态
    interface ChangeMingPianStatusRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]改变自己名片的签名
    interface ChangeMingPianQianMingReq extends ReqObj {
        // 0隐藏 1展示
            qianMing ?:string
    }

// [反馈]改变自己名片的签名
    interface ChangeMingPianQianMingRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]跨服玩家名片面板
    interface CrossPlayerCardPanelReq extends ReqObj {
        // 玩家id
            playerId ?:number
    }

// [返回]名片面板
    interface CrossPlayerCardPanelRsp extends RspObj {
        // 基本信息
            commonInfo ?:PlayerCardCommonInfoUnit

    }

// [请求]跨服玩家名片精灵详情
    interface CrossPlayerCardHeroPanelReq extends ReqObj {
        // 玩家id
            playerId ?:number
    }

// [返回]跨服玩家名片精灵详情
    interface CrossPlayerCardHeroPanelRsp extends RspObj {
        // 玩家宠物详情信息
            heroes ?:PlayerCardAllHeroesUnit

    }

// [请求]跨服名片玩家详情信息
    interface CrossPlayerDetailInfoReq extends ReqObj {
        // 玩家id
            playerId ?:number
    }

// [返回]跨服名片玩家详情信息
    interface CrossPlayerDetailInfoRsp extends RspObj {
        // 基本信息
            commonInfo ?:PlayerCardCommonInfoUnit
        // 战阵列表
            huizhangList ?:Array<ZhanzhenAllInfoUnit>
        // 跟随宠信息
            followPetShow ?:NewFollowPetShowUnit
        // 皮神外显
            piShenShow ?:number
        // 称号列表
            chengHaoList ?:Array<number>
        // 幻化
            huanHua ?:PlayerCardHuanHuaUnit
        // Z力量
            zliliang ?:PlayerCardZliliangUnit
        // 忍蛙类型列表
            renWaTypeList ?:Array<RenWaTypeDataUnit>
        // 忍蛙列表
            renWaList ?:Array<RenWaDataUnit>
        // 印记列表
            yinJiInfoList ?:Array<YinJiInfoUnit>
        // 坐骑信息，可能为undefined
            zuoqi ?:PlayerCardZuoqiUnit
        // 皮神
            piShen ?:PlayerCardLuoTuoMuUnit
        // 训练师
            xunLianShi ?:PlayerCardZhuJuePeiYangUnit
        // 转生
            zhuanSheng ?:PlayerCardZhuanShengUnit
        // 翅膀信息，可能为undefined
            chibang ?:PlayerCardZuoqiUnit
        // 跟随宠
            followPet ?:PlayerCardFollowPetUnit
        // 守护兽
            guardPet ?:PlayerCardGuardPetUnit
        // 威名
            weiming ?:PlayerCardWeimingUnit
        // 皮肤图鉴信息列表
            pifuTujianInfoList ?:Array<PifuTujianInfoUnit>
        // 足迹信息，可能为undefined
            zuji ?:PlayerCardZuoqiUnit
        // 头像框
            touxiangkuangInfo ?:PlayerCardTouxiangkuangUnit
        // 头衔信息，可能为undefined
            touxian ?:PlayerCardTouxianUnit
        // 聊天框
            liaotiankuangInfo ?:PlayerCardTouxiangkuangUnit
        // 跨服某子系统全部已开启的皮肤数据
            subSystemPifuList ?:Array<PlayerCardSubSystemPifuUnit>
        // 命星数据
            fateStarList ?:Array<FateStarInfoUnit>
        // 头衔光环信息，可能为undefined
            touxianGuanghuan ?:PlayerCardTouxianGuanghuanUnit
        // 是否已经激活传送光环
            isChuanSongGuanghuanAcitve ?:boolean
        // 是否已经激活秘法证书
            isMifaZhengshuAcitve ?:boolean

    }

// 【请求】排行榜
    interface GetPaihangBangReq extends ReqObj {
        // 排行榜类型：1,等级榜；2,战力榜；3,竞技榜；4,匹配榜；5,遭遇榜
            type ?:number
        // 查询指定服务器的排行榜
            serverNum ?:number
    }

// 【反馈】排行榜
    interface GetPaihangBangRsp extends RspObj {
        // 排名
            position ?:number
        // 剩余可膜拜次数
            mobaiCount ?:number
        // 排行列表
            paihangList ?:Array<PaihangInfoUnit>
        // 查询指定服务器的排行榜
            serverNum ?:number

    }

// 【请求】请求排行榜分享奖励
    interface GetPaihangBangShareRewardReq extends ReqObj {
        // 排行榜类型：1,等级榜；2,战力榜；3,竞技榜；4,匹配榜；5,遭遇榜
            type ?:number
    }

// 【反馈】请求排行榜分享奖励
    interface GetPaihangBangShareRewardRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】请求所有排行榜类型
    interface GetAllPaihangBangTypeReq extends ReqObj {
    }

// 【反馈】请求所有排行榜类型
    interface GetAllPaihangBangTypeRsp extends RspObj {
        // 排行列表
            paihangShareList ?:Array<PaihangShareInfoUnit>

    }

// [请求]商品列表
    interface ScoreShopGoodsListReq extends ReqObj {
        // 商店类型 2 荣誉商城(声望) 3 世界boss
            shopType ?:number
    }

// [反馈]商品列表
    interface ScoreShopGoodsListRsp extends RspObj {
        // 
            goodsList ?:Array<ScoreShopGoodsInfoUnit>
        // 玩家当前积分
            jifen ?:number
        // 剩余免费次数
            freeRefreshTimes ?:number
        // 刷新消耗
            refreshCost ?:JiangliInfoUnit
        // 剩余花钱刷新次数
            moneyRefreshTimes ?:number
        // 下次刷新时间，单位：毫秒
            nextRefreshTime ?:number

    }

// [请求]购买商品
    interface BuyScoreShopGoodsReq extends ReqObj {
        // 商品Email数据表dbID，数据表主Key
            goodsDBID ?:number
        // 商店类型 2 荣誉商城(声望) 3 世界boss
            shopType ?:number
    }

// [返回]购买商品
    interface BuyScoreShopGoodsRsp extends RspObj {
        // 是否成功的标志
            isSuccess ?:boolean

    }

// [请求]刷新商品
    interface RefreshScoreShopReq extends ReqObj {
        // 商店类型 2 荣誉商城(声望) 3 世界boss
            shopType ?:number
    }

// [返回]刷新商品
    interface RefreshScoreShopRsp extends RspObj {
        // 是否成功的标志
            isSuccess ?:boolean

    }

// [请求]恋爱互动面板
    interface LianAiHuDongPanelReq extends ReqObj {
        // 人物id
            renWuId ?:number
    }

// [返回]恋爱互动面板
    interface LianAiHuDongPanelRsp extends RspObj {
        // 今日还能挑战的次数
            canFightTimes ?:number
        // 送礼物的信息
            lianAiLiWuInfoList ?:Array<LianAiLiWuInfoUnit>
        // 下次可挑战的时间点  秒
            nestFightTime ?:number
        // 好感度等级
            haoGanDuLevel ?:number
        // 当前有多少经验  分子
            currExp ?:number
        // 满经验经验是多少 分母
            fullExp ?:number
        // 当前属性列表
            currShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 增加属性列表
            addShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 房子激活属性列表
            teShuShuXingInfoList ?:Array<QiJiTeShuShuXingInfoUnit>
        // 技能描述
            lianAiJiNengDescInfoList ?:Array<LianAiJiNengDescInfoUnit>
        // 胜利对话
            shengLiDuiHua ?:string
        // 技能属性列表
            jiNengShuXingInfoList ?:Array<ShuXingInfoUnit>

    }

// [请求]恋爱送礼物
    interface SongLiWuReq extends ReqObj {
        // 人物id
            renWuId ?:number
        // 道具id
            itemId ?:number
        // 数量
            count ?:number
    }

// [返回]恋爱送礼物
    interface SongLiWuRsp extends RspObj {
        // 增加的经验值
            addExp ?:number
        // 是否升级
            isLevelUp ?:boolean
        // 当前属性列表   用这个值替换当前属性
            currShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 送完礼物在以后的对话
            duiHua ?:string

    }

// [请求]恋爱切磋
    interface LianAiQieCuoReq extends ReqObj {
        // 人物id
            renWuId ?:number
    }

// [返回]恋爱切磋
    interface LianAiQieCuoRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]恋爱任务最后一步
    interface FinishLianAiRenWuReq extends ReqObj {
        // 人物id
            renWuId ?:number
    }

// [返回]恋爱任务最后一步
    interface FinishLianAiRenWuRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 解锁后的信息
            jieSuoUnit ?:YiJieSuoTownInfoUnit

    }

// [请求]恋爱任务中的打斗
    interface LianAiTaskFightReq extends ReqObj {
        // 人物id
            renWuId ?:number
        // 任务id
            taskId ?:number
    }

// [返回]恋爱任务中的打斗
    interface LianAiTaskFightRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]她的故事
    interface HerStoryReq extends ReqObj {
        // 人物id
            renWuId ?:number
    }

// [返回]她的故事
    interface HerStoryRsp extends RspObj {
        // 故事
            story ?:string

    }

// [请求]恋爱技能升级面板
    interface LianAiJiNengShengJiPanelReq extends ReqObj {
        // 人物id
            renWuId ?:number
        // 技能defid
            jiNengDefId ?:number
    }

// [返回]恋爱技能升级面板
    interface LianAiJiNengShengJiPanelRsp extends RspObj {
        // 需要的材料
            caiLiaoList ?:Array<JiangliInfoUnit>
        // 旧的描述
            oldDesc ?:string
        // 新的描述
            newDesc ?:string
        // 升级需要的好感度等级
            needHaoGanDuLevel ?:number
        // 当前等级属性
            curShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 下一级等级属性
            nextShuXingInfoList ?:Array<ShuXingInfoUnit>

    }

// [请求]恋爱技能升级
    interface LianAiJiNengShengJiReq extends ReqObj {
        // 人物id
            renWuId ?:number
        // 技能defid
            jiNengDefId ?:number
    }

// [返回]恋爱技能升级
    interface LianAiJiNengShengJiRsp extends RspObj {
        // 新的技能描述
            lianAiJiNengDescInfoList ?:Array<LianAiJiNengDescInfoUnit>
        // 新技能对应的房子图
            fangZiTu ?:string

    }

// [请求]恋爱技能开关
    interface LianAiJiNengSwitchReq extends ReqObj {
        // 人物id
            renWuId ?:number
        // 技能defid
            jiNengDefId ?:number
        // 1.功能开启 2.功能关闭 0.无对应的功能
            status ?:number
    }

// [返回]恋爱技能开关
    interface LianAiJiNengSwitchRsp extends RspObj {
        // 1.功能开启 2.功能关闭 0.无对应的功能
            status ?:number

    }

// [请求]周期性特权卡面板信息
    interface PeriodCardPanelReq extends ReqObj {
    }

// [返回]购买周期性特权卡
    interface PeriodCardPanelRsp extends RspObj {
        // 累计充值送特权卡的列表。如果所有的均已激活，则该列表为空，请注意。
            rechargeCardList ?:Array<CumulativeRechargeCardInfoUnit>

    }

// [请求]周期性特权卡领取特权表奖励
    interface PeriodCardGetTequanRewardReq extends ReqObj {
        // Project表Id
            projectDefId ?:number
    }

// [返回]周期性特权卡领取特权表奖励
    interface PeriodCardGetTequanRewardRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】位置上的技能书升级属性预览
    interface SkillBookUpgradePropShowReq extends ReqObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
    }

// 【反馈】位置上的技能书升级属性预览
    interface SkillBookUpgradePropShowRsp extends RspObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 当前属性列表
            curPropList ?:Array<PropertyInfoUnit>
        // 
            nextPropList ?:Array<PropertyInfoUnit>
        // 升级所需消耗
            xiaohaoList ?:Array<JiangliInfoUnit>

    }

// 【请求】位置上的技能书升级
    interface SkillBookUpgradeReq extends ReqObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
    }

// 【反馈】位置上的技能书升级
    interface SkillBookUpgradeRsp extends RspObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 新的等级
            level ?:number

    }

// 【请求】位置上的技能书升星属性预览
    interface SkillBookStarUpPropShowReq extends ReqObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
    }

// 【反馈】位置上的技能书升星属性预览
    interface SkillBookStarUpPropShowRsp extends RspObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 当前星级的技能书描述
            curDesc ?:string
        // 当前星级的技能书描述
            nextDesc ?:string
        // 升星所需消耗
            xiaohaoList ?:Array<JiangliInfoUnit>

    }

// 【请求】技能书升星
    interface SkillBookStarUpReq extends ReqObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
    }

// 【反馈】技能书升星
    interface SkillBookStarUpRsp extends RspObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 新的星级
            star ?:number

    }

// 【请求】技能书分解
    interface SkillBookSendBackReq extends ReqObj {
        // 技能书DbId List
            skillBookDbIDs ?:Array<number>
        // 是否分解奖励预览
            isForShow ?:boolean
    }

// 【请求】技能书分解
    interface SkillBookSendBackRsp extends RspObj {
        // 是否分解奖励预览
            isForShow ?:boolean
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】某玩家某位置的技能书协作列表
    interface GetSkillBookXiezuoInfoReq extends ReqObj {
        // 玩家PlayerId，如果是自己，不需要传
            playerId ?:number
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
    }

// 【反馈】某玩家某位置的技能书协作列表
    interface GetSkillBookXiezuoInfoRsp extends RspObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 位置上的技能书列表
            xiezuoList ?:Array<SkillBookXiezuoUnit>

    }

// 【请求】技能书上阵
    interface SkillBookTakePositionReq extends ReqObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 新上阵的技能书Db Id
            skillBookDbId ?:number
    }

// 【反馈】技能书上阵
    interface SkillBookTakePositionRsp extends RspObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 新上阵的技能书Db Id
            skillBookDbId ?:number

    }

// 【请求】技能书相应的宠物上阵
    interface SkillBookHeroTakePositionReq extends ReqObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 替换全部覆盖掉的宠物Db Id List
            heroIdList ?:Array<number>
    }

// 【反馈】技能书相应的宠物上阵
    interface SkillBookHeroTakePositionRsp extends RspObj {
        // 0表示主阵容，1-3表示1-3编队。默认值=主阵容
            teamIndex ?:number
        // 1-3技能书可上阵位置
            bookIndex ?:number
        // 替换的宠物Db Id List
            heroIdList ?:Array<number>

    }

// 【请求】技能书合成
    interface SkillBookMergeReq extends ReqObj {
        // 技能书 db Id List
            skillBookIdList ?:Array<number>
    }

// 【反馈】技能书合成
    interface SkillBookMergeRsp extends RspObj {

    }

// 【请求】购买一元特价商品
    interface BuyYiyuanGoodsReq extends ReqObj {
        // 一元特价静态表主ID
            yiyuantejiaId ?:number
        // 卖出数量
            count ?:number
    }

// 【反馈】购买一元特价商品
    interface BuyYiyuanGoodsRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// 【请求】获取（首次或日常）一元商品列表
    interface GetYiyuanGoodsListReq extends ReqObj {
    }

// 【反馈】获取（首次或日常）一元商品列表
    interface GetYiyuanGoodsListRsp extends RspObj {
        // 是否是首次购买1为首次，0为日常
            chixu ?:number

    }

// 加载新活动列表
    interface GetNewActivityListReq extends ReqObj {
    }

// 加载新活动列表
    interface GetNewActivityListRsp extends RspObj {
        // 活动列表
            activityList ?:Array<NewActivityInfoUnit>

    }

// 玩家获取迎财神活动信息请求
    interface GetYingCaiShenReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 玩家获取迎财神活动信息反馈
    interface GetYingCaiShenRsp extends RspObj {
        // 剩余次数
            lastCount ?:number
        // 本次花费
            xiaofei ?:number
        // 最高获得
            maxTo ?:number
        // VIP等级限制
            vip ?:number

    }

// 玩家获取迎财神活动奖励请求
    interface GetAwardFromYingCaiShenReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 玩家获取迎财神活动奖励反馈
    interface GetAwardFromYingCaiShenRsp extends RspObj {
        // 奖励的元宝，可能为0
            currency ?:number
        // 剩余次数
            lastCount ?:number
        // 本次花费
            xiaofei ?:number
        // 最高获得
            maxTo ?:number
        // VIP等级限制
            vip ?:number

    }

// 玩家获取充值排行活动信息请求
    interface GetChongZhiPaiHangReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 玩家获取充值排行活动信息请求
    interface GetChongZhiPaiHangRsp extends RspObj {
        // 排行榜
            chongzhiPaihangList ?:Array<NewChongZhiPaiHangInfoUnit>

    }

// 玩家获取新限时排行数据信息请求
    interface GetNewValueListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 玩家获取新限时排行数据信息请求
    interface GetNewValueListRsp extends RspObj {
        // 排行分数信息
            valueListInfo ?:Array<NewValueListInfoUnit>

    }

// 打开累计充值面板
    interface GetLeijiChongzhiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 打开累计充值面板
    interface GetLeijiChongzhiListRsp extends RspObj {
        // 获取累计充值列表
            detailList ?:Array<ActivityDetailInfoUnit>
        // 期间的充值总金额：单位(元)
            totalCount ?:number

    }

// 领取累计充值奖励
    interface DoGetLeijiChongzhiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
        // 是否一键领取奖励
            isAllReward ?:boolean
    }

// 领取累计充值奖励
    interface DoGetLeijiChongzhiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 打开累计消费面板
    interface GetXiaofeiJiangliListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 打开累计消费面板
    interface GetXiaofeiJiangliListRsp extends RspObj {
        // 获取消费奖励列表
            xiaofeiJiangliList ?:Array<ActivityDetailInfoUnit>
        // 消费金额
            xiaofeijine ?:number
        // 金券消费金额
            jinquanxiaofeijine ?:number
        // 消费活动跳转活动DbId
            xiaofeiHuodongTiaozhuanDbId ?:number

    }

// 领取消费奖励
    interface DoGetXiaofeiJiangliReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 消费奖励表Id
            xiaofeijiangliId ?:number
        // 静态表id
            defId ?:number
    }

// 领取消费奖励
    interface DoGetXiaofeiJiangliRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]获取每日充值列表
    interface GetMeiriChongzhiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]获取每日充值列表
    interface GetMeiriChongzhiListRsp extends RspObj {
        // 获取累计充值列表
            detailList ?:Array<ActivityDetailInfoUnit>
        // 期间的充值总金额：单位(元)
            totalCount ?:number

    }

// [请求]领取每日充值奖励
    interface DoGetMeiriChongzhiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取每日充值奖励
    interface DoGetMeiriChongzhiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]获取每日消费列表
    interface GetMeiriXiaofeiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]获取每日消费列表
    interface GetMeiriXiaofeiListRsp extends RspObj {
        // 获取消费奖励列表
            meiriXiaofeiList ?:Array<ActivityDetailInfoUnit>
        // 期间的消费总元宝
            totalYuanbao ?:number
        // 期间的消费总金券
            totalJinquan ?:number

    }

// [请求]领取消费奖励
    interface DoGetMeiriXiaofeiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 消费奖励表Id
            xiaofeijiangliId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取每日消费奖励
    interface DoGetMeiriXiaofeiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]限时冲级列表
    interface XianShiChongJiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]限时冲级列表
    interface XianShiChongJiListRsp extends RspObj {
        // 获取消费奖励列表
            XianShiChongJiList ?:Array<XianShiChongJiInfoUnit>

    }

// [请求]领取冲级奖励
    interface DoGetXianShiChongJiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取每日冲级奖励
    interface DoGetXianShiChongJiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]打折商品list
    interface DaZheShangPinListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]打折商品list
    interface DaZheShangPinListRsp extends RspObj {
        // 打折商品
            daZheShangPinList ?:Array<DaZheShangPinInfoUnit>

    }

// [请求]购买打折商品
    interface GouMaiDaZheShangPinReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
        // 购买第几个商品   根据位置来 1 2 3 4 全部发0 
            index ?:number
    }

// [反馈]购买打折商品
    interface GouMaiDaZheShangPinRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]成长基金list
    interface ChengZhangJiJinListReq extends ReqObj {
        // 活动id
            activityId ?:number
    }

// [请求]成长基金list
    interface ChengZhangJiJinListRsp extends RspObj {
        // 免费成长基金list
            freeJiJinList ?:Array<JiJinInfoUnit>
        // 付费成长基金list
            payJiJinList ?:Array<JiJinInfoUnit>
        // 累计充值
            totalJine ?:number
        // 充值近额表Id
            chongZhiJinEId ?:number
        // 按钮图片
            buttonImage ?:string

    }

// [请求]领取成长基金
    interface GetChengZhangJiJinReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [请求]领取成长基金
    interface GetChengZhangJiJinRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]加载单笔充值数据
    interface LoadDanBiChongZhiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载单笔充值数据
    interface LoadDanBiChongZhiListRsp extends RspObj {
        // 单笔充值数据
            detailList ?:Array<DanBiChongZhiDetailInfoUnit>
        // 对于特殊物品在商城也能卖 但是有限购 活动列表也需要展示一个购买次数
            hasBuy ?:number
        // 限购个数 这是总个数
            limitBuy ?:number
        // 单笔团购充值数据
            tuangouInfo ?:DanBiChongZhiDetailInfoUnit

    }

// [请求]领取单笔充值
    interface GetDanBiChongZhiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取单笔充值
    interface GetDanBiChongZhiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]加载五元充值数据
    interface LoadWuYuanChongZhiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载五元充值数据
    interface LoadWuYuanChongZhiListRsp extends RspObj {
        // 累计充值天数
            dayCount ?:number
        // 今天是否已经充值
            isTodayPay ?:boolean
        // 单笔充值数据
            detailList ?:Array<WuYuanChongZhiDetailInfoUnit>

    }

// [请求]领取五元充值
    interface GetWuYuanChongZhiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取五元充值
    interface GetWuYuanChongZhiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]加载活动任务数据
    interface LoadMissionActivityListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载活动任务数据
    interface LoadMissionActivityListRsp extends RspObj {
        // 活动任务数据
            detailList ?:Array<MissionActivityInfoUnit>

    }

// [请求]领取活动任务奖励
    interface GetMissionActivityRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取活动任务奖励
    interface GetMissionActivityRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]登录送礼活动
    interface GetLeiJiDengLuRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]登录送礼活动
    interface GetLeiJiDengLuRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]登录送礼数据
    interface LoadLeiJiDengLuListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]登录送礼数据
    interface LoadLeiJiDengLuListRsp extends RspObj {
        // 获取累计充值列表
            leiJiDengLuList ?:Array<ActivityDetailInfoUnit>
        // 累计登录天数
            tianShu ?:number
        // 补签信息
            addLoginDayInfo ?:LeiJiDengLuAddLoginDayUnit

    }

// [请求]加载道具收集活动数据
    interface LoadItemCollectActivityListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载道具收集活动数据
    interface LoadItemCollectActivityListRsp extends RspObj {
        // 道具收集活动数据
            detailList ?:Array<ItemCollectActivityInfoUnit>

    }

// [请求]领取道具收集活动数据
    interface GetItemCollectActivityRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取道具收集活动数据
    interface GetItemCollectActivityRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]加载一冲选领数据
    interface LoadYiChongXuanLingListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载一冲选领数据
    interface LoadYiChongXuanLingListRsp extends RspObj {
        // 一冲选领数据
            detailList ?:Array<YiChongXuanLingDetailInfoUnit>
        // 冲多少钱
            chongZhiJinE ?:number
        // 能不能领 能 true        不能  false
            canGet ?:boolean

    }

// [请求]领取一冲选领
    interface GetYiChongXuanLingReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取一冲选领
    interface GetYiChongXuanLingRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]摇奖面板
    interface YaoJiangPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]摇奖面板
    interface YaoJiangPanelRsp extends RspObj {
        // 面板展示多少个道具 仅仅是面板 不是发奖
            jiangliList ?:Array<JiangliInfoUnit>
        // 摇奖系统记录
            yaoJiangLogList ?:Array<string>

    }

// [请求]摇奖
    interface YaoJiangReq extends ReqObj {
        // 0 摇奖1次    1 10次
            type ?:number
        // 活动ID
            activityId ?:number
    }

// [返回]摇奖
    interface YaoJiangRsp extends RspObj {
        // 出哪一个  从0开始  打开面板消息里 defList的index
            indexList ?:Array<number>
        // 出的道具的数量
            countList ?:Array<number>
        // 摇奖系统记录
            yaoJiangLogList ?:Array<string>

    }

// [请求] 活动副本面板
    interface HuoDongFuBenPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 活动副本面板
    interface HuoDongFuBenPanelRsp extends RspObj {
        // 面板展示多少个道具 仅仅是面板 不是发奖
            huoDongFuBenJiangLiPanelInfoList ?:Array<HuoDongFuBenJiangLiPanelInfoUnit>

    }

// [请求] 进入副本
    interface EnterFuBenReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 
            defId ?:number
    }

// [返回] 进入副本
    interface EnterFuBenRsp extends RspObj {
        // 怪物信息
            huoDongFuBenGuaiWuInfoList ?:Array<HuoDongFuBenGuaiWuInfoUnit>

    }

// [请求] 打活动副本
    interface FightHuoDongFuBenReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 
            defId ?:number
        // 怪物位置  从0开始
            index ?:number
    }

// [返回] 打活动副本
    interface FightHuoDongFuBenRsp extends RspObj {
        // 掉进仓库的奖励列表
            jinCangKuJiangLiList ?:Array<JiangliInfoUnit>
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 总伤害
            damage ?:number
        // 是否出现隐藏boss
            appearYinCangBoss ?:boolean
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求] 扫荡副本
    interface HuoDongFuBenSaoDangReq extends ReqObj {
        // 0扫荡本地图内的怪物    1扫荡所有门票
            type ?:number
        // 
            defId ?:number
        // 活动ID
            activityId ?:number
    }

// [返回] 扫荡副本
    interface HuoDongFuBenSaoDangRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求] 购买门票 买完了就直接就进去了
    interface GouMaiMenPiaoReq extends ReqObj {
        // 
            defId ?:number
        // 活动ID
            activityId ?:number
        // 
            count ?:number
    }

// [返回] 购买门票 买完了就直接就进去了
    interface GouMaiMenPiaoRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]开服每日一冲列表
    interface GetKaiFuMeiRiYiChongListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]开服每日一冲列表
    interface GetKaiFuMeiRiYiChongListRsp extends RspObj {
        // 
            kaiFuHuoDongInfoList ?:Array<KaiFuHuoDongInfoUnit>
        // 完成度给的百分比值 前端直接用就行了
            wanChengDu ?:number
        // 当前是开服时间的第几天
            openServerDay ?:number

    }

// [请求]领取每日一冲奖励
    interface DoGetKaiFuMeiRiYiChongReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取每日一冲奖励
    interface DoGetKaiFuMeiRiYiChongRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]开服登录列表
    interface GetKaiFuDengLuListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]开服登录列表
    interface GetKaiFuDengLuListRsp extends RspObj {
        // 
            kaiFuHuoDongInfoList ?:Array<KaiFuHuoDongInfoUnit>
        // 完成度给的百分比值 前端直接用就行了
            wanChengDu ?:number
        // 当前是开服时间的第几天
            openServerDay ?:number

    }

// [请求]领取登录奖励
    interface DoGetKaiFuDengLuReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取登录奖励
    interface DoGetKaiFuDengLuRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]开服半价购买列表
    interface GetKaiFuBanJiaGouMaiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [请求]开服半价购买列表
    interface GetKaiFuBanJiaGouMaiListRsp extends RspObj {
        // 
            kaiFuHuoDongInfoList ?:Array<KaiFuHuoDongInfoUnit>
        // 完成度给的百分比值 前端直接用就行了
            wanChengDu ?:number
        // 当前是开服时间的第几天
            openServerDay ?:number

    }

// [请求]购买
    interface DoGetKaiFuBanJiaGouMaiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]购买
    interface DoGetKaiFuBanJiaGouMaiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求] 七日竞赛活动列表数据
    interface SevenMatchPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 七日竞赛活动列表数据
    interface SevenMatchPanelRsp extends RspObj {
        // 竞赛活动列表数据
            sevenMatchList ?:Array<SevenMatchInfoUnit>
        // 在线时长免费奖励内容
            onlineReward ?:JiangliInfoUnit
        // 下次领取在线时长奖励的时间戳，单位：毫秒
            nextTimeForOnlineReward ?:number

    }

// [请求] 七日竞赛玩家排行数据
    interface SevenMatchPlayerListReq extends ReqObj {
        // 竞赛活动ID
            matchId ?:number
    }

// [返回] 七日竞赛玩家排行数据
    interface SevenMatchPlayerListRsp extends RspObj {
        // 奖励数据
            rewardList ?:Array<ServerMatchRewardInfoUnit>
        // 玩家数据
            playerList ?:Array<ServerMatchPlayerInfoUnit>
        // 我的排行
            position ?:number
        // 达到xx后全服可领取奖励情况，可为undefined
            firstOneAllServerReward ?:FirstOneAllServerRewardUnit
        // 一元特价表Id，0表示买光了
            yiyuantejiaDefId ?:number
        // 我的今日累计充值金额，单位：元
            myRmb ?:number

    }

// [请求] 购买奖励请求预览
    interface GetSevenMatchGiftInfoReq extends ReqObj {
        // 竞赛活动ID
            matchId ?:number
    }

// [返回] 购买奖励请求预览
    interface GetSevenMatchGiftInfoRsp extends RspObj {
        // 是否调整
            isJump ?:boolean
        // 跳转到
            jumpto ?:number
        // 礼包标题
            giftTitle ?:string
        // 礼包描述
            giftDes ?:string
        // 礼包列表
            liBaoList ?:Array<SevenMatchGiftInfoUnit>

    }

// [请求] 购买七日竞赛礼包
    interface BuySevenMatchGiftReq extends ReqObj {
        // 竞赛活动ID
            matchId ?:number
        // 礼包编号
            liBaoNum ?:number
    }

// [返回] 购买七日竞赛礼包
    interface BuySevenMatchGiftRsp extends RspObj {
        // 可获得奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]每日一够列表
    interface GetMeiRiYiGouListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]每日一够列表
    interface GetMeiRiYiGouListRsp extends RspObj {
        // 
            detailList ?:Array<MeiRiYiGouInfoUnit>
        //  当前可买最大天数
            getDay ?:number

    }

// [请求]领取每日一购奖励
    interface DoGetMeiRiYiGouJiangLiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取领取每日一购奖励
    interface DoGetMeiRiYiGouJiangLiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]加载单笔充值数据
    interface LoadDanBiGouMaiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载单笔充值数据
    interface LoadDanBiGouMaiListRsp extends RspObj {
        // 单笔充值数据
            detailList ?:Array<DanBiGouMaiDetailInfoUnit>
        // 对于特殊物品在商城也能卖 但是有限购 活动列表也需要展示一个购买次数
            hasBuy ?:number
        // 限购个数 这是总个数
            limitBuy ?:number

    }

// [请求]获得单笔购买奖励
    interface GetDanBiGouMaiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
        // 购买数量
            count ?:number
    }

// [反馈]获得单笔购买奖励
    interface GetDanBiGouMaiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 静态表id
            defId ?:number
        // 已经购买次数,用于限购判断
            buyCount ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]加载显示升阶数据
    interface LoadXianShiShengJieListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载限时升阶数据
    interface LoadXianShiShengJieListRsp extends RspObj {
        // 限时升阶数据
            detailList ?:Array<XianShiShengJieDetailInfoUnit>

    }

// [请求]获得显示升阶奖励
    interface GetXianShiShengJieReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]获得显示升阶奖励
    interface GetXianShiShengJieRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]加载大转盘信息(新)
    interface LoadDaZhuanPanPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载大转盘信息(新)
    interface LoadDaZhuanPanPanelRsp extends RspObj {
        // 奖励信息
            rewardList ?:Array<JiangliInfoUnit>
        // 翻倍开始时间,单位:秒
            startTime ?:number
        // 翻倍结束时间,单位:秒
            finishTime ?:number
        // 倍率
            scale ?:number

    }

// [请求]大转盘摇奖(新)
    interface GetDaZhuanPanRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]大转盘摇奖(新)
    interface GetDaZhuanPanRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励
            reward ?:JiangliInfoUnit
        // 奖励Index
            index ?:number

    }

// 请求锦标赛排行信息
    interface GetKuaFuJinBiaoSaiRankInfosReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 请求锦标赛排行信息
    interface GetKuaFuJinBiaoSaiRankInfosRsp extends RspObj {
        // 个人排行信息（ <=11 个数据,最后一个是自己的)
            jinBiaoSaiGeRenRankInfo ?:Array<JinBiaoSaiGeRenInfoUnit>
        // 公会排行信息( <=11 个数据,最后一个是自己的)
            jinBiaoSaiGongHuiInfos ?:Array<JinBiaoSaiGongHuiInfoUnit>

    }

// 跨服锦标赛副本信息
    interface GetKuaFuJinBiaoSaiInfosReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 跨服锦标赛副本信息
    interface GetKuaFuJinBiaoSaiInfosRsp extends RspObj {
        // 怪物信息
            monsters ?:Array<HuoDongFuBenGuaiWuInfoUnit>
        // 积分箱子信息
            scoreBoxs ?:Array<KuaFuJBSScoreBoxInfoUnit>
        // 挑战次数信息
            fightTimesInfo ?:KuaFuJBSFightInfoUnit
        // 个人积分
            geRenScore ?:number
        // 公会积分
            gongHuiScore ?:number

    }

// 挑战boss
    interface KuaFuJinBiaoSaiFightReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 怪物id
            defId ?:number
        // 怪物所在位置(从0开始)
            index ?:number
        // 是否是一键击杀
            isFive ?:boolean
        // 怪物id
            bossIds ?:Array<number>
    }

// 挑战boss
    interface KuaFuJinBiaoSaiFightRsp extends RspObj {
        // 1:胜利 2:失败
            win ?:number
        // 敌人头像id
            enemyTouxiangId ?:number
        // 得到的奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 购买挑战次数
    interface KuaFuJinBiaoSaiBuyTimesReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 购买的次数
            buyTimes ?:number
    }

// 购买挑战次数
    interface KuaFuJinBiaoSaiBuyTimesRsp extends RspObj {
        // 拥有的挑战次数
            haveFightTimes ?:number

    }

// 购买箱子
    interface KuaFuJinBiaoSaiBuyBoxReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 箱子defId
            defId ?:number
        // 购买的箱子个数
            buyCount ?:number
    }

// 购买箱子
    interface KuaFuJinBiaoSaiBuyBoxRsp extends RspObj {
        // 拥有的数量
            haveCount ?:number

    }

// 打开箱子(上限为1000个,代码限制就行)
    interface KuaFuJinBiaoSaiOpenBoxReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 箱子defId
            defId ?:number
    }

// 打开箱子
    interface KuaFuJinBiaoSaiOpenBoxRsp extends RspObj {
        // 箱子defId
            defId ?:number
        // 拥有的数量
            haveCount ?:number
        // 个人积分
            geRenScore ?:number
        // 公会积分
            gongHuiScore ?:number

    }

// [请求]切换地图
    interface SwitchMapReq extends ReqObj {
        // 地图Id
            mapId ?:number
    }

// [返回]切换地图
    interface SwitchMapRsp extends RspObj {
        // 地图Id
            mapId ?:number
        // 当前地图的天气  0代表没有天气
            tianQi ?:number
        // 天气神兽的武将defid
            heroDefId ?:number
        // 是否是引导神兽
            isYInDaoShenShou ?:boolean

    }

// [请求]获取怪物
    interface GetGuaiWuReq extends ReqObj {
    }

// [返回]获取怪物
    interface GetGuaiWuRsp extends RspObj {
        // 出现方式 打还是抓 出现方式 1 以抓宠方式出现 2 以战斗方式出现
            way ?:number
        // 地图Id
            mapId ?:number

    }

// [请求]打怪物
    interface FightGuaiWuReq extends ReqObj {
    }

// [返回]打怪物
    interface FightGuaiWuRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]获取跟天气有关的怪物
    interface GetTianQiGuaiWuReq extends ReqObj {
    }

// [返回]获取跟天气有关的怪物
    interface GetTianQiGuaiWuRsp extends RspObj {
        // 出现方式 打还是抓  出现方式 1 以抓宠方式出现 2 以战斗方式出现
            way ?:number
        // 天气Id
            tianQiId ?:number
        // 天气神兽的武将defId
            heroDefId ?:number
        // 是不是引导中的boss
            isYInDaoShenShou ?:boolean

    }

// [请求]获得天气地图list
    interface GetTianQiMapListReq extends ReqObj {
    }

// [返回]获得天气地图list
    interface GetTianQiMapListRsp extends RspObj {
        // 跟天气有关的那些怪物的list
            zhanBaiFenXiList ?:Array<TianQiGuaiWuUnit>

    }

// [请求]打天气怪物
    interface FightTianQiGuaiWuReq extends ReqObj {
        // 地图Id
            mapId ?:number
    }

// [返回]打天气怪物
    interface FightTianQiGuaiWuRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】设定当前使用战阵
    interface ZhanZhenNowReq extends ReqObj {
        // 玩家需要使用的战阵
            zhanzhenDbID ?:number
    }

// 【反馈】设定当前使用战阵，返回当前使用战阵dbID
    interface ZhanZhenNowRsp extends RspObj {
        // 玩家需要使用的战阵
            zhanzhenDbID ?:number

    }

// 【请求】合成战阵
    interface MakeZhanZhenReq extends ReqObj {
        // 玩家拥有的某一个战阵碎片数据库dbID
            zhanzhenSuiPianDbID ?:number
    }

// 【反馈】返回新合成战阵的dbID
    interface MakeZhanZhenRsp extends RspObj {
        // 返回新合成战阵的dbID
            zhanzhenDbID ?:number

    }

// 【请求】战阵强化
    interface ZhanZhenQianghuaReq extends ReqObj {
        // 玩家要强化的战阵
            zhanzhenDbID ?:number
        // 本次强化需要吃掉的战阵或者战阵碎片
            useZhanzhenDbID ?:number
        // 0 - 表示需要服务器判断是否有经验损失，并提示， 1 - 无条件吃掉，损失经验值。
            confirmFlag ?:number
    }

// 【反馈】战阵强化
    interface ZhanZhenQianghuaRsp extends RspObj {
        // 本次强化的战阵
            zhanzhenDbID ?:number
        // 强化后战阵等级
            zhanzhenLevel ?:number
        // 强化后战阵经验
            zhanzhenExp ?:number

    }

// 【请求】获得战阵列表
    interface GetZhanZhenListReq extends ReqObj {
        // 玩家ID
            playerID ?:number
    }

// 【反馈】获得战阵列表
    interface GetZhanZhenListRsp extends RspObj {
        // 玩家拥有的战阵列表
            zhanzhenList ?:Array<ZhanzhenInfoUnit>

    }

// 【请求】获得战阵碎片列表
    interface GetZhanZhenSuiPianListReq extends ReqObj {
        // 玩家ID
            playerID ?:number
    }

// 【反馈】获得战阵碎片列表
    interface GetZhanZhenSuiPianListRsp extends RspObj {
        // 玩家拥有的战阵碎片列表
            zhanzhenSuipianList ?:Array<ZhanzhenSuipianInfoUnit>
        // 战力排行
            zhanLiPaiHang ?:number
        // 玩家ID
            piPeiPaiHang ?:number

    }

// [请求]公会boss面板
    interface GongHuiBossPanelReq extends ReqObj {
    }

// [返回]公会boss面板   打死boss的推送/召唤 全公会在线玩家推送也会推送
    interface GongHuiBossPanelRsp extends RspObj {
        // bossDbId
            dbId ?:number
        // 当前参加人数
            renShu ?:number
        // 公会等级
            gongHuiLevel ?:number
        // true 可以打  false 不可以
            canFight ?:boolean
        // 剩余血量的百分比 已经百分比化 直接使用
            hp ?:number
        // 可以打boss的时间戳
            cdTime ?:number
        // 重置时间戳
            leftCdTime ?:number
        // 剩余可以召唤的次数
            leftZhaoHuanCiShu ?:number
        // 公会bossdefId
            defId ?:number
        // 奖励列表
            gongHuiBossGuaiWuInfoList ?:Array<GongHuiBossGuaiWuInfoUnit>
        // 是否自动召唤
            isAutoZhaoHuan ?:boolean

    }

// [请求]打公会boss
    interface FightGongHuiBossReq extends ReqObj {
        // bossDbId
            dbId ?:number
        // 是否扫荡
            isSweep ?:boolean
    }

// [反馈]打公会boss
    interface FightGongHuiBossRsp extends RspObj {
        // 参加人数
            joinPlayerNum ?:number
        // 个人奖励列表
            geRenJiangliList ?:Array<JiangliInfoUnit>
        // 公会奖励列表
            extraRewardList ?:Array<JiangliInfoUnit>
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 本次获得积分
            score ?:number
        // 本次BOSS受到的伤害
            damage ?:number
        // 打的和看到的是否一样
            isSame ?:boolean
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>
        // 是否扫荡
            isSweep ?:boolean

    }

// [请求]召唤  想本公会所有在下人员推送面板消息
    interface ZhaoHuanGongHuiBossReq extends ReqObj {
    }

// [请求]公会boss伤害排行列表
    interface GongHuiBossShangHaiListReq extends ReqObj {
        // boss Id
            bossId ?:number
    }

// [返回]公会boss伤害排行列表
    interface GongHuiBossShangHaiListRsp extends RspObj {
        // 伤害日志排行
            shangHaiLogList ?:Array<GongHuiBossLogUnit>
        // 自己的排行
            ownPaiMing ?:number

    }

// [请求]公会boss是否自动召唤
    interface GongHuiBossAutoZhaoHuanReq extends ReqObj {
        // 是否自动召唤 true 自动 false不自动
            isAutoZhaoHuan ?:boolean
    }

// [返回]公会boss是否自动召唤
    interface GongHuiBossAutoZhaoHuanRsp extends RspObj {
        // 公会Id
            gonghuiId ?:number
        // 是否自动召唤 true 自动 false不自动
            isAutoZhaoHuan ?:boolean

    }

// 打开日充累充面板
    interface GetRiChongLeiChongListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 打开日充累充面板
    interface GetRiChongLeiChongListRsp extends RspObj {
        // 每日充值的奖励列表
            meiRiDetailList ?:Array<RiChongLeiChongInfoUnit>
        // 累计充值的奖励列表
            leiJiDetailList ?:Array<RiChongLeiChongInfoUnit>
        // 期间的充值总金额：单位(元)
            totalCount ?:number
        // 完成累充的数字
            completeLeiJiCount ?:number

    }

// 领取累计充值奖励
    interface DoGetRiChongLeiChongReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// 领取累计充值奖励
    interface DoGetRiChongLeiChongRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 首冲团购面板
    interface ShouChongTuanGouPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 首冲团购面板
    interface ShouChongTuanGouPanelRsp extends RspObj {
        // 团购信息
            dataList ?:Array<ShouChongTuanGouUnit>
        // 活动期间产生首充的人
            totalPeople ?:number
        // 活动期间总充值
            totalChongZhi ?:number

    }

// 领取首充团购奖励
    interface GetShouChongTuanGouRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // defId
            defId ?:number
        // 1 免费 2 首充 3高级
            oType ?:number
    }

// 领取首充团购奖励
    interface GetShouChongTuanGouRewardRsp extends RspObj {
        // 是否充值成功
            isSuccess ?:boolean

    }

// 打开签到奖励
    interface GetQianDaoJiangLiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 打开签到奖励
    interface GetQianDaoJiangLiListRsp extends RspObj {
        // 
            info1List ?:Array<RiChongLeiChongInfoUnit>
        // 
            info2List ?:Array<RiChongLeiChongInfoUnit>
        // 
            info3List ?:Array<RiChongLeiChongInfoUnit>
        // 
            info4List ?:Array<RiChongLeiChongInfoUnit>
        // 完成累充的数字
            completeLeiJiCount ?:number

    }

// 领取签到奖励
    interface DoGetQianDaoJiangReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// 领取签到奖励
    interface DoGetQianDaoJiangRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]天天返利面板
    interface TianTianFanLiPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载天天返利面板
    interface TianTianFanLiPanelRsp extends RspObj {
        // 所需充值金额（元）
            needRechargeCount ?:number
        // 已累计充值天数
            hasRechargeDays ?:number
        // 特殊奖励数据
            specialBonusList ?:Array<TianTianFanLiInfoUnit>
        // 天天返利数据
            detailList ?:Array<TianTianFanLiInfoUnit>

    }

// [请求]领取天天返利奖励
    interface ReceiveTianTianFanLiRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取天天返利奖励
    interface ReceiveTianTianFanLiRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 静态表id
            defId ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]神秘来电面板
    interface ShenMiLaiDianPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]神秘来电面板
    interface ShenMiLaiDianPanelRsp extends RspObj {
        // 结束时间 毫秒
            endTime ?:number
        // 充值ID
            chongZhiId ?:number
        // 老价格
            oldPrice ?:number
        // 新价格
            newPrice ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 打开登录好礼
    interface GetDengLuHaoLiListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 打开登录好礼
    interface GetDengLuHaoLiListRsp extends RspObj {
        // 
            info1List ?:Array<RiChongLeiChongInfoUnit>
        // 
            info2List ?:Array<RiChongLeiChongInfoUnit>
        // 
            info3List ?:Array<RiChongLeiChongInfoUnit>
        // 
            info4List ?:Array<RiChongLeiChongInfoUnit>
        // 完成累充的数字
            completeLeiJiCount ?:number

    }

// 领取登录好礼
    interface DoDengLuHaoLiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// 领取登录好礼
    interface DoDengLuHaoLiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]多倍奖励面板
    interface DuoBeiJiangLiPanelReq extends ReqObj {
        // 多倍奖励的类型
            type ?:number
    }

// [返回]多倍奖励面板
    interface DuoBeiJiangLiPanelRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 领取状态：0、不可领；1、可以领，但尚未领取 2、已领取；
            doGet ?:number
        // 活动说明
            shuoMing ?:string

    }

// [请求]领取多倍奖励
    interface GetDuoBeiJiangLiReq extends ReqObj {
        // 多倍奖励的类型
            type ?:number
    }

// [返回]领取多倍奖励
    interface GetDuoBeiJiangLiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]跨服秒杀面板
    interface KuaFuMiaoShaPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]跨服秒杀面板
    interface KuaFuMiaoShaPanelRsp extends RspObj {
        // 钻石场
            zhanShiChangList ?:Array<KuaFuMiaoShaInfoUnit>
        // 金券场
            jinQuanChangList ?:Array<KuaFuMiaoShaInfoUnit>
        // 下期预览  没有就是没有下期了 前端自己判断
            xiaQiYuLanList ?:Array<KuaFuMiaoShaInfoUnit>
        // 结束时间戳 毫秒
            countdown ?:number

    }

// [请求]买跨服秒杀
    interface BuyKuaFuMiaoShaReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [返回]买跨服秒杀
    interface BuyKuaFuMiaoShaRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]开服限购大R礼包活动
    interface KaifuXiangouInfoPanelReq extends ReqObj {
        // 活动ID
            activityIdList ?:Array<number>
    }

// [返回]开服限购大R礼包活动
    interface KaifuXiangouInfoPanelRsp extends RspObj {
        // 开服限购大R礼包活动内容
            kaifuXiangouInfoList ?:Array<KaifuXiangouInfoUnit>
        // 活动红点列表
            newActivityTipList ?:Array<NewActivityTipsInfoUnit>

    }

// [请求]买开服限购大R礼包
    interface BuyKaifuXiangouInfoReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [返回]买开服限购大R礼包
    interface BuyKaifuXiangouInfoRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求] 领取达到xx后的全服奖励
    interface TakeFirstOneAllServerRewardReq extends ReqObj {
        // 竞赛活动ID
            matchId ?:number
        // FirstOndDef.Id
            defId ?:number
    }

// [返回] 领取达到xx后的全服奖励
    interface TakeFirstOneAllServerRewardRsp extends RspObj {
        // 竞赛活动ID
            matchId ?:number
        // 下一个可领取的全服奖励情况，如果没有下一个，则返回undefined
            firstOneAllServerReward ?:FirstOneAllServerRewardUnit

    }

// [请求] 限时召唤面板
    interface XianShiZhaoHuanPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 限时召唤面板
    interface XianShiZhaoHuanPanelRsp extends RspObj {
        // 消耗
            costInfo ?:JiangliInfoUnit
        // 普通奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>
        // 心愿列表
            chooseLiList ?:Array<XianShiZhaoHuanChooseUnit>
        // 我选择的心愿
            myChoose ?:number

    }

// [请求] 限时召唤
    interface GetXianShiZhaoHuanReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 召唤次数
            num ?:number
    }

// [返回] 限时召唤
    interface GetXianShiZhaoHuanRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求] 限时召唤选择心愿
    interface GetXianShiZhaoHuanChooseReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 心愿Id
            defId ?:number
    }

// [返回] 限时召唤选择心愿
    interface GetXianShiZhaoHuanChooseRsp extends RspObj {

    }

// [请求] 皇权诏令面板
    interface HuangquanZhaolingPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 皇权诏令面板
    interface HuangquanZhaolingPanelRsp extends RspObj {
        // 当前活跃值
            huoyue ?:number
        // 是否已经解锁诏令
            isBought ?:boolean
        // 充值档位ID
            chongzhiId ?:number
        // 是否可买活跃值
            isCanBuyHuoyue ?:boolean
        // 购买活跃值单价
            costBuy ?:JiangliInfoUnit
        // 充值档位对应的活跃值
            huoyueByChongzhi ?:number
        // 皇权诏令活跃奖励列表
            detailList ?:Array<HuangquanZhaolingInfoUnit>
        // 是否可获得称号
            isCanGetChenghao ?:boolean

    }

// [请求] 皇权诏令领取奖励
    interface GetHuangquanZhaolingRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 策划表Id
            defId ?:number
    }

// [返回] 皇权诏令领取奖励
    interface GetHuangquanZhaolingRewardRsp extends RspObj {
        // 策划表Id
            defId ?:number
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>
        // 0:未领取过；1:普通奖励领取过；2:全部领取过
            rewardStatus ?:number

    }

// [请求] 皇权诏令购买活跃
    interface BuyHuangquanZhaolingHuoyueReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 购买的活跃度
            count ?:number
    }

// [返回] 皇权诏令购买活跃
    interface BuyHuangquanZhaolingHuoyueRsp extends RspObj {
        // 购买后的当前活跃度
            huoyue ?:number

    }

// [请求] 登录现金红包面板
    interface DengluHongbaoPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 登录现金红包面板
    interface DengluHongbaoPanelRsp extends RspObj {
        // 当前红包可提现余额
            totalCashAmount ?:number
        // 登录现金红包列表
            detailList ?:Array<DengluHongbaoInfoUnit>

    }

// [请求] 登录现金红包开红包
    interface OpenDengluHongbaoReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 策划表Id
            defId ?:number
    }

// [返回] 登录现金红包开红包
    interface OpenDengluHongbaoRsp extends RspObj {
        // 当前红包可提现余额
            totalCashAmount ?:number
        // 策划表Id
            defId ?:number
        // 返现金额，单位：分
            amount ?:number
        // 0不可领 1可开红包 2可领红包 3已经领取
            status ?:number

    }

// [请求] 红包提现
    interface GetCashFromHongbaoRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 策划表类型，对应leixing字段
            renwuType ?:number
        // 玩家此次充值时所用的设备系统平台，ios/android
            os ?:string
        // 额外参数，其他渠道相关附加参数
            ext ?:string
    }

// [返回] 红包提现
    interface GetCashFromHongbaoRewardRsp extends RspObj {
        // 当前红包可提现余额
            totalCashAmount ?:number

    }

// [请求] 任务现金红包面板
    interface RenwuHongbaoPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 任务现金红包面板
    interface RenwuHongbaoPanelRsp extends RspObj {
        // 当前红包可提现余额
            totalCashAmount ?:number
        // 描述ID，对应beizhuid字段
            beizhuDefId ?:number
        // 任务现金红包列表
            detailList ?:Array<RenwuHongbaoInfoUnit>

    }

// [请求] 任务现金红包开红包
    interface OpenRenwuHongbaoReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 策划表类型，对应leixing字段
            renwuType ?:number
    }

// [返回] 任务现金红包开红包
    interface OpenRenwuHongbaoRsp extends RspObj {
        // 当前红包可提现余额
            totalCashAmount ?:number
        // 策划表类型，对应leixing字段
            renwuType ?:number
        // 返现金额，单位：分
            amount ?:number
        // 0不可领 1可开红包 2可领红包 3已经领取
            status ?:number

    }

// [请求]申请开服限购活动的折扣
    interface ApplyDiscountOfKaifuXiangouReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [返回]申请开服限购活动的折扣
    interface ApplyDiscountOfKaifuXiangouRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 是否可以继续申请折扣
            isApplyToDiscount ?:boolean
        // 新的折扣价
            discountPrice ?:number

    }

// [请求] 限时等级礼包面板
    interface XianShiDengJiLiBaoPanelReq extends ReqObj {
    }

// [返回] 限时等级礼包面板
    interface XianShiDengJiLiBaoPanelRsp extends RspObj {
        // 礼包列表
            libaoList ?:Array<XianShiDengJiLiBaoUnit>

    }

// [请求] 领取限时等级礼包奖励
    interface GetXianShiDengJiLiBaoRewardReq extends ReqObj {
        // 礼包Id
            defId ?:number
    }

// [返回] 领取限时等级礼包奖励
    interface GetXianShiDengJiLiBaoRewardRsp extends RspObj {

    }

// [请求] 自定义礼包充值消费礼包活动面板
    interface DiyGiftPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 自定义礼包充值消费礼包活动面板
    interface DiyGiftPanelRsp extends RspObj {
        // 礼包列表
            detailList ?:Array<DiyGiftInfoUnit>

    }

// [请求] 自定义礼包充值消费礼包奖励配置面板
    interface DiySelectRewardPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 礼包ID
            defId ?:number
    }

// [返回] 自定义礼包充值消费礼包奖励配置面板
    interface DiySelectRewardPanelRsp extends RspObj {
        // 位置列表
            positionInfoList ?:Array<DiyGiftPositionInfoUnit>

    }

// [请求] 自定义礼包充值消费礼包确认选择的奖励
    interface DiySelectRewardConfirmReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 礼包ID
            defId ?:number
        // 位置奖励信息列表
            positionInfoList ?:Array<DiyGiftPositionInfoUnit>
    }

// [返回] 自定义礼包充值消费礼包确认选择的奖励
    interface DiySelectRewardConfirmRsp extends RspObj {
        // 成功
            isSuccess ?:boolean

    }

// [请求] 购买自定义礼包充值消费礼包
    interface DiyGiftBuyReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 礼包ID
            defId ?:number
    }

// [返回] 购买自定义礼包充值消费礼包
    interface DiyGiftBuyRsp extends RspObj {
        // 成功
            isSuccess ?:boolean
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// [请求] 折扣商店活动面板
    interface ZhekouShangdianPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 折扣商店活动面板
    interface ZhekouShangdianPanelRsp extends RspObj {
        // 商品列表
            detailList ?:Array<ZhekouShangdianGoodsUnit>
        // 刷新价格信息
            refreshPriceInfo ?:JiangliInfoUnit
        // 剩余的免费次数
            freeRefreshCount ?:number
        // 剩余的付费刷新次数
            paidRefreshCount ?:number

    }

// [请求] 折扣商店活动刷新商品
    interface ZhekouShangdianRefreshGoodsReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 折扣商店活动刷新商品
    interface ZhekouShangdianRefreshGoodsRsp extends RspObj {
        // 商品列表
            detailList ?:Array<ZhekouShangdianGoodsUnit>
        // 刷新价格信息
            refreshPriceInfo ?:JiangliInfoUnit
        // 剩余的免费次数
            freeRefreshCount ?:number
        // 剩余的付费刷新次数
            paidRefreshCount ?:number

    }

// [请求] 折扣商店活动购买商品
    interface ZhekouShangdianBuyGoodsReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 商品Def ID
            defId ?:number
        // 购买个数
            count ?:number
    }

// [返回] 折扣商店活动购买商品
    interface ZhekouShangdianBuyGoodsRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// [请求] 各类基金活动面板
    interface FundActivityPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 各类基金活动面板
    interface FundActivityPanelRsp extends RspObj {
        // 是否已经解锁诏令
            isBought ?:boolean
        // 解锁所需的消耗
            unlockInfo ?:JiangliInfoUnit
        // 对应条件的当前数值
            currValue ?:number
        // 各类基金活动明细列表
            detailList ?:Array<FundActivityDetailInfoUnit>
        // 对应条件的当前条件描述文本
            currValueDesc ?:string

    }

// [请求] 各类基金活动领取奖励
    interface GetFundActivityRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 策划表Id
            defId ?:number
        // 1、免费奖励；2、付费豪华奖励
            rewardType ?:number
    }

// [返回] 各类基金活动领取奖励
    interface GetFundActivityRewardRsp extends RspObj {
        // 策划表Id
            defId ?:number
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>
        // 0:未领取过；1:普通奖励领取过；2:全部领取过
            rewardStatus ?:number
        // (关卡基金专用)1、免费奖励；2、付费豪华奖励
            rewardType ?:number
        // (关卡基金专用)0:未领取过；2:可领取；3:普通奖励领取过；
            freeStatus ?:number
        // (关卡基金专用)0:未领取过；1:满足条件但是未付费解锁；2:可领取；3:奖励已领取过
            costStatus ?:number

    }

// [请求] 各类基金活动解锁高级奖励
    interface UnlockFundActivityHighRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回] 各类基金活动解锁高级奖励
    interface UnlockFundActivityHighRewardRsp extends RspObj {
        // 是否解锁成功
            isSuccess ?:boolean

    }

// [请求] 七日商店购买商品
    interface BuySevenDayShopGoodsReq extends ReqObj {
        // 活动id
            activityId ?:number
        // sevendayshop表ID
            defId ?:number
    }

// [返回] 七日商店购买商品
    interface BuySevenDayShopGoodsRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean
        // 活动id
            activityId ?:number
        // sevendayshop表ID
            defId ?:number
        // 奖励内容
            reward ?:JiangliInfoUnit

    }

// [请求]金券消耗排行列表
    interface XiaohaoPaihangListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]金券消耗排行列表
    interface XiaohaoPaihangListRsp extends RspObj {
        // 奖励数据
            rewardList ?:Array<XiaohaoPaihangRewardInfoUnit>
        // 玩家数据
            playerList ?:Array<XiaohaoPaihangPlayerInfoUnit>
        // 我的排行
            position ?:number
        // 积分说明：jifenpaihang表对应模板下的第一行数据shuoming列
            scoreDesc ?:string
        // 我的今日累计充值金额，单位：元
            myRmb ?:number

    }

// [请求]活跃任务数据列表
    interface HuoyueRenwuMissionListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]活跃任务数据列表
    interface HuoyueRenwuMissionListRsp extends RspObj {
        // 任务数据
            missionList ?:Array<HuoyueRenwuMissionInfoUnit>

    }

// [请求]回馈礼包活动充值登录礼包活动
    interface GetRechargeLoginGiftRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]回馈礼包活动充值登录礼包活动
    interface GetRechargeLoginGiftRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]回馈礼包活动充值登录礼包活动面板消息
    interface RechargeLoginGiftPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]回馈礼包活动充值登录礼包活动面板消息
    interface RechargeLoginGiftPanelRsp extends RspObj {
        // 获取累计登录列表
            leiJiDengLuList ?:Array<ActivityDetailInfoUnit>
        // 累计登录天数
            tianShu ?:number
        // 是否已激活累计登陆
            isActive ?:boolean
        // 激活条件及激活奖励
            activeInfo ?:ActivityDetailActiveInfoUnit

    }

// [请求]幸运转盘活动面板消息
    interface LuckyWheelPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]幸运转盘活动面板消息
    interface LuckyWheelPanelRsp extends RspObj {
        // 格子列表
            gridList ?:Array<LuckyWheelGridUnit>
        // 剩余摇奖次数
            leftGetTimes ?:number
        // 下一次摇奖的消耗
            nextCondition ?:LuckyWheelRewardConditionUnit

    }

// [请求]幸运转盘活动摇奖
    interface GetLuckyWheelRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]幸运转盘活动摇奖
    interface GetLuckyWheelRewardRsp extends RspObj {
        // 格子ID，对应xingyunzhuanpan表Id
            defId ?:number
        // 获得的奖励
            rewardInfo ?:JiangliInfoUnit
        // 下一次摇奖的消耗
            nextCondition ?:LuckyWheelRewardConditionUnit

    }

// [请求]金猪存钱罐活动面板消息
    interface PiggyBankPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]金猪存钱罐活动面板消息
    interface PiggyBankPanelRsp extends RspObj {
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>
        // 当前章节Id
            chapterDefId ?:number
        // 当前层数
            nowCount ?:number
        // 条件：章节
            needChapterDefId ?:number
        // 条件：层数
            needCount ?:number
        // 充值金额表Id
            chongzhiId ?:number
        // 原价：单位RMB元
            originalRmb ?:number
        // 状态：0:未完成 1:可领取 2:已领取
            status ?:number
        // 是否奖励已满
            isFullReward ?:boolean

    }

// [请求]金猪存钱罐活动领奖
    interface GetPiggyBankRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]金猪存钱罐活动领奖
    interface GetPiggyBankRewardRsp extends RspObj {
        // 活动ID
            activityId ?:number
        // 是否充值后的主动推送奖励
            isPushAfterChongzhi ?:boolean
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// [请求]金猪存钱罐充值奖励查看通知
    interface ReadPiggyBankRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]金猪存钱罐充值奖励查看通知
    interface ReadPiggyBankRewardRsp extends RspObj {
        // 活动ID
            activityId ?:number

    }

// [请求]七日竞赛活动领取在线时长礼包
    interface SevenMatchGetOnlineRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]七日竞赛活动领取在线时长礼包
    interface SevenMatchGetOnlineRewardRsp extends RspObj {
        // 在线时长免费奖励内容
            onlineReward ?:JiangliInfoUnit
        // 下次领取在线时长奖励的时间戳，单位：毫秒
            nextTimeForOnlineReward ?:number

    }

// 客户端心跳请求
    interface UpdateClientStateReq extends ReqObj {
    }

// 客户端心跳请求
    interface UpdateClientStateRsp extends RspObj {
        // 在线确认,为true时须前端立即发送UpdateClientStateReq
            isOnlineConfirm ?:boolean

    }

// 加载服务器列表请求
    interface LoadServerListReq extends ReqObj {
    }

// 加载服务器列表反馈
    interface LoadServerListRsp extends RspObj {
        // 服务器列表
            serverList ?:Array<ServerInfoUnit>

    }

// 加载服务器列表请求(gm专用)
    interface LoadServerList4GMReq extends ReqObj {
        // Game Platform ID
            platformId ?:number
    }

// 加载服务器列表反馈(gm专用)
    interface LoadServerList4GMRsp extends RspObj {
        // 服务器列表
            serverList ?:Array<ServerInfoUnit>
        // Game Platform ID
            platformId ?:number

    }

// 检测并使用礼品码
    interface CheckGiftCodeAndUseReq extends ReqObj {
        // 礼品码
            giftCode ?:string
        // playerID
            playerId ?:number
        // 该类型礼品码可使用的次数限制
            canUseCount ?:number
        // 玩家所属渠道
            playerChannel ?:string
    }

// 检测并使用礼品码
    interface CheckGiftCodeAndUseRsp extends RspObj {
        // 状态类型：1,使用成功；2，礼品码不存在；3，礼品码已被使用；4, 使用过同类型的礼品码
            type ?:number
        // 礼品码奖励数据
            rewardInfoList ?:Array<GiftCodeRewardInfoUnit>

    }

// 中心服ping值检测
    interface CheckPingTimeReq extends ReqObj {
        // 时间戳
            time ?:number
    }

// 中心服ping值检测
    interface CheckPingTimeRsp extends RspObj {
        // 时间戳
            time ?:number

    }

// 【请求】查看排位赛王者大师的排行
    interface GetRankingPaiHangInfoCrossServerReq extends ReqObj {
        // 
            playerId ?:number
        // 排位赛段级ID,取自PaihangPP表
            paihangPPDefId ?:number
        // 当前所在等级段(等级段最小等级)
            levelPart ?:number
        // 排位赛类型 0等级  1转生
            matchType ?:number
    }

// 【反馈】查看排位赛王者大师的排行
    interface GetRankingPaiHangInfoCrossServerRsp extends RspObj {
        // 战区玩家数据
            paiHangList ?:Array<RankingPaiHangPlayerUnit>
        // 局数
            sizeFight ?:number
        // 皮肤形象
            piFuDefIdList ?:Array<number>
        // 称号
            chenghaoId ?:number
        // 玩家坐骑ID
            playerZuoqi ?:number
        // 玩家翅膀ID
            playerChibang ?:number
        // 皮神形象
            luoMuTuoDisplay ?:number
        // 
            playerId ?:number

    }

// [请求]忍蛙面板
    interface RenWaPanelReq extends ReqObj {
    }

// [返回]忍蛙面板
    interface RenWaPanelRsp extends RspObj {
        // 忍蛙的名字
            renWaName ?:string
        // 忍蛙的id
            renWaId ?:number
        // 忍蛙属性列表左
            leftShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 忍蛙属性列表右
            rightShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 装备星级
            equipLevel ?:number
        // 升星消耗道具的id
            equipCostItemId ?:number
        // 升星消耗道具的数量 一次的数量
            equipCostItemCount ?:number
        // 是否满级
            isFull ?:boolean
        // 忍蛙的技能Id
            renWaJiNengId ?:number

    }

// [请求]忍蛙升级面板
    interface RenWaUpPanelReq extends ReqObj {
        // 1 升级 2 培养 3 进化 
            type ?:number
    }

// [返回]忍蛙升级面板
    interface RenWaUpPanelRsp extends RspObj {
        // 忍蛙的id
            renWaId ?:number
        // 当前属性列表
            currShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
            yuLanShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 消耗道具的id
            costItemId ?:number
        // 消耗道具的数量 一次的数量
            costItemCount ?:number
        // 当前等级
            currLevel ?:number
        // 是不是升到满级了 true是 false不是
            isFull ?:boolean
        // 当前的名字
            cueName ?:string
        // 下一阶段忍蛙的id
            nextRenWaId ?:number
        // 下一阶段的名字
            nextName ?:string
        // 当前蛙技能
            curSkill0 ?:number
        // 下一级蛙技能
            nextSkill0 ?:number

    }

// [请求]忍蛙升级
    interface GetRenWaUpReq extends ReqObj {
        // 1 升级 2 培养 3 进化 4 一键升星
            type ?:number
    }

// [返回]忍蛙升级
    interface GetRenWaUpRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]忍村建筑升级面板
    interface RenCunJianZhuUpPanelReq extends ReqObj {
        // 1 升级 2 激活 
            type ?:number
    }

// [返回]忍村建筑升级面板
    interface RenCunJianZhuUpPanelRsp extends RspObj {
        // 当前建筑id
            currJianZhuid ?:number
        // 当前属性列表
            currShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
            yuLanShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 消耗道具的id
            costItemId ?:number
        // 消耗道具的数量 一次的数量
            costItemCount ?:number
        // 当前等级
            currLevel ?:number
        // 是不是升到满级了 true是 false不是
            isFull ?:boolean
        // 忍村建筑列表
            jianzhuList ?:Array<RenCunJianZhuUnit>

    }

// [请求]忍村建筑升级
    interface GetRenCunJianZhuUpReq extends ReqObj {
        // 1 升级 2 激活
            type ?:number
        // 1 升级 2 激活
            jianzhuId ?:number
    }

// [返回]忍村建筑升级
    interface GetRenCunJianZhuUpRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]忍村建筑技能面板
    interface RenCunJianZhuSkillPanelReq extends ReqObj {
    }

// [返回]忍村建筑技能面板
    interface RenCunJianZhuSkillPanelRsp extends RspObj {
        // 建筑技能面板
            skillList ?:Array<RenCunJianZhuSkillUnit>

    }

// [请求]建筑技能升级
    interface GetJianZhuSkillUpReq extends ReqObj {
        // 建筑技能id
            skillId ?:number
    }

// [返回]建筑技能升级
    interface GetJianZhuSkillUpRsp extends RspObj {
        // 升级是否成功
            isSuccess ?:boolean

    }

// [请求]战斗准备界面
    interface FightReadyPanelReq extends ReqObj {
    }

// [返回]战斗准备界面
    interface FightReadyPanelRsp extends RspObj {
        // 是否已经通关
            isTongGuan ?:boolean
        // 守卫到第几波
            curNum ?:number
        // 宝箱id和获取时间
            boxes ?:Array<RenCunBaoXiangUnit>
        // 扫荡花费的金券数
            saoDangPrice ?:number
        // 第几波之后才可以开始扫荡
            saoDangLimit ?:number
        // 今日剩余扫荡次数
            leftSaoDangNum ?:number
        // 今日剩余挑战次数
            leftFightNum ?:number
        // 今日剩余免费挑战次数
            leftFreeFightNum ?:number
        // 购买挑战次数花费金券
            buyPrice ?:number
        // 今日剩余开宝箱次数
            leftGetBoxNum ?:number

    }

// [请求]保卫战斗开始
    interface GetBaoWeiFightStartReq extends ReqObj {
    }

// [返回]保卫战斗开始
    interface GetBaoWeiFightStartRsp extends RspObj {
        // 第几波
            curNum ?:number
        // 上路怪物
            OneBoss ?:Array<RenCunBossUnit>
        // 下路怪物
            TwoBoss ?:Array<RenCunBossUnit>
        // 掉落的宝箱id
            baoXiangId ?:number
        // 建筑hp
            jianzhuhp ?:number
        // 忍蛙属性列表
            renWaShuXingList ?:Array<ShuXingInfoUnit>
        // 建筑技能列表
            jianZhuSkillList ?:Array<RenCunJianZhuSkillUnit>
        // 技能间隔
            jiNengJianGe ?:number
        // 是否添加了被动效果
            isBeiDong ?:boolean
        // 攻击速度
            atk_speed ?:number
        // 攻击范围
            atk_scope ?:number
        // 战斗uid
            fightuid ?:number
        // 忍蛙id
            renwaId ?:number
        // 建筑形象
            jianzhuImage ?:string
        // 被动技能列表
            beiDongSkillList ?:Array<RenCunJianZhuSkillUnit>

    }

// [请求]保卫忍村每日奖励面板
    interface MeiRiJiangLiPanelReq extends ReqObj {
    }

// [返回]保卫忍村每日奖励面板
    interface MeiRiJiangLiPanelRsp extends RspObj {
        // 奖励列表
            rewards ?:Array<JiangliInfoUnit>
        // 今日是否已经领取
            isGet ?:boolean

    }

// [请求]领取每日奖励
    interface GetMeiRiJiangLiReq extends ReqObj {
    }

// [返回]领取每日奖励
    interface GetMeiRiJiangLiRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// [请求]开宝箱
    interface GetBaoXiangRewardReq extends ReqObj {
        // 宝箱id
            baoxiangId ?:number
        // 是否花费金券开箱子
            isJinQuan ?:boolean
    }

// [返回]开宝箱
    interface GetBaoXiangRewardRsp extends RspObj {
        // 是否开启成功
            jiangliList ?:Array<JiangliInfoUnit>
        // 是否开启成功
            isSuccess ?:boolean

    }

// [请求]扫荡上一层
    interface RenCunSaoDangReq extends ReqObj {
    }

// [返回]扫荡上一层
    interface RenCunSaoDangRsp extends RspObj {
        // 掉落的箱子信息
            xiangZiUnit ?:RenCunBaoXiangUnit

    }

// [请求]忍村战斗校验
    interface RenCunFightCheckReq extends ReqObj {
        // 是否胜利
            fightresult ?:boolean
        // 战斗技能时间间隔列表
            skillJianGe ?:Array<number>
        // 建筑技能列表
            jianZhuSkillList ?:Array<ShuXingInfoUnit>
        // 校验boss列表
            BossList ?:Array<BossCheckUnit>
        // 忍蛙攻击
            renwaatk ?:number
        // 战斗uid
            fightuid ?:number
    }

// [返回]忍村战斗校验
    interface RenCunFightCheckRsp extends RspObj {
        // 是否作弊
            isZuoBi ?:boolean

    }

// [请求]忍村红点
    interface RenCunRedPointReq extends ReqObj {
    }

// [返回]忍村红点
    interface RenCunRedPointRsp extends RspObj {
        // 能否升级
            redPointList ?:Array<BoolRedPointUnit>

    }

// [请求]忍蛙的属性详情
    interface RenWaAllShuXingListReq extends ReqObj {
    }

// [返回]忍蛙的属性详情
    interface RenWaAllShuXingListRsp extends RspObj {
        // 忍蛙提供的属性列表
            shuXingList ?:Array<ShuXingInfoUnit>
        // 忍蛙的属性
            renWaShuXingList ?:Array<ShuXingInfoUnit>

    }

// [请求]忍蛙新面板
    interface RenWaPanelNewReq extends ReqObj {
    }

// [返回]忍蛙的属性详情
    interface RenWaPanelNewRsp extends RspObj {
        // 所有忍蛙信息
            renWaDataList ?:Array<RenWaDataUnit>

    }

// [请求]忍蛙升级
    interface GetRenWaLevelUpReq extends ReqObj {
        // 忍蛙类型
            renWaType ?:number
    }

// [返回]忍蛙升级
    interface GetRenWaLevelUpRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]所有忍蛙type
    interface GetAllRenWaTypeReq extends ReqObj {
    }

// [返回]所有忍蛙type
    interface GetAllRenWaTypeRsp extends RspObj {
        // 所有type
            typeList ?:Array<RenWaTypeDataUnit>

    }

// 【反馈】英雄状态改变的英雄列表
    interface ChangedHeroListRsp extends RspObj {
        // 状态改变的英雄列表
            heroList ?:Array<HeroInfoUnit>
        // 1 - 新增， 2 - 更新，3 - 删除
            changeType ?:number
        // false 不飘  true 飘
            piaozi ?:boolean
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_11 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_12 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_13 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_14 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_15 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_16 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_17 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_18 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_19 ?:number

    }

// 【反馈】临时背包状态改变的列表
    interface ChangedTempBeibaoListRsp extends RspObj {
        // 状态改变的临时背包列表
            tempBeibaoList ?:Array<TempBeibaoInfoUnit>
        // 1 - 新增， 2 - 更新，3 - 删除
            changeType ?:number

    }

// 【反馈】道具状态改变的列表
    interface ChangedDaojuListRsp extends RspObj {
        // 状态改变的道具列表
            daojuList ?:Array<DaojuInfoUnit>
        // 1 - 新增， 2 - 更新，3 - 删除
            changeType ?:number

    }

// 玩家用户信息变更推送
    interface ChangedUserRsp extends RspObj {
        // 玩家昵称
            playerName ?:string
        // 玩家性别
            gender ?:number
        // 金币
            money ?:number
        // 钻石
            currency ?:number
        // 等级
            level ?:number
        // vip等级
            viplevel ?:number
        // 经验值
            exp ?:number
        // 体力
            physicalPower ?:number
        // 下一点体力恢复剩余时间
            physicalPowerTime ?:number
        // 30钻石首抽
            wj_30firstGet ?:number
        // 30钻石上次免费抽时间
            wj_30lastGetTime ?:number
        // 300钻石首抽
            wj_300firstGet ?:number
        // 300钻石上次免费抽时间
            wj_300lastGetTime ?:number
        // 英雄招募必出5星积分
            wj_get5Acc ?:number
        // 装备容量上限
            zhuangbei_countMax ?:number
        // 道具容量上限(玩家背包)，最大250个
            daoju_countMax ?:number
        // 武将容量上限(玩家背包)，初始50，最大100个
            wj_countMax ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_11 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_12 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_13 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_14 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_15 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_16 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_17 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_18 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_19 ?:number
        // 玩家当前使用的战阵，玩家初始没有战阵
            zhanzhenNow ?:number
        // 服务器时间
            serverTime ?:number
        // 免费挖宝石次数
            freeDigBaoshiCount ?:number
        // 免费挖高级宝石次数
            freeDigHighBaoshiCount ?:number
        // 是否付费用户：0、未付费；1、已付费；
            isPay ?:number
        // 训练场，是否开启双倍经验：1、开启状态；2、关闭状态
            trainingDbExp ?:number
        // 头像ID（武将ID）
            touxiangId ?:number
        // 财神殿上次挂机时间
            guajiTime ?:number
        // 财神殿挂机上次过关时间:当改时间为0时则挂机停止
            guajiTongguanTime ?:number
        // 当天已购买银两次数
            buyYinliangCount ?:number
        // 当天已购买体力次数
            buyTiliCount ?:number
        // 世界BOSS积分
            score ?:number
        // 装备币
            zhuangBeiBi ?:number
        // 精灵币
            jingLingBi ?:number
        // false 不是  true是
            isYueKa ?:boolean
        // false 不是  true是
            isZhongShen ?:boolean
        // 是否年卡用户
            isYearCard ?:boolean
        // 建号选择宠物的defId
            xuanZeChongWuDefId ?:number
        // 比武排行
            paihangBW ?:number
        // 代币
            coupon ?:number
        // 称号
            chenghaoId ?:number
        // 新版玩家个人形象数据
            playerPhoto ?:PlayerPhotoUnit

    }

// 【反馈】战阵碎片状态改变的列表
    interface ChangedZhanzhenSuipianListRsp extends RspObj {
        // 状态改变的战阵碎片列表
            zhanzhenSuipianList ?:Array<ZhanzhenSuipianInfoUnit>
        // 1 - 新增， 2 - 更新，3 - 删除
            changeType ?:number

    }

// 【反馈】战阵状态改变的列表
    interface ChangedZhanzhenListRsp extends RspObj {
        // 状态改变的战阵列表
            zhanzhenList ?:Array<ZhanzhenInfoUnit>
        // 1 - 新增， 2 - 更新，3 - 删除
            changeType ?:number
        // false 不飘  true 飘
            piaozi ?:boolean

    }

// 【反馈】装备状态改变的列表
    interface ChangedZhuangbeiListRsp extends RspObj {
        // 玩家获得装备列表
            zhuangbeiList ?:Array<ZhuangbeiInfoUnit>
        // 1 - 新增， 2 - 更新，3 - 删除
            changeType ?:number
        // false 不飘  true 飘
            piaozi ?:boolean

    }

// 【反馈】武将转化为残魂
    interface ChangedHeroToSoulRsp extends RspObj {
        // 武将Id
            wujiangId ?:number
        // 残魂Id
            canhunId ?:number
        // 武将转换为残魂的数量
            count ?:number

    }

// 【反馈】各频道公告推送
    interface PushBroadcastRsp extends RspObj {
        // 处理结果
            content ?:string
        // 公告类型：1 - 英雄获取， 2 - 装备获取，3 - 英雄升级，4 - 装备升级…… 12- 砸金蛋 13 -魔法骰子 14 - 刮刮乐 15-战场boss重生
            type ?:number
        // 
            defType ?:number
        // 
            defId ?:number
        // 公告优先级, 从高到低排序, 优先级为1的要比0级的优先显示
            priority ?:number
        // 0世界 1本服 2公会 3公告 4组队副本 5宅急送 6 系统 7公会组队副本
            channel ?:number
        // 跳转Id
            jumpId ?:number
        // vip条件
            vip ?:number
        // 玩家等级条件
            level ?:number

    }

// 【反馈】比武场比武战斗CD相关信息
    interface PushBiwuFightCDInfoRsp extends RspObj {
        // 刷新时间
            refreshTime ?:number
        // CD时长,单位：秒
            cdTime ?:number
        // 剩余比武次数
            biwuCount ?:number

    }

// 【反馈】聊天信息
    interface PushChatMessageRsp extends RspObj {
        // 讲话人ID
            playerId ?:number
        // 聊天信息的DBId
            chatDBId ?:number
        // 聊天内容
            content ?:string
        // 聊天内容中的物品信息（1'道具', 2'装备', 3'武将', 4'阵法', 5'耳朵'）
            messageInfoList ?:Array<ChatMessageInfoUnit>
        // 
            vipLevel ?:number
        // 
            playerName ?:string
        // 排位赛段位的defId
            paihangPPDefId ?:number
        // 等级段
            levelPart ?:number
        // 服务器id
            serverId ?:number
        // 头像id 和打架时候的一样
            face ?:number
        // 徽章图
            huiZhangTu ?:string
        // 0世界 1本服 2公会 3公告 4组队副本 5宅急送 6 系统 7公会组队副本
            channel ?:number
        // 公会id
            gonghuiId ?:number
        // 蓝钻等级
            lanZuanLevel ?:number
        // 是否是年费蓝钻
            isLanZuanNianFei ?:boolean
        // 是否是豪华蓝钻
            isLanZuanHaoHua ?:boolean
        // 先使用服务器编号,来自玩家原始创角时所属服务器编号
            showServerNum ?:number
        // 玩家等級
            level ?:number
        // 角色信息
            playerInfo ?:PaihangInfoUnit
        // 抢夺记录Id
            qiangDuoRecordId ?:number
        // 失效时间戳
            shiXiaoTime ?:number
        // 0 援助失败 1记录过期 2援助成功
            status ?:number
        // 是否可以援助
            canYuanZhu ?:boolean
        // 私聊的对方Id，如果其他channel要用，请同时修改注释
            targetId ?:number
        // 新版玩家个人形象数据
            playerPhoto ?:PlayerPhotoUnit
        // 聊天展示特权表Id列表
            liaotianZhanshiDefIdList ?:Array<number>
        // 聊天框Id
            liaotiankuangDefId ?:number

    }

// 【反馈】财神殿挂机，推送已打过的关卡
    interface PushCsdGuajiMessageRsp extends RspObj {
        // 打过的活动关ID
            huodongGuanId ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【反馈】有任务完成可以领取奖励了
    interface PushMissionDoGetRsp extends RspObj {
        // 任务dbId
            dbId ?:number
        // 手势指引0不指引   1指引
            shifoutishi ?:number
        // 任务defId
            renwuDefId ?:number

    }

// 【反馈】IP地址
    interface PushIPRsp extends RspObj {
        // 给客户端推送客户端自身的外网IP
            ipAddress ?:string

    }

// 【反馈】匹配战获取奖励或开始结束信息的推送
    interface PushPipeiZhanInfoRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 被奖励人的playerID
            playerId ?:number
        // 被奖励人的Name
            playerName ?:string
        // 对手的Name
            enemyName ?:string
        // 显示匹配战开始及结束提示：1、13点开始；2、14点结束；3、19点开始；4：20点结束
            showInfo ?:number

    }

// 【反馈】玩家开启的功能列表
    interface PushPlayerFunctionsRsp extends RspObj {
        // 已开通的功能ID列表
            gongnengIDs ?:Array<number>

    }

// 【反馈】玩家订单支付成功告知
    interface PushOrderPayOKRsp extends RspObj {
        // 静态表中物品ID
            defId ?:number
        // 是否来自威名充值
            isWeimingRecharge ?:boolean

    }

// 【反馈】通知玩家已在其它地方登陆
    interface PushPlayerMultiLoginRsp extends RspObj {

    }

// 【反馈】9899特价类充值活动
    interface PushYiyuantejiaInfoRsp extends RspObj {
        // 一元礼包
            yiyuanList ?:Array<ShuXingInfoUnit>
        // 连续购买礼包
            lianXuList ?:Array<ShuXingInfoUnit>
        // 98特价ID / 100 + 1,如果均没有购买,则为1,如果已经领取奖励,则传递更高等级的98特价ID
            yiyuantejia98Id ?:number
        // 对应98特价状态 1 未购买； 2 已经购买未领取；3 已经领取奖励
            yiyuantejia98State ?:number
        // 次级98特价购买时间,如果均没有购买,则为玩家创建角色时间
            gotTime4Yiyuantejia98 ?:number
        // 49/99特价ID,如果均没有购买,则为1,如果已经领取奖励,则传递更高等级的98特价ID
            yiyuantejia99Id ?:number
        // 对应99特价状态 1 未购买； 2 已经购买未领取；3 已经领取奖励
            yiyuantejia99State ?:number
        // 99特价活动活动开始时间(单位:秒)
            timeStartForYiyuantejia99 ?:number
        // 99特价活动活动结束时间(单位:秒)
            timeFinishForYiyuantejia99State ?:number
        // 96特价ID 增长量是70
            yiyuantejia96Id ?:number
        // 对应96特价状态 1 未购买； 2 已经购买未领取；3 已经领取奖励
            yiyuantejia96State ?:number
        // 这一档99礼包的限制等级
            yiyuantejia99Level ?:number
        // 一元红点字段
            yiyuanHong ?:number
        // 0元礼包状态字段
            lingyuanStatus ?:number
        // 1 未购买；2 已经购买未领取；3 已经领取奖励
            zuiQiangLiBaoStatus ?:number
        // 激活的Z力量
            zLiLiangDefId ?:Array<number>
        // 某个模板下的一元特价状态数据
            mobanStatusList ?:Array<YiyuantejiaMobanStatusInfoUnit>

    }

// [请求]在主城循环发的红点
    interface PushLoopRedPointReq extends ReqObj {
        // 请求次数，从1开始，=1表示登录后的第一次请求
            reqNum ?:number
    }

// [反馈]在主城循环发的红点    false 不显示红点    true  显示红点
    interface PushLoopRedPointRsp extends RspObj {
        // bool红点list
            boolPointList ?:Array<BoolRedPointUnit>
        // byte红点list
            bytePointList ?:Array<ByteRedPointUnit>
        // byte属性值list，1）首冲四倍奖励是否已领取：1不显示红点；2显示红点；3该奖励已经存在且已领取。2）是不是在组队副本队伍中：0不在；1在。
            byteValueUnitList ?:Array<ByteRedPointUnit>
        // 当前小关ID
            curXiaoguanID ?:number
        // 请求次数
            reqNum ?:number
        // 第一次请求的时候返回的红点列表
            firstBoolPointList ?:Array<BoolRedPointUnit>

    }

// [返回]推送神器改变
    interface ChangedShenqiListBackRsp extends RspObj {
        // 属性
            shenQiList ?:Array<ShenQiInfoUnit>
        // 1 - 新增， 2 - 更新，3 - 删除
            changeType ?:number
        // false 不飘  true 飘
            piaozi ?:boolean

    }

// 【反馈】推送恋爱类型的任务  登录的时候 推送  完成恋爱类型的任务 推送
    interface PushLianAiMissionListRsp extends RspObj {
        // 任务列表
            missionList ?:Array<MissionInfoUnit>

    }

// 【反馈】跳动提示
    interface PushTiaoDongTiShiRsp extends RspObj {
        // 跳动的武将信息
            tiaoDongHeroInfoList ?:Array<TiaoDongHeroInfoUnit>
        // 探险跳动  true 跳  false 不动
            tanXianTiaoDong ?:boolean

    }

// 【反馈】玩吧交互
    interface PushWanBaJaoHuRsp extends RspObj {
        // 备注表id
            jiaoHuId ?:number
        // 好友的ipenId
            friendOpenId ?:string

    }

// 种族值改变突破信息
    interface ChangedZhongZuZhiTuPoListRsp extends RspObj {
        // 种族值突破信息
            zhongZuZhiTuPoList ?:Array<ZhongZuZhiTuPoInfoUnit>
        // 1 - 新增， 2 - 更新，3 - 清除
            changeType ?:number
        // false 不飘  true 飘
            piaozi ?:boolean

    }

// 【反馈】宠物改变的英雄列表
    interface ChangedFollowPetListRsp extends RspObj {
        // 状态改变的跟随宠物列表
            followPetList ?:Array<FollowPetUnit>
        // 1 - 新增， 2 - 更新，3 - 删除
            changeType ?:number
        // false 不飘  true 飘
            piaozi ?:boolean

    }

// 推送今天0点和明天0点的时间点
    interface PushTimeRsp extends RspObj {
        // 今天0点的时间
            todayZeroTime ?:number
        // 明天0点的时间
            nextTodayZeroTime ?:number
        // 组队副本不能打的时间点 如果当前时间比它大 不能打  这个时间在0点会推送
            zuDuiFuBenFightTime ?:number

    }

// 推送发生变化的数据
    interface PushChangedCurrencyRsp extends RspObj {
        // 名字
            name ?:string
        // 数字积分类消息
            numberValueList ?:Array<NumberValueUnit>
        // 100 level,101 viplevel,102 exp,103 wj_30firstGet,104 wj_30lastGetTime,105 wj_300firstGet,106 wj_300lastGetTime,107 wj_get5Acc,108 isPay,109 touxiangId,110 buyYinliangCount,111 xuanZeChongWuDefId,112 paihangBW,113 chenghaoId,114 serverTime
            otherBasicValueList ?:Array<NumberValueUnit>
        // 变化的阵容信息
            teamList ?:Array<TeamInfoUnit>
        // Entity类数据
            entityValueList ?:Array<EntityValueUnit>
        // 新版玩家个人形象数据
            playerPhoto ?:PlayerPhotoUnit
        // Entity类数据,不在奖励类型中的
            entityValueNoRewardTypeList ?:Array<EntityValueNoRewardTypeUnit>
        // 文本内容积分类消息
            stringWordsList ?:Array<StringWordsUnit>

    }

// 登录时推送玩家基本信息
    interface PushPlayerBasicInfoAtLoginReq extends ReqObj {
    }

// 登录时推送玩家基本信息
    interface PushPlayerBasicInfoAtLoginRsp extends RspObj {
        // 玩家昵称
            playerName ?:string
        // 玩家性别
            gender ?:number
        // 等级
            level ?:number
        // vip等级
            viplevel ?:number
        // 30钻石首抽
            wj_30firstGet ?:number
        // 30钻石上次免费抽时间
            wj_30lastGetTime ?:number
        // 300钻石首抽
            wj_300firstGet ?:number
        // 300钻石上次免费抽时间
            wj_300lastGetTime ?:number
        // 服务器时间
            serverTime ?:number
        // 是否付费用户：0、未付费；1、已付费；
            isPay ?:number
        // false 不是  true是
            isYueKa ?:boolean
        // false 不是  true是
            isZhongShen ?:boolean
        // 当天已购买银两次数
            buyYinliangCount ?:number
        // 建号选择宠物的defId
            xuanZeChongWuDefId ?:number
        // 比武排行
            paihangBW ?:number
        // 道具列表
            itemList ?:Array<DaojuInfoUnit>
        // 装备列表
            zhuangBeiList ?:Array<ZhuangbeiInfoUnit>
        // 精灵列表
            heroList ?:Array<HeroInfoUnit>
        // 编队信息
            teamList ?:Array<TeamInfoUnit>
        // 积分List
            numberList ?:Array<NumberValueUnit>
        // 坐骑 翅膀
            featureList ?:Array<FeatureInfoUnit>
        // 主线任务信息
            missionInfo ?:MissionInfoUnit
        // 道馆通关
            daoGuanTongGuanNum ?:number
        // 守护兽特权礼包结束时间
            shenChongTeQuanEndTime ?:number
        // 守护兽特权是否开启
            shenChongTeQuanOpen ?:boolean
        // 1没有礼包 2不可领取 3有礼包未领取 4领取了
            shenChongTeQuanStatus ?:number
        // 最强礼包结束时间
            zuiQiangLiBaoEndTime ?:number
        // 1没有礼包 2不可领取 3有礼包未领取 4领取了 5过期显示
            zuiQiangLiBaoStatus ?:number
        // 种族值突破信息
            zhongZuZhiTuPoList ?:Array<ZhongZuZhiTuPoInfoUnit>
        // 职业神器列表
            zhiYeShenQiList ?:Array<ZhiYeShenQiUnit>
        // 铁拳 排位赛等基本信息
            otherList ?:Array<NumberValueUnit>
        // 礼包信息列表
            giftPackageList ?:Array<GiftPackageInfoUnit>
        // 转生等级
            zhuanShengLevel ?:number
        // 公会徽章 边框,底图,标志
            gongHuiHuiZhang ?:string
        // 遭遇战剩余挑战次数
            zaoYuZhanLeftCount ?:number
        // 贵族特权开启列表
            nobleAutoList ?:Array<number>
        // 专属神器信息
            zhuanShuShenQiList ?:Array<ZhuanShuShenQiPushfoUnit>
        // 跟随宠信息
            followPetShow ?:NewFollowPetShowUnit
        // 公会道馆剩余次数
            gonghuiDaoguanCiShu ?:number
        // 消耗道具数组
            costItemList ?:Array<CostItemUnit>
        // 搁置待完成的剧情线
            hadShowStoryLineList ?:Array<StoryLineUnit>
        // 新的尚未展示的剧情线
            newEventStoryLineList ?:Array<StoryLineUnit>
        // 新版玩家个人形象数据
            playerPhoto ?:PlayerPhotoUnit
        // 拥有的全部技能书数据
            skillBookList ?:Array<SkillBookUnit>
        // 新守护兽升星消耗数据列表
            guardPetLevelUpCostInfoList ?:Array<NewGuardPetLevelUpCostInfoUnit>
        // 新手推荐阵容已上阵Id列表，图鉴已上阵Id列表
            activeAniSignIDList ?:Array<number>
        // 小镇未解锁房屋列表
            townUnOpenRoomShortInfoList ?:Array<TownUnOpenRoomShortInfoUnit>
        // 登录后弹出的Project相关页面：竞技场、等级礼包等
            popUpProjectIdList ?:Array<number>
        // 神技背包物品列表
            magicalSkillItemList ?:Array<MagicalSkillItemUnit>
        // Rogoulike地下城数据
            rogoulikeInfo ?:RogoulikeInfoUnit

    }

// 【反馈】玩家优惠券订单支付成功告知
    interface PushYouHuiQuanPayOKRsp extends RspObj {
        // 充值金额表ID
            chongZhiId ?:number
        // 优惠券表Id
            youHuiQuanDefID ?:number

    }

// [请求]获取假公告数据
    interface GetFakeGongGaoListReq extends ReqObj {
    }

// [反馈]获取假公告数据
    interface GetFakeGongGaoListRsp extends RspObj {
        // 随机名称
            ranNames ?:Array<string>
        // 公告信息
            gongGaoInfoList ?:Array<GongGaoInfoUnit>

    }

// [反馈]实时广播房间玩家信息
    interface BroadBossRoomPlayerInfoRsp extends RspObj {
        // 归属权对应的playerId
            bossRewardPlayerId ?:number
        // 归属权对应的boss
            bossId ?:number
        // 房间玩家信息广播
            playerInfoList ?:Array<BossPlayerSimpleInfoUnit>

    }

// [请求]目标玩家是否跟我同服
    interface GetPlayerIsTheSameServerReq extends ReqObj {
        // 对方PlayerId
            playerId ?:number
    }

// [反馈]目标玩家是否跟我同服
    interface GetPlayerIsTheSameServerRsp extends RspObj {
        // 是否同服
            isSame ?:boolean

    }

// [反馈]第一次获得精灵
    interface PushFirstGetHeroShowRsp extends RspObj {
        // 精灵Id
            heroDbId ?:number
        // 精灵defId
            heroDefId ?:number
        // 星级
            star ?:number
        // playerId
            playerId ?:number
        // 创建时间
            createTime ?:number
        // 延迟时间
            delaySecond ?:number

    }

// [推送]复仇页面的对手上线了
    interface PushYourEnemyLoginRsp extends RspObj {
        // 玩家ID
            enemyId ?:number
        // 玩家创建角色时,所属服务器编号
            enemyServerNum ?:number
        // 玩家昵称
            enemyName ?:string
        // 玩家等级
            enemyLevel ?:number
        // 玩家VIP等级
            enemyVipLevel ?:number
        // 玩家贵族等级
            enemyNobleLevel ?:number

    }

// [推送]自动激活的Project通知
    interface PushAutoActiveProjectInfoRsp extends RspObj {
        // Project表ID
            projectId ?:number
        // 对应的策划表ID
            defId ?:number

    }

// [推送]资源礼包的通知
    interface PushResourceGiftActivityRsp extends RspObj {
        // 活动类型
            activityType ?:number
        // 名称
            title ?:string
        // 内容
            desc ?:string
        // 奖励内容
            jiangliList ?:Array<JiangliInfoUnit>
        // 通知结束时间，单位：毫秒
            endTime ?:number

    }

// [推送]特殊累计充值展示
    interface PushCumulativeRechargeDisplayRsp extends RspObj {
        // 展示信息
            cumulativeRechargeDisplayInfo ?:CumulativeRechargeDisplayInfoUnit

    }

// [请求]主角装备面板
    interface ZhuJueZhuangBeiPanelReq extends ReqObj {
    }

// [返回]主角装备面板
    interface ZhuJueZhuangBeiPanelRsp extends RspObj {
        // 主角装备列表
            positionList ?:Array<ZhuJueEquipPositionUnit>

    }

// [请求]主角装备强化
    interface GetZhuangBeiQiangHuaReq extends ReqObj {
        // 操作的位置
            position ?:number
        // 1 普通 2 一件强化
            eType ?:number
    }

// [返回]主角装备强化
    interface GetZhuangBeiQiangHuaRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]灵魂石升级
    interface GetLingHunShiLvUpReq extends ReqObj {
        // 操作的位置
            position ?:number
    }

// [返回]灵魂石升级
    interface GetLingHunShiLvUpRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]操作装备或者灵魂石
    interface ChangeEquipOrItemReq extends ReqObj {
        // 操作的位置
            position ?:number
        // 1 装备 2 灵魂石
            selectType ?:number
        // 1 穿上 2脱下 3更换
            operateType ?:number
        // 老的装备或者灵魂石ID
            oldId ?:number
        // 新的装备或者灵魂石ID
            newId ?:number
    }

// [返回]操作装备或者灵魂石
    interface ChangeEquipOrItemRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]链接精灵
    interface GetLinkHeroReq extends ReqObj {
        // 操作的位置
            position ?:number
        // 1 链接上 2 接触链接 3 更换链接 
            oType ?:number
        // 原来链接的精灵dbId
            oldHeroId ?:number
        // 新链接的精灵dbId
            newHeroId ?:number
    }

// [返回]链接精灵
    interface GetLinkHeroRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]主角装备背包
    interface ZhuJueEquipBagReq extends ReqObj {
        // 位置
            position ?:number
    }

// [返回]主角装备背包
    interface ZhuJueEquipBagRsp extends RspObj {
        // 主角装备列表
            equipList ?:Array<ZhuJueEquipUnit>

    }

// [请求]装备详情面板
    interface ZhuJueEquipDetailPanelReq extends ReqObj {
    }

// [返回]装备详情面板
    interface ZhuJueEquipDetailPanelRsp extends RspObj {
        // 详情列表
            detailList ?:Array<ZhuJueEquipDetailUnit>

    }

// [请求]主角装备强化面板
    interface ZhuJueEquipQiangHuaPanelReq extends ReqObj {
    }

// [返回]主角装备强化面板
    interface ZhuJueEquipQiangHuaPanelRsp extends RspObj {
        // 强化列表
            qiangHuaList ?:Array<ZhuJueEquipQiangHuaUnit>

    }

// [请求]链接宠的信息发生改变
    interface ChangeLinkHeroInfoReq extends ReqObj {
    }

// [返回]链接宠的信息发生改变
    interface ChangeLinkHeroInfoRsp extends RspObj {
        // 精灵ID
            heroId ?:number
        // 精灵DbId
            heroDbId ?:number
        // 现在是否是链接状态
            isLianJie ?:boolean

    }

// [请求]主角装备红点信息
    interface GetZhuJueEquipRedPointReq extends ReqObj {
    }

// [返回]主角装备红点信息
    interface GetZhuJueEquipRedPointRsp extends RspObj {
        // 可以装备的位置列表
            equipPositionList ?:Array<number>
        // 可以链接球的位置列表
            itemPositionList ?:Array<number>
        // 可以链接宠的位置列表
            linkHeroPositionList ?:Array<number>
        // 装备是否可以强化
            canQiangHua ?:boolean
        // 属性是否可以重铸
            canChongZhu ?:boolean
        // 符文按钮是否亮起
            showFuwenRedPoint ?:boolean

    }

// [请求]主角装备重铸面板
    interface ZhuJueEquipChongZhuPanelReq extends ReqObj {
    }

// [返回]主角装备重铸面板
    interface ZhuJueEquipChongZhuPanelRsp extends RspObj {
        // 主角装备洗练属性列表
            equipPropList ?:Array<ZhuJueEquipPropUnit>
        // 消耗的道具
            costItem ?:number
        // 消耗的道具数量
            costCount ?:number

    }

// [请求]主角装备重铸
    interface GetZhuJueEquipChongZhuReq extends ReqObj {
        // 位置
            position ?:number
        // 属性编号
            number ?:number
    }

// [返回]主角装备重铸
    interface GetZhuJueEquipChongZhuRsp extends RspObj {
        // 重铸后的结果
            propResult ?:EquipPropUnit

    }

// [请求]主角装备重铸结果操作
    interface OperateChongZhuResultReq extends ReqObj {
        // 位置
            position ?:number
        // 属性编号
            number ?:number
        // 1 保留 2 放弃
            operationType ?:number
    }

// [返回]主角装备重铸
    interface OperateChongZhuResultRsp extends RspObj {
        // 重铸后的结果
            propResult ?:EquipPropUnit

    }

// [请求]主角装备一键重铸
    interface ZhuJueEquipOneKeyChongZhuReq extends ReqObj {
        // 位置
            position ?:number
        // 属性编号
            number ?:number
        // 是否类型不变
            isContainType ?:number
        // 达到什么百分比的时候停止 三个比例
            stopPercent ?:number
    }

// [返回]主角装备一键重铸
    interface ZhuJueEquipOneKeyChongZhuRsp extends RspObj {
        // 变化后的属性类型
            changeEquipPropList ?:EquipPropUnit
        // 是否满足洗练条件 不再发送
            isStop ?:boolean

    }

// [请求]操作一键重铸结果
    interface OperateEquipOneKeyResultReq extends ReqObj {
        // 位置
            position ?:number
        // 属性编号
            number ?:number
        // 1 保留 2 放弃
            operateType ?:number
    }

// [返回]操作一键重铸结果
    interface OperateEquipOneKeyResultRsp extends RspObj {
        // 新的属性类型
            changeEquipPropList ?:EquipPropUnit

    }

// [请求]停止一键重铸
    interface StopOneKeyChongZhuReq extends ReqObj {
        // 位置
            position ?:number
        // 属性编号
            number ?:number
    }

// [返回]停止一键重铸
    interface StopOneKeyChongZhuRsp extends RspObj {

    }

// [请求]符文面板
    interface GetFuWenPanelReq extends ReqObj {
        // 位置 1-6
            position ?:number
    }

// [返回]符文面板
    interface GetFuWenPanelRsp extends RspObj {
        // 镶嵌符文信息
            infoList ?:Array<FuWenInfoUnit>
        // 属性列表
            attrList ?:Array<ShuXingInfoUnit>
        // 是否显示商城红点
            showShopTip ?:boolean
        // 是否显示获取按钮的红点
            showHuoQuTip ?:boolean

    }

// [请求]符文背包
    interface GetFuWenPacketReq extends ReqObj {
    }

// [返回]符文背包
    interface GetFuWenPacketRsp extends RspObj {
        // 符文信息
            infoList ?:Array<FuWenInfoUnit>

    }

// [请求]分解符文预览
    interface BreakFuWenYuLanReq extends ReqObj {
        // 不为0,分解单个 为0 按下面组合字段走批量分解
            fuwenId ?:number
        // 符文等级组合
            levelList ?:Array<number>
    }

// [返回]分解符文预览
    interface BreakFuWenYuLanRsp extends RspObj {
        // 0 成功
            ret ?:number
        // 奖励
            reward ?:JiangliInfoUnit

    }

// [请求]符文商店
    interface FuWenShopReq extends ReqObj {
    }

// [返回]符文商店
    interface FuWenShopRsp extends RspObj {
        // 碎片id
            suiPianId ?:number
        // 碎片数量
            suiPianCount ?:number
        // 符文
            fuwenList ?:Array<FuWenShopInfoUnit>

    }

// [请求]符文购买
    interface FuWenBuyReq extends ReqObj {
        // 静态表id
            defId ?:number
        // 购买数量
            count ?:number
    }

// [返回]符文购买
    interface FuWenBuyRsp extends RspObj {
        // 0 成功
            ret ?:number

    }

// [请求]符文镶嵌
    interface FuWenEquipReq extends ReqObj {
        // 唯一id
            id ?:number
        // 阵容位置
            position ?:number
        // 颜色
            yanse ?:number
        // 镶嵌位置1,2,3,4,5
            subpos ?:number
        // 0 装上， 1 拆下
            type ?:number
    }

// [返回]符文镶嵌
    interface FuWenEquipRsp extends RspObj {
        // 0 成功
            ret ?:number

    }

// [请求]分解符文
    interface BreakFuWenReq extends ReqObj {
        // 不为0,分解单个 为0 按下面组合字段走批量分解
            fuwenId ?:number
        // 符文等级组合
            levelList ?:Array<number>
    }

// [返回]分解符文
    interface BreakFuWenRsp extends RspObj {
        // 0 成功
            ret ?:number

    }

// [请求]宅急送面板
    interface ZJSPanelReq extends ReqObj {
    }

// [返还]宅急送面板
    interface ZJSPanelRsp extends RspObj {
        // 宅急送id
            zjsId ?:number
        // 宅急送状态 0：创建 1：组队等待 2：配送中 3：被抢3次 4：配送完成 5：奖励发放完成
            zjsStatus ?:number
        // 宅急送星级
            zjsLevel ?:number
        // 免费次数
            freecount ?:number
        // 刷新每次的消耗
            oneCost ?:JiangliInfoUnit
        // 一键最高的消耗
            allCost ?:JiangliInfoUnit
        // 属性列表
            JiangLiList ?:Array<JiangliInfoUnit>
        // 奖励列表
            shuXingInfoList ?:Array<ShuXingInfoUnit>
        // 移动速度
            moveSpeed ?:number
        // 剩余抢夺次数
            qiangDuoCount ?:number
        // 付费抢夺次数
            costQiangDuo ?:number
        // 付费购买的数额
            gouMaiCost ?:number
        // 购买次数
            gouMaiCount ?:number
        // 剩余配送次数
            peiSongCount ?:number
        // 剩余护送次数
            huSongCount ?:number
        // 是否是队长
            isLeader ?:boolean
        // 结束时间
            endTime ?:number
        // 配送时长
            peiSongTime ?:number
        // 今日是否受到攻击了
            isUnderAttackToDay ?:boolean

    }

// [请求]刷新星级
    interface ZJSFreshReq extends ReqObj {
        // 刷新类型 0免费 1一次 2刷满
            freshType ?:number
    }

// [返还]刷新星级
    interface ZJSFreshRsp extends RspObj {
        // 宅急送星级
            zjsLevel ?:number
        // 免费次数
            freecount ?:number
        // 刷新每次的消耗
            oneCost ?:JiangliInfoUnit
        // 属性列表
            JiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]查看当前状态
    interface ZJSStatusReq extends ReqObj {
    }

// [返还]查看当前状态
    interface ZJSStatusRsp extends RspObj {
        // 宅急送id
            zjsId ?:number
        // 宅急送状态
            status ?:number

    }

// [请求]组队界面
    interface ZJSZuDuiPanelReq extends ReqObj {
        // 宅急送id
            zjsId ?:number
    }

// [返还]组队界面
    interface ZJSZuDuiPanelRsp extends RspObj {
        // 队长id
            leaderId ?:number
        // 宅急送星级
            zjslevel ?:number
        // 配送奖励列表
            psJLList ?:Array<JiangliInfoUnit>
        // 护送奖励列表
            hsJLList ?:Array<JiangliInfoUnit>
        // 玩家列表
            ZJSPlayerList ?:Array<ZJSPlayerInfoUnit>
        // 护送时间(ms)
            huSongTime ?:number

    }

// [请求]护送者进入退出队伍
    interface ZJSpersonActReq extends ReqObj {
        // 宅急送id
            zjsId ?:number
        // 1加入 0退出
            actType ?:number
        // 是否确认解散
            conFirm ?:boolean
    }

// [返还]护送者进入退出队伍
    interface ZJSpersonActRsp extends RspObj {
        // 玩家列表
            ZJSPlayerList ?:Array<ZJSPlayerInfoUnit>
        // 护送时间(ms)
            huSongTime ?:number

    }

// [请求]踢人
    interface ZJSKickReq extends ReqObj {
        // 宅急送id
            zjsId ?:number
        // 踢人id
            playerId ?:number
    }

// [返还]踢人
    interface ZJSKickRsp extends RspObj {
        // 玩家列表
            ZJSPlayerList ?:Array<ZJSPlayerInfoUnit>
        // 护送时间(ms)
            huSongTime ?:number

    }

// [请求]开始配送
    interface ZJSPeiSongStartReq extends ReqObj {
        // 是否确认
            isConfirm ?:boolean
    }

// [返还]开始配送
    interface ZJSPeiSongStartRsp extends RspObj {
        // 玩家形象
            xingXiang ?:number
        // 完成时间
            endTime ?:number
        // 奖励列表
            JiangLiList ?:Array<JiangliInfoUnit>
        // 其他宅急送玩家
            ZJSOhterList ?:Array<ZJSDataInfoUnit>
        // 护送1的头像
            oneTouXiang ?:number
        // 护送2的头像
            twoTouXiang ?:number
        // 获取奖励的实际%
            percent ?:number
        // 新版队长个人形象数据
            leaderPhoto ?:PlayerPhotoUnit
        // 新版护送1的个人形象数据
            onePhoto ?:PlayerPhotoUnit
        // 新版护送2的个人形象数据
            twoPhoto ?:PlayerPhotoUnit

    }

// [请求]坐骑界面
    interface ZJSZuoQiPanelReq extends ReqObj {
    }

// [返还]坐骑界面
    interface ZJSZuoQiPanelRsp extends RspObj {
        // 玩家形象
            xingXiang ?:number
        // 坐骑速度
            zuoqiSpeed ?:number
        // 皮肤列表
            pifulist ?:Array<number>

    }

// [请求]坐骑切换
    interface ZJSZuoQiChangeReq extends ReqObj {
        // 坐骑id
            zuoqiId ?:number
    }

// [返还]坐骑切换
    interface ZJSZuoQiChangeRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]装备界面
    interface ZJSEquipPanelReq extends ReqObj {
    }

// [返还]装备界面
    interface ZJSEquipPanelRsp extends RspObj {
        // 装备等级
            equipLevel ?:number
        // 当前属性列表
            curShuXingInfo ?:Array<ShuXingInfoUnit>
        // 当前速度
            curSpeed ?:number
        // 下一阶段属性列表
            nextShuXingInfo ?:Array<ShuXingInfoUnit>
        // 下级速度
            nextSpeed ?:number
        // 消耗道具的id
            costItemId ?:number
        // 消耗道具的数量   一次的数量
            costItemCount ?:number

    }

// [请求]装备强化
    interface ZJSEquipUpReq extends ReqObj {
    }

// [返还]装备强化
    interface ZJSEquipUpRsp extends RspObj {
        // 装备等级
            equipLevel ?:number
        // 当前属性列表
            curShuXingInfo ?:Array<ShuXingInfoUnit>
        // 当前速度
            curSpeed ?:number
        // 下一阶段属性列表
            nextShuXingInfo ?:Array<ShuXingInfoUnit>
        // 下级速度
            nextSpeed ?:number
        // 消耗道具的id
            costItemId ?:number
        // 消耗道具的数量   一次的数量
            costItemCount ?:number

    }

// [请求]配送界面
    interface ZJSPeiSongPanelReq extends ReqObj {
        // 宅急送id
            zjsId ?:number
    }

// [返还]开始配送
    interface ZJSPeiSongPanelRsp extends RspObj {
        // 队长名称
            leaderName ?:string
        // 队长头像
            touXiang ?:number
        // 玩家形象
            xingXiang ?:number
        // 完成时间
            endTime ?:number
        // 奖励列表
            JiangLiList ?:Array<JiangliInfoUnit>
        // 护送1的头像
            oneTouXiang ?:number
        // 护送2的头像
            twoTouXiang ?:number
        // 获取奖励的实际%
            percent ?:number
        // 是否是队长
            isLeader ?:boolean
        // 新版队长个人形象数据
            leaderPhoto ?:PlayerPhotoUnit
        // 新版护送1的个人形象数据
            onePhoto ?:PlayerPhotoUnit
        // 新版护送2的个人形象数据
            twoPhoto ?:PlayerPhotoUnit

    }

// [请求]获取新的玩家组
    interface ZJSOthersReq extends ReqObj {
        // 宅急送id
            zjsId ?:number
    }

// [返还]获取新的玩家组
    interface ZJSOthersRsp extends RspObj {
        // 其他宅急送玩家
            ZJSOhterList ?:Array<ZJSDataInfoUnit>

    }

// [请求]获取抢夺对手
    interface ZJSgetTargetsReq extends ReqObj {
        // 选择类型0 默认 1 按人名  2 按公会名
            type ?:number
        // 服务器号
            serverNO ?:number
        // 搜索条件
            search ?:string
    }

// [返还]获取抢夺对手
    interface ZJSgetTargetsRsp extends RspObj {
        // 免费抢夺次数
            freeQiangDuo ?:number
        // 付费抢夺次数
            costQiangDuo ?:number
        // 付费购买的数额
            gouMaiCost ?:number
        // 购买次数
            gouMaiCount ?:number
        // 对战者组
            playerList ?:Array<ZJSDataInfoUnit>
        // 下次可刷新时间
            nextCdTime ?:number

    }

// [请求]购买抢夺次数
    interface ZJSBuyQiangDuoReq extends ReqObj {
        // 是否确认
            confirm ?:boolean
    }

// [返还]购买抢夺次数
    interface ZJSBuyQiangDuoRsp extends RspObj {
        // 免费抢夺次数
            freeQiangDuo ?:number
        // 付费抢夺次数
            costQiangDuo ?:number
        // 付费购买的数额
            gouMaiCost ?:number
        // 购买次数
            gouMaiCount ?:number

    }

// [请求]抢夺可得奖励
    interface ZJSQiangDuoJiangLiReq extends ReqObj {
        // 宅急送id
            zjsId ?:number
    }

// [返回]抢夺可得奖励
    interface ZJSQiangDuoJiangLiRsp extends RspObj {
        // 奖励列表
            JiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]抢夺某人
    interface ZJSQiangDuoFightReq extends ReqObj {
        // 宅急送id
            zjsId ?:number
    }

// [返回]抢夺结果
    interface ZJSQiangDuoFightRsp extends RspObj {
        // 是否成功
            success ?:boolean
        // 奖励列表
            JiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]查看历史记录
    interface ZJSShowHistroyReq extends ReqObj {
    }

// [返回]查看历史记录
    interface ZJSShowHistroyRsp extends RspObj {
        // 历史记录列表
            histroyList ?:Array<ZJSHistroyInfoUnit>

    }

// [请求]查看历史记录
    interface ZJSFightLogReq extends ReqObj {
        // 战斗记录id
            fightLogId ?:number
    }

// [返回]查看历史记录
    interface ZJSFightLogRsp extends RspObj {
        // 战斗日志
            fightInfo ?:string

    }

// [请求]双倍时间红点提示
    interface ZJSTipInfoReq extends ReqObj {
    }

// [返回]双倍时间红点提示
    interface ZJSTipInfoRsp extends RspObj {
        // 是否是双倍时间
            status ?:boolean

    }

// 宅急送喊话邀请
    interface ZhaiJiSongHanHuaYaoQingReq extends ReqObj {
        // 房间ID
            roomId ?:number
    }

// 宅急送喊话邀请
    interface ZhaiJiSongHanHuaYaoQingRsp extends RspObj {
        // 是否邀请成功
            isSuccess ?:boolean

    }

// 宅急送扫荡
    interface SweepZhaijisongReq extends ReqObj {
    }

// 宅急送扫荡
    interface SweepZhaijisongRsp extends RspObj {
        // 是否成功
            success ?:boolean
        // 奖励列表
            JiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]寻宝乐园面板
    interface XunBaoLeYuanPanelReq extends ReqObj {
        // 活动Id
            activityId ?:number
    }

// [返回]寻宝乐园面板
    interface XunBaoLeYuanPanelRsp extends RspObj {
        // 面板展示多少个道具 仅仅是面板 不是发奖
            puTongJiangliList ?:Array<JiangliInfoUnit>
        // 面板展示多少个道具 仅仅是面板 不是发奖
            teShuJiangliList ?:Array<JiangliInfoUnit>
        // 剩余次数
            leftCount ?:number
        // 总次数
            totalCount ?:number

    }

// [请求]摇奖
    interface XunBaoReq extends ReqObj {
        // 活动Id
            activityId ?:number
        // 0 1次   1 10次
            type ?:number
    }

// [返回]寻宝乐园面板
    interface XunBaoRsp extends RspObj {
        // 
            puTongunBaoLeYuanInfoList ?:Array<XunBaoLeYuanInfoUnit>
        // 
            teShuXunBaoLeYuanInfoList ?:Array<XunBaoLeYuanInfoUnit>

    }

// 获取神宠信息
    interface GetShenChongInfoReq extends ReqObj {
    }

// 获取神宠信息
    interface GetShenChongInfoRsp extends RspObj {
        // 神宠信息
            shenChongInfos ?:Array<ShenChongInfoUnit>
        // 属性信息
            propertyInfos ?:Array<HeroPropertyInfoUnit>
        // 出战神宠Id
            equipShenChongId ?:number
        // 神宠等级
            level ?:number
        // 进阶等级
            jinJieLv ?:number
        // 进阶是否有小红点
            jinjieTip ?:boolean
        // 升级是否有小红点
            lvUpTip ?:boolean
        // 守护兽是否可以重置
            isReset ?:boolean
        // 开启红点
            openTip ?:boolean

    }

// 打开神宠升级界面
    interface OpenShenChongLvUpReq extends ReqObj {
    }

// 打开神宠升级界面
    interface OpenShenChongLvUpRsp extends RspObj {
        // 当前等级属性值
            curPropertyInfos ?:Array<HeroPropertyInfoUnit>
        // 下一等级属性值
            nextPropertyInfos ?:Array<HeroPropertyInfoUnit>
        // 需要的道具
            needItem ?:Array<JiangliInfoUnit>
        // 神宠等级
            level ?:number

    }

// 神宠升级
    interface ShenChongLvUpReq extends ReqObj {
        // 0: 升级 1: 一键升级
            lvUpType ?:number
    }

// 神宠升级
    interface ShenChongLvUpRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 神宠激活
    interface ShenChongActivateReq extends ReqObj {
        // 激活哪只神宠
            defId ?:number
    }

// 神宠激活
    interface ShenChongActivateRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 打开神宠进阶
    interface OpenShenChongUpgradeReq extends ReqObj {
    }

// 打开神宠进阶
    interface OpenShenChongUpgradeRsp extends RspObj {
        // 当前等级属性值
            curPropertyInfos ?:Array<HeroPropertyInfoUnit>
        // 下一等级属性值
            nextPropertyInfos ?:Array<HeroPropertyInfoUnit>
        // 当前阶数
            curJieShu ?:number
        // 需要的道具
            needItem ?:Array<JiangliInfoUnit>
        // 升级下一阶所需的等级条件
            tiaojian ?:number

    }

// 神宠进阶
    interface ShenChongUpgradeReq extends ReqObj {
    }

// 神宠提升(进阶或激活)
    interface ShenChongUpgradeRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 打开神宠选择或切换界面
    interface OpenShenChongSelectReq extends ReqObj {
    }

// 打开神宠选择或切换界面
    interface OpenShenChongSelectRsp extends RspObj {
        // 火系神宠
            fireShenChongInfos ?:Array<ShenChongInfoUnit>
        // 水系神宠
            waterShenChongInfos ?:Array<ShenChongInfoUnit>
        // 草系属神宠
            grassShenChongInfos ?:Array<ShenChongInfoUnit>

    }

// 神宠系统别选择或切换
    interface ShenChongSelectReq extends ReqObj {
        // 选择切换哪个系统别的神宠
            shenChongType ?:number
    }

// 神宠系统别选择或切换
    interface ShenChongSelectRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 出战哪只宠物
    interface EquipShenChongReq extends ReqObj {
        // 出战哪只神宠
            shenChongId ?:number
    }

// 出战哪只宠物
    interface EquipShenChongRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]骑技面板
    interface QiJiTuPoPanelReq extends ReqObj {
    }

// [返回]骑技面板
    interface QiJiTuPoPanelRsp extends RspObj {
        // 当前属性列表
            currShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 增加属性列表
            addShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
            yuLanShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 当前阶段
            currJieDuan ?:number
        // 经验百分比 最大是9999 前端显示99.99
            jingYanBeiFenBi ?:number
        // 特殊属性列表
            TeShuShuXingInfoList ?:Array<string>
        // 是否可以突破  true 可以    false 不可以
            canTuPo ?:boolean
        // 坐骑等级突破限制
            needZuoQiDengJi ?:number
        // 是不是升到头了  true是    false不是
            isFull ?:boolean
        // 消耗列表
            costList ?:Array<JiangliInfoUnit>

    }

// [请求]骑技突破
    interface QiJiTuPoTuPoReq extends ReqObj {
        // 1 培养一次    2一件培养    3突破
            type ?:number
    }

// [返回]骑技面板
    interface QiJiTuPoTuPoRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]骑技突破  满级预览
    interface QiJiTuPoManJieYuLanReq extends ReqObj {
        // 1 培养一次    2一件培养    3突破
            type ?:number
    }

// [请求]骑技突破  满级预览
    interface QiJiTuPoManJieYuLanRsp extends RspObj {
        // 下一阶段属性列表
            yuLanShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 满阶所有特殊属性列表 前端自己区分那个是已经激活的
            teShuShuXingInfoList ?:Array<QiJiTeShuShuXingInfoUnit>

    }

// 【请求】获取充值金额列表
    interface GetChongzhiJineListReq extends ReqObj {
    }

// 【反馈】获取充值金额列
    interface GetChongzhiJineListRsp extends RspObj {
        // 获取充值金额列表
            chongzhiJineList ?:Array<ChongzhiJineInfoUnit>
        // true 有过首充   false  没有
            firstCharge ?:boolean
        // true 买过   false  没有
            zhongshenka ?:boolean
        // 是否充过四倍档
            isHaveFour ?:boolean

    }

// 【请求】点击充值
    interface DoGetChongzhiJineReq extends ReqObj {
        // 充值金额静态表中的id
            chongzhijineId ?:number
        // 用户登录的渠道简称
            channelAlias ?:string
        // 子渠道
            subChannel ?:string
        // 会话 ID
            sessionId ?:string
        // 设备类型，获取不到可留空
            model ?:string
        // H5 游戏，请传 IP
            deviceId ?:string
        // 透传参数，取进游戏的时的 state 字段的值，有大用
            ext ?:string
        // 登录的 openid
            uid ?:string
        // 平台：1，android；2，IOS
            platform ?:number
        // 为好友充值
            goalPlayerId ?:number
        // 订单的发货额外附加信息
            orderDeliveryAttachInfo ?:OrderDeliveryAttachInfoUnit
    }

// 【反馈】点击充值
    interface DoGetChongzhiJineRsp extends RspObj {
        // 是否充值成功
            isSuccess ?:boolean
        // 支付请求链接
            payURL ?:string
        // GAMEID+DATE+服务器生成的订单ID
            gameOrderId ?:string

    }

// 【请求】点击购买月卡
    interface BuyYueKaReq extends ReqObj {
        // 月卡静态表的id
            yuekaId ?:number
    }

// 【反馈】点击购买月卡
    interface BuyYueKaRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// 【请求】首冲
    interface GetChongzhiShouciNewReq extends ReqObj {
    }

// 【反馈】首冲
    interface GetChongzhiShouciNewRsp extends RspObj {
        // 首冲信息
            dataList ?:Array<ChongZhiShouCiNewUnit>
        // 当前是首冲第几天
            curDay ?:number
        // 一次性奖励
            onceReward ?:ChongZhiShouCiNewOnceRewardUnit

    }

// 【请求】领取首冲四倍奖励
    interface GetChongzhiShouciNewRewardReq extends ReqObj {
        // 第几天
            day ?:number
        // 1、按天领取奖励；2、领取一次性奖励
            type ?:number
    }

// 【反馈】领取首冲四倍奖励
    interface GetChongzhiShouciNewRewardRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean
        // 领取到的奖励列表
            rewardList ?:Array<JiangliInfoUnit>
        // 1、按天领取奖励；2、领取一次性奖励
            type ?:number

    }

// 【请求】发起充值,通用接口
    interface ChargeStartReq extends ReqObj {
        // 充值金额静态表中的id
            chongzhijineId ?:number
        // 玩家此次充值时所用的设备系统平台，ios/android
            os ?:string
        // 目标玩家ID
            goalPlayerId ?:number
        // 登录时传入的token
            token ?:string
        // 额外参数
            ext ?:string
        // 订单的发货额外附加信息
            orderDeliveryAttachInfo ?:OrderDeliveryAttachInfoUnit
    }

// 【反馈】发起充值,通用接口
    interface ChargeStartRsp extends RspObj {
        // 游戏订单ID
            playerOrderId ?:string
        // 第三方OpenId/UUID
            accountName ?:string
        // 商品Id
            itemCode ?:string
        // 平台方生成的预付订单号
            sdkOrderNo ?:string
        // 平台方的支付签名
            sdkPaySign ?:string
        // 发起充值时间，单位：毫秒
            createOrderTime ?:number
        // 各平台额外参数。QQ大厅对应字段：url_params
            params ?:string
        // 登录IP
            loginIp ?:string
        // 充值项目说明
            shuoming ?:string
        // 充值金额，等于充值金额表rmb
            rmb ?:number

    }

// 【请求】腾讯开放平台发起充值
    interface ChargeStartQQReq extends ReqObj {
        // session key
            openKey ?:string
        // 充值金额静态表中的id
            goodsId ?:number
        // 玩家此次充值时所用的设备系统平台，ios/android
            os ?:string
        // 跳转到应用首页后，URL后会带该参数。由平台直接传给应用，应用原样传给平台即可。表示平台的信息加密串，根据openid，openkey，pf，appid等生成。
            pfkey ?:string
        // 目标玩家ID
            goalPlayerId ?:number
    }

// 【反馈】腾讯开放平台发起充值
    interface ChargeStartQQRsp extends RspObj {
        // 交易的token号,有效期为15分钟。
            token ?:string
        // url参数。
            urlParams ?:string

    }

// 【请求】玩吧充值
    interface ChargeStartQZoneReq extends ReqObj {
        // session key
            openKey ?:string
        // 充值金额静态表中的id
            goodsId ?:number
        // 玩家此次充值时所用的设备系统平台，ios/android
            os ?:string
        // 区ID，用于区分用户是在哪一款平台下(Android、IOS等)。
            zoneid ?:number
        // 目标玩家ID
            goalPlayerId ?:number
    }

// 【反馈】玩吧充值
    interface ChargeStartQZoneRsp extends RspObj {
        // 返回码。
            retCode ?:number
        // 如果错误，返回错误信息。
            message ?:string
        // 用户积分
            score ?:number

    }

// 【请求】QQ游戏大厅充值
    interface ChargeStartQQGameReq extends ReqObj {
        // session key
            openKey ?:string
        // 充值金额静态表中的id
            goodsId ?:number
        // 玩家此次充值时所用的设备系统平台，ios/android
            os ?:string
        // 跟平台来源和openKey根据规则生成的一个密钥串
            pfKey ?:string
        // pf
            pf ?:string
        // 目标玩家ID
            goalPlayerId ?:number
    }

// 【反馈】QQ游戏大厅充值
    interface ChargeStartQQGameRsp extends RspObj {
        // 返回码。0为成功,其他为失败
            retCode ?:number
        // Url参数
            urlParams ?:string
        // 游戏币个数
            balance ?:number
        // 是否满足首次充值
            isFirstSave ?:boolean

    }

// 【请求】获取充值代币列表
    interface ChongZhiDaiBiListReq extends ReqObj {
    }

// 【反馈】获取充值代币列表
    interface ChongZhiDaiBiListRsp extends RspObj {
        // 获取充值金额列表
            chongzhiJineList ?:Array<ChongzhiJineInfoUnit>
        // true 有过首充   false  没有
            firstCharge ?:boolean
        // true 买过   false  没有
            zhongshenka ?:boolean

    }

// 【请求】金券购买
    interface JinQuanGouMaiReq extends ReqObj {
        // 充值金额表defId
            chongZhiJinEDefId ?:number
        // 金券数量
            jinQuanCount ?:number
    }

// 【反馈】金券购买
    interface JinQuanGouMaiRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// 【请求】改变充值发货状态
    interface UpdateChongZhiFaHuoStyleReq extends ReqObj {
        // 订单号
            orderId ?:string
    }

// 【请求】收到Sdk方的支付成功消息
    interface PayOrderSuccessReq extends ReqObj {
        // 游戏订单id
            gameOrderId ?:string
    }

// 【推送】推送引导礼包
    interface PushGuildGiftPackInfoRsp extends RspObj {
        // 引导礼包内容，undefined表示当前礼包消失
            guildGiftPackInfo ?:GuildGiftPackInfoUnit

    }

// 【请求】领取引导礼包奖励
    interface GetGuildGiftPackRewardReq extends ReqObj {
        // 礼包Id，领奖用
            giftPackId ?:number
    }

// [返回]领取引导礼包奖励
    interface GetGuildGiftPackRewardRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】发起支付时追加额外信息例如支付方式等
    interface AddExtendInfoForChargeStartReq extends ReqObj {
        // 游戏订单id
            gameOrderId ?:string
        // 支付方式：1采用客服会话支付 ，2采用弹出二维码支付
            paymentMethod ?:number
    }

// [请求]加载等级-vip礼包数据
    interface LoadDengJiVipLiBaoListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载等级-vip礼包数据
    interface LoadDengJiVipLiBaoListRsp extends RspObj {
        // 等级-vip
            detailList ?:Array<DengJiVipLiBaoInfoUnit>

    }

// [请求]领取等级-vip礼包
    interface GetDengJiVipLiBaoReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取等级-vip礼包
    interface GetDengJiVipLiBaoRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]砸金蛋面板
    interface LoadZaJinDanPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]砸金蛋面板
    interface LoadZaJinDanPanelRsp extends RspObj {
        // 砸蛋所需要的道具 
            xiaoHaoDaoJuId ?:number
        // 面板展示砸蛋可能获得奖励 
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]砸金蛋
    interface GetZaJinDanReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 砸几个蛋
            num ?:number
    }

// [返回]砸金蛋
    interface GetZaJinDanRsp extends RspObj {
        // 砸蛋所得奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]购买锤子
    interface GouMaiChuiZiReq extends ReqObj {
        // 锤子ID
            daoJuId ?:number
        // 买几个锤子
            num ?:number
    }

// [返回]购买锤子
    interface GouMaiChuiZiRsp extends RspObj {
        // 购买是否成功
            isSuccess ?:boolean

    }

// 加载周末在线礼包
    interface LoadWeekendZaiXianJiangLiReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]加载周末在线礼包
    interface LoadWeekendZaiXianJiangLiRsp extends RspObj {
        // 获取在线奖励列表
            zaixianJiangliList ?:Array<WeekendZaixianJiangliInfoUnit>
        // 在线时间
            onlineTime ?:number

    }

// 领取周末在线礼包
    interface GetWeekendZaiXianJiangLiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 在线奖励表Id
            zaixianjiangliId ?:number
    }

// [返回]领取周末在线礼包
    interface GetWeekendZaiXianJiangLiRsp extends RspObj {
        // 领取是否成功
            isSuccess ?:boolean

    }

// 记载周末登录礼包
    interface LoadWeekendLoginJiangLiReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]加载周末登录礼包
    interface LoadWeekendLoginJiangLiRsp extends RspObj {
        // 获取登录奖励列表
            loginJiangliList ?:Array<WeekendLoginJiangliInfoUnit>

    }

// 预览周末登陆礼包
    interface ShowWeekendLoginJiangLiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 奖励表Id
            defId ?:number
    }

// [返回]预览周末登陆礼包
    interface ShowWeekendLoginJiangLiRsp extends RspObj {
        // 预览是否成功
            isSuccess ?:boolean

    }

// 领取周末登录礼包
    interface GetWeekendLoginJiangLiReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 奖励表Id
            defId ?:number
    }

// [返回]领取周末登录礼包
    interface GetWeekendLoginJiangLiRsp extends RspObj {
        // 领取是否成功
            isSuccess ?:boolean
        // 下一档位登录奖励
            nextJiangliUnit ?:WeekendLoginJiangliInfoUnit

    }

// [请求]刮刮乐面板
    interface LoadGuaGuaLePanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]刮刮乐面板
    interface LoadGuaGuaLePanelRsp extends RspObj {
        // 刮刮乐所需要的道具 
            xiaoHaoDaoJuId ?:number
        // 面板展示刮刮乐可能获得奖励 
            jiangLiList ?:Array<JiangliInfoUnit>
        // 面板展示刮刮乐已经刮出来的奖励 
            hasList ?:Array<MoFaShaiZiInfoUnit>
        // 背景图片ID
            mapId ?:number

    }

// [请求]刮刮乐
    interface GetGuaGuaLeReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 0 一个 1剩下的多个
            type ?:number
        // 位置
            position ?:number
    }

// [返回]刮刮乐
    interface GetGuaGuaLeRsp extends RspObj {
        // 刮刮乐所得奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]购买刮刮刀
    interface GouMaiGuaGuaDaoReq extends ReqObj {
        // 买几个刮刮刀
            num ?:number
    }

// [返回]购买刮刮刀
    interface GouMaiGuaGuaDaoRsp extends RspObj {
        // 购买是否成功
            isSuccess ?:boolean

    }

// [请求]老玩家回归面板
    interface LaoWanJiaHuiGuiPanelReq extends ReqObj {
        // 活动Id
            activityId ?:number
    }

// [返回]老玩家回归面板
    interface LaoWanJiaHuiGuiPanelRsp extends RspObj {
        // 档位
            dangWei ?:number
        // 奖励列表
            jiangliList ?:Array<LaoWanJiaHuiGuiInfoUnit>
        // 开始时间
            startTime ?:number
        // 结束时间
            endTime ?:number

    }

// [请求]领取老玩家回归奖励
    interface GetLaoWanJiaHuiGuiReq extends ReqObj {
        // 活动Id
            activityId ?:number
        // 奖励Id
            defId ?:number
    }

// [返回]领取老玩家回归奖励
    interface GetLaoWanJiaHuiGuiRsp extends RspObj {
        // 领取的奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 领取是否成功
            isSuccess ?:boolean

    }

// [请求]跨服积分赛公会统计
    interface GetGongHuiTongJiReq extends ReqObj {
        // 活动Id
            activityId ?:number
    }

// [返回]跨服积分赛公会统计
    interface GetGongHuiTongJiRsp extends RspObj {
        // 公会名称
            gongHuiName ?:string
        // 公会统计信息
            tongJiInfoList ?:Array<GongHuiTongJiInfoUnit>

    }

// [请求]道具消耗返利面板
    interface DaoJuXiaoHaoFanLiPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]道具消耗返利面板
    interface DaoJuXiaoHaoFanLiPanelRsp extends RspObj {
        // 面板展示消耗信息和奖励列表 
            xiaoHaoList ?:Array<DaoJuXiaoHaoInfoUnit>

    }

// [请求]领取道具消耗奖励
    interface GetDaoJuXiaoHaoRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 策划表 id
            defId ?:number
    }

// [返回]领取道具消耗奖励
    interface GetDaoJuXiaoHaoRewardRsp extends RspObj {
        // 道具消耗奖励
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]召唤皮神面板
    interface LoadZhaoHuanPiShenPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]召唤皮神面板
    interface LoadZhaoHuanPiShenPanelRsp extends RspObj {
        // 召唤皮神所需要的道具 
            xiaoHaoDaoJuId ?:number
        // 面板展示召唤皮神可能获得奖励 
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]召唤皮神
    interface GetZhaoHuanPiShenReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 1 召唤一次 2 终极召唤
            type ?:number
    }

// [返回]召唤皮神
    interface GetZhaoHuanPiShenRsp extends RspObj {
        // 召唤皮神所得奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]购买召唤卡
    interface GouMaiZhaoHuanKaReq extends ReqObj {
        // 买几个召唤卡
            num ?:number
    }

// [返回]购买召唤卡
    interface GouMaiZhaoHuanKaRsp extends RspObj {
        // 购买是否成功
            isSuccess ?:boolean

    }

// [请求]道具互换界面
    interface DaoJuHuHuanPanelReq extends ReqObj {
        // 活动id
            activityId ?:number
    }

// [返回]道具互换界面
    interface DaoJuHuHuanPanelRsp extends RspObj {
        // 每个档位的道具信息
            infoList ?:Array<DaoJuHuHuanInfoUnit>

    }

// [请求]进行道具互换
    interface GetDaoJuHuHuanReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 旧的道具或者精灵
            oldDaoJu ?:JiangliInfoUnit
        // 新的道具或者精灵
            newDaoJu ?:JiangliInfoUnit
    }

// [返回]进行道具互换
    interface GetDaoJuHuHuanRsp extends RspObj {
        // 是否互换成功
            isSuccess ?:boolean

    }

// [请求]每日团购界面
    interface MeiRituanGouPanelReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 礼包档位
            libaoType ?:number
    }

// [返回]每日团购界面
    interface MeiRituanGouPanelRsp extends RspObj {
        // 团购礼包列表
            libaoList ?:Array<MeiRiTuanGouInfoUnit>

    }

// [请求]购买团购礼包
    interface GetMeiRituanGouReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 礼包ID
            libaoId ?:number
    }

// [返回]购买团购礼包
    interface GetMeiRituanGouRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// [请求]获取礼包额外奖励
    interface GetOtherRewardReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 礼包ID
            libaoId ?:number
    }

// [返回]获取礼包额外奖励
    interface GetOtherRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// [请求]怪物来袭面板
    interface LoadGuaiWuLaiXiPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]怪物来袭面板
    interface LoadGuaiWuLaiXiPanelRsp extends RspObj {
        // ]杀怪所需要的道具 
            xiaoHaoDaoJuId ?:number
        // 面板展示]杀怪可能获得奖励 
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]杀怪
    interface KillGuaiWuReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 杀几个蛋
            num ?:number
    }

// [返回]杀怪
    interface KillGuaiWuRsp extends RspObj {
        // 杀怪所得奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]购买道具
    interface GouMaiKillToolReq extends ReqObj {
        // 道具id
            daoJuId ?:number
        // 买几个道具
            num ?:number
    }

// [返回]购买道具
    interface GouMaiKillToolRsp extends RspObj {
        // 购买是否成功
            isSuccess ?:boolean

    }

// [请求]获取排行榜
    interface GuaiWuLaiXiRankReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]购买道具
    interface GuaiWuLaiXiRankRsp extends RspObj {
        // 怪物来袭排名信息
            ranlist ?:Array<GuaiWuLaiXiRankInfoUnit>
        // 我的相关信息
            myinfo ?:GuaiWuLaiXiRankInfoUnit

    }

// [推送]变化的活动数据
    interface PushChangeNewActivityRsp extends RspObj {
        // 变化的活动数据
            activityInfo ?:NewActivityInfoUnit

    }

// [请求]节日基金面板
    interface LoadJieRiJiJinPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]]节日基金面板
    interface LoadJieRiJiJinPanelRsp extends RspObj {
        // 可投资的金额金额
            vailableDefindValues ?:Array<number>
        // 已经投资的金额,如果这个字段>0 说明 已经投资，上面字段可忽略，否则显示上面字段的内容
            selectValue ?:number
        // 奖励列表
            rewardList ?:Array<JieRiJiJinRewardInfoUnit>

    }

// [请求]节日基金套现
    interface GetJieRiJiJinRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 套哪天的钱
            day ?:number
    }

// [返回]节日基金套现
    interface GetJieRiJiJinRewardRsp extends RspObj {
        // 1成功，负数失败
            result ?:number
        // 当前的钱
            remain ?:number

    }

// [请求]节日基金投资
    interface BuyJieRiJiJinReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 投资金额
            count ?:number
    }

// [返回]节日基金投资
    interface BuyJieRiJiJinRsp extends RspObj {
        // 结果,1,成功，负数错误
            result ?:number
        // 已经投资的金额
            selectValue ?:number
        // 奖励列表
            rewardList ?:Array<JieRiJiJinRewardInfoUnit>

    }

// [请求]招财猫面板
    interface GetZhaoCaiMaoPanelReq extends ReqObj {
        // 活动id
            activityId ?:number
    }

// [返回]招财猫面板
    interface GetZhaoCaiMaoPanelRsp extends RspObj {
        // 最高可得金卷
            max ?:number
        // 投入金卷
            requireJinJuan ?:number
        // 可投入次数
            remainTime ?:number
        // 还需充值
            needRmb ?:number

    }

// [请求]招财
    interface GetZhaoCaiMaoRewardReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 投入金额
            jinQuan ?:number
    }

// [返回]招财
    interface GetZhaoCaiMaoRewardRsp extends RspObj {
        // 1成功
            ret ?:number
        // 增加的金卷
            change ?:number

    }

// 【请求】七日任务
    interface OpenSevenDayTaskReq extends ReqObj {
        // 活动id
            activityId ?:number
    }

// 【反馈】七日任务
    interface OpenSevenDayTaskRsp extends RspObj {
        // 今日好礼
            loginInfo ?:SDTLoginInfoUnit
        // 任务栏1名称
            questOneName ?:string
        // 任务栏1说明
            questOneShuoMing ?:string
        // 任务栏1当前值
            questOneValue ?:number
        // 任务1
            questOneInfo ?:Array<SDTQuestInfoUnit>
        // 任务栏2名称
            questTwoName ?:string
        // 任务栏2说明
            questTwoShuoMing ?:string
        // 任务栏2当前值
            questTwoValue ?:number
        // 任务2
            questTwoInfo ?:Array<SDTQuestInfoUnit>
        // 任务栏3名称
            questThreeName ?:string
        // 任务栏3说明
            questThreeShuoMing ?:string
        // 任务栏3当前值
            questThreeValue ?:number
        // 任务3
            questThreeInfo ?:Array<SDTQuestInfoUnit>
        // 今日特惠
            buyInfo ?:SDTBuyInfoUnit
        // 每日完成任务总奖励
            dailyInfo ?:SDTDailyInfoUnit
        // 天数id，从小到大的顺序代表1-7天
            dayId ?:number
        // 前一天是否有可领取的奖励
            preDayBonus ?:boolean
        // 后一天是否有可领取的奖励
            nextDayBonux ?:boolean
        // 七日商店在售商品数据
            goodsList ?:Array<SevenDayShopGoodsInfoUnit>
        // 七日商店栏目名称
            sevenDayShopName ?:string
        // 前一天是否有可购买的商品
            preDayShopTips ?:boolean
        // 后一天是否有可购买的商品
            nextDayShopTips ?:boolean

    }

// 【请求】领取七日任务奖励
    interface SevenDayTaskReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 领取类型：【1：今日好礼；2：任务1；3：任务2；4：今日特惠；5：每日完成任务总奖励】
            type ?:number
        // 任务ID
            defId ?:number
    }

// 【反馈】领取七日任务奖励
    interface SevenDayTaskRsp extends RspObj {
        // 领取类型：【1：今日好礼；2：任务1；3：任务2；4：今日特惠5 ：每日完成任务总奖励】
            type ?:number
        // 任务ID
            defId ?:number
        // 领取数据
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]充值一冲选领
    interface ChongZhiYiChongXuanLingReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 充值金额表defId
            chongZhiJinEDefId ?:number
        // 金券数量
            jinQuanCount ?:number
    }

// [反馈]领取一冲选领
    interface ChongZhiYiChongXuanLingRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]挑战神宠界面
    interface TiaoZhanShenShouPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]挑战神宠界面
    interface TiaoZhanShenShouPanelRsp extends RspObj {
        // 关卡列表
            guanKaInfo ?:Array<TiaoZhanShenShouGuanKaInfoUnit>
        // 当前关卡
            curDefId ?:number
        // 我的排名
            position ?:number
        // 排行信息
            paihangList ?:Array<PaihangInfoUnit>

    }

// [请求]挑战神宠
    interface TiaoZhanShenShouReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 关卡ID
            defId ?:number
    }

// [反馈]挑战神宠
    interface TiaoZhanShenShouRsp extends RspObj {
        // 战报
            fightInfo ?:string
        // 是否胜利
            isWin ?:boolean
        // 奖励列表
            gains ?:Array<JiangliInfoUnit>

    }

// [请求]嘉年华面板
    interface LoadJiaNianHuaPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]嘉年华面板
    interface LoadJiaNianHuaPanelRsp extends RspObj {
        // 列表
            infoList ?:Array<JiaNianHuaRewardInfoUnit>

    }

// [请求]获取嘉年华奖励
    interface GetJiaNiaNianHuaRewardReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 0 登陆，1 充值， 2 消费
            getType ?:number
    }

// [请求]获取嘉年华奖励
    interface GetJiaNiaNianHuaRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 当前的积分
            score ?:number

    }

// [请求]嘉年华商店面板
    interface LoadJiaNianHuaShopPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]嘉年华商店面板
    interface LoadJiaNianHuaShopPanelRsp extends RspObj {
        // 积分
            score ?:number
        // 列表
            infoList ?:Array<JiaNianHuaShopInfoUnit>

    }

// [请求]获取嘉年华买道具
    interface BuyJiaNiaNianHuaItemReq extends ReqObj {
        // 活动id
            activityId ?:number
        // defId
            defId ?:number
        // 购买的次数
            count ?:number
    }

// [请求]获取嘉年华买道具
    interface BuyJiaNiaNianHuaItemRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 仅当成功字段有效当前的积分
            score ?:number

    }

// [请求]连续充值活动面板
    interface LianXuChongZhiHuoDongPanelReq extends ReqObj {
        // 活动id
            activityId ?:number
    }

// [请求]连续充值活动面板
    interface LianXuChongZhiHuoDongPanelRsp extends RspObj {
        // 活动信息列表
            infoList ?:Array<LianXuChongZhiHuoDongInfoUnit>
        // 连续充值天数
            lianXuDay ?:number

    }

// [请求]连续充值活动面板
    interface GetLianXuChongZhiHuoDongRewardReq extends ReqObj {
        // 活动id
            activityId ?:number
        // defId
            defId ?:number
    }

// [请求]领取连续充值活动奖励
    interface GetLianXuChongZhiHuoDongRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// [请求]神宠基金列表
    interface ShenChongJiJinListReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]神宠基金列表
    interface ShenChongJiJinListRsp extends RspObj {
        // 
            ShenChongJiJinInfoList ?:Array<ShenChongJiJinInfoUnit>
        // 购买价格
            price ?:number
        // 能不能买 活动结束就不能买了 但是已经买过的人可以领
            canBuy ?:boolean
        // 是否购买过
            bought ?:boolean

    }

// [请求]领取神宠基金
    interface GetShenChongJiJinReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取神宠基金
    interface GetShenChongJiJinRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]购买神宠基金
    interface BuyShenChongJiJinReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]购买神宠基金
    interface BuyShenChongJiJinRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]加载超值返利数据
    interface LoadChaoZhiFanLiReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [反馈]加载超值返利数据
    interface LoadChaoZhiFanLiRsp extends RspObj {
        // 超值返利数据
            detailList ?:Array<ChaoZhiFanLiInfoUnit>

    }

// [请求]领取超值返利奖励
    interface ReceiveChaoZhiFanLiRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// [反馈]领取超值返利奖励
    interface ReceiveChaoZhiFanLiRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 静态表id
            defId ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]跨服砸金蛋面板
    interface LoadKuaFuZaJinDanPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]跨服砸金蛋面板
    interface LoadKuaFuZaJinDanPanelRsp extends RspObj {
        // 砸蛋所需要的道具 
            xiaoHaoDaoJuId ?:number
        // 面板展示砸蛋可能获得奖励 
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]跨服砸金蛋
    interface GetKuaFuZaJinDanReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 砸几个蛋
            num ?:number
    }

// [返回]跨服砸金蛋
    interface GetKuaFuZaJinDanRsp extends RspObj {
        // 砸蛋所得奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]跨服砸金蛋排行榜
    interface GetKuaFuZaJinDanRankingReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]跨服砸金蛋排行榜
    interface GetKuaFuZaJinDanRankingRsp extends RspObj {
        // 排行列表
            top10 ?:Array<KuaFuZaJinDanRankingInfoUnit>
        // 我的排名，没排名0
            myRank ?:number
        // 我的积分
            myScore ?:number

    }

// 【请求】挖宝石
    interface DigBaoshiReq extends ReqObj {
    }

// 【反馈】挖宝石
    interface DigBaoshiRsp extends RspObj {
        // 获取的宝石ID（静态表ID）
            daojuId ?:number

    }

// 【请求】挖高级宝石
    interface DigHighBaoshiReq extends ReqObj {
    }

// 【反馈】挖高级宝石
    interface DigHighBaoshiRsp extends RspObj {
        // 获取的宝石ID（静态表ID）
            daojuId ?:number

    }

// 【请求】挖顶级宝石
    interface DigTopLevelBaoshiReq extends ReqObj {
    }

// 【反馈】挖顶级宝石
    interface DigTopLevelBaoshiRsp extends RspObj {
        // 获取的宝石ID（静态表ID）
            daojuId ?:number

    }

// 【请求】宝石转换
    interface TransformBaoShiReq extends ReqObj {
        // 新活动ID
            newActivityId ?:number
        // 需要转换的道具ID
            daojuDbId ?:number
        // 转换后的宝石ID（静态表ID）
            targetDaojuId ?:number
    }

// 【反馈】宝石转换
    interface TransformBaoShiRsp extends RspObj {
        // 转换后的宝石ID（静态表ID）
            targetDaojuId ?:number

    }

// 【请求】加载公会战地图相关信息
    interface LoadGongHuiZhanSceneDataReq extends ReqObj {
    }

// 【反馈】加载公会战地图相关信息
    interface LoadGongHuiZhanSceneDataRsp extends RspObj {
        // 战场信息
            sceneInfos ?:Array<GongHuiZhanSceneInfoUnit>
        // 最强公会
            zuiQianGongHui ?:GongHuiZhanGongHuiInfoUnit
        // 最强个人
            zuiQianGeRen ?:GongHuiZhanPlayerInfoUnit
        // 个人排名
            geRenRank ?:number
        // 个人积分
            geRenScore ?:number
        // 公会排名
            gongHuiRank ?:number
        // 公会积分
            gongHuiScore ?:number
        // 追击距离,格子数
            followRange ?:number
        // 下一次能进的时间戳
            nextEnterSceneTime ?:number

    }

// 【请求】加载加载公会战活动数据
    interface LoadGongHuiZhanDataReq extends ReqObj {
    }

// 【反馈】加载或推送加载公会战活动数据
    interface LoadGongHuiZhanDataRsp extends RspObj {
        // 活动状态, 0:未开启 1:准备 2:开启 3:结束
            status ?:number
        // 活动开始时间,单位:毫秒
            beginTime ?:number
        // 活动结束时间,单位:毫秒
            endTime ?:number
        // 可重新进入时间,0,Undefined表示无限制
            reenterableTime ?:number
        // 累计胜利场数,即击杀人数
            winSize ?:number
        // 是否今天参加过活动,并且逃跑了
            hadEnteredThisDay ?:boolean
        // 是否可以进公会战
            canEnter ?:boolean

    }

// 【请求】公会战鼓舞
    interface GongHuiZhanGuWuReq extends ReqObj {
        // 1:普通鼓舞; 2:一键鼓舞
            type ?:number
    }

// 【反馈】公会战鼓舞
    interface GongHuiZhanGuWuRsp extends RspObj {
        // 1:普通鼓舞; 2:一键鼓舞
            type ?:number
        // 能力值
            nengLi ?:number
        // 鼓舞信息
            guWuInfo ?:GongHuiZhanGuWuInfoUnit

    }

// 【请求】获取公会战鼓舞信息
    interface GetGongHuiZhanGuWuInfoReq extends ReqObj {
    }

// 【反馈】获取公会战鼓舞信息
    interface GetGongHuiZhanGuWuInfoRsp extends RspObj {
        // 一次鼓舞消耗
            oneGuWuCost ?:Array<JiangliInfoUnit>
        // 十次鼓舞消耗
            tenGuWuCost ?:Array<JiangliInfoUnit>
        // 能力值
            nengLi ?:number
        // 普通鼓舞
            puTongGuWu ?:GongHuiZhanGuWuInfoUnit
        // 一键鼓舞
            yiJianGuWu ?:GongHuiZhanGuWuInfoUnit

    }

// 【请求】加载战场排行数据
    interface LoadGongHuiZhanRankReq extends ReqObj {
        // 0:个人排行 1:公会排行 2:所有排行
            type ?:number
        // 请求数量
            count ?:number
        // 是否立即结算本次活动奖励
            isClear ?:boolean
    }

// 【反馈】加载战场排行数据
    interface LoadGongHuiZhanRankRsp extends RspObj {
        // 排行数据
            playerList ?:Array<GongHuiZhanPlayerInfoUnit>
        // 我的数据
            myData ?:GongHuiZhanPlayerInfoUnit
        // 公会排行数据
            gongHuiList ?:Array<GongHuiZhanGongHuiInfoUnit>
        // 我的公会数据
            myGongHuiData ?:GongHuiZhanGongHuiInfoUnit
        // 同一个公会人数
            gongHuiNum ?:number

    }

// 请求公会战奖励
    interface GetGongHuiZhanRewardReq extends ReqObj {
        // 玩家Id
            playerId ?:number
    }

// 请求公会战奖励
    interface GetGongHuiZhanRewardRsp extends RspObj {
        // 结果: 0:失败 1:成功
            rspResult ?:number
        // 玩家Id
            playerId ?:number
        // 奖励数据
            JiangliInfos ?:Array<JiangliInfoUnit>

    }

// 【请求】公会战排行奖励
    interface GetGongHuiZhanRankRewardReq extends ReqObj {
    }

// 【反馈】公会战排行奖励
    interface GetGongHuiZhanRankRewardRsp extends RspObj {
        // 个人积分奖励
            playerRewardList ?:Array<GongHuiZhanRankRewardInfoUnit>
        // 公会积分奖励
            gonghuiRewardList ?:Array<GongHuiZhanRankRewardInfoUnit>

    }

// 【请求】扩展玩家物件空间
    interface BuySpaceReq extends ReqObj {
        // 1 - 英雄空间， 2 - 装备空间， 3 - 道具空间
            spaceType ?:number
    }

// 【反馈】返回扩展玩家物件空间
    interface BuySpaceRsp extends RspObj {
        // 1 - 英雄空间， 2 - 装备空间， 3 - 道具空间
            spaceType ?:number
        // 购买类型的新空间值
            spaceNewValue ?:number

    }

// 【请求】获得玩家道具列表
    interface GetPlayerDaojuListReq extends ReqObj {
    }

// 【反馈】获得玩家道具列表
    interface GetPlayerDaojuListRsp extends RspObj {
        // 玩家获得道具列表
            daojuList ?:Array<DaojuInfoUnit>

    }

// 【请求】卖出道具
    interface SellDaojuReq extends ReqObj {
        // 道具dbID，道具表数据表主键
            daojuDbID ?:number
        // 卖出数量
            sellCount ?:number
    }

// 【反馈】卖出道具
    interface SellDaojuRsp extends RspObj {
        // 道具dbID，道具表数据表主键
            daojuDbID ?:number
        // 该道具剩余数量
            leftCount ?:number
        // 出卖道具后，玩家所有银两
            yinLiang ?:number

    }

// 【请求】使用道具（根据F_SCRIPT_ID做相应操作）
    interface OpenDaojuReq extends ReqObj {
        // 道具dbID，道具表数据表主键
            daojuDbID ?:number
        // 选择神兽礼盒的第几只宠物
            index ?:number
        // 同一Id全部使用
            isOpenAll ?:boolean
        // 同一道具类型全部使用
            isOpenAllType ?:boolean
        // 使用数量
            useCount ?:number
    }

// 【反馈】使用道具（根据F_SCRIPT_ID做相应操作）
    interface OpenDaojuRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 类型：1，打开元宵礼盒
            type ?:number
        // 跳转ID
            jumpId ?:number
        // 额外参数
            canShu1 ?:number
        // 道具表ID
            daojuDefId ?:number

    }

// 【请求】一键碎片合成
    interface HeChengJingLingOneKeyReq extends ReqObj {
    }

// 【反馈】一键碎片合成
    interface HeChengJingLingOneKeyRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 拆分宝石
    interface ChaiFenBaoShiReq extends ReqObj {
        // 道具dbID，道具表数据表主键
            daojuDbID ?:number
    }

// 【反馈】拆分宝石
    interface ChaiFenBaoShiRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 快速购买道具
    interface BuyItemQuickReq extends ReqObj {
        // 道具id
            itemDefId ?:number
        // 数量
            count ?:number
    }

// 快速购买道具
    interface BuyItemQuickRsp extends RspObj {
        // 是否互换成功
            isSuccess ?:boolean

    }

// 【请求】领取临时背包物品
    interface GetTempBeibaoReq extends ReqObj {
        // 领取临时背包列表
            tempBeibaoList ?:Array<TempBeibaoInfoUnit>
    }

// 【反馈】领取临时背包物品
    interface GetTempBeibaoRsp extends RspObj {
        // 处理结果：1、领取成功；2、领取失败；
            result ?:number

    }

// 【请求】获取任务类道具的任务条件进度值
    interface GetMissionCountForItemReq extends ReqObj {
        // 道具表Id
            defId ?:number
    }

// 【反馈】获取任务类道具的任务条件进度值
    interface GetMissionCountForItemRsp extends RspObj {
        // 要求达成的条件值
            conditionCount ?:number
        // 当前已经完成的数值
            currentCount ?:number

    }

// 【请求】获取优惠券列表
    interface GetYouHuiQuanListReq extends ReqObj {
    }

// 【反馈】获取优惠券列表
    interface GetYouHuiQuanListRsp extends RspObj {
        // 获取优惠券列表
            youHuiQuanList ?:Array<YouHuiQuanInfoUnit>

    }

// 【请求】使用优惠券
    interface UseYouHuiQuanReq extends ReqObj {
        // 优惠券Id
            liQuanId ?:number
    }

// 【反馈】使用优惠券
    interface UseYouHuiQuanRsp extends RspObj {
        // 使用的优惠券Id
            liQuanId ?:number
        // 是否使用成功
            isSuccess ?:boolean
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】战斗pve普通、pve精英
    interface FightPVEReq extends ReqObj {
        // 进行战斗的静态表（xiaoguan/daguan）中的Id
            nParam ?:number
        // 标识战斗类型：1、pve普通(小关)；2、pve精英(大关)
            fType ?:number
    }

// 【反馈】战斗pve普通、pve精英
    interface FightPVERsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】获得玩家已通过小关列表
    interface GetPlayerXiaoguanListReq extends ReqObj {
    }

// 【反馈】获得玩家已通过小关列表
    interface GetPlayerXiaoguanListRsp extends RspObj {
        // 玩家关卡列表
            xiaoguanList ?:Array<GuanqiaInfoUnit>
        // 玩家星数奖励下一个奖励的ID
            xingshuJinagliId ?:number

    }

// 【请求】获得玩家已通过大关列表
    interface GetPlayerDaguanListReq extends ReqObj {
    }

// 【反馈】获得玩家已通过大关列表
    interface GetPlayerDaguanListRsp extends RspObj {
        // 玩家关卡列表
            daguanList ?:Array<GuanqiaInfoUnit>
        // 玩家战阵defId
            zhanZhenDefId ?:number

    }

// 【请求】关卡--扫荡：小关、大关 
    interface SweepFightReq extends ReqObj {
        // 进行扫荡的静态表（xiaoguan/daguan）中的Id
            nParam ?:number
        // 标识扫荡关卡类型：1、普通(小关)；2、精英(大关);  
            fType ?:number
    }

// 【反馈】关卡--扫荡：小关、大关、装备副本
    interface SweepFightRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number

    }

// 【请求】关卡--重置某一章节的所有精英关卡
    interface ResetDaguanReq extends ReqObj {
        // 要重置的大关章节的静态ID
            zhangjieId ?:number
    }

// 【反馈】关卡--重置精英关卡
    interface ResetDaguanRsp extends RspObj {
        // 是否重置成功
            isSuccess ?:boolean

    }

// 【请求】购买挑战次数
    interface BuyChallengeTimesReq extends ReqObj {
        // 关卡id
            guanqiaId ?:number
        // 关卡类型 1普通  2精英 3精英统一次数
            guanqiaType ?:number
        // 购买多少次
            buyTimes ?:number
    }

// 【反馈】购买挑战次数
    interface BuyChallengeTimesRsp extends RspObj {
        // 是否重置成功
            isSuccess ?:boolean
        // 关卡id
            guanqiaId ?:number
        // 关卡类型 1普通  2精英 3精英统一次数
            guanqiaType ?:number
        // 购买多少次
            buyTimes ?:number

    }

// [请求]购买挑战次数
    interface TongYiFightCountReq extends ReqObj {
    }

// 【反馈】购买挑战次数
    interface TongYiFightCountRsp extends RspObj {
        // 统一挑战剩余次数
            leftCount ?:number
        // 购买多少次
            buyTimes ?:number

    }

// [请求] 装备副本战斗
    interface ZhuangBeiFuBenFightPVEReq extends ReqObj {
        // 进行战斗的静态表中的Id
            defId ?:number
    }

// [请求] 装备副本战斗
    interface ZhuangBeiFuBenFightPVERsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求] 获得装备副本次数
    interface GetZhuangBeiFuBenCishuReq extends ReqObj {
    }

// [请求] 获得装备副本次数
    interface GetZhuangBeiFuBenCishuRsp extends RspObj {
        // 新的挑战次数
            newFightTimes ?:number
        // 已经买了多少次
            buyTimes ?:number

    }

// 【请求】获得玩家已通过副本中心列表
    interface GetFubenZhongXinListReq extends ReqObj {
    }

// 【反馈】获得玩家已通过副本中心列表
    interface GetFubenZhongXinListRsp extends RspObj {
        // 副本Id列表
            fuBenInfoList ?:Array<FuBenInfoUnit>

    }

// 【请求】获得游戏配置数据
    interface GetGameDefListReq extends ReqObj {
        // 配置项类型:1小关数据(普通关卡); 2大关数据(道馆挑战); 3怪物数据；4磨炼；6称号；7公会副本；8商城；9怪物骨骼；10小镇入侵者怪物；11新神技
            type ?:number
        // 游戏数据配置项ID列表
            defIdList ?:Array<number>
    }

// 【反馈】获得游戏配置数据
    interface GetGameDefListRsp extends RspObj {
        // 配置项类型:1小关数据(普通关卡); 2大关数据(道馆挑战); 3怪物数据；4磨炼；6称号；7公会副本；8商城；9怪物骨骼；10小镇入侵者怪物；11新神技
            type ?:number
        // 小关数据列表
            xiaoguanDefList ?:Array<XiaoguanDefUnit>
        // 大关数据列表
            daguanDefList ?:Array<DaguanDefUnit>
        // 怪物数据列表
            guaiwuDefList ?:Array<GuaiwuDefUnit>
        // 磨炼数据列表
            moLianFuBenDefList ?:Array<MoLianFuBenDefUnit>
        // 皮肤系统
            piFuXiTongDefList ?:Array<PiFuXiTongDefUnit>
        // 称号数据
            chengHaoDefList ?:Array<ChengHaoDefUnit>
        // 是否请求所有数据 (目前已经支持的有称号数据)
            isAllData ?:boolean
        // 公会副本
            gongHuiFuBenDefList ?:Array<GongHuiFuBenDefUnit>
        // 商城数据
            shopDefList ?:Array<ShopDefUnit>
        // 怪物骨骼数据
            guaiwuGugeDefList ?:Array<GugeUnit>
        // 小镇入侵者怪物数据
            xiaozhenRuqinDefList ?:Array<XiaozhenRuqinDefUnit>
        // 新神技数据
            magicalSkillDefList ?:Array<MagicalSkillDefUnit>

    }

// [请求] 装备副本战斗
    interface FuBenFightPVEReq extends ReqObj {
        // 副本的类型 宝石4 附魔石5 骑术6
            fuBenType ?:number
        // 进行战斗的静态表中的Id
            defId ?:number
    }

// [请求] 装备副本战斗
    interface FuBenFightPVERsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求] 副本中心列表
    interface FuBenLieBiaoReq extends ReqObj {
    }

// [请求] 副本中心列表
    interface FuBenLieBiaoRsp extends RspObj {
        // 奖励列表
            defList ?:Array<FuBenFanBeiDefUnit>
        // 列表
            LieBiaoUnits ?:Array<FuBenLieBiaoUnit>
        // 多倍奖励加成系数
            jiaChengXiShu ?:number

    }

// 【请求】关卡--扫荡：小关、大关、装备   宝石  附魔石   骑术  副本  超进化
    interface SweepFightNewFuBenReq extends ReqObj {
        // 进行扫荡的静态表（xiaoguan/daguan）中的Id
            nParam ?:number
        //  3、装备副本  4 宝石       5附魔石       6骑术   7超进化
            fType ?:number
    }

// 【反馈】装备副本 宝石 等等几个副本
    interface SweepFightNewFuBenRsp extends RspObj {
        // 奖励列表
            newJiangliList ?:Array<NewFuBenJiangLiInfoUnit>

    }

// 购买副本挑战次数
    interface GouMaiFuBenCiShuReq extends ReqObj {
        // 副本类型
            type ?:number
        // 购买的次数
            num ?:number
    }

// 【反馈】购买副本挑战次数
    interface GouMaiFuBenCiShuRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// 副本扫荡消耗金券
    interface FuBenSaoDangByJinQuanReq extends ReqObj {
        // 副本类型
            type ?:number
        // 进行扫荡的静态表（xiaoguan/daguan）中的Id
            nParam ?:number
        // 扫荡次数
            num ?:number
    }

// 【反馈】副本扫荡消耗金券
    interface FuBenSaoDangByJinQuanRsp extends RspObj {
        // 奖励列表
            newJiangliList ?:Array<NewFuBenJiangLiInfoUnit>

    }

// 【请求】道馆关卡Boss战斗结束后的通知
    interface NoticeDaguanBossFightIsEndReq extends ReqObj {
        // daguan表的静态ID
            defId ?:number
    }

// 【请求】副本通关大奖
    interface LoadInstanceCompleteRewardReq extends ReqObj {
        // 副本类型：2、大关（光明之路）；3、磨炼（友谊之路）；4、天空塔（黑暗之心）
            type ?:number
    }

// 【反馈】副本通关大奖
    interface LoadInstanceCompleteRewardRsp extends RspObj {
        // 副本类型：2、大关（光明之路）；3、磨炼（友谊之路）；4、天空塔（黑暗之心）
            type ?:number
        // 通关大奖列表
            instanceCompleteRewardList ?:Array<InstanceCompleteRewardUnit>
        // 超越的玩家比例，万分比
            beyondPlayerRate ?:number

    }

// 【请求】副本通关大奖
    interface ReceiveInstanceCompleteRewardReq extends ReqObj {
        // 副本类型：2、大关（光明之路）；3、磨炼（友谊之路）；4、天空塔（黑暗之心）
            type ?:number
        // 领奖ID
            id ?:number
    }

// 【反馈】副本通关大奖
    interface ReceiveInstanceCompleteRewardRsp extends RspObj {
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// [请求]红包列表
    interface GetHongBaoListReq extends ReqObj {
    }

// [反馈]红包列表
    interface GetHongBaoListRsp extends RspObj {
        // 红包信息
            hongBaoInfoList ?:Array<HongBaoInfoUnit>
        // 我领取了多少个
            hasGetCount ?:number

    }

// [请求]抢红包
    interface GetHongBaoReq extends ReqObj {
        // 红包的dbId
            hongbaoDbId ?:number
        // 是否一键
            isOneKey ?:boolean
    }

// [返回]抢红包
    interface GetHongBaoRsp extends RspObj {
        // 领取信息
            recordInfoList ?:Array<HongBaoRecordInfoUnit>
        // 抢到了多少 如果是0说明没抢到
            getMoney ?:number

    }

// 【请求】领取邮件奖励
    interface GetEmailRewardReq extends ReqObj {
        // Email静态表ID
            emailDBId ?:number
    }

// 【反馈】领取邮件奖励
    interface GetEmailRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】获取玩家Email的列表
    interface GetEmailListReq extends ReqObj {
        // 是否引导
            isGuide ?:boolean
        // 页签一：系统1；页签二：其他0；页签三：遭遇战2；
            emailType ?:number
    }

// 【反馈】获取玩家Emial的列表
    interface GetEmailListRsp extends RspObj {
        // 玩家Email信息列表
            emailList ?:Array<EmailInfoUnit>
        // 页签一：系统1；页签二：其他0；页签三：遭遇战2；
            emailType ?:number

    }

// [请求]删除邮件
    interface DeleteEmailReq extends ReqObj {
        // 商品Email数据表dbID，数据表主Key
            emailDBID ?:number
    }

// [返回]删除邮件
    interface DeleteEmailRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]删除所有邮件 删除所有已经查看且没有附件的邮件
    interface DeleteAllEmailReq extends ReqObj {
        // 删除类型：（页签一：系统1；页签二：其他=3+4+5；页签三：遭遇战2；）
            delType ?:number
    }

// [返回]删除所有邮件 删除所有已经查看且没有附件的邮件
    interface DeleteAllEmailRsp extends RspObj {
        // 玩家Email信息列表
            emailList ?:Array<EmailInfoUnit>

    }

// [请求]查看邮件
    interface ViewEmailReq extends ReqObj {
        // 商品Email数据表dbID，数据表主Key
            emailDBID ?:number
    }

// [返回]查看邮件
    interface ViewEmailRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]领取所有邮件
    interface GetAllEmailReq extends ReqObj {
        // 页签一：系统1；页签二：其他0；页签三：遭遇战2；
            emailType ?:number
    }

// [返回]领取所有邮件
    interface GetAllEmailRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】向服务器发送聊天信息
    interface SendMessageReq extends ReqObj {
        // 聊天内容中的物品信息（1'道具', 2'装备', 3'武将', 4'阵法', 5'耳朵'）
            messageInfoList ?:Array<ChatMessageInfoUnit>
        // 聊天内容（仅转发给前端使用）
            content ?:string
        // 请求条数 登录的时候需要向中心
            count ?:number
    }

// 【反馈】向服务器发送聊天信息
    interface SendMessageRsp extends RspObj {
        // 是否发送成功
            isSuccess ?:boolean

    }

// 【请求】请求聊天中英雄的信息
    interface GetChatHeroReq extends ReqObj {
        // 聊天记录的DBId
            chatDBId ?:number
        // 英雄DBId
            heroDBId ?:number
        // 英雄静态Id
            wujiangId ?:number
        // 服务器id
            serverId ?:number
        // 需不需要跨服false不需要  true需要 这个值前端不需要
            needCrossServer ?:boolean
    }

// 【反馈】请求聊天中英雄的信息
    interface GetChatHeroRsp extends RspObj {
        // 英雄列表（但长度只是一个）
            heroList ?:Array<HeroInfoUnit>
        // 协作开启状态（内容为前端写好的，直接取出发出即可）
            xiezuo ?:string
        // 服务器id
            serverId ?:number

    }

// 【请求】请求聊天中装备的信息
    interface GetChatZhuangbeiReq extends ReqObj {
        // 聊天记录的DBId
            chatDBId ?:number
        // 装备DBId
            zhuangbeiDBId ?:number
        // 装备静态Id
            zhuangbeiId ?:number
        // 服务器id
            serverId ?:number
        // 需不需要跨服false不需要  true需要 这个值前端不需要
            needCrossServer ?:boolean
    }

// 【反馈】请求聊天中英雄的信息
    interface GetChatZhuangbeiRsp extends RspObj {
        // 获得装备列表（但长度只是一个）
            zhuangbeiList ?:Array<ZhuangbeiInfoUnit>

    }

// 【请求】向所有服务器发送聊天信息
    interface SendAllServerMessageReq extends ReqObj {
        // 聊天内容中的物品信息（1'道具', 2'装备', 3'武将', 4'阵法', 5'耳朵'）
            messageInfoList ?:Array<ChatMessageInfoUnit>
        // 聊天内容（仅转发给前端使用）
            content ?:string
        // 聊天频道 0世界  1本服  2公会 3公告 100 私聊
            channel ?:number
        // 私聊的对方Id，如果其他channel要用，请同时修改注释
            targetId ?:number
    }

// 【反馈】向服务器发送聊天信息
    interface SendAllServerMessageRsp extends RspObj {
        // 是否发送成功
            isSuccess ?:boolean
        // 私聊的对方Id，如果其他channel要用，请同时修改注释
            targetId ?:number

    }

// 【反馈】登录的时候 向服务器请求20条信息 防止玩家进来的时候 聊天窗口是空的
    interface GetMessageAtLoginReq extends ReqObj {
        // 聊天频道 0世界  1本服  2公会 3公告
            channel ?:number
        // 最新一条聊天记录的id
            latestDBID ?:number
    }

// 【反馈】登录的时候 向服务器请求20条信息 防止玩家进来的时候 聊天窗口是空的
    interface GetMessageAtLoginRsp extends RspObj {
        // 回传
            channel ?:number
        // 登录的时候推送20条信息  他的里面套一个ChatMessageInfo
            messageInfoList ?:Array<ChatMessageLoginInfoUnit>
        // 私聊的玩家信息
            personalInfoList ?:Array<PersonalChatInfoUnit>

    }

// 【请求】请求聊天中英雄的信息
    interface GetChatHeroCrossServerReq extends ReqObj {
        // 聊天记录的DBId
            chatDBId ?:number
        // 英雄DBId
            heroDBId ?:number
        // 英雄静态Id
            wujiangId ?:number
        // 服务器id
            serverId ?:number
        // 需不需要跨服false不需要  true需要 这个值前端不需要
            needCrossServer ?:boolean
        // 协作开启状态（内容为前端写好的，直接取出发出即可）
            xiezuo ?:string
        // 玩家id
            playerId ?:number
    }

// 【反馈】请求聊天中英雄的信息
    interface GetChatHeroCrossServerRsp extends RspObj {
        // 英雄列表（但长度只是一个）
            heroList ?:Array<HeroInfoUnit>
        // 协作开启状态（内容为前端写好的，直接取出发出即可）
            xiezuo ?:string
        // 服务器id
            serverId ?:number
        // 玩家id
            playerId ?:number

    }

// 【请求】请求聊天中装备的信息
    interface GetChatZhuangbeiCrossReq extends ReqObj {
        // 聊天记录的DBId
            chatDBId ?:number
        // 装备DBId
            zhuangbeiDBId ?:number
        // 装备静态Id
            zhuangbeiId ?:number
        // 服务器id
            serverId ?:number
        // 需不需要跨服false不需要  true需要 这个值前端不需要
            needCrossServer ?:boolean
        // 玩家id
            playerId ?:number
    }

// 【反馈】请求聊天中英雄的信息
    interface GetChatZhuangbeiCrossRsp extends RspObj {
        // 获得装备列表（但长度只是一个）
            zhuangbeiList ?:Array<ZhuangbeiInfoUnit>
        // 玩家id
            playerId ?:number

    }

// 【请求】举报广告刷屏玩家
    interface ReportAdvertPlayerReq extends ReqObj {
        // 服务器id
            serverId ?:number
        // 玩家id
            playerId ?:number
    }

// 【反馈】举报广告刷屏玩家
    interface ReportAdvertPlayerRsp extends RspObj {
        // 是否重复举报该玩家
            isRepeat ?:boolean

    }

// 用户离开聊天界面
    interface LeaveChatReq extends ReqObj {
        // 是否离开聊天界面
            status ?:boolean
    }

// [请求]聊天界面战斗
    interface ChatFightReq extends ReqObj {
        // 服务器id
            serverId ?:number
        // 目标角色id
            targetPlayerId ?:number
        // 频道类型0：世界聊天，1：本服，2：公会，3：组队副本，5：宅急送
            chatType ?:number
    }

// [返回]聊天界面战斗
    interface ChatFightRsp extends RspObj {
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]膜拜玩家
    interface MoBaiReq extends ReqObj {
        // 排行榜类型：1,等级榜；2,战力榜；3,竞技榜；4,匹配榜；5,遭遇榜
            type ?:number
    }

// [返回]膜拜玩家
    interface MoBaiRsp extends RspObj {
        // 是否成功的标志
            isSuccess ?:boolean
        // 排行榜类型：1,等级榜；2,战力榜；3,竞技榜；4,匹配榜；5,遭遇榜
            type ?:number
        // 剩余可膜拜次数
            mobaiCount ?:number
        // 奖励金币数量
            yinliang ?:number

    }

// 【请求】私聊
    interface EnterPersonalChatReq extends ReqObj {
        // 对方玩家Id
            playerId ?:number
    }

// 【反馈】私聊
    interface EnterPersonalChatRsp extends RspObj {
        // 聊天内容
            messageInfoList ?:Array<ChatMessageLoginInfoUnit>
        // 对方玩家Id
            playerId ?:number

    }

// 【请求】离开私聊
    interface LeavePersonalChatReq extends ReqObj {
    }

// 【反馈】有新的私聊消息到达
    interface PushPersonalChatIsRedRsp extends RspObj {
        // 回传
            channel ?:number
        // 私聊的玩家红点信息
            personalInfo ?:PersonalChatInfoUnit

    }

// 【请求】删除与该玩家的私聊记录
    interface ClearPersonalChatRecordsByPlayerIdReq extends ReqObj {
        // 对方玩家Id
            playerId ?:number
    }

// 【请求】通知前端：删除与该玩家的私聊记录
    interface ClearPersonalChatRecordsByPlayerIdRsp extends RspObj {
        // 玩家Id
            playerId ?:number

    }

// 【请求】更新已读的ChatDBId
    interface UpdatePersonalChatDBIdReq extends ReqObj {
        // 对方玩家Id
            playerId ?:number
        // 最新的已读DB Id
            chatDBId ?:number
    }

// 【请求】通知前端：删除相应聊天记录
    interface PushDeleteChatDbIdInfoRsp extends RspObj {
        // 要删除的聊天信息列表
            deleteChatInfoList ?:Array<DeleteChatInfoUnit>

    }

// [请求]探索副本列表
    interface TanSuoFuBenListReq extends ReqObj {
    }

// [返回]探索副本列表
    interface TanSuoFuBenListRsp extends RspObj {
        // 探索副本列表
            tanSuoFuBenInfoList ?:Array<TanSuoFuBenUnit>
        // 已经生成过的未领取的免费奖励的位置信息
            freeRewardExists ?:Array<TanSuoFuBenRewardUnit>
        // 人物坐标
            personPoint ?:PointUnit
        // 白金贵族以上的当前剩余重置副本次数
            resetCount ?:number

    }

// [请求]探索副本采集奖励信息 
    interface GetCaiJiInfoByVipReq extends ReqObj {
        // 奖励Id
            jiangLiId ?:number
    }

// [返回]探索副本采集奖励信息
    interface GetCaiJiInfoByVipRsp extends RspObj {
        // 每个vip等级对应的倍数上限
            vipBeiShuList ?:Array<ShuXingInfoUnit>

    }

// [请求]开启探索副本
    interface OpenTanSuoFuBenReq extends ReqObj {
        // 副本类型
            fuBenType ?:number
    }

// [返回]开启探索副本
    interface OpenTanSuoFuBenRsp extends RspObj {
        // 副本信息
            fuBenInfo ?:TanSuoFuBenUnit
        // 针对boss副本信息
            otherFuBenInfo ?:TanSuoFuBenUnit

    }

// [请求]更新人物坐标
    interface UpdatePersonPositionReq extends ReqObj {
        // 人物坐标
            personPoint ?:PointUnit
    }

// [返回]更新人物坐标
    interface UpdatePersonPositionRsp extends RspObj {

    }

// [请求]探索副本阵型界面
    interface TanSuoFuBenRoomPanelReq extends ReqObj {
    }

// [返回]阵型界面
    interface TanSuoFuBenRoomPanelRsp extends RspObj {
        // 精灵列表
            heroList ?:Array<GongHuiFuBenRoomHeroUnit>

    }

// [请求]探索副本更换战斗精灵
    interface TanSuoFuBenChangeRoomHeroReq extends ReqObj {
        // 新上阵的精灵ID
            newHeroId ?:number
        // 1 第一个 2 第二个
            position ?:number
    }

// [返回]探索副本更换战斗精灵
    interface TanSuoFuBenChangeRoomHeroRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]探索副本更改房间人员
    interface TanSuoFuBenChangeRoomPersonReq extends ReqObj {
        // 1 踢人 2 自己离开
            type ?:number
        // 踢掉的人
            playerId ?:number
    }

// [返回]探索副本更改房间人员
    interface TanSuoFuBenChangeRoomPersonRsp extends RspObj {
        // 房间状态 1.队长解散 2.队长踢人，3.自己离开，4,战斗正常解散 5，特殊情况解散
            roomStatus ?:number
        // 离开队伍人的Id
            leavePlayerId ?:number

    }

// [请求]探索副本挑战
    interface TanSuoFuBenFightStartReq extends ReqObj {
        // 挑战的副本类型
            fuBenType ?:number
        // StoryLine剧情Db Id
            dbId ?:number
    }

// [返回]探索副本挑战
    interface TanSuoFuBenFightStartRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]探索副本创建或加入房间
    interface TanSuoFuBenCreateOrJoinRoomReq extends ReqObj {
        // 副本类型
            fuBenType ?:number
        // 加入房间需传房间号
            roomId ?:number
    }

// [返回]探索副本创建或加入房间
    interface TanSuoFuBenCreateOrJoinRoomRsp extends RspObj {
        // 自己的房间信息
            roomData ?:TanSuoFuBenRoomInfoUnit
        // 房间状态 1房间失效 2 战力不足
            roomStatus ?:number

    }

// [请求]探索副本采集奖励
    interface TanSuoFuBenCaiJiJiangLiReq extends ReqObj {
        // 奖励Id
            jiangLiId ?:number
        // 几倍采集
            vip ?:number
    }

// [返回]探索副本采集奖励
    interface TanSuoFuBenCaiJiJiangLiRsp extends RspObj {
        // 奖励信息
            jiangliInfo ?:JiangliInfoUnit
        // 宝箱奖励信息
            baoXiangRewardList ?:Array<JiangliInfoUnit>
        // 倍数
            beiShu ?:number
        // 是不是特殊奖励
            isSpecial ?:boolean

    }

// [请求]探索副本一键邀请
    interface TanSuoFuBenInviteOthersReq extends ReqObj {
        // 战力要求
            zhanLiLimit ?:number
    }

// [返回]探索副本一键邀请
    interface TanSuoFuBenInviteOthersRsp extends RspObj {

    }

// [请求]修改探索副本战力要求
    interface UpdateTanSuoZhanLiLimitReq extends ReqObj {
        // 战力要求
            zhanLiLimit ?:number
    }

// [返回]修改探索副本战力要求
    interface UpdateTanSuoZhanLiLimitRsp extends RspObj {

    }

// [返回]探索副本重置推送
    interface PushTanSuoFuBenResetRsp extends RspObj {
        // 是否已近重置
            isReset ?:boolean

    }

// [请求]一键开启探索副本
    interface OpenTanSuoFuBenOneKeyReq extends ReqObj {
        // 采集倍数
            beiShu ?:number
    }

// [返回]一键开启探索副本
    interface OpenTanSuoFuBenOneKeyRsp extends RspObj {
        // 采集的奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]重置探索副本
    interface ResetTanSuoFuBenReq extends ReqObj {
    }

// [返回]重置探索副本
    interface ResetTanSuoFuBenRsp extends RspObj {

    }

// [请求]奇遇面板
    interface QuYuPanelReq extends ReqObj {
    }

// [返回]奇遇面板
    interface QuYuPanelRsp extends RspObj {
        // 奇遇列表信息
            qiyuPanelInfo ?:Array<QiYuPanelInfoUnit>
        // 竞技场次数
            jjcCiShu ?:number
        // 精灵球数量
            jingLingQiu ?:number
        // 已经完成的迷宫数
            wanChengMiGong ?:number
        // 天空塔层数
            tianKongTaCengShu ?:number
        // 装备副本次数
            zhuangBeiFuBenCiShu ?:number
        // 道馆次数
            daoGuanCiShu ?:number
        // 匹配战次数
            piPeiZhanCiShu ?:number

    }

// [请求]获取次数
    interface GetHintCountInfoReq extends ReqObj {
    }

// [返回]获取次数
    interface GetHintCountInfoRsp extends RspObj {
        // 快速战斗
            kuaisuzhandou ?:number
        // 追击神兽剩余次数  迷宫
            migong ?:number
        // 招财剩余次数
            zhaocai ?:number
        // 附魔石副本剩余次数
            fumoshifuben ?:number
        // 宝石副本剩余次数
            baoshifuben ?:number
        // 骑术副本剩余次数
            qishufuben ?:number
        // 羽毛副本剩余次数
            yumaofuben ?:number
        // 超进化
            chaojinhuafuben ?:number
        // 经验果实
            jingyanguoshifuben ?:number
        // 已经充值9元档的次数 注意 是已经冲了的次数  每天限制充值一次
            haschonzhi9yuancishu ?:number

    }

// 【请求】天空塔面板
    interface TianKongTaPanelReq extends ReqObj {
        // 0是默认界面
            zhangJieId ?:number
    }

// 【反馈】天空塔面板
    interface TianKongTaPanelRsp extends RspObj {
        // 接下来要打的章节
            willFightZhangJieId ?:number
        // 接下来要打的boss编号
            willFightBossNum ?:number
        // 小关数据
            fightDataList ?:Array<TianKongTaUnit>
        // 星星奖励领取数据
            starRewardList ?:Array<TianKongTaStarRewardUnit>
        // 红点列表
            redPointList ?:Array<BoolRedPointUnit>
        // 剩余挑战次数
            leftFightCount ?:number
        // 一键剩余次数
            yiJianLeftCount ?:number
        // 第一个的怪物骨骼数据。后面的5个怪物d相同直接用不同则前端须另行获取
            firstOneGuaiwuGuge ?:GugeUnit
        // 当前玩家总星数
            starNumNow ?:number
        // 获得总星数达到N星可继续挑战，返回当前章节的数据
            starNumNeed ?:number

    }

// 【请求】领取天空塔奖励
    interface GetTianKongTaRewardReq extends ReqObj {
        // 章节Id
            zhangJieId ?:number
        // 星星数
            starCount ?:number
    }

// 【反馈】领取天空塔奖励
    interface GetTianKongTaRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】一键扫荡
    interface SweepOneKeyReq extends ReqObj {
        // 1 扫荡奖励预览  2 确认
            type ?:number
    }

// 【反馈】一键扫荡
    interface SweepOneKeyRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>
        // 即将挑战的章节
            willFightZhangJie ?:number
        // 即将挑战的boss编号
            willFightBossNum ?:number
        // 扫荡次数
            saoDangCiShu ?:number
        // 1 扫荡奖励预览  2 确认
            type ?:number
        // 消耗
            costItem ?:JiangliInfoUnit

    }

// 【请求】天空塔战斗
    interface TianKongTaFightReq extends ReqObj {
        // 章节ID
            zhangJieId ?:number
        // Boss编号
            BossNum ?:number
    }

// 【反馈】天空塔战斗
    interface TianKongTaFightRsp extends RspObj {
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】天空塔排行
    interface GetTianKongTaPaiHangReq extends ReqObj {
        // 排行类型
            type ?:number
    }

// 【反馈】天空塔排行
    interface GetTianKongTaPaiHangRsp extends RspObj {
        // 排行列表
            paiHangList ?:Array<TianKongTaPaiHangUnit>
        // 我的排名
            myPosition ?:number
        // 我的星星
            myStar ?:number

    }

// 【请求】宠物手册信息
    interface GetPetHandbookPannelReq extends ReqObj {
    }

// 【反馈】宠物手册信息
    interface GetPetHandbookPannelRsp extends RspObj {
        // 宠物手册列表
            petHandbookList ?:Array<PetHandbookInfoUnit>
        // 当前增加属性列表
            curPropList ?:Array<ShuXingInfoUnit>

    }

// 【请求】激活宠物手册
    interface ActivePetHandbookReq extends ReqObj {
        // defId
            defId ?:number
    }

// 【反馈】激活宠物手册
    interface ActivePetHandbookRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // defId
            defId ?:number
        // 当前增加属性列表
            curPropList ?:Array<ShuXingInfoUnit>
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】宠物羁绊信息
    interface GetPetCombinationPannelReq extends ReqObj {
        // 世代
            generation ?:number
    }

// 【反馈】宠物羁绊信息
    interface GetPetCombinationPannelRsp extends RspObj {
        // 世代
            generation ?:number
        // 宠物信息列表
            petCombinationList ?:Array<PetCombinationUnit>

    }

// 【请求】激活宠物羁绊
    interface ActivePetCombinationReq extends ReqObj {
        // defId，对应jinglingjiban表中id
            defId ?:number
    }

// 【反馈】激活宠物羁绊
    interface ActivePetCombinationRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // defId
            defId ?:number

    }

// 【请求】新版守护兽面板
    interface NewGuardPetPanelReq extends ReqObj {
    }

// 【反馈】新版守护兽面板
    interface NewGuardPetPanelRsp extends RspObj {
        // 守护兽列表
            guardPetList ?:Array<NewGuardPetInfoUnit>
        // 总属性列表
            totalShuXingList ?:Array<ShuXingInfoUnit>
        // 守护兽上阵列表
            positionList ?:Array<NewGuardPetPositionUnit>
        // 洗练属性最高品质，统一的，与属性类型无关
            maxGrade ?:number
        // 该守护兽对应丹药的全部分类：初中高
            danyaoTypeList ?:Array<number>

    }

// 【请求】新版守护兽技能信息
    interface NewGuardPetSkillInfoPanelReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
    }

// 【反馈】新版守护兽技能信息
    interface NewGuardPetSkillInfoPanelRsp extends RspObj {
        // 守护兽头像
            petAdvatar ?:string
        // 技能名称
            skillName ?:string
        // 技能等级
            skillLevel ?:number
        // 技能描述
            skillDesc ?:string
        // 下一级技能描述
            skillNextDesc ?:string
        // 技能升级所需守护兽等级
            unlockLevel ?:number
        // 是否满级
            isFull ?:boolean
        // 消耗列表
            costList ?:Array<JiangliInfoUnit>
        // 攻击目标显示
            attackTarget ?:string
        // 当前技能等级攻防血属性列表
            curShuXingList ?:Array<ShuXingInfoUnit>
        // 当前技能等级攻防血属性列表
            nextShuXingList ?:Array<ShuXingInfoUnit>

    }

// 【请求】新版守护兽升级
    interface GetNewGuardPetLvUpReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
    }

// 【反馈】新版守护兽升级
    interface GetNewGuardPetLvUpRsp extends RspObj {
        // 守护兽DB Id
            dbId ?:number
        // 是否成功
            isSuccess ?:boolean
        // 守护兽信息更新
            guardPetInfo ?:NewGuardPetInfoUnit
        // 当前等级
            curLevel ?:number
        // 消耗列表
            costList ?:Array<JiangliInfoUnit>
        // 总属性列表
            totalShuXingList ?:Array<ShuXingInfoUnit>

    }

// 【请求】新版守护兽技能升级
    interface GetNewGuardPetSkillLvUpReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
    }

// 【反馈】新版守护兽技能升级
    interface GetNewGuardPetSkillLvUpRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【请求】新版守护兽上阵交换
    interface NewGuardPetPositionChangeReq extends ReqObj {
        // 位置
            position ?:number
        // 新上阵的守护兽DB Id
            dbId ?:number
    }

// 【反馈】新版守护兽上阵交换
    interface NewGuardPetPositionChangeRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 新的守护兽上阵列表
            positionList ?:Array<NewGuardPetPositionUnit>

    }

// 【请求】唤醒守护兽技能
    interface ActiveGuardPetSkillReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
        // 唤醒方式：1、达到玩家等级唤醒；2、消耗道具唤醒
            type ?:number
    }

// 【反馈】唤醒守护兽技能
    interface ActiveGuardPetSkillRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【请求】新版守护兽进阶面板
    interface GetNewGuardPetJinjiePanelReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
    }

// 【反馈】新版守护兽进阶面板
    interface GetNewGuardPetJinjiePanelRsp extends RspObj {
        // 守护兽进阶属性和消耗
            info ?:NewGuardPetJinjieInfoUnit

    }

// 【请求】新版守护兽进阶升级
    interface GetNewGuardPetJinjieUpgradeReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
    }

// 【反馈】新版守护兽进阶升级
    interface GetNewGuardPetJinjieUpgradeRsp extends RspObj {
        // 守护兽进阶属性和消耗
            info ?:NewGuardPetJinjieInfoUnit

    }

// 【请求】新版守护兽洗练面板
    interface GetNewGuardPetXilianPanelReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
    }

// 【反馈】新版守护兽洗练面板
    interface GetNewGuardPetXilianPanelRsp extends RspObj {
        // 属性列表
            propertyInfoList ?:Array<XiLianPropUnit>
        // 洗练消耗列表
            costList ?:Array<JiangliInfoUnit>
        // 洗练属性最高品质，统一的，与属性类型无关
            maxGrade ?:number

    }

// 【请求】新版守护兽进阶洗练
    interface GetNewGuardPetXilianReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
        // 洗练的属性编号，从1开始编号，目前最大8条属性
            propNum ?:number
    }

// 【反馈】新版守护兽进阶洗练
    interface GetNewGuardPetXilianRsp extends RspObj {
        // 属性列表
            propertyInfoList ?:Array<XiLianPropUnit>

    }

// 【请求】新版守护兽遣散
    interface NewGuardPetSendBackReq extends ReqObj {
        // 回收的守护兽DB Id list
            dbIdList ?:Array<DBIdAndIntListUnit>
        // 是否遣散回收预览
            isPreview ?:boolean
    }

// 【反馈】新版守护兽遣散
    interface NewGuardPetSendBackRsp extends RspObj {
        // 遣散奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 是否遣散回收预览
            isPreview ?:boolean

    }

// 【请求】新版守护兽吃丹面板
    interface GetNewGuardPetDanyaoPanelReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
        // 丹药的分类：初中高
            danyaoType ?:number
    }

// 【反馈】新版守护兽吃丹面板
    interface GetNewGuardPetDanyaoPanelRsp extends RspObj {
        // 吃丹药的信息
            danyaoInfo ?:NewGuardPetDanyaoInfoUnit

    }

// 【请求】新版守护兽吃丹药
    interface GetNewGuardPetEatDanyaoReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
        // 丹药的分类：初中高
            danyaoType ?:number
        // 吃丹数量
            count ?:number
    }

// 【反馈】新版守护兽吃丹药
    interface GetNewGuardPetEatDanyaoRsp extends RspObj {
        // 吃丹药的信息
            danyaoInfo ?:NewGuardPetDanyaoInfoUnit

    }

// 【反馈】新版守护兽的红点推送
    interface PushNewGuardPetRedPointRsp extends RspObj {
        // 单个守护兽的红点推送
            guardPetRedPointInfoList ?:Array<NewGuardPetRedPointInfoUnit>

    }

// 【请求】新守护兽升星额外属性描述
    interface GetNewGuardPetExtraDescInfoReq extends ReqObj {
        // 守护兽ID
            defId ?:number
    }

// 【反馈】新守护兽升星额外属性描述
    interface GetNewGuardPetExtraDescInfoRsp extends RspObj {
        // 升星额外属性描述列表
            extraDescInfoList ?:Array<NewGuardPetExtraDescInfoUnit>

    }

// 【请求】新手攻略战力提升
    interface GetStrategyFightReq extends ReqObj {
        // 查看的神魔位置，11-16，如果没传，默认为主阵容第一个有效位置
            pos ?:number
    }

// 【反馈】新手攻略战力提升
    interface GetStrategyFightRsp extends RspObj {
        // 查看的神魔位置，11-16，如果没传，默认为主阵容第一个有效位置
            pos ?:number
        // 本服第一名总战力
            maxFight ?:number
        // 提升列表
            promoteList ?:Array<StrategyFightUnit>

    }

// 【请求】新手攻略热度阵容进阶阵容
    interface GetStrategyRecommendReq extends ReqObj {
        // 1、热度阵容；2、进阶阵容；
            type ?:number
    }

// 【反馈】新手攻略热度阵容进阶阵容
    interface GetStrategyRecommendRsp extends RspObj {
        // 推荐列表
            recommendList ?:Array<StrategyRecommendUnit>

    }

// 【请求】新手攻略大神风采
    interface GetStrategyFirstReq extends ReqObj {
    }

// 【反馈】新手攻略大神风采
    interface GetStrategyFirstRsp extends RspObj {
        // 提升列表
            promoteList ?:Array<StrategyRecommendUnit>
        // 本服第一
            firstInfo ?:StrategyFirstUnit

    }

// 【请求】通过道具激活守护兽付费技能
    interface ActiveNewGuardPetPaySkillReq extends ReqObj {
        // 守护兽DB Id
            dbId ?:number
        // 守护兽付费技能表Id
            defId ?:number
    }

// 【反馈】通过道具激活守护兽付费技能
    interface ActiveNewGuardPetPaySkillRsp extends RspObj {
        // 守护兽DB Id
            dbId ?:number
        // 守护兽付费技能表Id
            defId ?:number
        // 是否激活
            isActive ?:boolean

    }

// 跨服的战队PK信息
    interface ZhanDuiRemotePKInfosReq extends ReqObj {
    }

// 跨服的战队PK信息
    interface ZhanDuiRemotePKInfosRsp extends RspObj {
        // 自己所属战队id
            zhanDuiId ?:number
        // 每局的间隔时间(s)
            intervalTime ?:number
        // 开赛前多久不能支持队伍(s)
            zhiChiLimitTime ?:number
        // 开赛前多久不能更新阵容(s)
            editPosLimitTime ?:number
        // 是否报过名
            isBaoMing ?:boolean
        // 是否在64强内
            isIn64 ?:boolean
        // 是否是总决
            isFinal ?:boolean
        // 是否能调整阵型(3-4季军赛时,决赛队伍里的队伍信息)
            finalFightGroupZhanDuiInfo ?:ZhanDuiSimpleInfoUnit
        // 对战开始时间
            startTime ?:number
        // 多少强的比赛(64强)
            roundDes ?:string
        // 前一场的战斗结束时间
            preBattleOverTime ?:number
        // 前一场战斗的战队pk赛的队伍对战信息
            preZhanDuiPKGroup ?:ZhanDuiPKGroupUnit
        // 战队pk赛的队伍对战信息
            zhanDuiPKGroups ?:Array<ZhanDuiPKGroupUnit>
        // 战队pk赛的队伍支持情况
            ZhanDuiPKGroupZhiChis ?:Array<ZhanDuiPKGroupZhiChiUnit>

    }

// 打开支持战队界面
    interface OpenZhiChiZhanDuiReq extends ReqObj {
        // pk的战队组id
            pkGroupId ?:number
    }

// 打开支持战队界面
    interface OpenZhiChiZhanDuiRsp extends RspObj {
        // 左边战队上一轮的战斗日志ids
            leftFightLogIds ?:Array<ZhanDuiPlayerFightLogInfoUnit>
        // 右边战队上一轮的战斗日志ids
            rightFightLogIds ?:Array<ZhanDuiPlayerFightLogInfoUnit>

    }

// 打开支持日志界面
    interface OpenZhiChiLogReq extends ReqObj {
    }

// 打开支持日志界面
    interface OpenZhiChiLogRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number
        // 钻石总收益
            zuanshi ?:number
        // 金卷总收益
            jinjuan ?:number
        // 当前比赛支持花费记录
            zhanDuiZhiChiLogs ?:Array<ZhanDuiZhiChiLogUnit>
        // 历史比赛支持营收记录
            histroyZhiChiRevenue ?:Array<ZhanDuiZhiChiLogUnit>

    }

// 历史战绩日志
    interface HistroyFightLogReq extends ReqObj {
        // 战队Id
            zhanduiId ?:number
    }

// 历史战绩日志
    interface HistroyFightLogRsp extends RspObj {
        // 是否胜利
            isWin ?:boolean
        // 战斗日志信息
            histroyLogs ?:HistroyLogUnit

    }

// 战队支持
    interface ZhanDuiZhiChiReq extends ReqObj {
        // pk的两战队组id
            pkGroupId ?:number
        // 战队id
            zhanDuiId ?:number
        // 支持声望数量
            shengwang ?:number
    }

// 战队支持
    interface ZhanDuiZhiChiRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 战斗回放
    interface FightReplayReq extends ReqObj {
        // 战队id
            zhanDuiId ?:number
        // 战斗日志id
            fightLogId ?:number
    }

// 战队支持
    interface FightReplayRsp extends RspObj {
        // 战队id
            zhanDuiId ?:number
        // 战斗日志
            fightLog ?:string

    }

// 本地的战队PK信息
    interface ZhanDuiLocalPKInfosReq extends ReqObj {
    }

// 本地的战队PK信息
    interface ZhanDuiLocalPKInfosRsp extends RspObj {
        // 对战开始时间
            startTime ?:number
        // 自己所属战队
            zhanDuiId ?:number
        // 是否报过名
            isBaoMing ?:boolean
        // A组战队pk赛的队伍对战信息
            aZhanDuiPKGroups ?:Array<ZhanDuiPKGroupUnit>
        // B组战队pk赛的队伍对战信息
            bZhanDuiPKGroups ?:Array<ZhanDuiPKGroupUnit>
        // C组战队pk赛的队伍对战信息
            cZhanDuiPKGroups ?:Array<ZhanDuiPKGroupUnit>

    }

// 修改战队成员出战位置
    interface UpdateRemoteZhanDuiPositionReq extends ReqObj {
        // 修改后的战斗序列
            playerIdList ?:Array<number>
    }

// 修改战队成员出战位置
    interface UpdateRemoteZhanDuiPositionRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 检查跨服战队赛状态
    interface CheckZhanDuiSaiStatusReq extends ReqObj {
    }

// 检查跨服战队赛状态
    interface CheckZhanDuiSaiStatusRsp extends RspObj {
        // 是否开启
            status ?:boolean

    }

// 获取前三名的信息
    interface GetTopThreeZhanDuiInfoReq extends ReqObj {
    }

// 获取前三名的信息
    interface GetTopThreeZhanDuiInfoRsp extends RspObj {
        // 第几届
            season ?:number
        // 冠军玩家所在服务器编号
            serverNum ?:number
        // 钻石收益
            zuanshi ?:number
        // 金卷收益
            jinjuan ?:number
        // 前三战队信息
            top3ZhanDuiInfo ?:Array<ZhanDuiSimpleInfoUnit>

    }

// 查看玩家阵型
    interface ShowPlayerZhenXingReq extends ReqObj {
        // 玩家id
            playerId ?:number
        // 第几队
            position ?:number
    }

// 查看玩家阵型
    interface ShowPlayerZhenXingRsp extends RspObj {
        // 当前阵型
            curZhenXing ?:ZDSZhenXingInfoUnit
        // 上一场阵型
            oldZhenXing ?:ZDSZhenXingInfoUnit
        // 第几队
            position ?:number

    }

// [请求]z力量面板
    interface ZLiLiangPanelReq extends ReqObj {
    }

// [返回]z力量面板
    interface ZLiLiangPanelRsp extends RspObj {
        // 激活的神之力
            defIdList ?:Array<number>
        // 激活的魔之力
            defIdWithItemList ?:Array<number>

    }

// [请求]使用道具激活魔之力Z力量
    interface ActiveZliliangWithItemReq extends ReqObj {
        // 待激活的Z力量
            defId ?:number
    }

// [返回]使用道具激活魔之力Z力量
    interface ActiveZliliangWithItemRsp extends RspObj {
        // 是佛激活成功
            isSuccess ?:boolean

    }

// 【请求】获取十连抽1列表
    interface GetShilianChouListReq extends ReqObj {
        // 活动dbId
            activityId ?:number
    }

// 【反馈】获取十连抽1列表
    interface GetShilianChouListRsp extends RspObj {
        // 获取十连抽1列表
            shilianChou1List ?:Array<ActivityDetailInfoUnit>
        // 累计十连抽次数
            totalCount ?:number

    }

// 【请求】领取获取十连抽奖励
    interface DoGetShilianChouReq extends ReqObj {
        // 静态表ID
            defId ?:number
        // 活动dbId
            activityId ?:number
    }

// 【反馈】领取获取十连抽奖励
    interface DoGetShilianChouRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】兑换元宵红包
    interface DuihuanHongbaoReq extends ReqObj {
    }

// 【反馈】兑换元宵红包
    interface DuihuanHongbaoRsp extends RspObj {
        // 是否兑换成功
            isSuccess ?:boolean

    }

// 【请求】获取大转盘信息
    interface GetDzpInfoReq extends ReqObj {
        // 活动dbId
            activityId ?:number
    }

// 【反馈】获取大转盘信息
    interface GetDzpInfoRsp extends RspObj {
        // 上次刷新时间
            refreshTime ?:number
        // 免费刷新次数
            freeRefreshCount ?:number
        // 免费抽取次数
            freeGetCount ?:number
        // 当前奖池抽取次数
            count ?:number
        // 价格
            jiage ?:number
        // 每静态表ID,当奖励是银两宝石时该值为0
            jlId1 ?:number
        // 1: 道具 2：武将 3：装备 4：金元宝 5 银元宝 6 战阵
            type1 ?:number
        // 数量
            count1 ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
            doGet1 ?:number
        // 每静态表ID,当奖励是银两宝石时该值为0
            jlId2 ?:number
        // 1: 道具，2：武将 3：装备 4：金元宝 5 银元宝 6 战阵
            type2 ?:number
        // 数量
            count2 ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
            doGet2 ?:number
        // 每静态表ID,当奖励是银两宝石时该值为0
            jlId3 ?:number
        // 1: 道具 2：武将 3：装备 4：金元宝 5 银元宝 6 战阵
            type3 ?:number
        // 数量
            count3 ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
            doGet3 ?:number
        // 每静态表ID,当奖励是银两宝石时该值为0
            jlId4 ?:number
        // 1: 道具 2：武将 3：装备 4：金元宝 5 银元宝 6 战阵
            type4 ?:number
        // 数量
            count4 ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
            doGet4 ?:number
        // 每静态表ID,当奖励是银两宝石时该值为0
            jlId5 ?:number
        // 1: 道具 2：武将 3：装备 4：金元宝 5 银元宝 6 战阵
            type5 ?:number
        // 数量
            count5 ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
            doGet5 ?:number
        // 每静态表ID,当奖励是银两宝石时该值为0
            jlId6 ?:number
        // 1: 道具 2：武将 3：装备 4：金元宝 5 银元宝 6 战阵
            type6 ?:number
        // 数量
            count6 ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
            doGet6 ?:number
        // 每静态表ID,当奖励是银两宝石时该值为0
            jlId7 ?:number
        // 1: 道具 2：武将 3：装备 4：金元宝 5 银元宝 6 战阵
            type7 ?:number
        // 数量
            count7 ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
            doGet7 ?:number
        // 每静态表ID,当奖励是银两宝石时该值为0
            jlId8 ?:number
        // 1: 道具 2：武将 3：装备 4：金元宝 5 银元宝 6 战阵
            type8 ?:number
        // 数量
            count8 ?:number
        // 是否已经领取奖励：0、未领取；1、已领取；
            doGet8 ?:number

    }

// 【请求】抽取大转盘奖励
    interface GetDzpRewardReq extends ReqObj {
        // 活动dbId
            activityId ?:number
    }

// 【反馈】抽取大转盘奖励
    interface GetDzpRewardRsp extends RspObj {
        // 获取了第几个奖励：1-8
            index ?:number
        // 
            count ?:number
        // 
            jiage ?:number

    }

// 【请求】重置大转盘
    interface ResetDzpReq extends ReqObj {
        // 活动dbId
            activityId ?:number
    }

// 【反馈】重置大转盘
    interface ResetDzpRsp extends RspObj {
        // 是否重置成功
            isSuccess ?:boolean

    }

// [请求]魔法骰子面板
    interface MoFaShaiZiPanelReq extends ReqObj {
        // 活动id
            activityId ?:number
    }

// [返回]魔法骰子面板
    interface MoFaShaiZiPanelRsp extends RspObj {
        // 面板展示多少个道具 仅仅是面板
            jiangliList ?:Array<MoFaShaiZiInfoUnit>
        // 上次掷到的位置
            lastPosition ?:number
        // 普通骰子剩余个数
            commonLeftCount ?:number
        // 遥控骰子剩余个数
            superLeftCount ?:number
        // 遥控骰子道具ID
            superId ?:number
        // 今天是否已经获取了遥控骰子
            isSuper ?:boolean

    }

// [请求]掷骰子
    interface GetShaiZiJiangLiReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 0 普通   1 遥控
            type ?:number
        // 遥控骰子想要掷出的点数
            point ?:number
    }

// [返回]掷骰子
    interface GetShaiZiJiangLiRsp extends RspObj {
        // 本次掷出的点数
            point ?:number
        // 本次掷出的奖励
            getjiangli ?:JiangliInfoUnit

    }

// [请求]公会排行
    interface GongHuiPaiHangReq extends ReqObj {
        // 0排名  1推荐
            type ?:number
        // 第几页
            pageNumber ?:number
    }

// [返回]公会排行
    interface GongHuiPaiHangRsp extends RspObj {
        // 惩罚时间  单位:分钟
            chengFaTime ?:number
        // 好友列表
            gongHuiListInfo ?:Array<GongHuiListInfoUnit>
        // 一共有多少公会
            totelCount ?:number
        // 共多少页
            pageCount ?:number

    }

// [请求]创建公会
    interface CreateGongHuiReq extends ReqObj {
        // 工会的名字
            name ?:string
        // 公会等级 默认为1
            gongHuiLevel ?:number
        // 徽章边框
            huiZhangBianKuang ?:number
        // 徽章底图
            huiZhangDitu ?:number
        // 徽章标志
            huiZhangBiaoZhi ?:number
    }

// [返回]创建公会
    interface CreateGongHuiRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]申请公会
    interface ApplyForGongHuiReq extends ReqObj {
        // 工会的dbId
            id ?:number
    }

// [返回]创建公会
    interface ApplyForGongHuiRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]查找公会
    interface FindGongHuiReq extends ReqObj {
        // 公会name
            name ?:string
    }

// [返回]查找公会
    interface FindGongHuiRsp extends RspObj {
        // 公会列表
            gongHuiListInfo ?:Array<GongHuiListInfoUnit>

    }

// [返回]玩家的公会id 如果玩家在线 给他推送
    interface WanJiaGongHuiIdRsp extends RspObj {
        // 公会id
            dbId ?:number
        // 给不给提示 true 给     false不给
            flg ?:boolean
        // 职位
            zhiWei ?:number
        // 是不是在公会道馆坑中
            inGongHuiDaoGuan ?:boolean
        // 公会名字
            gongHuiName ?:string
        // 公会徽章 边框,底图,标志
            gongHuiHuiZhang ?:string
        // 公会等级
            gongHuiLevel ?:number

    }

// [请求]公会主面板
    interface GongHuiPanelReq extends ReqObj {
    }

// [返回]查公会主面板
    interface GongHuiPanelRsp extends RspObj {
        // 公会宣言
            xuanYan ?:string
        // 
            zhiWei ?:number
        // 公会等级
            gongHuiLevel ?:number
        // 公会名字
            gongHuiName ?:string
        // 公会人数
            gongHuiMemberCount ?:number
        // 公会经验
            exp ?:number
        // 会长名字
            huiZhangMingZi ?:string
        // 会长头像
            huiZhangTouXiang ?:number
        // 会长竞技场称号
            huiZhangJJCChengHao ?:number
        // 上次发邮件的时间
            timeJianGe ?:number
        // 加入公会的时间 单位秒
            joinGongHuiTime ?:number
        // 攻坚战可以打的时间点 单位秒 攻坚战的时间可以单独配置 这个时间做少开启一个
            openGongJianZhanTime ?:number
        // 是否第一次加入公会
            isFirst ?:boolean
        // 新版会长个人形象数据
            huizhangPhoto ?:PlayerPhotoUnit

    }

// [请求]编辑宣言
    interface EditXuanYanReq extends ReqObj {
        // 公会宣言
            content ?:string
    }

// [返回]编辑宣言
    interface EditXuanYanRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]获取成员列表
    interface GetMembersListReq extends ReqObj {
        // 工会的dbId
            id ?:number
        // 1默认；2按照活跃度排序
            sortType ?:number
    }

// [返回]获取成员列表
    interface GetMembersListRsp extends RspObj {
        // 成员列表
            gongHuiMemberInfoList ?:Array<GongHuiMemberInfoUnit>
        // 这个玩家 加入工会的时间
            joinTime ?:number
        // 是否NPC会长
            isNpcPresident ?:boolean
        // 1默认；2按照活跃度排序
            sortType ?:number

    }

// [请求]修改职位
    interface ChangeZhiWeiReq extends ReqObj {
        // 成员id
            memberId ?:number
        // 踢出公会 传-1
            ZhiWei ?:number
    }

// [返回]获取成员列表
    interface ChangeZhiWeiRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]公会的申请列表
    interface RequestListReq extends ReqObj {
    }

// [返回]公会的申请列表
    interface RequestListRsp extends RspObj {
        // 成员列表
            gongHuiMemberInfoList ?:Array<GongHuiMemberInfoUnit>
        // 0自动加入  1批准加入 权限 会长 副会长
            joinType ?:number

    }

// [请求]同意申请
    interface ApplyRequestReq extends ReqObj {
        // 
            id ?:number
        // 0同意  1拒绝 2全部拒绝
            type ?:number
    }

// [返回]同意申请
    interface ApplyRequestRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]大厅信息
    interface DaTingInfoReq extends ReqObj {
    }

// [返回]大厅信息
    interface DaTingInfoRsp extends RspObj {
        // 建设度
            jianSheDu ?:number
        // 公会等级
            gongHuiLevel ?:number
        // 公会币
            gongHuiBi ?:number
        // 今日建设人数
            jianSheToday ?:number
        // 成员数量
            memberCount ?:number
        // 今日已经通用建设次数
            hasJianSheCiShu ?:number
        // 今日已经精灵建设次数
            hasJingLingCiShu ?:number
        // 捐赠列表
            longList ?:Array<GongHuiHuoYueLogInfoUnit>
        // 本公会今天已经建设的总次数
            gongHuiHasJianSheTimes ?:number
        // 本公会今天已经捐精灵的总次数
            gongHuiHasJuanJingLingTimes ?:number
        // 公会活跃任务列表
            missionList ?:Array<MissionInfoUnit>
        // 整个公会的当前活跃度
            activityPoint ?:number
        // 公会活跃奖励列表
            activityPointRewardList ?:Array<GongHuiActivityPointRewardUnit>
        // 是否今日更换过公会
            isLeaveGonghuiToday ?:boolean

    }

// [请求]建设工会
    interface JianSheReq extends ReqObj {
        // 1 2 3 代表钱 4代表精灵
            type ?:number
        // 精灵dbId
            dbId ?:number
    }

// [返回]建设工会
    interface JianSheRsp extends RspObj {
        // 建设度
            jianSheDu ?:number
        // 公会等级
            gongHuiLevel ?:number
        // 科技币
            keJiBi ?:number
        // 公会币
            gongHuiBi ?:number
        // 当前活跃
            currHuoYue ?:number
        // 今日建设人数
            jianSheToday ?:number
        // 成员数量
            memberCount ?:number
        // 活跃宝箱1的领取状态 true可以领  false 领过了 
            huoYueJiangLi1 ?:boolean
        // 活跃宝箱2的领取状态 true可以领  false 领过了 
            huoYueJiangLi2 ?:boolean
        // 活跃宝箱3的领取状态 true可以领  false 领过了 
            huoYueJiangLi3 ?:boolean
        // 今日已经通用建设次数
            hasJianSheCiShu ?:number
        // 今日已经精灵建设次数
            hasJingLingCiShu ?:number
        // 捐赠列表
            longList ?:Array<GongHuiHuoYueLogInfoUnit>
        // 本公会今天已经建设的总次数
            gongHuiHasJianSheTimes ?:number
        // 本公会今天已经捐精灵的总次数
            gongHuiHasJuanJingLingTimes ?:number

    }

// [请求]领取公会活跃奖励
    interface GetGongHuiActivityPointRewardReq extends ReqObj {
        // 活跃奖励Id
            defId ?:number
    }

// [返回]领取公会活跃奖励
    interface GetGongHuiActivityPointRewardRsp extends RspObj {
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// [请求]改变加入
    interface ChangeJoinTypeReq extends ReqObj {
        // 0自动加入  1批准加入 权限 会长 副会长
            joinType ?:number
    }

// [返回]改变加入
    interface ChangeJoinTypeRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]公会商店列表 主要返回那个商品能不能买 商店商品有每日限购
    interface GongHuiShangDianListReq extends ReqObj {
    }

// [返回]公会商店列表 主要返回那个商品能不能买 商店商品有每日限购
    interface GongHuiShangDianListRsp extends RspObj {
        // 公会币
            gonghuibi ?:number
        // 商品
            infoList ?:Array<GongHuiShangDianInfoUnit>

    }

// [请求]购买
    interface BuyGongHuiShangDianReq extends ReqObj {
        // 商品
            defId ?:number
        // 储量
            count ?:number
    }

// [返回]购买
    interface BuyGongHuiShangDianRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// [请求]弹劾会长
    interface TanHeReq extends ReqObj {
        // 
            leaderId ?:number
    }

// [返回]弹劾会长
    interface TanHeRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]公会福利列表
    interface FuLiBaoXiangListReq extends ReqObj {
    }

// [返回]公会福利列表
    interface FuLiBaoXiangListRsp extends RspObj {
        // 集体科技列表
            gongHuiFuLiInfoList ?:Array<GongHuiFuLiInfoUnit>

    }

// [请求]获取福利并加载领取历史记录
    interface GetFuLiAndLogReq extends ReqObj {
        // 福利DbId
            dbId ?:number
        // 是否是个人发送的福利宝箱
            leiXing ?:number
    }

// [返回]获取福利并加载领取历史记录
    interface GetFuLiAndLogRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 奖励列表
            gongHuiFuLiLogInfoList ?:Array<GongHuiFuLiLogInfoUnit>

    }

// [请求]群发邮件
    interface QunFaEmailReq extends ReqObj {
        // 奖励列表
            content ?:string
    }

// [返回]群发邮件
    interface QunFaEmailRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]公会改名
    interface ChangeGongHuiNameReq extends ReqObj {
        // 公会新的名字
            newName ?:string
    }

// [返回]公会改名
    interface ChangeGongHuiNameRsp extends RspObj {
        // 是否更改成功
            isSuccess ?:boolean
        // 公会新的名字
            newName ?:string

    }

// [请求]发送公会福利
    interface SendGongHuiFuLiReq extends ReqObj {
    }

// [返回]发送公会福利
    interface SendGongHuiFuLiRsp extends RspObj {
        // 发送福利者奖励
            JiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]福利奖励预览
    interface ShowFuLiJiangLiReq extends ReqObj {
    }

// [返回]福利奖励预览
    interface ShowFuLiJiangLiRsp extends RspObj {
        // 发送福利者奖励
            sendJiangLiList ?:Array<JiangliInfoUnit>
        // 领取福利者奖励
            getJiangLiList ?:Array<JiangliInfoUnit>
        // 今日发送剩余次数
            leftCount ?:number
        // 消耗金券数量
            spendCount ?:number

    }

// [请求]签到
    interface GongHuiSginReq extends ReqObj {
    }

// [反馈]签到
    interface GongHuiSginRsp extends RspObj {
        // 签到奖励
            gains ?:Array<JiangliInfoUnit>

    }

// [请求]签到信息
    interface GongHuiSginInfoReq extends ReqObj {
    }

// [反馈]签到信息
    interface GongHuiSginInfoRsp extends RspObj {
        // 签到奖励
            gains ?:Array<JiangliInfoUnit>
        // 签到状态
            sign ?:boolean

    }

// 【请求】选择公会徽章
    interface ChooseGongHuiHuiZhangReq extends ReqObj {
        // 选择列表
            chooseList ?:Array<ShuXingInfoUnit>
    }

// 【反馈】选择公会徽章
    interface ChooseGongHuiHuiZhangRsp extends RspObj {
        // 是否选择成功
            isSuccess ?:boolean

    }

// 【请求】一键申请公会
    interface ApplyGongHuiOneKeyReq extends ReqObj {
    }

// 【反馈】一键申请公会
    interface ApplyGongHuiOneKeyRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【请求】公会拍卖商品信息
    interface GongHuiPaiMaiInfoListReq extends ReqObj {
    }

// 【反馈】公会拍卖商品信息
    interface GongHuiPaiMaiInfoListRsp extends RspObj {
        // 公会所有拍卖品集合
            gongHuiPaiMaiInfoList ?:Array<GongHuiPaiMaiInfoUnit>

    }

// 【请求】购买公会拍卖商品
    interface GouMaiPaiMaiInfoReq extends ReqObj {
        // 购买公会拍卖品dbId
            gouMaiDbId ?:number
    }

// 【反馈】购买公会拍卖商品
    interface GouMaiPaiMaiInfoRsp extends RspObj {
        // 购买公会拍卖品dbId
            gouMaiDbId ?:number

    }

// 【请求】购买公会拍卖商品日志
    interface GouMaiRiZhiListReq extends ReqObj {
        // 购买公会拍卖品dbId
            gouMaiDbId ?:number
    }

// 【反馈】购买公会拍卖商品日志
    interface GouMaiRiZhiListRsp extends RspObj {
        // 公会所有拍卖品购买日志集合
            gongHuiPaiMaiGouMaiList ?:Array<string>

    }

// 【反馈】公会拍卖品推送
    interface PushGongHuiPaiMaiInfoRsp extends RspObj {
        // 1 - 新增， 2 - 被购买
            type ?:number
        // 公会拍卖品信息
            paiMaiItemInfo ?:GongHuiPaiMaiInfoUnit

    }

// 【反馈】公会守护面板
    interface GetGongHuiShouHuPanelReq extends ReqObj {
    }

// 【反馈】公会守护面板
    interface GetGongHuiShouHuPanelRsp extends RspObj {
        // boss形象
            bossImage ?:string
        // 掉落奖励
            rewardList ?:Array<JiangliInfoUnit>
        // 当前报名人数
            curNum ?:number
        // 战斗开始时间
            fightStartTime ?:number
        // 战斗结束时间
            fightEndTime ?:number
        // 发奖时间
            rewardTime ?:number
        // 是否已经报名
            isBaoMing ?:boolean

    }

// 【反馈】公会守护报名
    interface GetGongHuiShouHuBaoMingReq extends ReqObj {
    }

// 【反馈】公会守护报名
    interface GetGongHuiShouHuBaoMingRsp extends RspObj {

    }

// 【反馈】公会守护掷骰子
    interface GetGongHuiShouHuTouZiReq extends ReqObj {
    }

// 【反馈】公会守护掷骰子
    interface GetGongHuiShouHuTouZiRsp extends RspObj {
        // 点数
            dianShu ?:number

    }

// 【反馈】公会守护状态
    interface PushShouHuGongHuiStatusRsp extends RspObj {
        // 开始战斗时间
            fightStartTime ?:number
        // 战斗结束时间
            fightEndTime ?:number
        // 掷骰子结束时间
            rewardTime ?:number
        // 是否已经扔过骰子
            isHaveTouZi ?:boolean

    }

// 【请求】秘法证书面板信息
    interface MifaZhengshuPanelReq extends ReqObj {
    }

// 【反馈】秘法证书面板信息
    interface MifaZhengshuPanelRsp extends RspObj {
        // 是否已经激活
            isActive ?:boolean
        // 未激活时的可激活方式：1新服专享激活；2指定充值项目激活
            activeType ?:number
        // 新服专享ProjectId，activeType=1 时赋值
            projectId ?:number
        // 新服专享赠送的激活道具信息，activeType=1 时赋值，否则为undefined
            activeItem ?:JiangliInfoUnit
        // 充值金额表Id，activeType=2 时赋值
            chongzhiJineDefId ?:number
        // 总人数
            totalCount ?:number
        // 本公会当前已经激活的人数
            activeCount ?:number
        // 神魔战场技能瞬间伤害值.单位:秒
            skillAttack ?:number
        // 神魔战场技能冷却.单位:秒
            skillCd ?:number
        // 神魔战场技能冷却.单位:秒
            descList ?:Array<string>

    }

// 【请求】获得玩家装备列表
    interface GetPlayerZhuangbeiListReq extends ReqObj {
    }

// 【反馈】获得玩家装备列表
    interface GetPlayerZhuangbeiListRsp extends RspObj {
        // 玩家获得装备列表
            zhuangbeiList ?:Array<ZhuangbeiInfoUnit>

    }

// 【请求】玩家装备变化
    interface ChangeZhuangbeiReq extends ReqObj {
        // 装备Db ID
            zhuangbeiDbID ?:number
        // 1 - 强化， 2 - 进阶 3 - 附魔 4 - 镶嵌(装备强化已经不在这个接口中实现了)  5-在装备上进行宝石合成 6 升星
            changeType ?:number
        // 1 - 装备， 2 - 宝石 （该参数仅在宝石强化时有效） 附魔时 1是装备 2 是附魔石 10 升星消耗胚子 11 消耗道具1 12 消耗道具2
            qianghuaType ?:number
        // 【镶嵌】 - 宝石的dbID 0 - 表示卸下 其他地方目前没有发现使用的
            objectID ?:number
        // 该参数仅【镶嵌】有效，确定镶嵌宝石的位置
            posIndex ?:number
        // 0 - 指定附魔石数量， 1 - 一键附魔【仅在附魔操作时使用】
            isFSingleZhuangbei ?:number
        // 【升星】 - 所需装备Db Id List
            costZhuangbeiIdList ?:DbIdListInfoUnit
    }

// 【反馈】玩家装备变化
    interface ChangeZhuangbeiRsp extends RspObj {
        // 剩余银两
            yinLiang ?:number
        // 当前强化装备的全部信息
            zhuangbeiInfo ?:ZhuangbeiInfoUnit
        // 新附魔属性列表
            propertyInfoList ?:Array<PropertyInfoUnit>
        // 下一级属性列表
            NextPropertyInfoList ?:Array<PropertyInfoUnit>
        // 1 - 强化， 2 - 进阶 3 - 附魔 4 - 镶嵌(装备强化已经不在这个接口中实现了)  5 宝石合成  6 升星
            changeType ?:number
        // 装备返还奖励
            backJiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】英雄装备穿上、卸下
    interface PutZhuangbeiOnOffReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。
            heroIndex ?:number
        // 装备Db ID
            zhuangbeiDbID ?:number
        // 1 - 穿上， 2 - 卸下
            changeType ?:number
    }

// 【反馈】英雄装备穿上、卸下
    interface PutZhuangbeiOnOffRsp extends RspObj {
        // 被操作的装备DB Id
            zhuangBeiDbID ?:number

    }

// 【请求】打开装备礼包道具，随机获取一件道具
    interface OpenZhuangbeiGiftReq extends ReqObj {
        // 道具Db ID
            daojuDBId ?:number
    }

// 【反馈】打开装备礼包道具，随机获取一件道具
    interface OpenZhuangbeiGiftRsp extends RspObj {
        // 奖励的装备DB Id
            zhuangBeiDBID ?:number

    }

// 【请求】装备强化
    interface ZhuangbeiQianghuaReq extends ReqObj {
        // 装备Db ID
            zhuangbeiDBId ?:number
        // 强化次数
            count ?:number
    }

// 【反馈】装备强化
    interface ZhuangbeiQianghuaRsp extends RspObj {
        // 是否
            isSuccess ?:boolean

    }

// [请求]英雄一键穿装备
    interface WearEquipmentReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。
            heroIndex ?:number
        // 装备位置
            equipType ?:number
    }

// [返回]英雄一键穿装备
    interface WearEquipmentRsp extends RspObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。
            heroIndex ?:number
        // 被操作的装备list
            zhuangbeiInfoList ?:Array<ZhuangbeiInfoUnit>
        // 被替换的装备dbId
            replacedDbIds ?:Array<number>
        // 装备位置
            equipType ?:number

    }

// [请求]装备一键强化
    interface EquipmentChangesReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。
            heroIndex ?:number
        // 是否是引导
            isGuide ?:boolean
    }

// 【反馈】玩家装备变化
    interface EquipmentChangesRsp extends RspObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。
            heroIndex ?:number
        // 是否
            isSuccess ?:boolean

    }

// [请求]合成宝石
    interface HeChengBaoShiReq extends ReqObj {
        // 宝石Db ID
            daojvDbID ?:number
        // 合成宝石的数量
            count ?:number
    }

// [反馈]合成宝石
    interface HeChengBaoShiRsp extends RspObj {
        // 是否
            isSuccess ?:boolean
        // 新宝石的defId
            newBaoShiDefId ?:number
        // 合成新宝石的数量
            count ?:number

    }

// [请求]装备出售预览
    interface ZBChuShouYuLanReq extends ReqObj {
        // 装备dbId
            zhuangbeiDbIds ?:Array<number>
        // 是否引导
            isGuide ?:boolean
    }

// [反馈]装备出售预览
    interface ZBChuShouYuLanRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]装备出售
    interface ZBChuShouReq extends ReqObj {
        // 装备dbId
            zhuangbeiDbIds ?:Array<number>
        // 是否引导
            isGuide ?:boolean
    }

// [反馈]装备出售
    interface ZBChuShouRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 最大熔炼值
            maxRongLianZhi ?:number
        // 当前熔炼值
            currRongLianZhi ?:number

    }

// [请求]装备合成
    interface ZBHeChengReq extends ReqObj {
        // 装备dbId
            zhuangbeiDbIds ?:Array<number>
    }

// [反馈]装备合成
    interface ZBHeChengRsp extends RspObj {
        // 新装备debId
            zhuangbeiDbId ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]装备合成预览
    interface ZBHeChengYuLanReq extends ReqObj {
        // 装备dbId
            zhuangbeiDbIds ?:Array<number>
    }

// [反馈]装备合成预览
    interface ZBHeChengYuLanRsp extends RspObj {
        // 奖励列表  新装备放到第一个
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]卸下装备
    interface ReplaceZhuangBeiReq extends ReqObj {
        // 装备dbId
            zhuangbeiDbIds ?:Array<number>
    }

// [反馈]卸下装备
    interface ReplaceZhuangBeiRsp extends RspObj {
        // 0 成功 1 失败
            isSuccess ?:number

    }

// [请求]操作外显装备
    interface ChangeExpandZhuangBeiReq extends ReqObj {
        // 1 坐骑 2翅膀
            type ?:number
        // 1 穿上 2脱下 2 更换
            oType ?:number
        // 位置
            position ?:number
        // 原来的装备Id
            oldId ?:number
        // 新的装备Id
            newId ?:number
    }

// [反馈]操作外显装备
    interface ChangeExpandZhuangBeiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【请求】获得玩家外显装备列表
    interface GetExpandZhuangbeiListReq extends ReqObj {
    }

// 【反馈】获得玩家外显装备列表
    interface GetExpandZhuangbeiListRsp extends RspObj {
        // 外显装备列表
            zhuangbeiList ?:Array<ExpandZhuangbeiInfoUnit>

    }

// 【请求】获得玩家骑技飞技信息
    interface GetQiJiAndFEiJiInfoReq extends ReqObj {
    }

// 【反馈】获得玩家骑技飞技信息
    interface GetQiJiAndFEiJiInfoRsp extends RspObj {
        // 骑技属性列表
            qiJiInfoList ?:Array<ShuXingInfoUnit>
        // 飞技属性列表
            feiJiInfoList ?:Array<ShuXingInfoUnit>

    }

// 【请求】升星预览
    interface YuLanZhuangbeiShengxingReq extends ReqObj {
        // 查看的装备数据库ID
            zhuangbeiDBId ?:number
    }

// 【反馈】升星预览
    interface YuLanZhuangbeiShengxingRsp extends RspObj {
        // 当前强化装备的全部信息
            zhuangbeiInfo ?:ZhuangbeiInfoUnit
        // 新附魔属性列表
            propertyInfoList ?:Array<PropertyInfoUnit>
        // 下一级属性列表
            NextPropertyInfoList ?:Array<PropertyInfoUnit>

    }

// 【请求】装备升星加成属性查看
    interface ShowZhuangbeiJiaChengPropReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。
            heroIndex ?:number
    }

// 【反馈】装备升星加成属性查看
    interface ShowZhuangbeiJiaChengPropRsp extends RspObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。
            heroIndex ?:number
        // 当前hero装备总星数
            totalStar ?:number
        // 自己加成属性列表
            selfPropertyInfoList ?:Array<ZhuangbeiStarShowUnit>
        // 全局加成属性列表
            globalPropertyInfoList ?:Array<ZhuangbeiStarShowUnit>

    }

// 【请求】装备洗练
    interface ZhuangbeiXiLianReq extends ReqObj {
        // 装备DbId
            zhuangbeiDbId ?:number
        // 6 条属性的下标 1 ~ 6
            index ?:number
    }

// 【反馈】装备洗练
    interface ZhuangbeiXiLianRsp extends RspObj {
        // 属性列表
            propertyInfoList ?:Array<XiLianPropUnit>

    }

// 【请求】装备精练
    interface ZhuangbeiJingLianReq extends ReqObj {
        // 查看的装备数据库ID
            zhuangbeiDBId ?:number
    }

// 【反馈】装备精练
    interface ZhuangbeiJingLianRsp extends RspObj {
        // 当前强化装备的全部信息
            zhuangbeiInfo ?:ZhuangbeiInfoUnit
        // 当前属性列表
            propertyInfoList ?:Array<PropertyInfoUnit>
        // 下一级属性列表
            NextPropertyInfoList ?:Array<PropertyInfoUnit>

    }

// 【请求】精炼预览
    interface YuLanZhuangbeiJinglianReq extends ReqObj {
        // 查看的装备数据库ID
            zhuangbeiDBId ?:number
    }

// 【反馈】精炼预览
    interface YuLanZhuangbeiJinglianRsp extends RspObj {
        // 当前强化装备的全部信息
            zhuangbeiInfo ?:ZhuangbeiInfoUnit
        // 当前属性列表
            propertyInfoList ?:Array<PropertyInfoUnit>
        // 下一级属性列表
            NextPropertyInfoList ?:Array<PropertyInfoUnit>
        // 精炼消耗列表
            costItemList ?:Array<JiangliInfoUnit>

    }

// 【请求】装备熔炼
    interface ZhuangbeiRongLianPanelReq extends ReqObj {
    }

// 【反馈】装备熔炼
    interface ZhuangbeiRongLianPanelRsp extends RspObj {
        // 当前熔炼值
            currRongLianZhi ?:number
        // 最大熔炼值 参数表10146
            maxRongLianZhi ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 装备列表
            equipIdAndRongLianZhiList ?:Array<DBIdAndIntListUnit>

    }

// 【请求】装备天赋（进阶）属性列表
    interface GetZhuangbeiTianfuPropsReq extends ReqObj {
        // 1、附魔；2、升星；3、精炼
            operationType ?:number
        // 跟operationType对应的moban值
            moban ?:number
    }

// 【反馈】装备天赋（进阶）属性列表
    interface GetZhuangbeiTianfuPropsRsp extends RspObj {
        // 1、附魔；2、升星；3、精炼
            operationType ?:number
        // 跟operationType对应的moban值
            moban ?:number
        // 奖励列表
            tianfuPropList ?:Array<ZhuangbeiStarShowUnit>

    }

// 【请求】装备突破属性列表
    interface GetZhuangbeiTUPoPropsReq extends ReqObj {
        // 装备ID
            defId ?:number
    }

// 【反馈】装备突破属性列表
    interface GetZhuangbeiTUPoPropsRsp extends RspObj {
        // 回传
            defId ?:number
        // 附魔属性列表
            fuMoPropList ?:Array<ZhuangbeiStarShowUnit>
        // 精炼属性列表
            jingLianPropList ?:Array<ZhuangbeiStarShowUnit>

    }

// 【请求】玩家装备变化
    interface ZhuangbeiHechengReq extends ReqObj {
        // 1 - 一键合成， 2 - 指定合成， 3 - 升星全部
            type ?:number
        // 1 - 一键合成，指定合成的装备部分类型
            equipType ?:number
        // 2 - 指定合成，指定消耗的装备Db ID列表
            dbIdList ?:Array<number>
        // 是否预览新装备
            isNewZhuangbeiPreview ?:boolean
        // 1 - 一键合成，指定合成的武将职业类型
            heroPhoto ?:string
    }

// 【反馈】装备合成
    interface ZhuangbeiHechengRsp extends RspObj {
        // 1 - 一键合成， 2 - 指定合成， 3 - 升星全部
            type ?:number
        // 1 - 一键合成，指定合成的装备部分类型
            equipType ?:number
        // 是否预览新装备
            isNewZhuangbeiPreview ?:boolean
        // 新装备奖励列表
            newZhuangbeiList ?:Array<JiangliInfoUnit>
        // 除了装备之外的物料消耗列表, isNewZhuangbeiPreview=true时才赋值
            itemCostList ?:Array<JiangliInfoUnit>
        // 装备返还奖励
            backJiangliList ?:Array<JiangliInfoUnit>
        // 1 - 一键合成，指定合成的武将职业类型
            heroPhoto ?:string

    }

// 【请求】装备图鉴面板
    interface ZhuangbeiTujianPanelReq extends ReqObj {
    }

// 【反馈】装备图鉴面板
    interface ZhuangbeiTujianPanelRsp extends RspObj {
        // 套装属性列表
            suitList ?:Array<ZhuangbeiSuitInfoUnit>
        // 装备列表
            zhuangbeiTujianList ?:Array<ZhuangbeiTujianInfoUnit>

    }

// 获取已有战队信息
    interface GetZhanDuiListReq extends ReqObj {
        // 第几页
            page ?:number
    }

// 获取已有战队信息
    interface GetZhanDuiListRsp extends RspObj {
        // 一共多少个战队
            totalZhanDuiNum ?:number
        // 一些战队信息
            zhanDuiInfos ?:Array<ZhanDuiInfoUnit>
        // 邀请红点  true 有  false没有
            inviteTip ?:boolean

    }

// 打开战队界面
    interface OpenZhanDuiReq extends ReqObj {
    }

// 打开战队界面
    interface OpenZhanDuiRsp extends RspObj {
        // 是否已经报过名
            isBaoMing ?:boolean
        // 报名结束时间
            endBaoMingTime ?:number
        // 0 还未加入战队，1 已经加入战队, 2 本服战队赛已经开始 ,3 跨服淘汰赛已开始,4 跨服淘汰赛已结束,5 从未开启过且下月才会开始
            playerStatus ?:number

    }

// 创建战队
    interface CreateZhanDuiReq extends ReqObj {
        // 战队名字
            name ?:string
    }

// 创建战队(创建成功后，直接返回打开战队界面)
    interface CreateZhanDuiRsp extends RspObj {

    }

// 申请加入战队
    interface ApplyZhanDuiReq extends ReqObj {
        // 战队id
            zhanDuiId ?:number
    }

// 申请加入战队
    interface ApplyZhanDuiRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 打开战队申请列表
    interface OpenZhanDuiApplyListReq extends ReqObj {
    }

// 打开战队申请列表
    interface OpenZhanDuiApplyListRsp extends RspObj {
        // 战队成员信息
            zhanDuiMemberInfos ?:Array<ZhanDuiMemberInfoUnit>

    }

// 处理申请人员
    interface DealZhanDuiApplyReq extends ReqObj {
        // 0: 同意 1:拒绝
            type ?:number
        // 申请的玩家id
            playerId ?:number
    }

// 处理申请人员
    interface DealZhanDuiApplyRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 踢掉战队人员
    interface KickZhanDuiMemberReq extends ReqObj {
        // 踢掉的玩家id
            playerId ?:number
    }

// 踢掉战队人员
    interface KickZhanDuiMemberRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 解散战队或脱离战队
    interface DelZhanDuiReq extends ReqObj {
        // 0:解散战队 1:脱离战队
            type ?:number
    }

// 解散战队
    interface DelZhanDuiRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 报名参赛
    interface ApplyMatchReq extends ReqObj {
    }

// 报名参赛
    interface ApplyMatchRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 邀请加入战队
    interface InviteJoinZhanDuiReq extends ReqObj {
        // 邀请的玩家id
            playerId ?:number
    }

// 邀请加入战队
    interface InviteJoinZhanDuiRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 处理邀请信息
    interface DealZhanDuiInviteReq extends ReqObj {
        // 0: 同意 1:拒绝
            type ?:number
        // 邀请的战队ID
            zhanDuiId ?:number
    }

// 处理邀请信息
    interface DealZhanDuiInviteRsp extends RspObj {
        // 0:失败 1:成功(成功后,推送OpenZhanDui协议)
            result ?:number

    }

// 打开搜索队员界面
    interface OpenMemberSearchReq extends ReqObj {
    }

// 打开搜索队员界面
    interface OpenMemberSearchRsp extends RspObj {
        // 推荐的人员信息
            memberInfo ?:Array<ZhanDuiMemberInfoUnit>

    }

// 搜索队员
    interface ZhanDuiMemberSearchReq extends ReqObj {
        // 搜索的人员名字
            name ?:string
    }

// 搜索队员
    interface ZhanDuiMemberSearchRsp extends RspObj {
        // 搜索到的人员信息
            memberInfo ?:ZhanDuiMemberInfoUnit

    }

// 修改战队成员出战位置
    interface UpdateZhanDuiPositionReq extends ReqObj {
        // 修改后的战斗序列
            playerIdList ?:Array<number>
    }

// 修改战队成员出战位置
    interface UpdateZhanDuiPositionRsp extends RspObj {
        // 0:失败 1:成功
            result ?:number

    }

// 登陆时检查玩家是否已经在战队中
    interface CheckPlayerStatusReq extends ReqObj {
    }

// 登陆时检查玩家是否已经在战队中
    interface CheckPlayerStatusRsp extends RspObj {
        // 0 还未加入战队，1 已经加入战队, 2 本服战队赛已经开始 ,3 跨服淘汰赛已开始,4 跨服淘汰赛已结束,5 从未开启过且下月才会开始
            playerStatus ?:number

    }

// 打开战队邀请列表
    interface OpenZhanDuiInviteListReq extends ReqObj {
    }

// 打开战队邀请列表
    interface OpenZhanDuiInviteListRsp extends RspObj {
        // 战队信息
            zhanDuiInfos ?:Array<ZhanDuiInfoUnit>

    }

// 获取战队比赛奖励
    interface GetZhanduiMatchRewardReq extends ReqObj {
        // 服务器排名
            paiMing ?:number
        // 战队ID
            zhanDuiId ?:number
    }

// 获取战队比赛奖励
    interface GetZhanduiMatchRewardRsp extends RspObj {
        // 0成功 1 失败
            result ?:boolean

    }

// 收回战队比赛奖励
    interface RemoveZhanduiRewardReq extends ReqObj {
        // 服务器排名
            paiMing ?:number
        // 战队ID
            zhanDuiId ?:number
    }

// 收回战队比赛奖励
    interface RemoveZhanduiRewardRsp extends RspObj {
        // 0成功 1 失败
            result ?:boolean

    }

// 争霸赛商城列表
    interface ZhanduiShopListReq extends ReqObj {
    }

// 争霸赛商城列表
    interface ZhanduiShopListRsp extends RspObj {
        // 商品信息列表
            shopList ?:Array<ZhanDuiShopInfoUnit>

    }

// 购买争霸赛商城商品
    interface GetZhanduiShopGoodsReq extends ReqObj {
        // 商品Id
            defId ?:number
        // 商品数量
            count ?:number
    }

// 争霸赛商城列表
    interface GetZhanduiShopGoodsRsp extends RspObj {
        // 是否购买成功
            isSucce ?:boolean

    }

// 获取联盟币数量
    interface GetLianMengScoreCountReq extends ReqObj {
    }

// 获取联盟币数量
    interface GetLianMengScoreCountRsp extends RspObj {
        // 联盟币数量
            count ?:number

    }

// 【请求】商店购买商品
    interface ShopGoodsReq extends ReqObj {
        // 商店静态表ID
            shopId ?:number
        // 卖出数量
            count ?:number
        // 购买类型：1，普通商店购买；2，全服商店购买；3、世界BOSS商店购买
            type ?:number
        // 商店分类 1，钻石商城 2，金券商城
            fenlei ?:number
        // 每组道具的数量
            number ?:number
    }

// 【反馈】商店购买商品
    interface ShopGoodsRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// 【请求】获取玩家购买商品的列表
    interface GetShopGoodsListReq extends ReqObj {
    }

// 【反馈】获取玩家购买商品的列表
    interface GetShopGoodsListRsp extends RspObj {
        // 获取玩家购物信息列表
            shopGoodsList ?:Array<ShopInfoUnit>
        // 获取全服限购商品列表
            shopGlobalGoodsList ?:Array<ShopInfoUnit>
        // 获取玩家打折购物信息列表
            daZheShopGoodsList ?:Array<ShopInfoUnit>

    }

// 【请求】加载黑市商品列表
    interface LoadHSGoodsReq extends ReqObj {
    }

// 【反馈】加载黑市商品列表
    interface LoadHSGoodsRsp extends RspObj {
        // 黑市商品列表
            goodsList ?:Array<HSGoodsInfoUnit>
        // 下次免费刷新时间
            refreshTime ?:number

    }

// 【请求】购买黑市商品
    interface ShopHSGoodsReq extends ReqObj {
        // 黑市商品ID
            goodsId ?:number
    }

// 【反馈】购买黑市商品
    interface ShopHSGoodsRsp extends RspObj {
        // 黑市商品信息
            goodsInfo ?:HSGoodsInfoUnit

    }

// 【请求】黑市商品刷新
    interface RefreshHSGoodsReq extends ReqObj {
        // 是否元宝刷新
            isCurrency ?:boolean
    }

// 【反馈】黑市商品刷新
    interface RefreshHSGoodsRsp extends RspObj {

    }

// 【请求】商店中 这个这个商品 购买了多少个
    interface GetHasBuyCountShopReq extends ReqObj {
        // shopId
            shopId ?:number
    }

// 【反馈】黑市商品刷新
    interface GetHasBuyCountShopRsp extends RspObj {
        // 
            count ?:number
        // shopId
            shopId ?:number

    }

// 【请求】商城面板
    interface ShangChengPanelReq extends ReqObj {
        // 商城一级Type
            shopType ?:number
        // 页签
            shopTab ?:number
    }

// 【反馈】商城面板
    interface ShangChengPanelRsp extends RspObj {
        // 货币类型
            costTypeList ?:Array<NumberValueUnit>
        // 购买限制
            gouMaiXianZhiList ?:Array<NumberValueUnit>
        // 商品列表
            goodsList ?:Array<ShangChengGoodsUnit>

    }

// 【请求】购买商城商品
    interface BuyShangChengGoodsReq extends ReqObj {
        // 商品ID
            defId ?:number
        // 购买数量
            count ?:number
    }

// 【反馈】购买商城商品
    interface BuyShangChengGoodsRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// 【请求】推送商城页签状态
    interface PushShangChengStatusReq extends ReqObj {
    }

// 【反馈】推送商城页签状态
    interface PushShangChengStatusRsp extends RspObj {
        // 商城状态
            statusList ?:Array<ShangChengStatusUnit>

    }

// 【请求】商品预览
    interface ShangChengGoodsShowReq extends ReqObj {
        // 道具Id
            itemId ?:number
    }

// 【反馈】商品预览
    interface ShangChengGoodsShowRsp extends RspObj {
        // 商品信息
            goodsInfo ?:ShangChengGoodsUnit

    }

// 【请求】商城面板
    interface NewShopPanelReq extends ReqObj {
        // 商店类型1.宠物商店 2.装备强化材料 3.装备传说装备，4.装备福利
            shopType ?:number
    }

// 【反馈】商城面板
    interface NewShopPanelRsp extends RspObj {
        // 商店类型1.宠物商店 2.装备强化材料 3.装备传说装备，4.装备福利
            shopType ?:number
        // 货币类型
            costTypeList ?:Array<JiangliInfoUnit>
        // 购买限制
            gouMaiXianZhiList ?:Array<NumberValueUnit>
        // 是否是刷新类型
            isRefresh ?:boolean
        // 刷新CD-下次刷新的时间点
            refreshCD ?:number
        // 刷新消耗
            costInfo ?:JiangliInfoUnit
        // 刷新剩余次数
            refreshLeftCount ?:number
        // 免费刷新剩余次数
            freeRefreshLeftCount ?:number
        // 商品列表
            goodsList ?:Array<NewShopGoodsUnit>
        // 是否需要强制引导
            isGuide ?:boolean

    }

// 【请求】购买商品
    interface BuyNewShopGoodsReq extends ReqObj {
        // 商店类型1.宠物商店 2.装备强化材料 3.装备传说装备，4.装备福利
            shopType ?:number
        // 物品id
            id ?:number
        // 购买个数
            count ?:number
    }

// 【反馈】购买商品
    interface BuyNewShopGoodsRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【请求】刷新商品
    interface RefreshNewShopReq extends ReqObj {
        // 商店类型1.宠物商店 2.装备强化材料 3.装备传说装备，4.装备福利
            shopType ?:number
    }

// 【反馈】刷新商品
    interface RefreshNewShopRsp extends RspObj {
        // 商店类型1.宠物商店 2.装备强化材料 3.装备传说装备，4.装备福利
            shopType ?:number
        // 是否成功的标志
            isSuccess ?:boolean

    }

// 【请求】老商城商品信息
    interface OldShopShowReq extends ReqObj {
        // 道具Id
            itemId ?:number
    }

// 【反馈】老商城商品信息
    interface OldShopShowRsp extends RspObj {
        // 商品信息
            shopInfo ?:ShopInfoUnit

    }

// 【请求】地牢商店面板
    interface DungeonShopPanelReq extends ReqObj {
    }

// 【反馈】地牢商店面板
    interface DungeonShopPanelRsp extends RspObj {
        // 商品列表
            goodsList ?:Array<DungeonShopGoodsUnit>
        // Npc说话内容:dilaoshangdian表npcliaotian字段
            npcChat ?:string

    }

// 【请求】购买地牢商品
    interface BuyDungeonShopGoodsReq extends ReqObj {
        // 商品id
            defId ?:number
    }

// 【反馈】购买地牢商品
    interface BuyDungeonShopGoodsRsp extends RspObj {
        // Npc说话内容:dilaoshangdian表npcliaotian字段
            npcChat ?:string
        // 购买后的Goods信息
            goods ?:DungeonShopGoodsUnit
        // 购买得到的奖励信息
            item ?:JiangliInfoUnit

    }

// 玩家任务列表
    interface GetMissionListReq extends ReqObj {
        // 3成就 10签到 15神技成就 与任务表类型保持一致
            tag ?:number
        // 签到活动Id
            newActivityId ?:number
    }

// 玩家任务列表
    interface GetMissionListRsp extends RspObj {
        // 任务列表
            missionList ?:Array<MissionInfoUnit>

    }

// 玩家获取任务奖励请求
    interface GetMissionJiangliReq extends ReqObj {
        // 任务DBID
            id ?:number
        // 签到那用 活动Id
            newActivityId ?:number
        // 参数1-4，对应任务表canshu1~4的意思
            canshuIndex ?:number
        // 3成就 10签到 与任务表类型保持一致
            tag ?:number
        // 是否一键领取tag分类下的全部任务奖励
            isAllReward ?:boolean
    }

// 玩家获取任务奖励请求
    interface GetMissionJiangliRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 任务defId
            renWuDefId ?:number
        // 3成就 10签到 与任务表类型保持一致
            tag ?:number
        // 是否一键领取tag分类下的全部任务奖励
            isAllReward ?:boolean

    }

// 玩家引导任务
    interface GetGuideMissionReq extends ReqObj {
    }

// 玩家引导任务
    interface GetGuideMissionRsp extends RspObj {
        // 任务信息
            mission ?:MissionInfoUnit

    }

// 展示邀请面板
    interface ShowYaoQingPanelReq extends ReqObj {
    }

// 展示邀请面板
    interface ShowYaoQingPanelRsp extends RspObj {
        // 奖励列表
            yaoQingMissionInfoList ?:Array<YaoQingMissionInfoUnit>

    }

// 极限挑战面板
    interface LoadJiXianTiaoZhanPanelReq extends ReqObj {
    }

// 极限挑战面板
    interface LoadJiXianTiaoZhanPanelRsp extends RspObj {
        // 任务列表
            missionList ?:Array<JiXianTiaoZhanRenWuUnit>

    }

// 领取极限挑战奖励
    interface GetJiXianTiaoZhanRewardReq extends ReqObj {
        // 任务ID
            renWuId ?:number
    }

// 领取极限挑战奖励
    interface GetJiXianTiaoZhanRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 领取七日大奖任务奖励
    interface GetSevenMatchRewardMissionReq extends ReqObj {
    }

// 领取七日大奖任务奖励
    interface GetSevenMatchRewardMissionRsp extends RspObj {

    }

// 参与日常任务引导
    interface JoinDaliyMissionYinDaoReq extends ReqObj {
    }

// 参与日常任务引导
    interface JoinDaliyMissionYinDaoRsp extends RspObj {

    }

// 获取日常任务列表
    interface GetDailyMissionListReq extends ReqObj {
    }

// 获取日常任务列表
    interface GetDailyMissionListRsp extends RspObj {
        // 日常任务列表
            dailyMissionList ?:Array<DailyMissionInfoUnit>
        // 活跃任务列表
            missionList ?:Array<MissionInfoUnit>

    }

// 接收到被踢的消息
    interface GetFollowPetBeiTiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 踢我那个人的服务器长id
            fromPlayerServerId ?:number
        // 踢我那个人的服务器号
            fromPlayerServerNo ?:number

    }

// 接收到搜索玩家房间信息的请求
    interface GetSearchPetRoomRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 错误信息
            message ?:string
        // 搜索的那个人
            fromPlayerId ?:number
        // 搜索的那个人的服务器长id
            fromPlayerServerId ?:number
        // 我的房间属性信息
            followPetRdmRoom ?:FollowPetRdmRoomUnit
        // 是否已经被寄养
            isJiYang ?:boolean
        // 寄养宠物的信息
            jiYangPetInfo ?:FollowPetUnit

    }

// [请求]蓝钻信息
    interface LanZuanInfoReq extends ReqObj {
    }

// [返回]蓝钻信息
    interface LanZuanInfoRsp extends RspObj {
        // 蓝钻等级
            lanZuanLevel ?:number
        // 是否是年费蓝钻
            isLanZuanNianFei ?:boolean
        // 是否是豪华蓝钻
            isLanZuanHaoHua ?:boolean
        // true有红点        false没有
            meiRiTips ?:boolean
        // true有红点        false没有
            chengZhangTips ?:boolean
        // true有红点        false没有
            xinShouTips ?:boolean

    }

// [请求]蓝钻每日礼包
    interface LanZuanMeiRiLiBaoListReq extends ReqObj {
    }

// [返回]蓝钻每日礼包
    interface LanZuanMeiRiLiBaoListRsp extends RspObj {
        // 奖励列表
            lanZuanLiBaoList ?:Array<LanZuanLiBaoInfoUnit>

    }

// [请求]蓝钻成长礼包
    interface LanZuanChengZhangLiBaoListReq extends ReqObj {
    }

// [返回]蓝钻成长礼包
    interface LanZuanChengZhangLiBaoListRsp extends RspObj {
        // 奖励列表
            lanZuanLiBaoList ?:Array<LanZuanLiBaoInfoUnit>

    }

// [请求]蓝钻新手礼包
    interface LanZuanXinShouLiBaoListReq extends ReqObj {
    }

// [返回]蓝钻新手礼包
    interface LanZuanXinShouLiBaoListRsp extends RspObj {
        // 奖励列表
            lanZuanLiBaoList ?:Array<LanZuanLiBaoInfoUnit>

    }

// [请求]领取蓝钻每日礼包
    interface GetLanZuanMeiRiLiBaoReq extends ReqObj {
    }

// [返回]领取蓝钻每日礼包
    interface GetLanZuanMeiRiLiBaoRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]领取蓝钻成长礼包
    interface GetLanZuanChengZhangLiBaoReq extends ReqObj {
        // 哪一档   传玩家等级
            level ?:number
    }

// [返回]领取蓝钻成长礼包
    interface GetLanZuanChengZhangLiBaoRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]领取蓝钻新手礼包
    interface GetLanZuanXinShouLiBaoReq extends ReqObj {
    }

// [返回]领取蓝钻新手礼包
    interface GetLanZuanXinShouLiBaoRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]领取特权奖励 首充
    interface GetTeQuanRewardReq extends ReqObj {
    }

// [返回]领取特权奖励
    interface GetTeQuanRewardRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]最强礼包预览
    interface LoadZuiQiangLiBaoPanelReq extends ReqObj {
    }

// [返回]最强礼包预览
    interface LoadZuiQiangLiBaoPanelRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 礼包结束时间
            endTime ?:number

    }

// [请求]领取最强礼包
    interface GetZuiQiangLiBaoRewardReq extends ReqObj {
    }

// [返回]领取最强礼包
    interface GetZuiQiangLiBaoRewardRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]资源月卡界面
    interface ZiYuanYueKaPanelReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// [返回]资源月卡界面
    interface ZiYuanYueKaPanelRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<ZiYuanYueKaUnit>
        // 是否已经购买过
            isBuy ?:boolean

    }

// [请求]领取资源月卡奖励
    interface GetZiYuanYueKaRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 奖励ID
            rewardId ?:number
    }

// [返回]领取资源月卡奖励
    interface GetZiYuanYueKaRewardRsp extends RspObj {
        // 领取成功
            isSuccess ?:boolean

    }

// [请求]蓝钻礼包列表
    interface LanZuanLiBaoListReq extends ReqObj {
    }

// [返回]蓝钻礼包列表
    interface LanZuanLiBaoListRsp extends RspObj {
        // 蓝钻每日礼包
            meiRiLiBaoList ?:Array<LanZuanLiBaoInfoUnit>
        // 豪华蓝钻礼包
            haoHuaLiBaoList ?:Array<LanZuanLiBaoInfoUnit>
        // 年费蓝钻礼包
            nianFeiLiBaoList ?:Array<LanZuanLiBaoInfoUnit>
        // 蓝钻成长礼包
            chengZhangLiBaoList ?:Array<LanZuanLiBaoInfoUnit>
        // 蓝钻新手礼包
            xinShouLiBaoList ?:Array<LanZuanLiBaoInfoUnit>

    }

// [请求]一键领取蓝钻礼包
    interface GetLanZuanLiBaoOneKeyReq extends ReqObj {
    }

// [返回]一键领取蓝钻礼包
    interface GetLanZuanLiBaoOneKeyRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]qq大厅礼包列表
    interface QQGameLiBaoListReq extends ReqObj {
    }

// [返回]qq大厅礼包列表
    interface QQGameLiBaoListRsp extends RspObj {
        // 新手礼包
            xinShouLiBaoList ?:Array<LanZuanLiBaoInfoUnit>
        // 每日礼包
            meiRiLiBaoList ?:Array<LanZuanLiBaoInfoUnit>
        // 成长礼包
            chengZhangLiBaoList ?:Array<LanZuanLiBaoInfoUnit>

    }

// [请求]领取qq大厅礼包
    interface GetQQGameLiBaoReq extends ReqObj {
        // 1 新手 2 每日 3 成长
            oType ?:number
    }

// [返回]领取qq大厅礼包
    interface GetQQGameLiBaoRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]黄金投资list
    interface HuangJinTouZiListReq extends ReqObj {
        // 类型 1 黄  2 白 3 钻
            type ?:number
    }

// [请求]黄金投资list
    interface HuangJinTouZiListRsp extends RspObj {
        // 黄金投资list
            infoList ?:Array<HuangJinTouZiInfoUnit>
        // 是否已经激活投资
            isJiHuo ?:boolean
        // 累计登陆天数
            loginDay ?:number
        // 需要消耗的类型
            needXiaoHaoType ?:number
        // 需要消耗的数量
            needXiaoHaoCount ?:number

    }

// [请求]领取黄金投资奖励
    interface GetHuangJinTouZiReq extends ReqObj {
        // 静态表id
            defId ?:number
    }

// [请求]领取黄金投资
    interface GetHuangJinTouZiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]激活黄金投资
    interface ActiveHuangJinTouZiReq extends ReqObj {
        // 1 黄金 2 白金 3 钻石贵族
            type ?:number
    }

// [请求]激活黄金投资
    interface ActiveHuangJinTouZiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]黄金贵族面板
    interface HuangJinGuiZuPanelReq extends ReqObj {
    }

// [请求]黄金贵族面板
    interface HuangJinGuiZuPanelRsp extends RspObj {
        // 是否自动挑战世界boss
            isWorldBossAuto ?:boolean
        // 是否自动挑战领主
            isFightXiaoGuanAuto ?:boolean

    }

// [请求]修改贵族特权开关
    interface UpdateGuiZuAutoReq extends ReqObj {
        // 2世界boss 6领主
            sType ?:number
        // 开关
            isAuto ?:boolean
    }

// [请求]修改贵族特权开关
    interface UpdateGuiZuAutoRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 2世界boss 6领主
            sType ?:number
        // 开关
            isAuto ?:boolean

    }

// 贵族特权开关信息
    interface PushGuiZuAutoChangeRsp extends RspObj {
        // 当前开启的开关
            openTypes ?:Array<number>

    }

// [请求]创角返利面板
    interface ChuangJueFanLiPanelReq extends ReqObj {
        // 活动Id
            activityId ?:number
    }

// 创角返利面板
    interface ChuangJueFanLiPanelRsp extends RspObj {
        // 活动结束时间
            endTime ?:number
        // 充值条件 金券
            chingZhiTiaoJian ?:number
        // 已经充值
            hasChongZhi ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]领取创角返利奖励
    interface GetChuangJueFanLiReq extends ReqObj {
        // 活动Id
            activityId ?:number
    }

// 领取创角返利奖励
    interface GetChuangJueFanLiRsp extends RspObj {
        // 活动结束时间
            endTime ?:number
        // 充值条件
            tiaoJian ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]专属神器面板  详情面板  所有属性都能看到的面板
    interface ZhuanShuPanelReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。上下阵都要赋值
            heroIndex ?:number
    }

// [返回]专属神器面板  详情面板  所有属性都能看到的面板
    interface ZhuanShuPanelRsp extends RspObj {
        // 强化属性列表
            qiangHuaShuXing ?:Array<ShuXingInfoUnit>
        // 神器属性列表
            shenQiShuXing ?:Array<ShuXingInfoUnit>
        // 专属属性列表
            zhuanShuShuXing ?:Array<ZhuanShuShenQiShuXingInfoUnit>
        // 佩戴的专属神器的dbid
            zhuanShuShenQiDbId ?:number
        // 佩戴的专属神器的define id
            zhuanShuShenQiDefId ?:number
        // 佩戴的专属神器的图
            zhuanShuShenQiTu ?:string
        // 佩戴的专属神器的等级
            zhuanShuShenLevel ?:number
        // 佩戴的专属神器的名字
            zhuanShuShenName ?:string
        // 神器的特效类型
            teXiaoType ?:number
        // 图鉴星级
            starNum ?:number
        // 需要的职业
            profession ?:number
        // 是否激活专属技能
            isJiHuo ?:boolean

    }

// 【请求】精灵 专属 神器穿上、卸下
    interface PutZhuanShuShenQiOnOffReq extends ReqObj {
        // Db ID
            shenQiDbID ?:number
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。上下阵都要赋值
            heroIndex ?:number
        // 1 - 穿上， 2 - 卸下
            changeType ?:number
    }

// 【反馈】请求】精灵 专属 神器穿上、卸下
    interface PutZhuanShuShenQiOnOffRsp extends RspObj {
        // 被操作的神器DB Id
            shenQiDbID ?:number

    }

// [请求]专属神器背包
    interface ZhuanShuShenQiBagListReq extends ReqObj {
        // 请求页签的类型 -1 全部  0  其他  剩下的参考策划表
            type ?:number
    }

// [返回]专属神器背包
    interface ZhuanShuShenQiBagListRsp extends RspObj {
        // 专属神器背包中的类
            zhuanShuShuXing ?:Array<ZhuanShuShenQiBagInfoUnit>

    }

// 【请求】专属神器强化
    interface ZhuanShuQiangHuaReq extends ReqObj {
        // Db ID
            shenQiDbID ?:number
        // 是不是一键强化  true是  false 不是
            oneKey ?:boolean
    }

// 【反馈】专属神器强化
    interface ZhuanShuQiangHuaRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// 【请求】专属神器回收预览
    interface ZhuanShuHuiShouYuLanReq extends ReqObj {
        // 要回收的dbIdList
            shenQiDbIDList ?:Array<number>
    }

// 【反馈】专属神器回收预览
    interface ZhuanShuHuiShouYuLanRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 返还材料百分比
            percent ?:number
        // 消耗
            cost ?:JiangliInfoUnit

    }

// 【请求】专属神器回收批量预览   返回 ZhuanShuHuiShouYuLan
    interface ZhuanShuHuiShouPiLiangYuLanReq extends ReqObj {
        // 类型 50级神器到 120神器 类型分别是 1-8
            typeList ?:Array<number>
    }

// 【请求】专属神器回收  接受dbId
    interface ZhuanShuHuiShouReq extends ReqObj {
        // 要回收的dbIdList
            shenQiDbIDList ?:Array<number>
    }

// 【反馈】专属神器回收结果
    interface ZhuanShuHuiShouRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】专属神器回收批量   返回 ZhuanShuHuiShou
    interface ZhuanShuHuiShouPiLiangReq extends ReqObj {
        // 类型 50级神器到 120神器 类型分别是 1-8
            typeList ?:Array<number>
    }

// 【请求】专属神器强化面板
    interface ZhuanShuQiangHuaPanelReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。上下阵都要赋值
            heroIndex ?:number
    }

// 【反馈】专属神器强化面板
    interface ZhuanShuQiangHuaPanelRsp extends RspObj {
        // 佩戴的专属神器的dbid
            zhuanShuShenQiDbId ?:number
        // 佩戴的专属神器的图
            zhuanShuShenQiTu ?:string
        // 佩戴的专属神器的等级
            zhuanShuShenLevel ?:number
        // 佩戴的专属神器的名字
            zhuanShuShenName ?:string
        // 神器的特效类型
            teXiaoType ?:number
        // 当前强化属性列表
            currQiangHuaShuXing ?:Array<ShuXingInfoUnit>
        // 下一级强化属性列表
            nextQiangHuaShuXing ?:Array<ShuXingInfoUnit>
        // 要消耗的道具id
            costItemDefId ?:number
        // 要消耗的道具数量
            costItemCount ?:number

    }

// 【请求】专属神器图鉴ICON
    interface ZhuanShuTuJianIconReq extends ReqObj {
    }

// 【反馈】专属神器图鉴ICON
    interface ZhuanShuTuJianIconRsp extends RspObj {
        // 火系图鉴icon
            huoxiIcon ?:Array<ZhuanShuShenQiTuJianIconInfoUnit>
        // 水系图鉴icon
            shuixiIcon ?:Array<ZhuanShuShenQiTuJianIconInfoUnit>
        // 草系图鉴icon
            caoxiIcon ?:Array<ZhuanShuShenQiTuJianIconInfoUnit>
        // 暗系图鉴icon
            anxiIcon ?:Array<ZhuanShuShenQiTuJianIconInfoUnit>
        // 光系图鉴icon
            guangxiIcon ?:Array<ZhuanShuShenQiTuJianIconInfoUnit>
        // 特殊图鉴icon
            texiIcon ?:Array<ZhuanShuShenQiTuJianIconInfoUnit>

    }

// 【请求】专属神器图鉴itemInfo
    interface ZhuanShuTuJianItemInfoReq extends ReqObj {
        // 图鉴DefId
            tuJianDefId ?:number
    }

// 【反馈】专属神器图鉴itemInfo
    interface ZhuanShuTuJianItemInfoRsp extends RspObj {
        // 图鉴name
            tuJianName ?:string
        // 图鉴icon
            icon ?:string
        // 星数
            starNum ?:number
        // 条件
            tiaoJian ?:number
        // 当前属性
            currentProp ?:Array<PropertyInfoUnit>
        // 下一级属性
            nextProp ?:Array<PropertyInfoUnit>
        // 需求材料
            xuQiuCaiLiao ?:JiangliInfoUnit
        // 材料数量 不包括穿戴着的神器
            shenQiCount ?:number
        // 图鉴属性列表
            tuJianShuXing ?:Array<ShuXingInfoUnit>
        // 这个神器是否穿戴
            isPutOn ?:boolean
        // 当前技能升级信息
            currentSkill ?:ZhuanShuShenQiShuSkillInfoUnit
        // 下一星级的技能升级信息
            nextSkill ?:ZhuanShuShenQiShuSkillInfoUnit

    }

// 【请求】激活专属神器图鉴返回材料
    interface JiHuoTuJianFanHuanReq extends ReqObj {
        // 图鉴DefId
            tuJianDefId ?:number
    }

// 【返回】激活专属神器图鉴返回材料
    interface JiHuoTuJianFanHuanRsp extends RspObj {
        // 0：无返回材料  1：有返回材料
            success ?:number
        // 图鉴类型
            fanHuanCaiLiao ?:JiangliInfoUnit

    }

// 【请求】激活专属神器图鉴
    interface JiHuoTuJianReq extends ReqObj {
        // 图鉴DefId
            tuJianDefId ?:number
    }

// 【返回】激活专属神器图鉴
    interface JiHuoTuJianRsp extends RspObj {
        // 0：失败  1：成功
            success ?:number

    }

// 【请求】专属神器红点
    interface tuJianPointReq extends ReqObj {
    }

// 【返回】专属神器红点
    interface tuJianPointRsp extends RspObj {
        // defId
            pointList ?:Array<number>

    }

// [请求]洛托姆之力主界面
    interface LuoTuoMuMainPanelReq extends ReqObj {
    }

// [返回]洛托姆之力主界面
    interface LuoTuoMuMainPanelRsp extends RspObj {
        // 洛托姆之力数据
            ltmList ?:Array<LuoTuoMuInfoUnit>
        // 属性列表
            shuXingInfoList ?:Array<ShuXingInfoUnit>

    }

// [请求]洛托姆之力界面
    interface LuoTuoMuPanelReq extends ReqObj {
        // 洛托姆之力类型0强化 1精炼 2重铸
            ltmType ?:number
    }

// [返回]洛托姆之力界面
    interface LuoTuoMuPanelRsp extends RspObj {
        // 洛托姆之力数据
            ltmList ?:Array<LuoTuoMuInfoUnit>
        // 当前点
            curpoint ?:number
        // 当前属性列表
            curShuXingInfo ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
            yuLanShuXingInfo ?:Array<ShuXingInfoUnit>
        // 消耗道具的id
            costItemId ?:number
        // 消耗道具的数量   一次的数量
            costItemCount ?:number

    }

// [请求]洛托姆之力升级
    interface LuoTuoMuUpReq extends ReqObj {
        // 洛托姆之力类型0强化 1精炼 2重铸
            ltmType ?:number
        // 强化类型0 单次 1一键
            upType ?:number
    }

// [返回]洛托姆之力升级
    interface LuoTuoMuUpRsp extends RspObj {
        // 升级次数
            upNum ?:number
        // 洛托姆之力数据
            ltmList ?:Array<LuoTuoMuInfoUnit>
        // 当前点
            curpoint ?:number
        // 当前属性列表
            curShuXingInfo ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
            yuLanShuXingInfo ?:Array<ShuXingInfoUnit>
        // 消耗道具的id
            costItemId ?:number
        // 消耗道具的数量   一次的数量
            costItemCount ?:number

    }

// [请求]进阶界面
    interface LuoTuoMuJinJiePanelReq extends ReqObj {
    }

// [返回]进阶界面
    interface LuoTuoMuJinJiePanelRsp extends RspObj {
        // 资源数据
            icoList ?:Array<LuoTuoMuDisplayInfoUnit>
        // 显示类型
            showType ?:number
        // 当前进阶等级
            level ?:number
        // 当前属性列表
            curShuXingInfo ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
            yuLanShuXingInfo ?:Array<ShuXingInfoUnit>
        // 进阶消耗
            costList ?:Array<JiangliInfoUnit>
        // 是否突破
            isTuPo ?:boolean

    }

// [请求]洛托姆之力进阶
    interface LuoTuoMuUpGradeReq extends ReqObj {
    }

// [返回]洛托姆之力进阶
    interface LuoTuoMuUpGradeRsp extends RspObj {
        // 1,成功
            ret ?:number

    }

// [推送]洛托姆之力信息
    interface PushLuoTuoMuJinJieInfoReq extends ReqObj {
    }

// [推送]洛托姆之力信息
    interface PushLuoTuoMuJinJieInfoRsp extends RspObj {
        // playerId
            playerId ?:number
        // 资源数据
            icoInfo ?:LuoTuoMuDisplayInfoUnit

    }

// [请求]切换皮肤
    interface ChangeDisplayReq extends ReqObj {
        // 显示类型
            showType ?:number
    }

// [回复]切换皮肤
    interface ChangeDisplayRsp extends RspObj {
        // 1,成功
            ret ?:number

    }

// 【请求】快捷购买体力
    interface QuickbuyTiliReq extends ReqObj {
    }

// 【反馈】快捷购买体力
    interface QuickbuyTiliRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// 【请求】快捷购买银两
    interface QuickbuyYinliangReq extends ReqObj {
        // 1 正常购买 2 一键购买
            status ?:number
    }

// 【反馈】快捷购买银两
    interface QuickbuyYinliangRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// 获取出海地区信息
    interface GetAreaListReq extends ReqObj {
    }

// 获取出海地区信息
    interface GetAreaListRsp extends RspObj {
        // 出海地区信息
            areaList ?:Array<ChuhaiAreaInfoUnit>

    }

// 获得当前船只信息
    interface GetShipInfoReq extends ReqObj {
    }

// 获得当前船只信息
    interface GetShipInfoRsp extends RspObj {
        // 船只信息
            shipInfo ?:ShipInfoUnit

    }

// 进行出海
    interface ChuhaiReq extends ReqObj {
        // 出海区域
            areaId ?:number
        // 消耗类型 1.金券 2.钻石
            costType ?:number
        // 出海次数
            times ?:number
    }

// 进行出海
    interface ChuhaiRsp extends RspObj {
        // 结果
            isSuccess ?:boolean
        // 更新船只信息
            shipInfo ?:ShipInfoUnit

    }

// 立即完成出海
    interface FinishShipReq extends ReqObj {
        // 消耗类型 1.金券 2.钻石
            costType ?:number
    }

// 立即完成出海
    interface FinishShipRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 更新船只信息
            shipInfo ?:ShipInfoUnit

    }

// 获取升级属性列表
    interface LevelUpListReq extends ReqObj {
    }

// 获取升级属性列表
    interface LevelUpListRsp extends RspObj {
        // 当前属性
            currentList ?:ShipPropUnit
        // 下一级属性
            nextList ?:ShipPropUnit
        // 升级所需道具id
            daoJuId ?:number
        // 升级所需道具count
            daoJuCount ?:number

    }

// 升级
    interface LevelUpReq extends ReqObj {
    }

// 升级
    interface LevelUpRsp extends RspObj {
        // 是否升级成功
            isSuccess ?:boolean
        // 更新船只信息
            shipInfo ?:ShipInfoUnit

    }

// 获取激活属性列表
    interface JihuoListReq extends ReqObj {
        // 船只编号
            shipNum ?:number
    }

// 获取激活属性列表
    interface JihuoListRsp extends RspObj {
        // 当前属性
            currentList ?:ShipPropUnit
        // 下一级属性
            nextList ?:ShipPropUnit
        // 所需道具id
            daoJuId ?:number
        // 所需道具count
            daoJuCount ?:number
        // 是否开启
            isOpen ?:boolean
        // 上一艘船只名称
            preShipName ?:string
        // 这一艘船只名称
            curShipName ?:string

    }

// 激活
    interface JihuoReq extends ReqObj {
        // 船只编号
            shipNum ?:number
    }

// 激活
    interface JihuoRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 更新船只信息
            shipInfo ?:ShipInfoUnit

    }

// 航海日志
    interface ShipLogReq extends ReqObj {
    }

// 航海日志
    interface ShipLogRsp extends RspObj {
        // 日志
            logList ?:Array<ShipLogInfoUnit>

    }

// 领取奖励
    interface GetShipRewardReq extends ReqObj {
        // 领取类型 0：单次领取 1：全部领取
            gettype ?:number
        // 道具defId
            defId ?:number
    }

// 领取奖励
    interface GetShipRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]专属神器显示属性
    interface ZhuanShuShenQiItemInfoReq extends ReqObj {
        // 请求神器DefId
            defId ?:number
    }

// [返回]专属神器显示属性
    interface ZhuanShuShenQiItemInfoRsp extends RspObj {
        // 专属神器描述
            zhuanShuDesc ?:ZhuanShuShenQiDescInfoUnit

    }

// [请求]领取专属神器
    interface GetZhuanShuShenQiItemReq extends ReqObj {
        // 请求神器DefId
            defId ?:number
    }

// [返回]领取专属神器
    interface GetZhuanShuShenQiItemRsp extends RspObj {
        // 是否成功
            success ?:number

    }

// 获取拍卖信息
    interface GetPaiMaiListReq extends ReqObj {
        // 0.公会 1.世界 2.我参与的 4.我上架的
            type ?:number
        // 页码
            pageNum ?:number
        // 筛选等级
            level ?:number
    }

// 获取拍卖信息
    interface GetPaiMaiListRsp extends RspObj {
        // 拍卖列表
            paimaiList ?:Array<PaiMaiWuPinInfoUnit>

    }

// 竞拍物品
    interface JingPaiReq extends ReqObj {
        // 0.加价 1.一口价
            type ?:number
        // 购买物品的dbId
            itemDbId ?:number
    }

// 竞拍物品
    interface JingPaiRsp extends RspObj {
        // 拍卖物品信息
            paimaiItem ?:PaiMaiWuPinInfoUnit
        // 竞拍结果 0.失败 1.出价成功 2.竞拍成功
            success ?:number

    }

// 竞拍物品分类列表
    interface GetPaiMaiLabelReq extends ReqObj {
    }

// 竞拍物品分类列表
    interface GetPaiMaiLabelRsp extends RspObj {
        // 分类列表
            labelList ?:Array<PaiMaiLabelInfoUnit>

    }

// 交易记录
    interface GetPaiMaiLogReq extends ReqObj {
    }

// 交易记录
    interface GetPaiMaiLogRsp extends RspObj {
        // 交易记录
            paiMaiLog ?:Array<PaiMaiLogInfoUnit>

    }

// 上架物品
    interface ShangJiaPaiMaiReq extends ReqObj {
        // 物品类型
            itemType ?:number
        // 上架物品的defId
            itemDefId ?:number
        // 上架物品的数量
            itemCount ?:number
        // 上架物品dbid
            itemDbId ?:number
    }

// 上架物品
    interface ShangJiaPaiMaiRsp extends RspObj {
        // 拍卖物品信息
            paimaiItem ?:PaiMaiWuPinInfoUnit
        // 竞拍结果
            success ?:boolean

    }

// 拍卖物品背包
    interface PaiMaiBeiBaoReq extends ReqObj {
        // 物品类型
            itemType ?:number
    }

// 拍卖物品背包
    interface PaiMaiBeiBaoRsp extends RspObj {
        // 拍卖物品背包
            paimaiItem ?:Array<PaiMaiBeiBaoInfoUnit>

    }

// 获取我的拍卖信息
    interface GetMyPaiMaiListReq extends ReqObj {
    }

// 获取我的拍卖信息
    interface GetMyPaiMaiListRsp extends RspObj {
        // 拍卖列表
            paimaiList ?:Array<PaiMaiWuPinInfoUnit>

    }

// 获取我的拍卖信息
    interface GetMyPaiMaiItemReq extends ReqObj {
        // 物品DBID
            itemDbId ?:number
    }

// 获取我的拍卖信息
    interface GetMyPaiMaiItemRsp extends RspObj {
        // 物品信息
            paimaiList ?:PaiMaiWuPinInfoUnit

    }

// 【请求】遗迹探险面板
    interface YiJiTanXianPannelReq extends ReqObj {
    }

// 【反馈】遗迹探险面板
    interface YiJiTanXianPannelRsp extends RspObj {
        // 当前消耗
            curCost ?:JiangliInfoUnit
        // 消耗上限
            maxCost ?:number
        // 当前已有的免费刷新次数
            curFreeFreshTimes ?:number
        // 刷新所需信息1
            refreshNeedFirst ?:JiangliInfoUnit
        // 刷新所需信息2（优先用1,1不足时用2）
            refreshNeedSecond ?:JiangliInfoUnit
        // 任务列表信息
            taskList ?:Array<YiJiTanXianTaskInfoUnit>

    }

// 【请求】遗迹探险刷新
    interface YiJiTanXianRefreshReq extends ReqObj {
    }

// 【反馈】遗迹探险刷新
    interface YiJiTanXianRefreshRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【请求】单条任务详细信息
    interface YiJiTanXianTaskDetailPannelReq extends ReqObj {
        // 任务dbId
            taskDbId ?:number
    }

// 【反馈】单条任务详细信息
    interface YiJiTanXianTaskDetailPannelRsp extends RspObj {
        // 任务dbId
            taskDbId ?:number
        // 任务中的武将列表
            taskHeroDbIds ?:Array<number>
        // 所需时间（毫秒）
            needTime ?:number
        // 所需星级
            needStar ?:number
        // 所需精灵类型列表（武将类型:1火系 2水系 3草系 4光系 5暗系 6全系）
            needHeroTypeList ?:Array<number>

    }

// 【请求】遗迹探险派出
    interface YiJiTanXianStartReq extends ReqObj {
        // 任务dbId
            taskDbId ?:number
        // 已选择的武将列表
            choosedHeroDbIds ?:Array<number>
    }

// 【反馈】遗迹探险派出
    interface YiJiTanXianStartRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 任务dbId
            taskDbId ?:number
        // 接取任务时间戳
            acceptTaskTime ?:number

    }

// 【请求】遗迹探险提前完成任务
    interface YiJiTanXianFinishTaskReq extends ReqObj {
        // 任务dbId
            taskDbId ?:number
    }

// 【反馈】遗迹探险提前完成任务
    interface YiJiTanXianFinishTaskRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 任务dbId
            taskDbId ?:number
        // 奖励数据
            bonusList ?:Array<JiangliInfoUnit>

    }

// 【请求】遗迹探险领取奖励
    interface YiJiTanXianReceiveBonusReq extends ReqObj {
        // 是否领取所有奖励
            receiveAll ?:boolean
        // 任务dbId
            taskDbId ?:number
    }

// 【反馈】遗迹探险领取奖励
    interface YiJiTanXianReceiveBonusRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 任务dbId
            taskDbId ?:number
        // 奖励数据
            bonusList ?:Array<JiangliInfoUnit>

    }

// 【请求】获得兑换列表
    interface GetDuihuanListReq extends ReqObj {
        // 类型 1.装备兑换 2.武将兑换 3.宝石兑换
            type ?:number
    }

// 【反馈】获得兑换列表
    interface GetDuihuanListRsp extends RspObj {
        // 兑换列表
            duihuanList ?:Array<DuihuanInfoUnit>
        // 上次刷新时间点
            freshTime ?:number
        // 类型 1.装备兑换 2.武将兑换 3.宝石兑换
            type ?:number

    }

// 【请求】兑换请求
    interface DoExchangeReq extends ReqObj {
        // 兑换索引ID
            duihuanSuoyinId ?:number
    }

// 【反馈】兑换请求
    interface DoExchangeRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]获得当前小关
    interface CurrXiaoGuanReq extends ReqObj {
    }

// [反馈]获得当前小关
    interface CurrXiaoGuanRsp extends RspObj {
        // 获得当前小关defId
            xiaoGuanDefId ?:number
        // 打了多少波小怪
            fightTimes ?:number
        // 当前地图
            currMap ?:number
        // 已经出现了多少次
            yiJingChuXian ?:number
        // 当前地图的天气  0代表没有天气
            tianQi ?:number

    }

// [请求]推图战斗
    interface TuiTuFightReq extends ReqObj {
        // 获得当前小关
            xiaoGuanDefId ?:number
        // 金币最终的总奖励信息
            jiangli ?:JiangliInfoUnit
    }

// [返回]推图战斗
    interface TuiTuFightRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 是否已经买过了一元礼包
            isHaveYiYuan ?:boolean
        // 一元礼包获得的通关钻石数
            yiYuanTeJiaCount ?:number
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 获得新的小关
            xiaoGuanDefId ?:number
        // 敌人头像id
            enemyTouxiangId ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>
        // 每小时 金币 经验果实 精灵币 奖励
            guaJiHourRewardList ?:Array<GuaJiHourRewardUnit>
        // 金猪关卡时的精灵宠物信息
            heroList ?:Array<TuiTuFightHeroUnit>
        // 可选神技卡列表
            itemList ?:Array<MagicalSkillItemUnit>
        // 胜利后的下个小关信息
            nextXiaoguanDef ?:XiaoguanDefUnit

    }

// [请求]推图战斗的Boss和奖励
    interface GetTuiTuFightBossAndRewardReq extends ReqObj {
        // 获得当前小关
            xiaoGuanDefId ?:number
    }

// [返回]推图战斗
    interface GetTuiTuFightBossAndRewardRsp extends RspObj {
        // 金币奖励信息
            jiangli ?:JiangliInfoUnit
        // boss怪物信息
            boss ?:GuaiwuDefUnit

    }

// [请求]碾压
    interface CompleteSuppressionReq extends ReqObj {
    }

// [返回]碾压
    interface CompleteSuppressionRsp extends RspObj {
        // 碾压的小关和隐藏精英怪结果信息
            completeSuppressionInfoList ?:Array<CompleteSuppressionInfoUnit>
        // 获得新的小关
            xiaoGuanDefId ?:number
        // 汇总后的奖励列表
            totalRewardList ?:Array<JiangliInfoUnit>

    }

// 【请求】更新Rogoulike地下城的Diy json数据
    interface UpdateRogoulikeDiyStringReq extends ReqObj {
        // diy字符串数据，前端存入的json列表。
            diyStringList ?:Array<DiyStringUnit>
        // 是否覆盖FieldName列表，覆盖的意思是只保留这次保存的所有fieldName，本次不传的将被删除掉。
            isOverride ?:boolean
    }

// 【反馈】更新Rogoulike地下城的Diy json数据
    interface UpdateRogoulikeDiyStringRsp extends RspObj {
        // 成功
            isSuccess ?:boolean

    }

// [请求]Rogoulike隐藏精英怪战斗
    interface RogoulikeHideGuaiwuFightReq extends ReqObj {
        // Rogoulike隐藏精英怪DefId
            defId ?:number
    }

// [返回]Rogoulike隐藏精英怪战斗
    interface RogoulikeHideGuaiwuFightRsp extends RspObj {
        // Rogoulike隐藏精英怪DefId
            defId ?:number
        // 可选神技卡列表
            itemList ?:Array<MagicalSkillItemUnit>
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]Rogoulike初始化新的一层数据
    interface InitRogoulikeCengDataReq extends ReqObj {
        // 要初始化第几层
            ceng ?:number
    }

// [返回]Rogoulike初始化新的一层数据
    interface InitRogoulikeCengDataRsp extends RspObj {
        // 要初始化第几层
            ceng ?:number
        // 小关关卡id
            defId ?:number
        // 额外点击元素集合列表
            extraElementList ?:Array<RogoulikeExtraElementInfoUnit>
        // 普通层的地牢
            dungeon ?:RogoulikeCengDungeonUnit
        // 是否能碾压
            isCanCompleteSuppression ?:boolean

    }

// [请求]Rogoulike地下城装备刚刚获得的神技卡
    interface PutOnMagicalSkillAtRogoulikeReq extends ReqObj {
        // 装备的神魔DbId
            heroDbId ?:number
        // 对应的神技道具DbId列表。
            dbIdList ?:Array<number>
        // 是否战斗后选卡
            isSelectRewardAfterRogoulikeFight ?:boolean
    }

// [返回]Rogoulike地下城装备刚刚获得的神技卡
    interface PutOnMagicalSkillAtRogoulikeRsp extends RspObj {
        // 变化的神技卡列表
            entityValueList ?:Array<EntityValueUnit>
        // 是否战斗后选卡
            isSelectRewardAfterRogoulikeFight ?:boolean
        // 装备的神魔DbId
            heroDbId ?:number
        // 神技协作效果列表
            magicalSkillCooperateList ?:Array<MagicalSkillCooperateUnit>
        // 神技强度
            heroFightIncrease ?:number

    }

// [请求]加载所有神技卡数据
    interface GetAllRogoulikeMagicalSkillsReq extends ReqObj {
    }

// [返回]加载所有神技卡数据
    interface GetAllRogoulikeMagicalSkillsRsp extends RspObj {
        // 所有地下城中的神技卡信息
            magicalSkillItemList ?:Array<MagicalSkillItemUnit>
        // 尚未挑选的神技卡列表
            notSelectItemList ?:Array<MagicalSkillItemUnit>
        // 尚未挑选的神技卡哪里来的：1、领主boss；2、隐藏精英怪
            notSelectItemSourceType ?:number
        // 上阵神魔神技扩展数据列表
            heroList ?:Array<RogoulikeHeroMagicalSkillExtendUnit>

    }

// [请求]进入Rogoulike地图
    interface EnterRogoulikeMapReq extends ReqObj {
        // 是否同时返回Rogoulike信息
            isGetRogoulikeInfo ?:boolean
    }

// [返回]进入Rogoulike地图
    interface EnterRogoulikeMapRsp extends RspObj {
        // 是否同时返回Rogoulike信息
            isGetRogoulikeInfo ?:boolean
        // Rogoulike地下城数据，isGetRogoulikeInfo=true时返回
            rogoulikeInfo ?:RogoulikeInfoUnit

    }

// [请求]领奖来自Rogoulike小怪和宝箱
    interface GetRewardFromRogoulikeXiaoguaiAndBaoxiangReq extends ReqObj {
        // 领奖时所在Scene：1Rogoulike 2Dungeon
            sceneType ?:number
        // 当前在第几层领奖
            ceng ?:number
        // 1、小怪；2：宝箱；3、罐子; 4、偶遇金猪；5：加血buff；6、加攻buff；
            type ?:number
        // id
            id ?:number
        // 是否仅仅是使其消失，而不领走奖励
            isOnlyClearIt ?:boolean
        // 剩余血量
            hp ?:number
        // 地牢当前层数
            layerNum ?:number
    }

// [返回]领奖来自Rogoulike小怪和宝箱
    interface GetRewardFromRogoulikeXiaoguaiAndBaoxiangRsp extends RspObj {
        // 领奖时所在Scene：1Rogoulike 2Dungeon
            sceneType ?:number
        // 当前在第几层领奖
            ceng ?:number
        // 1、小怪；2：宝箱；3、罐子; 4、偶遇金猪；5：加血buff；6、加攻buff；7：同步HP
            type ?:number
        // id
            id ?:number
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>
        // 是否仅仅是使其消失，而不领走奖励
            isOnlyClearIt ?:boolean
        // 剩余血量
            hp ?:number
        // 地牢当前层数
            layerNum ?:number

    }

// [请求]Rogoulike已开启的章节列表
    interface GetRogoulikeChapterListReq extends ReqObj {
    }

// [返回]Rogoulike已开启的章节列表
    interface GetRogoulikeChapterListRsp extends RspObj {
        // 当前章节信息
            currChapter ?:RogoulikeChapterInfoUnit

    }

// [请求]Rogoulike当前章节获得的所有奖励
    interface GetRogoulikeAllRewardsReq extends ReqObj {
        // 是否切换章节
            isSwitchChapter ?:boolean
    }

// [返回]Rogoulike当前章节获得的所有奖励
    interface GetRogoulikeAllRewardsRsp extends RspObj {
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>
        // 是否切换章节
            isSwitchChapter ?:boolean
        // 神技结算奖励列表
            magicalSkillSettlementList ?:Array<RogoulikeMagicalSkillSettlementUnit>

    }

// [请求]Rogoulike开启新的章节
    interface NewRogoulikeChapterReq extends ReqObj {
    }

// [返回]Rogoulike开启新的章节
    interface NewRogoulikeChapterRsp extends RspObj {
        // Rogoulike地下城数据
            rogoulikeInfo ?:RogoulikeInfoUnit

    }

// [请求]确认离开Rogoulike迷宫
    interface ConfirmToLeaveRogoulikeReq extends ReqObj {
    }

// [请求]Rogoulike秘境发奖
    interface GetRogoulikeMysteryRewardsReq extends ReqObj {
        // 秘境唯一Id，前后端发奖用
            id ?:number
    }

// [返回]Rogoulike秘境发奖
    interface GetRogoulikeMysteryRewardsRsp extends RspObj {
        // 秘境唯一Id，前后端发奖用
            id ?:number
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// [请求]Rogoulike秘境战斗
    interface RogoulikeMysteryFightReq extends ReqObj {
        // 秘境唯一Id，前后端发奖用
            id ?:number
        // 金猪秘境：金币最终的总奖励信息
            jiangli ?:JiangliInfoUnit
    }

// [返回]Rogoulike秘境战斗
    interface RogoulikeMysteryFightRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 金猪秘境时的精灵宠物信息
            heroList ?:Array<TuiTuFightHeroUnit>

    }

// [请求]Rogoulike秘境金猪奖励上限
    interface GetRogoulikeMysteryLimitRewardsReq extends ReqObj {
        // 秘境唯一Id，前后端发奖用
            id ?:number
    }

// [返回]Rogoulike秘境金猪奖励上限
    interface GetRogoulikeMysteryLimitRewardsRsp extends RspObj {
        // 秘境唯一Id，前后端发奖用
            id ?:number
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// [推送]Rogoulike地下城信息推送
    interface PushRogoulikeChangeDataRsp extends RspObj {
        // 玩家当前的攻击和血量发生变化 hp:113, attack:110
            changePropertyList ?:Array<HeroPropertyInfoUnit>

    }

// 【请求】Rogoulike神技属性查看
    interface RogoulikeMagicalSkillCardViewReq extends ReqObj {
        // 要查看的技能卡或者背包物品dbID
            dbId ?:number
    }

// 【反馈】Rogoulike神技属性查看
    interface RogoulikeMagicalSkillCardViewRsp extends RspObj {
        // 要查看的神技卡信息
            cardInfo ?:MagicalSkillCardUnit

    }

// [请求]地牢怪战斗
    interface RogoulikeDungeonMonsterFightReq extends ReqObj {
        // 当前第几层
            layerNum ?:number
    }

// [返回]地牢怪战斗
    interface RogoulikeDungeonMonsterFightRsp extends RspObj {
        // 当前第几层
            layerNum ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 战斗后，新的重置消耗的金券
            resetCost ?:JiangliInfoUnit

    }

// [请求]地牢重置
    interface RogoulikeDungeonResetReq extends ReqObj {
        // 重置第几层的地牢
            ceng ?:number
    }

// [返回]地牢重置
    interface RogoulikeDungeonResetRsp extends RspObj {
        // 重置第几层的地牢
            ceng ?:number
        // 重置后的地牢信息
            dungeon ?:RogoulikeCengDungeonUnit

    }

// [请求]迷宫内传送功能面板
    interface ChuansongGuanghuanPanelReq extends ReqObj {
    }

// [返回]迷宫内传送功能面板
    interface ChuansongGuanghuanPanelRsp extends RspObj {
        // 是否已经激活
            isActive ?:boolean
        // 未激活时的可激活方式：1新服专享激活；2指定充值项目激活
            activeType ?:number
        // 新服专享ProjectId，activeType=1 时赋值
            projectId ?:number
        // 新服专享赠送的激活道具信息，activeType=1 时赋值，否则为undefined
            activeItem ?:JiangliInfoUnit
        // 充值金额表Id，activeType=2 时赋值
            chongzhiJineDefId ?:number

    }

// [请求]Rogoulike中玩家被打死了
    interface RogoulikePlayerIsDeadReq extends ReqObj {
        // 被杀死时所在Scene：1Rogoulike 2Dungeon
            sceneType ?:number
    }

// [返回]Rogoulike中玩家被打死了
    interface RogoulikePlayerIsDeadRsp extends RspObj {
        // 被杀死时所在Scene：1Rogoulike 2Dungeon
            sceneType ?:number
        // 禁止进入时间戳，单位：毫秒
            forbiddenTime ?:number

    }

// [请求]奇遇礼包面板
    interface QiYuLiBaoPanelReq extends ReqObj {
    }

// [返回]奇遇礼包面板
    interface QiYuLiBaoPanelRsp extends RspObj {
        // 奇遇礼包列表
            qiYuLiBaoInfoList ?:Array<QiYuLiBaoInfoUnit>
        // 礼包持续时间 要要做倒计时  单位 秒
            deadTime ?:number
        // 打折百分比
            percent ?:number

    }

// [请求]购买奇遇礼包
    interface BuyQiYuLiBaoReq extends ReqObj {
        // 礼包dbId
            dbId ?:number
    }

// [返回]购买奇遇礼包
    interface BuyQiYuLiBaoRsp extends RspObj {
        // 
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]领取奇遇礼包
    interface GetQiYuLiBaoReq extends ReqObj {
        // 礼包dbId
            dbId ?:number
    }

// [返回]领取奇遇礼包
    interface GetQiYuLiBaoRsp extends RspObj {
        // 
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】请求获取的称号ID list
    interface LoadPlayerTitleIdListReq extends ReqObj {
    }

// 【反馈】请求获取的称号IDlist
    interface LoadPlayerTitleIdListRsp extends RspObj {
        // 称号IDlist
            idList ?:Array<number>
        // 未查看的称号IDlist
            newIdList ?:Array<number>
        // 累积击杀数量
            pvpKill ?:number
        // 当前拥有互助点数
            dianShu ?:number
        // 当前战棋胜场数
            zhanQiShengChang ?:number

    }

// 【请求】获取称号接口 （消耗代金券）
    interface BuyPlayerTitleReq extends ReqObj {
        // 购买的称号ID
            id ?:number
        // 是否立即自动穿戴
            isAutoPutOn ?:boolean
    }

// 【反馈】获取称号接口 （消耗代金券）
    interface BuyPlayerTitleRsp extends RspObj {
        // 新购买的称号ID, 购买失败,不传递此参数,JS中为undefined
            id ?:number
        // 是否自动使用当前新购买的称号
            isAutoChange ?:boolean

    }

// 【请求】使用称号接口
    interface ChangePlayerTitleReq extends ReqObj {
        // 称号ID
            id ?:number
    }

// 【反馈】使用称号接口
    interface ChangePlayerTitleRsp extends RspObj {
        // 新称号ID
            id ?:number

    }

// 【请求】查看新获取的称号ID
    interface ReadPlayerTitleReq extends ReqObj {
        // 第一次查看的称号ID,如果之前查看过了,则不要重复发送该消息
            id ?:number
    }

// 【请求】请求获取的头像框ID list
    interface LoadTouxiangkuangIdListReq extends ReqObj {
        // 1、头像框；2、聊天框
            type ?:number
    }

// 【反馈】请求获取的头像框IDlist
    interface LoadTouxiangkuangIdListRsp extends RspObj {
        // 1、头像框；2、聊天框
            type ?:number
        // 已经激活的头像框表IDlist
            defIdList ?:Array<number>
        // 当前正在使用的头像框
            currentDefId ?:number

    }

// 【请求】激活头像框接口
    interface ActiveTouxiangkuangReq extends ReqObj {
        // 1、头像框；2、聊天框
            type ?:number
        // 购买的头像框ID
            defId ?:number
        // 是否立即自动穿戴
            isAutoPutOn ?:boolean
    }

// 【反馈】激活头像框接口
    interface ActiveTouxiangkuangRsp extends RspObj {
        // 1、头像框；2、聊天框
            type ?:number
        // 新购买的头像框ID, 购买失败,不传递此参数,JS中为undefined
            defId ?:number
        // 是否自动使用当前新购买的头像框
            isAutoChange ?:boolean

    }

// 【请求】使用头像框接口
    interface ChangeTouxiangkuangReq extends ReqObj {
        // 1、头像框；2、聊天框
            type ?:number
        // 头像框表ID
            defId ?:number
    }

// 【反馈】使用头像框接口
    interface ChangeTouxiangkuangRsp extends RspObj {
        // 1、头像框；2、聊天框
            type ?:number
        // 新头像框ID
            defId ?:number

    }

// [请求]公会战报名
    interface GongHuiZhanBaoMingReq extends ReqObj {
        // 工会id
            gonghuiId ?:number
    }

// [返回]公会战报名状态
    interface GongHuiZhanBaoMingRsp extends RspObj {
        // 0:成功 1：失败
            status ?:number

    }

// [请求]阵型调整
    interface GongHuiGroupTiaoZhengReq extends ReqObj {
        // 公会战1路列表玩家id
            battle1list ?:Array<number>
        // 公会战2路列表玩家id
            battle2list ?:Array<number>
        // 公会战3路列表玩家id
            battle3list ?:Array<number>
    }

// [返回]公会战调整是否成功
    interface GongHuiGroupTiaoZhengRsp extends RspObj {
        // 0：成功 1：单队长度错误 2：总数不和当前人员匹配
            status ?:number

    }

// [请求]公会战阵型
    interface GongHuiZhanZhenXingReq extends ReqObj {
    }

// [返回]公会战阵型
    interface GongHuiZhanZhenXingRsp extends RspObj {
        // 公会战1路列表
            battle1list ?:Array<GongHuiBattleMemberInfoUnit>
        // 公会战2路列表
            battle2list ?:Array<GongHuiBattleMemberInfoUnit>
        // 公会战3路列表
            battle3list ?:Array<GongHuiBattleMemberInfoUnit>

    }

// 历史回放界面
    interface HisToryPanelReq extends ReqObj {
    }

// 历史回放界面
    interface HisToryPanelRsp extends RspObj {
        // 最后的公会对战记录
            gonghuiInfo ?:GongHuiZhanFightUnit
        // 个人最后的对战记录
            personInfo ?:GongHuiZhanMemberFightUnit

    }

// 返回排行榜全部
    interface ShowGHZRankAllReq extends ReqObj {
    }

// 返回排行榜全部
    interface ShowGHZRankAllRsp extends RspObj {
        // 我的排名
            ownRankIndex ?:number
        // 前32名
            rankall ?:Array<GongHuiZhanRankInfoUnit>

    }

// 公会战展示主界面
    interface ShowGHZPanelReq extends ReqObj {
    }

// 公会战展示主界面
    interface ShowGHZPanelRsp extends RspObj {
        // 前3名
            rankTop3 ?:Array<GongHuiZhanRankInfoUnit>
        // 自己公会信息
            OwnRank ?:GongHuiZhanRankInfoUnit
        // 报名开始时间
            baomingStartTime ?:number
        // 报名截止时间
            baomingEndTime ?:number
        // 切换指令时间
            changeTime ?:number
        // 0：未报名 1：已报名”
            isbaoming ?:number
        // 当前服务器时间
            nowTime ?:number
        // 服务器跨服区间
            serverIds ?:string

    }

// 公会战战斗主界面
    interface ShowGHZFightReq extends ReqObj {
    }

// 公会战战斗主界面
    interface ShowGHZFightRsp extends RspObj {
        // 对战双方
            fightList ?:Array<GongHuiZhanFightUnit>
        // 0：海选 1：32强淘汰赛 2:16强 3：8强 4：4强 5：决赛
            fightRound ?:number
        // 0：可调阵状态 1：不可调阵等待战斗 2：战斗中
            fightState ?:number
        // 切换下个状态的时间
            nextstateTime ?:number
        // 回合数
            round ?:number

    }

// [请求]公会战集体回放
    interface GongHuiZhanJiTiHuiFangReq extends ReqObj {
        // 日志来自哪个服务器
            serverId ?:number
        // 
            PVPFightLogId ?:number
    }

// [返回]公会战集体回放
    interface GongHuiZhanJiTiHuiFangRsp extends RspObj {
        // 公会1的第一小队
            gongHui1Battle1list ?:Array<GongHuiZhanMemberHuiFangInfoUnit>
        // 公会1的第二小队
            gongHui1Battle2list ?:Array<GongHuiZhanMemberHuiFangInfoUnit>
        // 公会1的第三小队
            gongHui1Battle3list ?:Array<GongHuiZhanMemberHuiFangInfoUnit>
        // 公会2的第一小队
            gongHui2Battle1list ?:Array<GongHuiZhanMemberHuiFangInfoUnit>
        // 公会2的第二小队
            gongHui2Battle2list ?:Array<GongHuiZhanMemberHuiFangInfoUnit>
        // 公会2的第三小队
            gongHui2Battle3list ?:Array<GongHuiZhanMemberHuiFangInfoUnit>

    }

// [请求]收到其他服务器发过来的 公会战pvp战斗记录id
    interface WatchGongHuiZhanPVPReq extends ReqObj {
        // 
            fightLogServerId ?:number
        // 
            fightLogDBId ?:number
    }

// [返回]收到其他服务器发过来的 公会战pvp战斗记录id
    interface WatchGongHuiZhanPVPRsp extends RspObj {
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string

    }

// 获取跟随宠信息
    interface GetFollowPetListReq extends ReqObj {
    }

// 获取跟随宠信息
    interface GetFollowPetListRsp extends RspObj {
        // 跟随宠信息
            followPets ?:Array<FollowPetUnit>
        // 跟随宠皮肤id
            followPetPiFuId ?:number

    }

// 进入跟随宠房间
    interface EnterFollowPetRoomReq extends ReqObj {
    }

// 进入跟随宠房间
    interface EnterFollowPetRoomRsp extends RspObj {
        // 是否是自己房间
            isSelfRoom ?:boolean
        // 生育率
            rearRate ?:number
        // 最大次数
            maxTimes ?:number
        // 剩余寄养次数
            leftTimes ?:number
        // 点赞次数
            dianZanCount ?:number
        // 寄养宠物信息
            parkPet ?:PetRoomParkPetUnit
        // 房间信息
            roomDecorate ?:Array<PetRoomDecorateUnit>
        // 房间成长值
            heroPropertyInfos ?:Array<HeroPropertyInfoUnit>
        // 我的剩余可点赞次数
            myLeftCount ?:number
        // 房间是否上锁
            isLock ?:boolean
        // 房间公告
            gonggao ?:string
        // 剩余输入密码错误次数
            myLeftErrorCount ?:number
        // 是否设置了自动跟随
            isAuto ?:boolean
        // 我的房间号
            myRoomId ?:number
        // 房间是否可以通过消耗道具进入
            isDaoJu ?:boolean
        // 房间饰品信息
            shiPinList ?:Array<ShiPinPositionUnit>
        // 跟随宠皮肤
            petPiFuId ?:number

    }

// 升级装饰面板
    interface PetRoomDecoratePanelReq extends ReqObj {
        // 装饰类型
            type ?:number
    }

// 升级装饰面板
    interface PetRoomDecoratePanelRsp extends RspObj {
        // 属性信息
            curPropertyInfo ?:Array<ShuXingInfoUnit>
        // 属性信息
            nextPropertyInfos ?:Array<ShuXingInfoUnit>
        // 消耗道具ID
            costItemId ?:number
        // 消耗道具数量
            costItemCount ?:number
        // 该类型装饰当前等级
            curLevel ?:number
        // 该类型装饰是否已经满级
            isFull ?:boolean
        // 已经点了几下
            hasExp ?:number
        // 需要点几下能达到满级
            needExp ?:number

    }

// 升级装饰
    interface PetRoomDecorateLvUpReq extends ReqObj {
        // 装饰类型
            decorateType ?:number
        // 升级方式 1 普通升级 2 一键升级
            upType ?:number
    }

// 升级装饰
    interface PetRoomDecorateLvUpRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 返回自己房间
    interface BackFollowPetRoomReq extends ReqObj {
    }

// 返回自己房间
    interface BackFollowPetRoomRsp extends RspObj {
        // 寄养宠物信息
            parkPet ?:PetRoomParkPetUnit
        // 装饰信息
            roomDecorate ?:Array<PetRoomDecorateUnit>
        // 房间成长值
            heroPropertyInfos ?:Array<HeroPropertyInfoUnit>
        // 我的房间点赞数
            myDianZanCount ?:number
        // 我的房间公告
            myGongGao ?:string
        // 我的房间号
            myRoomId ?:number
        // 是否设置了自动孵化
            isAuto ?:boolean
        // 是否设置了通过消耗道具进入房间
            isDaoJu ?:boolean
        // 是否上锁
            isLock ?:boolean
        // 房间饰品
            shiPinList ?:Array<ShiPinPositionUnit>
        // 我的跟随宠皮肤
            myPetPiFuId ?:number

    }

// 选择装饰
    interface GetPetRoomDecorateReq extends ReqObj {
        // 装饰类型
            type ?:number
        // 策划表id
            defId ?:number
    }

// 选择装饰
    interface GetPetRoomDecorateRsp extends RspObj {
        // 是否装饰成功
            isSuccess ?:boolean

    }

// 宠物传承
    interface GetPetChuanChengReq extends ReqObj {
        // 传授宠物id
            fromPetId ?:number
        // 继承宠物id
            toPetId ?:number
        // 传授属性类型列表
            fromPropertyTypes ?:Array<number>
        // 继承属性类型列表
            toPropertyTypes ?:Array<number>
    }

// 宠物传承
    interface GetPetChuanChengRsp extends RspObj {
        // 是否传承成功
            isSuccess ?:boolean

    }

// 宠物成长
    interface GetPetChengZhangReq extends ReqObj {
        // 培养宠物id
            goalPetId ?:number
        // 1 宠物 2 道具
            materialList ?:Array<ShenQiXiaoHaoInfoUnit>
    }

// 宠物成长
    interface GetPetChengZhangRsp extends RspObj {
        // 是否成长成功
            isSuccess ?:boolean

    }

// 跟随宠物孵化
    interface GetPetFuHuaReq extends ReqObj {
        // 消耗类型:1.宠物蛋；2.金券
            costType ?:number
        // 消耗道具数量
            costCount ?:number
    }

// 跟随宠物孵化
    interface GetPetFuHuaRsp extends RspObj {
        // 跟随宠信息
            followPet ?:Array<FollowPetUnit>

    }

// 随机房间
    interface FollowPetRdmRoomReq extends ReqObj {
    }

// 随机房间
    interface FollowPetRdmRoomRsp extends RspObj {
        // 属性信息
            followPetRdmRoom ?:Array<FollowPetRdmRoomUnit>

    }

// 寄养
    interface FollowPetParkPetReq extends ReqObj {
        // 寄养在谁家
            playerId ?:number
        // 输入的密码
            inputPassword ?:string
        // 是否扣道具成功
            useDaoJu ?:boolean
        // 是否是通过搜索房间号发起的寄养
            isSearch ?:boolean
        // 目标serverId
            goalServerId ?:number
        // 是否是公会入口触发的寄养
            isGongHui ?:boolean
    }

// 寄养
    interface FollowPetParkPetRsp extends RspObj {
        // 0:成功 1:失败 2:密码输入错误 3:免密进房间米有同步的情况 4 密码设置未同步
            result ?:number
        // 是否可以免密进入
            isDaoJu ?:boolean

    }

// 改变跟随状态
    interface ChangeFollowReq extends ReqObj {
        // 设置跟随宠物ID
            targetPetId ?:number
    }

// 改变跟随状态
    interface ChangeFollowRsp extends RspObj {
        // 改变是否成功
            result ?:boolean

    }

// 检测是否有可孵化的蛋
    interface FollowPetCheckChildReq extends ReqObj {
    }

// 检测是否有可孵化的蛋
    interface FollowPetCheckChildRsp extends RspObj {
        // 0:没有子代 1:有了代
            result ?:number
        // 跟随宠蛋
            followPetEgg ?:FollowPetEggUnit

    }

// 孵化子代
    interface FollowPetHatchChildReq extends ReqObj {
        // 选择哪个属性
            propTypes ?:Array<number>
    }

// 孵化子代
    interface FollowPetHatchChildRsp extends RspObj {
        // 是否还有蛋
            isHave ?:boolean
        // 跟随宠蛋
            nextFollowPetEgg ?:FollowPetEggUnit

    }

// 提高生育率
    interface FollowPetUpRearRateReq extends ReqObj {
        // 0:自己寄养的宠物房间 1:被别人寄养的房间
            type ?:number
        // 提升的生育率
            rearRate ?:number
    }

// 提高生育率
    interface FollowPetUpRearRateRsp extends RspObj {
        // 生育率
            rearRate ?:number

    }

// 点赞
    interface FollowPetDianZanReq extends ReqObj {
        // 给谁点赞
            goalPlayerId ?:number
        // 目标玩家服务器长id 用于跨服
            goalServerId ?:number
        // 目标玩家服务器号
            goalServerNo ?:number
    }

// 点赞
    interface FollowPetDianZanRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 获得点赞
    interface GetFollowPetDianZanReq extends ReqObj {
        // 给谁点赞
            goalPlayerId ?:number
        // 目标玩家服务器长id 用于跨服
            goalServerId ?:number
        // 目标玩家服务器号
            goalServerNo ?:number
    }

// 获得点赞
    interface GetFollowPetDianZanRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 踢人
    interface FollowPetTiRenReq extends ReqObj {
        // 踢掉谁
            goalPlayerId ?:number
        // 踢掉的那个人的服务器长id
            goalPlayerServerId ?:number
        // 踢掉的那个人的服务器号
            goalPlayerServerNo ?:number
    }

// 踢人
    interface FollowPetTiRenRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 获取寄养日志
    interface GetFollowPetJiYangRecordReq extends ReqObj {
    }

// 获取寄养日志
    interface GetFollowPetJiYangRecordRsp extends RspObj {
        // 日志列表
            recordList ?:Array<JiYangRecordUnit>

    }

// 本服寄养预览 公会或者排行榜
    interface BenFuJiYangYuLanReq extends ReqObj {
        // 目标玩家id
            goalPlayerId ?:number
        // 是不是从公会进来的
            isGongHui ?:boolean
    }

// 本服寄养预览 公会或者排行榜
    interface BenFuJiYangYuLanRsp extends RspObj {
        // 房间信息
            followPetRoomUnit ?:FollowPetRdmRoomUnit
        // 是否已经被寄养
            isJiYang ?:boolean
        // 寄养宠物的信息
            jiYangPetInfo ?:FollowPetUnit
        // 我的剩余点赞数
            myLeftCount ?:number
        // 我的剩余输错密码数
            myLeftErrorCount ?:number

    }

// 提升代数
    interface IncreaseMyGenerationReq extends ReqObj {
    }

// 提升代数
    interface IncreaseMyGenerationRsp extends RspObj {
        // 是否提升成功
            isSuccess ?:boolean

    }

// 修改房间公告
    interface UpdatePetRoomGongGaoReq extends ReqObj {
        // 新的房间公告
            newGongGao ?:string
    }

// 修改房间公告
    interface UpdatePetRoomGongGaoRsp extends RspObj {
        // 是否修改成功
            isSuccess ?:boolean

    }

// 修改房间密码面板
    interface UpdatePasswordPanelReq extends ReqObj {
    }

// 修改房间密码面板
    interface UpdatePasswordPanelRsp extends RspObj {
        // 我的密码
            myPassword ?:string

    }

// 修改房间密码或道具状态
    interface UpdatePasswordOrItemReq extends ReqObj {
        // 新的房间密码
            newPassword ?:string
        // 新的状态
            isDaoJu ?:boolean
    }

// 修改房间密码或道具状态
    interface UpdatePasswordOrItemRsp extends RspObj {
        // 是否修改成功
            isSuccess ?:boolean

    }

// 按房间号搜索房间
    interface SearchFollowPetRoomReq extends ReqObj {
        // 服务器号
            serverNo ?:number
        // 房间号
            roomId ?:number
    }

// 按房间号搜索房间
    interface SearchFollowPetRoomRsp extends RspObj {
        // 属性信息
            followPetRdmRoom ?:FollowPetRdmRoomUnit
        // 是否已经被寄养
            isJiYang ?:boolean
        // 寄养宠物的信息
            jiYangPetInfo ?:FollowPetUnit
        // 错误信息
            message ?:string

    }

// 修改自动孵化上阵状态
    interface UpdateAutoReq extends ReqObj {
        // 是否设置为自动孵化自动跟随
            isAuto ?:boolean
    }

// 修改自动孵化上阵状态
    interface UpdateAutoRsp extends RspObj {
        // 是否修改成功
            isSuccess ?:boolean

    }

// 解锁
    interface UnLockReq extends ReqObj {
    }

// 解锁
    interface UnLockRsp extends RspObj {
        // 是否解锁成功
            isSuccess ?:boolean

    }

// 房间饰品面板
    interface PetRoomShiPinPanelReq extends ReqObj {
    }

// 房间饰品面板
    interface PetRoomShiPinPanelRsp extends RspObj {
        // 房间饰品信息
            shiPinList ?:Array<FollowPetRoomShiPinUnit>

    }

// 激活房间饰品
    interface AddPetRoomShiPinReq extends ReqObj {
        // 饰品类型
            shiPinType ?:number
    }

// 添加房间饰品
    interface AddPetRoomShiPinRsp extends RspObj {
        // 是否激活成功
            isSuccess ?:boolean

    }

// 放入房间饰品
    interface PutPetRoomShiPinReq extends ReqObj {
        // 饰品类型
            type ?:number
        // 是否是添加
            isAdd ?:boolean
    }

// 放入房间饰品
    interface PutPetRoomShiPinRsp extends RspObj {
        // 是否放入成功
            isSuccess ?:boolean

    }

// 调整饰品位置
    interface ChangeShiPinPositionReq extends ReqObj {
        // 新的饰品位置信息
            newPositionlist ?:Array<ShiPinPositionUnit>
    }

// 调整饰品位置
    interface ChangeShiPinPositionRsp extends RspObj {
        // 是否调整成功
            isSuccess ?:boolean

    }

// 跟随宠皮肤面板
    interface FollowPetPiFuPanelReq extends ReqObj {
    }

// 跟随宠皮肤面板
    interface FollowPetPiFuPanelRsp extends RspObj {
        // 皮肤面板
            piFuList ?:Array<FollowPetPiFuUnit>

    }

// 激活跟随宠皮肤
    interface GetFollowPetPiFuReq extends ReqObj {
        // 皮肤id
            piFuId ?:number
    }

// 激活跟随宠皮肤
    interface GetFollowPetPiFuRsp extends RspObj {
        // 是否激活成功
            isSuccess ?:boolean

    }

// 穿戴跟随宠皮肤
    interface PutOnFollowPetPiFuReq extends ReqObj {
        // 皮肤id
            piFuId ?:number
        // 1 穿上 2 脱下
            type ?:number
    }

// 穿戴跟随宠皮肤
    interface PutOnFollowPetPiFuRsp extends RspObj {
        // 是否穿戴成功
            isSuccess ?:boolean

    }

// 获取跟随宠皮肤信息
    interface GetFollowPetPiFuInfoReq extends ReqObj {
        // 跟随宠皮肤id
            piFuId ?:number
    }

// 获取跟随宠皮肤信息
    interface GetFollowPetPiFuInfoRsp extends RspObj {
        // 皮肤信息
            followPetPiFuInfo ?:FollowPetPiFuUnit

    }

// 获取跟随宠装饰红点
    interface GetPetDecorateRedPointReq extends ReqObj {
    }

// 获取跟随宠装饰红点
    interface GetPetDecorateRedPointRsp extends RspObj {
        // 是否能升阶
            isCan ?:boolean

    }

// [请求]新跟随宠界面
    interface NewFollowPetPanelReq extends ReqObj {
    }

// [返回]新跟随宠界面
    interface NewFollowPetPanelRsp extends RspObj {
        // 当前阶数
            curJieShu ?:number
        // 升级属性列表
            levelShuXingList ?:Array<ShuXingInfoUnit>
        // 进阶属性列表
            jinJieShuXingList ?:Array<ShuXingInfoUnit>
        // 红点列表
            redPointList ?:Array<BoolRedPointUnit>

    }

// [请求]新跟随宠强化界面
    interface NewFollowPetQiangHuaPanelReq extends ReqObj {
    }

// [返回]新跟随宠强化界面
    interface NewFollowPetQiangHuaPanelRsp extends RspObj {
        // 属性列表
            curShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 属性列表
            nextShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 当前等级
            curLevel ?:number
        // 消耗
            cost ?:JiangliInfoUnit
        // 是否满级
            isFull ?:boolean

    }

// [请求]新跟随宠进阶界面
    interface NewFollowPetJinJiePanelReq extends ReqObj {
    }

// [返回]新跟随宠强化界面
    interface NewFollowPetJinJiePanelRsp extends RspObj {
        // 资源数据
            icoList ?:Array<NewFollowPetShowUnit>
        // 显示类型
            showType ?:number
        // 当前进阶等级
            curLevel ?:number
        // 属性列表
            curShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 属性列表
            nextShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 消耗列表
            costList ?:Array<JiangliInfoUnit>
        // 是否突破
            isTuPo ?:boolean
        // 是否满级
            isFull ?:boolean

    }

// [请求]跟随宠升级或进阶
    interface GetNewFollowPetLvUpReq extends ReqObj {
        // 1 升级 2 进阶
            oType ?:number
        // 是否是一键升级
            isOneKey ?:boolean
    }

// [返回]跟随宠升级或进阶
    interface GetNewFollowPetLvUpRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]修改显示类型
    interface ChangeShowTypeReq extends ReqObj {
        // 显示类型
            showType ?:number
    }

// [返回]修改显示类型
    interface ChangeShowTypeRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]新跟随宠信息
    interface PushNewFollowPetInfoReq extends ReqObj {
    }

// [返回]新跟随宠信息
    interface PushNewFollowPetInfoRsp extends RspObj {
        // playerId
            playerId ?:number
        // 资源数据
            icoInfo ?:NewFollowPetShowUnit

    }

// [请求]玩家皮肤面板
    interface PiFuPanelReq extends ReqObj {
    }

// [返回]玩家皮肤面板
    interface PiFuPanelRsp extends RspObj {
        // 属性列表
            shuXingInfoList ?:Array<ShuXingInfoUnit>
        // 红点集合  顺序 翅膀 坐骑 皮肤 装饰  规则 那一类活动在 并且没激活
            hongDianList ?:Array<boolean>
        // 是不是存在这一类的皮肤 类型顺序参考策划表
            existPiFuByTypeList ?:Array<boolean>
        // 套装属性信息列表
            taozhuangInfoList ?:Array<PiFuTaozhuangInfoUnit>

    }

// [请求]玩家皮肤类型的集合
    interface PiFuTypeListReq extends ReqObj {
        // 时装的类型
            type ?:number
    }

// [返回]玩家皮肤类型的集合
    interface PiFuTypeListRsp extends RspObj {
        // 皮肤列表
            piFuInfoList ?:Array<PiFuInfoUnit>

    }

// [请求]穿戴皮肤
    interface ChuanDaiPiFuReq extends ReqObj {
        // 
            defId ?:number
        // true 穿  false 脱
            isWear ?:boolean
    }

// [返回]穿戴皮肤
    interface ChuanDaiPiFuRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]购买皮肤
    interface GouMaiPiFuReq extends ReqObj {
        // 
            defId ?:number
    }

// [返回]购买皮肤
    interface GouMaiPiFuRsp extends RspObj {
        // 
            isSuccess ?:boolean
        // 
            defId ?:number

    }

// [请求]进阶皮肤面板
    interface JinJiePiFuPanelReq extends ReqObj {
        // 
            defId ?:number
    }

// [返回]进阶皮肤面板
    interface JinJiePiFuPanelRsp extends RspObj {
        // 当前属性
            curShuXingList ?:Array<ShuXingInfoUnit>
        // 下一星属性
            nextShuXingList ?:Array<ShuXingInfoUnit>
        // 消耗皮肤数量
            costCount ?:number

    }

// [请求]进阶皮肤
    interface JinJiePiFuReq extends ReqObj {
        // 
            defId ?:number
    }

// [返回]进阶皮肤
    interface JinJiePiFuRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// 玩吧空间背景
    interface CoverRewardPanelReq extends ReqObj {
    }

// 玩吧空间背景
    interface CoverRewardPanelRsp extends RspObj {
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>
        // 领取状态 0 没领过 1 领过了
            status ?:number

    }

// 领取玩吧空间背景奖励
    interface GetCoverRewardReq extends ReqObj {
    }

// 领取玩吧空间背景奖励
    interface GetCoverRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 玩吧vip礼包面板
    interface WanBaVipRewardPanelReq extends ReqObj {
        // 我的vip等级
            myVipLevel ?:number
    }

// 玩吧vip礼包面板
    interface WanBaVipRewardPanelRsp extends RspObj {
        // 奖励列表
            rewardList ?:Array<WanBaVipRewardInfoUnit>

    }

// 领取玩吧vip礼包奖励
    interface GetWanBaVipRewardReq extends ReqObj {
        // 我的vip等级
            myVipLevel ?:number
        // 领取那个礼包
            defId ?:number
    }

// 领取玩吧vip礼包奖励
    interface GetWanBaVipRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// [请求]玩家养成线升级时，道具材料不足
    interface CultivateCardActionBeLackOfItemsReq extends ReqObj {
        // 当前操作的Project Id
            projectDefId ?:number
        // 根据操作的养成线来区分，如果是神魔类，则为HeroDbId，如果是守护兽类，则为守护兽DB Id，依次类推
            dbId ?:number
    }

// [请求]玩家养成线弹窗礼包推送
    interface PushCultivateCardActionGiftPackRsp extends RspObj {
        // 礼包Db Id
            dbId ?:number
        // 策划礼包表Id
            defId ?:number
        // 礼包资源图
            img ?:string
        // 打折美术图
            imgDiscount ?:string
        // 礼包过期时间戳，单位：毫秒
            expireMillis ?:number
        // 礼包中的奖励内容
            detailInfo ?:DanBiChongZhiDetailInfoUnit

    }

// [请求]购买养成线弹窗礼包
    interface BuyCultivateCardActionGiftPackReq extends ReqObj {
        // 礼包Db Id
            dbId ?:number
    }

// [请求]购买养成线弹窗礼包
    interface BuyCultivateCardActionGiftPackRsp extends RspObj {
        // 礼包Db Id
            dbId ?:number
        // 奖励内容
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]面板
    interface PanelReq extends ReqObj {
    }

// [返回]面板
    interface PanelRsp extends RspObj {
        // 当前体力
            currTiLi ?:number
        // 体力的上限
            tiLiShangXian ?:number
        // 下次体力恢复的时间  秒  用作倒计时
            refreshTime ?:number
        // 下一级def 如果满了 就不传
            nextDef ?:ZhanDouDiTieDefUnit

    }

// [请求]战斗
    interface DiTieFightReq extends ReqObj {
    }

// [返回]战斗
    interface DiTieFightRsp extends RspObj {
        // false 没有暴击  true 暴击了
            baoJi ?:boolean
        // 经验
            exp ?:number

    }

// [请求]扫荡
    interface SaoDangFightReq extends ReqObj {
        // 扫荡消耗的体力
            costTiLi ?:number
    }

// [返回]扫荡
    interface SaoDangFightRsp extends RspObj {
        // 暴击次数
            baoJiciShu ?:number
        // 经验
            exp ?:number

    }

// [请求]购买上限
    interface GouMaiShangXianReq extends ReqObj {
    }

// [返回]购买上限
    interface GouMaiShangXianRsp extends RspObj {
        // 当前体力
            currTiLi ?:number
        // 体力上限
            tiLiShangXian ?:number
        // 经验
            isSuccess ?:boolean
        // 下一级def 如果满了 就不传
            nextDef ?:ZhanDouDiTieDefUnit

    }

// 公共异常反馈
    interface PublicErrorRsp extends RspObj {
        // 请求指令主编码
            requestMainCode ?:number
        // 请求指令子编码
            requestSubCode ?:number
        // 错误代码编号
            errorCode ?:number
        // 替换文本
            replaceContents ?:Array<string>

    }

// 游戏管理接口请求
    interface GameManageReq extends ReqObj {
        // 请求指令编码
            requestCode ?:string
    }

// 游戏管理接口反馈
    interface GameManageRsp extends RspObj {
        // 处理结果
            result ?:string

    }

// 玩家更换头像
    interface ChangePhotoReq extends ReqObj {
        // 武将ID
            wujiangId ?:number
    }

// 玩家更换头像
    interface ChangePhotoRsp extends RspObj {
        // 是否更换成功
            isSuccess ?:boolean

    }

// 玩家更换名称
    interface ChangeNameReq extends ReqObj {
        // 玩家新的名称
            playerName ?:string
    }

// 玩家更换名称
    interface ChangeNameRsp extends RspObj {
        // 是否更换成功
            isSuccess ?:boolean
        // 玩家新的名称
            playerName ?:string

    }

// 礼品码兑换
    interface UseGiftCodeReq extends ReqObj {
        // 礼品码Code
            gitfCode ?:string
        // 渠道
            channelAlias ?:string
    }

// 礼品码兑换
    interface UseGiftCodeRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 是否更换成功
            isSuccess ?:boolean

    }

// 【反馈】玩家已经拥有过的武将列表
    interface GetHeroLogListReq extends ReqObj {
        // 玩家已经拥有过的武将列表
            heroLogList ?:Array<HeroLogInfoUnit>
    }

// 【反馈】玩家已经拥有过的武将列表
    interface GetHeroLogListRsp extends RspObj {
        // 玩家已经拥有过的武将列表
            heroLogList ?:Array<HeroLogInfoUnit>
        // 图鉴总属性
            totalShuXingList ?:Array<ShuXingInfoUnit>
        // 图鉴累计钻石
            totalZuanShiCount ?:number

    }

// 【反馈】获取精灵图鉴面板
    interface GetHeroTuJianPanelReq extends ReqObj {
        // 精灵Id
            heroId ?:number
    }

// 【反馈】获取精灵图鉴面板
    interface GetHeroTuJianPanelRsp extends RspObj {
        // 图鉴当前等级
            curLevel ?:number
        // 当前图鉴的属性
            shuXingInfoList ?:Array<ShuXingInfoUnit>

    }

// 【反馈】领取精灵图鉴奖励
    interface GetHeroTuJianRewardReq extends ReqObj {
    }

// 【反馈】领取精灵图鉴奖励
    interface GetHeroTuJianRewardRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 【请求】领取VIP奖励
    interface GetVipJiangliReq extends ReqObj {
        // vip静态ID
            vipId ?:number
    }

// 【反馈】领取VIP奖励
    interface GetVipJiangliRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // vip静态ID
            vipId ?:number
        // VIP礼包奖励列表
            vipGiftRewardList ?:Array<JiangliInfoUnit>

    }

// 【请求】获取以成为的VIP列表
    interface GetVipListReq extends ReqObj {
    }

// 【反馈】获取以成为的VIP列表
    interface GetVipListRsp extends RspObj {
        // vip列表
            vipList ?:Array<VipInfoUnit>
        // 玩家充值金币总数量
            totalBaoshi ?:number

    }

// 【请求】VIP免费激活
    interface VipFreeUpgradeReq extends ReqObj {
        // 新的vip等级
            vipId ?:number
    }

// 【反馈】VIP免费激活
    interface VipFreeUpgradeRsp extends RspObj {
        // 激活后的vip等级
            vipId ?:number

    }

// 【请求】新的vip列表
    interface GetNewVipListReq extends ReqObj {
    }

// 【反馈】新的vip列表
    interface GetNewVipListRsp extends RspObj {
        // Vip信息
            vipInfo ?:NewVipInfoUnit
        // Vip礼包列表
            vipGiftInfoList ?:Array<VipGiftInfoUnit>

    }

// 【请求】VIP激活
    interface NewVipUpgradeReq extends ReqObj {
        // 新的vip等级
            vipId ?:number
    }

// 【反馈】VIP免费激活
    interface NewVipUpgradeRsp extends RspObj {
        // 激活后的vip等级
            vipId ?:number

    }

// 【请求】激活并领取当前累计钻石
    interface JiHuoAndGetLeiJiZuanShiReq extends ReqObj {
    }

// 【反馈】激活并领取当前累计钻石
    interface JiHuoAndGetLeiJiZuanShiRsp extends RspObj {
        // 获取钻石数
            totalZuanShiCount ?:number

    }

// 【反馈】推送VIP1免费领取钻石状态
    interface PushVipFreeDiamondInfoRsp extends RspObj {
        // 免费领取钻石信息
            diamondInfo ?:VipFreeDiamondInfoUnit

    }

// 【请求】VIP1免费领取钻石
    interface GetVipFreeDiamondReq extends ReqObj {
    }

// 【反馈】VIP1免费领取钻石
    interface GetVipFreeDiamondRsp extends RspObj {
        // 免费领取钻石信息
            diamondInfo ?:VipFreeDiamondInfoUnit
        // 本次可领取的奖励信息
            rewardInfo ?:JiangliInfoUnit

    }

// 【请求】获取VIP直升时间状态信息
    interface GetVipLevelUpDirectlyInfoReq extends ReqObj {
    }

// 【请求】获取VIP直升时间状态信息
    interface GetVipLevelUpDirectlyInfoRsp extends RspObj {
        // 人民币元
            chongzhiTiaojian ?:number
        // VIP直升奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// 【请求】VIP直升
    interface ClickVipLevelUpDirectlyReq extends ReqObj {
    }

// 【反馈】VIP直升
    interface ClickVipLevelUpDirectlyRsp extends RspObj {
        // vip开始
            vipFrom ?:number
        // vip结束
            vipTo ?:number
        // VIP直升奖励列表
            rewardList ?:Array<JiangliInfoUnit>
        // 直升VIP带来的VIP任务奖励数据列表
            vipMissionRewardInfoList ?:Array<VipMissionRewardInfoUnit>

    }

// [请求]迷宫面板
    interface MiGongPanelReq extends ReqObj {
    }

// [反馈]迷宫面板
    interface MiGongPanelRsp extends RspObj {
        // 已经重置了多少次
            hasResetTimes ?:number
        // 正在进行的迷宫的defId
            miGongDefId ?:number
        // 正在进行的迷宫的层数
            cengShu ?:number
        // 是否需要重置,才能挑战新迷宫
            isResetToNew ?:boolean

    }

// [请求]迷宫主怪战斗
    interface MiGongZhuGuaiFightReq extends ReqObj {
        // 正在进行的迷宫的defId
            miGongDefId ?:number
        // 正在进行的迷宫的层数
            cengShu ?:number
        // 要挑战的defId
            guaiGuDefId ?:number
    }

// [返回]迷宫主怪战斗
    interface MiGongZhuGuaiFightRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 剩余boss数量 用于校准
            leftBossCount ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]迷宫主怪战斗
    interface MiGongBossFightReq extends ReqObj {
        // 正在进行的迷宫的defId
            miGongDefId ?:number
    }

// [返回]迷宫主怪战斗
    interface MiGongBossFightRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]获得迷宫宝箱
    interface GetBaoXiangReq extends ReqObj {
        // 正在进行的迷宫的defId
            miGongDefId ?:number
        // 正在进行的迷宫的层数
            cengShu ?:number
        // 要领取的那个箱子的id
            baoXiangDefId ?:number
    }

// [返回]获得迷宫宝箱
    interface GetBaoXiangRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]保存迷宫信息
    interface SaveInfoReq extends ReqObj {
        // 正在进行的迷宫的defId
            miGongDefId ?:number
        // 正在进行的迷宫的层数
            cengShu ?:number
        // 前端需要的地图信息
            diTuInfo ?:string
        // 前端需要的怪物坐标信息
            guaiWuZuoBiaoInfo ?:string
        // 前端需要的宝箱信息
            baoXiangInfo ?:string
        // 前端需要的怪物信息
            guaiWuInfo ?:string
    }

// [返回]保存迷宫信息
    interface SaveInfoRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// [请求] 重置迷宫
    interface ResetMiGongReq extends ReqObj {
    }

// [请求] 重置迷宫  返回迷宫面板信息
    interface ResetMiGongRsp extends RspObj {

    }

// [请求]根据迷宫def和层数 和获得迷宫信息
    interface GetMiGongInfoReq extends ReqObj {
        // 正在进行的迷宫的defId
            miGongDefId ?:number
        // 正在进行的迷宫的层数
            cengShu ?:number
        // 最大的已通关的层数
            maxCengShu ?:number
    }

// [返回]根据迷宫def和层数 和获得迷宫信息
    interface GetMiGongInfoRsp extends RspObj {
        // 前端需要的地图信息
            diTuInfo ?:string
        // 前端需要的怪物信息
            guaiWuInfo ?:string
        // 前端需要的宝箱信息
            baoXiangInfo ?:string
        // 如果玩家第一次进入地图   返回怪物信息 格式 defId,defId,defId
            newGuaiFu ?:string
        // 已经领了多少个宝箱
            gotBaoXiangCount ?:number
        // 打败了的怪物信心 格式 defId,defId,defId
            finishGuaiWu ?:string
        // 还有那个宝箱没领取 格式 defId,defId,defId
            weiLingBaoXiang ?:string
        // 扫荡最大层数
            saoDangZuiDaCengShu ?:number

    }

// [请求]迷宫扫荡
    interface SweepMiGongReq extends ReqObj {
        // 正在进行扫荡的迷宫的defId
            miGongDefId ?:number
    }

// [返回]迷宫扫荡
    interface SweepMiGongRsp extends RspObj {
        // 
            baoXiangList ?:Array<SweepMiGongOutcomeUnit>
        // 
            guaiWuList ?:Array<SweepMiGongOutcomeUnit>

    }

// [请求]抓宠面板
    interface ZhuaChongPanelReq extends ReqObj {
        // 房间defId
            roomDefId ?:number
    }

// [反馈]抓宠面板
    interface ZhuaChongPanelRsp extends RspObj {
        // 正在战斗的怪物的defId
            guaiWuDefId ?:number
        // 怪物剩余hp
            hp ?:number
        // 已经抓了多少次
            cishu ?:number
        // 已经抓到的那些精灵
            jingLingQiuList ?:Array<JingLingQiuInfoUnit>
        // 
            yinDao ?:number
        // 概率 百分比
            probability ?:number
        // 价格信息
            chongWuPrice ?:ZhuaChongPriceInfoUnit
        // 房间信息
            roomInfo ?:ZhuaChongRoomUnit

    }

// [请求]扔石头
    interface RengShiTouReq extends ReqObj {
        // 0小石头   1大石头
            shiTouType ?:number
    }

// [返回]扔石头
    interface RengShiTouRsp extends RspObj {
        // 剩余hp 0 或者负数 死了
            hp ?:number

    }

// [请求]抓宠物
    interface ZhuaChongWuReq extends ReqObj {
        // 精灵球type
            jingLingQiuType ?:number
        // 如果是高级球 这个参数是抓宠概率
            percentage ?:number
    }

// [返回]抓宠物
    interface ZhuaChongWuRsp extends RspObj {
        // 已经抓了多少次
            cishu ?:number
        // 是否抓获成功
            isSuccess ?:boolean
        // 已经抓到的那些精灵
            jingLingQiuList ?:Array<JingLingQiuInfoUnit>
        // 
            yinDao ?:number
        // 如果用高级球失败 要给补偿
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]遇到敌人
    interface YuDiReq extends ReqObj {
    }

// [返回]遇到敌人
    interface YuDiRsp extends RspObj {
        // 宠物价格信息
            chongWu ?:ZhuaChongPriceInfoUnit

    }

// [请求]领宠物
    interface LingChongWuReq extends ReqObj {
        // 精灵球的id,具有唯一性
            id ?:number
    }

// [请求]领宠物
    interface LingChongWuRsp extends RspObj {
        // 精灵球的id,具有唯一性
            id ?:number
        // 奖励列表 如果奖励是空 说明抽到了流量 Flow grade
            jiangliList ?:Array<JiangliInfoUnit>
        // 已经抓到的那些精灵
            jingLingQiuList ?:Array<JingLingQiuInfoUnit>

    }

// [请求]扔石头
    interface YeWaiRengShiTouReq extends ReqObj {
        // 0小石头   1大石头
            shiTouType ?:number
        // 0普通类   1boss
            guaiWuType ?:number
    }

// [返回]扔石头
    interface YeWaiRengShiTouRsp extends RspObj {
        // 剩余hp 0 或者负数 死了
            hp ?:number

    }

// [请求]抓宠物
    interface YeWaiZhuaChongWuReq extends ReqObj {
        // 精灵球type
            jingLingQiuType ?:number
        // 0普通类   1boss
            guaiWuType ?:number
        // 如果是高级球 这个参数是抓宠概率
            percentage ?:number
    }

// [返回]抓宠物
    interface YeWaiZhuaChongWuRsp extends RspObj {
        // 已经抓了多少次
            cishu ?:number
        // 是否抓获成功
            isSuccess ?:boolean
        // 已经抓到的那些精灵
            jingLingQiuList ?:Array<JingLingQiuInfoUnit>
        // 
            yinDao ?:number
        // 奖励列表 如果奖励是空 
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]抓宠面板
    interface YeWaiZhuaChongPanelReq extends ReqObj {
        // 0普通类   1boss
            guaiWuType ?:number
    }

// [反馈]抓宠面板
    interface YeWaiZhuaChongPanelRsp extends RspObj {
        // 正在战斗的怪物的defId
            guaiWuDefId ?:number
        // 怪物剩余hp
            hp ?:number
        // 已经抓了多少次
            cishu ?:number
        // 已经抓到的那些精灵
            jingLingQiuList ?:Array<JingLingQiuInfoUnit>
        // 
            yinDao ?:number
        // 价格信息
            chongWuPrice ?:ZhuaChongPriceInfoUnit

    }

// [请求]抓宠房间面板
    interface ZhuaChongRoomPanelReq extends ReqObj {
    }

// [反馈]抓宠房间面板
    interface ZhuaChongRoomPanelRsp extends RspObj {
        // 抓宠中心房间
            zhuaChongRoomList ?:Array<ZhuaChongRoomUnit>
        // 
            yinDao ?:number

    }

// [请求]购买抓宠中心探索次数
    interface BuySearchTimesReq extends ReqObj {
        // 房间defId
            roomDefId ?:number
    }

// [反馈]购买抓宠中心探索次数
    interface BuySearchTimesRsp extends RspObj {
        // 新的剩余次数
            leftTimes ?:number

    }

// [反馈]获取抓宠助手精灵列表
    interface GetZhuaChongZhuShouHeroListReq extends ReqObj {
    }

// [反馈]获取抓宠助手精灵列表
    interface GetZhuaChongZhuShouHeroListRsp extends RspObj {
        // 抓宠助手列表
            heroList ?:Array<ZhuaChongZhuShouHeroUnit>
        // 是否开启自动抓宠
            isAuto ?:boolean

    }

// [请求]修改抓宠助手状态
    interface UpdateZhuaChongZhuShouAutoReq extends ReqObj {
        // 是否开启自动抓宠
            isAuto ?:boolean
    }

// [反馈]购买抓宠中心探索次数
    interface UpdateZhuaChongZhuShouAutoRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]打开流量转盘面板
    interface GetLiuLiangPanelReq extends ReqObj {
    }

// [反馈]打开流量转盘面板
    interface GetLiuLiangPanelRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 当前有多少次机会
            leafTimes ?:number
        // 获奖名单 最大20
            winnersList ?:Array<string>
        // 有没有未领取的奖励 false 没有 true有    如果有 需要弹出输入手机号面板
            hasGift ?:boolean

    }

// [请求]玩转盘 如果玩家摇到流量并且已经保存了手机号,直接发奖 如果没保存手机号  打开手机号面板
    interface PlayTurntableReq extends ReqObj {
    }

// [反馈]玩转盘
    interface PlayTurntableRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 奖励列表 如果奖励是空 说明抽到了流量 Flow grade
            jiangliList ?:Array<JiangliInfoUnit>
        // 抽取流量的档次 0代表没有抽到 1:10m-20m 2:30m－50m 3:70m－100m 4:150m－200m 5:500m
            flowGrade ?:number
        // 是否需要打开手机号面板true 需要  false 不要 提示:输入后无法修改
            needPhoneNO ?:boolean

    }

// [请求]保存手机号 这个消息的出发情况:玩家摇到流量 但是没有保存手机号 ,保存手机号后发奖
    interface SavePhoneNOReq extends ReqObj {
        // 手机号
            TelephoneNO ?:string
    }

// [反馈]保存手机号
    interface SavePhoneNORsp extends RspObj {
        // 是否保存成功
            isSuccess ?:boolean

    }

// 皮卡丘run面板信息
    interface PikaChuRunPanelReq extends ReqObj {
        // 活动id
            activityId ?:number
    }

// 皮卡丘run面板信息
    interface PikaChuRunPanelRsp extends RspObj {
        // 活动状态:0:不能押注阶段 1:能押注阶段 2:比赛开始 3:比赛结束(显示结果)
            state ?:number
        // 活动时间:下一阶段的时间戳
            activityTime ?:number
        // 皮卡丘状态
            pikachuState ?:Array<PikachuStateUnit>

    }

// 皮卡丘run押注
    interface PikaChuRunBetReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 押注哪只
            whichPikachu ?:number
        // 押注多少钱
            money ?:number
    }

// 皮卡丘run押注
    interface PikaChuRunBetRsp extends RspObj {
        // 皮卡丘状态
            pikachuState ?:Array<PikachuStateUnit>

    }

// 皮卡丘run加buff次数
    interface PikaChuRunBuffReq extends ReqObj {
        // 活动id
            activityId ?:number
        // 押注哪只
            whichPikachu ?:number
    }

// 皮卡丘run加buff次数
    interface PikaChuRunBuffRsp extends RspObj {
        // 皮卡丘状态
            pikachuState ?:Array<PikachuStateUnit>

    }

// 开始比赛后刷新下状态
    interface PikaChuRunRefreshReq extends ReqObj {
        // 活动id
            activityId ?:number
    }

// 开始比赛后刷新下状态
    interface PikaChuRunRefreshRsp extends RspObj {
        // 皮卡丘状态
            pikachuState ?:Array<PikachuStateUnit>

    }

// [请求]主角培养面板
    interface ZhuJuePeiYangPanelReq extends ReqObj {
    }

// [返回]主角培养面板
    interface ZhuJuePeiYangPanelRsp extends RspObj {
        // 当前属性列表
            currShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
            yuLanShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 当前等级
            currLevel ?:number
        // 是否可以培养 true 可以    false 不可以
            canPeiYang ?:boolean
        // 主角等级培养限制
            needZhuJueDengJi ?:number
        // 是不是升到满级了  true是    false不是
            isFull ?:boolean
        // 进阶消耗
            costList ?:Array<JiangliInfoUnit>
        // 是否突破
            isTuPo ?:boolean

    }

// [请求]主角培养
    interface GetZhuJuePeiYangReq extends ReqObj {
        // 1 培养
            type ?:number
    }

// [返回]主角培养
    interface GetZhuJuePeiYangRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]玩家战阵面板
    interface ZhanZhenPanelReq extends ReqObj {
    }

// [反馈]玩家战阵面板 返回玩家当前徽章id
    interface ZhanZhenPanelRsp extends RspObj {
        // 当前徽章List
            zhanZhenDefList ?:Array<ZhanzhenAllInfoUnit>
        // 将要挑战的大关defId
            willFightDefId ?:number
        // 最大已经通关的大关defId
            maxDefId ?:number
        // 是否已经全部通关所有大关关卡
            isAllTongguan ?:boolean
        // 战力排行
            zhanLiPaiHang ?:number
        // 排位赛位置
            paihangPPDefId ?:number
        // 重置到第几个道馆
            hasResetDaGuan ?:number
        // 今天打到的最大关卡
            todayMaxFight ?:number
        // 剩余挑战次数
            leftFightCount ?:number
        // 剩余扫荡次数
            leftSweepCount ?:number

    }

// [请求]用道具升级战阵
    interface UpGradeZhanZhenReq extends ReqObj {
        // 操作类型 0用道具  1用钻石
            type ?:number
        // 徽章类型
            zhanZhenGrade ?:number
    }

// [反馈]用道具升级战阵
    interface UpGradeZhanZhenRsp extends RspObj {
        // 玩家当前徽章id
            zhanZhenDefId ?:number

    }

// [请求]重置道馆
    interface ResetDaoGuanReq extends ReqObj {
        // 重置的道馆数量
            resetNum ?:number
    }

// [反馈]重置道馆
    interface ResetDaoGuanRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 重置到哪个章节
            hasResetZhangJie ?:number
        // 重置了多少次
            hasResetNum ?:number

    }

// [请求]道馆扫荡预览
    interface SweepDaGuanShowReq extends ReqObj {
    }

// [返回]道馆扫荡预览
    interface SweepDaGuanShowRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>
        // 扫荡花费
            costList ?:Array<JiangliInfoUnit>
        // 是否免费
            isFree ?:boolean

    }

// [请求]道馆扫荡确认
    interface SweepDaGuanEnsureReq extends ReqObj {
    }

// [返回]道馆扫荡确认
    interface SweepDaGuanEnsureRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]组队副本面板
    interface ZuDuiFuBenPanelReq extends ReqObj {
    }

// [返回]组队副本面板
    interface ZuDuiFuBenPanelRsp extends RspObj {
        // 剩余可组队次数
            leftTimes ?:number
        // 战队信息列表
            zhanDuiListList ?:Array<ZuDuiFuBenPanelInfoUnit>
        // 战队信息列表
            paihangInfo ?:PaihangInfoUnit
        // 战队战斗记录
            logList ?:Array<ZuDuiFuBenFightLogUnit>

    }

// [请求]创建副本队伍
    interface CreateFuBenDuiWuReq extends ReqObj {
        // 战力要求
            limitFight ?:number
    }

// [返回]组队面板
    interface CreateFuBenDuiWuRsp extends RspObj {
        // 战队信息列表
            memberListList ?:Array<ZuDuiFuBenMemberInfoUnit>

    }

// [请求]加入队伍
    interface JoinDuiWuReq extends ReqObj {
        // 要加入队伍的dbId
            dbId ?:number
        // 服务器ID 长id 用于跨服消息
            serverChangId ?:number
        // 房间号
            roomId ?:number
    }

// [返回]加入队伍
    interface JoinDuiWuRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]组队副本踢人
    interface ZuDuiFuBenTiRenReq extends ReqObj {
        // 被踢的玩家
            playerId ?:number
    }

// [返回]组队副本踢人
    interface ZuDuiFuBenTiRenRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]修改限制战力
    interface ModifyLimitFightReq extends ReqObj {
        // 
            limitFight ?:number
    }

// [返回]修改限制战力
    interface ModifyLimitFightRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]组队副本 准备
    interface ZuDuiFuBenReadyReq extends ReqObj {
        // 发0 从准备变成没准备  发1 从没准备变成准备
            ready ?:number
    }

// [返回]组队副本 准备
    interface ZuDuiFuBenReadyRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]退出队伍
    interface LeaveTeamReq extends ReqObj {
    }

// [返回]退出队伍
    interface LeaveTeamRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]组队副本 打
    interface ZuDuiFuBenFightReq extends ReqObj {
    }

// [返回]组队副本 打
    interface ZuDuiFuBenFightRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]组队副本成员界面  去队长服务器找队伍 找不到 有可能是你停服的时候 队长把你踢了 如果找不到 要做相应处理
    interface ZuDuiFuBenMemberListPanelReq extends ReqObj {
    }

// [返回]组队面板
    interface ZuDuiFuBenMemberListPanelRsp extends RspObj {
        // 战队信息列表
            memberListList ?:Array<ZuDuiFuBenMemberInfoUnit>
        // 能领奖的时间戳  如果当前时间小于这个时间 不能退出或者解散队伍
            canRewardTime ?:number
        // 剩余可尝试次数
            leftTryTimes ?:number
        // 房间号
            roomNum ?:number
        // 聊天信息
            chatMessage ?:Array<ChatMessageLoginInfoUnit>

    }

// [请求]查看组队副本成员精灵信息
    interface LoadZuDuiFuBenMemberHeroInfoReq extends ReqObj {
        // 
            memberId ?:number
    }

// [请求]查看组队副本成员精灵信息
    interface LoadZuDuiFuBenMemberHeroInfoRsp extends RspObj {
        // 精灵宠物数据
            heroViewList ?:Array<WarZonePlayerHeroViewUnit>

    }

// [请求]根据房间(队伍号)号搜索队伍
    interface SearchTeamByRoomIdReq extends ReqObj {
        // 发起请求的玩家id
            roomId ?:number
    }

// [请求]根据房间(队伍号)号搜索队伍
    interface SearchTeamByRoomIdRsp extends RspObj {
        // 队伍信息
            zuDuiFuBenPanelInfo ?:ZuDuiFuBenPanelInfoUnit

    }

// [推送] 玩家在线被踢 推送这个状态
    interface ZuDuiFuBenStatusRsp extends RspObj {
        // 是不是在组队副本队伍中  0 不在  1在
            status ?:number
        // 是不是被踢
            beiTi ?:boolean
        // 是不是解散  注意 这是队长手动解散
            jieSan ?:boolean

    }

// [请求]变换出场顺序
    interface ChangeIndexReq extends ReqObj {
        // 出场顺序
            indexList ?:Array<ZuDuiFuBenIndexInfoUnit>
    }

// [返回] 变换出场顺序
    interface ChangeIndexRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// [请求]进入战斗界面 组队副本 打的动画信息   打的时候前端除了发打 还要发这个 返回当前播放到哪个关卡
    interface TeamFightReq extends ReqObj {
    }

// [请求]进入战斗界面 组队副本 打的动画信息   打的时候前端除了发打 还要发这个 返回当前播放到哪个关卡
    interface TeamFightRsp extends RspObj {
        // 队伍数据
            teamDataList ?:Array<TeamDataUnit>
        // 关卡
            guanqiaData ?:GuanqiaDataUnit
        // 当前播放到哪个关卡(层)
            guanqia ?:number

    }

// [请求]异步请求关卡信息
    interface PushTeamGuanqiaFightInfoReq extends ReqObj {
    }

// [请求]异步请求关卡信息
    interface PushTeamGuanqiaFightInfoRsp extends RspObj {
        // 关卡数据
            guanqiaList ?:Array<GuanqiaDataUnit>
        // 
            isTongguan ?:boolean
        // 
            rewardTime ?:number
        // 是否有新纪录    true 有  false 没有
            hasNewRecord ?:boolean
        // 本次打到了多少层
            layer ?:number

    }

// [请求]组队副本排行榜
    interface GetZuDuiFuBenPaiHangBangReq extends ReqObj {
        // 排行榜类型：22组队副本排行榜
            type ?:number
    }

// 组队副本排行榜
    interface GetZuDuiFuBenPaiHangBangRsp extends RspObj {
        // 排名
            position ?:number
        // 剩余可膜拜次数
            mobaiCount ?:number
        // 排行列表
            paihangList ?:Array<PaihangInfoUnit>

    }

// [请求]组队副本聊天
    interface SendZuDuiFuBenChatMessageReq extends ReqObj {
        // 讲话人ID
            playerId ?:number
        // 
            playerName ?:string
        // 服务器编号
            serverNo ?:number
        // 聊天内容
            content ?:string
        // 队伍id
            teamId ?:number
        // 目标对象Id
            goalPlayerId ?:number
        // 聊天id
            chatDbId ?:number
    }

// 组队副本聊天
    interface SendZuDuiFuBenChatMessageRsp extends RspObj {
        // 讲话人ID
            playerId ?:number
        // 
            playerName ?:string
        // 服务器编号
            serverNo ?:number
        // 聊天内容
            content ?:string
        // 目标对象Id
            goalPlayerId ?:number
        // 聊天id
            chatDbId ?:number

    }

// [请求]向其他服务器请求 组队副本奖励的的信息
    interface GetTeamGuanqiaFightInfoReq extends ReqObj {
        // 
            playerId ?:number
        // 
            duiWuId ?:number
    }

// [请求]向其他服务器请求 组队副本奖励的的信息
    interface GetTeamGuanqiaFightInfoRsp extends RspObj {
        // 关卡数据
            guanqiaList ?:Array<GuanqiaDataUnit>
        // 
            isTongguan ?:boolean
        // 
            rewardTime ?:number
        // 
            playerId ?:number
        // 新打的层数
            layer ?:number
        // true 需要记录日志  false 不用
            needLog ?:boolean
        // 成员字符串信息
            memberInfoStr ?:string
        // 房间号
            roomNum ?:number
        // 服务器号
            serverInfo ?:number

    }

// [请求]向队长服发送组队副本聊天
    interface SendZuDuiFuBenChatMessageToMemberReq extends ReqObj {
        // 讲话人ID
            playerId ?:number
        // 
            playerName ?:string
        // 服务器编号
            serverNo ?:number
        // 聊天内容
            content ?:string
        // 队伍id
            teamId ?:number
        // 目标对象Id
            goalPlayerId ?:number
        // 聊天id
            chatDbId ?:number
    }

// 向队长服发送组队副本聊天
    interface SendZuDuiFuBenChatMessageToMemberRsp extends RspObj {
        // 讲话人ID
            playerId ?:number
        // 
            playerName ?:string
        // 服务器编号
            serverNo ?:number
        // 聊天内容
            content ?:string
        // 队伍id
            teamId ?:number
        // 目标对象Id
            goalPlayerId ?:number
        // 聊天id
            chatDbId ?:number

    }

// [请求]组队副本匹配 开始
    interface ZuDuiFuBenPiPeiKaiShiReq extends ReqObj {
    }

// [请求]组队副本匹配 取消
    interface ZuDuiFuBenPiPeiQuXiaoReq extends ReqObj {
    }

// 组队副本聊天红点
    interface PushZuDuiFuBenChatRedReq extends ReqObj {
    }

// 组队副本聊天红点
    interface PushZuDuiFuBenChatRedRsp extends RspObj {
        // 0 不显示红点； 1 显示红点；
            zuDuChatRed ?:number

    }

// 获得组队副本
    interface GetZuDuiFuBenStatusReq extends ReqObj {
    }

// 组队副本聊天红点
    interface GetZuDuiFuBenStatusRsp extends RspObj {
        // 是不是在组队副本队伍中  0 不在  1在
            status ?:number

    }

// 组队副本战斗记录
    interface ZuDuFuBenFightLogReq extends ReqObj {
        // 要看第几场 从1开始
            round ?:number
    }

// 组队副本战斗记录
    interface ZuDuFuBenFightLogRsp extends RspObj {
        // 战斗记录
            fightLog ?:string

    }

// 组队喊话邀请
    interface HanHuaYaoQingReq extends ReqObj {
        // 房间ID
            roomId ?:number
    }

// 组队喊话邀请
    interface HanHuaYaoQingRsp extends RspObj {
        // 是否邀请成功
            isSuccess ?:boolean

    }

// [请求]骑技面板
    interface ChiBangTuPoPanelReq extends ReqObj {
    }

// [返回]骑技面板
    interface ChiBangTuPoPanelRsp extends RspObj {
        // 当前属性列表
            currShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 增加属性列表
            addShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 下一阶段属性列表
            yuLanShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 当前阶段
            currJieDuan ?:number
        // 经验百分比 最大是9999 前端显示99.99
            jingYanBeiFenBi ?:number
        // 特殊属性列表
            TeShuShuXingInfoList ?:Array<string>
        // 是否可以突破  true 可以    false 不可以
            canTuPo ?:boolean
        // 翅膀等级突破限制
            needChiBangDengJi ?:number
        // 是不是升到头了  true是    false不是
            isFull ?:boolean
        // 消耗列表
            costList ?:Array<JiangliInfoUnit>

    }

// [请求]翅膀突破
    interface ChiBangTuPoTuPoReq extends ReqObj {
        // 1 培养一次    2一件培养    3突破
            type ?:number
    }

// [返回]骑技面板
    interface ChiBangTuPoTuPoRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]翅膀突破  满级预览
    interface ChiBangTuPoManJieYuLanReq extends ReqObj {
        // 1 培养一次    2一件培养    3突破
            type ?:number
    }

// [请求]翅膀突破  满级预览
    interface ChiBangTuPoManJieYuLanRsp extends RspObj {
        // 下一阶段属性列表
            yuLanShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 满阶所有特殊属性列表 前端自己区分那个是已经激活的
            teShuShuXingInfoList ?:Array<QiJiTeShuShuXingInfoUnit>

    }

// [请求]探险面板
    interface TanXianPanelReq extends ReqObj {
    }

// [返回]探险面板
    interface TanXianPanelRsp extends RspObj {
        // 玩家当前探险等级
            tanXianLevel ?:number
        // 探险经验
            tanXianExp ?:number
        // 
            shuXingList ?:Array<TanXianShuXingInfoUnit>
        // 探险等级红点 true 有  false没有
            levelPoint ?:boolean
        // 探险技能红点 true 有  false没有
            jiNengPoint ?:boolean
        // 探险收益红点 true 有  false没有
            shouYiPoint ?:boolean
        // 探险成员红点 true 有  false没有
            heroPoint ?:boolean
        // 精灵列表,如果尚未召唤,则DB ID为0。未排序,需要按照上阵精灵ID列表重新排序
            heroList ?:Array<TanXianHeroInfoUnit>

    }

// [请求]升级探险
    interface ShengJiTanXianReq extends ReqObj {
    }

// [返回]升级探险
    interface ShengJiTanXianRsp extends RspObj {
        // 是否升级成功
            isSuccess ?:boolean
        // 新的探险等级
            newDefId ?:number
        // 精灵列表,如果尚未召唤,则DB ID为0。未排序,需要按照上阵精灵ID列表重新排序
            heroList ?:Array<TanXianHeroInfoUnit>

    }

// [请求]升级探险属性
    interface ShengJiShuXingReq extends ReqObj {
        // 
            shuXingDbId ?:number
    }

// [返回]升级探险属性
    interface ShengJiShuXingRsp extends RspObj {
        // 是否升级成功
            isSuccess ?:boolean
        // 新的属性等级
            newDefId ?:number

    }

// [请求]领取探险奖励
    interface GetTanXianRewardReq extends ReqObj {
    }

// [返回]领取探险奖励
    interface GetTanXianRewardRsp extends RspObj {
        // 
            isSuccess ?:boolean
        // 
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// [请求]展示探险奖励
    interface ShowTanXianRewardReq extends ReqObj {
    }

// [返回]展示探险属性
    interface ShowTanXianRewardRsp extends RspObj {
        // 
            isSuccess ?:boolean
        // 
            tanXianLiangLiShowInfoList ?:Array<TanXianLiangLiShowInfoUnit>
        // 挂机时间
            guaJiTime ?:number

    }

// [请求]展示探险精灵列表
    interface GetTanXianHeroListReq extends ReqObj {
        // 星级
            star ?:number
    }

// [返回]展示探险精灵列表
    interface GetTanXianHeroListRsp extends RspObj {
        // 星级
            star ?:number
        // 精灵列表,如果尚未召唤,则DB ID为0。未排序,需要按照上阵精灵ID列表重新排序
            heroList ?:Array<TanXianHeroInfoUnit>

    }

// [请求]探险精灵上阵
    interface MakeTanXianHeroInOutReq extends ReqObj {
        // 精灵ID
            dbId ?:number
        // 操作类型: 1上阵; 2下阵
            operation ?:number
    }

// [返回]探险精灵上阵
    interface MakeTanXianHeroInOutRsp extends RspObj {
        // 新上阵精灵的DB id
            heroIn ?:number
        // 下阵精灵的DB id
            heroOut ?:number
        // 精灵列表,如果尚未召唤,则DB ID为0。未排序,需要按照上阵精灵ID列表重新排序
            heroList ?:Array<TanXianHeroInfoUnit>

    }

// [请求]探险精灵召唤
    interface MakeNewTanXianHeroReq extends ReqObj {
        // 精灵ID
            heroId ?:number
    }

// [返回]探险精灵召唤
    interface MakeNewTanXianHeroRsp extends RspObj {
        // 新召唤出来的精灵ID
            dbId ?:number
        // 精灵ID
            heroId ?:number

    }

// [请求]探险精灵召升级
    interface ShengJiTanXianHeroReq extends ReqObj {
        // 精灵DbID
            dbId ?:number
    }

// [返回]探险精灵升级
    interface ShengJiTanXianHeroRsp extends RspObj {
        // 升级的精灵dbId
            dbId ?:number
        // 探险精灵表的defId
            tanxianjignlingleveldefId ?:number
        // 精灵列表,如果尚未召唤,则DB ID为0。未排序,需要按照上阵精灵ID列表重新排序
            heroList ?:Array<TanXianHeroInfoUnit>

    }

// 【请求】礼包信息
    interface GetGiftPackageListReq extends ReqObj {
    }

// 【反馈】礼包信息
    interface GetGiftPackageListRsp extends RspObj {
        // 礼包信息列表
            giftPackageList ?:Array<GiftPackageInfoUnit>

    }

// 【请求】购买礼包
    interface BuyGiftPackageReq extends ReqObj {
        // 礼包Id
            giftPackageId ?:number
    }

// 【反馈】购买礼包
    interface BuyGiftPackageRsp extends RspObj {
        // 0:失败--失败后不会再次推送新的列表 1:成功
            buyResult ?:number
        // 购买成功的奖励列表
            hasBuyedList ?:Array<JiangliInfoUnit>
        // 购买成功后，新的礼包信息列表
            giftPackageList ?:Array<GiftPackageInfoUnit>

    }

// [请求]突发事件面板
    interface TuFaShiJianPanelReq extends ReqObj {
        // 突发事件defId
            defId ?:number
    }

// [返回]突发事件面板
    interface TuFaShiJianPanelRsp extends RspObj {
        // 有没有可以领取的宝箱
            canGet ?:boolean
        // 
            tuFaShiJianDef ?:TuFaShiJianDefUnit
        // 突发事件dbId
            tuFaShiJianDbId ?:number
        // 条件list
            baoXiangTiaoJianInfoList ?:Array<BaoXiangTiaoJianInfoUnit>

    }

// [请求]领取突发事件的奖励
    interface TuFaShiJianLingJiangReq extends ReqObj {
        // 突发事件dbId
            tuFaShiJianDbId ?:number
    }

// [返回]领取突发事件的奖励
    interface TuFaShiJianLingJiangRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】查看自身排位赛数据
    interface LoadMyRankingReq extends ReqObj {
        // 排位赛类型 0等级  1转生
            matchType ?:number
    }

// 【反馈】查看自身排位赛数据
    interface LoadMyRankingRsp extends RspObj {
        // 排位赛段级ID,取自PaihangPP表, isRankingChange=true时有效,如未定位,则该值为0
            paihangPPDefId ?:number
        // 定级赛战斗次数,paihangPPDefId=0表示定级中
            provisionalSize ?:number
        // 战斗局数,定级后才传
            sizeFight ?:number
        // 胜利战斗局数,定级后才传
            sizeWin ?:number
        // 战区点数,定级后才传
            point ?:number
        // 定级状态: 0.无状态; 1.定级中; 2.定级成功
            provisionalState ?:number
        // 晋升状态: 0.无晋升状态或者晋升中; 1.获取晋升资格; 2.晋升成功; 3.晋升失败
            promoteState ?:number
        // 是否段位级位发生变化,+1表示上升,-1表示下降
            rankingChange ?:number
        // 全服最高等级
            maxPlayerLevel ?:number
        // 当前所在等级段(等级段最小等级)
            levelPart ?:number
        // 排位赛状态: 1比赛中; 2结算中; 3数据初始化中; 
            matchStatus ?:number
        // 排位赛开始时间
            rankingMatchStartTime ?:number
        // 排位赛结束时间
            rankingMatchEndTime ?:number
        // 元素入场券个数
            elementTicket ?:number
        // 转生排位赛当前所在等级段(等级段最小等级)
            zslevelPart ?:number
        // 转生最高等级
            maxZsPlayerLevel ?:number
        // 剩余挑战次数
            leftFightCount ?:number
        // 剩余可购买次数
            leftCanBuy ?:number
        // 购买消耗
            cost ?:JiangliInfoUnit

    }

// 【请求】排位赛战斗
    interface RankingFightReq extends ReqObj {
        // 排位赛类型 0等级  1转生
            matchType ?:number
        // 转生排位赛场景 0为默认随机  1-5对应相应场景
            fightScene ?:number
    }

// 【反馈】排位赛战斗
    interface RankingFightRsp extends RspObj {
        // 精灵战斗输出回复列表
            heroList ?:Array<HeroFightInfoUnit>
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：1我方胜利，2我方失败
            win ?:number
        // 对手的头像(空或0表示没有头像)
            enemyTouxiangId ?:number
        // 对手的服务器编号
            enemyServerNo ?:number
        // 对手的昵称
            enemyName ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】排位赛战斗奖励结算
    interface MakeRewardOnRankingFightReq extends ReqObj {
        // 排位赛类型 0等级  1转生
            matchType ?:number
    }

// 【反馈】排位赛战斗奖励结算
    interface MakeRewardOnRankingFightRsp extends RspObj {
        // 排位赛段级ID,取自PaihangPP表
            paihangPPDefId ?:number
        // 战区编号
            warZone ?:number
        // 战区点数变化,正数表示+,负数表示-
            pointChange ?:number
        // 商城积分(匹配币)变化,正数表示+,负数表示-
            matchScoreChange ?:number

    }

// 【请求】查看战区数据
    interface GetRankingWarZoneInfoReq extends ReqObj {
        // 排位赛段级ID,取自PaihangPP表
            paihangPPDefId ?:number
        // 排位赛类型 0等级  1转生
            matchType ?:number
    }

// 【反馈】查看战区数据
    interface GetRankingWarZoneInfoRsp extends RspObj {
        // 战区编号
            warZone ?:number
        // 晋升数据
            promoteList ?:Array<WarZonePlayerUnit>
        // 战区玩家数据
            playerList ?:Array<WarZonePlayerUnit>

    }

// 【请求】查看排位赛他人数据
    interface LoadRankingOtherPlayerInfoReq extends ReqObj {
        // 待查看的玩家ID
            searchId ?:number
        // 排位赛类型 0等级  1转生
            matchType ?:number
    }

// 【反馈】查看排位赛他人数据
    interface LoadRankingOtherPlayerInfoRsp extends RspObj {
        // 玩家ID
            playerId ?:number
        // 玩家昵称
            playerName ?:string
        // 服务器编号
            ServerNo ?:number
        // 排位赛段级ID,取自PaihangPP表, isRankingChange=true时有效
            paihangPPDefId ?:number
        // 连胜次数
            successiveWin ?:number
        // 战斗局数
            sizeFight ?:number
        // 胜利战斗局数
            sizeWin ?:number
        // 战区点数
            point ?:number
        // 精灵宠物数据
            heroViewList ?:Array<WarZonePlayerHeroViewUnit>
        // 是否可以挑战此玩家     (主动打某人 的cd次数还不够不够)
            isCanFight ?:boolean
        // 剩余指定打某人  的次数
            leftFightSomeoneTimes ?:number
        // 元素入场券个数
            elementTicket ?:number

    }

// 【请求】排位赛记录列表
    interface GetMatchRecordListReq extends ReqObj {
        // 排位赛类型 0等级  1转生
            matchType ?:number
    }

// 【反馈】排位赛记录列表
    interface GetMatchRecordListRsp extends RspObj {
        // 排位赛记录列表
            rankingRecordList ?:Array<RankingRecordInfoUnit>

    }

// 【请求】排位赛战斗观看
    interface GetRankingRecordFightJsonReq extends ReqObj {
        // 战斗记录ID
            recordId ?:number
    }

// 【反馈】排位赛战斗观看
    interface GetRankingRecordFightJsonRsp extends RspObj {
        // 战斗记录
            fightInfo ?:string

    }

// 【请求】选择等级段(等级段最小等级)
    interface ChooseLevelPartReq extends ReqObj {
        // 当前所在等级段(等级段最小等级)
            levelPart ?:number
        // 排位赛类型 0等级  1转生
            matchType ?:number
    }

// 【请求】选择等级段(等级段最小等级)
    interface ChooseLevelPartRsp extends RspObj {
        // 全服最高等级
            maxPlayerLevel ?:number
        // 当前所在等级段(等级段最小等级)
            levelPart ?:number

    }

// 【请求】查看排位赛王者大师的排行
    interface GetRankingPaiHangInfoReq extends ReqObj {
        // 
            playerId ?:number
        // 排位赛段级ID,取自PaihangPP表
            paihangPPDefId ?:number
        // 当前所在等级段(等级段最小等级)
            levelPart ?:number
        // 排位赛类型 0等级  1转生
            matchType ?:number
    }

// 【反馈】查看排位赛王者大师的排行
    interface GetRankingPaiHangInfoRsp extends RspObj {
        // 战区玩家数据
            paihangList ?:Array<RankingPaiHangPlayerUnit>
        // 局数
            sizeFight ?:number
        // 皮肤形象
            piFuDefIdList ?:Array<number>
        // 称号
            chenghaoId ?:number
        // 玩家坐骑ID
            playerZuoqi ?:number
        // 玩家翅膀ID
            playerChibang ?:number
        // 皮神形象,
            luoTuoMuDisplay ?:number
        // 
            playerId ?:number

    }

// 【请求】排位赛战斗
    interface RankingFightSomeoneReq extends ReqObj {
        // 
            energyPlayerId ?:number
    }

// 【反馈】排位赛战斗
    interface RankingFightSomeoneRsp extends RspObj {
        // 精灵战斗输出回复列表
            heroList ?:Array<HeroFightInfoUnit>
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：1我方胜利，2我方失败
            win ?:number
        // 对手的头像(空或0表示没有头像)
            enemyTouxiangId ?:number
        // 对手的服务器编号
            enemyServerNo ?:number
        // 对手的昵称
            enemyName ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】转生排位赛战斗
    interface RankingFightSomeoneZSReq extends ReqObj {
        // 
            energyPlayerId ?:number
        // 转生排位赛场景 0为默认随机  1-5对应相应场景
            fightScene ?:number
    }

// 【反馈】转生排位赛战斗
    interface RankingFightSomeoneZSRsp extends RspObj {
        // 精灵战斗输出回复列表
            heroList ?:Array<HeroFightInfoUnit>
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：1我方胜利，2我方失败
            win ?:number
        // 对手的头像(空或0表示没有头像)
            enemyTouxiangId ?:number
        // 对手的服务器编号
            enemyServerNo ?:number
        // 对手的昵称
            enemyName ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】购买排位赛挑战次数
    interface BuyPaiWeiFightCountReq extends ReqObj {
    }

// 【反馈】购买排位赛挑战次数
    interface BuyPaiWeiFightCountRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 购买次数的价格
            cost ?:JiangliInfoUnit

    }

// 【请求】排位赛扫荡
    interface SweepMatchRankingReq extends ReqObj {
    }

// 【反馈】排位赛扫荡
    interface SweepMatchRankingRsp extends RspObj {
        // 排位赛是否正常开启中
            isMatchOpen ?:boolean
        // 是否已经报名参加排位赛
            isInMatchRank ?:boolean
        // 排位赛段级ID,取自PaihangPP表
            paihangPPDefId ?:number
        // 战区编号
            warZone ?:number
        // 战区点数变化,正数表示+,负数表示-
            pointChange ?:number
        // 商城积分(匹配币)变化,正数表示+,负数表示-
            matchScoreChange ?:number
        // 精灵战斗输出回复列表
            heroList ?:Array<HeroFightInfoUnit>
        // 找不到对手
            isNoTarget ?:boolean
        // 是否战斗胜利
            isWin ?:boolean

    }

// 【请求】获得玩家英雄列表
    interface GetPlayerHeroListReq extends ReqObj {
    }

// 【反馈】获得玩家英雄列表
    interface GetPlayerHeroListRsp extends RspObj {
        // 玩家英雄列表
            heroList ?:Array<HeroInfoUnit>
        // 种族值突破信息
            zhongZuZhiTuPoList ?:Array<ZhongZuZhiTuPoInfoUnit>
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_11 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_12 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_13 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_14 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_15 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_16 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_17 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_18 ?:number
        // 阵法该位置上阵武将（使用hero表的dbID），0表示空着
            zf_19 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_111 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_112 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_113 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_114 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_115 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_116 ?:number

    }

// 【请求】招募英雄
    interface GetHeroReq extends ReqObj {
        // 招募类型, 1 - 30宝石， 2 - 300宝石， 3 - 3000宝石抽十次, ， 4 - 300宝石抽十次
            getType ?:number
    }

// 【反馈】招募英雄
    interface GetHeroRsp extends RspObj {
        // 气运必出导致抽到的5星英雄ID（静态ID）
            heroID_5c ?:number
        // 0-没有英雄变成残魂，1-有英雄变成残魂
            isHeroToSoul ?:number
        // 玩家英雄列表
            wujiangIds ?:Array<JiangliInfoUnit>
        // 道具列表
            itemIds ?:Array<JiangliInfoUnit>
        // 0 - 不是免费抽取，1 - 30钻石免费抽取，2 - 300 钻石免费抽取
            freeGetFlag ?:number
        // 武将抽取时间
            getTime ?:number
        // 招募后，英雄招募必出5星气运值
            wj_get5Acc ?:number
        // 首次获得的四星英雄列表
            firstFourStarWujiangList ?:Array<JiangliInfoUnit>
        // 再超级抽多少次必得心愿神魔
            leftTimesToWish ?:number

    }

// 【请求】残魂招募英雄
    interface GetHeroBySoulReq extends ReqObj {
        // 残魂道具的DBId
            daojuDBId ?:number
    }

// 【反馈】残魂招募英雄
    interface GetHeroBySoulRsp extends RspObj {
        // 招募的奖励
            jiangLiInfo ?:JiangliInfoUnit
        // 0-没有英雄变成残魂，大于0有英雄变成残魂
            heroToSoulCount ?:number

    }

// 【请求】英雄变化
    interface HeroChangeReq extends ReqObj {
        // 玩家当前选中的英雄ID
            heroDbID ?:number
        // 1 - 强化， 2 - 升官 3 - 继承
            cmdType ?:number
        // cmdType(1，2) - 玩家当前选中的背包中的物品dbID， 3 - 玩家所有的英雄dbID
            eatID ?:number
        // 1 - 物品， 2 - 武将
            eatType ?:number
        // 仅在继承时有效，0 - 表示需要服务器判断是否有经验损失，并提示， 1 - 无条件继承，损失经验值。
            confirmFlag ?:number
        // 消耗道具的数量
            eatCount ?:number
        // 培养类型 0小培养  1一键小培养   2大培养
            peiYangType ?:number
        // 1 宠物 2 道具
            tuPoCostType ?:number
        // 升星消耗的DBId列表
            costHeroIdList ?:Array<DbIdListInfoUnit>
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容。一键升级时使用
            index ?:number
    }

// 【反馈】英雄变化
    interface HeroChangeRsp extends RspObj {
        // 玩家剩余的银两
            yinLiang ?:number
        // 是否缺少道具
            isLackItem ?:boolean
        // 宠物被吃掉的返还的原消耗材料
            otherJiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】英雄阵容调整
    interface HeroTakePositionReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 上阵英雄dbID。如果只是下阵没有上阵，则不传
            heroDbIdIn ?:number
        // 被替换掉的英雄dbID。下阵只需赋值Out，In无需赋值。如果是互相位置，则Out会自动转移到In所占据的Index
            heroDbIdOut ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。上下阵都要赋值
            heroIndex ?:number
    }

// 【反馈】英雄阵容调整
    interface HeroTakePositionRsp extends RspObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 上阵英雄dbID。如果只是下阵没有上阵，则不传
            heroDbIdIn ?:number
        // 被替换掉的英雄dbID。下阵只需赋值Out，In无需赋值。如果是互相位置，则Out会自动转移到In所占据的Index
            heroDbIdOut ?:number
        // 影响位置：11 ~ 19。协作位111 ~ 119。上下阵都要赋值
            heroIndex ?:number

    }

// 【请求】英雄上阵位置交换
    interface HeroChangePositionReq extends ReqObj {
        // 上阵英雄dbID
            heroDbID1 ?:number
        // 上阵英雄dbID
            heroDbID2 ?:number
    }

// 【反馈】英雄上阵位置交换，返回新的战阵位置武将信息
    interface HeroChangePositionRsp extends RspObj {
        // 宠物阵容信息
            heroFormInfo ?:HeroFormInfoUnit

    }

// 【请求】协作英雄上阵
    interface HeroAssistTakePositionReq extends ReqObj {
        // 上阵英雄dbID
            heroDbID ?:number
        // 上阵英雄占据的位置：111 ~ 119
            posType ?:number
    }

// 【更新】返回新的协作武将位置信息
    interface HeroAssistListRsp extends RspObj {
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_111 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_112 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_113 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_114 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_115 ?:number
        // 协作阵容该位置上阵武将（使用hero表的dbID），0表示空着，-1表示未开启
            zf_116 ?:number

    }

// 【请求】自动补充协作英雄
    interface HeroAssistAddHeroReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 上阵英雄dbID List
            heroDbIDs ?:Array<number>
    }

// 【请求】一键协作
    interface HeroAssistAutoAddReq extends ReqObj {
        // 上阵英雄dbID List
            heroDbIDs ?:Array<number>
    }

// 【请求】英雄回收预览
    interface HeroSendBackRewardViewReq extends ReqObj {
        // herodbId
            heroDbIDs ?:Array<number>
        // 是否引导
            isGuide ?:boolean
    }

// 【请求】英雄回收预览
    interface HeroSendBackRewardViewRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]点亮命星
    interface DianLiangMingXingReq extends ReqObj {
        // 英雄dbID
            heroDbID ?:number
    }

// [返回]点亮命星
    interface DianLiangMingXingRsp extends RspObj {
        // 命星阶段
            mxjieduan ?:number
        // 命星等级
            mxlevel ?:number

    }

// 【请求】回收武将
    interface HeroSendBackReq extends ReqObj {
        // herodbId
            heroDbIDs ?:Array<number>
        // 是否引导
            isGuide ?:boolean
    }

// 【请求】回收武将
    interface HeroSendBackRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 回收的英雄dbID
            heroDbID ?:number

    }

// 【请求】武将重生
    interface HeroChongShengReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
    }

// 【请求】武将重生
    interface HeroChongShengRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 【请求】武将属性计算显示值
    interface HeroPropertyShowReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
        // 1. 升级 2.培养 3.进化
            type ?:number
    }

// 【反馈】武将属性计算显示值
    interface HeroPropertyShowRsp extends RspObj {
        // 英雄dbId
            heroDbId ?:number
        // 当前属性列表
            curShuXingList ?:Array<ShuXingInfoUnit>
        // 
            showShuXingList ?:Array<ShuXingInfoUnit>

    }

// 【请求】激活协作
    interface JiHuoXieZuoReq extends ReqObj {
    }

// 【请求】武将重生预览
    interface HeroChongShengYuLanReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
    }

// 【请求】武将重生预览
    interface HeroChongShengYuLanRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 重生所需消耗
            xiaohaoInfo ?:JiangliInfoUnit
        // 是否可以重生：7星宠物才可以重生
            isAbleToChongSheng ?:boolean

    }

// 【请求】超进化升级
    interface ChaoJinHuaShengJiReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
    }

// 【请求】超进化升级
    interface ChaoJinHuaShengJiRsp extends RspObj {
        // 新的阶段
            newJie ?:number
        // 新的星级
            newXing ?:number

    }

// 【请求】超进化选择和切换
    interface ChaoJinHuaXuanZeReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
        // 新的形态id
            newDefId ?:number
    }

// 【请求】超进化选择和切换
    interface ChaoJinHuaXuanZeRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// 【请求】展示超进化属性
    interface ChaoJinHuaShowReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
        // 新的形态id
            newDefId ?:number
    }

// 【反馈】展示超进化属性
    interface ChaoJinHuaShowRsp extends RspObj {
        // 英雄dbId
            heroDbId ?:number
        // 当前属性列表
            curShuXingList ?:Array<ShuXingInfoUnit>
        // 预览属性列表
            showShuXingList ?:Array<ShuXingInfoUnit>

    }

// 【请求】展示超进化属性
    interface ChaoJinHuaZhuanHuanShowReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
        // 新的形态id
            newDefId ?:number
    }

// 【反馈】展示超进化属性
    interface ChaoJinHuaZhuanHuanShowRsp extends RspObj {
        // 英雄dbId
            heroDbId ?:number
        // 原始生命值
            hp0 ?:number
        // 原始攻击力
            attack0 ?:number
        // 原始防御值
            defense0 ?:number
        // 生命值
            hp ?:number
        // 攻击力
            attack ?:number
        // 防御值
            defense ?:number

    }

// 【请求】闪光喷漆展示属性
    interface ShanGuangPenQiShowReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
    }

// 【反馈】闪光喷漆展示属性
    interface ShanGuangPenQiShowRsp extends RspObj {
        // 英雄dbId
            heroDbId ?:number
        // 当前属性列表
            curShuXingInfoList ?:Array<ShuXingInfoUnit>
        // 预览属性列表
            showShuXingInfoList ?:Array<ShuXingInfoUnit>

    }

// 【请求】闪光喷漆
    interface ShanGuangPenQiReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
    }

// 【请求】闪光喷漆
    interface ShanGuangPenQiRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【请求】精灵洗练
    interface HeroXiLianReq extends ReqObj {
        // 英雄dbId
            heroDbId ?:number
        // 属性编号
            propNum ?:number
    }

// 【请求】精灵洗练
    interface HeroXiLianRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【请求】新版种族值升级
    interface NewZhongZuZhiReq extends ReqObj {
        // 查看的装备数据库ID
            heroId ?:number
    }

// 【反馈】新版种族值升级
    interface NewZhongZuZhiRsp extends RspObj {
        // 是否成功 true成功
            isSuccess ?:boolean

    }

// 【请求】新版种族值突破详情面板
    interface NewZhongZuZhiTuPoInfoReq extends ReqObj {
        // 查看的装备数据库ID
            heroId ?:number
    }

// 【反馈】新版种族值突破详情面板
    interface NewZhongZuZhiTuPoInfoRsp extends RspObj {
        // 突破面板消息
            tuPoInfoList ?:Array<ZhongZuZhiTuPoDetailsUnit>

    }

// 【请求】预览新版种族值
    interface YuLanNewZhongZuZhiReq extends ReqObj {
        // 查看的装备数据库ID
            heroId ?:number
    }

// 【反馈】预览新版种族值
    interface YuLanNewZhongZuZhiRsp extends RspObj {
        // 当前种族值等级
            currLevel ?:number
        // 当前属性列表
            propertyInfoList ?:Array<PropertyInfoUnit>
        // 下一级属性列表
            NextPropertyInfoList ?:Array<PropertyInfoUnit>
        // 精炼消耗列表
            costItemList ?:Array<JiangliInfoUnit>
        // 当前经验值
            currExp ?:number
        // 当前最小经验值
            minExp ?:number
        // 当前最大经验值
            maxExp ?:number
        // 是否突破
            isTuPo ?:boolean
        // 突破属性描述
            tuPoPropDesc ?:string
        // 是否最大值即不能再突破了
            isMax ?:boolean

    }

// 【反馈】新版种族值新增武将更新前端
    interface NewWuJiangZhongZuZhiTuPoInfoRsp extends RspObj {
        // 新宠种族值信息
            tuPoInfo ?:ZhongZuZhiTuPoInfoUnit

    }

// 【反馈】加载十连抽扭蛋图鉴列表
    interface GetShilianXianshiShidaiListReq extends ReqObj {
    }

// 【反馈】加载十连抽扭蛋图鉴列表
    interface GetShilianXianshiShidaiListRsp extends RspObj {
        // 十连抽扭蛋图鉴列表
            shidaiInfoList ?:Array<ShilianXianshiShidaiInfoUnit>

    }

// 获取属性信息列表
    interface GetShuXingListReq extends ReqObj {
    }

// 【反馈】获取属性信息列表
    interface GetShuXingListRsp extends RspObj {
        // 属性列表
            shuXingList ?:Array<NewShuXingInfoUnit>

    }

// 【请求】英雄显示装备样式设置
    interface HeroSetShowEquipFlagReq extends ReqObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。
            heroIndex ?:number
        // 是否显示装备样式
            isShowEquip ?:boolean
    }

// 【反馈】英雄显示装备样式设置
    interface HeroSetShowEquipFlagRsp extends RspObj {
        // 0表示主阵容，1-10表示1-10编队。默认值=主阵容
            index ?:number
        // 影响位置：11 ~ 19。
            heroIndex ?:number
        // 是否显示装备样式
            isShowEquip ?:boolean

    }

// 【反馈】记录图鉴已上阵Id
    interface AddActiveAniSignIDReq extends ReqObj {
        // 图鉴已上阵精灵宠物Id
            activeAniSignIDList ?:Array<number>
    }

// 【请求】疯狂十连抽面板信息
    interface GetCrazyGachaTenPullsPanelReq extends ReqObj {
    }

// 【反馈】疯狂十连抽面板信息
    interface GetCrazyGachaTenPullsPanelRsp extends RspObj {
        // 面板信息Unit
            panelInfo ?:CrazyGachaTenPullsPanelInfoUnit

    }

// 【请求】疯狂十连抽
    interface CrazyGachaTenPullsReq extends ReqObj {
        // 位置1-3
            pos ?:number
    }

// 【反馈】疯狂十连抽
    interface CrazyGachaTenPullsRsp extends RspObj {
        // 疯狂十连抽位置信息
            posInfo ?:CrazyGachaTenPullsPositionInfoUnit
        // 首次获得的四星英雄列表
            firstFourStarWujiangList ?:Array<JiangliInfoUnit>

    }

// 【请求】疯狂十连抽领取奖励
    interface GetRewardFromCrazyGachaTenPullsReq extends ReqObj {
        // 位置1-3
            pos ?:number
    }

// 【反馈】疯狂十连抽领取奖励
    interface GetRewardFromCrazyGachaTenPullsRsp extends RspObj {
        // 最好的那张五星卡
            bestReward ?:JiangliInfoUnit
        // 十连抽奖励结果
            rewardList ?:Array<JiangliInfoUnit>
        // 是否还有后续的疯狂十连抽
            isHasNext ?:boolean
        // 后续的疯狂十连抽面板信息Unit
            panelInfo ?:CrazyGachaTenPullsPanelInfoUnit

    }

// 【请求】种族抽卡
    interface ZhongzuQiyueGachaReq extends ReqObj {
        // 种族分类：1 机铠 2 魔灵 3 妖兽 6 新神&古神
            type ?:number
    }

// 【反馈】种族抽卡
    interface ZhongzuQiyueGachaRsp extends RspObj {
        // 种族分类：1 机铠 2 魔灵 3 妖兽 6 新神&古神
            type ?:number
        // 种族抽卡结果
            rewardInfo ?:JiangliInfoUnit

    }

// 【请求】心愿神魔面板
    interface WishHeroPanelReq extends ReqObj {
    }

// 【反馈】心愿神魔面板
    interface WishHeroPanelRsp extends RspObj {
        // 心愿神魔列表
            wishHeroList ?:Array<WishHeroUnit>
        // 可选的神魔列表
            optionalHeroList ?:Array<OptionalWishHeroInfoUnit>

    }

// 【请求】指定心愿神魔
    interface UpdateWishHeroReq extends ReqObj {
        // 新的心愿神魔Id列表
            wishHeroDefIdList ?:Array<number>
    }

// 【反馈】指定心愿神魔
    interface UpdateWishHeroRsp extends RspObj {

    }

// 【请求】积分契约
    interface GachaCostScoreReq extends ReqObj {
    }

// 【反馈】积分契约
    interface GachaCostScoreRsp extends RspObj {
        // 抽到的神魔
            reward ?:JiangliInfoUnit

    }

// 【请求】积分兑换奖池
    interface ShowWishJifenShenmoAllReq extends ReqObj {
    }

// 【反馈】积分兑换奖池
    interface ShowWishJifenShenmoAllRsp extends RspObj {
        // 奖励神魔列表
            petInfoList ?:Array<ShilianXianshiPetInfoUnit>

    }

// 【请求】加载世界BOSS信息
    interface GetBossInfoReq extends ReqObj {
    }

// 【反馈】加载世界BOSS信息
    interface GetBossInfoRsp extends RspObj {
        // Boss怪物ID，对应怪物表
            bossDefId ?:number
        // BOSS等级
            bossLevel ?:number
        // BOSS剩余血量百分比，1~10000
            bossHPValue ?:number
        // 活动状态:0活动未开启；1活动已开启
            status ?:number
        // 活动开启时间
            beginTime ?:number
        // 活动结束时间
            endTime ?:number
        // 剩余的征讨令数量
            token ?:number
        // 自动获得1个征讨令的时间, 0表示征讨令数量已满
            tokenUpdateTime ?:number
        // 当天买征讨令次数
            tokenBuy ?:number
        // 玩家自身的排名，0表示没有名次
            myIndex ?:number
        // 玩家给BOSS带来的伤害总值
            myDamage ?:number
        // 最终击杀者ID, 0表示没有
            lastKillPlayerId ?:number
        // 最终击杀者昵称
            lastKillPlayerName ?:string
        // 伤害排名列表
            rankList ?:Array<DamageRankInfoUnit>
        // 下次普通攻击时间，单位：毫秒；0表示可立即攻击
            nextAttackTime ?:number
        // 剩余挑战次数
            leftFightCount ?:number

    }

// 【请求】攻击世界BOSS
    interface BossFightReq extends ReqObj {
        // 是否双倍攻击
            isDouble ?:boolean
    }

// 【反馈】攻击世界BOSS
    interface BossFightRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 本次获得积分
            score ?:number
        // 本次BOSS受到的伤害
            damage ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】征讨令购买
    interface TokenBuyReq extends ReqObj {
    }

// 【反馈】征讨令购买
    interface TokenBuyRsp extends RspObj {
        // 剩余的征讨令数量
            token ?:number

    }

// 【请求】世界BOSS红点信息
    interface BossTipsInfoReq extends ReqObj {
    }

// 【反馈】世界BOSS红点信息
    interface BossTipsInfoRsp extends RspObj {
        // 活动状态:0活动未开启；1活动已开启
            status ?:number
        // 剩余的征讨令数量
            token ?:number

    }

// 【请求】立即消除世界BOSS普通战斗冷却时间
    interface RemoveBossFightCDReq extends ReqObj {
    }

// 【反馈】立即消除世界BOSS普通战斗冷却时间
    interface RemoveBossFightCDRsp extends RspObj {
        // 下次普通攻击时间，单位：毫秒；0表示可立即攻击
            nextAttackTime ?:number

    }

// 【请求】公会战鼓舞
    interface SGongHuiZhanGuWuReq extends ReqObj {
        // 玩家id
            crossPlayerId ?:number
        // 1:普通鼓舞; 2:一键鼓舞
            type ?:number
    }

// 【反馈】公会战鼓舞
    interface SGongHuiZhanGuWuRsp extends RspObj {
        // 玩家id
            crossPlayerId ?:number
        // 1:普通鼓舞; 2:一键鼓舞
            type ?:number
        // 返回编码, 1:表示玩家不存在
            returnCode ?:number

    }

// 【请求】公会战邮件奖励
    interface SGongHuiZhanEmailRewardReq extends ReqObj {
        // 1:个人 2:公会
            mailType ?:number
        // 目标id
            id ?:number
        // 排行
            rank ?:number
        // 公会战奖励策划表id
            gongHuiZhanJiangLiDefId ?:number
        // 公会战积分
            score ?:number
        // 跟着开服天数走的奖励数据
            jiangliList ?:Array<JiangliInfoUnit>
    }

// [请求]获得神器列表
    interface GetShenQiListReq extends ReqObj {
    }

// [返回]获得神器列表
    interface GetShenQiListRsp extends RspObj {
        // 属性
            shenQiList ?:Array<ShenQiInfoUnit>

    }

// [请求]神器升级
    interface ShenQiLevelUpReq extends ReqObj {
        // 神器dbId
            dbId ?:number
        // 属性
            shenQiXiaoHaoList ?:Array<ShenQiXiaoHaoInfoUnit>
    }

// [返回]神器升级
    interface ShenQiLevelUpRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]神器洗练
    interface ShenQiXiLianReq extends ReqObj {
        // 0钻石       1道具
            type ?:number
        // 
            dbId ?:number
        // 锁定属性的list
            suoDingList ?:Array<number>
    }

// [返回]神器洗练
    interface ShenQiXiLianRsp extends RspObj {
        // 属性类型
            shuXingType ?:number
        // 神器dbId
            dbId ?:number
        // 第一个属性  从0开始
            index ?:number
        // 属性值
            shuXingValue ?:number

    }

// 【请求】英雄神器穿上、卸下
    interface PutShenQiOnOffReq extends ReqObj {
        // Db ID
            shenQiDbID ?:number
        // 英雄Db ID
            heroDbID ?:number
        // 1 - 穿上， 2 - 卸下
            changeType ?:number
    }

// 【反馈】英雄神器穿上、卸下
    interface PutShenQiOnOffRsp extends RspObj {
        // 被操作的神器DB Id
            shenQiDbID ?:number

    }

// 【请求】锁定属性
    interface ShuXingSuoDingReq extends ReqObj {
        // Db ID
            shenQiDbID ?:number
        // 锁定的位置 从1开始
            index ?:number
    }

// 【反馈】锁定属性
    interface ShuXingSuoDingRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// 【请求】属性替换
    interface ShuXingTiHuanReq extends ReqObj {
        // Db ID
            shenQiDbID ?:number
        // 锁定的位置 从1开始
            index ?:number
    }

// 【反馈】属性替换
    interface ShuXingTiHuanRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]装备商店列表
    interface ZhuanBeiShopGoodsListReq extends ReqObj {
    }

// [反馈]装备商店列表
    interface ZhuanBeiShopGoodsListRsp extends RspObj {
        // 
            goodsList ?:Array<HuiShouShopGoodsInfoUnit>

    }

// [请求]购买装备商店商品
    interface BuyZhuanBeiShouShopGoodsReq extends ReqObj {
        // 策划表defId
            defId ?:number
        // 购买个数
            count ?:number
    }

// [返回]购买商品
    interface BuyZhuanBeiShouShopGoodsRsp extends RspObj {
        // 是否成功的标志
            isSuccess ?:boolean

    }

// [请求]精灵商店列表
    interface JingLingShopGoodsListReq extends ReqObj {
    }

// [反馈]精灵商店列表
    interface JingLingShopGoodsListRsp extends RspObj {
        // 
            goodsList ?:Array<HuiShouShopGoodsInfoUnit>
        // 刷新可用免费次数
            freeRefreshTimes ?:number
        // 今日已用刷新次数
            useRefreshTimesToday ?:number
        // 倒计时  0表示免费次数满了
            countdown ?:number

    }

// [请求]购买精灵商店商品
    interface BuyJingLingShouShopGoodsReq extends ReqObj {
        // 商品数据表dbID，数据表主Key
            goodsDBID ?:number
    }

// [返回]购买精灵商店商品
    interface BuyJingLingShouShopGoodsRsp extends RspObj {
        // 是否成功的标志
            isSuccess ?:boolean
        // 培养石的购买次数是否小于等于3次
            isPeiyangStoneLitteThanThree ?:boolean

    }

// [请求]刷新精灵商品
    interface RefreshJingLingShopReq extends ReqObj {
    }

// [返回]刷新商品
    interface RefreshJingLingShopRsp extends RspObj {
        // 是否成功的标志
            isSuccess ?:boolean

    }

// [请求]玩家威名面板信息
    interface GetWeimingLevelInfoReq extends ReqObj {
    }

// [返回]玩家威名面板信息
    interface GetWeimingLevelInfoRsp extends RspObj {
        // 威名当前阶段
            jieudan ?:number
        // 次数
            cishu ?:number
        // 满阶次数
            maxCishu ?:number
        // 威名阶段是否已经满阶
            isFullJieduan ?:boolean
        // 当前升级或者进阶信息
            currPropAndRank ?:WeimingPropAdnRankInfoUnit
        // 下一升级或者进阶信息
            nextPropAndRank ?:WeimingPropAdnRankInfoUnit
        // 升级或者进阶消耗
            costList ?:Array<JiangliInfoUnit>
        // 宝石列表
            gemstoneInfoList ?:Array<WeimingLevelGemstoneInfoUnit>
        // 技能阶段
            skillLevel ?:number
        // 威名技能Icon
            icon ?:string
        // 技能描述
            skillDesc ?:string
        // 威名名称资源
            weimingName ?:string

    }

// [请求]玩家威名升级进阶
    interface WeimingLevelUpReq extends ReqObj {
        // 操作类型 1升级（默认）；2进阶
            operationType ?:number
    }

// [返回]玩家威名升级进阶
    interface WeimingLevelUpRsp extends RspObj {
        // 威名当前阶段
            jieudan ?:number
        // 次数
            cishu ?:number
        // 满阶次数
            maxCishu ?:number
        // 威名阶段是否已经满阶
            isFullJieduan ?:boolean
        // 当前升级或者进阶信息
            currPropAndRank ?:WeimingPropAdnRankInfoUnit
        // 下一升级或者进阶信息
            nextPropAndRank ?:WeimingPropAdnRankInfoUnit
        // 升级或者进阶消耗
            costList ?:Array<JiangliInfoUnit>
        // 当前的威名宝石阶段是否已满
            isFullGemstoneJieduan ?:boolean

    }

// [请求]玩家威名技能信息
    interface GetWeimingSkillInfoReq extends ReqObj {
        // 威名阶段
            weimingJieduan ?:number
    }

// [返回]玩家威名技能信息
    interface GetWeimingSkillInfoRsp extends RspObj {
        // 威名阶段
            weimingJieduan ?:number
        // 威名当前技能信息
            skillInfo ?:WeimingSkillInfoUnit

    }

// [请求]玩家威名升级技能
    interface WeimingSkillLevelUpReq extends ReqObj {
        // 威名阶段
            weimingJieduan ?:number
    }

// [返回]玩家威名升级技能
    interface WeimingSkillLevelUpRsp extends RspObj {
        // 威名阶段
            weimingJieduan ?:number
        // 威名当前技能信息
            skillInfo ?:WeimingSkillInfoUnit

    }

// [请求]玩家威名充值面板信息
    interface GetWeimingRechargeInfoReq extends ReqObj {
    }

// [返回]玩家威名充值面板信息
    interface GetWeimingRechargeInfoRsp extends RspObj {
        // 威名已充值信息
            rechargeInfoList ?:Array<WeimingRechargeInfoUnit>

    }

// 【请求】请求boss等级列表
    interface GetBossLevelListReq extends ReqObj {
        // 1 本服boss 2 个人boss 3 引导boss 4 秘境boss 5 纪元boss(套装战力限制)
            type ?:number
    }

// 【反馈】请求boss列表
    interface GetBossLevelListRsp extends RspObj {
        // 本服boss页签
            benFuBossLevelList ?:Array<BossLevelListMapUnit>
        // 个人boss页签
            geRenBossVipList ?:Array<BossLevelListMapUnit>
        // boss剩余收益次数
            leftBossCount ?:number
        // 购买次数消耗
            buyJoinCount ?:JiangliInfoUnit
        // 购买次数消耗
            otherBuyJoinCount ?:JiangliInfoUnit
        // 当前可购买次数
            canBuyCount ?:number
        // 当前可够买次数是不是到最大了(提升v等也不能继续买了)
            isMaxCountVip ?:boolean
        // 套装总战力
            suitTotalZhanli ?:number
        // 开始时间戳 全服boss
            startTime ?:number
        // 结束时间戳 全服boss
            endTime ?:number
        // 下次刷新时间戳 全服boss
            nextRefreshTime ?:number
        // 自动恢复 剩余的可恢复次数 全服boss
            refreshCountLimit ?:number
        // 全服boss：刷新进入次数cd。 秘境boss：刷新解锁cd
            refreshCDInterval ?:number
        // 基础次数
            baseCount ?:number
        // 单次购买次数
            simpleBuyCount ?:number
        // CD次数刷新时间戳 本服boss、秘境boss
            bossCountCDTime ?:number
        // boss最大可收益次数
            maxBossCount ?:number
        // 位面战争：本段区服列表
            serverInfoList ?:Array<BossMapServerInfoUnit>

    }

// 【请求】请求boss列表
    interface GetBossListReq extends ReqObj {
        // 世代数
            shiDai ?:number
        // 1 本服 2个人
            sType ?:number
    }

// 【反馈】请求boss列表
    interface GetBossListRsp extends RspObj {
        // map列表
            bossMapList ?:Array<BossListMapUnit>

    }

// 【请求】进入地图
    interface EnterBossMapReq extends ReqObj {
        // 1 本服boss 2 个人boss
            sType ?:number
        // 引导任务Id 正常进入为0
            guideTaskId ?:number
        // 地图Id
            mapId ?:number
        // 追踪的玩家Id
            goalPlayerId ?:number
        // 援助的玩家Id
            yuanZhuPlayerId ?:number
        // 是否是人在地图内时的刷新重进
            isInside ?:boolean
        // 月卡，后端自赋值，不用前端赋值
            isMonthCardBuy ?:boolean
        // 终身卡，后端自赋值，不用前端赋值
            isWholeLifeCardBuy ?:boolean
        // 援助得到奖励的剩余次数，后端自赋值，不用前端赋值
            asstRewardLeftCount ?:number
        // 每秒回血数值，后端自赋值，不用前端赋值
            reclHpPerSecond ?:number
        // 秘法证书的作用秒数，后端自赋值，不用前端赋值
            mifaZhengshuSeconds ?:number
        // 秘法证书的CD秒数，后端自赋值，不用前端赋值
            mifaZhengshuCd ?:number
        // 命星技能表可增加的HP绝对值，后端自赋值，不用前端赋值
            fateStarSkillHp ?:number
        // 命星技能表的冷却秒数，后端自赋值，不用前端赋值
            fateStarSkillCd ?:number
    }

// 【反馈】进入地图
    interface EnterBossMapRsp extends RspObj {
        // map的Id
            defId ?:number
        // 地图名字
            mapName ?:string
        // 地图描述
            mapDesc ?:string
        // 1 本服 2个人
            sType ?:number
        // 1 和平场 2战争场
            sceneType ?:number
        // 1 最终一击 2 首次摸到 3 伤害第一
            ruleType ?:number
        // 地图(boss)等级
            mapLevel ?:number
        // 追击范围
            followRange ?:number
        // 移动速度
            moveSpeed ?:number
        // 地图上的怪物信息
            mapMonsterList ?:Array<BossMapMonsterUnit>
        // 地图上的其余人的信息
            playerList ?:Array<BossPlayerInfoUnit>
        // 0 正常进入 20场景已满员 21禁止进入此地图 22搜的人不存在 23 剩余次数不足 24 援助人数到达上限 25 boss已死
            returnCode ?:number
        // 复活消耗
            reLiveCost ?:Array<JiangliInfoUnit>
        // 免费复活时间
            reliveTime ?:number
        // 掉落奖励
            jiangLiList ?:Array<JiangliInfoUnit>
        // 是否可双倍奖励
            isDoubleReward ?:boolean
        // 双倍奖励消耗，可能为undefined
            costForDoubleReward ?:JiangliInfoUnit
        // 剩余可双倍奖励的机会次数
            doubleRewardCount ?:number
        // 上次发起援助的时间
            lastGetYuanZhuTime ?:number
        // 是否是第一次野外抓宠
            isFirstFieldPet ?:boolean
        // 多少秒后可以再次进入
            secondsForNextEnter ?:number
        // boss入场动作结束时间，可能为undefined，请判空
            bossAnimationEndTime ?:number
        // 援助得到奖励的剩余次数
            asstRewardLeftCount ?:number
        // 援助得到奖励的总次数
            asstRewardMaxCount ?:number
        // 是否激活了秘法证书
            isActiveMifaZhengshu ?:boolean
        // 秘法证书CD结束时间，单位：毫秒，0表示没有施放
            mifaZhengshuEndTime ?:number
        // 是否激活了命星技能
            isActiveFateStarSkill ?:boolean
        // 命星技能CD结束时间，单位：毫秒，0表示没有施放
            fateStarSkillEndTime ?:number

    }

// 【请求】离开地图
    interface PlayerLeaveMapReq extends ReqObj {
        // 是否正常离开场景
            isLeaveFromMenu ?:boolean
        // 1 本服 2个人
            sType ?:number
    }

// 【反馈】离开地图
    interface PlayerLeaveMapRsp extends RspObj {
        // 1自己离开 2被杀 3强制踢出 
            returnCode ?:number

    }

// 【请求】玩家移动
    interface PlayerMoveInMapReq extends ReqObj {
        // 开始移动时间
            startTime ?:number
        // 玩家当前坐标
            location ?:PointUnit
        // 移动路径
            pathList ?:Array<PointUnit>
        // 方向
            directionEnd ?:number
        // npc的Id
            npcId ?:number
        // 1 本服 2个人
            sType ?:number
    }

// 【反馈】玩家移动广播
    interface PlayerMoveInMapRsp extends RspObj {
        // 返回编码
            returnCode ?:number
        // 发起请求的npc的Id
            npcId ?:number

    }

// 【请求】玩家发起攻击
    interface PlayerAttackInMapReq extends ReqObj {
        // 1 本服 2个人
            sType ?:number
        // 目标怪
            bossId ?:number
        // 目标玩家ID,为0,表示取消跟踪
            playerId ?:number
    }

// 【反馈】玩家发起攻击
    interface PlayerAttackInMapRsp extends RspObj {
        // 返回编码, 0表示玩家存在, 120表示取消跟踪,其他返回号待定
            returnCode ?:number
        // 目标Id
            targetPlayerId ?:number
        // 目标Id
            targetBossId ?:number

    }

// 【反馈】玩家复活
    interface PlayerRecallHPInMapReq extends ReqObj {
        // 消耗的奖励类型 0为免费
            costType ?:number
        // 1 本服 2个人
            sType ?:number
    }

// 【反馈】玩家复活
    interface PlayerRecallHPInMapRsp extends RspObj {
        // 玩家当前血量
            hpValue ?:number
        // 方向
            directionEnd ?:number
        // 玩家坐标
            location ?:PointUnit
        // 玩家占有boss时间
            occupancyTime ?:number

    }

// 【请求】ping值
    interface CheckClientPingTimeInMapReq extends ReqObj {
        // 时间戳
            time ?:number
        // 1 本服 2个人
            sType ?:number
    }

// 【反馈】ping值
    interface CheckClientPingTimeInMapRsp extends RspObj {
        // 时间戳
            time ?:number
        // 上次ping的时间戳
            prePingTime ?:number

    }

// 【反馈】推送攻击信息
    interface PushAttackInfoInMapRsp extends RspObj {
        // 攻击方
            attack ?:AttackInMapUnit
        // 受击方列表
            goalList ?:Array<AttackInMapUnit>
        // 攻击秒数，数值为1-3
            attackSec ?:number

    }

// 【反馈】广播玩家移动路径
    interface BroadcastPlayerMoveInMapRsp extends RspObj {
        // 移动玩家ID
            playerId ?:number
        // 开始移动时间
            startTime ?:number
        // 玩家当前坐标
            location ?:PointUnit
        // 移动路径
            pathList ?:Array<PointUnit>
        // 方向
            directionEnd ?:number

    }

// 【反馈】广播玩家当前坐标纠正
    interface BroadcastPlayerLocationFixInMapRsp extends RspObj {
        // 移动玩家ID
            playerId ?:number
        // 新的玩家坐标,前端收到后,要立即做坐标纠正
            location ?:PointUnit
        // 方向
            directionEnd ?:number

    }

// 【反馈】广播新玩家进入场景了
    interface BroadcastPlayerEnterSceneInMapRsp extends RspObj {
        // 玩家信息
            playerInfo ?:BossPlayerInfoUnit

    }

// 【反馈】广播怪物信息
    interface BroadcastMonsterInfoInMapRsp extends RspObj {
        // 怪物信息
            bossInfo ?:BossMapMonsterUnit
        // 1 复活 2 死亡 3 回血 4 保护盾 5 无攻击回满血 
            sType ?:number
        // 奖励倒计时结束时间戳
            doubleEndTime ?:number
        // 归属者的奖励列表
            jiangLiOfRewardPlayerList ?:Array<JiangliInfoUnit>
        // 归属者的VIP奖励列表，有VIP限制的在VIP奖励那显示
            vipJiangOfRewardPlayerLiList ?:Array<VipJiangliInfoUnit>
        // 我的奖励列表
            myJiangLiList ?:Array<JiangliInfoUnit>
        // 我的VIP奖励列表，有VIP限制的在VIP奖励那显示
            myVipJiangLiList ?:Array<VipJiangliInfoUnit>
        // 归属者PlayerId
            rewardPlayerId ?:number

    }

// 【反馈】广播玩家离开场景
    interface BroadcastPlayerLeaveSceneInMapRsp extends RspObj {
        // 1 被杀 2其他被动离开 3.主动离开
            reason ?:number
        // 玩家ID
            playerId ?:number

    }

// 【反馈】广播归属权变化
    interface BroadcastRewardPersonChangeInMapRsp extends RspObj {
        // 归属权对应的boss
            bossId ?:number
        // 玩家ID 0为回到boss身上
            playerId ?:number
        // 伤害排行场 其余为0
            maxDamage ?:number
        // 玩家占有时间  单位:秒
            occupancTime ?:number

    }

// 【反馈】广播变化
    interface BroadcastPersonChangeInMapRsp extends RspObj {
        // 新加入地图的人
            addPerson ?:BossPlayerInfoUnit
        // 被挤掉的玩家Id
            playerId ?:number
        // 地图信息发生变化的玩家，例如HP变化等等
            changePerson ?:BossPlayerInfoUnit

    }

// 【反馈】搜人
    interface SearchPlayerInMapReq extends ReqObj {
        // 搜的人
            playerId ?:number
    }

// 【反馈】搜人
    interface SearchPlayerInMapRsp extends RspObj {
        // 0 没搜到
            status ?:number
        // 地图Id
            mapId ?:number
        // 地图名字
            mapName ?:string
        // boss map
            bossListMap ?:BossListMapUnit

    }

// 添加仇人
    interface AddChouRenInMapReq extends ReqObj {
        // 添加的仇人Id
            playerId ?:number
        // 1 本服 2个人
            sType ?:number
    }

// 【反馈】添加仇人
    interface AddChouRenInMapRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 购买个人boss挑战次数
    interface BuyGeRenBossJoinCountReq extends ReqObj {
        // boss类型
            bossType ?:number
    }

// 【反馈】购买个人boss挑战次数
    interface BuyGeRenBossJoinCountRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 购买次数的价格
            cost ?:JiangliInfoUnit

    }

// 【反馈】怪物列表额外推送
    interface BossLevelListPushRsp extends RspObj {
        // 类型
            sType ?:number
        // 世代数
            shiDai ?:number
        // 地图内的怪物信息
            mapMonsterList ?:Array<BossSceneOtherUnit>

    }

// 【反馈】当前可打的最大boss
    interface MaxBossCanFightPushRsp extends RspObj {
        // 地图信息
            mapInfo ?:BossListMapUnit

    }

// 【反馈】boss奖励双倍
    interface BossRewardDoubleReq extends ReqObj {
        // 是否双倍
            isDouble ?:boolean
        // 1 本服 2个人
            sType ?:number
    }

// 【反馈】boss奖励双倍
    interface BossRewardDoubleRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【反馈】请求boss援助
    interface GetBossYuanZhuReq extends ReqObj {
        // boss类型
            sceneType ?:number
    }

// 【反馈】请求boss援助
    interface GetBossYuanZhuRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 【反馈】推送boss援助列表
    interface PushBossYuanZhuRsp extends RspObj {
        // 援助信息列表
            yuanZhuInfoList ?:Array<BossYuanZhuInfoUnit>
        // 援助参与奖励
            yuanZhuJoinReward ?:Array<JiangliInfoUnit>
        // 援助成功奖励
            yuanZhuWinReward ?:Array<JiangliInfoUnit>
        // 援助成功最大次数
            yuanZhuWinMaxCount ?:number
        // 我的剩余次数
            myYuanZhuLeftWinCount ?:number
        // 援助点击最大次数
            yuanZhuJoinMaxCount ?:number
        // 我的剩余次数
            myYuanZhuLeftJoinCount ?:number

    }

// 【反馈】推送加血列表
    interface PushPlayerAddHpRsp extends RspObj {
        // 加血玩家列表
            addHpPlayerList ?:Array<BossPlayerAddHpUnit>

    }

// 【反馈】推送加血列表
    interface PushYuanZhuRewardRsp extends RspObj {
        // 援助参与奖励
            joinRewardList ?:Array<JiangliInfoUnit>
        // 援助获胜奖励
            winRewardList ?:Array<JiangliInfoUnit>

    }

// 修改boss状态
    interface BossStatusChangeReq extends ReqObj {
        // 场景类型
            sceneType ?:number
        // 1开始 2 结束
            sType ?:number
        // 剩余血量千分比
            leftHpPercent ?:number
        // 抓捕结果
            result ?:number
    }

// 【反馈】推送boss时间变化
    interface PushBossTimeChangeRsp extends RspObj {
        // 1 本服boss 2 个人boss 3 引导boss 4 秘境boss 5 纪元boss(套装战力限制)
            type ?:number
        // boss剩余收益次数
            leftBossCount ?:number
        // CD次数刷新时间戳 本服boss、秘境boss
            bossCountCDTime ?:number

    }

// 【跨服反馈】世界战场，剩余次数变化的推送
    interface BossLeftCountInfoPushRsp extends RspObj {
        // 世界战场剩余次数
            leftCountList ?:Array<BossLeftCountInfoUnit>

    }

// 【跨服推送】世界战场，Boss剩余HP变化的推送
    interface BossHpInfoPushRsp extends RspObj {
        // 世界战场Boss剩余HP
            bossHpInfo ?:BossHpInfoUnit

    }

// 施放秘法证书
    interface UseMifaZhengshuReq extends ReqObj {
        // 1 本服 2个人 4 究极 5 纪元
            sType ?:number
        // 技能分类：1 秘法证书；2命星技能
            opType ?:number
    }

// 【反馈】施放秘法证书
    interface UseMifaZhengshuRsp extends RspObj {
        // CD结束时间，单位：毫秒，0表示没有施放
            endTime ?:number
        // 技能分类：1 秘法证书；2命星技能
            opType ?:number

    }

// 【请求】本服Boss世界战场扫荡
    interface SweepBossInMapReq extends ReqObj {
        // 1 本服 2个人
            sType ?:number
    }

// 【反馈】本服Boss世界战场扫荡
    interface SweepBossInMapRsp extends RspObj {
        // 1 本服 2个人
            sType ?:number
        // 怪物信息
            bossInfo ?:BossMapMonsterUnit
        // 我的奖励列表
            myJiangLiList ?:Array<JiangliInfoUnit>
        // 我的VIP奖励列表，有VIP限制的在VIP奖励那显示
            myVipJiangLiList ?:Array<VipJiangliInfoUnit>

    }

// [请求]攻坚战道馆列表
    interface GongJianZhanDaoGuanListReq extends ReqObj {
    }

// [请求]攻坚战道馆列表
    interface GongJianZhanDaoGuanListRsp extends RspObj {
        // 道馆列表列表
            gaoGuanListInfo ?:Array<GongJianZhanDaoGuanInfoUnit>
        // 我公会的排名
            paiMing ?:number
        // 积分
            totalJiFen ?:number
        // 积分/分钟
            totalChanChu ?:number

    }

// [请求]进入攻坚战
    interface EnterGongJianZhanReq extends ReqObj {
        // 要进入哪个
            defId ?:number
        // 如果是通过邮件过来的 要给只给id
            emailDbId ?:number
    }

// [请求]进入攻坚战
    interface EnterGongJianZhanRsp extends RspObj {
        // 道馆人物列表
            gaoGuanRenWuListInfo ?:Array<GongJianZhanRenWuInfoUnit>
        // 剩余挑战次数
            leftTimes ?:number
        // 我的产出  积分/分钟
            myChanChu ?:number
        // 购买次数所花金券数量
            costJinQuan ?:number

    }

// [请求]打攻坚战
    interface FightGongJianZhanReq extends ReqObj {
        // 要进入哪个
            defId ?:number
        // 位置
            weiZhi ?:number
    }

// [请求]打攻坚战
    interface FightGongJianZhanRsp extends RspObj {
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]首届馆主
    interface GongJianZhanShouJieGuanZhuReq extends ReqObj {
    }

// [请求]首届馆主
    interface GongJianZhanShouJieGuanZhuRsp extends RspObj {
        // 道馆人物列表
            guanZhuInfoListInfo ?:Array<GuanZhuInfoUnit>

    }

// [请求]公会攻坚战排行
    interface GongHuiGongJianZhanPaiHangReq extends ReqObj {
    }

// [返回]公会攻坚战排行
    interface GongHuiGongJianZhanPaiHangRsp extends RspObj {
        // 列表
            gongHuiListInfo ?:Array<GongHuiListInfoUnit>
        // 我公会的信息
            myGongHuiListInfo ?:GongHuiListInfoUnit

    }

// [请求]攻坚战记录
    interface GongJianZhanRecordReq extends ReqObj {
    }

// [请求]攻坚战记录
    interface GongJianZhanRecordRsp extends RspObj {
        // 攻坚战打斗记录
            recordList ?:Array<GongHuiFenZangLogUnit>

    }

// [请求]攻坚战指派列表
    interface GongJianZhanZhiPaiListReq extends ReqObj {
    }

// [请求]攻坚战指派列表
    interface GongJianZhanZhiPaiListRsp extends RspObj {
        // 指派列表
            zhiPaiListInfo ?:Array<ZhiPaiInfoUnit>

    }

// [请求]攻坚战成员贡献列表
    interface ChengYuanGongXianListReq extends ReqObj {
    }

// [请求]攻坚战成员贡献列表
    interface ChengYuanGongXianListRsp extends RspObj {
        // 公会名称
            gongHuiName ?:string
        // 公会统计信息
            tongJiInfoList ?:Array<GongHuiTongJiInfoUnit>

    }

// [请求]邮件
    interface GongJianZhanEmailReq extends ReqObj {
        // 奖励列表
            content ?:string
        // 要进入哪个
            defId ?:number
        // 位置
            weiZhi ?:number
        // 玩家id
            playerId ?:number
    }

// [返回]邮件
    interface GongJianZhanEmailRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]公会道馆援助
    interface GonghuiDaoguanYuanzhuReq extends ReqObj {
        // 记录id
            recordId ?:number
        // 是否花钱
            isBuy ?:boolean
    }

// [返回]公会道馆援助
    interface GonghuiDaoguanYuanzhuRsp extends RspObj {
        // 
            isSuccess ?:boolean
        // 1记录过期 2已经抢夺成功 3已经被援助成功 4援助成功
            status ?:number
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]公会道馆发起援助
    interface GonghuiDaoguanFaQiYuanzhuReq extends ReqObj {
        // 记录id
            recordId ?:number
    }

// [返回]公会道馆发起援助
    interface GonghuiDaoguanFaQiYuanzhuRsp extends RspObj {
        // 
            isSuccess ?:boolean
        // 1记录过期 2已经抢夺成功 3已经被援助成功 4援助成功
            status ?:number

    }

// [请求]磨炼面板
    interface MoLianPanelReq extends ReqObj {
    }

// [返回]磨炼面板
    interface MoLianPanelRsp extends RspObj {
        // 
            moLianPaihangInfoList ?:Array<MoLianPaihangInfoUnit>
        // 已经通过的那个defId
            currDefId ?:number
        // true是  false 不是
            isMax ?:boolean
        // 有没有宝箱
            haveBaoXiang ?:boolean
        // 有没有可以领取的宝箱
            canGet ?:boolean
        // 出现隐藏任务的时候 传这个参数
            yinCangRenWuDef ?:YinCangRenWuDefUnit
        // 有没有领第三关的奖励
            isLingJiang ?:boolean
        // 所有buff属性
            shuXingInfoList ?:Array<ShuXingInfoUnit>
        // 是否可以领取buff
            isCanGetBuff ?:boolean
        // 是否可以进行战斗
            isCanFight ?:boolean
        // 最大通关数
            maxDefId ?:number
        // 胜利记录
            winRecord ?:boolean
        // 失败记录
            defeatRecord ?:boolean
        // 剩余挑战次数
            leftFightCount ?:number
        // 一键剩余次数
            yiJianLeftCount ?:number
        // 剩余免费扫荡次数
            leftFreeSweepCount ?:number

    }

// [请求]磨炼排行
    interface MoLianPaiHangReq extends ReqObj {
    }

// [返回]磨炼排行
    interface MoLianPaiHangRsp extends RspObj {
        // 
            moLianPaihangInfoList ?:Array<MoLianPaihangInfoUnit>
        // 玩家自己的排行
            playerPaiHang ?:number

    }

// 【请求】磨炼战斗
    interface MoLianFightPVEReq extends ReqObj {
    }

// 【反馈】磨炼战斗
    interface MoLianFightPVERsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】磨炼隐藏任务战斗
    interface MoLianRenWuFightPVEReq extends ReqObj {
    }

// 【反馈】磨炼隐藏任务战斗
    interface MoLianRenWuFightPVERsp extends RspObj {
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]领这界面上 第三关的奖励
    interface LingJiangReq extends ReqObj {
    }

// [返回]领这界面上 第三关的奖励
    interface LingJiangRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]随机buff界面
    interface RandomMoLianBuffPanelReq extends ReqObj {
    }

// [返回]随机buff界面
    interface RandomMoLianBuffPanelRsp extends RspObj {
        // 随机出来的buff列表
            buffIdList ?:Array<number>
        // 刷新所需金券数
            refreshCost ?:number

    }

// [请求]选择buff
    interface ChooseMoLianBuffReq extends ReqObj {
        // 选择的buffId
            buffId ?:number
    }

// [返回]选择buff
    interface ChooseMoLianBuffRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 类型
            buffType ?:number
        // 值
            buffValue ?:number

    }

// [请求]刷新buff
    interface RefreshMoLianBuffReq extends ReqObj {
    }

// [返回]刷新buff
    interface RefreshMoLianBuffRsp extends RspObj {
        // buff列表
            buffIdList ?:Array<number>
        // 刷新所需金券数
            refreshCost ?:number

    }

// 【请求】磨炼战斗简单
    interface MoLianFightNormalReq extends ReqObj {
        // 战斗类型 1普通 2 任务
            fightType ?:number
    }

// 【反馈】磨炼战斗简单
    interface MoLianFightNormalRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 是否胜利
            isWin ?:boolean
        // 战斗类型
            fightType ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// 【请求】磨炼战斗回放
    interface MoLianFightRecordReq extends ReqObj {
        // 1失败 2 胜利
            type ?:number
    }

// 【反馈】磨炼战斗回放
    interface MoLianFightRecordRsp extends RspObj {
        // 最近一次失败的战斗信息的Json串
            fightInfo ?:string

    }

// 【请求】一键战斗
    interface MoLianFightOneKeyReq extends ReqObj {
        // 1 弹面板预览扫荡掉落  2 确认消耗金券购买扫荡
            type ?:number
    }

// 【反馈】一键战斗
    interface MoLianFightOneKeyRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 1 弹面板预览扫荡掉落  2 确认消耗金券购买扫荡
            type ?:number
        // 扫荡次数
            saoDangCiShu ?:number
        // 消耗
            costItem ?:JiangliInfoUnit

    }

// 【请求】坐骑翅膀面板
    interface ZuoqiPanelReq extends ReqObj {
        // 1是坐骑；2是翅膀。
            type ?:number
    }

// 【反馈】坐骑翅膀面板
    interface ZuoqiPanelRsp extends RspObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 全队属性加成
            shuxingList ?:Array<PropertyInfoUnit>
        // 全部坐骑翅膀
            zuoqiList ?:Array<ZuoqiUnit>
        // 是否隐藏坐骑翅膀
            isHideZuoqi ?:boolean

    }

// 【请求】坐骑翅膀激活
    interface ZuoqiJihuoReq extends ReqObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 坐骑翅膀defId
            defId ?:number
        // 服务器内部请求
            isRequestFromServer ?:boolean
    }

// 【反馈】坐骑翅膀激活
    interface ZuoqiJihuoRsp extends RspObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 全队属性加成
            shuxingList ?:Array<PropertyInfoUnit>
        // 当前坐骑翅膀信息
            zuoqi ?:ZuoqiUnit

    }

// 【请求】坐骑翅膀升星面板
    interface ZuoqiShengxingPanelReq extends ReqObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 坐骑翅膀defId
            defId ?:number
    }

// 【反馈】坐骑翅膀升星面板
    interface ZuoqiShengxingPanelRsp extends RspObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 当前星级
            xingLevel ?:number
        // 当前星级属性
            currShuxingList ?:Array<PropertyInfoUnit>
        // 下一星级属性加成
            nextShuxingList ?:Array<PropertyInfoUnit>
        // 升星消耗
            shengxingCostList ?:Array<JiangliInfoUnit>
        // 是否使用中
            isInUse ?:boolean
        // 是否星级已满
            isFullXingLevel ?:boolean

    }

// 【请求】坐骑翅膀升星
    interface ZuoqiShengxingReq extends ReqObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 坐骑翅膀defId
            defId ?:number
    }

// 【反馈】坐骑翅膀升星
    interface ZuoqiShengxingRsp extends RspObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 当前星级
            xingLevel ?:number
        // 当前星级属性
            currShuxingList ?:Array<PropertyInfoUnit>
        // 下一星级属性加成
            nextShuxingList ?:Array<PropertyInfoUnit>
        // 升星消耗
            shengxingCostList ?:Array<JiangliInfoUnit>
        // 是否星级已满
            isFullXingLevel ?:boolean

    }

// 【请求】坐骑翅膀切换使用
    interface ZuoqiSwtichReq extends ReqObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 坐骑翅膀defId, -1表示隐藏坐骑翅膀;-2表示取消隐藏坐骑翅膀
            defId ?:number
    }

// 【反馈】坐骑翅膀切换使用
    interface ZuoqiSwtichRsp extends RspObj {
        // 1是坐骑；2是翅膀。
            type ?:number
        // 坐骑翅膀defId
            newDefId ?:number
        // 替换下来的坐骑翅膀defId
            oldDefId ?:number

    }

// 【请求】获取主角页面信息
    interface GetActorInfoReq extends ReqObj {
    }

// 【反馈】获取主角页面信息
    interface GetActorInfoRsp extends RspObj {
        // 排位赛defid
            paihangPPDefId ?:number
        // 战力榜排名
            zhanLiPaiHang ?:number
        // 战阵列表
            huizhangList ?:Array<ZhanzhenAllInfoUnit>
        // 图鉴数量
            tujianCount ?:number
        // 徽章图
            huiZhangTu ?:string
        // 头衔光环所需充值金额
            needRechargeYuanForTouxianGuanghuan ?:number
        // 是否已经激活秘法证书
            isMifaZhengshuAcitve ?:boolean

    }

// 【请求】获取主角免费时装
    interface GetAllFreeFashionReq extends ReqObj {
    }

// 【反馈】获取主角免费时装
    interface GetAllFreeFashionRsp extends RspObj {
        // 免费时装列表
            freeFashionList ?:Array<FreeFashionUnit>
        // 过期时间，单位毫秒
            expireMillis ?:number

    }

// 【请求】领取主角免费时装
    interface GetFreeFashionRewardsReq extends ReqObj {
        // 免费时装表Def id
            defId ?:number
    }

// 【反馈】领取主角免费时装
    interface GetFreeFashionRewardsRsp extends RspObj {
        // 奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// 【请求】头衔面板消息
    interface TouxianPanelReq extends ReqObj {
    }

// 【反馈】头衔面板消息
    interface TouxianPanelRsp extends RspObj {
        // 头衔信息
            touxianList ?:Array<TouxianInfoUnit>
        // 每日奖励信息
            dailyRewardList ?:Array<JiangliInfoUnit>
        // 是否领取了每日奖励
            isReceiveDailyReward ?:boolean
        // 头衔福利的奖励信息
            fuliRewardList ?:Array<JiangliInfoUnit>
        // 领取福利需要的头衔Id
            needTouxianDefId ?:number
        // 是否领取了福利奖励
            isReceiveFuliReward ?:boolean

    }

// 【请求】头衔晋升
    interface TouxianLevelUpReq extends ReqObj {
    }

// 【反馈】头衔晋升
    interface TouxianLevelUpRsp extends RspObj {
        // 晋升成功
            isSuccess ?:boolean

    }

// 【请求】领取头衔奖励
    interface ReceiveTouxianRewardReq extends ReqObj {
        // 1、领取头衔福利；2、领取每日奖励
            type ?:number
    }

// 【反馈】领取头衔奖励
    interface ReceiveTouxianRewardRsp extends RspObj {
        // 领取到的奖励列表
            rewardList ?:Array<JiangliInfoUnit>

    }

// 【请求】主角某子系统皮肤面板
    interface RoleSubSystemPifuPanelReq extends ReqObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
    }

// 【反馈】主角某子系统皮肤面板
    interface RoleSubSystemPifuPanelRsp extends RspObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 全队属性加成
            propertyList ?:Array<PropertyInfoUnit>
        // 全部的皮肤分组列表
            typeGroupList ?:Array<RoleSubSystemPifuTypeGroupUnit>

    }

// 【请求】主角某子系统皮肤激活
    interface RoleSubSystemPifuActiveReq extends ReqObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 皮肤defId
            defId ?:number
        // 服务器内部请求，前端忽略该字段
            isRequestFromServer ?:boolean
    }

// 【反馈】主角某子系统皮肤激活
    interface RoleSubSystemPifuActiveRsp extends RspObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 全队属性加成
            propertyList ?:Array<PropertyInfoUnit>
        // 当前皮肤信息
            pifuInfo ?:RoleSubSystemPifuUnit

    }

// 【请求】主角某子系统皮肤升星面板
    interface RoleSubSystemPifuStarLevelUpPanelReq extends ReqObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 皮肤defId
            defId ?:number
    }

// 【反馈】主角某子系统皮肤升星面板
    interface RoleSubSystemPifuStarLevelUpPanelRsp extends RspObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 当前星级
            starLevel ?:number
        // 当前星级属性
            currPropertyList ?:Array<PropertyInfoUnit>
        // 下一星级属性加成
            nextPropertyList ?:Array<PropertyInfoUnit>
        // 升星消耗
            starLevelUpCostList ?:Array<JiangliInfoUnit>
        // 是否使用中
            isInUse ?:boolean
        // 是否星级已满
            isFullStarLevel ?:boolean

    }

// 【请求】主角某子系统皮肤升星操作
    interface RoleSubSystemPifuStarLevelUpReq extends ReqObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 皮肤defId
            defId ?:number
    }

// 【反馈】主角某子系统皮肤升星操作
    interface RoleSubSystemPifuStarLevelUpRsp extends RspObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 当前星级
            starLevel ?:number
        // 当前星级属性
            currPropertyList ?:Array<PropertyInfoUnit>
        // 下一星级属性加成
            nextPropertyList ?:Array<PropertyInfoUnit>
        // 升星消耗
            starLevelUpCostList ?:Array<JiangliInfoUnit>
        // 是否星级已满
            isFullXingLevel ?:boolean

    }

// 【请求】主角某子系统皮肤切换使用
    interface RoleSubSystemPifuSwitchReq extends ReqObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 皮肤defId
            defId ?:number
    }

// 【反馈】主角某子系统皮肤切换使用
    interface RoleSubSystemPifuSwitchRsp extends RspObj {
        // 1是精灵皮肤；2是跟随宠皮肤；3是神格皮肤。
            systemType ?:number
        // 皮肤defId
            newDefId ?:number
        // 替换下来的皮肤defId
            oldDefId ?:number

    }

// 【请求】命星系统面板
    interface FateStarPanelReq extends ReqObj {
    }

// 【反馈】命星系统面板
    interface FateStarPanelRsp extends RspObj {
        // 当前所有命星的列表
            fateStarList ?:Array<FateStarInfoUnit>
        // 技能面板信息
            skillPanelInfo ?:FateStarSkillPanelInfoUnit

    }

// 【请求】激活命星点
    interface ActiveFateStarPointReq extends ReqObj {
    }

// 【反馈】激活命星点
    interface ActiveFateStarPointRsp extends RspObj {
        // 本次激活的命星点信息
            activePointInfo ?:FateStarPointInfoUnit
        // 命星当前点激活需要的消耗
            costList ?:Array<JiangliInfoUnit>
        // 开启下一个命星，如果当前命星满级了的话
            unlockFateStar ?:FateStarInfoUnit
        // 技能面板信息
            skillPanelInfo ?:FateStarSkillPanelInfoUnit

    }

// 【请求】头衔光环面板
    interface TouxianGuanghuanPanelReq extends ReqObj {
    }

// 【反馈】头衔光环面板
    interface TouxianGuanghuanPanelRsp extends RspObj {
        // 光环列表
            guanghuanList ?:Array<TouxianGuanghuanInfoUnit>
        // 玩家累充达到多少元可激活第一条数据
            needRechargeYuan ?:number
        // 总战力文字描述
            totalFightDesc ?:string
        // 总战力文字描述
            isActivityRunning ?:boolean
        // 头衔证书展示信息
            cumulativeRechargeDisplayInfo ?:CumulativeRechargeDisplayInfoUnit

    }

// 【请求】激活下一个头衔光环
    interface ActiveNextTouxianGuanghuanReq extends ReqObj {
    }

// 【反馈】激活下一个头衔光环
    interface ActiveNextTouxianGuanghuanRsp extends RspObj {
        // 本次激活的光环Id
            activeDefId ?:number

    }

// 获取合服首充活动列表信息
    interface GetHeFuShouChongInfosReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 获取合服首充活动列表信息
    interface GetHeFuShouChongInfosRsp extends RspObj {
        // 当前时间是第几天
            curDay ?:number
        // 活动列表
            activityList ?:Array<HeFuShouChongInfoUnit>

    }

// 领取合服首充奖励
    interface HeFuShouChongRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// 领取合服首充奖励
    interface HeFuShouChongRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 获取个人飙战列表信息
    interface GetGeRenBiaoZhanInfosReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 获取个人飙战列表信息
    interface GetGeRenBiaoZhanInfosRsp extends RspObj {
        // 充值金额
            money ?:number
        // 玩家当前战力
            curFight ?:number
        // 玩家合服前历史战力
            historyFight ?:number
        // 活动列表
            activityList ?:Array<GeRenBiaoZhanInfoUnit>

    }

// 领取合服首充奖励
    interface GeRenBiaoZhanRewardReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// 领取合服首充奖励
    interface GeRenBiaoZhanRewardRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 获取合服飙战列表信息
    interface GetHeFuBiaoZhanInfosReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 获取合服飙战列表信息
    interface GetHeFuBiaoZhanInfosRsp extends RspObj {
        // 提升战力排行(-1:榜外 其它:相应的排行)
            rank ?:number
        // 玩家合服前历史战力
            historyFight ?:number
        // 排行分数信息
            valueListInfo ?:Array<NewValueListInfoUnit>

    }

// 合服前预售活动
    interface GetHeFuYuShouInfosReq extends ReqObj {
        // 活动ID
            activityId ?:number
    }

// 合服前预售活动
    interface GetHeFuYuShouInfosRsp extends RspObj {
        // 奖励及货币数据 及奖励状态信息
            detailList ?:Array<HeFuYuShouGoodInfoUnit>

    }

// 单笔购买奖励
    interface HeFuYuShouBuyGoodReq extends ReqObj {
        // 活动ID
            activityId ?:number
        // 静态表id
            defId ?:number
    }

// 单笔购买奖励
    interface HeFuYuShouBuyGoodRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 静态表id
            defId ?:number
        // 已经购买次数,用于限购判断
            buyCount ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// [请求]印记列表
    interface YinJiLieBiaoReq extends ReqObj {
    }

// [返回]印记列表
    interface YinJiLieBiaoRsp extends RspObj {
        // 
            yinJiInfoList ?:Array<YinJiInfoUnit>

    }

// [请求]升级印记
    interface YinJiShengJiReq extends ReqObj {
        // 1火    2水    3草    4光 5暗
            type ?:number
    }

// [返回]升级印记
    interface YinJiShengJiRsp extends RspObj {
        // 
            yinJiInfoList ?:Array<YinJiInfoUnit>

    }

// [请求]我要变强面板
    interface WoYaoBianQiangPanelReq extends ReqObj {
        // 1 单宠成长 2 全队提升 
            type ?:number
    }

// [返回]我要变强面板
    interface WoYaoBianQiangPanelRsp extends RspObj {
        // 返回的全队信息
            teamInfoList ?:Array<WoYaoBianQiangUnit>
        // 返回的单宠信息
            heroInfoList ?:Array<WoYaoBianQiangDanChongUnit>

    }

// [请求]购买变强礼包
    interface GetBianQiangLiBaoReq extends ReqObj {
        // 礼包id
            libaodbId ?:number
    }

// [返回]购买变强礼包
    interface GetBianQiangLiBaoRsp extends RspObj {
        // 是否购买成功
            isSuccess ?:boolean

    }

// 【请求】奖励列表
    interface GetAwardsListReq extends ReqObj {
    }

// 【反馈】奖励列表
    interface GetAwardsListRsp extends RspObj {
        // 奖励列表
            awardInfo ?:Array<AwardInfoUnit>

    }

// 【请求】领取奖励
    interface ReceiveAwardReq extends ReqObj {
        // 奖励id
            id ?:number
    }

// 【反馈】领取奖励
    interface ReceiveAwardRsp extends RspObj {
        // id
            id ?:number
        // 是否成功
            isSuccess ?:boolean

    }

// 玩家登陆游戏请求
    interface LoginGameReq extends ReqObj {
        // 版本字符串
            versionString ?:string
        // 机型
            model ?:string
        // 服务器ID
            serverId ?:number
        // 玩家登陆token
            token ?:string
        // 玩家ID,如果没有则不传
            playerId ?:number
        // 渠道
            channel ?:string
        // 验证码
            sign ?:string
        // 独角兽sdk渠道ID
            channelId_djs ?:string
        // 独角兽subChild
            subChild_djs ?:string
        // 前端参数
            urlParam ?:string
        // sdk渠道
            sdkChannel ?:string
        // 渠道ID
            sdkId ?:string
    }

// 玩家登陆游戏反馈
    interface LoginGameRsp extends RspObj {
        // 返回码, 1未创建角色，0可登陆，-1登陆失败
            retCode ?:number
        // 消息提示
            reason ?:string
        // 随机名称. 当retCode=1时不为空
            randomName ?:string
        // 玩家用户ID
            playerId ?:number
        // 1为强制更新，-1为非强制更新，0为不需要更新
            updateCode ?:number
        // 客户端更新地址
            updateURL ?:string
        // 开服时间
            openServerTime ?:number
        // 是否测试服
            isTestServer ?:boolean
        // 是否是白名单用户
            isInBaiMingDan ?:boolean
        // 最小年龄
            minAge ?:number
        // 最大年龄
            maxAge ?:number

    }

// 玩家用户登陆请求
    interface UserLoginReq extends ReqObj {
        // 渠道
            channel ?:string
        // 系统平台：0-Android, 1-IOS
            platform ?:number
        // 玩家登陆IP
            IP ?:string
        // 设备ID（硬件设备唯一标示）
            deviceId ?:string
        // 服务器ID
            serverId ?:number
        // 独角兽sdk渠道ID
            channelId_djs ?:string
        // 独角兽subChild
            subChild_djs ?:string
        // 前端参数
            urlParam ?:string
        // sdk渠道
            sdkChannel ?:string
        // 渠道ID
            sdkId ?:string
        // 打点需要的额外参数
            ext ?:string
    }

// 玩家用户登陆反馈
    interface UserLoginRsp extends RspObj {
        // 基本信息
            basicInfo ?:PlayerBasicInfoUnit
        // 30钻石首抽
            wj_30firstGet ?:number
        // 30钻石上次免费抽时间
            wj_30lastGetTime ?:number
        // 30钻石免费抽取时间间隔
            wj_30freeTimeInterval ?:number
        // 300钻石首抽
            wj_300firstGet ?:number
        // 300钻石上次免费抽时间
            wj_300lastGetTime ?:number
        // 300钻石免费抽取时间间隔
            wj_300freeTimeInterval ?:number
        // 英雄招募必出5星积分
            wj_get5Acc ?:number
        // 装备容量上限
            zhuangbei_countMax ?:number
        // 道具容量上限(玩家背包)，最大250个
            daoju_countMax ?:number
        // 武将容量上限(玩家背包)，初始50，最大100个
            wj_countMax ?:number
        // 服务器时间
            serverTime ?:number
        // 新手引导使用该字段，取值范围1~255。注：该字段只在登陆时传一次。
            eduIdx ?:number
        // 当天已购买银两次数
            buyYinliangCount ?:number
        // 注册时间
            createTime ?:number
        // 世界BOSS积分
            score ?:number
        // false 不是  true是
            isYueKa ?:boolean
        // false 不是  true是
            isZhongShen ?:boolean
        // 是否年卡用户
            isYearCard ?:boolean
        // 阶段性引导
            val ?:number
        // 剩余次数
            leftCount ?:number
        // 剩余时间 单位 秒
            leftTime ?:number
        // 激活协作次数
            jihuoxiezuocishu ?:number
        // 自动重连验证码
            reconnectCode ?:number
        // 前端存储数据
            statusJson ?:string
        // 公会数据
            gongHuiData ?:GongHuiDataUnit
        // PVP场景ID
            sceneId ?:number
        // 个人PVP地图层级,55(默认值),75,95,115,135,即开放等级,依次+20
            sceneLevel ?:number
        // 称号
            chenghaoId ?:number
        // 金券
            coupon ?:number
        // 等级大转盘摇奖次数
            zhuanPanRewardCount ?:number
        // 进入游戏后首页Project.id
            gameEnterProjectType ?:number
        // Rogoulike地下城地图类型
            rogoulikeInfoMapType ?:number

    }

// 玩家创建角色请求
    interface CreateActorReq extends ReqObj {
        // 玩家昵称
            playerName ?:string
        // 职业Id
            professionDefId ?:number
        // 初始武将ID
            heroId ?:number
        // 服务器ID
            serverId ?:number
        // 邀请者的平台账号id  openid
            inviterAccouId ?:string
        // sdk打点数据
            ext ?:string
        // 新的骨骼配置信息
            newGugePartList ?:Array<GugePartUnit>
        // 是否一键快速创角
            isOneKey ?:boolean
        // 创建角色所选年龄
            age ?:number
    }

// 玩家用户登陆反馈
    interface CreateActorRsp extends RspObj {
        // 玩家ID
            playerId ?:number
        // 玩家昵称
            playerName ?:string
        // 初始武将ID
            heroId ?:number

    }

// 玩家重新登录在重连后
    interface LoginOnReconnectingReq extends ReqObj {
        // 用户ID
            playerId ?:number
        // 版本字符串
            versionString ?:string
        // 服务器ID
            serverId ?:number
        // 渠道
            channel ?:string
        // 自动重连验证码
            reconnectCode ?:number
        // 机型
            model ?:string
        // 独角兽sdk渠道ID
            channelId_djs ?:string
        // 独角兽subChild
            subChild_djs ?:string
        // 前端参数
            urlParam ?:string
        // sdk渠道
            sdkChannel ?:string
        // sdkId
            sdkId ?:string
    }

// 玩家重新登录在重连后
    interface LoginOnReconnectingRsp extends RspObj {
        // 返回码, 1未创建角色，0可登陆，-1登陆失败
            retCode ?:number
        // 1为强制更新，-1为非强制更新，0为不需要更新
            updateCode ?:number
        // 自动重连验证码
            reconnectCode ?:number

    }

// 标记玩家已经接收到PushPlayerBasicInfoAtLogin请求了
    interface MarkPlayerReceivePushPlayerBasicInfoAtLoginMessageReq extends ReqObj {
    }

// 玩家用户属性更新
    interface PlayerPropUpdateRsp extends RspObj {
        // 属性编码集合
            propCodes ?:Array<number>
        // 属性值集合
            propValues ?:Array<number>

    }

// 玩家用户登出
    interface UserLogoutReq extends ReqObj {
    }

// 反馈玩家用户登出
    interface UserLogoutRsp extends RspObj {
        // 是否成功登陆,失败的话提醒重新登录
            isSuccess ?:boolean

    }

// 玩家登录成功进入家园主画面后的请求，用于一些挂机结算处理
    interface LoginIsOKReq extends ReqObj {
    }

// 【请求】玩家功能开启
    interface PlayerFunctionReq extends ReqObj {
        // 功能开启id
            functionId ?:number
    }

// 保存玩家等级提示引导
    interface SaveLevelTipReq extends ReqObj {
        // 要保存的等级提示
            levelTip ?:number
    }

// 保存玩家等级提示引导
    interface SaveLevelTipRsp extends RspObj {
        // 是否保存成功
            isSuccess ?:boolean

    }

// 获取等级提示引导
    interface GetLevelTipReq extends ReqObj {
    }

// 获取等级提示引导
    interface GetLevelTipRsp extends RspObj {
        // 要保存的等级提示
            levelTip ?:number

    }

// 获取分享奖励
    interface GetFenxiangReq extends ReqObj {
        // token值 前端传过来的 用作api的验证
            token ?:string
        // 分享信息 玩家填的
            content ?:string
        // 名称
            channel ?:string
    }

// 获取分享奖励
    interface GetFenxiangRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 剩余次数
            leftCount ?:number
        // 剩余时间 单位 秒
            leftTime ?:number

    }

// 获取关注奖励
    interface GetGuanZhuReq extends ReqObj {
        // 渠道代码
            channel ?:string
    }

// 获取关注奖励
    interface GetGuanZhuRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 获取关注奖励
    interface GetGuanZhu175Req extends ReqObj {
        // gid
            gid ?:string
    }

// 获取关注奖励
    interface GetGuanZhu175Rsp extends RspObj {
        // 返回结果 1未订阅 2 发奖 3 已经关注过了 
            flg ?:number
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>

    }

// 是否可以分享
    interface CanFenxiangReq extends ReqObj {
        // 名称
            channel ?:string
    }

// 是否可以分享
    interface CanFenxiangRsp extends RspObj {
        // true 可以  false 不可以
            isSuccess ?:boolean

    }

// 邀请奖励发放
    interface GetInviteRewardReq extends ReqObj {
        // 渠道代码
            channel ?:string
        // 邀请者的渠道用户唯一标识
            accountName ?:string
        // 被邀请者的渠道用户唯一标识
            inviteeAccountName ?:string
    }

// 邀请奖励发放
    interface GetInviteRewardRsp extends RspObj {
        // 0 成功 1 无此用户 2 邀请奖励达到上限 3 被邀请用者重复发放
            code ?:number

    }

// 某页面进入通知指令
    interface OnPageEnterReq extends ReqObj {
        // projectID
            projectId ?:number
        // 页面名称，具体页面代码，需要跟前端一起核对，严格区分大小写
            pageName ?:string
    }

// 某页面退出通知指令
    interface OnPageExitReq extends ReqObj {
        // 页面名称，具体页面代码，需要跟前端一起核对，严格区分大小写
            projectId ?:number
        // projectID
            pageName ?:string
    }

// 【请求】更多随机昵称
    interface MoreRandomNamesReq extends ReqObj {
        // 服务器ID
            serverId ?:number
    }

// 【反馈】更多随机昵称
    interface MoreRandomNamesRsp extends RspObj {
        // 随机昵称列表
            nickNameList ?:Array<string>

    }

// 【请求】检查玩家昵称是否可使用
    interface MarkNickNameUseReq extends ReqObj {
        // 玩家昵称
            nickName ?:string
        // 服务器ID
            serverId ?:number
        // 年龄
            age ?:number
        // 返回的职业次数
            professionCount ?:number
    }

// 【反馈】检查玩家昵称是否可使用
    interface MarkNickNameUseRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 玩家昵称
            nickName ?:string
        // 年龄
            age ?:number
        // 待选择的职业列表，一共3个
            professionList ?:Array<PlayerProfessionUnit>

    }

// 【请求】激活年卡
    interface GetYearCardReq extends ReqObj {
        // mubiaoDef.Id
            mubiaoDefId ?:number
    }

// 【反馈】激活年卡
    interface GetYearCardRsp extends RspObj {
        // 是否年卡用户
            isYearCard ?:boolean

    }

// 【请求】请求随机名字
    interface GetRandomNamesReq extends ReqObj {
        // mubiaoDef.Id
            mubiaoDefId ?:number
    }

// 【反馈】请求随机名字
    interface GetRandomNamesRsp extends RspObj {
        // 随机昵称列表
            nickNameList ?:Array<string>

    }

// 【推送】推送主角武将等批量变化属性
    interface PushChangedEntitiesBatchReq extends ReqObj {
        // Player Id
            playerId ?:number
        // 1主角; 2武将
            type ?:number
    }

// 保存音乐开关等状态信息
    interface StatusJsonReq extends ReqObj {
        // 
            StatusJson ?:string
    }

// 保存玩家创建角色步骤
    interface CreatePlayerStepRecordReq extends ReqObj {
        // 1,开始游戏第一步(不用发); 2,选择合适的昵称完毕; 3,选择合适的主宠物完毕
            step ?:number
    }

// 保存关注状态
    interface SaveGuanZhuStatusReq extends ReqObj {
        // 是否已经关注 0 没有    1有
            isGuanZhu ?:boolean
    }

// 保存关注状态
    interface SaveGuanZhuStatusRsp extends RspObj {
        // 是否领取成功
            isSuccess ?:boolean

    }

// 获取前端特定符号状态
    interface GetClientMarkValueReq extends ReqObj {
        // 类型
            type ?:number
    }

// 获取前端特定符号状态
    interface GetClientMarkValueRsp extends RspObj {
        // 类型
            type ?:number
        // 数值
            value ?:number

    }

// 标识前端特定符号状态值
    interface MarkClientMarkValueReq extends ReqObj {
        // 类型
            type ?:number
        // 增加的数值
            addValue ?:number
    }

// 【请求】通过渠道请求奖励
    interface GetJiangLiListByChannelReq extends ReqObj {
        // 奖励分类:1关注；2实名认证；3分享；4添加桌面；
            type ?:number
        // 渠道
            channel ?:string
    }

// 【反馈】通过渠道请求奖励
    interface GetJiangLiListByChannelRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】玩吧奖励
    interface GetWanBaJiangLiListReq extends ReqObj {
        // 渠道
            channel ?:string
    }

// 【反馈】玩吧奖励
    interface GetWanBaJiangLiListRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】玩家登陆流程处理
    interface PlayerLoginProcessReq extends ReqObj {
        // 1、正常登陆;2、断线重连
            loginType ?:number
        // 渠道
            channel ?:string
        // 系统平台：0-Android, 1-IOS
            platform ?:number
        // 玩家登陆IP
            IP ?:string
        // 设备ID（硬件设备唯一标示）
            deviceId ?:string
        // 服务器ID
            serverId ?:number
        // 用户ID
            playerId ?:number
        // 版本字符串
            versionString ?:string
        // 自动重连验证码
            reconnectCode ?:number
        // 独角兽sdk渠道ID
            channelId_djs ?:string
        // 独角兽subChild
            subChild_djs ?:string
        // sdk渠道
            sdkChannel ?:string
        // 渠道ID
            sdkId ?:string
    }

// 【请求】赠送玩家金券
    interface AddJinQuanReq extends ReqObj {
        // 充值玩家id
            fromPlayerId ?:number
        // 目标玩家id
            goalPlayerId ?:number
        // 充值金额id
            defId ?:number
        // 订单号
            orderId ?:number
        // 金券数量
            num ?:number
    }

// 新手引导面板
    interface NewPlayerGuidePanelReq extends ReqObj {
    }

// 【反馈】新手引导面板
    interface NewPlayerGuidePanelRsp extends RspObj {
        // 已经摇了几次奖
            getRewardCount ?:number
        // 已经摇出来的rewardId
            hasRewards ?:Array<number>
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 新手引导摇奖
    interface GetNewPlayerYaoJiangReq extends ReqObj {
    }

// 【反馈】新手引导摇奖
    interface GetNewPlayerYaoJiangRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 领取累计钻石
    interface GetLeiJiZuanShiReq extends ReqObj {
    }

// 【反馈】领取累计钻石
    interface GetLeiJiZuanShiRsp extends RspObj {
        // 领取了多少钻石
            count ?:number
        // 是否成功
            isSuccess ?:boolean

    }

// 通知活动结束
    interface NotifyActivityCloseReq extends ReqObj {
    }

// 【反馈】通知活动结束
    interface NotifyActivityCloseRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean

    }

// 获取活动是否结束
    interface GetActivityCloseReq extends ReqObj {
    }

// 【反馈】获取活动是否结束
    interface GetActivityCloseRsp extends RspObj {
        // 是否结束
            isClose ?:boolean

    }

// 【反馈】前端接到这个消息 弹出防沉迷面板 不输入关面板或者输入成年身份证  前端自己做刷新
    interface FangChenMiPanelRsp extends RspObj {
        // 为什么推送   1剩余30分钟时推送   2在线超过3小时 推送 
            reason ?:number
        // 玩家的认证状态  返回值:0 未录入 1 表示录入的身份证是成年人身份证 2 合法但是属于未成人年
            status ?:number
        // 提示文字内容
            words ?:string

    }

// 【反馈】 输入防沉迷信息  前端做验证 是不是成年 如果是 给后端发这个  不关游戏
    interface InputFangChenMiInfoReq extends ReqObj {
        // 姓名
            name ?:string
        // 身份证号码
            number ?:string
    }

// 【反馈】 输入防沉迷信息  前端做验证 是不是成年 如果是 给后端发这个  不关游戏
    interface InputFangChenMiInfoRsp extends RspObj {
        // 玩家当前防沉迷的状态返回值 字符串:1 表示身份证录入合法 2 合法但是属于未成人年 其他均不合法是否结束
            status ?:number

    }

// 【请求】 取得对应id的服务器内信息
    interface GetBeiZhuLiteReq extends ReqObj {
        // 请求的信息id
            id ?:number
    }

// 【回馈】  取得对应id的服务器内信息
    interface GetBeiZhuLiteRsp extends RspObj {
        // 备注lite内的信息
            words ?:string

    }

// 【请求】 输入手机号领取奖励
    interface GetPhoneRewardReq extends ReqObj {
        // 输入的手机号
            phoneNum ?:string
    }

// 【回馈】输入手机号领取奖励
    interface GetPhoneRewardRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】增加分享次数
    interface AddShareTimesReq extends ReqObj {
    }

// 【反馈】增加分享次数
    interface AddShareTimesRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>
        // 是否发奖
            isGiveBonus ?:boolean

    }

// 【请求】收藏小游戏奖励
    interface GetShouCangLiReq extends ReqObj {
    }

// 【反馈】收藏小游戏奖励
    interface GetShouCangLiRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】跳过战斗
    interface TiaoGuoZhanDouReq extends ReqObj {
    }

// 【反馈】跳过战斗
    interface TiaoGuoZhanDouRsp extends RspObj {

    }

// 【请求】修正地址指令
    interface FixRemoteResourceUrlRsp extends RspObj {
        // vip等级高的玩家的请求地址
            vipResourceUrl ?:string

    }

// 回到挂机页面，第一次登陆时不要调用，只能是从其他页面返回时调用
    interface GoBackToGuajiLayerReq extends ReqObj {
    }

// 【请求】收藏小游戏奖励展示
    interface ShouCangLiShowReq extends ReqObj {
    }

// 【反馈】收藏小游戏奖励展示
    interface ShouCangLiShowRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【反馈】创角弹出特殊奖励
    interface PushSpecialCreateRewardRsp extends RspObj {
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】领取创角特殊奖励
    interface GetSpecialCreateRewardReq extends ReqObj {
    }

// 【反馈】领取创角特殊奖励
    interface GetSpecialCreateRewardRsp extends RspObj {

    }

// 【请求】加载渠道通用分类奖励请求
    interface LoadChannelCommonRewardReq extends ReqObj {
        // 请求分类：1、QQ订阅奖励
            type ?:number
    }

// 【反馈】加载渠道通用分类奖励请求
    interface LoadChannelCommonRewardRsp extends RspObj {
        // 请求分类：1、QQ订阅奖励
            type ?:number
        // 是否已经领取过该奖励了
            isGetReward ?:boolean
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】领取渠道通用分类奖励请求
    interface GetChannelCommonRewardReq extends ReqObj {
        // 请求分类：1、QQ订阅奖励
            type ?:number
    }

// 【反馈】领取渠道通用分类奖励请求
    interface GetChannelCommonRewardRsp extends RspObj {
        // 请求分类：1、QQ订阅奖励
            type ?:number
        // 奖励列表
            jiangLiList ?:Array<JiangliInfoUnit>

    }

// 【请求】刷新职业
    interface RefreshPlayerProfessionReq extends ReqObj {
        // 服务器ID
            serverId ?:number
        // 年龄
            age ?:number
        // 返回的职业次数
            professionCount ?:number
    }

// 【反馈】刷新职业
    interface RefreshPlayerProfessionRsp extends RspObj {
        // 年龄
            age ?:number
        // 待选择的职业列表，一共3个
            professionList ?:Array<PlayerProfessionUnit>

    }

// 【请求】通过渠道请求奖励状态
    interface GetJiangLiStatusByChannelReq extends ReqObj {
        // 奖励分类列表:1关注；2实名认证；3分享；4添加桌面；5微信授权；6绑定手机号
            typeList ?:Array<number>
        // 渠道
            channel ?:string
    }

// 【反馈】通过渠道请求奖励
    interface GetJiangLiStatusByChannelRsp extends RspObj {
        // 状态列表
            jiangLiStatusList ?:Array<ChannelJiangLiStatusUnit>

    }

// 【请求】消耗道具获取体力
    interface GetTiliByDaojuReq extends ReqObj {
        // 道具dbID，道具表数据表主键
            daojuDbID ?:number
    }

// 【反馈】消耗道具获取体力
    interface GetTiliByDaojuRsp extends RspObj {

    }

// [请求]敌人列表
    interface GuaJiEnergyListReq extends ReqObj {
        // 已经存在的敌人列表
            hasPlayerIdList ?:Array<number>
        // 请求敌人数量
            enemyCount ?:number
    }

// [反馈]敌人列表
    interface GuaJiEnergyListRsp extends RspObj {
        // 敌人列表
            guaJiEnergyInfoList ?:Array<GuaJiEnergyInfoUnit>

    }

// [请求]被动战斗
    interface BeiDongZhanDouReq extends ReqObj {
        // 机器人的id
            guaJiEnergyPlayerId ?:number
    }

// [返回]被动战斗
    interface BeiDongZhanDouRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 积分
            jifen ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]主动战斗
    interface ZhuDongZhanDouReq extends ReqObj {
        // 机器人的id
            guaJiEnergyPlayerId ?:number
    }

// [返回]主动战斗
    interface ZhuDongZhanDouRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 积分
            jifen ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>
        // 本次增加的积分
            addJifen ?:number

    }

// [请求]战斗记录
    interface FightRecordReq extends ReqObj {
        // true  需要提醒  false 不要
            warn ?:boolean
        // 机器人的id
            guaJiEnergyPlayerId ?:number
        // 是否是任务引导
            isGuide ?:boolean
    }

// [返回]战斗记录
    interface FightRecordRsp extends RspObj {
        // 战斗记录信息
            guaJiEnergyRecordAndChouRenInfoList ?:Array<GuaJiEnergyRecordAndChouRenInfoUnit>
        // true  需要提醒  false 不要
            warn ?:boolean
        // 仇人数量
            chouRenCount ?:number
        // 复仇得奖上限
            fuChouRewardMax ?:number
        // 当前复仇次数
            curFuChouCount ?:number
        // 剩余获奖总次数
            leftTotal ?:number
        // 剩余可买次数
            leftBuyTotal ?:number
        // 购买次数的价格
            cost ?:JiangliInfoUnit

    }

// [请求]复仇
    interface FuChouReq extends ReqObj {
        // 记录的id
            recordDbId ?:number
        // 
            guaJiEnergyPlayerId ?:number
        // 敌人头像id
            energyTouXiangId ?:number
    }

// [返回]复仇
    interface FuChouRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 积分
            jifen ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>
        // 剩余获奖总次数
            leftTotal ?:number
        // 剩余可买次数
            leftBuyTotal ?:number
        // 本次增加的积分
            addJifen ?:number

    }

// [请求]敌人消息
    interface GuaJiEnergyDisappearReq extends ReqObj {
        // 敌人的id列表
            guaJiEnergyPlayerIdList ?:Array<number>
    }

// [请求]敌人列表
    interface YinDaoGuaJiEnergyListReq extends ReqObj {
    }

// [反馈]敌人列表
    interface YinDaoGuaJiEnergyListRsp extends RspObj {
        // 敌人列表
            guaJiEnergyInfoList ?:Array<GuaJiEnergyInfoUnit>

    }

// [请求]主动战斗
    interface YinDaoZhuDongZhanDouReq extends ReqObj {
    }

// [返回]主动战斗
    interface YinDaoZhuDongZhanDouRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 积分
            jifen ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>

    }

// [请求]仇人列表
    interface ChouRenListReq extends ReqObj {
    }

// [返回]仇人列表
    interface ChouRenListRsp extends RspObj {
        // 战斗记录信息
            guaJiEnergyRecordAndChouRenInfoList ?:Array<GuaJiEnergyRecordAndChouRenInfoUnit>
        // 抢夺宿敌得奖上限
            suDiRewardMax ?:number
        // 当前抢夺宿敌次数
            curQiangDuoCount ?:number
        // 搜索消耗
            searchCost ?:JiangliInfoUnit
        // 剩余获奖总次数
            leftTotal ?:number
        // 剩余可买次数
            leftBuyTotal ?:number
        // 购买次数的价格
            cost ?:JiangliInfoUnit

    }

// [请求]抢夺仇人
    interface QiangDuoChouRenReq extends ReqObj {
        // 
            dbId ?:number
    }

// [返回]抢夺仇人
    interface QiangDuoChouRenRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 异常码：0无异常，1体力不足，2超过挑战次数
            ngCode ?:number
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 返回码：星数结果：1星、2星、3星
            starCount ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string
        // 返回码：玩家增加的经验值
            playerExpAdd ?:number
        // 返回码：武将增加的经验值
            wujiangExpAdd ?:number
        // 积分
            jifen ?:number
        // 战败分析的list
            zhanBaiFenXiList ?:Array<ZhanBaiFenXiInfoUnit>
        // 剩余获奖总次数
            leftTotal ?:number
        // 剩余可买次数
            leftBuyTotal ?:number
        // 增加的积分
            addJifen ?:number

    }

// [请求]添加仇人
    interface AddChouRenReq extends ReqObj {
        // 玩家挂机对战记录Db Id
            dbId ?:number
        // 绕过复仇记录，直接添加仇人，=player.Id
            enemyId ?:number
    }

// [反馈]添加仇人
    interface AddChouRenRsp extends RspObj {
        // 添加仇人成功
            isSuccess ?:boolean

    }

// [请求]删除仇人
    interface ShanChuChouRenReq extends ReqObj {
        // 
            dbId ?:number
    }

// [反馈]删除仇人
    interface ShanChuChouRenRsp extends RspObj {
        // 
            isSuccess ?:boolean

    }

// [请求]开宝箱
    interface KeiBaoXiangReq extends ReqObj {
        // 1金钥匙     2银钥匙
            type ?:number
    }

// [反馈]开宝箱
    interface KeiBaoXiangRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 
            playerId ?:number
        // 我的宝箱个数
            pvpBox ?:number
        // 我的金钥匙个数
            pvpBoxGoldKey ?:number
        // 我的银钥匙个数
            pvpBoxSliverKey ?:number
        // 
            isSuccess ?:boolean
        // 
            errorCode ?:number

    }

// [请求]挂机pvp宝箱面板
    interface GuaJiPVPBaoXiangPenalReq extends ReqObj {
    }

// [反馈]挂机pvp宝箱面板
    interface GuaJiPVPBaoXiangPenalRsp extends RspObj {
        // 我的宝箱个数
            pvpBox ?:number
        // 我的金钥匙个数
            pvpBoxGoldKey ?:number
        // 我的银钥匙个数
            pvpBoxSliverKey ?:number

    }

// [请求]根据玩家名字 获得地图跑动敌人 跨服
    interface GetDiTuDiRenByPlayerNameCrossServerReq extends ReqObj {
        // 目标昵称
            targetPlayerName ?:string
        // 当前玩家Id
            playerId ?:number
    }

// [反馈]根据玩家名字 获得地图跑动敌人 跨服
    interface GetDiTuDiRenByPlayerNameCrossServerRsp extends RspObj {
        // 目标信息
            targetInfo ?:PlayerInfoUnit
        // 当前玩家Id
            playerId ?:number
        // 是否模拟玩家信息
            isFaker ?:boolean

    }

// [请求]购买复仇宿敌奖励次数
    interface AddFuChouSuDiRewardCountReq extends ReqObj {
    }

// [反馈]购买复仇宿敌奖励次数
    interface AddFuChouSuDiRewardCountRsp extends RspObj {
        // 是否成功
            isSuccess ?:boolean
        // 购买次数的价格
            cost ?:JiangliInfoUnit

    }

// [请求]挂机随机事件的抢劫玩家
    interface RobPlayerOnStoryLineReq extends ReqObj {
        // 剧情线Entity DB Id
            dbId ?:number
    }

// [反馈]挂机随机事件的抢劫玩家
    interface RobPlayerOnStoryLineRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 返回码：1左侧获胜，2左侧失败
            win ?:number
        // 全部战斗信息的Json串
            fightInfo ?:string

    }

// [请求]复仇敌人当前Project状态搜索
    interface RevengeEnemyProjectStatusSearchReq extends ReqObj {
        // 对手Id
            enemyId ?:number
    }

// [反馈]复仇敌人当前Project状态搜索
    interface RevengeEnemyProjectStatusSearchRsp extends RspObj {
        // 当前Project状态列表
            projectStatusList ?:Array<RevengeEnemySearchProjectStatusUnit>
        // 刷新消耗
            costObj ?:JiangliInfoUnit

    }

// [请求]复仇敌人当前Project内容信息搜索
    interface RevengeEnemyProjectInfoSearchReq extends ReqObj {
        // 对手Id
            enemyId ?:number
    }

// [反馈]复仇敌人当前Project内容信息搜索
    interface RevengeEnemyProjectInfoSearchRsp extends RspObj {
        // 当前Project内容信息列表
            projectInfoList ?:Array<RevengeEnemySearchProjectInfoUnit>

    }

// [请求]前往复仇对手所在页面
    interface RevengeEnemyLeaveForNewLayerReq extends ReqObj {
        // 1、神兽战场；2、小镇坐标
            type ?:number
        // 对手Id
            enemyId ?:number
        // 要前往的战场对应的Project表Id
            projectDefId ?:number
    }

// [反馈]前往复仇对手所在页面
    interface RevengeEnemyLeaveForNewLayerRsp extends RspObj {
        // 1、神兽战场；2、小镇坐标
            type ?:number
        // 0:正常可打；1:玩家房子保护中；2:我的战斗次数已经用完
            townStatus ?:number
        // 0:地图我可达且当前可进入；1:对手地图我条件不足；2:对手地图我次数不足；3:对手所在地图boss已经死亡；
            superBossStatus ?:number
        // 玩家将要进入的地图
            mapId ?:number
        // Project表Id
            projectDefId ?:number
        // 玩家将要进入的地图信息
            mapInfo ?:BossListMapUnit

    }

// [请求]遭遇战和复仇抢夺的扫荡
    interface SweepSuddenFightAndRevengeReq extends ReqObj {
        // 日常任务DbId
            missionDbId ?:number
    }

// [返回]遭遇战和复仇抢夺的扫荡
    interface SweepSuddenFightAndRevengeRsp extends RspObj {
        // 奖励列表
            jiangliList ?:Array<JiangliInfoUnit>
        // 是否战胜
            isWin ?:boolean
        // 积分
            jifen ?:number
        // 本次增加的积分
            addJifen ?:number

    }

}